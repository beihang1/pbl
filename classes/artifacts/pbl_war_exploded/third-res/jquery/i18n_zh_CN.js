﻿///<jscompress sourcefile="bootstrap-combotree-zh_CN.js" />
var i18n_Combotree_select = "请选择";
var i18n_Combotree_loading = "加载中";
///<jscompress sourcefile="common-zh_CN.js" />
var i18n_Common_value_invalid = "值不合法";
var i18n_Common_request_loading_wait ="正在执行您的请求，请稍等...";
var i18n_Common_res_not_access ="该资源不存在或无访问权限";
var i18n_Common_viewing="浏览 &hellip;";
var i18n_Common_warning = '警告';
var i18n_Common_success = '成功';
var i18n_Common_prompt = '提示';
var i18n_Common_error = '错误';
var i18n_Common_confirm = '确认';
var i18n_Common_cancel = '取消';
var i18n_Common_loading = '加载中...';
var i18n_Common_delete = '删除';
var i18n_Common_dowload = '下载';
var i18n_Common_reset = '恢复';
var i18n_Common_upload = '上传';
var i18n_Common_only_select_file = '只能选择文件';
var i18n_Common_must_select_file = '必须上传文件';
var i18n_Common_size_error = '文件 "{name}" ({size} KB) 超过了允许上传的最大大小：{maxSize} KB. 请重新选择文件!';
var i18n_Common_file_count_error = '上传文件数({n}) 超过了允许上传的最大文件数 {m}. 请重新选择文件!';
var i18n_Common_file_not_finde = '文件 "{name}" 未找到!';
var i18n_Common_file_not_read = '文件 "{name}" 不可读.';
var i18n_Common_file_not_preview = '文件预览忽略文件 "{name}".';
var i18n_Common_file_read_error = '读文件时发生错误 "{name}".';
var i18n_Common_file_type_error = '文件类型不合法 "{name}". 仅能选择 "{types}" 等类型的文件进行上传.';
var i18n_Common_file_ext_name_error = '文件扩展名不合法 "{name}". 仅能选择 "{extensions}" 等文件进行上传.';
var i18n_Common_file_invalid = '选择的文件不合法';
var i18n_Common_file_reload = '文件加载中 {index} / {files} &hellip;';
var i18n_Common_file_reload_count = '文件加载中 {index} / {files} - {name} - {percent}% 已完成.';
var i18n_Common_file_selected_count = '{n} 个文件被选中';
///<jscompress sourcefile="zh_CN.js" />
var i18n_Datetimepicker_KeyYear = "年";
var i18n_Datetimepicker_KeyMonth = "月";
var i18n_Datetimepicker_KeyDay = "日";

///<jscompress sourcefile="zh_CN.js" />
var i18n_Dduallistbox_SelectNo = "未选择"
var i18n_Dduallistbox_SelectYes = "已选择"
var i18n_Dduallistbox_AddSelect = "添加选中"
var i18n_Dduallistbox_AddAll = "添加全部"
var i18n_Dduallistbox_FilterWhere = "过滤条件"
var i18n_Dduallistbox_RemoveSelect = "移除选中"
var i18n_Dduallistbox_RemoveAll = "移除全部"
var i18n_Dduallistbox_Total = "共{0}个"
var i18n_Dduallistbox_NULL  = "无"
var i18n_Dduallistbox_ClearFilter = "清空过滤"
var i18n_Dduallistbox_MatchTo = "匹配到"
///<jscompress sourcefile="zh_CN.js" />
var i18n_Jasny_SelectFileBySize = "请选择小于%s的文件";
var i18n_Jasny_SelectFileByType = "请选择类型为%s的文件";


///<jscompress sourcefile="zh_CN.js" />
var i18n_tabelConfig_TotalRecord = "共%s条记录";
var i18n_TableDm_Start = "首页";
var i18n_TableDm_Pre = "上一页";
var i18n_TableDm_Next = "下一页";
var i18n_TableDm_End = "末页";
!function(a){"use strict";a.fn.bootstrapTable.locales["zh_CN"]={formatLoadingMessage:function(){return"正在努力地加载数据中，请稍候……"},formatRecordsPerPage:function(a){return"每页显示 "+a+" 条记录"},formatShowingRows:function(a,b,c){return"显示第 "+a+" 到第 "+b+" 条记录，总共 "+c+" 条记录"},formatSearch:function(){return"搜索"},formatNoMatches:function(){return"没有找到匹配的记录"},formatPaginationSwitch:function(){return"隐藏/显示分页"},formatRefresh:function(){return"刷新"},formatToggle:function(){return"切换"},formatColumns:function(){return"列"}},a.extend(a.fn.bootstrapTable.defaults,a.fn.bootstrapTable.locales["zh_CN"])}(jQuery);
///<jscompress sourcefile="zh_CN.js" />
(function ($) {
    /**
     * Simplified Chinese language package
     * Translated by @shamiao
     */
    $.fn.bootstrapValidator.i18n = $.extend(true, $.fn.bootstrapValidator.i18n, {
        base64: {
            'default': '请输入有效的Base64编码'
        },
        between: {
            'default': '请输入在 %s 和 %s 之间的数值',
            notInclusive: '请输入在 %s 和 %s 之间(不含两端)的数值'
        },
        callback: {
            'default': '请输入有效的值'
        },
        choice: {
            'default': '请输入有效的值',
            less: '请至少选中 %s 个选项',
            more: '最多只能选中 %s 个选项',
            between: '请选择 %s 至 %s 个选项'
        },
        creditCard: {
            'default': '请输入有效的信用卡号码'
        },
        cusip: {
            'default': '请输入有效的美国CUSIP代码'
        },
        cvv: {
            'default': '请输入有效的CVV代码'
        },
        date: {
            'default': '请输入有效的日期'
        },
        different: {
            'default': '请输入不同的值'
        },
        digits: {
            'default': '请输入有效的数字'
        },
        ean: {
            'default': '请输入有效的EAN商品编码'
        },
        emailAddress: {
            'default': '请输入有效的邮件地址'
        },
        file: {
            'default': '请选择有效的文件'
        },
        greaterThan: {
            'default': '请输入大于等于 %s 的数值',
            notInclusive: '请输入大于 %s 的数值'
        },
        grid: {
            'default': '请输入有效的GRId编码'
        },
        hex: {
            'default': '请输入有效的16进制数'
        },
        hexColor: {
            'default': '请输入有效的16进制颜色值'
        },
        iban: {
            'default': '请输入有效的IBAN(国际银行账户)号码',
            countryNotSupported: '不支持 %s 国家或地区',
            country: '请输入有效的 %s 国家或地区的IBAN(国际银行账户)号码',
            countries: {
                AD: '安道​​尔',
                AE: '阿联酋',
                AL: '阿尔巴尼亚',
                AO: '安哥拉',
                AT: '奥地利',
                AZ: '阿塞拜疆',
                BA: '波斯尼亚和黑塞哥维那',
                BE: '比利时',
                BF: '布基纳法索',
                BG: '保加利亚',
                BH: '巴林',
                BI: '布隆迪',
                BJ: '贝宁',
                BR: '巴西',
                CH: '瑞士',
                CI: '科特迪瓦',
                CM: '喀麦隆',
                CR: '哥斯达黎加',
                CV: '佛得角',
                CY: '塞浦路斯',
                CZ: '捷克共和国',
                DE: '德国',
                DK: '丹麦',
                DO: '多米尼加共和国',
                DZ: '阿尔及利亚',
                EE: '爱沙尼亚',
                ES: '西班牙',
                FI: '芬兰',
                FO: '法罗群岛',
                FR: '法国',
                GB: '英国',
                GE: '格鲁吉亚',
                GI: '直布罗陀',
                GL: '格陵兰岛',
                GR: '希腊',
                GT: '危地马拉',
                HR: '克罗地亚',
                HU: '匈牙利',
                IE: '爱尔兰',
                IL: '以色列',
                IR: '伊朗',
                IS: '冰岛',
                IT: '意大利',
                JO: '约旦',
                KW: '科威特',
                KZ: '哈萨克斯坦',
                LB: '黎巴嫩',
                LI: '列支敦士登',
                LT: '立陶宛',
                LU: '卢森堡',
                LV: '拉脱维亚',
                MC: '摩纳哥',
                MD: '摩尔多瓦',
                ME: '黑山',
                MG: '马达加斯加',
                MK: '马其顿',
                ML: '马里',
                MR: '毛里塔尼亚',
                MT: '马耳他',
                MU: '毛里求斯',
                MZ: '莫桑比克',
                NL: '荷兰',
                NO: '挪威',
                PK: '巴基斯坦',
                PL: '波兰',
                PS: '巴勒斯坦',
                PT: '葡萄牙',
                QA: '卡塔尔',
                RO: '罗马尼亚',
                RS: '塞尔维亚',
                SA: '沙特阿拉伯',
                SE: '瑞典',
                SI: '斯洛文尼亚',
                SK: '斯洛伐克',
                SM: '圣马力诺',
                SN: '塞内加尔',
                TN: '突尼斯',
                TR: '土耳其',
                VG: '英属维尔京群岛'
            }
        },
        id: {
            'default': '请输入有效的身份证件号码',
            countryNotSupported: '不支持 %s 国家或地区',
            country: '请输入有效的 %s 国家或地区的身份证件号码',
            countries: {
                BA: '波黑',
                BG: '保加利亚',
                BR: '巴西',
                CH: '瑞士',
                CL: '智利',
                CN: '中国',
                CZ: '捷克共和国',
                DK: '丹麦',
                EE: '爱沙尼亚',
                ES: '西班牙',
                FI: '芬兰',
                HR: '克罗地亚',
                IE: '爱尔兰',
                IS: '冰岛',
                LT: '立陶宛',
                LV: '拉脱维亚',
                ME: '黑山',
                MK: '马其顿',
                NL: '荷兰',
                RO: '罗马尼亚',
                RS: '塞尔维亚',
                SE: '瑞典',
                SI: '斯洛文尼亚',
                SK: '斯洛伐克',
                SM: '圣马力诺',
                TH: '泰国',
                ZA: '南非'
            }
        },
        identical: {
            'default': '请输入相同的值'
        },
        imei: {
            'default': '请输入有效的IMEI(手机串号)'
        },
        imo: {
            'default': '请输入有效的国际海事组织(IMO)号码'
        },
        integer: {
            'default': '请输入有效的整数值'
        },
        ip: {
            'default': '请输入有效的IP地址',
            ipv4: '请输入有效的IPv4地址',
            ipv6: '请输入有效的IPv6地址'
        },
        isbn: {
            'default': '请输入有效的ISBN(国际标准书号)'
        },
        isin: {
            'default': '请输入有效的ISIN(国际证券编码)'
        },
        ismn: {
            'default': '请输入有效的ISMN(印刷音乐作品编码)'
        },
        issn: {
            'default': '请输入有效的ISSN(国际标准杂志书号)'
        },
        lessThan: {
            'default': '请输入小于等于 %s 的数值',
            notInclusive: '请输入小于 %s 的数值'
        },
        mac: {
            'default': '请输入有效的MAC物理地址'
        },
        meid: {
            'default': '请输入有效的MEID(移动设备识别码)'
        },
        notEmpty: {
            'default': '请填写必填项目'
        },
        numeric: {
            'default': '请输入有效的数值，允许小数'
        },
        phone: {
            'default': '请输入有效的电话号码',
            countryNotSupported: '不支持 %s 国家或地区',
            country: '请输入有效的 %s 国家或地区的电话号码',
            countries: {
                BR: '巴西',
                CN: '中国',
                CZ: '捷克共和国',
                DK: '丹麦',
                ES: '西班牙',
                FR: '法国',
                GB: '英国',
                MA: '摩洛哥',
                PK: '巴基斯坦',
                RO: '罗马尼亚',
                RU: '俄罗斯',
                SK: '斯洛伐克',
                TH: '泰国',
                US: '美国',
                VE: '委内瑞拉'
            }
        },
        regexp: {
            'default': '请输入符合正则表达式限制的值'
        },
        remote: {
            'default': '请输入有效的值'
        },
        rtn: {
            'default': '请输入有效的RTN号码'
        },
        sedol: {
            'default': '请输入有效的SEDOL代码'
        },
        siren: {
            'default': '请输入有效的SIREN号码'
        },
        siret: {
            'default': '请输入有效的SIRET号码'
        },
        step: {
            'default': '请输入在基础值上，增加 %s 的整数倍的数值'
        },
        stringCase: {
            'default': '只能输入小写字母',
            upper: '只能输入大写字母'
        },
        stringLength: {
            'default': '请输入符合长度限制的值',
            less: '最多只能输入 %s 个字符',
            more: '需要输入至少 %s 个字符',
            between: '请输入 %s 至 %s 个字符'
        },
        uri: {
            'default': '请输入一个有效的URL地址'
        },
        uuid: {
            'default': '请输入有效的UUID',
            version: '请输入版本 %s 的UUID'
        },
        vat: {
            'default': '请输入有效的VAT(税号)',
            countryNotSupported: '不支持 %s 国家或地区',
            country: '请输入有效的 %s 国家或地区的VAT(税号)',
            countries: {
                AT: '奥地利',
                BE: '比利时',
                BG: '保加利亚',
                BR: '巴西',
                CH: '瑞士',
                CY: '塞浦路斯',
                CZ: '捷克共和国',
                DE: '德国',
                DK: '丹麦',
                EE: '爱沙尼亚',
                ES: '西班牙',
                FI: '芬兰',
                FR: '法语',
                GB: '英国',
                GR: '希腊',
                EL: '希腊',
                HU: '匈牙利',
                HR: '克罗地亚',
                IE: '爱尔兰',
                IS: '冰岛',
                IT: '意大利',
                LT: '立陶宛',
                LU: '卢森堡',
                LV: '拉脱维亚',
                MT: '马耳他',
                NL: '荷兰',
                NO: '挪威',
                PL: '波兰',
                PT: '葡萄牙',
                RO: '罗马尼亚',
                RU: '俄罗斯',
                RS: '塞尔维亚',
                SE: '瑞典',
                SI: '斯洛文尼亚',
                SK: '斯洛伐克',
                VE: '委内瑞拉',
                ZA: '南非'
            }
        },
        vin: {
            'default': '请输入有效的VIN(美国车辆识别号码)'
        },
        zipCode: {
            'default': '请输入有效的邮政编码',
            countryNotSupported: '不支持 %s 国家或地区',
            country: '请输入有效的 %s 国家或地区的邮政编码',
            countries: {
                BR: '巴西',
                CA: '加拿大',
                CZ: '捷克共和国',
                DK: '丹麦',
                GB: '英国',
                IT: '意大利',
                MA: '摩洛哥',
                NL: '荷兰',
                RO: '罗马尼亚',
                RU: '俄罗斯',
                SE: '瑞典',
                SG: '新加坡',
                SK: '斯洛伐克',
                US: '美国'
            }
        }
    });
}(window.jQuery));

///<jscompress sourcefile="zh_CN.js" />
/*!
 * Bootstrap-select v1.6.3 (http://silviomoreto.github.io/bootstrap-select/)
 *
 * Copyright 2013-2014 bootstrap-select
 * Licensed under MIT (https://github.com/silviomoreto/bootstrap-select/blob/master/LICENSE)
 */
(function ($) {
  $.fn.selectpicker.defaults = {
    noneSelectedText: '没有选中任何项',
    noneResultsText: '没有找到匹配项',
    countSelectedText: '选中{1}中的{0}项',
    maxOptionsText: ['超出限制 (最多选择{n}项)', '组选择超出限制(最多选择{n}组)'],
    multipleSeparator: ','
  };
}(jQuery));

///<jscompress sourcefile="zh_CN.js" />
(function(e){"function"==typeof define&&define.amd?define(["jquery","moment"],e):e(jQuery,moment)})(function(e,t){(t.defineLocale||t.lang).call(t,"zh-cn",{months:"一月_二月_三月_四月_五月_六月_七月_八月_九月_十月_十一月_十二月".split("_"),monthsShort:"1月_2月_3月_4月_5月_6月_7月_8月_9月_10月_11月_12月".split("_"),weekdays:"星期日_星期一_星期二_星期三_星期四_星期五_星期六".split("_"),weekdaysShort:"周日_周一_周二_周三_周四_周五_周六".split("_"),weekdaysMin:"日_一_二_三_四_五_六".split("_"),longDateFormat:{LT:"Ah点mm",LTS:"Ah点m分s秒",L:"YYYY-MM-DD",LL:"YYYY年MMMD日",LLL:"YYYY年MMMD日LT",LLLL:"YYYY年MMMD日ddddLT",l:"YYYY-MM-DD",ll:"YYYY年MMMD日",lll:"YYYY年MMMD日LT",llll:"YYYY年MMMD日ddddLT"},meridiemParse:/凌晨|早上|上午|中午|下午|晚上/,meridiemHour:function(e,t){return 12===e&&(e=0),"凌晨"===t||"早上"===t||"上午"===t?e:"下午"===t||"晚上"===t?e+12:e>=11?e:e+12},meridiem:function(e,t){var n=100*e+t;return 600>n?"凌晨":900>n?"早上":1130>n?"上午":1230>n?"中午":1800>n?"下午":"晚上"},calendar:{sameDay:function(){return 0===this.minutes()?"[今天]Ah[点整]":"[今天]LT"},nextDay:function(){return 0===this.minutes()?"[明天]Ah[点整]":"[明天]LT"},lastDay:function(){return 0===this.minutes()?"[昨天]Ah[点整]":"[昨天]LT"},nextWeek:function(){var e,n;return e=t().startOf("week"),n=this.unix()-e.unix()>=604800?"[下]":"[本]",0===this.minutes()?n+"dddAh点整":n+"dddAh点mm"},lastWeek:function(){var e,n;return e=t().startOf("week"),n=this.unix()<e.unix()?"[上]":"[本]",0===this.minutes()?n+"dddAh点整":n+"dddAh点mm"},sameElse:"LL"},ordinalParse:/\d{1,2}(日|月|周)/,ordinal:function(e,t){switch(t){case"d":case"D":case"DDD":return e+"日";case"M":return e+"月";case"w":case"W":return e+"周";default:return e}},relativeTime:{future:"%s内",past:"%s前",s:"几秒",m:"1分钟",mm:"%d分钟",h:"1小时",hh:"%d小时",d:"1天",dd:"%d天",M:"1个月",MM:"%d个月",y:"1年",yy:"%d年"},week:{dow:1,doy:4}}),e.fullCalendar.datepickerLang("zh-cn","zh-CN",{closeText:"关闭",prevText:"&#x3C;上月",nextText:"下月&#x3E;",currentText:"今天",monthNames:["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],monthNamesShort:["一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月"],dayNames:["星期日","星期一","星期二","星期三","星期四","星期五","星期六"],dayNamesShort:["周日","周一","周二","周三","周四","周五","周六"],dayNamesMin:["日","一","二","三","四","五","六"],weekHeader:"周",dateFormat:"yy-mm-dd",firstDay:1,isRTL:!1,showMonthAfterYear:!0,yearSuffix:"年"}),e.fullCalendar.lang("zh-cn",{buttonText:{month:"月",week:"周",day:"日",list:"日程"},allDayText:"全天",eventLimitText:function(e){return"另外 "+e+" 个"}})});
