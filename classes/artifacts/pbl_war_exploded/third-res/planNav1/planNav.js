﻿
/**
* 计划详情展示页面
*/
(function($,undefined){
	$.fn.planNav = function(opt,param){
		return this.each(function(){

			var para = {};    // 保留参数
			var self = this;  // 保存组件对象
			
			var defaults = {
				 

			};
			options = $.extend(defaults,opt);
			options.join=options.join? "true" == options.join:false;
			

			this.init = function(){
				this.createHtml();  // 创建组件html
				this.initTheme();//设置主题
				//this.createCorePlug();  // 调用核心js
			};
			/**
			 * 功能：创建计划的导航课程详情
			 * 参数: 无
			 * 返回: 无
			 */
		this.createHtml = function(){
			
			options.studyNode=new Array(options.planNodeList.length);//计划节点用户学习课程数
			options.nodeCourse=new Array(options.planNodeList.length);//计划节点课程整数数
			var html='';
			var course='';
			html=html+'<div class="plan-step-wrap js-route-panel" id="js-route-panel">';
			
			html=html+'	<ul class="plan-step clearfix">'; 
			$.each(options.planNodeList,function(n,v) {
				var studyNum=0;//该节点下已经学习的课程数
				var nodeCourseNum=0;
				course='';
				html=html+'   <li class="step-item clearfix ';
				if(n==0){
					html=html+'step-first'; //开始节点标记

				}
				
				
				html=html+'">'; 
				html=html+'    <i class="line"></i>'; 
				html=html+'    <i class="dot"></i>'; 
				html=html+'    <span class="hd l">'+v.nodeName+'</span>'; 
				html=html+'    <i class="v-line l"></i>'; 
				html=html+'<div class="bd l clearfix">';
				//遍历节点子节点
				if(v.childNodes){
					
					$.each(v.childNodes,function(cN,cV){ 
						html=html+'    <a href="javascript:;" class="step-anchor"> '; 
						html=html+'    <b id="nodeId'+n+cN+'" class="contentTip">'+this.nodeName+'</b> '; 
						html=html+' <script>';
						html=html+' 	 $("#nodeId'+n+cN+'").poshytip({';

						html=html+' 	 	className: "tip-simple",';

						html=html+' content:"'+this.content+'",';
						html=html+' showTimeout: 0,';

						html=html+' alignTo: "target",';

						html=html+' alignX: "center",';

						html=html+' offsetY: 15,';

						html=html+' allowTipHover: false';

						html=html+' });';
						html=html+'  </script>';
						if(cN < v.childNodes.length-1 ){
							html=html+'    <i class="step-arr"></i>'; 
						}
						html=html+'   </a>'; 
						//遍历节点下的课程
						if(this.nodeCourses){
							
							//课程描述
							var str="";
							course=course+'<ul class="clearfix">';
							$.each(this.nodeCourses,function(){
								nodeCourseNum++;//计划总课程+1
								course=course+'		<li class="course-one course-active">';
								course=course+'				<a href="'+this.courseHref+'" target="_blank">';
				
								course=course+'					<div class="course-list-img">';
								course=course+'						<img width="240" height="135" alt="'+this.title+'"';
								course=course+'							src="'+this.imgSrc+'"/>';
								course=course+'					</div>';
								course=course+'					<h5>';
								if(options.join && '1'== this.sessionUserStudy){
									var studyTagName='已学';
									if(this.sessionUserStudyFormatter != null && typeof this.sessionUserStudyFormatter == "function"){
										studyTagName=this.sessionUserStudyFormatter();

									}
									course=course+'				   	<i class="learned">'+studyTagName+'</i>';
									studyNum++;
								}
								course=course+'						<span>'+this.title+'</span>';
								course=course+'					</h5>';
								if(this.tagList){
									course=course+'				<div class="tags">';
									$.each(this.tagList,function(tN,tV){ 
	                               		 course=course+'			<span>'+tV.tagName+'</span>';
	                                });
	                                 course=course+'			 </div>';
									}
								course=course+'					<div class="tips">';
								str=this.content;
								var contentLen=this.contentLen?this.contentLen:20; 
								if(str){
									if(str.length> contentLen){
										str=str.substr(0, contentLen)+"...";
									}
								}else{
									str="";
								}
								course=course+'						<p class="text-ellipsis">'+str+'</p>';
								var cSName=this.courseStatus?this.courseStatus:'';

								if(this.courseStatusFormatter != null && typeof this.courseStatusFormatter == "function"){
									cSName=this.courseStatusFormatter(this.courseStatus);
								}
								course=course+'						<span class="l ">'+cSName+' &nbsp;&nbsp;&nbsp;</span>';
				
								course=course+'						<span class="l ml20"> '+this.numTudy+'人学习</span></div> ';
								if(this.studyDuration){
									course=course+'					<span class="time-label"> '+this.studyDuration+'</span> ';
								}
								
								//course=course+'					<b class="follow-label">跟我学</b> </a>';
								course=course+'			</a></li>';
							});
							
							course=course+'</ul>';
						}
						
						
					});
					
				}
				
				html=html+'</div>'; 
				html=html+'<div class="step-medias-wrap" style="display: none">';
				html=html+'<div class="step-medias course-list">';
				html=html+course;
				html=html+'</div>'; 
				html=html+'</div>'; 
				html=html+'</li>'; 
				options.studyNode[n]=studyNum;
				options.nodeCourse[n]=nodeCourseNum
			});
			html=html+'</ul>'; 
			html=html+'</div>'; 
			  
 
			var d=$(self);
			$(self).html(html); 
		};
		this.initTheme=function(){  

            var join=options.join; 
            if(join==false){
            	$('.step-item').each(function(){
            		$(this).removeClass('step-cur')});
            }else{
            	$('.step-item').each(function(n,v){
            		if(options.studyNode[n] > 0){
            			if(options.studyNode[n] == options.nodeCourse[n] ){
							$(this).addClass('step-ok');
            			}else{
            				$(this).addClass('step-cur');
            			}
            			
            			$(this).children('span').css('color','#00b33b');//已经学习，节点名称设置绿色
            		}
            	});
            }
            
			$(".course-list ul").hide();  
			$(".plan-step-wrap .step-item .step-anchor b").each(function(index){
				//计划标签添加点击事件
				$(this).click(function(){ 
					if($(this).hasClass("tagSelected")){
						$(".plan-step-wrap .step-item .step-anchor b").each(function(){
							$(this).removeClass("tagSelected")
							$(this).removeAttr("style");
						});
						//点击取消 
						//该计划课程关闭
						$(this).parent().parent().next().hide();
						$(this).parent().parent().next().children().children().each(function(i){$(this).hide()});
					}else{ 
						$(".plan-step-wrap .step-item .step-anchor b").each(function(){
							$(this).removeClass("tagSelected")
							$(this).removeAttr("style");
						});
						//点击选中
						
						$('.step-medias-wrap .clearfix').each(function(i){
							if(i==index){
									$(this).show();
								}else{
									$(this).hide();
								}
						}); 
						$(this).parent().parent().next().show();
						$(this).addClass("tagSelected"); 
						if(join){
							$(this).css('background-color','#00b33b');//已经参加计划，主题设置绿色

						}else{
							$(this).css('background-color','#f01400');//退出该计划，设置红色
						}
						
						$(this).css('color','white');
						
					} 
				});
			});
		};
		// 初始化上传控制层插件
			this.init();
		});
		
	
	};
})(jQuery);
