﻿
/**
* 计划详情展示页面
*/
(function($,undefined){
	$.fn.planNav = function(opt,param){
		return this.each(function(){

			var para = {};    // 保留参数
			var self = this;  // 保存组件对象
			
			var defaults = {
				 

			};
			options = $.extend(defaults,opt);
			
			//options.join=options.join? "true" == options.join:false;
			

			this.init = function(){
				this.createHtml();  // 创建组件html
				this.initTheme();//设置主题
				//this.createCorePlug();  // 调用核心js
			};
			/**
			 * 功能：创建计划的导航课程详情
			 * 参数: 无
			 * 返回: 无
			 */
		this.createHtml = function(){
			options.studyNode=new Array(options.planNodeList.length);//计划节点用户学习课程数
			options.nodeCourse=new Array(options.planNodeList.length);//计划节点课程整数数
			var html='';
			var course='';
			html=html+'<div class="plan-step-wrap js-route-panel" id="js-route-panel">';
			
			html=html+'	<ul class="plan-step clearfix">'; 
			$.each(options.planNodeList,function(n,v) {
			//	debugger;
				var studyNum=0;//该节点下已经学习的课程数
				var nodeCourseNum=0;
				course='';
				html=html+'   <li class="step-item clearfix ';
				if(n==0){
					html=html+'step-first'; //开始节点标记
				}
				
				
				html=html+'">'; 
				if(n==options.planNodeList.length-1){
					html=html+'    <i class="line" style="height: 25px"></i>';
				}else{
					html=html+'    <i class="line"></i>';
				}
				 
				html=html+'    <i class="dot"></i>'; 
				html=html+'    <span class="hd hd_l" style="font-size: 16px;">'+v.nodeName+'</span>'; 
				html=html+'    <i class="v-line l"></i>'; 
				html=html+'<div class="bd l clearfix">';
				
				//遍历节点下的课程
					if(this.courses){
						//课程描述
						var str="";
						course=course+'<ul class="clearfix">';
						$.each(this.courses,function(){
							nodeCourseNum++;//计划总课程+1
							course=course+'		<li class="course-one course-active" style="width:280px">';
							if(options.join){
								course=course+'				<a href="/pblMgr/pub/course/courseDetailInit.action?courseId='+this.id+'" target="_blank">';
							}
							course=course+'					<div class="course-list-img">';
							course=course+'						<img width="240" height="135" alt="'+this.title+'"';
							course=course+'							src="/pblMgr/pub/image.action?fileId='+this.pictureId+'"/>';
							course=course+'					</div>';
							course=course+'					<h5>';
							if(options.join && '0'!= this.studySchedule&&null!=this.studySchedule){
								var studyTagName='已学';
								course=course+'				   	<i class="learned">'+studyTagName+'</i>';
								studyNum++;
							}
							course=course+'						<span>'+this.title+'</span>';
							course=course+'					</h5>';
							if(this.tagList){
								course=course+'				<div class="tags">';
								$.each(this.tagList,function(tN,tV){ 
									 course=course+'			<span>'+tV.tagName+'</span>';
								});
								 course=course+'			 </div>';
								}
							course=course+'					<div class="tips">';
							str=this.summary;
							var contentLen=this.contentLen?this.contentLen:20; 
							if(str && str != ''){
								if(str.length> contentLen){
									str=str.substr(0, contentLen)+"...";
								}
							}else{
								str="<div>　</div>";
							}
							course=course+'						<p class="text-ellipsis" style="margin-top: 10px;">'+str+'</p>';
							course=course+'						<span class="l ml20" style="padding-right: 20px;"> '+this.numFavorite+'人收藏&nbsp;&nbsp;&nbsp;&nbsp;'+this.numStudy+'人学习</span></div> ';
							if(this.studyDuration){
								course=course+'					<span class="time-label"> '+this.studyDuration+'分钟</span> ';
							}
							
							course=course+'			</a></li>';
						});
						
						course=course+'</ul>';
					}
				html=html+'</div>'; 
				html=html+'<div class="content" style="margin-top: 20px;font-size: 14px;">'+this.content+'</div>';
				html=html+'<div class="step-medias-wrap" style="display: none">';
				html=html+'<div class="step-medias course-list">';
				html=html+course;
				html=html+'</div>'; 
				html=html+'</div>'; 
				html=html+'</li>'; 
				options.studyNode[n]=studyNum;
				options.nodeCourse[n]=nodeCourseNum
			});
			html=html+'</ul>'; 
			html=html+'</div>'; 
			  
 
			var d=$(self);
			$(self).html(html); 
		};
		this.initTheme=function(){  
            var join=options.join; 
            if(join==false){
            	$('.step-item').each(function(){
            		$(this).removeClass('step-cur')});
            }else{
            	$('.step-item').each(function(n,v){
            		if(options.studyNode[n] > 0){
            			if(options.studyNode[n] == options.nodeCourse[n] ){
							$(this).addClass('step-ok');
            			}else{
            				$(this).addClass('step-cur');
            			}
            			
            			$(this).children('span').css('color','#00b33b');//已经学习，节点名称设置绿色
            		}
            	});
            }
            
			$(".course-list ul").hide();  
			$(".plan-step-wrap .step-item  .hd_l").each(function(index){
				if("1"==options.studyModel){
					var stud = options.planNodeList[index].studyNodeFlag;
						$(this).click(function(){
							if("1"==stud||index==0){
								if($(this).hasClass("tagSelected")){
									$(".plan-step-wrap .step-item .hd_l").each(function(){
										$(this).removeClass("tagSelected")
										$(this).removeAttr("style");
									});
									//点击取消 
									//该计划课程关闭
									$(this).next().next().next().next().hide();
									$(this).parent().parent().next().children().children().each(function(i){$(this).hide()});
								}else{ 
									$(".plan-step-wrap .step-item .hd_l").each(function(){
										$(this).removeClass("tagSelected")
										$(this).removeAttr("style");
									});
									//点击选中
									$('.step-medias-wrap .clearfix').each(function(i){
										if(i==index){
												$(this).show();
											}else{
												$(this).hide();
											}
									}); 
									$(this).next().next().next().next().show();
									$(this).addClass("tagSelected"); 
									if(join){
										$(this).css('color','#00b33b').css("font-size","16px");//已经参加计划，主题设置绿色
			
									}else{
										$(this).css('color','#f01400').css("font-size","16px");//退出该计划，设置红色
									}
								} 
							}else{
								warning("开启该节点需学习完上一节点所有课程！");
							}
						});
					
				}else{
					$(this).click(function(){ 
						if($(this).hasClass("tagSelected")){
							$(".plan-step-wrap .step-item .hd_l").each(function(){
								$(this).removeClass("tagSelected")
								$(this).removeAttr("style");
							});
							//点击取消 
							//该计划课程关闭
							$(this).next().next().next().next().hide();
							$(this).parent().parent().next().children().children().each(function(i){$(this).hide()});
						}else{ 
							$(".plan-step-wrap .step-item .hd_l").each(function(){
								$(this).removeClass("tagSelected")
								$(this).removeAttr("style");
							});
							//点击选中
							$('.step-medias-wrap .clearfix').each(function(i){
								if(i==index){
										$(this).show();
									}else{
										$(this).hide();
									}
							}); 
							$(this).next().next().next().next().show();
							$(this).addClass("tagSelected"); 
							if(join){
								$(this).css('color','#00b33b').css("font-size","16px");//已经参加计划，主题设置绿色
	
							}else{
								$(this).css('color','#f01400').css("font-size","16px");//退出该计划，设置红色
							}
						} 
					});
				}
			});
		};
		// 初始化上传控制层插件
			this.init();
		});
		
	
	};
})(jQuery);
