

(function($) {
	var BootstrapIconPicker = function (el, options) {
	    this.options = options;
	    this.$el = $(el);
	    this.init();
	};
	
	BootstrapIconPicker.prototype.init = function () {
		var _this = this;
		if(!this.options.buttonOnly && this.$el.data("iconPicker")==undefined ){
			_this.$el.addClass("form-control");
			_this.$wraper=$("<div/>",{class:"input-group iconselect-rythm"});
			_this.$el.wrap(this.$wraper);
			var dv = _this.options.value ? _this.options.value : '';
			_this.$p=$("<span class=\"input-group-addon\" style=\"width:36px; border-left:none;background: none;\"><i class=\"" + dv + "\"></i></span>");
			_this.$button=$("<span class=\"input-group-addon pointer\" style=\"background: none;\"><i class=\"glyphicon  glyphicon-picture\"></i></span>");
			_this.$el.after(_this.$p);
			_this.$p.after(_this.$button);
			_this.$preview = _this.$p.find("i");
			_this.$button.click(function(){
				if(!_this.disabled){
					_this.createUI();
					_this.showList(_this.options.icons);
				}
        	});
            _this.$el.data("iconPicker",{attached:true});
         }
	};
	
	BootstrapIconPicker.prototype.createUI = function(){
		var $element = this.$el;
		var _this = this;
     	_this.$popup=$('<div/>',{
     		css: {
	        		'top':$element.offset().top+$element.outerHeight()+6,
	        		'left':$element.offset().left
	        	},
	        	class:'icon-popup'
     	})

     	_this.$popup.html('<div class="ip-control"> \
					          <ul> \
					            <li><a href="javascript:;" class="btn" data-dir="-1"><span class="glyphicon  glyphicon-fast-backward"></span></a></li> \
					            <li><input type="text" class="form-control ip-search glyphicon  glyphicon-search" style="margin-top: -6px;" placeholder="Search" /></li> \
					            <li><a href="javascript:;"  class="btn" data-dir="1" style="margin-right: 3px;"><span class="glyphicon  glyphicon-fast-forward"></span></a></li> \
					          </ul> \
					      </div> \
					     <div class="icon-list"> </div> \
				         ').appendTo("body");
     	
     	
     	_this.$popup.addClass('dropdown-menu').show();
     	_this.$popup.mouseenter(function() {  _this.mouseOver=true;  }).mouseleave(function() { _this.mouseOver=false;  });

     	var lastVal="", start_index=0,per_page=30,end_index=start_index+per_page;
     	$(".ip-control .btn",_this.$popup).click(function(e){
             e.stopPropagation();
             var dir=$(this).attr("data-dir");
             start_index=start_index+per_page*dir;
             start_index=start_index<0?0:start_index;
             var max = _this.options.filtericons ? _this.options.filtericons.length : _this.options.icons.length;
             max = (parseInt(max/30) + ((max%30 > 0)?1:0))*30;
             if(start_index+per_page<=max){
               $.each($(".icon-list>ul li"),function(i){
                   if(i>=start_index && i<start_index+per_page){
                      $(this).show();
                   }else{
                     $(this).hide();
                   }
               });
             }else{
               start_index=max - 30;
             }
         });
     	
     	$('.ip-control .ip-search',_this.$popup).on("keyup",function(e){
             if(lastVal!=$(this).val()){
                 lastVal=$(this).val();
                 if(lastVal==""){
                 	_this.showList(_this.options.icons);
                 }else{
                	 _this.showList($(_this.options.icons)
						        .map(function(i,v){ 
							            if(v.toLowerCase().indexOf(lastVal.toLowerCase())!=-1){return v} 
							        }).get());
					}
                 
             }
         });  
     	$(document).mouseup(function (e){
			    if (!_this.$popup.is(e.target) && _this.$popup.has(e.target).length === 0) {
			        _this.removeInstance();
			    }
			});
     };
     
     BootstrapIconPicker.prototype.removeInstance = function(){
     	$(".icon-popup").remove();
     };
     
     BootstrapIconPicker.prototype.setValue = function(value){
    	 this.$el.val(value);
    	 this.$preview.attr('class',value);
     }
     
     BootstrapIconPicker.prototype.disable = function(){
    	 this.$el.prop('readonly', true);
    	 this.disabled = true;
     }
     
     BootstrapIconPicker.prototype.enable = function(){
    	 this.$el.prop('readonly', false);
    	 this.disabled = false;
     }
     
     BootstrapIconPicker.prototype.isDisabled = function(){
    	 return this.disabled;
     }

     BootstrapIconPicker.prototype.showList = function (arrLis){
    	 var $element = this.$el;
    	 var _this = this;
    	 _this.options.filtericons = arrLis;
     	$ul=$("<ul>");
     	
     	for (var i in arrLis) {
     		var t = arrLis[i];
     		$ul.append("<li><a href=\"#\" title=\""+ t +"\"><span class=\"" + t + "\"></span></a></li>");
     	};

     	$(".icon-list",_this.$popup).html($ul);
     	$(".icon-list li a",_this.$popup).click(function(e){
     		e.preventDefault();
     		var title=$(this).attr("title");
     		_this.setValue(title);
     		_this.removeInstance();
     	});
     };
	
    $.fn.BootstrapIconPicker = function( option, _relatedTarget) {
    	var $this = $(this),
    	data = $this.data('bootstrap.iconpicker'),
        options = $.extend({}, $this.data(),
            typeof option === 'object' && option);

	    if (typeof option === 'string') {
	        if (!data) {
	            return;
	        }
	        if(option === 'options'){
	        	value = data[option];
	        }else{
	        	value = data[option](_relatedTarget);
	        }
	    }
	    if (!data) {
	        $this.data('bootstrap.iconpicker', (data = new BootstrapIconPicker(this, options)));
	    }
    }

}(jQuery));