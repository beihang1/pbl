/**
 * Bootstrap Table Config
 */
(function ($) {
    'use strict';

    $.extend($.fn.bootstrapTable.defaults, {
    	cache: false,
    //    height: 400,
        striped: true,
        pagination: true,
        sidePagination:'server',
        pageSize: 20,
        pageList: [10, 20, 50, 100, 200],
        search: false,
        showColumns: true,
        formatShowingRows:function (pageFrom, pageTo, totalRows) {
            return i18n_tabelConfig_TotalRecord.format(totalRows);
        },
        showRefresh: false,
        showToggle:true,
      //  clickToSelect: true,
        minimumCountColumns: 2,
        onLoadSuccess:function(data){
        },
        onLoadError: function (req, status, e) {
        	if(status == 408){
        		// 登录超时
  			  var loginUrl = CONTEXT_ROOT + "login.action?error=error.session.timeout";
  			  if(req.responseText){
  				  try{
  					  var rs = $.parseJSON(req.responseText);
  					  if(rs.data && rs.data.loginUrl){
						  loginUrl = rs.data.loginUrl;
					  }
  				  }catch(e){}
  			  }
  			  window.location.href = loginUrl;
  		  } else if(status == 403){
  			  // 无访问权限
  			  warning(i18n_Common_res_not_access);
  		  } else {
  	    	var msg = "无法访问网络";
  			parent.error(msg);
  		  }
        	return false;
        }
    });

    $.extend($.fn.bootstrapTable.columnDefaults, {
    	align: 'left',
        valign: 'middle'
    });
})(jQuery);