var i18n_tabelConfig_TotalRecord = "Total %s records";
var i18n_TableDm_Start = "First";
var i18n_TableDm_Pre = "Pre";
var i18n_TableDm_Next = "Next";
var i18n_TableDm_End = "Last";
!function(a){"use strict";a.fn.bootstrapTable.locales["en_US"]={formatLoadingMessage:function(){return"Loading, please wait..."},formatRecordsPerPage:function(a){return a+" records per page"},formatShowingRows:function(a,b,c){return"Showing "+a+" to "+b+" of "+c+" rows"},formatSearch:function(){return"Search"},formatNoMatches:function(){return"No matching records found"},formatPaginationSwitch:function(){return"Hide/Show pagination"},formatRefresh:function(){return"Refresh"},formatToggle:function(){return"Toggle"},formatColumns:function(){return"Columns"}},a.extend(a.fn.bootstrapTable.defaults,a.fn.bootstrapTable.locales["en_US"])}(jQuery);