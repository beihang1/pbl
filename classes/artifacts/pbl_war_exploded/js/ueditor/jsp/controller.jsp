<%@ page language="java" contentType="text/html; charset=UTF-8"
	import="com.baidu.ueditor.ActionEnter"
	import="com.cloudsoaring.web.common.constant.ConfigNames"
	import="com.cloudsoaring.web.common.utils.ConfigUtil"
    pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%

    request.setCharacterEncoding( "utf-8" );
	response.setHeader("Content-Type" , "text/html");
	
	String rootPath = application.getRealPath( "/" );
	String uploadRootPath = ConfigUtil.getProperty(ConfigNames.UEDITOR_UPLOAD_ROOT_PATH);
	out.write( new ActionEnter( request, rootPath, uploadRootPath).exec() );
	
%>