/***<!-- 全局Angular变量，其他地方直接用myApp变量来使用 -->**/
	var myApp = angular.module("myApp",[]);
	//angularjs $http 服务提交参数配置
	myApp.config(function($httpProvider) {
    $httpProvider.defaults.headers.put['Content-Type'] = 'application/x-www-form-urlencoded';
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
    // Override $http service's default transformRequest
    $httpProvider.defaults.transformRequest = [function(data) {
        var param = function(obj) {
            var query = '';
            var name, value, fullSubName, subName, subValue, innerObj, i;
            for (name in obj) {
                value = obj[name];

                if (value instanceof Array) {
                    for (i = 0; i < value.length; ++i) {
                        subValue = value[i];
                        fullSubName = name + '[' + i + ']';
                        innerObj = {};
                        innerObj[fullSubName] = subValue;
                        query += param(innerObj) + '&';
                    }
                } else if (value instanceof Object) {
                    for (subName in value) {
                        subValue = value[subName];
                        fullSubName = name + '[' + subName + ']';
                        innerObj = {};
                        innerObj[fullSubName] = subValue;
                        query += param(innerObj) + '&';
                    }
                } else if (value !== undefined && value !== null) {
                    query += encodeURIComponent(name) + '='
                            + encodeURIComponent(value) + '&';
                }
            }

            return query.length ? query.substr(0, query.length - 1) : query;
        };

        return angular.isObject(data) && String(data) !== '[object File]'
                ? param(data)
                : data;
    }];
})
.filter("formatResponseTime",function(){
	return function(str){
		var ss = parseInt(str);
		if(ss < 60){
			return ss+"秒";
		}else if(ss < 60*60){
			return Math.floor(ss/60)+"分钟";
		}else if(ss < 60*60*12){
			return Math.floor(ss/3600)+"小时";
		}else if(ss < 60*60*24){
			return "半天";
		}else if(ss < 60*60*24*30){
			return Math.floor(ss/(3600*24))+"天";
		}else{
			return Math.floor(ss/(3600*24*30))+"个月";
		}
	}
})
.filter("replaceToNumber",function(){
	return function(str){
		if(str==null || str == '' || str == undefined || str == 'undefined'){
			return 0;
		}else{
			return str;
		}
	}
})
.filter("formatToHour",function(){
	return function (str){
		if(str==null || str == '' || str == undefined || str == 'undefined'){
			str = 0;
		}else {
			str = parseInt(str);
		}
		return Math.floor(str/3600)+"";
	}
	
})
.filter("subStringLength",function(){
	return function(str,length){
		if(str==null || str == '' || str == undefined || str == 'undefined'){
			return "";
		}else{
			if(str.length > length){
				str = str.substr(0,length)+"..."
				return str;
			}else{
				return str;
			}
		}
	}
})
.filter("formatAvgAccept",function(){
	return function(str){
		if(str==null || str == '' || str == undefined || str == 'undefined'){
			return str;
		}else{
			var ss = parseInt(str);
			if(ss >=0 && ss <40){
				return "低";
			}else if(ss >= 40 && ss<60){
				return "中";
			}else{
				return "高";
			}
			
		}
	}
});
	
//根据id删除数组中对应数据
function deleteItem(list,id){
	var arr = new Array();
	for(var i=0;i<list.length;i++){
		var item = list[i];
		if(item.id != id){
			arr.push(list[i]);
		}
	}
	return arr;
}

//获取分页显示ITEM
function getPageList(page,total){
	var pageList = new Array();
	if(total <= 1){
		//不显示分页
		var pageList = [];
		return pageList;
	}
	//总数>pageSize
	//首页和上一页
	if(page<=1){
		pageList.push({text:"首页",css:"page_li_disabled",clickNum:1});
		pageList.push({text:"上一页",css:"page_li_disabled",clickNum:(page-1)});
	}else{
		pageList.push({text:"首页",css:"page_li_normal",clickNum:1});
		pageList.push({text:"上一页",css:"page_li_normal",clickNum:(page-1)});
	}
	//中间数字
	for(var i=1;i<=total;i++){
		if(total > 10){
			if(page < 10){
				if(i<=10){
					var css = "page_li_normal";
					if(page == i){
						css = "page_li_active";
					}
					pageList.push({text:""+i,css:css,clickNum:i});
				}
			}else if(page >= 10 && page<(total-5)){
				if(i>(page-5) && i<=(page+5)){
					var css = "page_li_normal";
					if(page == i){
						css = "page_li_active";
					}
					pageList.push({text:""+i,css:css,clickNum:i});
				}
			}else if(page>=(total-5)){
				if(i>(total-10)){
					var css = "page_li_normal";
					if(page == i){
						css = "page_li_active";
					}
					pageList.push({text:""+i,css:css,clickNum:i});
				}
			}
			
		}else{
			var css = "page_li_normal";
			if(page == i){
				css = "page_li_active";
			}
			pageList.push({text:""+i,css:css,clickNum:i});
		}
	}
	//尾页和下一页
	if(page>=total){
		pageList.push({text:"下一页",css:"page_li_disabled",clickNum:(page+1)});
		pageList.push({text:"尾页",css:"page_li_disabled",clickNum:page});
	}else{
		pageList.push({text:"下一页",css:"page_li_normal",clickNum:(page+1)});
		pageList.push({text:"尾页",css:"page_li_normal",clickNum:total});
	}
	return pageList;
}
