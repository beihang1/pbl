/***<!-- 全局Angular变量，其他地方直接用myApp变量来使用 -->**/
	var myApp = angular.module("myApp",[]);
	//angularjs $http 服务提交参数配置
	myApp.config(function($httpProvider) {
    $httpProvider.defaults.headers.put['Content-Type'] = 'application/x-www-form-urlencoded';
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
    // Override $http service's default transformRequest
    $httpProvider.defaults.transformRequest = [function(data) {
        var param = function(obj) {
            var query = '';
            var name, value, fullSubName, subName, subValue, innerObj, i;
            for (name in obj) {
                value = obj[name];

                if (value instanceof Array) {
                    for (i = 0; i < value.length; ++i) {
                        subValue = value[i];
                        fullSubName = name + '[' + i + ']';
                        innerObj = {};
                        innerObj[fullSubName] = subValue;
                        query += param(innerObj) + '&';
                    }
                } else if (value instanceof Object) {
                    for (subName in value) {
                        subValue = value[subName];
                        fullSubName = name + '[' + subName + ']';
                        innerObj = {};
                        innerObj[fullSubName] = subValue;
                        query += param(innerObj) + '&';
                    }
                } else if (value !== undefined && value !== null) {
                    query += encodeURIComponent(name) + '='
                            + encodeURIComponent(value) + '&';
                }
            }

            return query.length ? query.substr(0, query.length - 1) : query;
        };

        return angular.isObject(data) && String(data) !== '[object File]'
                ? param(data)
                : data;
    }];
})
.filter("formatResponseTime",function(){
	return function(str){
		return formatResponseTime(str);
	}
})
.filter("replaceToNumber",function(){
	return function(str){
		if(str==null || str == '' || str == undefined || str == 'undefined'){
			return 0;
		}else{
			return str;
		}
	}
})
.filter("formatToHour",function(){
	return function (str){
		return formatDurationToHour(str);
	}
})
.filter("subStringLength",function(){
	return function(str,length){
		if(str==null || str == '' || str == undefined || str == 'undefined'){
			return "";
		}else{
			if(str.length > length){
				str = str.substr(0,length)+"..."
				return str;
			}else{
				return str;
			}
		}
	}
})
.filter("formatAvgAccept",function(){
	return function(str){
		return formatMasterAccept();
	}
})
.filter("to_trusted",['$sce', function ($sce){  
    return function (text) {  
        return $sce.trustAsHtml(text);  
    }  
}])
.filter("formatPublishTime",function(){
	return function(str){
		return formatPublishTime(str);
	}
});
	
//根据id删除数组中对应数据
function deleteItem(list,id){
	var arr = new Array();
	for(var i=0;i<list.length;i++){
		var item = list[i];
		if(item.id != id){
			arr.push(list[i]);
		}
	}
	return arr;
}

//获取分页显示ITEM
function getPageList(page,total){
	var pageList = new Array();
	if(total <= 1){
		//不显示分页
		var pageList = [];
		return pageList;
	}
	//总数>pageSize
	//首页和上一页
	if(page<=1){
		pageList.push({text:"首页",css:"page_li_disabled",clickNum:1});
		pageList.push({text:"上一页",css:"page_li_disabled",clickNum:(page-1)});
	}else{
		pageList.push({text:"首页",css:"page_li_normal",clickNum:1});
		pageList.push({text:"上一页",css:"page_li_normal",clickNum:(page-1)});
	}
	//中间数字
	for(var i=1;i<=total;i++){
		if(total > 10){
			if(page < 10){
				if(i<=10){
					var css = "page_li_normal";
					if(page == i){
						css = "page_li_active";
					}
					pageList.push({text:""+i,css:css,clickNum:i});
				}
			}else if(page >= 10 && page<(total-5)){
				if(i>(page-5) && i<=(page+5)){
					var css = "page_li_normal";
					if(page == i){
						css = "page_li_active";
					}
					pageList.push({text:""+i,css:css,clickNum:i});
				}
			}else if(page>=(total-5)){
				if(i>(total-10)){
					var css = "page_li_normal";
					if(page == i){
						css = "page_li_active";
					}
					pageList.push({text:""+i,css:css,clickNum:i});
				}
			}
			
		}else{
			var css = "page_li_normal";
			if(page == i){
				css = "page_li_active";
			}
			pageList.push({text:""+i,css:css,clickNum:i});
		}
	}
	//尾页和下一页
	if(page>=total){
		pageList.push({text:"下一页",css:"page_li_disabled",clickNum:(page+1)});
		pageList.push({text:"尾页",css:"page_li_disabled",clickNum:total});
	}else{
		pageList.push({text:"下一页",css:"page_li_normal",clickNum:(page+1)});
		pageList.push({text:"尾页",css:"page_li_normal",clickNum:total});
	}
	return pageList;
}

//判断是否为空
function emptyStr(str){
	return (str==null||str==undefined||str==''||str=='null'||str=='undefined');
}


//将字符串标签拆成数组
function subToArr(str){
	if(emptyStr(str)){
		str = '';
	}
	var arr = str.split(" ");
	var newArr = [];
	for(var i=0;i<arr.length;i++){
		if(!emptyStr(arr[i])){
			newArr.push(arr[i]);
		}
	}
	return newArr;
}


