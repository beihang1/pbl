/**
 * @author hxiao
 * Sample Code:<div data-toggle="selector" data-id="xuanzheqi" data-empty="" data-url="@url("/role/getRoleModule.action?roleId=01")"></div>
 */

!function ($) {
	 var BootstrapSelector = function (el, options) {
        this.options = options;
        this.$element = $(el);
        this.$element_ = this.$element.clone();
        this.init();
    };
	    
    BootstrapSelector.DEFAULTS = {
        value:'',
        url:'',
        multiple:false,
        optionWidth:90,
        popplace:'top',
        labelfield:'',
        valuefield:'',
        onChange:function(value){
        	return false;
        },
        showLines:1,
        period:false
    };
    
    BootstrapSelector.prototype.format = function(str) {
        var args = arguments,
            flag = true,
            i = 1;

        str = str.replace(/%s/g, function () {
            var arg = args[i++];

            if (typeof arg === 'undefined') {
                flag = false;
                return '';
            }
            return arg;
        });
        if (flag) {
            return str;
        }
        return '';
    }

    BootstrapSelector.prototype.init = function () {
    	var _this = this;
    	this.$element.addClass('selector-rythm');
    	//区间选项
    	if(this.options.period){
    		//暂不支持
    	}else{
    		this.$more = $('<a class="selector-more-rythm"></a>');
    		this.$element.append(this.$more);
    		this.$more.click(function(){
    			//切换
    			_this.toggle();
    		});
    	}
    	this.$container = $('<ul class="selector-container-rythm"></ul>');
    	this.$element.append(this.$container);
    	//加载数据
    	var d = this.options.data ? this.options.data : this.options.url;
    	this.load(d, this.options.value);
    };
    
    BootstrapSelector.prototype.toggle = function(){
    	var mc = !this.$more.hasClass('opened');
    	this.$container.removeClass('opened');
    	if(mc){
    		this.$more.addClass('opened');
    		this.$container.addClass('opened');
    	}else{
    		this.$more.removeClass('opened');
    	}
    };
    
    BootstrapSelector.prototype.removeItems = function(){
    	this.$container.find('.selector-item-rythm').remove();
    };
    
    BootstrapSelector.prototype.load = function (value, sdata) {
    	var _this = this;
        if (typeof value !== 'undefined') {
        	 //url
             if(typeof value === 'string'){
            	 $.ajax({
               		url: value,
               		type: "get",
     	            success: function(data) {
     	            	_this.loadData(data, sdata);
     		            } 
               		});
             }else if($.isArray(value)){
            	 _this.loadData(value, sdata);
             }
        }else{
        	this.removeItems();
        }
        this.refresh();
    };
      
    BootstrapSelector.prototype.loadData = function(opts, sdata){
    	this.removeItems();
    	var nopts = [];
        if (typeof opts !== 'undefined') {
        	  var array = [];
        	  
        	  if($.isArray(opts)){
        		  array = opts;
        	  }else if(opts.hasOwnProperty('data') && $.isArray(opts.data)){
    			  array = opts.data;
    		  }
        	
        	  if($.isArray(array)){
            	 //数组
            	 for(var i=0; i<array.length; i++){
            		 var v = array[i];
            		 var ol = '';
            		 var ov = '';
            		 if(typeof v === 'string'){
            			 ol = v;
            			 ov = v;
            		 }else if(typeof v === 'object'){
            			 var canAdd = 0;
            			 if(v.hasOwnProperty(this.options.labelfield)){
            				 ol = v[this.options.labelfield];
            				 canAdd = 1;
            			 }
            			 if(v.hasOwnProperty(this.options.valuefield)){
            				 ov = v[this.options.valuefield];
            				 if(canAdd == 1){
            					 canAdd = 3;
            				 }else{
            					 canAdd = 2;
            				 }
            			 }
            			 if(canAdd == 1){
            				 ov = ol;
            			 }else if(canAdd == 2){
            				 ol = ov;
            			 }else if(canAdd == 0){
            				 continue;
            			 }
            		 }
            		 nopts.push('<li style="width: ' + this.options.optionWidth+'px;"><a title="' + ol + '" class="selector-item-rythm" data-value="' + ov + '">' + ol + '</a></li>');
            	 }
             }
        }
        this.$container.append(nopts.join(""));
        var this_ = this;
        this.$container.find('.selector-item-rythm').click(function(){
        	this_.setValue($(this).attr('data-value'));
        });
        this.setValue(sdata);
    };
    
    BootstrapSelector.prototype.getValue = function () {
    	var value =  this.$container.find('.selector-active-rythm').attr('data-value');
    	return value ? value :"";
    };
    
    BootstrapSelector.prototype.getText = function () {
    	var text = this.$container.find('.selector-active-rythm').text();
    	return text ? text :"";
    };
    
    BootstrapSelector.prototype.refresh = function () {
    	var sh = this.$container[0].scrollHeight;
    	var h = this.$container.height();
    	if(Math.abs(sh - h) < 10 && !this.$more.hasClass('opened')){
    		this.$more.css('visibility', 'hidden');
    	}else if(Math.abs(sh - h) < 10 && h < 30 && this.$more.hasClass('opened')){
    		this.$more.css('visibility', 'hidden');
    	}else{
    		this.$more.css('visibility', 'visible');
    	}
    };
    
    BootstrapSelector.prototype.selectNone = function () {
    	this.$container.find('.selector-active-rythm').removeClass("selector-active-rythm");
    };
    
    BootstrapSelector.prototype.setValue = function (value) {
    	this.selectNone();
    	if(value){
    		this.$container.find('.selector-item-rythm[data-value="' + value +'"]').addClass("selector-active-rythm");
    		if($('.selector-tags-rythm').length == 1){
    			$('.selector-tags-rythm').tagsinput('removeAll');
    			var hasChild = false;
    			$('.selector-rythm').each(function(){
    				var id = $(this).attr('id');
    				var v = $(this).bootstrapSelector('getValue');
    				var text = $(this).bootstrapSelector('getText');
    				if(v){
    					$('.selector-tags-rythm').tagsinput('add', { id: id, text: text,value:id + '&' + v});
    					hasChild = true;
    				}
    			});
    			if(!hasChild){
    				$('.selector-tags-rythm').tagsinput('add', { id: '', text: '全部',value:''});
    			}
    		}
    		this.options.onChange(value);
    	}
    };
   
    // BOOTSTRAP Combotree PLUGIN DEFINITION
    // =======================

    $.fn.bootstrapSelector = function (option, _relatedTarget) {
        var allowedMethods = [
                'getValue', 'setValue', 'destroy','getText','load','refresh','selectNone'
            ],
            value;
        var args = arguments;
        this.each(function () {
            var $this = $(this),
            	data = $this.data('bootstrap.selector'),
                options = $.extend({}, BootstrapSelector.DEFAULTS, $this.data(),
                    typeof option === 'object' && option);

            if (typeof option === 'string') {
                if ($.inArray(option, allowedMethods) < 0) {
                    throw "Unknown method: " + option;
                }

                if (!data) {
                    return;
                }
                
                if(option === 'options'){
                	value = data[option];
                }else{
                	var _option = args[0];
                     [].shift.apply(args);
                	value = data[_option].apply(data, args);
                }


                if (option === 'destroy') {
                    $this.removeData('bootstrap.selector');
                }
            }

            if (!data) {
                $this.data('bootstrap.selector', (data = new BootstrapSelector(this, options)));
            }
        });

        return typeof value === 'undefined' ? this : value;
    };

    $.fn.bootstrapSelector.Constructor = BootstrapSelector;
    $.fn.bootstrapSelector.defaults = BootstrapSelector.DEFAULTS;

    // BOOTSTRAP Combotree INIT
    // =======================

    $(function () {
        var resetMoreVisible = function(){
        	$('.selector-rythm').each(function(){
        		$(this).bootstrapSelector('refresh');
        	});
        };
        resetMoreVisible();
    	$(window).resize(function(){
    		resetMoreVisible();
    	});
    });

}(jQuery);
