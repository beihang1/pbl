@echo Compressing start
set "CURRENT_DIR=%~dp0"
@echo off
type^
 "%CURRENT_DIR%\third-res\bootstrap\css\bootstrap.css"^
 "%CURRENT_DIR%\third-res\bootstrap-dialog\css\bootstrap-dialog.css"^
 "%CURRENT_DIR%\third-res\bootstrap-datetimepicker\css\bootstrap-datetimepicker.css"^
 "%CURRENT_DIR%\third-res\bootstrap-switch\css\bootstrap-switch.css"^
 "%CURRENT_DIR%\third-res\bootstrap-jasny\css\bootstrap-jasny.css"^
 "%CURRENT_DIR%\third-res\bootflat\css\bootflat.css"^
 "%CURRENT_DIR%\third-res\bootstrap-validator\css\bootstrap-validator.css"^
 "%CURRENT_DIR%\third-res\font-awesome\css\font-awesome.css"^
 "%CURRENT_DIR%\third-res\bootstrap-jstree\css\bootstrap-jstree.css"^
 "%CURRENT_DIR%\third-res\bootstrap-table\css\bootstrap-table.css"^
 "%CURRENT_DIR%\third-res\bootstrap-duallistbox\css\bootstrap-duallistbox.css"^
 "%CURRENT_DIR%\third-res\bootstrap-select\css\bootstrap-select.css"^
 "%CURRENT_DIR%\third-res\bootstrap-fileinput\css\bootstrap-fileinput.css"^
 "%CURRENT_DIR%\third-res\bootstrap-flexslider\css\flexslider.css"^
 "%CURRENT_DIR%\third-res\bootstrap-tagsinput\bootstrap-tagsinput.css"^
 "%CURRENT_DIR%\css\base.css"^
 "%CURRENT_DIR%\css\components.css"^
 "%CURRENT_DIR%\css\icon.css"^
 "%CURRENT_DIR%\css\bs-wizard.css"^
 "%CURRENT_DIR%\css\arrowed.css"^
 "%CURRENT_DIR%\css\bootstrap-selector.css"^
 "%CURRENT_DIR%\third-res\full-calender\css\fullcalendar.css"^
 "%CURRENT_DIR%\third-res\bootstrap-colorpicker\css\bootstrap-colorpicker.css"^
 "%CURRENT_DIR%\third-res\bootstrap-iconpicker\css\bootstrap-iconpicker.css"^
 "%CURRENT_DIR%\third-res\jquery-gritter\css\jquery.gritter.css"^
 "%CURRENT_DIR%\css\animate.css"^
 "%CURRENT_DIR%\third-res\pace\css\pace-theme-minimal.css"^
 "%CURRENT_DIR%\third-res\jcrop\css\jquery.Jcrop.css"^
 | cleancss -o "%CURRENT_DIR%\css\cloud.min.css" --s0
@echo Compressing completed
@pause