//早期IE无此方法
if (!Object.keys) {
  Object.keys = (function () {
    var hasOwnProperty = Object.prototype.hasOwnProperty,
        hasDontEnumBug = !({toString: null}).propertyIsEnumerable('toString'),
        dontEnums = [
          'toString',
          'toLocaleString',
          'valueOf',
          'hasOwnProperty',
          'isPrototypeOf',
          'propertyIsEnumerable',
          'constructor'
        ],
        dontEnumsLength = dontEnums.length;
    return function (obj) {
      if (typeof obj !== 'object' && typeof obj !== 'function' || obj === null) throw new TypeError('Object.keys called on non-object');
      var result = [];
      for (var prop in obj) {
        if (hasOwnProperty.call(obj, prop)) result.push(prop);
      }
      if (hasDontEnumBug) {
        for (var i=0; i < dontEnumsLength; i++) {
          if (hasOwnProperty.call(obj, dontEnums[i])) result.push(dontEnums[i]);
        }
      }
      return result;
    }
  })()
};

function getAnimateCss(acss){
	return ENABLE_DIALOG_ANIMATE?acss:"";
}

function warning(msg,callbackOrUrl){
	BootstrapDialog.alert({
	    title: i18n_Common_warning,
	    type:"type-confirm",
	    cssClass:'dm-modal-sm confirm-rythm',
	    animateCss:getAnimateCss("shake"),
	    buttonLabel:"确定",
	    buttonCssClass:"btn-confirm",
	    size:BootstrapDialog.SIZE_NORMAL,
	    closeByBackdrop: false,
        closeByKeyboard: false,
	    message: msg,
	    callback:function(){
	    	callbackOrRedirect(callbackOrUrl);
	    }
	}); 
}

function callbackOrRedirect(callbackOrUrl){
	if(callbackOrUrl){
		if(typeof(callbackOrUrl) == "function"){
			(callbackOrUrl)();
		}else{
			redirect(callbackOrUrl);  
		}
	}
}

function success(msg,callbackOrUrl){
	BootstrapDialog.alert({
	    title: i18n_Common_success,
	    type:BootstrapDialog.TYPE_SUCCESS,
	    cssClass:'dm-modal-sm success-rythm',
	    animateCss:getAnimateCss("swing"),
	    buttonLabel:"确定",
	    buttonCssClass:"btn-success",
	    closeByBackdrop: false,
        closeByKeyboard: false,
	    size:BootstrapDialog.SIZE_NORMAL,
	    message: msg,
	    callback:function(){
	    	callbackOrRedirect(callbackOrUrl);
	    }
	}); 
}

function rowLink(link, rowIndex, callback){
	var table = $(link).parentsUntil('.fixed-table-body','table');
	var row = table.bootstrapTable('getData')[rowIndex];
	callback(row, rowIndex, table);
}

function buildQueryParam(form, params){
	return $.extend(params,buildFormData(form));
}

function buildQueryCondition(formid, tableid){
	var data = buildFormData('#' + formid);
	var btop = $('#' + tableid).bootstrapTable("getOptions");
	return $.extend(data,{
		pageNumber:btop.pageNumber,
		pageSize:btop.pageSize,
		sortName:btop.sortName,
		sortOrder:btop.sortOrder
	});
}

function createQueryParams(params){
	return buildQueryParam('#search-condition', params);
}

function info(msg,callbackOrUrl){
	BootstrapDialog.alert({
	    title: i18n_Common_prompt,
	    type:BootstrapDialog.TYPE_INFO,
	    size:BootstrapDialog.SIZE_NORMAL,
	    closeByBackdrop: false,
        closeByKeyboard: false,
	    buttonCssClass:"btn-primary",
	    cssClass:'dm-modal-sm info-rythm',
	    animateCss:getAnimateCss("bounce"),
	    buttonLabel:"确定",
	    message: msg,
	    callback:function(){
	    	callbackOrRedirect(callbackOrUrl);
	    }
	}); 
}

function error(msg,callbackOrUrl){
	BootstrapDialog.alert({
	    title: i18n_Common_error,
	    type:BootstrapDialog.TYPE_DANGER,
	    closeByBackdrop: false,
        closeByKeyboard: false,
	    cssClass:'dm-modal-sm danger-rythm',
	    animateCss:getAnimateCss("tada"),
	    buttonLabel:"确定",
	    buttonCssClass:"btn-danger",
	    size:BootstrapDialog.SIZE_NORMAL,
	    message: msg,
	    callback:function(){
	    	callbackOrRedirect(callbackOrUrl);
	    }
	}); 
}

function confirm(message, callbackOrUrl){
	 new BootstrapDialog({
        title: i18n_Common_confirm,
        message: message,
        type:'type-warning',
        closable: true,
        closeByBackdrop: false,
        closeByKeyboard: false,
        animateCss:getAnimateCss("bounceIn"),
        cssClass:'dm-modal-sm warning-rythm',
        data: {
            'callback': function(){
    	    	callbackOrRedirect(callbackOrUrl);
    	    }
        },
        buttons: [{
                label: i18n_Common_confirm,
                cssClass: 'btn-warning gh gh-ok',
                action: function(dialog) {
                    typeof dialog.getData('callback') === 'function' && dialog.getData('callback')(true);
                    dialog.close();
                }
	            },{
	                label: i18n_Common_cancel,
	                cssClass: 'btn-warning fa fa-reply',
	                action: function(dialog) {
	                    dialog.close();
	                }
	            }]
    }).open();
}
/**
 * 根据ID关闭指定Dialog
 * @param id Dialog Id
 */
function closePopup(id){
	var dialog = BootstrapDialog.dialogs[id];
	if(dialog){
		dialog.close();
	}
}

function closeSelfDialog(){
	parent.closePopup(POP_ID);
}

function closeSelfDialogAndCallback(callback){
	parent.closePopup(POP_ID);
	callback();
}

/**
 * 以弹出的方式打开远程页面
 * @param url 页面
 * @param conf 自定义参数
 */
function popupPage(url, conf){
	var queryStr = "?";
	if(url.indexOf("?") != -1){
		queryStr = "&";
	}
	var maxHeight = Math.max((conf ? conf.height || conf.maxHeight ||  500 : 500),500);
	var minHeight = conf ? conf.minHeight || 0 : 0;
	var config = {
		size:BootstrapDialog.SIZE_WIDE,
		closeByBackdrop: false,
        closeByKeyboard: false,
        onshown:function(dialog){
        	var popId = dialog.options.id;
        	var loading = document.getElementById('load-' + popId); 
        	var frame = document.getElementById('frame-' + popId);
        	if(loading){
        		loading.style.display = "";
        	}
        	frame.src = url + queryStr + 'POP_ID=' + popId;
        	$(frame).iframeAutoHeight({
        		maxHeight:maxHeight,
        		minHeight:conf.minHeight,
        		callbefore:function(){
	        		var load = document.getElementById('load-' + popId); 
	            	var iframe = document.getElementById('frame-' + popId);
	            	if(loading){
		        		load.innerHTML = "加载完成!"; 
		        		load.style.display = "none"; 
	            	}
	            	iframe.style.display = "";
	        	},
	        	callback:function(newHeight){
	        		var btnbox = $(this, window.top.document).contents().find('.btnbox-rythm');
	        		if(btnbox.length){
	        			$(this, window.top.document).contents().find('.content-rythm').height(newHeight.newFrameHeight - OFFSET_HEIGHT);
	        		}
	        	}
        	});
        },
        onhide:function(){
        	$("#frame-" + this.id).attr("src", "");
        },
		message:function(dialog) {
			var popId = dialog.options.id;
			if(config.ajax){
				return $.ajax({
					data:{POP_ID:popId},
					dataType:"html",
					url:url,
					async:false
				}).responseText;
			}else{
				return '<div id="load-' + popId +'" align="center" class="dialog-loading fade in"><span class="loading-circle-small"></span>'+i18n_Common_loading+'</div>' + 
				'<iframe id="frame-' + popId + '" name="frame-' + popId + '" style="display:none;width:100%;border:none"></iframe>';
			}
		}
	};
	if(conf){
		$.extend(config,conf);
	}
	config.cssClass = getOrElse(config.cssClass) + " dialog-for-page";
	config.animateCss = "bounceInDown";
	BootstrapDialog.show(config);
}

String.prototype.replaceAll = function(s1,s2){
   return this.replace(new RegExp(s1,"gm"),s2);
}

String.prototype.times=function(findStr){
	var reg = new RegExp(findStr, "g");
	var matches = this.match(reg);
	return matches?matches.length:0;
}

String.prototype.format = function() {
    var args = arguments,
        flag = true,
        i = 0;
    if(this.times("%s") == args.length ){
    	var str = this.replace(/%s/g, function () {
    		var arg = args[i++];
    		
    		if (typeof arg === 'undefined') {
    			flag = false;
    			return '';
    		}
    		return arg;
    	});
    	if (flag) {
    		return str;
    	}
    }else if(this.times("\\{\\d\\}") == args.length ){
    	var str = this;
    	for(var i=0;i<args.length;i++){
    		str = str.replaceAll("\\{"+i+"\\}",args[i]);
    	}
    	return str;
    }
    return this;
}
//对多余的字符进行隐藏
function hideStringByMax(oldString, maxLength) {
	var result = oldString;
	if(oldString!=null && oldString.length > maxLength){
		result = oldString.substring(0,maxLength);
		result += "...";
	}
	return result;
}

Array.prototype.insert = function (index, item) { 
	this.splice(index, 0, item); 
};

function buildHistQueryString(moduleId, params){
	return $.base64.encode(moduleId + "=" + $.base64.encode($.param(params)));
}
function resetLabelsWidth(){
	$('.label-rythm').each(function(){
		var group = $(this).parentsUntil('.group-rythm');
		var groupWidth = group.offsetParent().width();
		var labelWidth = group.find('.input-group-label').width();
		var requiredWidth = 20;
		$(this).width(groupWidth - labelWidth - requiredWidth);
	});
}

function research(){
	$('.table-rythm').each(function(){
		$(this).bootstrapTable('research');
	});
}

function refreshTable(){
	$('#search-result').bootstrapTable('refresh');
}

function clearSearch(btn){
	var form = $(btn).parentsUntil('#pageBody', 'form');
	//输入控件
	form.find('.input-rythm').each(function(){$(this).val('');});
	//日期控件
	form.find('.datetime-rythm,.datetime-input-rythm').each(function(){$(this).datetimepicker('val','');});
	//下拉控件
	form.find('.combobox-rythm').each(function(){
		$(this).selectpicker('val','');
	});
	//下拉树控件
	form.find('.combotree-rythm').each(function(){
		$(this).bootstrapCombotree('setValue','');
	});
	//单选、多选
	form.find('.icheck-input').attr('checked',false).change();
	//列表
	var tbo = $('.table-rythm').bootstrapTable('getOptions');
	if(tbo){
		tbo.totalRows = 0;
		tbo.pageNumber = 1;
	}
	$('.table-rythm').bootstrapTable('load',{record:0, data:[]});
}

function clearForm(form, excludeDisabled){
	var form = $(form);
	//输入控件
	form.find('.input-rythm').each(function(){
		if(!(excludeDisabled && isDisabled(this))){
			$(this).val('');
		}
	});
	//textarea
	form.find('.textarea-rythm').each(function(){
		if(!(excludeDisabled && isDisabled(this))){
			$(this).val('');
		}
	});
	//icheck
	form.find('.check-rythm input').each(function(){
		if(!(excludeDisabled && isDisabled(this))){
			$(this).icheck('unchecked');
		}
	});
	//日期控件
	form.find('.datetime-rythm,.datetime-input-rythm').each(function(){
		if(!(excludeDisabled && isDisabled(this))){
			$(this).datetimepicker('val','');
		}
	});
	//下拉控件
	form.find('.combobox-rythm').each(function(){
		if(!(excludeDisabled && isDisabled(this))){
			$(this).selectpicker('val','');
		}
	});
	//下拉树控件
	form.find('.combotree-rythm').each(function(){
		if(!(excludeDisabled && isDisabled(this))){
			$(this).bootstrapCombotree('setValue','');
		}
	});
	//switch
	form.find('.switch-rythm').each(function(){
		if(!(excludeDisabled && isDisabled(this))){
			$(this).bootstrapSwitch('setState',false);
		}
	});
	//文件
	form.find('.file-rythm').each(function(){
		if(!(excludeDisabled && isDisabled(this))){
			$(this).fileinput('clear');
		}
	});
	//数字增减器
	form.find('.spinner-rythm').each(function(){
		if(!(excludeDisabled && isDisabled(this))){
			$(this).val('');
		}
	});
	//颜色选择器
	form.find('.color-rythm').each(function(){
		if(!(excludeDisabled && isDisabled(this))){
			setValue($(this), '');
		}
	});
	//ICON选择器
	form.find('.iconpicker-rythm').each(function(){
		if(!(excludeDisabled && isDisabled(this))){
			setValue($(this), '');
		}
	});
}

function isDisabled(element){
	var el = $(element);
	//输入控件
	if(el.hasClass('input-rythm') || el.hasClass('textarea-rythm')){
		return el.prop('readonly');
	}else if(el.hasClass('spinner-rythm')){
		//数字增减器 min, max, fixlength, fixchar, fixleft
		return el.next().find('button').first().prop('disabled');
	}
	//icheck
	if(el.hasClass('check-rythm')){
		var inputs = el.find('input');
		if(inputs && inputs.length){
			for(var i = 0; i<inputs.length;i++){
				if(!$(inputs[i]).prop('disabled')){
					return false;
				}
			}
			return true;
		}
	}
	else if(el.hasClass('icheck')){
		var disabled = true;
		el.each(function(){
			if(!$(this).prop('disabled')){
				disabled = false;
				return false;
			}
		});
		return disabled;
	}
	//日期控件
	if(el.hasClass('datetime-rythm') || el.hasClass('datetime-input-rythm')){
		return el.datetimepicker('isDisabled');
	}
	//下拉控件
	if(el.hasClass('combobox-rythm')){
		return el.selectpicker('isDisabled');
	}
	//下拉树控件
	if(el.hasClass('combotree-rythm')){
		return el.bootstrapCombotree('isDisabled');
	}
	//switch
	if(el.hasClass('switch-rythm')){
		return el.bootstrapSwitch('isDisabled') || el.bootstrapSwitch('isReadOnly');
	}
	//文件
	if(el.hasClass('file-rythm')){
		return el.fileinput('isDisable');
	}
	//颜色选择器
	if(el.hasClass('color-rythm')){
		return el.prop('readonly');
	}
	//ICON选择器
	if(el.hasClass('iconpicker-rythm')){
		return el.prop('readonly');
	}
	return $(element).prop('disabled');
}

function setValue(element, v){
	var value = getOrElse(v);
	var el = $(element);
	//输入控件
	if(el.hasClass('input-rythm') || el.hasClass('textarea-rythm')){
		el.val(pad(value,el.attr('fixlength'),el.attr('fixchar'),el.attr('fixleft')));
		el.change();
	}
	//icheck
	else if(el.hasClass('check-rythm')){
		el.find('input').each(function(){
			$(this).icheck('unchecked');
		});
		var values = (typeof(v) == 'string') ?value.split(',') : v;
		if(values && values.length){
			for(var i = 0; i<values.length;i++){
				el.find('input[value=' + values[i]+ ']').icheck('checked');
			}
		}
	}else if(el.hasClass('tagsinput')){
		el.val(value);
		el.importTags(value);
	}
	else if(el.hasClass('icheck')){
		var values = ',' + ((typeof(v) == 'string') ?v : v.join(',')) + ',';
		el.each(function(){
			var checked = values.indexOf(',' + $(this).attr('value') + ',') > -1;
			if(checked){
				$(this).icheck('checked');
			}else{	
				$(this).icheck('unchecked');
			}
		});
	}
	//日期控件
	else if(el.hasClass('datetime-rythm') || el.hasClass('datetime-input-rythm')){
		if(typeof(v) == 'string' && !toNumber(v)){
			el.datetimepicker('val', v);
		}else if(typeof(v) == 'string' && toNumber(v)){
			el.datetimepicker('setDate', new Date(toNumber(v)));
		}else if(typeof(v) == 'object'){
			if(v == null) {
				el.datetimepicker('val', "");
			} else {
				el.datetimepicker('setDate', v);
			}
		}else if(typeof(v) == 'undefined'){
			
		}else if(typeof(v) == 'number'){
			el.datetimepicker('setDate', new Date(v));
		}else{
			el.datetimepicker('setDate', new Date(v));
		}
	}
	//下拉控件
	else if(el.hasClass('combobox-rythm')){
		el.selectpicker('val', value);
	}
	//下拉树控件
	else if(el.hasClass('combotree-rythm')){
		el.bootstrapCombotree('setValue', value);
	}else if(el.hasClass('selector-rythm')){
		//选择器
		el.bootstrapSelector('setValue', value);
	}else if(el.hasClass('spinner-rythm')){
		//数字增减器 min, max, fixlength, fixchar, fixleft
		el.val(getSafeValue(value, el.attr('min'),el.attr('max'),el.attr('fixlength'),el.attr('fixchar'),el.attr('fixleft')));
	}else if(el.hasClass('time-rythm')){
		//时间控件
		var hour = "",minute="";
		if(value){
			var values = value.split(":");
			if(values.length > 0){
				hour = values[0];
			}
			if(values.length > 1){
				minute = values[1];
			}
		}
		setValue(el.find('.hour-rythm'), hour);
		setValue(el.find('.minute-rythm'), minute);
	}else if(el.hasClass('edui-default')){
		UE.getEditor(el.attr('id')).setContent(value);
	}
	else if(el.hasClass('color-rythm')){
		//颜色控件
		el.val(value);
		$(el.parent()).colorpicker('setValue', value);
	}else if(el.hasClass('iconpicker-rythm')){
		//ICON选择器
		return el.BootstrapIconPicker('setValue', value);
	}
	//switch
	else if(el.hasClass('switch-rythm')){
		el.bootstrapSwitch('setState',v);
	}else if(el.length &&( el[0].tagName.toUpperCase() == 'SPAN' || el[0].tagName.toUpperCase() == 'DIV')){
		el.html(value);
	}else{
		el.val(value);
		el.change();
	}
}

function disable(element, disabled){
	var el = $(element);
	var input = el;
	if(typeof(disabled) == "undefined"){
		disabled = true;
	}
	//输入控件
	if(el.hasClass('input-rythm') || el.hasClass('textarea-rythm')){
		el.prop('readonly', disabled);
	}
	//icheck
	else if(el.hasClass('check-rythm') || el.hasClass('icheck')){
		el.icheck(disabled ? 'disabled':'enabled');
	}
	//日期控件
	else if(el.hasClass('datetime-rythm') || el.hasClass('datetime-input-rythm')){
		el.datetimepicker('setDisabled', disabled);
		input = el.datetimepicker('getInput');
	}
	//下拉控件
	else if(el.hasClass('combobox-rythm')){
		el.selectpicker('disabled', disabled);
	}
	//下拉树控件
	else if(el.hasClass('combotree-rythm')){
		el.bootstrapCombotree('setDisabled', disabled);
	}
	//switch
	else if(el.hasClass('switch-rythm')){
		el.bootstrapSwitch('setDisabled',disabled);
	}else if(el.hasClass('spinner-rythm')){
		//数字增减器 
		el.prop('readonly', disabled);
		el.parent().find('.btn').prop('disabled',disabled);
	}
	//时间控件
	else if(el.hasClass('time-rythm')){
		disable(el.find('.hour-rythm'), disabled);
		disable(el.find('.minute-rythm'), disabled);
		disabledFieldValidators(el.find('.hour-rythm'), disabled);
		input = el.find('.minute-rythm');
	}else if(el.hasClass('edui-default')){
		//富文本编辑框
		if(disabled){
			UE.getEditor(el.attr('id')).disable();
		}else{
			UE.getEditor(el.attr('id')).enable();
		}
	}
	//颜色选择器
	else if(el.hasClass('color-rythm')){
		if(disabled){
			el.parent().colorpicker('disable');
		}else{
			el.parent().colorpicker('enable');
		}
	}else if(el.hasClass('iconpicker-rythm')){
		//ICON选择器
		if(disabled){
			el.BootstrapIconPicker('disable');
		}else{
			el.BootstrapIconPicker('enable');
		}
	}
	//文件
	else if(el.hasClass('file-rythm')){
		el.fileinput(disabled?'disable':'enable');
	}else{
		el.attr('disabled', disabled);
	}
	
	disabledFieldValidators(input, disabled);
}

function getValue(element){
	var el = $(element);
	//输入控件
	if(el.hasClass('input-rythm') || el.hasClass('textarea-rythm')){
		return el.val();
	}
	//多文件控件
	if(el.hasClass('files-rythm')){
		return getFileIds(el.attr('name'));
	}
	//icheck
	if(el.hasClass('check-rythm')){
		var ret = [];
		el.find('input:checked').each(function(){
			ret.push($(this).val());
		});
		return ret.join(',');
	}else if(el.hasClass('icheck')){
		var ret = [];
		el.each(function(){
			if($(this).is(':checked')){
				ret.push($(this).val());
			}
		});
		return ret.join(',');
	}
	//日期控件
	else if(el.hasClass('datetime-rythm') || el.hasClass('datetime-input-rythm')){
		return el.datetimepicker('val');
	}
	//下拉控件
	else if(el.hasClass('combobox-rythm')){
		return el.selectpicker('val');
	}else if(el.hasClass('time-rythm')){
		//时间控件
		var hour = getValue(el.find('.hour-rythm'));
		var minute = getValue(el.find('.minute-rythm'));
		if(hour && minute){
			return hour + ":" + minute;
		}
		if(hour && !minute){
			return hour + ":00";
		}
		if(!hour && minute){
			return "00:" + minute;
		}
		return "";
	}
	//下拉树控件
	else if(el.hasClass('combotree-rythm')){
		return el.bootstrapCombotree('getValue');
	}else if(el.hasClass('selector-rythm')){
		//选择器
		return el.bootstrapSelector('getValue');
	}else if(el.hasClass('edui-default')){
		//富文本编辑框
		return UE.getEditor(el.attr('id')).getContent();
	}
	//switch
	else if(el.hasClass('switch-rythm')){
		return el.is(':checked');
	}else if(el.length &&( el[0].tagName.toUpperCase() == 'SPAN' || el[0].tagName.toUpperCase() == 'DIV')){
		return el.html();
	}else {
		return el.val();
	}
}

function getText(element){
	var el = $(element);
	//输入控件
	if(el.hasClass('input-rythm') || el.hasClass('textarea-rythm')){
		return el.val();
	}
	//icheck
	if(el.hasClass('check-rythm')){
		//icheck
		var ret = [];
		el.find('input:checked').each(function(){
			ret.push($(this).parentsUntil('.check-body','label').text());
		});
		return ret.join(',');
	}else if(el.hasClass('icheck')){
		var ret = [];
		el.each(function(){
			if($(this).is(':checked')){
				ret.push($(this).parentsUntil('.check-body','label').text());
			}
		});
		return ret.join(',');
	}
	//日期控件
	else if(el.hasClass('datetime-rythm') || el.hasClass('datetime-input-rythm')){
		return el.datetimepicker('val');
	}else if(el.hasClass('time-rythm')){
		return getValue(el);
	}//多文件控件
	else if(el.hasClass('files-rythm')){
		return getFileNames(el.attr('name'));
	}
	//下拉控件
	else if(el.hasClass('combobox-rythm')){
		return el.selectpicker('text');
	}
	//下拉树控件
	else if(el.hasClass('combotree-rythm')){
		return el.bootstrapCombotree('getText');
	}else if(el.hasClass('selector-rythm')){
		//选择器
		return el.bootstrapSelector('getText');
	}else if(el.hasClass('edui-default')){
		//富文本编辑框
		return UE.getEditor(el.attr('id')).getContent();
	}
	//switch
	else if(el.hasClass('switch-rythm')){
		return el.is(':checked');
	}
	//label
	else if(el.hasClass('label-rythm')){
		//去除回车
		return el.html().replace(/(^\s+)|(\s+$)/g, "");
	}
	else if(el.length &&( el[0].tagName.toUpperCase() == 'SPAN' || el[0].tagName.toUpperCase() == 'DIV')){
		return el.html();
	}else {
		return el.val();
	}
}

//form为jquery对象
function setFormValue(formData, form){
	if(formData && typeof(formData) == 'object'){
		for(var p in formData){ 
			if (typeof(formData[p]) != "function"){
				var value = formData[p];
				
				if(form){
					//在form下查找
					if($(form).find('[name='+p + ']').length>0){
						//1.优先根据name进行查找
						setValue($(form).find('[name='+p + ']'), value);
					}else if($(form).find('#'+p).length>0){
						//2.其次根据id进行查找
						setValue($(form).find('#'+p), value);
					}else if($(form).find('#div_' + p).length>0){
						//3.对特殊控件进行处理（日期类）
						setValue($(form).find('#div_' + p), value);
					}
				}else{
					//全document下查找
					if($('[name='+p + ']').length>0){
						//1.优先根据name进行查找
						setValue($('[name='+p + ']'), value);
					}else if($('#'+p).length>0){
						//2.其次根据id进行查找
						setValue($('#'+p), value);
					}else if($('#div_' + p).length>0){
						//3.对特殊控件进行处理（日期类）
						setValue($('#div_' + p), value);
					}
				}
				
				/*算法调整为以上
				if(domExsit(p)&&form && $(form).find('#'+p).length>0){
					if(){
						setValue($(form).find('#'+p), value);
					}else{
						setValue($('#'+p), value);
					}
				}else if(domExsit('div_' + p)){
					if(form && $(form).find('#div_' + p).length>0){
						setValue($(form).find('#div_' + p), value);
					}else{
						setValue($('#div_' + p), value);
					}
				}else{
					if(form && $(form).find('[name='+p + ']').length>0){
						setValue($(form).find('[name='+p + ']'), value);
					}else{
						setValue($('[name='+p + ']'), value);
					}
				}*/
			}
		}
	}
}

function setValueAny(idname,value){
	if(domExsit(idname)){
		setValue($('#'+idname), value);
	}else if(domExsit('div_' + idname)){
		var el = $('#div_'+idname);
		if(el.hasClass('check-rythm')){
			setValue(el, value);
		}
	}else if($('[name='+idname + ']').length>0){
		setValue($('[name='+idname + ']'), value);
	}else{
		setValue($(idname), value);
	}
}

function getValueAny(idname){
	var value = '';
	if(domExsit(idname)){
		value = getValue($('#'+idname));
	}else if(domExsit('div_' + idname)){
		var el = $('#div_'+idname);
		if(el.hasClass('check-rythm')){
			value = getValue(el);
		}
	}else if($('[name='+idname + ']').length>0){
		value = getValue($('[name='+idname + ']'));
	}else{
		value = getValue($(idname));
	}
	return value;
}

function getTextAny(idname){
	var text = '';
	if(domExsit(idname)){
		text = getText($('#'+idname));
	}else if(domExsit('div_' + idname)){
		var el = $('#div_'+idname);
		if(el.hasClass('check-rythm')){
			text = getText(el);
		}
	}else if($('[name='+idname + ']').length>0){
		text = getText($('[name='+idname + ']'));
	}else{
		text = getText($(idname));
	}
	return text;
}

function disableAny(idname, disabled){
	if(domExsit(idname)){
		disable($('#'+idname), disabled);
	}else if(domExsit('div_' + idname)){
		var el = $('#div_'+idname);
		if(el.hasClass('check-rythm')){
			disable(el, disabled);
		}
	}else if($('[name='+idname + ']').length>0){
		disable($('[name='+idname + ']'), disabled);
	}else{
		disable($(idname), disabled);
	}
}

function isDisabledAny(idname){
	var ret = false;
	if(domExsit(idname)){
		ret = isDisabled($('#'+idname));
	}else if(domExsit('div_' + idname)){
		var el = $('#div_'+idname);
		if(el.hasClass('check-rythm')){
			ret = isDisabled(el);
		}
	}else if($('[name='+idname + ']').length>0){
		ret = isDisabled($('[name='+idname + ']'));
	}else{
		isDisabled($(idname));
	}
	return ret;
}

function buildFormDataAll(form) {
	var data = buildFormData(form);
	//label
	$(form).find('span.label-rythm').each(function(){
		if($(this).attr('name')){
			data[$(this).attr('name')] = getText($(this));
		}
	});
	return data;
}
function buildFormData(form){
	var data = $(form).serializeObject();
	//switch
	$(form).find('.switch-rythm').each(function(){
		data[$(this).attr('name')] = $(this).is(':checked');
	});
	//下拉控件
	$(form).find('select.combobox-rythm, .combotree-rythm').each(function(){
		if($(this).attr('name')){
			data[$(this).attr('name')] = getValue($(this));
		}
	});
	//多文件控件
	$(form).find('.files-rythm').each(function(){
		if($(this).attr('name')){
			data[$(this).attr('name')] = getValue($(this));
			data[$(this).attr('name')+"_fileNames"] = getText($(this));
		}
	});
	//富文本
	$(form).find('.ueditor-rythm').each(function(){
		if($(this).attr('name')){
			data[$(this).attr('name')] = getValue($(this));
		}else if($(this).attr('id')){
			data[$(this).attr('id')] = getValue($(this));
		}
	});
	//选择器
	$(form).find('.selector-rythm').each(function(){
		if($(this).attr('name')){
			data[$(this).attr('name')] = getValue($(this));
		}
	});
	//time
	$(form).find('.time-rythm').each(function(){
		if($(this).attr('name')){
			data[$(this).attr('name')] = getValue($(this));
		}
	});
	if(data.hasOwnProperty('btSelectAll')){
		delete data.btSelectAll;
	}
	if(data.hasOwnProperty('btSelectItem')){
		delete data.btSelectItem;
	}
	return data;
}

function disabledForm(form, disabled){
	var form = $(form);
	//输入控件
	form.find('.input-rythm').each(function(){
		disable($(this), disabled);
	});
	//富文本
	$(form).find('.ueditor-rythm').each(function(){
		disable($(this), disabled);
	});
	//textarea
	form.find('.textarea-rythm').each(function(){
		disable($(this), disabled);
	});
	//icheck
	form.find('.check-rythm input').each(function(){
		disable($(this), disabled);
	});
	//日期控件
	form.find('.datetime-rythm,.datetime-input-rythm').each(function(){
		disable($(this), disabled);
	});
	//下拉控件
	form.find('select.combobox-rythm').each(function(){
		disable($(this), disabled);
	});
	//下拉树控件
	form.find('.combotree-rythm').each(function(){
		disable($(this), disabled);
	});
	//switch
	form.find('.switch-rythm').each(function(){
		disable($(this), disabled);
	});
	//文件
	form.find('.file-rythm').each(function(){
		disable($(this), disabled);
	});
	//多文件
	form.find('.files-rythm').each(function(){
		disable($(this), disabled);
	});
	//数字增减器
	form.find('.spinner-rythm').each(function(){
		disable($(this), disabled);
	});
	//颜色选择器
	form.find('.color-rythm').each(function(){
		disable($(this), disabled);
	});
	//ICON选择器
	form.find('.iconpicker-rythm').each(function(){
		disable($(this), disabled);
	});
}

var _Global_Ajax_Dialog;

var _Global_Clear_Msg_On_Click = true;
$(function(){
	//$('.popover-rythm,.link-rythm,.label-rythm,.input-select-rythm .input-group-addon,.iconlink-rythm').not(".dropdown-toggle,.selectpicker,.not-show-pop").popover({ trigger: "hover",container:"body" });
	
	$('.btn-back').click(function(){history.back()});
	
	$(document).keypress(function(e) {
		if(e.which === 13) {
			// enter has been pressed, execute a click on .js-new:
			$(".enter-button").first().click();
			return false;
		}
	});
//	$(document).click(function(e) {
//		//清空消息
//		if(_Global_Clear_Msg_On_Click){
//			clearBodyMessage();
//		}
//	});
	//文本固定宽度的处理
	resetLabelsWidth();
	$(window).resize(function(){
		resetLabelsWidth();
	});
	$.fn.serializeObject = function() {
		var o = {};
		var a = this.serializeArray();
		$.each(a, function() {
			if (o[this.name]) {
				if (!o[this.name].push) {
					o[this.name] = [ o[this.name] ];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	}
	
	//创建alert
	$.fn.alerts = function(cls, msg, id){
		//this.empty();
		var id = id ? 'msg_' + id:'msg_' +Math.uuid();
		if(id){
			clearAlert(id);
		}
		this.append('<div id="' + id + '" class="alert fade in" style="display:table;"><div class="alert-' + cls + ' alert-icon"></div><div class="alert-' + cls + ' alert-content"><button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button><span>' + msg + '</span></div></div>');
		$('#' + id).alert();
		return $('#' + id);
	} 
	//validator 默认属性
	/*$.fn.bootstrapValidator.DEFAULT_OPTIONS = $.extend($.fn.bootstrapValidator.DEFAULT_OPTIONS,{
		message: i18n_Common_value_invalid,
		container: 'popover',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		}
	});*/
	_Global_Ajax_Dialog = new BootstrapDialog({
		title:'',
		autodestroy:false,
		cssClass:'ajax-waiting-dialog',
        message: '<div align="center" class="dialog-loading fade in"><span class="loading-circle-small"></span>&nbsp;&nbsp;'+i18n_Common_request_loading_wait+'</div>',
        closable: false});
	//ajax 默认属性
	$.ajaxSetup({
	  cache:false,
	  global: false,
	  error: function(req, status, e){
		  if(req.status == 408){
			  // 登录超时
			  var loginUrl = CONTEXT_ROOT + "login.action?error=error.session.timeout";
			  if(req.responseText){
				  try{
					  var rs = $.parseJSON(req.responseText);
					  if(rs.data && rs.data.loginUrl){
						  loginUrl = rs.data.loginUrl;
					  }
				  }catch(e){}
			  }
			  window.location.href = loginUrl;
		  } else if(req.status == 403){
			  // 无访问权限
			  warning(i18n_Common_res_not_access);
		  } else {
	    	var msg = "无法访问网络";
			parent.bodyError(msg);
		  }
	  }
	});
	//文件上传默认配置
	/*$.fn.fileinput.defaults = $.extend($.fn.fileinput.defaults, {
	        showPreview: false,
	        showUpload: false,
	        showDownloadReset:true,
	        browseLabel: i18n_Common_viewing,
	        removeLabel: i18n_Common_delete,
	        downloadLabel: i18n_Common_dowload,
	        resetLabel: i18n_Common_reset,
	        uploadLabel: i18n_Common_upload,
	        browseClass:'btn btn-default file-rythm-browser',
	        downloadClass:'btn btn-default file-rythm-download',
	        resetClass:'btn btn-default file-rythm-reset',
	        uploadClass:'btn btn-default file-rythm-upload',
	        removeClass:'btn btn-default file-rythm-remove',
	        maxFileSize: 100*1024,//单位KB
	        maxFileCount: 1,
	        popplace:'top',
	        msgFoldersNotAllowed:i18n_Common_only_select_file,
	        msgRequired:i18n_Common_must_select_file,
	        msgSizeTooLarge: i18n_Common_size_error,
	        msgFilesTooMany: i18n_Common_file_count_error,
	        msgFileNotFound: i18n_Common_file_not_finde,
	        msgFileNotReadable: i18n_Common_file_not_read,
	        msgFilePreviewAborted: i18n_Common_file_not_preview,
	        msgFilePreviewError: i18n_Common_file_read_error,
	        msgInvalidFileType: i18n_Common_file_type_error,
	        msgInvalidFileExtension: i18n_Common_file_ext_name_error,
	        msgValidationError: '<span class="text-danger"><i class="glyphicon glyphicon-exclamation-sign"></i><i class="form-control-feedback bv-no-label bv-icon-input-group glyphicon glyphicon-remove"></i> '+ i18n_Common_file_invalid +'</span>',
	        msgRequiredError: '<span class="text-danger"><i class="glyphicon glyphicon-exclamation-sign"></i><i class="form-control-feedback bv-no-label bv-icon-input-group glyphicon glyphicon-remove"></i> {errorMsg}</span>',
	        msgLoading: i18n_Common_file_reload,
	        msgProgress: i18n_Common_file_reload_count,
	        msgSelected: i18n_Common_file_selected_count
	    });*/
});

// iCheck
window.icheck = {
		autoInit: false,
		autoAjax: true,
		tap: true,
		checkboxClass: 'icheckbox_square-red',
		radioClass: 'iradio_square-red'
};

function buildAjaxOptions(options){
	options = $.extend({method:'post'},options);
	var success = options.success;
	var fail = options.fail;
    var mask = options.mask;
    var bodyMsg = options.bodyMsg;
    var showMsg = options.hasOwnProperty("showMsg") ? (true && options.showMsg) : true;
// 删除，使用$.ajaxSetup代替
//    options = $.extend({error:function(){
//	    var msg = "无法访问网络";
//	    var r = bodyMsg ? parent.bodyError(msg) : parent.error(msg);
//    }}, options);
    options.success = function(result, textStatus, jqXHR) {
		if(result ==  "accessdenied"){
			var msg = "无访问权限";
			var r = bodyMsg ? parent.bodyError(msg) : parent.error(msg);
		}else{
			if(success && (!result.hasOwnProperty('status') || result.status)){
				success(result, textStatus, jqXHR);
			}
			if(fail &&  result.hasOwnProperty('status') && !result.status) {
				fail(result, textStatus, jqXHR);
			}
			if(showMsg && result.messages){
				var sr = result.status ? (bodyMsg ? parent.bodySuccess(result.messages) : parent.success(result.messages)) : (bodyMsg ? parent.bodyError(result.messages) : parent.error(result.messages));
			}
		}
    };
    if(mask){
    	options = $.extend({
		 beforeSend:function(){
			  parent._Global_Ajax_Dialog.open();
			  var $modal = parent._Global_Ajax_Dialog.getModal();
			  var $backdrop = $modal.data('bs.modal').$backdrop;
              $modal.css('z-index', 9010);
              $backdrop.css('z-index', 9000);
		  },
		  complete:function(){
			  parent._Global_Ajax_Dialog.close();
		  }
		}, options);
    }
    return options;
}

function ajax(options) {
    return $.ajax(buildAjaxOptions(options));
}

function ajaxf(options){
	var opts = buildAjaxOptions($.extend({
		secureuri: false,
		global:false
	},options));
	$.ajaxFileUpload(opts);
}

function format_number(n, precision){
	if(!n && n != 0){
		return "";
	} else if($.isNumeric(n)){
		if($.isNumeric(precision) && precision >= 0){
			var p = Math.pow(10, precision);
			n = Math.round(n * p) / p;
		}
		var b = n.toString();
		var flag = "";
		if(b.indexOf("-") == 0){
			b = b.substring(1);
			flag = "-";
		}
		var dot = b.indexOf(".");
		var len = dot >= 0 ? dot : b.length;
		if(len<=3){return flag + b;}
		var r=len%3;
		var f = dot >= 0 ? b.substring(dot) : ""
		var ret = r>0?b.slice(0,r)+","+b.slice(r,len).match(/\d{3}/g).join(",")+f:b.slice(r,len).match(/\d{3}/g).join(",")+f;
		return flag + ret;
	} else {
		return n;
	}
}

function longAjax(option){
	var opt = $.extend({
	 beforeSend:function(){
		  parent._Global_Ajax_Dialog.open();
	  },
	  complete:function(){
		  parent._Global_Ajax_Dialog.close();
	  }
	}, option);
	return $.ajax(opt);
}

function dateTimeFormat(value){
	var result = "";
	if(value){
		if(typeof(value) == 'object'){
			result = $.format.date(value, JS_DATE_TIME_FORMAT);
		} else {
			result = $.format.date(new Date(Number(value)), JS_DATE_TIME_FORMAT);
		} 
	}
	return result;
}

function dateTimeFormatMi(value){
	var result = "";
	if(value){
		if(typeof(value) == 'object'){
			result = $.format.date(value, JS_DATE_MINUTE_FORMAT);
		} else {
			result = $.format.date(new Date(Number(value)), JS_DATE_MINUTE_FORMAT);
		}
	}
	return result;
}

function dateFormat(value){
	var result = "";
	if(value){
		if(typeof(value) == 'object'){
			result = $.format.date(value, JS_DATE_FORMAT);
		} else {
			result = $.format.date(new Date(Number(value)), JS_DATE_FORMAT);
		} 
	}
	return result;
}

function getOrElse(val, defval){
	if(val){
		return val;
	}
	if(!val && defval){
		return defval;
	}
	return "";
}

var alertTimeouts = {};
var TIME_OUT_FOR_HIDE_MSG = 8000;
function bodyMessage(cls, msg, id){
	if(document.getElementById('bodyMsg')){
		var alert = $('#bodyMsg').alerts(cls, msg, id);
		var alertId = alert.attr('id');
		alert.show().css('opacity','0.8');
		
		alertTimeouts[alertId] = setTimeout("clearAlert('" + alertId + "')",TIME_OUT_FOR_HIDE_MSG);
		alert.hover(function(){
			 var id = $(this).attr('id');
			 var atime = alertTimeouts[id];
			 if(atime){
				 clearTimeout(atime);
			 }
			 delete alertTimeouts[id];
		 },function(){
			 var id = $(this).attr('id');
			 var atime = alertTimeouts[id];
			 if(atime){
				 clearTimeout(atime);
			 }
			 alertTimeouts[id] = setTimeout("clearAlert('" + id + "')",TIME_OUT_FOR_HIDE_MSG);
		 });
		 alert.click(function(){
			 var id = $(this).attr('id');
			 clearAlert(id);
		 });
	}
}

function buildNoticeArgs(){
	var args = arguments[0];
	var options = {};
	if(args.length == 1 && typeof(args[0]) == 'object'){
		options = args[0];
	}else if(args.length == 1 && typeof(args[0]) != 'object'){
		options.text = getOrElse(args[0]);
	}else if(args.length == 2 && (typeof(args[0]) == 'string' || typeof(args[0]) == 'number') && (typeof(args[1]) == 'string' || typeof(args[1]) == 'number')){
		options.title = args[0];
		options.text = args[1];
	}else if(args.length == 2 && (typeof(args[0]) == 'string' || typeof(args[0]) == 'number') && typeof(args[1]) == 'boolean'){
		options.text = args[0];
		options.sticky = args[1];
	}else if(args.length == 3){
		options.title = args[0];
		options.text = args[1];
		options.sticky = args[2];
	}else if(args.length == 4){
		options.title = args[0];
		options.text = args[1];
		options.sticky = args[2];
		options.image = args[3];
	}else if(args.length == 5){
		options.title = args[0];
		options.text = args[1];
		options.sticky = args[2];
		options.sound = args[4];
	}else if(args.length == 6){
		options.title = args[0];
		options.text = args[1];
		options.sticky = args[2];
		options.sound = args[4];
		options.url = args[5];
	}
	return options;
}

$.gritter.options.position = 'bottom-right';
var _GRITTERS = {};
var _GRITTERS_CALLBACK = {};
function notice(){
	var options = buildNoticeArgs(arguments);
	var opts = $.extend({
		title:'',
		text:'',
		sticky:false,
		image:'',
		time:'',
		class_name:'',
		before_open:null,
		after_open:null,
		before_close:null,
		after_close:null
	}, options);
    var fn = opts.id?opts.id:Math.uuid();
    if(_GRITTERS.hasOwnProperty(fn)){
		$.gritter.remove(_GRITTERS[fn]);
		delete _GRITTERS[fn];
	}
	if(opts.url || opts.callback){
		var linktext = getOrElse(opts.linkText, '立即查看');
		var target = getOrElse(opts.target, "_blank");
		var link = opts.url ? " href='%s' target='%s' ".format(opts.url, target):"";
		opts.text = getOrElse(opts.text) + "&nbsp;&nbsp;<a " + link + "onclick='clickGritter(\"" + fn + "\");' class='notice-view-detail'>" + linktext + "</a>"; 
		_GRITTERS_CALLBACK[fn] = opts.callback;
	}
	var num = $.gritter.add(opts);
	_GRITTERS[fn] = num;
	if(opts.sound){
		var soundUrl = CONTEXT_ROOT + '/sound/notice.mp3'; 
		if(typeof(opts.sound) == 'string'){
			soundUrl = opts.sound;
		}
		if($('#sound').length < 1){
			$('body').append('<audio id="sound" src=""></audio>');
		}
		var audio = document.getElementById('sound');
		audio.src = soundUrl;
		audio.play();
	}
}

function clickGritter(fn){
	try{
		if(_GRITTERS.hasOwnProperty(fn)){
			$.gritter.remove(_GRITTERS[fn]);
			delete _GRITTERS[fn];
		}
		if(_GRITTERS_CALLBACK.hasOwnProperty(fn)){
			if(typeof(_GRITTERS_CALLBACK[fn]) == 'function'){
				_GRITTERS_CALLBACK[fn]();
			}
			delete _GRITTERS_CALLBACK[fn];
		}
	}catch(e){
	}
}

function noticeInfo(){
	var options = buildNoticeArgs(arguments);
	var opts = $.extend({
		title:i18n_Common_prompt,
		class_name:'notice-info'
	}, options);
	notice(opts);
}
function noticeSuccess(){
	var options = buildNoticeArgs(arguments);
	var opts = $.extend({
		title:i18n_Common_success,
		class_name:'notice-success'
	}, options);
	notice(opts);
}
function noticeError(){
	var options = buildNoticeArgs(arguments);
	var opts = $.extend({
		title:i18n_Common_error,
		class_name:'notice-danger'
	}, options);
	notice(opts);
}
function noticeWarning(){
	var options = buildNoticeArgs(arguments);
	var opts = $.extend({
		title:i18n_Common_warning,
		class_name:'notice-warning'
	}, options);
	notice(opts);
}

function clearAlert(alertId){
	$('#'+alertId).remove();
	delete alertTimeouts[alertId];
}

function clearBodyMessage(){
	if(document.getElementById('bodyMsg')){
		 $('#bodyMsg').empty();
		 for(var alertId in alertTimeouts){
			 var atimeout = alertTimeouts[alertId];
			 if(atimeout){
				 clearTimeout(atimeout);
			 }
			 delete alertTimeouts[alertId];
		 }
	}
}

function bodySuccess(msg, id){
	bodyMessage('success', msg, id);
}

function bodyInfo(msg, id){
	bodyMessage('info', msg, id);
}

function bodyError(msg, id){
	bodyMessage('danger', msg, id);
}

function bodyWarning(msg, id){
	bodyMessage('warning', msg, id);
}

function domExsit(id){
	 var dom = document.getElementById(id);
	 if(dom){
		 return true;
	 }else{
		 return false;
	 }
}
function getSelectionsProperty(tableId,property){
	var selections = $('#'+tableId).bootstrapTable('getSelections');
	var propertyArr = new Array();
	for(var i=0;i<selections.length;i++){
		propertyArr.push(selections[i][property]);
	}
	return propertyArr.toString();
}
function getTableProperty(tableId,property){
	var columData = $('#'+tableId).bootstrapTable('getData');
	var propertyArr = new Array();
	for(var i=0;i<columData.length;i++){
		propertyArr.push(columData[i][property]);
	}
	return propertyArr.toString();
}
function redirect(url){
	window.location.href = url;
}

function multiValueText(obj, values, splitor){
	if(typeof(splitor) == "undefined"){
		splitor = ',';
	}
	if(typeof(values) == "undefined" || typeof(obj) != "object"){
		return '';
	}
	var vs = values.split(splitor);
	var ret = [];
	for(var i = 0; i < vs.length; i++){
		var v = vs[i];
		if(v && obj.hasOwnProperty(v)){
			ret.push(getOrElse(obj[v]));
		}
	}
	return ret.join(splitor);
}

Date.prototype.isLeapYear = function () { 
	var year = this.getFullYear();
    return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0)); 
};

Date.prototype.getDaysInMonth = function () { 
    return [31, (this.isLeapYear() ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][this.getMonth()];
};

//在当前时间上添加年数  
Date.prototype.addYear = function(years){  
    return this.addMonth(12*years);      
}  
//在当前时间上添加天数  
Date.prototype.addDay = function(days){ 
	var ret = new Date(this.getTime());
    var cd = ret.getDate();  
    cd += days;  
    ret.setDate(cd);  
    return ret;  
}  
//在当前时间上添加月数  
Date.prototype.addMonth = function(months){  
	var ret = new Date(this.getTime());
    var cm = ret.getMonth();  
    var date = ret.getDate();
    cm += months;
    ret.setDate(1);
    ret.setMonth(cm);    
    ret.setDate(Math.min(date, ret.getDaysInMonth()));
    return ret;  
}

Date.prototype.addMonthInside = function(months){ 
	var days = months < 0? -1 : 1;
    return this.addMonth(months).addDay(days);  
}

function checkFullMonth(fromDate, toDate){
	if(!fromDate || !toDate){
		return true;
	}
	var nf = fromDate.addDay(-1);
	var ndate = new Date(toDate.getFullYear(), toDate.getMonth(), 1);
	var day = Math.min(nf.getDate(), ndate.getDaysInMonth());
	return day == toDate.getDate();
}
//同步执行方法
function getSyncData(url) {
	var returnData = null; 
	if(typeof url === 'string'){
		$.ajax({
			url: url,
			async: false,
			type: "get",
			success: function(data) {
				returnData = data;
			} 
		});
	}
	return returnData;
}
//输入限制
function keyPress(type){
	event = window.event;
	var keyCode = event.keyCode; 
	if(keyCode >= 37 && keyCode <= 40){
		event.returnValue = true;
	}else if(type == 'number' || type == 'bank'){
		event.returnValue = (keyCode >= 48 && keyCode <= 57);
	}else if(type == 'float'){
		event.returnValue = ((keyCode >= 48 && keyCode <= 57)||event.keyCode==46);
	}else if(type == 'mobile' || type == 'phone'){
		event.returnValue = ((keyCode >= 48 && keyCode <= 57)||event.keyCode==189);
	}else if(type == 'idcard'){
		event.returnValue = ((keyCode >= 48 && keyCode <= 57) || event.keyCode == 88);
	}else{
		event.returnValue = true;
	}
}
//剪切板
function changeClipboard(type, max, min){
	var value = clipboardData.getData('text');
	var nv = '';
	if(type == 'number' || type == 'bank'){
		nv = value.replace(/\D/g,'');
		if(max && toNumber(nv) > toNumber(max)){
			nv = max;
		}else if(min && toNumber(nv) < toNumber(min)){
			nv = min;
		}
	}else if(type == 'float' || type == 'ip'){
		//先把非数字的都替换掉，除了数字和.
		nv = value.replace(/[^\d.]/g,"");
		//必须保证第一个为数字而不是.
		nv = nv.replace(/^\./g,"");
		//保证只有出现一个.而没有多个.
		if(type == 'float'){
			nv = nv.replace(/\.{2,}/g,".");
			//保证.只出现一次，而不能出现两次以上
			nv = nv.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
		}
	}else if(type == 'mobile' || type == 'phone'){
		nv = value.replace(/[^\d-]/g,"");
	}else if(type == 'half'){
		nv = value.replace(/[^\x00-\xff]/g,'');
	}else if(type == 'char'){
		nv = value.replace(/[^a-zA-Z]/g,'');
	}else if(type == 'charnum'){
		nv = value.replace(/[^a-zA-Z0-9]/g,'');
	}else if(type == 'idcard'){
		nv = value.replace(/[^\dX]/g,"");
	}else{
		return;
	}
	clipboardData.setData(nv);
}

//输入限制
function changeText(dome, type, max, min){
	var _this = $(dome);
	var value = _this.val();
	var nv = '';
	if(type == 'number' || type == 'bank'){
		nv = value.replace(/\D/g,'');
		if(max && toNumber(nv) > toNumber(max)){
			nv = max;
		}else if(min && toNumber(nv) < toNumber(min)){
			nv = min;
		}
	}else if(type == 'float' || type == 'ip'){
		//先把非数字的都替换掉，除了数字和.
		nv = value.replace(/[^\d.]/g,"");
		//必须保证第一个为数字而不是.
		nv = nv.replace(/^\./g,"");
		//保证只有出现一个.而没有多个.
		if(type == 'float'){
			nv = nv.replace(/\.{2,}/g,".");
			//保证.只出现一次，而不能出现两次以上
			nv = nv.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
		}
	}else if(type == 'mobile' || type == 'phone'){
		nv = value.replace(/[^\d-]/g,"");
	}else if(type == 'half'){
		nv = value.replace(/[^\x00-\xff]/g,'');
	}else if(type == 'char'){
		nv = value.replace(/[^a-zA-Z]/g,'');
	}else if(type == 'charnum'){
		nv = value.replace(/[^a-zA-Z0-9]/g,'');
	}else if(type == 'idcard'){
		nv = value.replace(/[^\dX]/g,"");
	}else{
		return;
	}
	if(nv == value){
		return;
	}
	_this.val(nv);
	//_this.change();
	//$(_this[0].form).bootstrapValidator('revalidateField', _this.attr('name'));
}
//取控件的数值
function getNumberValue(id){
	var value = $('#'+id).val();
	return isNaN(Number(value)) ? 0 : Number(value);
}
function toNumber(val){
	if(val){
		return isNaN(Number(val)) ? 0 : Number(val);
	}
	return Number(0);
}
//取小数
function getScaleNumber(value){
	if(!value || value.toString().indexOf('.') < 0){
		return 0;
	}
	var str = value.toString();
	return toNumber(str.substring(str.indexOf('.')))
}

/**
 ** 加法函数，用来得到精确的加法结果
 ** 说明：javascript的加法结果会有误差，在两个浮点数相加的时候会比较明显。这个函数返回较为精确的加法结果。
 ** 调用：accAdd(arg1,arg2)
 ** 返回值：arg1加上arg2的精确结果
 **/
function accAdd(arg1, arg2) {
    var r1, r2, m, c;
    try {
        r1 = arg1.toString().split(".")[1].length;
    }
    catch (e) {
        r1 = 0;
    }
    try {
        r2 = arg2.toString().split(".")[1].length;
    }
    catch (e) {
        r2 = 0;
    }
    c = Math.abs(r1 - r2);
    m = Math.pow(10, Math.max(r1, r2));
    if (c > 0) {
        var cm = Math.pow(10, c);
        if (r1 > r2) {
            arg1 = Number(arg1.toString().replace(".", ""));
            arg2 = Number(arg2.toString().replace(".", "")) * cm;
        } else {
            arg1 = Number(arg1.toString().replace(".", "")) * cm;
            arg2 = Number(arg2.toString().replace(".", ""));
        }
    } else {
        arg1 = Number(arg1.toString().replace(".", ""));
        arg2 = Number(arg2.toString().replace(".", ""));
    }
    return Number((arg1 + arg2) / m);
}

//给Number类型增加一个add方法，调用起来更加方便。
Number.prototype.add = function (arg) {
    return accAdd(arg, this);
};

/**
 ** 减法函数，用来得到精确的减法结果
 ** 说明：javascript的减法结果会有误差，在两个浮点数相减的时候会比较明显。这个函数返回较为精确的减法结果。
 ** 调用：accSub(arg1,arg2)
 ** 返回值：arg1加上arg2的精确结果
 **/
function accSub(arg1, arg2) {
    var r1, r2, m, n;
    try {
        r1 = arg1.toString().split(".")[1].length;
    }
    catch (e) {
        r1 = 0;
    }
    try {
        r2 = arg2.toString().split(".")[1].length;
    }
    catch (e) {
        r2 = 0;
    }
    m = Math.pow(10, Math.max(r1, r2)); //last modify by deeka //动态控制精度长度
    n = (r1 >= r2) ? r1 : r2;
    return Number(((arg1 * m - arg2 * m) / m).toFixed(n));
}

// 给Number类型增加一个mul方法，调用起来更加方便。
Number.prototype.sub = function (arg) {
    return accSub(this, arg);
};

/**
 ** 乘法函数，用来得到精确的乘法结果
 ** 说明：javascript的乘法结果会有误差，在两个浮点数相乘的时候会比较明显。这个函数返回较为精确的乘法结果。
 ** 调用：accMul(arg1,arg2)
 ** 返回值：arg1乘以 arg2的精确结果
 **/
function accMul(arg1, arg2) {
    var m = 0, s1 = arg1.toString(), s2 = arg2.toString();
    try {
        m += s1.split(".")[1].length;
    }
    catch (e) {
    }
    try {
        m += s2.split(".")[1].length;
    }
    catch (e) {
    }
    return Number(Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, m));
}

// 给Number类型增加一个mul方法，调用起来更加方便。
Number.prototype.mul = function (arg) {
    return accMul(arg, this);
};

/** 
 ** 除法函数，用来得到精确的除法结果
 ** 说明：javascript的除法结果会有误差，在两个浮点数相除的时候会比较明显。这个函数返回较为精确的除法结果。
 ** 调用：accDiv(arg1,arg2)
 ** 返回值：arg1除以arg2的精确结果
 **/
function accDiv(arg1, arg2) {
    var t1 = 0, t2 = 0, r1, r2;
    try {
        t1 = arg1.toString().split(".")[1].length;
    }
    catch (e) {
    }
    try {
        t2 = arg2.toString().split(".")[1].length;
    }
    catch (e) {
    }
    r1 = Number(arg1.toString().replace(".", ""));
    r2 = Number(arg2.toString().replace(".", ""));
    return Number((r1 / r2) * Math.pow(10, t2 - t1));
}

//给Number类型增加一个div方法，调用起来更加方便。
Number.prototype.div = function (arg) {
    return accDiv(this, arg);
};

//移除validation
function disabledFieldValidators(element, disabled){
	if($(element)[0] && $(element)[0].form){
		var form = $($(element)[0].form);
		form.bootstrapValidator('enableFieldValidators', element.attr('name'), !disabled);
		if(disabled){
			clearElementValidation();
		}else{
			form.bootstrapValidator('revalidateField', element.attr('name'));
		}
		form.bootstrapValidator('updateExcludedOption', element.attr('name'),disabled);
	}
	if(element.attr('data-bv-excluded')){
		element.attr('data-bv-excluded', ''+disabled);
	}
	var groupParent = element.parents('.group-with-required');
	if(groupParent.length == 1){
		var sreq = groupParent.find('span.input-group-required');
		if(sreq.length){
			var visible = (sreq.data('required') && !disabled) ? "visible" : "hidden";
			sreq.css('visibility', visible)
		}
	}
}

//清空validation
function clearElementValidation(element, group, container){
	//add by hxiao
	var $field = $(element);
	var field = $field.attr('name');
    group     = group || '.form-group';
    container = container || 'popover';
    //清空消息
    if($field.data('bv.messages')){
    	$field.data('bv.messages').find('.help-block[data-bv-validator][data-bv-for="' + field + '"]').remove().end()
    	.end()
    	.removeData('bv.messages')
    	// Remove feedback classes
    	// .parents(group)
    	//     .removeClass('has-feedback has-error has-success')
    	//     .end()
    	// Turn off events
    	.off('.bv')
    	.removeAttr('data-bv-field');
    	var $parent;
    	if($field.parents(group).length > 1 && $field.parentsUntil('.no-mp-group',group).length == 1){
    		$parent = $field.parentsUntil('.no-mp-group',group);
    	}else{
    		$parent = $field.parents(group);
    	}
    	$parent.removeClass('has-feedback has-error has-success');
    	// Remove feedback icons, tooltip/popover container
    	$icon = $field.parents(group).find('i[data-bv-icon-for="' + field + '"]');
    	if ($icon) {
    		switch (container) {
    		case 'tooltip':
    			$icon.tooltip('destroy').remove();
    			break;
    		case 'popover':
    			$icon.popover('destroy').remove();
    			break;
    		default:
    			$icon.remove();
    		break;
    		}
    	}
    }
}
// 判断是否为空
function isEmpty(str){
	if(str == null){
		return true;
	}else if(str == "undefind"){
		return true;
	}else if(str == "null"){
		return true;
	}else if(str == ""){
		return true;
	}else if(str instanceof String){
		if(str.trim().length == 0){
			return true;
		}
	}else{
		return false;
	}
}
// ajax处理返回消息提示
// param:ResultBean
function ajaxBackMsg(data){
	if(isEmpty(data)||!data.hasOwnProperty('status')||!data.hasOwnProperty('messages')){
		return error("返回的对象不对！");
	}
	if(data['status']){
		return success(data['messages']);
	}else{
		return error(data['messages']);
	}
}

function pad(value, len, pchar, left) {
	var length = toNumber(len);
	if(length){
		if(left){
			return (Array(length + 1).join(pchar) + value).slice(-length);
		}else{
			return (value + Array(length + 1).join(pchar)).slice(-length);
		}
	}
	return value;
}

function autoPad(element, len, pchar, left){
	var length = toNumber(len);
	if(length){
		$(element).val(pad($(element).val(),len,pchar,left));
	}
}

function getSafeValue(value, min, max, fixlength, fixchar, fixleft){
	var n = toNumber(value);
	n = Math.min(Math.max(toNumber(min), n), toNumber(max));
	return pad(n, fixlength, fixchar, fixleft);
}

String.prototype.text = function(){
	//过滤脚本,样式以及HTML标签
	return this.replace(/<\s*[s|S][c|C][r|R][i|I][p|P][t|T][^>]*>(.|[\r\n])*?<\s*\/[s|S][c|C][r|R][i|I][p|P][t|T][^>]*>/gi, '')
			   .replace(/<\s*[s|S][t|T][y|Y][l|L][e|E][^>]*>(.|[\r\n])*?<\s*\/[s|S][t|T][y|Y][l|L][e|E][^>]*>/gi, '')
			   .replace(/<\/?[^>]+>/g, '')
			   .replace(/\&[a-z]+;/gi, '');
};

function safeText(value){
	if(value){
		return value.text();
	}
	return getOrElse(value);
}

function goBack(){ 
	if ((navigator.userAgent.indexOf('MSIE') >= 0) && (navigator.userAgent.indexOf('Opera') < 0)){ // IE
		if(history.length > 0){ 
			window.history.go(-1); 
		}else{ 
			window.opener=null;
			window.close(); 
		} 
	}else{ // 非IE浏览器
		if (navigator.userAgent.indexOf('Firefox') >= 0 || 
			navigator.userAgent.indexOf('Opera') >= 0 || 
			navigator.userAgent.indexOf('Safari') >= 0 || 
			navigator.userAgent.indexOf('Chrome') >= 0 || 
			navigator.userAgent.indexOf('WebKit') >= 0){ 
	 
			if(window.history.length > 1){ 
				window.history.go(-1); 
			}else{ 
				window.opener=null;
				window.close(); 
			} 
		}else{ // 未知的浏览器
			window.history.go(-1); 
		} 
	} 
}

function getQueryString(key){
	var result = location.search.match(new RegExp("[\?\&]" + key + "=([^\&]+)","i"));    
    if(result == null || result.length < 2){    
    	return "";  
    } 
    return result[1];
}

//导师平均响应时间格式化函数 ss:响应时间字符串  单位：毫秒
function formatResponseTime(ss){
	if(ss==null||ss==undefined||ss==''||ss=='undefined'){
		ss = "0";
	}
	var time = parseInt(ss);
	if(time < 1000*60*60*12){
		return "半天内";
	}else{
		return (Math.ceil(time/(1000*60*60*24)))+"天内";
	}
}

//笔记时间，评论时间等显示规则：几分钟前（分钟级别）、几小时前（小时级别）、几天前（7天内），正常时间格式（7天以上）
//ss:时间毫秒数
function formatPublishTime(ss){
	if(ss==null||ss==undefined||ss==''||ss=='undefined'){
		ss = "0";
	}
	var time = parseInt(ss);
	time = new Date().getTime() - time;
	if(time < 1000*60*60){
		//time一个小时内
		return (Math.floor(time/(1000*60)))+"分钟前";
	}else if(time < 1000*60*60*24){
		//time一天内
		return (Math.floor(time/(1000*60*60)))+"小时前";
	}else if(time < 1000*60*60*24*7){
		//time在7天内
		return (Math.floor(time/(1000*60*60*24)))+"天前";
	}else{
		//time在7天以上
		return dateTimeFormatMi(time);
	}
}
//老师接受率：低接受率（<50%），中等接受率(<90%)，高接受率(>90%)
//value:百分比（不包括百分号）
function formatMasterAccept(ss){
	if(ss==null||ss==undefined||ss==''||ss=='undefined'){
		ss = "0";
	}
	var value = parseInt(ss);
	if(value <= 50){
		return "低接受率";
	}else if(value <= 90){
		return "中等接受率";
	}else{
		return "高接受率";
	}
}
//辅导等持续时常，类似每节课多长时间的概念
//ss:毫秒数
function formatDurationToHour(ss){
	if(ss==null||ss==undefined||ss==''||ss=='undefined'){
		ss = "0";
	}
	var time = parseInt(ss);
	return "约"+(Math.ceil(time/(1000*60*60)))+"小时";
}

function getFileSizeUnit(value){
	var unit = 'B';
	if(value){
		if(value.indexOf('K') > -1 || value.indexOf('k') > -1){
			unit = 'K';
		}
		if(value.indexOf('M') > -1 || value.indexOf('m') > -1){
			unit = 'M';
		}
		if(value.indexOf('G') > -1 || value.indexOf('g') > -1){
			unit = 'G';
		}
	}
	return unit;
}

function formatFileSize(v, target){
	var value = (v != null && typeof(v) != 'undefined') ? v.toString():"0";
	var units = 'BKMG';
	var unitnames = ['B','KB','MB','GB'];
	var orgunit = getFileSizeUnit(value);
	var nvalue = toNumber(value.replace(/\D/g,''));
	var ob = nvalue * Math.pow(1024,units.indexOf(orgunit));
	if(target){
		//目标单位
		var targetunit = getFileSizeUnit(target);
		var ui = units.indexOf(targetunit);
		return Math.round(accDiv(ob, Math.pow(1024,ui)) *100)/100 + unitnames[ui];
	}else{
		//自动单位
		for(var i = 0; i < units.length; i++){
			var tv = Math.round(accDiv(ob, Math.pow(1024,i)) *100)/100;
			if(tv >= 1000 && i < units.length - 1){
				continue;
			}
			return tv +  unitnames[i];
		}
	}
}

//对多余的字符进行隐藏
function hideStringByMax(oldString, maxLength) {
	var result = oldString;
	if(oldString!=null && oldString.length > maxLength){
		result = oldString.substring(0,maxLength);
		result += "...";
	}
	return result;
}

//rowNumber
function formatRowNumber(value, row, index){
	var params = $('#search-result').bootstrapTable('getLastQueryParams');
	return toNumber(params.offset) + index + 1;
}

//
function getFileExtension(fileName){
	if(fileName && fileName.indexOf('.') > 0){
		return fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
	}
	return "";
}

function isPicture(fileName){
	var fe = getFileExtension(fileName);
	return ",jpg,jpeg,bmp,gif,png,icon,".indexOf((','+fe+',').toLowerCase()) > -1;
}

function isVideo(fileName){
	var fe = getFileExtension(fileName);
	return ",mp4,wmv,flv,avi,mpeg,".indexOf((','+fe+',').toLowerCase()) > -1;
}

function getFileIcon(fileName){
	var fe = getFileExtension(fileName);
	if(fe){
		if(FILE_ICON_CSS.hasOwnProperty(fe)){
			return FILE_ICON_CSS[fe];
		}
	}
	return "fa fa fa-file-o";
}

var FILE_ICON_CSS = {
	"pdf":"fa fa-file-pdf-o",
	"jpg":"fa fa-file-image-o",
	"jpeg":"fa fa-file-image-o",
	"bmp":"fa fa-file-image-o",
	"gif":"fa fa-file-image-o",
	"png":"fa fa-file-image-o",
	"icon":"fa fa-file-image-o",
	"mp4":"fa fa-file-movie-o",
	"wmv":"fa fa-file-movie-o",
	"flv":"fa fa-file-movie-o",
	"avi":"fa fa-file-movie-o",
	"mpeg":"fa fa-file-movie-o",
	"xlsx":"fa fa-file-excel-o",
	"xls":"fa fa-file-excel-o",
	"doc":"fa fa-file-word-o",
	"docx":"fa fa-file-word-o",
	"mp3":"fa fa-file-sound-o",
	"wav":"fa fa-file-sound-o",
	"ape":"fa fa-file-sound-o",
	"flac":"fa fa-file-sound-o"
}

function getFileIds(name){
	var fileIds = [];
	$('#'+name+'_file_preview').find('.file-preview-frame').each(function(){fileIds.push($(this).attr('id'));});
	return fileIds.join(",");
}

function getImageFileIds(name){
	var fileIds = [];
	$('#'+name+'_file_preview').find('.file-preview-frame').each(function(){
		if($(this).find(".fa-file-image-o").length || $(this).find("img").length){
			fileIds.push($(this).attr('id'));
		}
		
	});
	return fileIds.join(",");
}

function getFileNames(name){
	var fileNames = [];
	$('#'+name+'_file_preview').find('.file-preview-frame .file-thumbnail-footer .file-footer-caption').each(function(){fileNames.push($(this).attr('title'));});
	return fileNames.join(",");
}

if (!Array.prototype.includes) {
	  Array.prototype.includes = function(searchElement /*, fromIndex*/ ) {
	    'use strict';
	    var O = Object(this);
	    var len = parseInt(O.length) || 0;
	    if (len === 0) {
	      return false;
	    }
	    var n = parseInt(arguments[1]) || 0;
	    var k;
	    if (n >= 0) {
	      k = n;
	    } else {
	      k = len + n;
	      if (k < 0) {k = 0;}
	    }
	    var currentElement;
	    while (k < len) {
	      currentElement = O[k];
	      if (searchElement === currentElement ||
	         (searchElement !== searchElement && currentElement !== currentElement)) {
	        return true;
	      }
	      k++;
	    }
	    return false;
	  };
	}

Array.prototype.merge=function(){
	var arrs = arguments;
	for(var i = 0; i<arrs.length;i++){
		if(typeof(arrs[i])=='string'&&!this.includes(arrs[i])){
			this.push(arrs[i]);
		}else if($.isArray(arrs[i])){
			for(var j=0;j<arrs[i].length;j++){
				if(typeof(arrs[i][j]) == 'string'&&!this.includes(arrs[i][j])){
					this.push(arrs[i][j]);
				}
			}
		}
	}
}

function getType(param) {     
    return ((_t = typeof (param)) == "object" ? Object.prototype.toString.call(param).slice(8, -1) : _t).toLowerCase();     
}   