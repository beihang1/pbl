/**
 * @author hxiao
 * Sample Code:<div data-toggle="combotree" data-id="quanxian" data-empty="" data-url="@url("/role/getRoleModule.action?roleId=01")"></div>
 */

!function ($) {
	 var BootstrapCombotree = function (el, options) {
        this.options = options;
        this.$el = $(el);
        this.$el_ = this.$el.clone();
        this.timeoutId_ = 0;
        this._initialedValue = false;
        this.init();
    };
	    
    BootstrapCombotree.DEFAULTS = {
        value:'',
        url:'',
        multiple:false,
        id:'',
        name:'',
        width:-1,
        empty:true,
        required:'',
        popplace:'top',
        textname:'',
        disabled:false,
        placeholder: i18n_Combotree_select,
        maxDropHeight:200
    };

    BootstrapCombotree.prototype.init = function () {
        this.initTextInput();
        this.initTree();
        var _this = this;
        $(document).click(function(e){ 
        if(!_this.options.disabled){
	    	 e = window.event || e; // 兼容IE7
	    	 obj = $(e.srcElement || e.target);
	//    	    if ($(obj).is('#' + _this.$treeId + ',#' + _this.$treeId + ' *') || $(obj).is('#' + _this.$dropId + ',#' + _this.$dropId + ' *')) { 
	//    	     // alert('内部区域'); 
	//    	   } else {
	//    		   _this.hideDrop();
	//    	   } 
	    	 if($(obj).is('#' + _this.$dropId + ',#' + _this.$dropId + ' *')){
	    		 return;
	    	 }
	    	 var offset = _this.$tree.offset();
	    	 var or = offset.left + _this.$tree.outerWidth();
	    	 var ob = offset.top + _this.$tree.outerHeight();
	    	 var ex = e.clientX + $(window).scrollLeft();
	    	 var ey = e.clientY + $(window).scrollTop();
	    	 if(ex < offset.left || ex > or ||ey < offset.top || ey > ob){
	    		 var doffset = _this.$drop.offset();
	    		 var dor = doffset.left + _this.$drop.outerWidth();
	    		 var dob = doffset.top + _this.$drop.outerHeight();
	    		 if(ex > doffset.left && ex < dor && ey > doffset.top && ey < dob){
	    			 _this.toggleDrop(e);
	    			 return false;
	    		 }else{
	    			 _this.hideDrop();
	    		 }
    	 }
    	 }
    	});
    };
    
    BootstrapCombotree.prototype.toggleDrop = function (e) {
    	if(!this.options.disabled){
    		if(this.$tree.css("display") == 'none'){
    			var width = this.$text[0].offsetWidth + this.$drop[0].offsetWidth;
    			this.$tree.width(width);
    			this.$tree.css("display",'block');
    		}else{
    			this.$tree.css("display",'none');
    		}
    	}
    };
    
    BootstrapCombotree.prototype.hideDrop = function () {
		this.$tree.css("display",'none');
    };
    
    BootstrapCombotree.prototype.initTextInput = function () {
    	var hasInput = this.$el.next('input[type=text].form-control').length;
    	if(!hasInput){
    		this.$value = $('<input type="hidden" id="' + this.options.id + '" name="' + this.options.name + '" />' );
    		this.$text = $('<input type="text" style="cursor: pointer;" readonly data-bv-excluded="false" data-bv-placement="' + this.options.popplace + '" class="form-control" ' + this.options.required + ' data-bv-trigger="change" name="' + this.options.textname +'" placeholder="' + this.options.placeholder + '"/>');
    		this.$dropId = 'drop' + Math.uuid();
    		this.$drop = $('<a style="width:39px;display: table-cell;border-top-left-radius: 0;padding-left:-1px;border-bottom-left-radius: 0;border-left:0px" id="' + this.$dropId + '" class="btn btn-default fa fa-caret-down combotree-rythm-down" > </a>');
    		this.$text.insertAfter(this.$el);
    		this.$value.insertAfter(this.$text);
    		this.$drop.insertAfter(this.$value);
    	}else{
    		this.$text = this.$el.next('input[type=text]');
    		this.$value = this.$text.parent().find('input[type=hidden]')
    		this.$drop =this.$text.parent().find('.combotree-rythm-down')
    		this.$dropId = this.$drop.attr('id');
    	}
    	if(this.options.width > -1){
    		this.$text.width(this.options.width - 30);
    	}
    };

    BootstrapCombotree.prototype.initTree = function () {
    	var _this = this;
    	this.$treeId = 'tree' + Math.uuid();
    	//var emptybar = '<ul class="dm-empty-bar"><li role="treeitem">&nbsp;</li></ul>';
    	//this.$emptybar = $(emptybar);
    	//this.$emptybar.click(function(){
    	//	_this.setValue('');
    	//});
    	this.$tree = $('<div class="combotree-rythm-drop" id="' + this.$treeId + '"></div>');
    	//if(!this.options.disabled){
	    	this.$drop.click(function(){
	    		_this.toggleDrop();
	    		return false;
	    	});
    	//}
    	this.$text.click(function(){
    		_this.toggleDrop();
    		return false;
    	});
    	
    	this.$tree.insertBefore(this.$el);
    	this.$tree.css('left', 'auto');//FIX 4 Firefox this.$text.css('left')
    	var width = this.$text[0].offsetWidth + this.$drop.offsetWidth;
    	this.$tree.width(width);
    	this.$tree.css('max-height', this.options.maxDropHeight);
    	this.$tree.jstree({
			'plugins': this.options.multiple ? ["checkbox","wholerow" ] : ["wholerow"],
			'core' : {
				//'check_callback' : true,
				'strings' : {
					 'Loading ...' : i18n_Combotree_loading
				 },
				 'themes': {
	                'name': 'proton',
	                'responsive': true,
	                "icons" : false
	            },
				'data' : {
			    	'url' : this.options.url,
			    	'success':function(data){
			    		if($.isArray(data) && _this.options.empty && !_this.options.multiple){
		    				data.insert(0, {id:"",text:""});
		    			}
			    	}
				}
	    	}
		}).on('loaded.jstree', function(event, data) {
			_this.setValue(_this.options.value);
			_this.$tree.jstree('open_all');
		  }).on('changed.jstree', function() {
			  _this.refresh();
		  });
    };

    BootstrapCombotree.prototype.getValue = function () {
    	return this.$value.val();
    };
    
    BootstrapCombotree.prototype.refresh = function () {
    	var selected = this.$tree.jstree('get_top_selected', true);
    	var values = [];
    	var texts = [];
    	for(var i = 0; i < selected.length; i++){
    		values[i] = selected[i].id;
    		texts[i] = selected[i].text;
    	}
    	this.$value.val(values.join(','));
    	this.$text.val(texts.join(','));
    	if(!this._initialedValue){
    		this._initialedValue = true;
    	}else{
    		this.$text.change();
    		if(!this.options.multiple){
    			this.hideDrop();
    		}
    	}
    };
    
    BootstrapCombotree.prototype.getText = function () {
    	return this.$text.val();
    };
    
    BootstrapCombotree.prototype.setDisabled = function (disabled) {
    	this.$text.prop('disabled', disabled);
    	this.$drop.attr('disabled', disabled);
    	this.options.disabled = disabled;
    };
    
    BootstrapCombotree.prototype.isDisabled = function () {
    	return this.options.disabled;
    };

    BootstrapCombotree.prototype.setValue = function (value) {
        var values = (typeof(value) == 'string' ) ? (value && value != ''? value.split(',') :  null) : value;
        if(values){
        	//for(var i = 0; i < values.length; i++){
        		this.$tree.jstree("select_node", values, true);
        	//}
        }else{
        	this.$tree.jstree("deselect_all");
        }
    };
   
    // BOOTSTRAP Combotree PLUGIN DEFINITION
    // =======================

    $.fn.bootstrapCombotree = function (option, _relatedTarget) {
        var allowedMethods = [
                'getValue', 'setValue', 'destroy','getText','setDisabled','isDisabled'
            ],
            value;

        this.each(function () {
            var $this = $(this),
            	data = $this.data('bootstrap.combotree'),
                options = $.extend({}, BootstrapCombotree.DEFAULTS, $this.data(),
                    typeof option === 'object' && option);

            if (typeof option === 'string') {
                if ($.inArray(option, allowedMethods) < 0) {
                    throw "Unknown method: " + option;
                }

                if (!data) {
                    return;
                }
                
                if(option === 'options'){
                	value = data[option];
                }else{
                	value = data[option](_relatedTarget);
                }


                if (option === 'destroy') {
                    $this.removeData('bootstrap.combotree');
                }
            }

            if (!data) {
                $this.data('bootstrap.combotree', (data = new BootstrapCombotree(this, options)));
            }
        });

        return typeof value === 'undefined' ? this : value;
    };

    $.fn.bootstrapCombotree.Constructor = BootstrapCombotree;
    $.fn.bootstrapCombotree.defaults = BootstrapCombotree.DEFAULTS;

    // BOOTSTRAP Combotree INIT
    // =======================

    $(function () {
        $('[data-toggle="combotree"]').bootstrapCombotree();
    });

}(jQuery);
