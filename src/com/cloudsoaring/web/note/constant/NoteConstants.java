package com.cloudsoaring.web.note.constant;

public interface NoteConstants extends NoteConfigNames, NoteDbConstants, NoteLogConstants, NoteDefaultValues, NoteMsgNames, NoteStateConstants{
  
}
