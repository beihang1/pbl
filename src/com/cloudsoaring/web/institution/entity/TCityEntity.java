package com.cloudsoaring.web.institution.entity;

import java.util.Date;

import com.cloudsoaring.web.common.entity.BaseEntity;
/**
 * t_city 市级信息
 * @author ljl 2020/06/24
 *
 */
public class TCityEntity extends BaseEntity{

	/** serialVersionUID*/
	private static final long serialVersionUID = 1L;
	//编码
	private String cityCode;
	//名称
	private String cityName;
	//省级编码
	private String provCode;
	//简称
	private String py;
	//删除标识
	private int isDelete;
	//排序
	private int sort;
	//更新时间
	private Date updateTime;

	public String getCityCode() {
		return cityCode;
	}


	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}


	public String getCityName() {
		return cityName;
	}


	public void setCityName(String cityName) {
		this.cityName = cityName;
	}


	public String getProvCode() {
		return provCode;
	}


	public void setProvCode(String provCode) {
		this.provCode = provCode;
	}


	public String getPy() {
		return py;
	}


	public void setPy(String py) {
		this.py = py;
	}


	public int getIsDelete() {
		return isDelete;
	}


	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}


	public int getSort() {
		return sort;
	}


	public void setSort(int sort) {
		this.sort = sort;
	}


	public Date getUpdateTime() {
		return updateTime;
	}


	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
	

}
