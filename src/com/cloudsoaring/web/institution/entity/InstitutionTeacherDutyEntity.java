package com.cloudsoaring.web.institution.entity;

import com.cloudsoaring.web.common.entity.BaseEntity;
/**
 *  
 * @author ljl 2020/06/23
 *
 */
public class InstitutionTeacherDutyEntity extends BaseEntity {

	/** serialVersionUID*/
	private static final long serialVersionUID = 1L;
	private Object dutyName;//职责
	private Object courseId;//学校id
	private Object courseName;//学校名称
	private Object gradeName;//年级名称
	private Object gradeId;//年级id
	private Object gradeLevel;//年级等级
	private Object classId;//班级id
	private Object className;//班级名称
	private Object teamLeaderId;//团队领导id
	private Object teamLeaderName;//团队领导名称
	private Object dgradeId;//年级
	private Object classDictId;//班级字典id
	private Object classDictName;//班级字典名称
	public Object getDutyName() {
		return dutyName;
	}
	public void setDutyName(Object dutyName) {
		this.dutyName = dutyName;
	}
	public Object getCourseId() {
		return courseId;
	}
	public void setCourseId(Object courseId) {
		this.courseId = courseId;
	}
	public Object getCourseName() {
		return courseName;
	}
	public void setCourseName(Object courseName) {
		this.courseName = courseName;
	}
	public Object getGradeName() {
		return gradeName;
	}
	public void setGradeName(Object gradeName) {
		this.gradeName = gradeName;
	}
	public Object getGradeId() {
		return gradeId;
	}
	public void setGradeId(Object gradeId) {
		this.gradeId = gradeId;
	}
	public Object getGradeLevel() {
		return gradeLevel;
	}
	public void setGradeLevel(Object gradeLevel) {
		this.gradeLevel = gradeLevel;
	}
	public Object getClassId() {
		return classId;
	}
	public void setClassId(Object classId) {
		this.classId = classId;
	}
	public Object getClassName() {
		return className;
	}
	public void setClassName(Object className) {
		this.className = className;
	}
	public Object getTeamLeaderId() {
		return teamLeaderId;
	}
	public void setTeamLeaderId(Object teamLeaderId) {
		this.teamLeaderId = teamLeaderId;
	}
	public Object getTeamLeaderName() {
		return teamLeaderName;
	}
	public void setTeamLeaderName(Object teamLeaderName) {
		this.teamLeaderName = teamLeaderName;
	}
	public Object getDgradeId() {
		return dgradeId;
	}
	public void setDgradeId(Object dgradeId) {
		this.dgradeId = dgradeId;
	}
	public Object getClassDictId() {
		return classDictId;
	}
	public void setClassDictId(Object classDictId) {
		this.classDictId = classDictId;
	}
	public Object getClassDictName() {
		return classDictName;
	}
	public void setClassDictName(Object classDictName) {
		this.classDictName = classDictName;
	}
	
	

}
