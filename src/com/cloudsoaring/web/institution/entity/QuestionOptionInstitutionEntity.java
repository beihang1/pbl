

package com.cloudsoaring.web.institution.entity;


import java.util.Map;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_question_option
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class QuestionOptionInstitutionEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**问题ID*/
	private String questionId;
	/**内容*/
	private String content;
	/**排序*/
	private java.math.BigDecimal optionOrder;
	/**被选择数*/
	private int numSelected;
	/**测验ID*/
	private String measureId;
	/**排序号*/
	private java.math.BigDecimal questionOrder;
	/**问卷参与总人数*/
	private java.math.BigDecimal totalNum;
	/**选项选择总人数*/
	private java.math.BigDecimal selectedNum;
	/**问卷问题总数*/
	private java.math.BigDecimal totalQuestion;
	/**每个问题选项总数*/
	private java.math.BigDecimal totalOption;
	
	public java.math.BigDecimal getTotalQuestion() {
		return totalQuestion;
	}
	public void setTotalQuestion(java.math.BigDecimal totalQuestion) {
		this.totalQuestion = totalQuestion;
	}
	public java.math.BigDecimal getQuestionOrder() {
		return questionOrder;
	}
	public void setQuestionOrder(java.math.BigDecimal questionOrder) {
		this.questionOrder = questionOrder;
	}
	public java.math.BigDecimal getTotalNum() {
		return totalNum;
	}
	public void setTotalNum(java.math.BigDecimal totalNum) {
		this.totalNum = totalNum;
	}
	public java.math.BigDecimal getSelectedNum() {
		return selectedNum;
	}
	public void setSelectedNum(java.math.BigDecimal selectedNum) {
		this.selectedNum = selectedNum;
	}
	private Map<String, String> optionMap;
	 
	public int getNumSelected() {
		return numSelected;
	}
	public void setNumSelected(int numSelected) {
		this.numSelected = numSelected;
	}
	/**问题ID*/
	public String getQuestionId(){
		return this.questionId;
	}
	/**问题ID*/
	public void setQuestionId(String questionId){
		this.questionId = questionId;
	}
	/**内容*/
	public String getContent(){
		return this.content;
	}
	/**内容*/
	public void setContent(String content){
		this.content = content;
	}
	public java.math.BigDecimal getOptionOrder() {
		return optionOrder;
	}
	public void setOptionOrder(java.math.BigDecimal optionOrder) {
		this.optionOrder = optionOrder;
	}
	public String getMeasureId() {
		return measureId;
	}
	public void setMeasureId(String measureId) {
		this.measureId = measureId;
	}
	public Map<String, String> getOptionMap() {
		return optionMap;
	}
	public void setOptionMap(Map<String, String> optionMap) {
		this.optionMap = optionMap;
	}
	public java.math.BigDecimal getTotalOption() {
		return totalOption;
	}
	public void setTotalOption(java.math.BigDecimal totalOption) {
		this.totalOption = totalOption;
	}
	
}
