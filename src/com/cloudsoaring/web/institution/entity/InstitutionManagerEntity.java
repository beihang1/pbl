package com.cloudsoaring.web.institution.entity;

import java.util.Date;

import com.cloudsoaring.web.common.entity.BaseEntity;
/**
 * tb_institution 组织结构基本信息表
 * @author ljl 2020/06/23
 *
 */
public class InstitutionManagerEntity extends BaseEntity {

	/** serialVersionUID*/
	private static final long serialVersionUID = 1L;
	//名称
	private String name;
	//性别
	private String sex;
	//头像
	private String photo;
	//账号id
	private String accountID;
	//学校名称
	private String schoolName;
	//电话
	private String phoneNumber;
	//邮箱
	private String email;
	//生日
	private String birthday;
	//用户来源（0：注册；1：导入；2：他校转入）
	private int sourceType;
	//系统状态（0：启用；1:禁用）
	private int sysState;
	//标志用户在学校在职状态（1：在职，2：转校，3：离职）
	private int workState;
	//0：待审核  1：通过  2：不通过；
	private int auditState;
	//学校id
	private int schoolId;
	//组织类型
	private int organType;
	//
	private int nodeType;
	//教育id
	private int eduBureauId;
	//教育名称
	private String eduBureauName;
	//角色
	private String role;
	//是否删除(0正常 1删除)
	private String isDelete;
	//
	private Date createTime;
	//
	private Date updateTime;
	//
	private String orgs;
	//省级编码
	private String provinceCode;
	//市级编码
	private String cityCode;
	//区县编码
	private String countyCode;
	//学校类型
	private String schoolType;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getAccountID() {
		return accountID;
	}
	public void setAccountID(String accountID) {
		this.accountID = accountID;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public int getSourceType() {
		return sourceType;
	}
	public void setSourceType(int sourceType) {
		this.sourceType = sourceType;
	}
	public int getSysState() {
		return sysState;
	}
	public void setSysState(int sysState) {
		this.sysState = sysState;
	}
	public int getWorkState() {
		return workState;
	}
	public void setWorkState(int workState) {
		this.workState = workState;
	}
	public int getAuditState() {
		return auditState;
	}
	public void setAuditState(int auditState) {
		this.auditState = auditState;
	}
	public int getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(int schoolId) {
		this.schoolId = schoolId;
	}
	public int getOrganType() {
		return organType;
	}
	public void setOrganType(int organType) {
		this.organType = organType;
	}
	public int getNodeType() {
		return nodeType;
	}
	public void setNodeType(int nodeType) {
		this.nodeType = nodeType;
	}
	public int getEduBureauId() {
		return eduBureauId;
	}
	public void setEduBureauId(int eduBureauId) {
		this.eduBureauId = eduBureauId;
	}
	public String getEduBureauName() {
		return eduBureauName;
	}
	public void setEduBureauName(String eduBureauName) {
		this.eduBureauName = eduBureauName;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getOrgs() {
		return orgs;
	}
	public void setOrgs(String orgs) {
		this.orgs = orgs;
	}
	public String getProvinceCode() {
		return provinceCode;
	}
	public void setProvinceCode(String provinceCode) {
		this.provinceCode = provinceCode;
	}
	public String getCityCode() {
		return cityCode;
	}
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	public String getCountyCode() {
		return countyCode;
	}
	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}
	public String getSchoolType() {
		return schoolType;
	}
	public void setSchoolType(String schoolType) {
		this.schoolType = schoolType;
	}
	
	
	
}
