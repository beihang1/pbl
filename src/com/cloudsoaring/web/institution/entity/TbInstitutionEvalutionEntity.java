

package com.cloudsoaring.web.institution.entity;


import java.util.Date;
import java.util.List;

import com.cloudsoaring.common.annotation.ClassDefine;
import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: tb_institution_evalution
 * @Description: tb_course
 * @date 
 * @version V1.0
 *
 */
@ClassDefine
public class TbInstitutionEvalutionEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**课程名称*/
	private String title;
	/**封面*/
	private String pictureId;
	/**是否免费（0：收费*/
	private java.math.BigDecimal freeFlag;
	/**价格*/
	private java.math.BigDecimal price;
	/**教师ID*/
	private String teacherId;
	/**课程描述*/
	private String content;
	/**课程详情*/
	private String courseDetail;
	/**好评百分比*/
	private java.math.BigDecimal voteGood;
	/**中评百分比*/
	private java.math.BigDecimal voteNormal;
	/**差评百分比*/
	private java.math.BigDecimal voteNotGood;
	/**好评票数*/
	private int goodVote;
	/**中评票数*/
	private int normalVote;
	/**差评票数*/
	private int notGoodVote;
	/**总票数*/
	private int totalVote;
	/**难度设置(0初级,1中级,2高级)*/
	private String courseStage;
	/**课程学习进度(百分比）*/
	private String studySchedule;
	/**推荐度NUM_RECOMMEND*/
	private java.math.BigDecimal numRecommend;
	
	public java.math.BigDecimal getNumRecommend() {
        return numRecommend;
    }
    public void setNumRecommend(java.math.BigDecimal numRecommend) {
        this.numRecommend = numRecommend;
    }
    public String getStudySchedule() {
		return studySchedule;
	}
	public void setStudySchedule(String studySchedule) {
		this.studySchedule = studySchedule;
	}
	public int getGoodVote() {
		return goodVote;
	}
	public void setGoodVote(int goodVote) {
		this.goodVote = goodVote;
	}
	public int getNormalVote() {
		return normalVote;
	}
	public void setNormalVote(int normalVote) {
		this.normalVote = normalVote;
	}
	public int getNotGoodVote() {
		return notGoodVote;
	}
	public void setNotGoodVote(int notGoodVote) {
		this.notGoodVote = notGoodVote;
	}
	/**课程状态(1:连载中,2:已完成,3:已结束)*/
	private String courseStatus;
	/**开课时间起始*/
	private java.util.Date startDate;
	/**开课时间结束*/
	private java.util.Date endDate;
	/**学习时长*/
	private String studyDuration;
	/**备注*/
	private String summary;
	/**状态（0：未发布，1：已发布）*/
	private String status;
	/**测验类型(1:测试，2:考试)*/
	private String measureType;
	/**考试开始时间*/
	private java.util.Date effectiveStartDate;
	/**考试结束时间*/
	private java.util.Date effectiveEndDate;
	/**考试时长(分钟)*/
	private java.math.BigDecimal timeDuration;
	/**视频*/
	private String videoId;
	/**课程须知*/
	private String notice;
	/**课程目标*/
	private String target;
	/**课程范围*/
	private String crowdType;
	/**是否必修*/
	private String flagRequired;
	/**是否可拖动进度*/
	private String flagSkipVideo;
	/**是否开启评论*/
	private String flagDiscuss;
	/**是否开启问答*/
	private String flagQa;
	/**是否开启投票*/
	private String flagVote;
	/**是否开启笔记*/
	private String flagNote;
	/**是否开启案例*/
	private String flagShare;
	/**是否开启问卷*/
	private String flagQuestionnaire;
	/**是否按照顺序学习*/
	private String flagOrderStudy;
	/**学习人数*/
	private java.math.BigDecimal numStudy;
	/**关注人数*/
	private java.math.BigDecimal numFavorite;
	/**浏览数*/
	private java.math.BigDecimal numView;
	/**评价人数*/
	private java.math.BigDecimal numScoreUser;
	/**TAG_ID*/
	private String tagId;
	/**LINK_ID*/
	private String linkId;
	/**用户检索条件，不要删*/
	private String conditionUserId;
	/**用来显示用户是否已经学习*/
	private String sessionUserStudy;
	/**该课程所含标签*/
	@SuppressWarnings("rawtypes")
    private List tagList;
	/**检索条件tagId以逗号拼接*/
	private List<String> tagIds;
	/**是否精品课程*/
	private String flagBoutique;
	/**是否首页轮播*/
    private String flagHomeCarousel;
	/**
	 * 投票人数
	 */
    private String numVote;
	/**教师名称*/
	private String personName;
	 /**课程来源*/
    private String courseRource;
    /**供应商*/
    private String supplier;
    
	public String getCourseRource() {
        return courseRource;
    }
    public void setCourseRource(String courseRource) {
        this.courseRource = courseRource;
    }
    public String getSupplier() {
        return supplier;
    }
    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }
    public String getFlagBoutique() {
        return flagBoutique;
    }
    public void setFlagBoutique(String flagBoutique) {
        this.flagBoutique = flagBoutique;
    }
    public String getFlagHomeCarousel() {
        return flagHomeCarousel;
    }
    public void setFlagHomeCarousel(String flagHomeCarousel) {
        this.flagHomeCarousel = flagHomeCarousel;
    }
    public String getPersonName() {
        return personName;
    }
    public void setPersonName(String personName) {
        this.personName = personName;
    }
    /**课程名称*/
	public String getTitle(){
		return this.title;
	}
	/**课程名称*/
	public void setTitle(String title){
		this.title = title;
	}
	/**封面*/
	public String getPictureId(){
		return this.pictureId;
	}
	/**封面*/
	public void setPictureId(String pictureId){
		this.pictureId = pictureId;
	}
	/**是否免费（0：收费*/
	public java.math.BigDecimal getFreeFlag(){
		return this.freeFlag;
	}
	/**是否免费（0：收费*/
	public void setFreeFlag(java.math.BigDecimal freeFlag){
		this.freeFlag = freeFlag;
	}
	/**价格*/
	public java.math.BigDecimal getPrice(){
		return this.price;
	}
	/**价格*/
	public void setPrice(java.math.BigDecimal price){
		this.price = price;
	}
	/**教师ID*/
	public String getTeacherId(){
		return this.teacherId;
	}
	/**教师ID*/
	public void setTeacherId(String teacherId){
		this.teacherId = teacherId;
	}
	/**课程描述*/
	public String getContent(){
		return this.content;
	}
	/**课程描述*/
	public void setContent(String content){
		this.content = content;
	}
	/**好评百分比*/
	public java.math.BigDecimal getVoteGood(){
		return this.voteGood;
	}
	/**好评百分比*/
	public void setVoteGood(java.math.BigDecimal voteGood){
		this.voteGood = voteGood;
	}
	/**中评百分比*/
	public java.math.BigDecimal getVoteNormal(){
		return this.voteNormal;
	}
	/**中评百分比*/
	public void setVoteNormal(java.math.BigDecimal voteNormal){
		this.voteNormal = voteNormal;
	}
	/**课程状态(1:连载中,2:已完成,3:已结束)*/
	public String getCourseStatus(){
		return this.courseStatus;
	}
	/**课程状态(1:连载中,2:已完成,3:已结束)*/
	public void setCourseStatus(String courseStatus){
		this.courseStatus = courseStatus;
	}
	/**开课时间起始*/
	public java.util.Date getStartDate(){
		return this.startDate;
	}
	/**开课时间起始*/
	public void setStartDate(java.util.Date startDate){
		this.startDate = startDate;
	}
	/**开课时间结束*/
	public java.util.Date getEndDate(){
		return this.endDate;
	}
	/**开课时间结束*/
	public void setEndDate(java.util.Date endDate){
		this.endDate = endDate;
	}
	/**学习时长*/
	public String getStudyDuration(){
		return this.studyDuration;
	}
	/**学习时长*/
	public void setStudyDuration(String studyDuration){
		this.studyDuration = studyDuration;
	}
	/**备注*/
	public String getSummary(){
		return this.summary;
	}
	/**备注*/
	public void setSummary(String summary){
		this.summary = summary;
	}
	/**状态（0：未发布，1：已发布）*/
	public String getStatus(){
		return this.status;
	}
	/**状态（0：未发布，1：已发布）*/
	public void setStatus(String status){
		this.status = status;
	}
	/**测验类型(1:测试，2:考试)*/
	public String getMeasureType(){
		return this.measureType;
	}
	/**测验类型(1:测试，2:考试)*/
	public void setMeasureType(String measureType){
		this.measureType = measureType;
	}
	/**考试开始时间*/
	public java.util.Date getEffectiveStartDate(){
		return this.effectiveStartDate;
	}
	/**考试开始时间*/
	public void setEffectiveStartDate(java.util.Date effectiveStartDate){
		this.effectiveStartDate = effectiveStartDate;
	}
	/**考试结束时间*/
	public java.util.Date getEffectiveEndDate(){
		return this.effectiveEndDate;
	}
	/**考试结束时间*/
	public void setEffectiveEndDate(java.util.Date effectiveEndDate){
		this.effectiveEndDate = effectiveEndDate;
	}
	/**考试时长(分钟)*/
	public java.math.BigDecimal getTimeDuration(){
		return this.timeDuration;
	}
	/**考试时长(分钟)*/
	public void setTimeDuration(java.math.BigDecimal timeDuration){
		this.timeDuration = timeDuration;
	}
	/**视频*/
	public String getVideoId(){
		return this.videoId;
	}
	/**视频*/
	public void setVideoId(String videoId){
		this.videoId = videoId;
	}
	/**课程须知*/
	public String getNotice(){
		return this.notice;
	}
	/**课程须知*/
	public void setNotice(String notice){
		this.notice = notice;
	}
	/**课程目标*/
	public String getTarget(){
		return this.target;
	}
	/**课程目标*/
	public void setTarget(String target){
		this.target = target;
	}
	/**课程范围*/
	public String getCrowdType(){
		return this.crowdType;
	}
	/**课程范围*/
	public void setCrowdType(String crowdType){
		this.crowdType = crowdType;
	}
	/**是否必修*/
	public String getFlagRequired(){
		return this.flagRequired;
	}
	/**是否必修*/
	public void setFlagRequired(String flagRequired){
		this.flagRequired = flagRequired;
	}
	/**是否可拖动进度*/
	public String getFlagSkipVideo(){
		return this.flagSkipVideo;
	}
	/**是否可拖动进度*/
	public void setFlagSkipVideo(String flagSkipVideo){
		this.flagSkipVideo = flagSkipVideo;
	}
	/**是否开启评论*/
	public String getFlagDiscuss(){
		return this.flagDiscuss;
	}
	/**是否开启评论*/
	public void setFlagDiscuss(String flagDiscuss){
		this.flagDiscuss = flagDiscuss;
	}
	/**是否开启问答*/
	public String getFlagQa(){
		return this.flagQa;
	}
	/**是否开启问答*/
	public void setFlagQa(String flagQa){
		this.flagQa = flagQa;
	}
	/**是否开启投票*/
	public String getFlagVote(){
		return this.flagVote;
	}
	/**是否开启投票*/
	public void setFlagVote(String flagVote){
		this.flagVote = flagVote;
	}
	/**是否开启笔记*/
	public String getFlagNote(){
		return this.flagNote;
	}
	/**是否开启笔记*/
	public void setFlagNote(String flagNote){
		this.flagNote = flagNote;
	}
	/**是否开启案例*/
	public String getFlagShare(){
		return this.flagShare;
	}
	/**是否开启案例*/
	public void setFlagShare(String flagShare){
		this.flagShare = flagShare;
	}
	/**是否开启问卷*/
	public String getFlagQuestionnaire(){
		return this.flagQuestionnaire;
	}
	/**是否开启问卷*/
	public void setFlagQuestionnaire(String flagQuestionnaire){
		this.flagQuestionnaire = flagQuestionnaire;
	}
	/**是否按照顺序学习*/
	public String getFlagOrderStudy(){
		return this.flagOrderStudy;
	}
	/**是否按照顺序学习*/
	public void setFlagOrderStudy(String flagOrderStudy){
		this.flagOrderStudy = flagOrderStudy;
	}
	/**学习人数*/
	public java.math.BigDecimal getNumStudy(){
		return this.numStudy;
	}
	/**学习人数*/
	public void setNumStudy(java.math.BigDecimal numStudy){
		this.numStudy = numStudy;
	}
	/**关注人数*/
	public java.math.BigDecimal getNumFavorite(){
		return this.numFavorite;
	}
	/**关注人数*/
	public void setNumFavorite(java.math.BigDecimal numFavorite){
		this.numFavorite = numFavorite;
	}
	/**浏览数*/
	public java.math.BigDecimal getNumView(){
		return this.numView;
	}
	/**浏览数*/
	public void setNumView(java.math.BigDecimal numView){
		this.numView = numView;
	}
	/**评价人数*/
	public java.math.BigDecimal getNumScoreUser(){
		return this.numScoreUser;
	}
	/**评价人数*/
	public void setNumScoreUser(java.math.BigDecimal numScoreUser){
		this.numScoreUser = numScoreUser;
	}
	/**TAG_ID*/
	public String getTagId(){
		return this.tagId;
	}
	/**TAG_ID*/
	public void setTagId(String tagId){
		this.tagId = tagId;
	}
	/**LINK_ID*/
	public String getLinkId(){
		return this.linkId;
	}
	/**LINK_ID*/
	public void setLinkId(String linkId){
		this.linkId = linkId;
	}
	public String getConditionUserId() {
		return conditionUserId;
	}
	public void setConditionUserId(String conditionUserId) {
		this.conditionUserId = conditionUserId;
	}
	public String getSessionUserStudy() {
		return sessionUserStudy;
	}
	public void setSessionUserStudy(String sessionUserStudy) {
		this.sessionUserStudy = sessionUserStudy;
	}

	@SuppressWarnings("rawtypes")
    public List getTagList() {
		return tagList;
	}
	@SuppressWarnings("rawtypes")
	public void setTagList(List tagList) {
		this.tagList = tagList;
	}
	public List<String> getTagIds() {
		return tagIds;
	}
	public void setTagIds(List<String> tagIds) {
		this.tagIds = tagIds;
	}
	public String getNumVote() {
		return numVote;
	}
	public void setNumVote(String numVote) {
		this.numVote = numVote;
	}
	public java.math.BigDecimal getVoteNotGood() {
		return voteNotGood;
	}
	public void setVoteNotGood(java.math.BigDecimal voteNotGood) {
		this.voteNotGood = voteNotGood;
	}
	public int getTotalVote() {
		return totalVote;
	}
	public void setTotalVote(int totalVote) {
		this.totalVote = totalVote;
	}
	public String getCourseStage() {
		return courseStage;
	}
	public void setCourseStage(String courseStage) {
		this.courseStage = courseStage;
	}
	public String getCourseDetail() {
		return courseDetail;
	}
	public void setCourseDetail(String courseDetail) {
		this.courseDetail = courseDetail;
	}
	
	private String shareFlag;
	private String shareComment;
	private Date shareTime;
	private String shareUserId;
	public String getShareFlag() {
		return shareFlag;
	}
	public void setShareFlag(String shareFlag) {
		this.shareFlag = shareFlag;
	}
	public String getShareComment() {
		return shareComment;
	}
	public void setShareComment(String shareComment) {
		this.shareComment = shareComment;
	}
	public Date getShareTime() {
		return shareTime;
	}
	public void setShareTime(Date shareTime) {
		this.shareTime = shareTime;
	}
	public String getShareUserId() {
		return shareUserId;
	}
	public void setShareUserId(String shareUserId) {
		this.shareUserId = shareUserId;
	}
	
}
