package com.cloudsoaring.web.institution.entity;

import java.util.Date;

import com.cloudsoaring.web.common.entity.BaseEntity;
/**
 * tb_institution_organ 组织结构基本信息表
 * @author ljl 2020/06/23
 *
 */
public class InstitutionOrganCanEntity extends BaseEntity {

	/** serialVersionUID*/
	private static final long serialVersionUID = 1L;
	 private String name;//名称
	 private String address_province;//省级编码',
	 private String address_city;//市区编码',
	 private String address_county;//县区编码',
	 private String address;//地址',
	 private int parent_id;//父类id',
	 private String school_type;//学校类型1:小学 2:初中 3:九年一贯制 4:完全中学 5:高级中学 6:十二年一贯制',
	 private int node_id;//节点类型名称(1省、2市、4市级教育局、3区、5区级教育局、6总校、7省教育厅、8行政科室、9教研组、10教学年级、11班级、12小组)',
	 private String organ_type;
	 private String tenant_id;//租户id',
	 private String sequence;//排序
	 private String[] node;//节点
	 private String update_time;//更新时间
     private String cheekArr;//
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress_province() {
		return address_province;
	}
	public void setAddress_province(String address_province) {
		this.address_province = address_province;
	}
	public String getAddress_city() {
		return address_city;
	}
	public void setAddress_city(String address_city) {
		this.address_city = address_city;
	}
	public String getAddress_county() {
		return address_county;
	}
	public void setAddress_county(String address_county) {
		this.address_county = address_county;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getParent_id() {
		return parent_id;
	}
	public void setParent_id(int parent_id) {
		this.parent_id = parent_id;
	}
	public String getSchool_type() {
		return school_type;
	}
	public void setSchool_type(String school_type) {
		this.school_type = school_type;
	}
	public int getNode_id() {
		return node_id;
	}
	public void setNode_id(int node_id) {
		this.node_id = node_id;
	}
	public String getOrgan_type() {
		return organ_type;
	}
	public void setOrgan_type(String organ_type) {
		this.organ_type = organ_type;
	}
	public String getTenant_id() {
		return tenant_id;
	}
	public void setTenant_id(String tenant_id) {
		this.tenant_id = tenant_id;
	}
	public String getSequence() {
		return sequence;
	}
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	public String[] getNode() {
		return node;
	}
	public void setNode(String[] node) {
		this.node = node;
	}
	public String getUpdate_time() {
		return update_time;
	}
	public void setUpdate_time(String update_time) {
		this.update_time = update_time;
	}
	public String getCheekArr() {
		return cheekArr;
	}
	public void setCheekArr(String cheekArr) {
		this.cheekArr = cheekArr;
	}
	 
	
	
}
