

package com.cloudsoaring.web.institution.entity;


import java.math.BigDecimal;
import java.util.List;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_measurement
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class MeasurementInstitutionEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**ID*/
	private String id;
	/**测验标题*/
	private String titile;
	/**来源*/
	private String  source;
	/**链接ID*/
	private String linkId;
	/**链接类型(COURSE:课程,CHAPTER:章节)*/
	private String linkType;
	/**测验类型(1:测试，2:考试)*/
	private String measureType;
	/**用户ID，非数据库字段*/
	private String userId;
	
	/**问卷编号*/
	private BigDecimal measureNum; 
	
	/**问卷发出份数*/
	private BigDecimal resultCnt;
	
	/**问卷收回份数*/
	private BigDecimal answerCnt;
	 /**问题列表*/
    private List<QuestionInstitutionEntity> questionList;  
    /**得分*/
    private java.math.BigDecimal goal;
    /**考试提交时间*/
    private String submitStartDate;
    private String submitEndDate;
    //姓名
    public String personName;
	/**课程标题*/
    private String courseTitle;
    private String insTitle;
    private String flag;
    private String flagName;
    /**学校名称*/
    private String insName;
    /**创建开始时间*/
    private String createStartDate;
    /**创建结束时间*/
    private String  createEndDate;
    //学校名称
    private String institutionName;
    
    public String getCourseTitle() {
        return courseTitle;
    }
    public void setCourseTitle(String courseTitle) {
        this.courseTitle = courseTitle;
    }
    public String getSubmitStartDate() {
        return submitStartDate;
    }
    public void setSubmitStartDate(String submitStartDate) {
        this.submitStartDate = submitStartDate;
    }
    public String getSubmitEndDate() {
        return submitEndDate;
    }
    public void setSubmitEndDate(String submitEndDate) {
        this.submitEndDate = submitEndDate;
    }
    public String getPersonName() {
        return personName;
    }
    public void setPersonName(String personName) {
        this.personName = personName;
    }
    public java.math.BigDecimal getGoal() {
        return goal;
    }
    public void setGoal(java.math.BigDecimal goal) {
        this.goal = goal;
    }
    public List<QuestionInstitutionEntity> getQuestionList() {
        return questionList;
    }
    public void setQuestionList(List<QuestionInstitutionEntity> questionList) {
        this.questionList = questionList;
    }
    public BigDecimal getMeasureNum() {
		return measureNum;
	}
	public void setMeasureNum(BigDecimal measureNum) {
		this.measureNum = measureNum;
	}
	public BigDecimal getResultCnt() {
		return resultCnt;
	}
	public void setResultCnt(BigDecimal resultCnt) {
		this.resultCnt = resultCnt;
	}
	public BigDecimal getAnswerCnt() {
		return answerCnt;
	}
	public void setAnswerCnt(BigDecimal answerCnt) {
		this.answerCnt = answerCnt;
	}
	/**总分设定*/
	private java.math.BigDecimal totalPoints;
	/**及格分数*/
	private java.math.BigDecimal standardPoints;
	/**测验标题*/
	public String getTitile(){
		return this.titile;
	}
	/**测验标题*/
	public void setTitile(String titile){
		this.titile = titile;
	}
	/**链接ID*/
	public String getLinkId(){
		return this.linkId;
	}
	/**链接ID*/
	public void setLinkId(String linkId){
		this.linkId = linkId;
	}
	/**链接类型(COURSE:课程,CHAPTER:章节)*/
	public String getLinkType(){
		return this.linkType;
	}
	/**链接类型(COURSE:课程,CHAPTER:章节)*/
	public void setLinkType(String linkType){
		this.linkType = linkType;
	}
	/**测验类型(1:测试，2:考试)*/
	public String getMeasureType(){
		return this.measureType;
	}
	/**测验类型(1:测试，2:考试)*/
	public void setMeasureType(String measureType){
		this.measureType = measureType;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
    public java.math.BigDecimal getTotalPoints() {
        return totalPoints;
    }
    public void setTotalPoints(java.math.BigDecimal totalPoints) {
        this.totalPoints = totalPoints;
    }
    public java.math.BigDecimal getStandardPoints() {
        return standardPoints;
    }
    public void setStandardPoints(java.math.BigDecimal standardPoints) {
        this.standardPoints = standardPoints;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getInsTitle() {
		return insTitle;
	}
	public void setInsTitle(String insTitle) {
		this.insTitle = insTitle;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getInsName() {
		return insName;
	}
	public void setInsName(String insName) {
		this.insName = insName;
	}
	public String getCreateStartDate() {
		return createStartDate;
	}
	public void setCreateStartDate(String createStartDate) {
		this.createStartDate = createStartDate;
	}
	public String getCreateEndDate() {
		return createEndDate;
	}
	public void setCreateEndDate(String createEndDate) {
		this.createEndDate = createEndDate;
	}
	public String getFlagName() {
		return flagName;
	}
	public void setFlagName(String flagName) {
		this.flagName = flagName;
	}
	public String getInstitutionName() {
		return institutionName;
	}
	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}
	
}
