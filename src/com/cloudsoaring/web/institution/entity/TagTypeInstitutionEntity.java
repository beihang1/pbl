package com.cloudsoaring.web.institution.entity;

import com.cloudsoaring.web.common.entity.BaseEntity;
/**
 * tb_tag_type_institution机构标签类对象
 * @author ljl
 *
 */
public class TagTypeInstitutionEntity extends BaseEntity {
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//标签类ID
	private String tagTypeId;
	//标签类名
	  private String tagTypeName;
	  //标签类型
	  private String tagClass;
	  //删除标记
	  private String flagDel;
	  //备注
	  private String remarks;
	  //是否锁定
	  private String flagLock;
	  //父标签类ID
	  private String parentId;
	  //业务类型：1课程 2计划
	  private String businessType;
	  
	  public String getParentId()
	  {
	    return this.parentId;
	  }
	  
	  public void setParentId(String parentId)
	  {
	    this.parentId = parentId;
	  }
	  
	  public String getBusinessType()
	  {
	    return this.businessType;
	  }
	  
	  public void setBusinessType(String businessType)
	  {
	    this.businessType = businessType;
	  }
	  
	  public String getTagTypeId()
	  {
	    return this.tagTypeId;
	  }
	  
	  public void setTagTypeId(String tagTypeId)
	  {
	    this.tagTypeId = tagTypeId;
	  }
	  
	  public String getTagTypeName()
	  {
	    return this.tagTypeName;
	  }
	  
	  public void setTagTypeName(String tagTypeName)
	  {
	    this.tagTypeName = tagTypeName;
	  }
	  
	  public String getTagClass()
	  {
	    return this.tagClass;
	  }
	  
	  public void setTagClass(String tagClass)
	  {
	    this.tagClass = tagClass;
	  }
	  
	  public String getFlagDel()
	  {
	    return this.flagDel;
	  }
	  
	  public void setFlagDel(String flagDel)
	  {
	    this.flagDel = flagDel;
	  }
	  
	  public String getRemarks()
	  {
	    return this.remarks;
	  }
	  
	  public void setRemarks(String remarks)
	  {
	    this.remarks = remarks;
	  }
	  
	  public String getFlagLock()
	  {
	    return this.flagLock;
	  }
	  
	  public void setFlagLock(String flagLock)
	  {
	    this.flagLock = flagLock;
	  }
}
