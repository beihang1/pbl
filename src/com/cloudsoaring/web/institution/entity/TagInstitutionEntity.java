package com.cloudsoaring.web.institution.entity;

import java.math.BigDecimal;

import com.cloudsoaring.web.common.entity.BaseEntity;
/**
 * tag_institution学校分类实体类
 * @author ljl
 *
 */
public class TagInstitutionEntity extends BaseEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	 //标签主键
	private String tagId;
	//分类类型id
	  private String tagTypeId;
	  //分类名称
	  private String tagName;
	  //分类标签下界值
	  private String tagMinValue;
	  //标签上界值
	  private String tagMaxValue;
	  //标识
	  private String flagDel;
	  //权重
	  private BigDecimal weight;
	  //父类id
	  private String parentId;
	  //图标
	  private String ico;
	  /**父子类型*/
	private String type;
	/**文件名*/
	private String fileName;
	private String flag;
	  public String getTagId()
	  {
	    return this.tagId;
	  }
	  
	  public void setTagId(String tagId)
	  {
	    this.tagId = tagId;
	  }
	  
	  public String getTagTypeId()
	  {
	    return this.tagTypeId;
	  }
	  
	  public void setTagTypeId(String tagTypeId)
	  {
	    this.tagTypeId = tagTypeId;
	  }
	  
	  public String getTagName()
	  {
	    return this.tagName;
	  }
	  
	  public void setTagName(String tagName)
	  {
	    this.tagName = tagName;
	  }
	  
	  public String getTagMinValue()
	  {
	    return this.tagMinValue;
	  }
	  
	  public void setTagMinValue(String tagMinValue)
	  {
	    this.tagMinValue = tagMinValue;
	  }
	  
	  public String getTagMaxValue()
	  {
	    return this.tagMaxValue;
	  }
	  
	  public void setTagMaxValue(String tagMaxValue)
	  {
	    this.tagMaxValue = tagMaxValue;
	  }
	  
	  public String getFlagDel()
	  {
	    return this.flagDel;
	  }
	  
	  public void setFlagDel(String flagDel)
	  {
	    this.flagDel = flagDel;
	  }
	  
	  public BigDecimal getWeight()
	  {
	    return this.weight;
	  }
	  
	  public void setWeight(BigDecimal weight)
	  {
	    this.weight = weight;
	  }
	  
	  public String getParentId()
	  {
	    return this.parentId;
	  }
	  
	  public void setParentId(String parentId)
	  {
	    this.parentId = parentId;
	  }
	  
	  public String getIco()
	  {
	    return this.ico;
	  }
	  
	  public void setIco(String ico)
	  {
	    this.ico = ico;
	  }

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}
	  
}
