package com.cloudsoaring.web.institution.entity;

import java.util.Date;

import com.cloudsoaring.web.common.entity.BaseEntity;
/**
 * t_distric 县级信息表
 * @author ljl 2020/06/24
 *
 */
public class TDistricEntity extends BaseEntity{

	/** serialVersionUID*/
	private static final long serialVersionUID = 1L;
	
	//编码
	private String discCode;
	//名称
	private String discName;
	//市级编码
	private String cityCode;
	//简称
	private String py;
	//排序
	private int sort;
	//更新时间
	private Date updateTime;

	public String getDiscCode() {
		return discCode;
	}
	public void setDiscCode(String discCode) {
		this.discCode = discCode;
	}
	public String getDiscName() {
		return discName;
	}
	public void setDiscName(String discName) {
		this.discName = discName;
	}
	public String getCityCode() {
		return cityCode;
	}
	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}
	public String getPy() {
		return py;
	}
	public void setPy(String py) {
		this.py = py;
	}
	public int getSort() {
		return sort;
	}
	public void setSort(int sort) {
		this.sort = sort;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}


}
