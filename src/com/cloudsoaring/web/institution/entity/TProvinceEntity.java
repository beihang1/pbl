package com.cloudsoaring.web.institution.entity;

import java.util.Date;

import com.cloudsoaring.web.common.entity.BaseEntity;
/**
 * t_province 省级信息
 * @author ljl 2020/06/24
 *
 */
public class TProvinceEntity extends BaseEntity{

	/** serialVersionUID*/
	private static final long serialVersionUID = 1L;
	//编码
	private String provCode;
	//名称
	private String provName;
	//简称
	private String py;
	//排序
	private int sort;
	//更新时间
	private Date updateTime;
	public String getProvCode() {
		return provCode;
	}
	public void setProvCode(String provCode) {
		this.provCode = provCode;
	}
	public String getProvName() {
		return provName;
	}
	public void setProvName(String provName) {
		this.provName = provName;
	}
	public String getPy() {
		return py;
	}
	public void setPy(String py) {
		this.py = py;
	}
	public int getSort() {
		return sort;
	}
	public void setSort(int sort) {
		this.sort = sort;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	
	
}
