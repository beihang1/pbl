package com.cloudsoaring.web.institution.service;

import org.springframework.stereotype.Service;

import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.institution.entity.TProvinceEntity;
import com.cloudsoaring.web.trainingplatform.constants.TpConstants;

/***
 * 省级管理相关的业务处理Service
 */
@Service
@SuppressWarnings("unchecked")
public class TProvinceService extends FileService implements TpConstants{
    /**
     * 分页查询省级信息
     * @param entity
     * @return
     */
	public ResultBean searchPage(TProvinceEntity entity){
		//分页查询t_province数据表
		return commonDao.searchList4Page(entity, INSTITUTION_PROVICE_SEARCH);
	}
	/**
	 * 保存
	 * @param entity
	 */
	public void insert(TProvinceEntity entity){
		//保存t_province数据表
		this.commonDao.insertData(entity,INSTITUTION_PROVICE_ADD);
	}
	/**
	 * 单条查询省级信息
	 * @param id
	 * @return
	 */
	public TProvinceEntity selectInstitutionManagerById(String id) {
		TProvinceEntity entity = new TProvinceEntity();
		entity.setId(id);
		//查询t_province数据表
		return commonDao.searchOneData(entity, INSTITUTION_PROVINCE_SEARCH_BY_ID);
	}
	/**
	 * 更新方法
	 * @param entity
	 */
	public void update(TProvinceEntity entity){
		//更新t_province数据表
		this.commonDao.updateData(entity, INSTITUTION_PROVINCE_UPDATE_BY_ID);
	}
}
