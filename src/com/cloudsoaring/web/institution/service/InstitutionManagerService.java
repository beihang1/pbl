package com.cloudsoaring.web.institution.service;

import org.springframework.stereotype.Service;

import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.institution.entity.InstitutionManagerEntity;
import com.cloudsoaring.web.trainingplatform.constants.TpConstants;

/***
 * 班级管理相关的业务处理Service
 */
@Service
@SuppressWarnings("unchecked")
public class InstitutionManagerService extends FileService implements TpConstants{
    /**
     * 分页查询tb_institution_manager
     * @param entity
     * @return
     */
	public ResultBean searchInstitutionManagerList(InstitutionManagerEntity entity){
		//返回分页查询结果
		return commonDao.searchList4Page(entity, INSTITUTION_MANAGER_SEARCH);
	}
	/**
	 * 保存tb_institution_manager信息
	 * @param entity
	 */
	public void insertInstitutionManager(InstitutionManagerEntity entity){
		//保存结构信息
		this.commonDao.insertData(entity,INSTITUTION_MANAGER_ADD);
	}
	/**
	 * 根据id查询tb_institution_manager信息
	 * @param id
	 * @return
	 */
	public InstitutionManagerEntity selectInstitutionManagerById(String id) {
		//实例化结构对象
		InstitutionManagerEntity entity = new InstitutionManagerEntity();
		//设置主键
		entity.setId(id);
		//查询结构信息
		return commonDao.searchOneData(entity, INSTITUTION_MANAGER_SEARCH_BY_ID);
	}
	/**
	 * 更新tb_institution_manager信息
	 * @param entity
	 */
	public void updateInstitutionManager(InstitutionManagerEntity entity){
		//更新机构信息
		this.commonDao.updateData(entity, INSTITUTION_MANAGER_UPDATE_BY_ID);
	}
}
