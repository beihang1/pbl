package com.cloudsoaring.web.institution.service;

import org.springframework.stereotype.Service;

import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.institution.entity.TCityEntity;
import com.cloudsoaring.web.trainingplatform.constants.TpConstants;

/***
 * 班级管理相关的业务处理Service
 */
@Service
@SuppressWarnings("unchecked")
public class TCityService extends FileService implements TpConstants{
     /**
      * 分页查询市级信息
      * @param entity
      * @return
      */
	public ResultBean searchPage(TCityEntity entity){
		//分页查询t_city数据表
		return commonDao.searchList4Page(entity, INSTITUTION_CITY_SEARCH);
	}
	/**
	 * 保存方法
	 * @param entity
	 */
	public void insert(TCityEntity entity){
		//保存t_city数据表
		this.commonDao.insertData(entity,INSTITUTION_CITY_ADD);
	}
	/**
	 * 单条数据查询方法
	 * @param id
	 * @return
	 */
	public TCityEntity selectInstitutionManagerById(String id) {
		TCityEntity entity = new TCityEntity();
		entity.setId(id);
		////单条查询t_city数据表
		return commonDao.searchOneData(entity, INSTITUTION_CITY_SEARCH_BY_ID);
	}
	/**
	 * 更新方法
	 * @param entity
	 */
	public void update(TCityEntity entity){
		//更新t_city数据表
		this.commonDao.updateData(entity, INSTITUTION_CITY_UPDATE_BY_ID);
	}
}
