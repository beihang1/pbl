package com.cloudsoaring.web.institution.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.exception.BusinessException;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.common.service.BaseService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.common.view.ResultBean.ErrorCode;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.constant.CourseStateConstants;
import com.cloudsoaring.web.course.entity.ChapterEntity;
import com.cloudsoaring.web.course.entity.CourseEntity;
import com.cloudsoaring.web.course.entity.MeasurementEntity;
import com.cloudsoaring.web.course.entity.QuestionEntity;
import com.cloudsoaring.web.course.entity.QuestionOptionEntity;
import com.cloudsoaring.web.course.entity.UserMeasuermentAnswerEntity;
import com.cloudsoaring.web.course.entity.UserMeasurementResultEntity;
import com.cloudsoaring.web.course.view.UserMeasurementResultView;
import com.cloudsoaring.web.institution.entity.InstitutionEntity;
import com.cloudsoaring.web.institution.entity.MeasurementInstitutionEntity;
import com.cloudsoaring.web.institution.entity.QuestionInstitutionEntity;
import com.cloudsoaring.web.institution.entity.QuestionOptionInstitutionEntity;
import com.cloudsoaring.web.institution.entity.TbInstitutionEvalutionEntity;


/**
 * 
 * @author ljl 前台测试信息
 */
@Service
public class MeasuermentInstitutionService extends BaseService implements CourseConstants {
	@Autowired
	private TagInstitutionEvalutionService courseService;
	/**
	 * 测试/考试提交
	 * 
	 * @param measureId 
	 * @param ids
	 * @param userAnswerIds
	 * @param submitType
	 * @return
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	public ResultBean submitMeasuerment(String linkType, String measureId,
			String ids, String userAnswerIds,
			@RequestParam(required = false) String submitType) {
		String userId = WebContext.getSessionUserId();
		//设置提交时间
		Date submitDate = new Date();
		if (StringUtil.isEmpty(submitType)) {
			submitType = "0";
		}
		//判断问题列表是否为空
		if (ids == null || userAnswerIds == null) {
			return ResultBean.error(getMessage("问题列表为空"));
		}
		//分割问题列表id
		String[] idArray = ids.split(",", -1);
		String[] userAnswerIdArray = userAnswerIds.split(";", -1);
		if (idArray.length == 0 || userAnswerIdArray.length == 0) {
			return ResultBean.error(getMessage("问题列表为空"));
		}
		//判断问题数与答案数是否一致
		if (idArray.length != userAnswerIdArray.length) {
			return ResultBean.error(getMessage("问题数与答案数不一致"));
		}
		//判断测试答题信息是否为空
		if (StringUtil.isEmpty(measureId)) {
			return ResultBean.error(getMessage("测试答题信息为空"));
		}
		//声明问题集合对象
		List<QuestionEntity> answerQuest = new ArrayList<QuestionEntity>();
		//循环遍历对象
		for (int i = 0, size = idArray.length; i < size; i++) {
			QuestionEntity question = new QuestionEntity();
			question.setId(idArray[i]);
			question.setMeasureId(measureId);
			question.setUserAnswerId(userAnswerIdArray[i]);
			answerQuest.add(question);
		}
		//判断问题列表是否为空
		if (answerQuest == null || answerQuest.size() == 0) {
			throw new BusinessException("问题列表为空！");
		}

		String measurementId = answerQuest.get(0).getMeasureId();

		// 校验测试 与 类型是否匹配
		MeasurementEntity measurementQuery = new MeasurementEntity();
		measurementQuery.setId(measurementId);
		measurementQuery.setLinkType(linkType);
		List<MeasurementEntity> measurementResult = search(measurementQuery);
		//测试信息是否存在
		if (measurementResult == null || measurementResult.size() == 0) {
			throw new BusinessException("测试信息不存在！");
		}

		MeasurementEntity measurementInfo = measurementResult.get(0);
		ExamInfo examInfo = getExamInfo(measurementInfo);
		// 如果是考试则进行时间检测
		Long residualTime = 0L;
		if (MEASUERMENT_TYPE_EXAM.equals(examInfo.measurementType)) {
			//查询考试信息
			residualTime = checkExam(submitDate, measurementId, userId,
					examInfo, EXAM_STATUS_END);
		}
		// 如果是系统自动，则不允许有剩余时间大于10秒
		if ((MEASUERMENT_SUBMIT_TYPE_PC_AUTO.equals(submitType) || MEASUERMENT_SUBMIT_TYPE_APP_AUTO
				.equals(submitType)) && residualTime > 10L) {
			//抛出异常
			throw new BusinessException("答题异常，请重新答题！");
		}
		// 测试题
		QuestionEntity dbQuestionQuery = new QuestionEntity();
		dbQuestionQuery.setMeasureId(measurementId);
		//设置排序字段
		dbQuestionQuery.setSortName("questionOrder");
		dbQuestionQuery.setSortOrder("asc");
		//查询问卷集合
		List<QuestionEntity> questionList = (List<QuestionEntity>) search(
				dbQuestionQuery, MEASUERMENT_SEARCH_QUESTION_LIST);
		Map<String, QuestionEntity> mapAnswerQuest = getMapQuestionListById(answerQuest);
		// 插入用户结果选项
		 BigDecimal goal = new BigDecimal(0);
		 BigDecimal totalNum = new BigDecimal(0);
		 //循环遍历
		for (QuestionEntity question : questionList) {
			QuestionEntity answerQuestion = mapAnswerQuest
					.get(question.getId());
			if (answerQuestion != null) {
				UserMeasuermentAnswerEntity answer = new UserMeasuermentAnswerEntity();
				answer.setQuestionId(answerQuestion.getId());
				answer.setOptionId(answerQuestion.getUserAnswerId());
				answer.setUserId(userId);
				// 删除旧的答案
				delete(answer, MEASUERMENT_ANSWER_DELETE);
				insert(answer, USER_MEASUERMENT_ANSWER_INSERT);
				// 插入答案历史表
				if (!MEASUERMENT_SUBMIT_TYPE_PC_PROCESS_SUBMIT
						.equals(submitType)
						&& !MEASUERMENT_SUBMIT_TYPE_APP_PROCESS_SUBMIT
								.equals(submitType)) {
					insert(answer, USER_MEASUREMENT_ANSWER_INSERTHIS);
				}
				 //计算回答是否正确
				 if(isRight(question.getReferenceAnswerId(), answerQuestion.getUserAnswerId())) {
					 goal= goal.add(question.getScore() != null ? question.getScore() : new BigDecimal(0));
					 totalNum =  totalNum.add(new BigDecimal(1));
				 }
			}

		}

		// 用户测验结果表
		UserMeasurementResultEntity resultInfo = new UserMeasurementResultEntity();
		resultInfo.setUserId(userId);
		resultInfo.setSubmitType(submitType);
		
		resultInfo.setMeasurementId(measurementId);
		// 计算总分
		resultInfo.setGoal(goal);
		// 考试
		if (MEASUERMENT_TYPE_EXAM.equals(examInfo.measurementType)) {
			// 过程提交不修改考试状态
			if (MEASUERMENT_SUBMIT_TYPE_PC_PROCESS_SUBMIT.equals(submitType)
					|| MEASUERMENT_SUBMIT_TYPE_APP_PROCESS_SUBMIT
							.equals(submitType)) {
				resultInfo.setExamStatus(EXAM_STATUS_START);
			} else {
				// 用户只需提交测试一次
				resultInfo.setExamStatus(EXAM_STATUS_END);
				resultInfo.setSubmitDate(new Date());
			}
			// 因为考试开始时已经插入UserMeasurementResult信息，故这里只做更新
			update(resultInfo);
			if (!MEASUERMENT_SUBMIT_TYPE_PC_PROCESS_SUBMIT.equals(submitType)
					&& !MEASUERMENT_SUBMIT_TYPE_APP_PROCESS_SUBMIT
							.equals(submitType)) {
				update(resultInfo, USER_MEASUREMENT_RESULT_UPDATEHIS);
			}
			// 普通测试
		} else {
			// 删除旧的答题
			delete(resultInfo, USER_MEASUERMENT_RESULT_DELETE);
			// 插入结果
			insert(resultInfo, USER_MEASUERMENT_RESULT_INSERT);

			// 插入结果
			insert(resultInfo, USER_MEASUREMENT_RESULT_INSERTHIS);
		}
		// 返回测试结果
		UserMeasurementResultView resultView = new UserMeasurementResultView();
		resultView.setGoal(resultInfo.getGoal());
		resultView.setTotalGoal(totalNum.toString());
		if (questionList != null) {
			resultView.setQuestionCount(questionList.size());
		} else {
			resultView.setQuestionCount(0);
		}
		// 如果是考试过程中的提交，不返回正确答案
		if (MEASUERMENT_SUBMIT_TYPE_PC_PROCESS_SUBMIT.equals(submitType)
				|| MEASUERMENT_SUBMIT_TYPE_APP_PROCESS_SUBMIT
						.equals(submitType)) {
			resultView.setGoal(new BigDecimal(0));
			resultView.setQuestionCount(0);
		}
		// 设置当前剩余时间
		if (MEASUERMENT_TYPE_EXAM.equals(examInfo.measurementType)) {
			resultView.setResidualTime(residualTime);
		}
		ResultBean result = new ResultBean();
		result.setData(resultView);
		//返回结果
		return result;
	}

	/**
	 * 获取考试的相关信息
	 * 
	 * @param dataM
	 * @param examInfo
	 */
	private ExamInfo getExamInfo(MeasurementEntity dataM) {
		ExamInfo examInfo = new ExamInfo();
		if (LINK_TYPE_COURSE.equals(dataM.getLinkType())) {
			CourseEntity queryCO = new CourseEntity();
			queryCO.setId(dataM.getLinkId());
			CourseEntity course = commonDao.searchOneData(queryCO,SEARCH_COURSE_BY_PK);
			if (COURSE_STATUS_NOT_PUBLIC.equals(course.getStatus())) {
				throw new BusinessException("测试管理课程已取消发布!");
			}
			examInfo.measurementType = course.getMeasureType();
			examInfo.measureType = course.getMeasureType();
			examInfo.measureStartDate = course.getEffectiveStartDate();
			examInfo.measureEndDate = course.getEffectiveEndDate();
			examInfo.measureDuration = course.getTimeDuration();
		} else {
			ChapterEntity queryCP = new ChapterEntity();
			queryCP.setId(dataM.getLinkId());
			ChapterEntity chapter = commonDao.searchOneData(queryCP,SEARCH_CHAPTER);
			if (!CHAPTER_STATUS_PUBLIC.equals(chapter.getStatus())) {
				throw new BusinessException("测试管理课程已取消发布!");
			}
			examInfo.measurementType = chapter.getMeasureType();
			examInfo.measureType = chapter.getMeasureType();
			examInfo.measureStartDate = chapter.getEffectiveStartDate();
			examInfo.measureEndDate = chapter.getEffectiveEndDate();
			examInfo.measureDuration = chapter.getTimeDuration();
		}
		return examInfo;
	}

	@SuppressWarnings("unused")
	private static class ExamInfo {
		//测试类型
		public String measurementType;
		public String measureType;
		//测试开始日期
		public Date measureStartDate;
		//测试结束日期
		public Date measureEndDate;
		//测试时长
		public BigDecimal measureDuration;
	}

	/**
	 * 检测是否能答题
	 * 
	 * @param measureId
	 * @param userId
	 * @param examInfo
	 * @param examHandle
	 *            考试操作（开始，还是结束）
	 * @return 返回结果 剩余时间
	 */
	@SuppressWarnings("unchecked")
	private Long checkExam(Date now, String measureId, String userId,
			ExamInfo examInfo, String examHandle) {
		//判断考试是否存在
		if (examInfo==null) {
			throw new BusinessException("无此考试信息！");
		}
		//判断考试对应的考试类型是否存在
		if (!MEASUERMENT_TYPE_EXAM.equals(examInfo.measurementType)) {
			throw new BusinessException("无此考试信息！");
		}
		//判断考试时间是否已到
		if (examInfo.measureStartDate.after(now)) {
			throw new BusinessException("考试时间还未到，请等待！");
		}
		//判断考试时间是否已过
		if (examInfo.measureEndDate.before(now)) {
			throw new BusinessException("考试时间已过，不能答题!");
		}

		// 用户结果表
		UserMeasurementResultEntity queryUMR2 = new UserMeasurementResultEntity();
		queryUMR2.setMeasurementId(measureId);
		queryUMR2.setUserId(userId);
		List<UserMeasurementResultEntity> listUMR2 = commonDao.searchList(queryUMR2,SQL_USER_MEASUERMENT_SELECT);
		// 第二次答题检测
		if (listUMR2 != null && listUMR2.size() != 0) {
			// 开始考试
			if (EXAM_STATUS_START.equals(examHandle)) {
				if (EXAM_STATUS_END.equals(listUMR2.get(0).getExamStatus())
						|| ((listUMR2.get(0).getStartDate())!=null && (listUMR2.get(0).getStartDate().getTime()
								+ examInfo.measureDuration.intValue() * 60
								* 1000 < now.getTime()))) {
					throw new BusinessException("您已参加过该考试或者考试已超时");
				}
				// 考试提交
			} else {
				if (EXAM_STATUS_END.equals(listUMR2.get(0).getExamStatus())) {
					throw new BusinessException("您已提交，请勿重复答题");
				}
				if ((listUMR2.get(0).getStartDate())!= null && listUMR2.get(0).getStartDate().getTime()
						+ examInfo.measureDuration.intValue() * 60 * 1000 + 30
						* 1000 < now.getTime()) {
					throw new BusinessException("考试已超时，禁止提交");
				}
			}
		}
		//返回时长
		if (listUMR2 == null || listUMR2.size() == 0) {
			return examInfo.measureDuration.intValue() * 60 * 1000 + 0L;
		}
		//返回结果
		return ((listUMR2.get(0).getStartDate() == null ) ? 0 : listUMR2.get(0).getStartDate().getTime()
				+ examInfo.measureDuration.intValue() * 60 * 1000 - now
					.getTime()) / 1000L;
	}

	/**
	 * 根据问题ID组成MAP
	 * 
	 * @param dbQuestionList
	 * @return 返回结果
	 */
	private Map<String, QuestionEntity> getMapQuestionListById(
			List<QuestionEntity> dbQuestionList) {
		//创建map对象
		Map<String, QuestionEntity> result = new HashMap<String, QuestionEntity>();
		if (dbQuestionList != null) {
			for (QuestionEntity q : dbQuestionList) {
				result.put(q.getId(), q);
			}
		}
		//返回结果
		return result;
	}

	/**
	 * 获取当前用户数据库中的总分,通过计算用户回答的问题
	 * 
	 * @param userId
	 * @param measurementId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Integer getCurrentGoal(String userId, String linkId, String linkType) {
		MeasurementEntity dataM = new MeasurementEntity();
		dataM.setUserId(userId);
		dataM.setLinkId(linkId);
		dataM.setLinkType(linkType);
		// 检索包含用户回答结果的问题清单
		List<QuestionEntity> questionList = (List<QuestionEntity>) search(
				dataM, MEASUERMENT_SEARCH_QUESTION_LIST_BY_MEASUREMENT);

		// 插入用户结果选项
		Integer goal = 0;
		for (QuestionEntity question : questionList) {
			// 计算回答是否正确
			if (isRight(question.getReferenceAnswerId(),
					question.getUserAnswerId())) {
				goal++;
			}
		}
		return goal;
	}

	/**
	 * 判断用户选项是否正确
	 * 
	 * @param question
	 * @param answerQuestion
	 * @return 返回结果
	 */
	private boolean isRight(String referenceAnswerId, String userAnswerId) {
		if (StringUtil.isEmpty(userAnswerId)) {
			return false;
		}
		String[] questionAnswers = referenceAnswerId.split(",");
		String[] userQuestionAnswers = userAnswerId.split(",");

		if (questionAnswers.length != userQuestionAnswers.length) {
			return false;
		}
		for (String questOne : questionAnswers) {
			// 是否存在正确答案
			boolean isHave = false;
			for (String userOne : userQuestionAnswers) {
				if (userOne.equals(questOne)) {
					isHave = true;
					break;
				}
			}
			if (!isHave) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 考试开始
	 * 
	 * @param linkId 链接ID，章节/课程ID
	 * @param linkType 章节/课程 CHAPTER/COURSE
	 * @return 返回结果
	 */
	public ResultBean examStart(String linkId,String linkType) {
		if (WebContext.isGuest()) {
			ResultBean result = new ResultBean().setStatus(false).setMessages("请重新登录后尝试");
			result.setErrorCode(ErrorCode.NOT_LOGIN);
			return result;
		}
		if (StringUtil.isEmpty(linkId)) {
			return new ResultBean().setStatus(false).setMessages("当前参数错误！");
		}
		if (StringUtil.isEmpty(linkType)) {
			return new ResultBean().setStatus(false).setMessages("链接类型为空！");
		}
		ResultBean result = ResultBean.success(MSG_S_SUBMIT);
		BigDecimal time = examStart(linkId,linkType, WebContext.getSessionUserId());
		result.setData(time);
		return result;
	}

	/**
	 * 开始考试,返回剩余秒
	 * 
	 * @param linkId 链接ID 测试/考试ID
	 * @param linkType 链接类型 测试/考试，1/2
	 * @param userId 当前登录用户
	 */
	@SuppressWarnings("unchecked")
	private BigDecimal examStart(String linkId,String linkType, String userId) {
		MeasurementEntity query = new MeasurementEntity();
		query.setLinkId(linkId);
		query.setLinkType(linkType);
		List<MeasurementEntity> listM = commonDao.searchList(query,MEASUREMENT_SELECT);
		if (listM == null || listM.size() == 0) {
			throw new BusinessException("无此测试信息！");
		}

		MeasurementEntity dataM = listM.get(0);
		String measureId = dataM.getId();
		ExamInfo examInfo = getExamInfo(dataM);

		Date now = new Date();
		checkExam(now, measureId, userId, examInfo,
				CourseStateConstants.EXAM_STATUS_START);

		// 用户结果表
		UserMeasurementResultEntity queryUMR = new UserMeasurementResultEntity();
		queryUMR.setMeasurementId(measureId);
		queryUMR.setUserId(userId);
		List<UserMeasurementResultEntity> listUMR = search(queryUMR);
		// 第一次答题
		if (listUMR == null || listUMR.size() == 0) {
			queryUMR.setGoal(new BigDecimal(0));
			queryUMR.setCreateDate(now);
			queryUMR.setStartDate(now);
			queryUMR.setCreateUser(userId);
			queryUMR.setExamStatus(CourseStateConstants.EXAM_STATUS_START);
			insert(queryUMR);
			// 考试历史数据表
			insert(queryUMR, USER_MEASUREMENT_RESULT_INSERTHIS);

			// 第一次答题，并加入课程
			if (CourseStateConstants.LINK_TYPE_COURSE.equals(dataM
					.getLinkType())) {
				courseService.joinCourse(linkId);
			}
			//
			return examInfo.measureDuration.multiply(new BigDecimal(60));

		}
		// 第二次答题
		UserMeasurementResultEntity umr = listUMR.get(0);
		long result = (umr.getStartDate() == null ) ? 0 : umr.getStartDate().getTime()
				+ examInfo.measureDuration.intValue() * 60 * 1000
				- now.getTime();
		// 剩余返回时间以秒
		return new BigDecimal(result / 1000);

	}
	
	/**
	 * 获取考试题
	 * @param linkId 链接ID，章节/课程ID
	 * @param linkType 章节/课程 CHAPTER/COURSE
	 * @return ResultBean data:List<QuestionEntity>结果 题目及选项
	 */
	public ResultBean getExamList(String linkId,String linkType){
		if(WebContext.isGuest()) {
			ResultBean error = new ResultBean().setStatus(false).setMessages("请重新登录后尝试");
			error.setErrorCode(ErrorCode.NOT_LOGIN);
			return error;
		}
		if(StringUtil.isEmpty(linkId)) {
			return new ResultBean().setStatus(false).setMessages("当前参数错误！");
		}
		ResultBean result = ResultBean.success(MSG_S_SUBMIT);
		List<QuestionEntity> questionList = examGetList(linkId,linkType, WebContext.getSessionUserId());
		result.setData(questionList);
		return result;
	}
	
	/**
	 * 获取考试题
	 * @param linkId 链接ID，章节/课程ID
	 * @param linkType 章节/课程 CHAPTER/COURSE
	 * @param sessionUserId 当前登录用户ID
	 * @return List<QuestionEntity> 题目及选项
	 */
	@SuppressWarnings("unchecked")
	private List<QuestionEntity> examGetList(String linkId,String linkType, String sessionUserId) {
		MeasurementEntity query = new MeasurementEntity();
		query.setLinkId(linkId);
		query.setLinkType(linkType);
		List<MeasurementEntity> listM = search(query);
		if(listM == null || listM.size() == 0)  {
			throw new BusinessException("无此测试信息！");
		}
		
		MeasurementEntity dataM = listM.get(0);
		String measureId = dataM.getId();
		ExamInfo examInfo = getExamInfo(dataM);
		checkExam(new Date(), measureId, sessionUserId, examInfo, CourseStateConstants.EXAM_STATUS_START);
		
		List<QuestionEntity> result = (List<QuestionEntity>) search(dataM, MEASUERMENT_SEARCH_QUESTION_LIST_BY_MEASUREMENT);
		
		for(QuestionEntity quest : result) {
			//抹去正确答案
			quest.setReferenceAnswerId(null);
			
			QuestionOptionEntity optionQuery = new QuestionOptionEntity();
			optionQuery.setQuestionId(quest.getId());
			List<QuestionOptionEntity> options = (List<QuestionOptionEntity>) search(optionQuery, MEASUERMENT_SEARCH_QUESTION_OPTIONS_LIST);
			quest.setOptions(options);
		}
		return result;
	}
	
	/**
	 * 得到用户的测试结果
	 * @param um
	 * @return 返回结果
	 */
	@SuppressWarnings("unchecked")
	public UserMeasurementResultEntity findUserExamResult(UserMeasurementResultEntity um){
		List<UserMeasurementResultEntity> list = commonDao.searchList(um, SQL_MEASUERMENT_USER_SEARCH);
		return (list!=null&&list.size()>0) ? list.get(0) : null;
	}
	
	/**
	 * 查找测试
	 * @param m
	 * @return 返回结果
	 */
	@SuppressWarnings("unchecked")
	public MeasurementEntity findMeasurement(MeasurementEntity m){
		List<MeasurementEntity> ms = commonDao.searchList(m, SQL_MEASUERMENT_SEARCH);
		return (ms!=null&&ms.size()>0) ? ms.get(0) : null;
	}
	/**
	 * 查找测试
	 * @param m
	 * @return 返回结果
	 */
	@SuppressWarnings("unchecked")
	public MeasurementInstitutionEntity findInstitutionMeasurement(MeasurementInstitutionEntity m){
		List<MeasurementInstitutionEntity> ms = commonDao.searchList(m, SQL_INSTITUTION_MEASUERMENT_SEARCH);
		return (ms!=null&&ms.size()>0) ? ms.get(0) : null;
	}
	/**
     * 删除用户的考试成绩
     * @param userId 用户ID
     * @param measuermentId 测试ID
     * @author adlay
     */
    public void deleteUserMeasuerment(String userId, String measuermentId){
        //删答案
        UserMeasuermentAnswerEntity uma = new UserMeasuermentAnswerEntity();
        uma.setUserId(userId);
        uma.setMeasuermentId(measuermentId);
        this.delete(uma, SQL_MEASUREMENT_DELETE_ANSWER_OF_USER);
        //删测试
        UserMeasurementResultEntity umr = new UserMeasurementResultEntity();
        umr.setUserId(userId);
        umr.setMeasurementId(measuermentId);
        this.delete(umr);
    }
    @SuppressWarnings("unchecked")
    public List<UserMeasurementResultEntity> searchUserMeasurementResultByEntiy(UserMeasurementResultEntity query) {
        return search(query,SQL_MEASUERMENT_USER_SEARCH);
    }
    /**
     * 根据参数获取测试信息（包含用户回答信息。不区分考试还是普通测试）
     * @param userId
     * @param linkId
     * @param linkType
     * @return 返回结果
     */
    @SuppressWarnings({ "unchecked", "unused" })
    public List<QuestionEntity> searchMeasuermentNormalAndExam(String userId,
            String measurementId) {
        //补充信息
        MeasurementEntity query = new MeasurementEntity();
        query.setId(measurementId);
        query = searchOneData(query);
        if(query == null) {
            return null;
        } 
        
        query.setUserId(userId);
        ExamInfo examInfo = getExamInfo(query);
        List<QuestionEntity> result = (List<QuestionEntity>) search(query, SQL_MEASUERMENT_SEARCH_QUESTION_LIST_BY_MEASUREMENT);
        
        for(QuestionEntity quest : result) {
            QuestionOptionEntity optionQuery = new QuestionOptionEntity();
            optionQuery.setQuestionId(quest.getId());
            List<QuestionOptionEntity> options = (List<QuestionOptionEntity>) search(optionQuery, MEASUERMENT_SEARCH_QUESTION_OPTIONS_LIST);
            quest.setOptions(options);
        }
        return result;
    }

	public MeasurementInstitutionEntity seachMeasureById(String id) {
		MeasurementInstitutionEntity entity = new MeasurementInstitutionEntity();
		entity.setLinkId(id);
		entity.setLinkType(LINK_TYPE_EVALUTION);
		entity.setMeasureType(MEASUERMENT_TYPE_QUESTIO);
		entity = commonDao.searchOneData(entity, SQL_SEACH_MEASURE_BY_EVALUTION_ID);
		List<QuestionInstitutionEntity> measurementResult = new ArrayList<QuestionInstitutionEntity>();
		measurementResult = (List<QuestionInstitutionEntity>) search(entity,
				MEASUERMENT_SEARCH_QUESTION_LIST_BY_MEASUREMENT_INSTITUTION);
		// 检索测试题中的选项
		for (QuestionInstitutionEntity quest : measurementResult) {
			QuestionOptionInstitutionEntity optionQuery = new QuestionOptionInstitutionEntity();
			optionQuery.setQuestionId(quest.getId());
			optionQuery.setSortName("sortOrder");
			optionQuery.setSortOrder("asc");
			List<QuestionOptionInstitutionEntity> options = (List<QuestionOptionInstitutionEntity>) search(optionQuery,
					MEASUERMENT_SEARCH_QUESTION_EVALUTION_OPTIONS_LIST);
			quest.setOptions(options);
		}
		if(!measurementResult.isEmpty()) {
			entity.setQuestionList(measurementResult);
		}
		return entity;
	}

	public void saveMeasure(MeasurementInstitutionEntity measure) {
		measure.setId(IDGenerator.genUID());
		measure.setMeasureType(MEASUERMENT_TYPE_QUESTIO);
		measure.setLinkType(LINK_TYPE_EVALUTION);
		if (isHasMeasure(measure) == 0) {
			commonDao.insertData(measure, MEASUREMENT_INSTITUTION_INSERT);
			CourseEntity entity = new CourseEntity();
			entity.setFlagQuestionnaire(FLAG);
			entity.setId(measure.getLinkId());
			commonDao.updateData(entity, SQL_UPDATE_FLAG_QUEATION_EVALUTION);
			// 保存问题
			if (measure.getQuestionList() != null) {
				insertQuestion(measure.getQuestionList(), measure.getId());
			}
		} else {
			throw new BusinessException("此课程下已经存在问卷，不允许添加！");
		}
		
	}
	/**
	    @SuppressWarnings({ "unchecked", "unused" })
    public List<QuestionEntity> searchMeasuermentNormalAndExam(String userId,
            String measurementId) {
        //补充信息
        MeasurementEntity query = new MeasurementEntity();
        query.setId(measurementId);
        query = searchOneData(query);
        if(query == null) {
            return null;
        } 
        
        query.setUserId(userId);
        ExamInfo examInfo = getExamInfo(query);
        List<QuestionEntity> result = (List<QuestionEntity>) search(query, SQL_MEASUERMENT_SEARCH_QUESTION_LIST_BY_MEASUREMENT);
        
        for(QuestionEntity quest : result) {
            QuestionOptionEntity optionQuery = new QuestionOptionEntity();
            optionQuery.setQuestionId(quest.getId());
            List<QuestionOptionEntity> options = (List<QuestionOptionEntity>) search(optionQuery, MEASUERMENT_SEARCH_QUESTION_OPTIONS_LIST);
            quest.setOptions(options);
        }
        return result;
    }

	public MeasurementInstitutionEntity seachMeasureById(String id) {
		MeasurementInstitutionEntity entity = new MeasurementInstitutionEntity();
		entity.setLinkId(id);
		entity.setLinkType(LINK_TYPE_EVALUTION);
		entity.setMeasureType(MEASUERMENT_TYPE_QUESTIO);
		entity = commonDao.searchOneData(entity, SQL_SEACH_MEASURE_BY_EVALUTION_ID);
		List<QuestionInstitutionEntity> measurementResult = new ArrayList<QuestionInstitutionEntity>();
		measurementResult = (List<QuestionInstitutionEntity>) search(entity,
				MEASUERMENT_SEARCH_QUESTION_LIST_BY_MEASUREMENT_INSTITUTION);
		// 检索测试题中的选项
		for (QuestionInstitutionEntity quest : measurementResult) {
			QuestionOptionInstitutionEntity optionQuery = new QuestionOptionInstitutionEntity();
			optionQuery.setQuestionId(quest.getId());
			optionQuery.setSortName("sortOrder");
			optionQuery.setSortOrder("asc");
			List<QuestionOptionInstitutionEntity> options = (List<QuestionOptionInstitutionEntity>) search(optionQuery,
					MEASUERMENT_SEARCH_QUESTION_EVALUTION_OPTIONS_LIST);
			quest.setOptions(options);
		}
		if(!measurementResult.isEmpty()) {
			entity.setQuestionList(measurementResult);
		}
		return entity;
	}

	public void saveMeasure(MeasurementInstitutionEntity measure) {
		measure.setId(IDGenerator.genUID());
		measure.setMeasureType(MEASUERMENT_TYPE_QUESTIO);
		measure.setLinkType(LINK_TYPE_EVALUTION);
		if (isHasMeasure(measure) == 0) {
			commonDao.insertData(measure, MEASUREMENT_INSTITUTION_INSERT);
			CourseEntity entity = new CourseEntity();
			entity.setFlagQuestionnaire(FLAG);
			entity.setId(measure.getLinkId());
			commonDao.updateData(entity, SQL_UPDATE_FLAG_QUEATION_EVALUTION);
			// 保存问题
			if (measure.getQuestionList() != null) {
				insertQuestion(measure.getQuestionList(), measure.getId());
			}
		} else {
			throw new BusinessException("此课程下已经存在问卷，不允许添加！");
		}
		
	}
	 */
	/**
	 * 问卷存在
	 * 
	 * @param measure
	 * @return
	 */
	public int isHasMeasure(MeasurementInstitutionEntity measure) {
		return commonDao.searchCount(measure, SQL_SEACH_ONE_IS_MEASURE);
	}
	/**
	 * 插入问题
	 * 
	 * @param newQuestionList
	 * @param measurementId
	 */
	public void insertQuestion(List<QuestionInstitutionEntity> newQuestionList, String measurementId) {
		QuestionEntity entity = new QuestionEntity();
		entity.setMeasureId(measurementId);
		Integer maxOrder = commonDao.searchOneData(entity, QUESTION_EVALUTION_MAX_ORDER);
		if (maxOrder == null) {
			maxOrder = 0;
		}
		Integer order = maxOrder + 1;
		for (QuestionInstitutionEntity questionData : newQuestionList) {
			questionData.setId(IDGenerator.genUID());
			questionData.setMeasureId(measurementId);
			questionData.setQuestionOrder(new BigDecimal(order));
			order++;
			// 参考答案序号,等选项保存后再保存问题
			int ii = 0;
			String referenceAnswerNo = null;
			if (questionData != null && StringUtil.isNotEmpty(questionData.getReferenceAnswerId())) {
				String[] ss = questionData.getReferenceAnswerId().split(",");
				for (String gg : ss) {
					ii = Integer.parseInt(gg) - 1;
					referenceAnswerNo += String.valueOf(ii) + ",";
				}
			}
			String referenceAnswerId = null;
			List<QuestionOptionInstitutionEntity> options = questionData.getOptions();
			if (options != null) {
				referenceAnswerId = insertQuestionOption(questionData, referenceAnswerNo);
			}
			questionData.setReferenceAnswerId(referenceAnswerId);
			insert(questionData);
		}
	}
	/**
	 public void insertQuestion(List<QuestionInstitutionEntity> newQuestionList, String measurementId) {
		QuestionEntity entity = new QuestionEntity();
		entity.setMeasureId(measurementId);
		Integer maxOrder = commonDao.searchOneData(entity, QUESTION_EVALUTION_MAX_ORDER);
		if (maxOrder == null) {
			maxOrder = 0;
		}
		Integer order = maxOrder + 1;
		for (QuestionInstitutionEntity questionData : newQuestionList) {
			questionData.setId(IDGenerator.genUID());
			questionData.setMeasureId(measurementId);
			questionData.setQuestionOrder(new BigDecimal(order));
			order++;
			// 参考答案序号,等选项保存后再保存问题
			int ii = 0;
			String referenceAnswerNo = null;
			if (questionData != null && StringUtil.isNotEmpty(questionData.getReferenceAnswerId())) {
				String[] ss = questionData.getReferenceAnswerId().split(",");
				for (String gg : ss) {
					ii = Integer.parseInt(gg) - 1;
					referenceAnswerNo += String.valueOf(ii) + ",";
				}
			}
			String referenceAnswerId = null;
			List<QuestionOptionInstitutionEntity> options = questionData.getOptions();
			if (options != null) {
				referenceAnswerId = insertQuestionOption(questionData, referenceAnswerNo);
			}
			questionData.setReferenceAnswerId(referenceAnswerId);
			insert(questionData);
		}
	} 
	 */
	/**
	 * 插入问题选项
	 * 
	 * @param newQuestion
	 * @param referenceAnswerNo
	 * @return 返回结果
	 */
	private String insertQuestionOption(QuestionInstitutionEntity newQuestion, String referenceAnswerNo) {

		List<QuestionOptionInstitutionEntity> options = newQuestion.getOptions();
		String referenceAnswerId = "";
		if (options != null) {
			// 保存问题选项
			int optionSize = options.size();

			for (int i = 0; i < optionSize; i++) {
				QuestionOptionInstitutionEntity option = options.get(i);
				option.setId(IDGenerator.genUID());
				option.setQuestionId(newQuestion.getId());
				option.setOptionOrder(new BigDecimal(i + ""));
				insert(option);
				if (StringUtil.isNotEmpty(referenceAnswerNo) && referenceAnswerNo.contains(i + "")) {
					referenceAnswerId = referenceAnswerId + "," + option.getId();
				}
			}
			if (StringUtil.isNotEmpty(referenceAnswerId)) {
				referenceAnswerId = referenceAnswerId.substring(1);
			}
		}
		return referenceAnswerId;
	}
    /**
                   * 更新问卷问题
     * @param measure
     */
	public void updateMeasure(MeasurementInstitutionEntity measure) {
		MeasurementInstitutionEntity oldEntity = seachMeasureByCourseId(measure.getLinkId());
		oldEntity.setTitile(measure.getTitile());
		commonDao.updateData(oldEntity, SQL_UPDATE_INSTITUTION_MEASURE);
		// 保存问题
		if (measure.getQuestionList() != null) {
			updateQuestion(measure.getId(), measure.getQuestionList(), oldEntity.getQuestionList());
		}
		
	}
	/**
	 * 
	 * 根据linkId查询问卷信息
	 * 
	 * @param id
	 * @return
	 */
	public MeasurementInstitutionEntity seachMeasureByCourseId(String id) {
		MeasurementInstitutionEntity entity = new MeasurementInstitutionEntity();
		entity.setLinkId(id);
		entity.setLinkType(LINK_TYPE_EVALUTION);
		entity.setMeasureType(MEASUERMENT_TYPE_QUESTIO);
		entity = commonDao.searchOneData(entity, SQL_SEACH_MEASURE_BY_EVALUTION_ID);
		List<QuestionInstitutionEntity> measurementResult = (List<QuestionInstitutionEntity>) search(entity,
				MEASUERMENT_SEARCH_QUESTION_LIST_BY_MEASUREMENT_INSTITUTION);
		// 检索测试题中的选项
		for (QuestionInstitutionEntity quest : measurementResult) {
			QuestionOptionEntity optionQuery = new QuestionOptionEntity();
			optionQuery.setQuestionId(quest.getId());
			optionQuery.setSortName("sortOrder");
			optionQuery.setSortOrder("asc");
			List<QuestionOptionInstitutionEntity> options = (List<QuestionOptionInstitutionEntity>) search(optionQuery,
					MEASUERMENT_SEARCH_QUESTION_EVALUTION_OPTIONS_LIST);
			quest.setOptions(options);
		}
		entity.setQuestionList(measurementResult);
		return entity;

	}
	/**
	 * 保存问题
	 * 
	 * @param measurementId   测试ID
	 * @param newQuestionList 新问题内容
	 * @param oldQuestionList 旧问题内容
	 */
	public void updateQuestion(String measurementId, List<QuestionInstitutionEntity> newQuestionList,
			List<QuestionInstitutionEntity> oldQuestionList) {
		Map<String, QuestionInstitutionEntity> questionMap = getMapQuestionByID1(newQuestionList);
		Map<String, QuestionInstitutionEntity> oldQuestionMap = getMapQuestionByID1(oldQuestionList);

		for (QuestionInstitutionEntity oldQuestion : oldQuestionList) {
			// 正确答案的ID
			String referenceAnswerId = "";
			// 问题为旧问题
			if (questionMap.containsKey(oldQuestion.getId())) {
				QuestionInstitutionEntity newQuestionEntity = questionMap.get(oldQuestion.getId());
				// 更新选项问题
				referenceAnswerId = updateOption(oldQuestion, newQuestionEntity.getReferenceAnswerId(),
						newQuestionEntity);
				oldQuestion.setQuestionType(newQuestionEntity.getQuestionType());
				oldQuestion.setScore(newQuestionEntity.getScore());
				oldQuestion.setContent(newQuestionEntity.getContent());
				oldQuestion.setQuestionOrder(newQuestionEntity.getQuestionOrder());
				oldQuestion.setReferenceAnswerId(referenceAnswerId);
				this.update(oldQuestion);

			} else {
				delete(oldQuestion);
				delete(oldQuestion, QUESTION_DELET_OPTION_BY_QUESTION);
				delete(oldQuestion, QUESTION_DELET_USER_ANSWER_BY_QUESTION);
			}
		}
		for (QuestionInstitutionEntity newQuestion : newQuestionList) {
			// 正确答案的ID
			String referenceAnswerId = "";
			// 问题为新问题
			if (!oldQuestionMap.containsKey(newQuestion.getId())) {

				newQuestion.setId(IDGenerator.genUID());
				newQuestion.setMeasureId(measurementId);
				referenceAnswerId = insertQuestionOption(newQuestion, newQuestion.getReferenceAnswerId());

				newQuestion.setReferenceAnswerId(referenceAnswerId);
				insert(newQuestion);
			}
		}
	}
	/**
	 public void updateQuestion(String measurementId, List<QuestionInstitutionEntity> newQuestionList,
			List<QuestionInstitutionEntity> oldQuestionList) {
		Map<String, QuestionInstitutionEntity> questionMap = getMapQuestionByID1(newQuestionList);
		Map<String, QuestionInstitutionEntity> oldQuestionMap = getMapQuestionByID1(oldQuestionList);

		for (QuestionInstitutionEntity oldQuestion : oldQuestionList) {
			// 正确答案的ID
			String referenceAnswerId = "";
			// 问题为旧问题
			if (questionMap.containsKey(oldQuestion.getId())) {
				QuestionInstitutionEntity newQuestionEntity = questionMap.get(oldQuestion.getId());
				// 更新选项问题
				referenceAnswerId = updateOption(oldQuestion, newQuestionEntity.getReferenceAnswerId(),
						newQuestionEntity);
				oldQuestion.setQuestionType(newQuestionEntity.getQuestionType());
				oldQuestion.setScore(newQuestionEntity.getScore());
				oldQuestion.setContent(newQuestionEntity.getContent());
				oldQuestion.setQuestionOrder(newQuestionEntity.getQuestionOrder());
				oldQuestion.setReferenceAnswerId(referenceAnswerId);
				this.update(oldQuestion);

			} else {
				delete(oldQuestion);
				delete(oldQuestion, QUESTION_DELET_OPTION_BY_QUESTION);
				delete(oldQuestion, QUESTION_DELET_USER_ANSWER_BY_QUESTION);
			}
		}
		for (QuestionInstitutionEntity newQuestion : newQuestionList) {
			// 正确答案的ID
			String referenceAnswerId = "";
			// 问题为新问题
			if (!oldQuestionMap.containsKey(newQuestion.getId())) {

				newQuestion.setId(IDGenerator.genUID());
				newQuestion.setMeasureId(measurementId);
				referenceAnswerId = insertQuestionOption(newQuestion, newQuestion.getReferenceAnswerId());

				newQuestion.setReferenceAnswerId(referenceAnswerId);
				insert(newQuestion);
			}
		}
	} 
	 */
	/**
	 * 以问题ID为MAP
	 * 
	 * @param questions
	 * @return 返回结果
	 */
	private Map<String, QuestionInstitutionEntity> getMapQuestionByID1(List<QuestionInstitutionEntity> questions) {
		Map<String, QuestionInstitutionEntity> result = new HashMap<String, QuestionInstitutionEntity>();
		// 如果问题列表为空，则已空的MAP返回
		if (questions != null) {
			for (QuestionInstitutionEntity question : questions) {
				result.put(question.getId(), question);
			}
		}
		return result;
	}
	/**
	 * 以问题ID为MAP
	 * 
	 * @param questions
	 * @return 返回结果
	 */
	private Map<String, QuestionInstitutionEntity> getMapQuestionByID(List<QuestionInstitutionEntity> questions) {
		Map<String, QuestionInstitutionEntity> result = new HashMap<String, QuestionInstitutionEntity>();
		// 如果问题列表为空，则已空的MAP返回
		if (questions != null) {
			for (QuestionInstitutionEntity question : questions) {
				result.put(question.getId(), question);
			}
		}
		return result;
	}
	/**
	 * 更新选项
	 * 
	 * @param oldQuestion
	 * @param referenceAnswerNo 正确答案序号
	 * @param newQuestion
	 * @return 正确答案的ID
	 */
	private String updateOption(QuestionInstitutionEntity oldQuestion, String referenceAnswerNo, QuestionInstitutionEntity newQuestion) {
		String referenceAnswerId = "";
		// 旧问题选项的ID
		// key为选项位置， value为选项ID
		Map<String, String> oldOptionIdMap = new HashMap<String, String>();
		if (oldQuestion.getOptions() != null) {
			for (int i = 0; i < oldQuestion.getOptions().size(); i++) {
				QuestionOptionInstitutionEntity option = oldQuestion.getOptions().get(i);
				oldOptionIdMap.put(i + "", option.getId());
			}
		}
		// 删除旧问题
		commonDao.deleteData(oldQuestion, QUESTION_DELET_OPTION_BY_QUESTION_INSTITUTION);

		// 插入新问题
		if (newQuestion.getOptions() != null) {
			for (int i = 0; i < newQuestion.getOptions().size(); i++) {
				QuestionOptionInstitutionEntity newOption = newQuestion.getOptions().get(i);
				// 新问题选项，相同位置的option必须沿用之前老的ID
				if (oldOptionIdMap.containsKey(i + "")) {
					newOption.setId(oldOptionIdMap.get(i + ""));
				} else {
					newOption.setId(IDGenerator.genDomID());
				}
				newOption.setQuestionId(oldQuestion.getId());
				insert(newOption);
				if (StringUtil.isNotEmpty(referenceAnswerNo) && referenceAnswerNo.contains(i + "")) {
					referenceAnswerId = referenceAnswerId + "," + newOption.getId();
				}
			}
		}
		if (StringUtil.isNotEmpty(referenceAnswerId)) {
			referenceAnswerId = referenceAnswerId.substring(1);
		}
		return referenceAnswerId;
	}
    /**
     * 查询考试结果
     * @param userId
     * @param measurementId
     * @return
     */
	public List<QuestionInstitutionEntity> searchMeasuermentInstitutionNormalAndExam(String userId,
			String measurementId) {
		 //补充信息
        MeasurementInstitutionEntity query = new MeasurementInstitutionEntity();
        query.setId(measurementId);
        query = searchOneData(query);
        if(query == null) {
            return null;
        } 
        //查询问题集合
        query.setUserId(userId);
        ExamInfo examInfo = getExamInfo(query);
        List<QuestionInstitutionEntity> result = (List<QuestionInstitutionEntity>) search(query, SQL_EVALUTION_MEASUERMENT_SEARCH_QUESTION_LIST_BY_MEASUREMENT);
        //循环遍历问题集合
        for(QuestionInstitutionEntity quest : result) {
            QuestionOptionInstitutionEntity optionQuery = new QuestionOptionInstitutionEntity();
            optionQuery.setQuestionId(quest.getId());
            List<QuestionOptionInstitutionEntity> options = (List<QuestionOptionInstitutionEntity>) search(optionQuery, MEASUERMENT_EVALUTION_SEARCH_QUESTION_OPTIONS_LIST);
            quest.setOptions(options);
        }
        //返回结果
        return result;
	}
	/**
	 * 查询考试信息
	 * @param dataM
	 * @return
	 */
	private ExamInfo getExamInfo(MeasurementInstitutionEntity dataM) {
		ExamInfo examInfo = new ExamInfo();
		//判断评价类型
		if (LINK_TYPE_EVALUTION.equals(dataM.getLinkType())) {
			TbInstitutionEvalutionEntity queryCO = new TbInstitutionEvalutionEntity();
			queryCO.setId(dataM.getLinkId());
			TbInstitutionEvalutionEntity course = commonDao.searchOneData(queryCO,SEARCH_INSTITUTION_EVALUTION_BY_PK);
			//判断评价是否取消发布
			if (COURSE_STATUS_NOT_PUBLIC.equals(course.getStatus())) {
				throw new BusinessException("测试管理课程已取消发布!");
			}
			examInfo.measurementType = course.getMeasureType();
			examInfo.measureType = course.getMeasureType();
			examInfo.measureStartDate = course.getEffectiveStartDate();
			examInfo.measureEndDate = course.getEffectiveEndDate();
			examInfo.measureDuration = course.getTimeDuration();
		} 
		return examInfo;
	}
   /**
    * 查询学校相关的问题集合
    * @param id
    * @return
    */
	public List<QuestionInstitutionEntity> searchQuestionList(String id) {
		//根据学校id查询问题信息
		QuestionInstitutionEntity dbQuestionQuery = new QuestionInstitutionEntity();
		dbQuestionQuery.setMeasureId(id);
		dbQuestionQuery.setSortName("questionOrder");
		dbQuestionQuery.setSortOrder("asc");
		List<QuestionInstitutionEntity> questionList = (List<QuestionInstitutionEntity>) search(dbQuestionQuery, MEASUERMENT_INSTITUTION_SEARCH_QUESTION_LIST);
		//判断集合是否为空
		if (questionList != null && questionList.size() != 0) {
			//循环遍历集合
			for (QuestionInstitutionEntity quest : questionList) {
				QuestionOptionInstitutionEntity optionQuery = new QuestionOptionInstitutionEntity();
				optionQuery.setQuestionId(quest.getId());
				//查询问题选项集合
				List<QuestionOptionInstitutionEntity> options = (List<QuestionOptionInstitutionEntity>) commonDao
						.searchList(optionQuery,MEASUERMENT_INSTITUTION_SEARCH_QUESTION_OPTIONS_LIST);
				quest.setOptions(options);
			}
		}
		//返回结果
		return questionList;
	}
    /**
     * 更新tb_measurement_institution数据表
     * @param measure
     */
	public void updateInstitutionMeasure(MeasurementInstitutionEntity measure) {
		MeasurementInstitutionEntity oldEntity = seachMeasureByCourseId(measure.getLinkId());
		oldEntity.setTitile(measure.getTitile());
		commonDao.updateData(oldEntity, SQL_UPDATE_INSTITUTION_MEASURE);
		// 保存问题
		if (measure.getQuestionList() != null) {
			updateInstitutionQuestion(measure.getId(), measure.getQuestionList(), oldEntity.getQuestionList());
		}
		
	}
	/**
	 * 更新问题表
	 * @param measurementId
	 * @param newQuestionList
	 * @param oldQuestionList
	 */
	public void updateInstitutionQuestion(String measurementId, List<QuestionInstitutionEntity> newQuestionList,
			List<QuestionInstitutionEntity> oldQuestionList) {
		//声明问题和选项对象
		Map<String, QuestionInstitutionEntity> questionMap = getMapQuestionByID1(newQuestionList);
		Map<String, QuestionInstitutionEntity> oldQuestionMap = getMapQuestionByID1(oldQuestionList);
        //循环遍历问题
		for (QuestionInstitutionEntity oldQuestion : oldQuestionList) {
			// 正确答案的ID
			String referenceAnswerId = "";
			// 问题为旧问题
			if (questionMap.containsKey(oldQuestion.getId())) {
				QuestionInstitutionEntity newQuestionEntity = questionMap.get(oldQuestion.getId());
				// 更新选项问题
				referenceAnswerId = updateOption(oldQuestion, newQuestionEntity.getReferenceAnswerId(),
						newQuestionEntity);
				oldQuestion.setQuestionType(newQuestionEntity.getQuestionType());
				oldQuestion.setScore(newQuestionEntity.getScore());
				oldQuestion.setContent(newQuestionEntity.getContent());
				oldQuestion.setQuestionOrder(newQuestionEntity.getQuestionOrder());
				oldQuestion.setReferenceAnswerId(referenceAnswerId);
				//更新问题信息
				this.update(oldQuestion);

			} else {
				//删除问题
				delete(oldQuestion);
				delete(oldQuestion, QUESTION_INSTITUTION_DELET_OPTION_BY_QUESTION);
				delete(oldQuestion, QUESTION_DELET_USER_ANSWER_BY_QUESTION);
			}
		}
		for (QuestionInstitutionEntity newQuestion : newQuestionList) {
			// 正确答案的ID
			String referenceAnswerId = "";
			// 问题为新问题
			if (!oldQuestionMap.containsKey(newQuestion.getId())) {
                //生成新问题主键
				newQuestion.setId(IDGenerator.genUID());
				//设置问题关联的学校
				newQuestion.setMeasureId(measurementId);
				//保存选项信息
				referenceAnswerId = insertQuestionOption(newQuestion, newQuestion.getReferenceAnswerId());

				newQuestion.setReferenceAnswerId(referenceAnswerId);
				//保存问题
				insert(newQuestion);
			}
		}
	}
	/**
	 public void updateInstitutionQuestion(String measurementId, List<QuestionInstitutionEntity> newQuestionList,
			List<QuestionInstitutionEntity> oldQuestionList) {
		//声明问题和选项对象
		Map<String, QuestionInstitutionEntity> questionMap = getMapQuestionByID1(newQuestionList);
		Map<String, QuestionInstitutionEntity> oldQuestionMap = getMapQuestionByID1(oldQuestionList);
        //循环遍历问题
		for (QuestionInstitutionEntity oldQuestion : oldQuestionList) {
			// 正确答案的ID
			String referenceAnswerId = "";
			// 问题为旧问题
			if (questionMap.containsKey(oldQuestion.getId())) {
				QuestionInstitutionEntity newQuestionEntity = questionMap.get(oldQuestion.getId());
				// 更新选项问题
				referenceAnswerId = updateOption(oldQuestion, newQuestionEntity.getReferenceAnswerId(),
						newQuestionEntity);
				oldQuestion.setQuestionType(newQuestionEntity.getQuestionType());
				oldQuestion.setScore(newQuestionEntity.getScore());
				oldQuestion.setContent(newQuestionEntity.getContent());
				oldQuestion.setQuestionOrder(newQuestionEntity.getQuestionOrder());
				oldQuestion.setReferenceAnswerId(referenceAnswerId);
				//更新问题信息
				this.update(oldQuestion);

			} else {
				//删除问题
				delete(oldQuestion);
				delete(oldQuestion, QUESTION_INSTITUTION_DELET_OPTION_BY_QUESTION);
				delete(oldQuestion, QUESTION_DELET_USER_ANSWER_BY_QUESTION);
			}
		}
		for (QuestionInstitutionEntity newQuestion : newQuestionList) {
			// 正确答案的ID
			String referenceAnswerId = "";
			// 问题为新问题
			if (!oldQuestionMap.containsKey(newQuestion.getId())) {
                //生成新问题主键
				newQuestion.setId(IDGenerator.genUID());
				//设置问题关联的学校
				newQuestion.setMeasureId(measurementId);
				//保存选项信息
				referenceAnswerId = insertQuestionOption(newQuestion, newQuestion.getReferenceAnswerId());

				newQuestion.setReferenceAnswerId(referenceAnswerId);
				//保存问题
				insert(newQuestion);
			}
		}
	}
	 public void updateInstitutionQuestion1(String measurementId, List<QuestionInstitutionEntity> newQuestionList,
			List<QuestionInstitutionEntity> oldQuestionList) {
		//声明问题和选项对象
		Map<String, QuestionInstitutionEntity> questionMap = getMapQuestionByID1(newQuestionList);
		Map<String, QuestionInstitutionEntity> oldQuestionMap = getMapQuestionByID1(oldQuestionList);
        //循环遍历问题
		for (QuestionInstitutionEntity oldQuestion : oldQuestionList) {
			// 正确答案的ID
			String referenceAnswerId = "";
			// 问题为旧问题
			if (questionMap.containsKey(oldQuestion.getId())) {
				QuestionInstitutionEntity newQuestionEntity = questionMap.get(oldQuestion.getId());
				// 更新选项问题
				referenceAnswerId = updateOption(oldQuestion, newQuestionEntity.getReferenceAnswerId(),
						newQuestionEntity);
				oldQuestion.setQuestionType(newQuestionEntity.getQuestionType());
				oldQuestion.setScore(newQuestionEntity.getScore());
				oldQuestion.setContent(newQuestionEntity.getContent());
				oldQuestion.setQuestionOrder(newQuestionEntity.getQuestionOrder());
				oldQuestion.setReferenceAnswerId(referenceAnswerId);
				//更新问题信息
				this.update(oldQuestion);

			} else {
				//删除问题
				delete(oldQuestion);
				delete(oldQuestion, QUESTION_INSTITUTION_DELET_OPTION_BY_QUESTION);
				delete(oldQuestion, QUESTION_DELET_USER_ANSWER_BY_QUESTION);
			}
		}
		for (QuestionInstitutionEntity newQuestion : newQuestionList) {
			// 正确答案的ID
			String referenceAnswerId = "";
			// 问题为新问题
			if (!oldQuestionMap.containsKey(newQuestion.getId())) {
                //生成新问题主键
				newQuestion.setId(IDGenerator.genUID());
				//设置问题关联的学校
				newQuestion.setMeasureId(measurementId);
				//保存选项信息
				referenceAnswerId = insertQuestionOption(newQuestion, newQuestion.getReferenceAnswerId());

				newQuestion.setReferenceAnswerId(referenceAnswerId);
				//保存问题
				insert(newQuestion);
			}
		}
	}
	 public void updateInstitutionQuestion2(String measurementId, List<QuestionInstitutionEntity> newQuestionList,
			List<QuestionInstitutionEntity> oldQuestionList) {
		//声明问题和选项对象
		Map<String, QuestionInstitutionEntity> questionMap = getMapQuestionByID1(newQuestionList);
		Map<String, QuestionInstitutionEntity> oldQuestionMap = getMapQuestionByID1(oldQuestionList);
        //循环遍历问题
		for (QuestionInstitutionEntity oldQuestion : oldQuestionList) {
			// 正确答案的ID
			String referenceAnswerId = "";
			// 问题为旧问题
			if (questionMap.containsKey(oldQuestion.getId())) {
				QuestionInstitutionEntity newQuestionEntity = questionMap.get(oldQuestion.getId());
				// 更新选项问题
				referenceAnswerId = updateOption(oldQuestion, newQuestionEntity.getReferenceAnswerId(),
						newQuestionEntity);
				oldQuestion.setQuestionType(newQuestionEntity.getQuestionType());
				oldQuestion.setScore(newQuestionEntity.getScore());
				oldQuestion.setContent(newQuestionEntity.getContent());
				oldQuestion.setQuestionOrder(newQuestionEntity.getQuestionOrder());
				oldQuestion.setReferenceAnswerId(referenceAnswerId);
				//更新问题信息
				this.update(oldQuestion);

			} else {
				//删除问题
				delete(oldQuestion);
				delete(oldQuestion, QUESTION_INSTITUTION_DELET_OPTION_BY_QUESTION);
				delete(oldQuestion, QUESTION_DELET_USER_ANSWER_BY_QUESTION);
			}
		}
		for (QuestionInstitutionEntity newQuestion : newQuestionList) {
			// 正确答案的ID
			String referenceAnswerId = "";
			// 问题为新问题
			if (!oldQuestionMap.containsKey(newQuestion.getId())) {
                //生成新问题主键
				newQuestion.setId(IDGenerator.genUID());
				//设置问题关联的学校
				newQuestion.setMeasureId(measurementId);
				//保存选项信息
				referenceAnswerId = insertQuestionOption(newQuestion, newQuestion.getReferenceAnswerId());

				newQuestion.setReferenceAnswerId(referenceAnswerId);
				//保存问题
				insert(newQuestion);
			}
		}
	}
	 public void updateInstitutionQuestion3(String measurementId, List<QuestionInstitutionEntity> newQuestionList,
			List<QuestionInstitutionEntity> oldQuestionList) {
		//声明问题和选项对象
		Map<String, QuestionInstitutionEntity> questionMap = getMapQuestionByID1(newQuestionList);
		Map<String, QuestionInstitutionEntity> oldQuestionMap = getMapQuestionByID1(oldQuestionList);
        //循环遍历问题
		for (QuestionInstitutionEntity oldQuestion : oldQuestionList) {
			// 正确答案的ID
			String referenceAnswerId = "";
			// 问题为旧问题
			if (questionMap.containsKey(oldQuestion.getId())) {
				QuestionInstitutionEntity newQuestionEntity = questionMap.get(oldQuestion.getId());
				// 更新选项问题
				referenceAnswerId = updateOption(oldQuestion, newQuestionEntity.getReferenceAnswerId(),
						newQuestionEntity);
				oldQuestion.setQuestionType(newQuestionEntity.getQuestionType());
				oldQuestion.setScore(newQuestionEntity.getScore());
				oldQuestion.setContent(newQuestionEntity.getContent());
				oldQuestion.setQuestionOrder(newQuestionEntity.getQuestionOrder());
				oldQuestion.setReferenceAnswerId(referenceAnswerId);
				//更新问题信息
				this.update(oldQuestion);

			} else {
				//删除问题
				delete(oldQuestion);
				delete(oldQuestion, QUESTION_INSTITUTION_DELET_OPTION_BY_QUESTION);
				delete(oldQuestion, QUESTION_DELET_USER_ANSWER_BY_QUESTION);
			}
		}
		for (QuestionInstitutionEntity newQuestion : newQuestionList) {
			// 正确答案的ID
			String referenceAnswerId = "";
			// 问题为新问题
			if (!oldQuestionMap.containsKey(newQuestion.getId())) {
                //生成新问题主键
				newQuestion.setId(IDGenerator.genUID());
				//设置问题关联的学校
				newQuestion.setMeasureId(measurementId);
				//保存选项信息
				referenceAnswerId = insertQuestionOption(newQuestion, newQuestion.getReferenceAnswerId());

				newQuestion.setReferenceAnswerId(referenceAnswerId);
				//保存问题
				insert(newQuestion);
			}
		}
	}
	 public void updateInstitutionQuestion5(String measurementId, List<QuestionInstitutionEntity> newQuestionList,
			List<QuestionInstitutionEntity> oldQuestionList) {
		//声明问题和选项对象
		Map<String, QuestionInstitutionEntity> questionMap = getMapQuestionByID1(newQuestionList);
		Map<String, QuestionInstitutionEntity> oldQuestionMap = getMapQuestionByID1(oldQuestionList);
        //循环遍历问题
		for (QuestionInstitutionEntity oldQuestion : oldQuestionList) {
			// 正确答案的ID
			String referenceAnswerId = "";
			// 问题为旧问题
			if (questionMap.containsKey(oldQuestion.getId())) {
				QuestionInstitutionEntity newQuestionEntity = questionMap.get(oldQuestion.getId());
				// 更新选项问题
				referenceAnswerId = updateOption(oldQuestion, newQuestionEntity.getReferenceAnswerId(),
						newQuestionEntity);
				oldQuestion.setQuestionType(newQuestionEntity.getQuestionType());
				oldQuestion.setScore(newQuestionEntity.getScore());
				oldQuestion.setContent(newQuestionEntity.getContent());
				oldQuestion.setQuestionOrder(newQuestionEntity.getQuestionOrder());
				oldQuestion.setReferenceAnswerId(referenceAnswerId);
				//更新问题信息
				this.update(oldQuestion);

			} else {
				//删除问题
				delete(oldQuestion);
				delete(oldQuestion, QUESTION_INSTITUTION_DELET_OPTION_BY_QUESTION);
				delete(oldQuestion, QUESTION_DELET_USER_ANSWER_BY_QUESTION);
			}
		}
		for (QuestionInstitutionEntity newQuestion : newQuestionList) {
			// 正确答案的ID
			String referenceAnswerId = "";
			// 问题为新问题
			if (!oldQuestionMap.containsKey(newQuestion.getId())) {
                //生成新问题主键
				newQuestion.setId(IDGenerator.genUID());
				//设置问题关联的学校
				newQuestion.setMeasureId(measurementId);
				//保存选项信息
				referenceAnswerId = insertQuestionOption(newQuestion, newQuestion.getReferenceAnswerId());

				newQuestion.setReferenceAnswerId(referenceAnswerId);
				//保存问题
				insert(newQuestion);
			}
		}
	}
	public void updateInstitutionQuestion6(String measurementId, List<QuestionInstitutionEntity> newQuestionList,
			List<QuestionInstitutionEntity> oldQuestionList) {
		//声明问题和选项对象
		Map<String, QuestionInstitutionEntity> questionMap = getMapQuestionByID1(newQuestionList);
		Map<String, QuestionInstitutionEntity> oldQuestionMap = getMapQuestionByID1(oldQuestionList);
        //循环遍历问题
		for (QuestionInstitutionEntity oldQuestion : oldQuestionList) {
			// 正确答案的ID
			String referenceAnswerId = "";
			// 问题为旧问题
			if (questionMap.containsKey(oldQuestion.getId())) {
				QuestionInstitutionEntity newQuestionEntity = questionMap.get(oldQuestion.getId());
				// 更新选项问题
				referenceAnswerId = updateOption(oldQuestion, newQuestionEntity.getReferenceAnswerId(),
						newQuestionEntity);
				oldQuestion.setQuestionType(newQuestionEntity.getQuestionType());
				oldQuestion.setScore(newQuestionEntity.getScore());
				oldQuestion.setContent(newQuestionEntity.getContent());
				oldQuestion.setQuestionOrder(newQuestionEntity.getQuestionOrder());
				oldQuestion.setReferenceAnswerId(referenceAnswerId);
				//更新问题信息
				this.update(oldQuestion);

			} else {
				//删除问题
				delete(oldQuestion);
				delete(oldQuestion, QUESTION_INSTITUTION_DELET_OPTION_BY_QUESTION);
				delete(oldQuestion, QUESTION_DELET_USER_ANSWER_BY_QUESTION);
			}
		}
		for (QuestionInstitutionEntity newQuestion : newQuestionList) {
			// 正确答案的ID
			String referenceAnswerId = "";
			// 问题为新问题
			if (!oldQuestionMap.containsKey(newQuestion.getId())) {
                //生成新问题主键
				newQuestion.setId(IDGenerator.genUID());
				//设置问题关联的学校
				newQuestion.setMeasureId(measurementId);
				//保存选项信息
				referenceAnswerId = insertQuestionOption(newQuestion, newQuestion.getReferenceAnswerId());

				newQuestion.setReferenceAnswerId(referenceAnswerId);
				//保存问题
				insert(newQuestion);
			}
		}
	}
	 */

}
