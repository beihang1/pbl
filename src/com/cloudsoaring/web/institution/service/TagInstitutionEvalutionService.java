package com.cloudsoaring.web.institution.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.exception.BusinessException;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.FileEntity;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.bus.service.MessageService;
import com.cloudsoaring.web.common.constant.Constants;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.constant.CourseDbConstants;
import com.cloudsoaring.web.course.constant.CourseStateConstants;
import com.cloudsoaring.web.course.entity.ChapterEntity;
import com.cloudsoaring.web.course.entity.CourseEntity;
import com.cloudsoaring.web.course.entity.CourseTeamEntity;
import com.cloudsoaring.web.course.entity.CourseTeamUserEntity;
import com.cloudsoaring.web.course.entity.MeasurementEntity;
import com.cloudsoaring.web.course.entity.QuestionEntity;
import com.cloudsoaring.web.course.entity.QuestionOptionEntity;
import com.cloudsoaring.web.course.entity.TagLinkEntity;
import com.cloudsoaring.web.course.entity.UserCourseEntity;
import com.cloudsoaring.web.course.entity.UserLogEntity;
import com.cloudsoaring.web.course.entity.UserOperEntity;
import com.cloudsoaring.web.course.service.ChapterService;
import com.cloudsoaring.web.course.view.CourseManagerView;
import com.cloudsoaring.web.course.view.CourseView;
import com.cloudsoaring.web.course.view.TagView;
import com.cloudsoaring.web.course.view.TeacherView;
import com.cloudsoaring.web.course.view.UserView;
import com.cloudsoaring.web.institution.entity.TagLinkInstitutionEntity;
import com.cloudsoaring.web.pointmanager.constant.PointStateConstants;
import com.cloudsoaring.web.pointmanager.service.PointManagerService;
import com.cloudsoaring.web.points.constant.PointConstants;
import com.cloudsoaring.web.points.entity.PointTypeEntity;
import com.cloudsoaring.web.points.entity.UserPoint;
import com.cloudsoaring.web.points.service.PointService;
import com.cloudsoaring.web.taxonomy.entity.TbInstitutionEvalutionManagerView;
import com.cloudsoaring.web.trainingplatform.constants.TpDbConstants;
import com.cloudsoaring.web.trainingplatform.entity.TpUserEntity;
import com.cloudsoaring.web.trainingplatform.utils.TpConfigUtil;

/***
 * 模块相关的业务处理Service
 * 
 * @author ljl
 *
 */
@Service
@SuppressWarnings("unchecked")
public class TagInstitutionEvalutionService extends FileService implements CourseConstants {

	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	@Autowired
	private PointService pointService;
	@Autowired
	private PointManagerService pointManagerService;
	/** 消息处理Service */
	@Autowired
	private MessageService messageService;


	

	/**
	 * 添加页面的标签列表 searchTagList
	 */
	public List<TagView> searchTagList(TagView tagQuery) {
		// tagQuery.setTagTypeName(CourseMsgNames.TAG_TYPE_FIRST);
		// tagQuery.setTagTypeName("方向");
		tagQuery.setTagTypeId(TAG_TYPE_ID_DIRECTIVE_COURSE);
		tagQuery.setBusinessType("1");
		return commonDao.searchList(tagQuery, SQL_SEARCH_FIRST_TAG_TYPE);
	}

	/**
	 * 添加页面的标签列表 searchTagList
	 */
	public List<TagView> searchTagList2(TagView tagQuery) {
		//tagQuery.setTagTypeName(CourseMsgNames.TAG_TYPE_FIRST);
		//tagQuery.setTagTypeName("学段");
		tagQuery.setTagTypeId("3");
		tagQuery.setBusinessType("2");
		return commonDao.searchList(tagQuery, SQL_SEARCH_FIRST_TAG_TYPE);
	}
	
	/**
	 * 添加页面的标签列表
	 */
	public List<TagView> searchTagSecondList(TagView tagQuery) {
		// tagQuery.setTagTypeName(CourseMsgNames.TAG_TYPE_SECOND);
		// tagQuery.setTagTypeName("分类");
		tagQuery.setTagTypeId(TAG_TYPE_ID_CATAGORY_COURSE);
		tagQuery.setBusinessType("1");
		return commonDao.searchList(tagQuery, SQL_SEARCH_FIRST_TAG_TYPE);
	}

	/**
	 * 添加页面的标签列表
	 */
	public List<TagView> searchTagSecondList2(TagView tagQuery) {
		// tagQuery.setTagTypeName(CourseMsgNames.TAG_TYPE_SECOND);
		// tagQuery.setTagTypeName("分类");
		tagQuery.setTagTypeId("4");
		tagQuery.setBusinessType("2");
		return commonDao.searchList(tagQuery, SQL_SEARCH_FIRST_TAG_TYPE);
	}
	
	/**
	 * 添加页面的标签列表：二级标签 searchTagList
	 */
	public List<TagView> searchSecondTagList(String id) {
		if(id != null && !"".equals(id)) {
			TagView tagQuery = new TagView();
			tagQuery.setParentId(id);
			List<TagView> list = new ArrayList<TagView>();
			// tagQuery.setTagTypeName("分类");
			if (id == null) {
				return list;
			} else {
				tagQuery.setTagTypeId(TAG_TYPE_ID_CATAGORY_COURSE);
				tagQuery.setBusinessType("1");
				return commonDao.searchList(tagQuery, SQL_SEARCH_SECOND_TAG_TYPE);
			}
		}else {
			return null;
		}
	}

	public List<TagView> searchSecondTagList2(String id) {
		TagView tagQuery = new TagView();
		tagQuery.setParentId(id);
		List<TagView> list = new ArrayList<TagView>();
		// tagQuery.setTagTypeName("分类");
		if (id == null) {
			return list;
		} else {
			tagQuery.setTagTypeId("4");
			tagQuery.setBusinessType("2");
			return commonDao.searchList(tagQuery, SQL_SEARCH_SECOND_TAG_TYPE);
		}

	}



	/**
	 * 课程详情页面，用户点击体验课程/加入课程，若用户是第一次点击，则创建一条用户课程数据
	 * 
	 * @param courseId 课程id
	 * @return 是否加入课程成功
	 * @author WUXIAOXIANG
	 */
	public ResultBean joinCourse(String courseId) {

		ResultBean result = new ResultBean();
		// 课程id为空
		assertNotEmpty(courseId, MSG_E_CID_NULL);
		// 课程不存在
		assertExist(courseId, SQL_IS_COURSE_EXIST, MSG_E_COURSE_NOT_EXIST);
		// 判断用户是否登录
		assertNotGuest();

		ResultBean course = searchCourseDetail(courseId);
		CourseView courseView = (CourseView) course.getData();
		String userId = WebContext.getSessionUserId();

		CourseView query = new CourseView();
		query.setId(courseId);
		query.setUserId(userId);
		assertNotExist(query, SQL_IS_COURSE_JOIN, "课程已加入");

		if (FLAG_FREE.equals(courseView.getFreeFlag()) || courseView.getPrice() == null
				|| 0 == courseView.getPrice().intValue()) {
			// 加入免费
			joinUserCourse(courseId);
			// 更新表，学习人数 +1
			commonDao.updateData(courseId, UPDATE_COURSE_NUMSTUDY);
			result.setStatus(true);
		} else {
			// 加入收费
			String typeCode = DEF_POINTS_TYPE_D0003;
			BigDecimal points = courseView.getPrice().multiply(new BigDecimal(TpConfigUtil.getPointsForSmallChange()));
			UserPoint point = pointService.consumePoint(PointConstants.SYSTEM_CODE_HS, userId, typeCode, points, null,
					null);
			if (point.isStatus()) {
				joinUserCourse(courseId);
				// 更新表，学习人数 +1
				commonDao.updateData(courseId, UPDATE_COURSE_NUMSTUDY);
				messageService.sendSystemMessage(WebContext.getSessionUserId(), getMessage(MSG_PAY_SUCCESS, "课程"));
				// 根据课程ID，查询导师ID
				CourseEntity entity = commonDao.searchOneData(courseId, SQL_SEACH_TEACHER_ID);
				pointManagerService.pointDivided(entity.getTeacherId(), points.toString(), "A0003");
				result.setStatus(true);
			} else {
				result.setMessages(point.getMessages());
				result.setStatus(false);
			}
		}
		return result;

	}

	/**
	 * 加入userCourse
	 * 
	 * @param courseId
	 * @return
	 */
	public void joinUserCourse(String courseId) {
		UserCourseEntity uc = new UserCourseEntity();
		uc.setCourseId(courseId);
		uc.setUserId(WebContext.getSessionUserId());

		// 检索用户是否存在
		UserCourseEntity uu = commonDao.searchOneData(uc, SEARCH_USER_COURSE);

		uc.setJoinCourse(new BigDecimal(ALREADY_JOIN_COURSE));
		uc.setJoinDate(new Date());
		uc.setUpdateUser(uc.getUserId());

		// 将的第一章传给LastChapterId
		String chapter = commonDao.searchOneData(courseId, SQL_SEARCH_COURSE_FIRST_CHAPTER);
		if (chapter != null) {
			uc.setLastChapterId(chapter);
		}

		if (uu == null) {
			// 不存在，插入新数据
			uc.setCreateUser(uc.getUserId());
			commonDao.insertData(uc, INSERT_USER_COURSE);
		} else {
			// 存在，更新数据
			commonDao.updateData(uc, UPDATE_USER_COURSE);
		}
	}
	/**
	 * 保存操作日志
	 * @param entity
	 */
	public void inserUserOper(UserOperEntity entity) {
		String userId = WebContext.getSessionUserId();
		String userName = WebContext.getSessionUser().getPersonName();
		entity.setUserId(userId);
		entity.setOperUser(userName);
		commonDao.insertData(entity, INSERT_USER_OPER);
	}
	/**
	 * 插入日志信息
	 * @param entity
	 */
	public void insertUserLog(UserLogEntity entity) {
		entity.setOperTime(new Date());
		commonDao.insertData(entity, INSERT_USER_LOG);
	}
	/**
	 * 根据用户名获取用户信息
	 * @param username
	 * @return
	 */
	public TpUserEntity getUserByUsername(String username) {
		TpUserEntity entity = new TpUserEntity();
		entity.setUserName(username);
		return commonDao.searchOneData(entity, SEARCH_USER_BY_USERNAME);
	}
	/**
	 * 浏览数+1
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 */
	public ResultBean addCourseViewNum(CourseEntity course) {
		ResultBean result = new ResultBean();
		// 不存在或未发布
		assertNotEmpty(course.getId(), MSG_E_NULL_ID);
		// 不存在
		assertExist(course.getId(), SQL_IS_COURSE_EXIST, MSG_E_COURSE_NOT_EXIST);
		commonDao.updateData(course, UPDATE_COURSE_NUMVIEW);
		return result;
	}

	/**
	 * 根据ID，检索详情，包括基本信息， 用户信息，用户是否登录，当前用户是否对评价、关注等信息
	 * 
	 * @param courseId String ID
	 * @return ResultBean data:CourseView
	 */
	public ResultBean searchCourseDetail(String courseId) {
		PointTypeEntity ptr = (PointTypeEntity) WebContext.session(PointStateConstants.POINT_TYPE_VOTE);
		if (ptr == null) {
			PointTypeEntity ptc = new PointTypeEntity(PointConstants.SYSTEM_CODE_HS,
					PointStateConstants.POINT_TYPE_VOTE);
			ptc.setSumKbn(SUM_KBN_EARN);
			ptr = pointManagerService.searchOneData(ptc);
			WebContext.session(PointStateConstants.POINT_TYPE_VOTE, ptr);
		}
		assertNotEmpty(courseId, MSG_E_EMPTY_COURSE_ID);
		// 已发布，检索信息
		CourseView view = new CourseView();
		view.setId(courseId);
		assertExist(view, SQL_IS_COURSE_EXIST, MSG_E_EMPTY_COURSE);
		view.setUserId(WebContext.getSessionUserId());
		view = commonDao.searchOneData(view, SEARCH_COURSE_DETAIL_BY_ID);
		// 浏览数+1
		CourseEntity c = new CourseEntity();
		c.setId(courseId);
		addCourseViewNum(c);
		// 判断用户是否登录
		if (WebContext.isGuest()) {
			// 用户未登录
			view.setUserLogin(new BigDecimal(USER_LOGING_NOT));
			view.setJoinCourse(new BigDecimal(NOT_JOIN_COURSE));
		} else {
			// 用户已登录
			view.setUserLogin(new BigDecimal(USER_LOGING_ALREADY));
			// 检索用户
			UserCourseEntity uc = new UserCourseEntity();
			uc.setUserId(WebContext.getSessionUserId());
			uc.setCourseId(courseId);
			uc = searchUserCourse(uc);
			if (uc == null) {
				// 用户不存在
				// 新增用户
				uc = new UserCourseEntity();
				uc.setUserId(WebContext.getSessionUserId());
				uc.setCourseId(courseId);
				commonDao.insertData(uc, INSERT_USER_COURSE);
				// 用户未加入
				view.setJoinCourse(new BigDecimal(NOT_JOIN_COURSE));
				// 未收藏
				view.setFavorite(new BigDecimal(NOT_SAVE));
				// 未评价
				// view.setScoreCourse(new BigDecimal(NOT_SCORE_COURSE));
				// 未投票
				view.setVote("");
			} else {
				// 是否加入
				view.setJoinCourse(new BigDecimal(StringUtil.getOrElse(uc.getJoinCourse(), NOT_JOIN_COURSE)));
				view.setJoinDate(uc.getJoinDate());
				// 是否关注收藏
				view.setFavorite(uc.getFavorite());
				view.setFavoriteDate(uc.getFavoriteDate());

				// 是否投票
				view.setVote(uc.getVote());
				view.setVoteDate(uc.getVoteDate());
				view.setVoteContent(uc.getVoteContent());
				// 最近学习的章节ID
				view.setLastChapterId(uc.getLastChapterId());
				// 最近学习的章节标题
				ChapterEntity chapter = new ChapterEntity();
				chapter.setId(uc.getLastChapterId());
				chapter = searchChapter(chapter);
				if (chapter != null) {
					view.setLastChapterTitle(chapter.getTitle());
				} else {
					view.setLastChapterTitle("");
				}
				// 学习进度(百分比）
				view.setStudyProgress(uc.getStudyProgress());
				// 学习用时(秒)
				view.setTimeCost(uc.getTimeCost());
				MeasurementEntity query = new MeasurementEntity();
				query.setUserId(WebContext.getSessionUserId());
				query.setLinkId(courseId);
				int userMeasurement = searchOneData(query, "UserMeasurementResult.selectUserMeasurmentCount");
				view.setFinishQuestionnaireFlag(userMeasurement > 0 ? "1" : "0");

				if (StringUtil.isEmpty(view.getLastChapterId())) {
					String lastChapterId = commonDao.searchOneData(courseId, SQL_SEARCH_COURSE_FIRST_CHAPTER);
					view.setLastChapterId(lastChapterId);
				}
			}
		}
		if(view.getShareUserId() != null) {
			TpUserEntity user = commonDao.searchOneData(view.getShareUserId(), TpDbConstants.USER_SEARCH_NAME_BY_USERID);
			view.setShareUserId(user.getPersonName());
		}
		ResultBean result = new ResultBean();
		result.setStatus(true);
		result.setData(view);
		return result;
	}
/**
 	public ResultBean searchCourse(String courseId) {
		PointTypeEntity ptr = (PointTypeEntity) WebContext.session(PointStateConstants.POINT_TYPE_VOTE);
		if (ptr == null) {
			PointTypeEntity ptc = new PointTypeEntity(PointConstants.SYSTEM_CODE_HS,
					PointStateConstants.POINT_TYPE_VOTE);
			ptc.setSumKbn(SUM_KBN_EARN);
			ptr = pointManagerService.searchOneData(ptc);
			WebContext.session(PointStateConstants.POINT_TYPE_VOTE, ptr);
		}
		assertNotEmpty(courseId, MSG_E_EMPTY_COURSE_ID);
		// 已发布，检索信息
		CourseView view = new CourseView();
		view.setId(courseId);
		assertExist(view, SQL_IS_COURSE_EXIST, MSG_E_EMPTY_COURSE);
		view.setUserId(WebContext.getSessionUserId());
		view = commonDao.searchOneData(view, SEARCH_COURSE_DETAIL_BY_ID);
		// 浏览数+1
		CourseEntity c = new CourseEntity();
		c.setId(courseId);
		addCourseViewNum(c);
		// 判断用户是否登录
		if (WebContext.isGuest()) {
			// 用户未登录
			view.setUserLogin(new BigDecimal(USER_LOGING_NOT));
			view.setJoinCourse(new BigDecimal(NOT_JOIN_COURSE));
		} else {
			// 用户已登录
			view.setUserLogin(new BigDecimal(USER_LOGING_ALREADY));
			// 检索用户
			UserCourseEntity uc = new UserCourseEntity();
			uc.setUserId(WebContext.getSessionUserId());
			uc.setCourseId(courseId);
			uc = searchUserCourse(uc);
			if (uc == null) {
				// 用户不存在
				// 新增用户
				uc = new UserCourseEntity();
				uc.setUserId(WebContext.getSessionUserId());
				uc.setCourseId(courseId);
				commonDao.insertData(uc, INSERT_USER_COURSE);
				// 用户未加入
				view.setJoinCourse(new BigDecimal(NOT_JOIN_COURSE));
				// 未收藏
				view.setFavorite(new BigDecimal(NOT_SAVE));
				// 未评价
				// view.setScoreCourse(new BigDecimal(NOT_SCORE_COURSE));
				// 未投票
				view.setVote("");
			} else {
				// 是否加入
				view.setJoinCourse(new BigDecimal(StringUtil.getOrElse(uc.getJoinCourse(), NOT_JOIN_COURSE)));
				view.setJoinDate(uc.getJoinDate());
				// 是否关注收藏
				view.setFavorite(uc.getFavorite());
				view.setFavoriteDate(uc.getFavoriteDate());

				// 是否投票
				view.setVote(uc.getVote());
				view.setVoteDate(uc.getVoteDate());
				view.setVoteContent(uc.getVoteContent());
				// 最近学习的章节ID
				view.setLastChapterId(uc.getLastChapterId());
				// 最近学习的章节标题
				ChapterEntity chapter = new ChapterEntity();
				chapter.setId(uc.getLastChapterId());
				chapter = searchChapter(chapter);
				if (chapter != null) {
					view.setLastChapterTitle(chapter.getTitle());
				} else {
					view.setLastChapterTitle("");
				}
				// 学习进度(百分比）
				view.setStudyProgress(uc.getStudyProgress());
				// 学习用时(秒)
				view.setTimeCost(uc.getTimeCost());
				MeasurementEntity query = new MeasurementEntity();
				query.setUserId(WebContext.getSessionUserId());
				query.setLinkId(courseId);
				int userMeasurement = searchOneData(query, "UserMeasurementResult.selectUserMeasurmentCount");
				view.setFinishQuestionnaireFlag(userMeasurement > 0 ? "1" : "0");

				if (StringUtil.isEmpty(view.getLastChapterId())) {
					String lastChapterId = commonDao.searchOneData(courseId, SQL_SEARCH_COURSE_FIRST_CHAPTER);
					view.setLastChapterId(lastChapterId);
				}
			}
		}
		if(view.getShareUserId() != null) {
			TpUserEntity user = commonDao.searchOneData(view.getShareUserId(), TpDbConstants.USER_SEARCH_NAME_BY_USERID);
			view.setShareUserId(user.getPersonName());
		}
		ResultBean result = new ResultBean();
		result.setStatus(true);
		result.setData(view);
		return result;
		public ResultBean searchCourse123(String courseId) {
		PointTypeEntity ptr = (PointTypeEntity) WebContext.session(PointStateConstants.POINT_TYPE_VOTE);
		if (ptr == null) {
			PointTypeEntity ptc = new PointTypeEntity(PointConstants.SYSTEM_CODE_HS,
					PointStateConstants.POINT_TYPE_VOTE);
			ptc.setSumKbn(SUM_KBN_EARN);
			ptr = pointManagerService.searchOneData(ptc);
			WebContext.session(PointStateConstants.POINT_TYPE_VOTE, ptr);
		}
		assertNotEmpty(courseId, MSG_E_EMPTY_COURSE_ID);
		// 已发布，检索信息
		CourseView view = new CourseView();
		view.setId(courseId);
		assertExist(view, SQL_IS_COURSE_EXIST, MSG_E_EMPTY_COURSE);
		view.setUserId(WebContext.getSessionUserId());
		view = commonDao.searchOneData(view, SEARCH_COURSE_DETAIL_BY_ID);
		// 浏览数+1
		CourseEntity c = new CourseEntity();
		c.setId(courseId);
		addCourseViewNum(c);
		// 判断用户是否登录
		if (WebContext.isGuest()) {
			// 用户未登录
			view.setUserLogin(new BigDecimal(USER_LOGING_NOT));
			view.setJoinCourse(new BigDecimal(NOT_JOIN_COURSE));
		}  else {
				// 是否加入
				view.setJoinCourse(new BigDecimal(StringUtil.getOrElse(uc.getJoinCourse(), NOT_JOIN_COURSE)));
				view.setJoinDate(uc.getJoinDate());
				// 是否关注收藏
				view.setFavorite(uc.getFavorite());
				view.setFavoriteDate(uc.getFavoriteDate());

				// 是否投票
				view.setVote(uc.getVote());
				view.setVoteDate(uc.getVoteDate());
				view.setVoteContent(uc.getVoteContent());
				// 最近学习的章节ID
				view.setLastChapterId(uc.getLastChapterId());
				// 最近学习的章节标题
				ChapterEntity chapter = new ChapterEntity();
				chapter.setId(uc.getLastChapterId());
				chapter = searchChapter(chapter);
				if (chapter != null) {
					view.setLastChapterTitle(chapter.getTitle());
				} else {
					view.setLastChapterTitle("");
				}
				// 学习进度(百分比）
				view.setStudyProgress(uc.getStudyProgress());
				// 学习用时(秒)
				view.setTimeCost(uc.getTimeCost());
				MeasurementEntity query = new MeasurementEntity();
				query.setUserId(WebContext.getSessionUserId());
				query.setLinkId(courseId);
				int userMeasurement = searchOneData(query, "UserMeasurementResult.selectUserMeasurmentCount");
				view.setFinishQuestionnaireFlag(userMeasurement > 0 ? "1" : "0");

				if (StringUtil.isEmpty(view.getLastChapterId())) {
					String lastChapterId = commonDao.searchOneData(courseId, SQL_SEARCH_COURSE_FIRST_CHAPTER);
					view.setLastChapterId(lastChapterId);
				}
			}
		}
		if(view.getShareUserId() != null) {
			TpUserEntity user = commonDao.searchOneData(view.getShareUserId(), TpDbConstants.USER_SEARCH_NAME_BY_USERID);
			view.setShareUserId(user.getPersonName());
		}
		ResultBean result = new ResultBean();
		result.setStatus(true);
		result.setData(view);
		return result;
	}
 */
	/***
	 * 检索用户
	 * 
	 * @param entity 用户Entity对象
	 * @return UserCourseEntity对象
	 */
	public UserCourseEntity searchUserCourse(UserCourseEntity entity) {
		// 不存在
		assertExist(entity.getCourseId(), SQL_IS_COURSE_EXIST, MSG_E_COURSE_NOT_EXIST);
		return commonDao.searchOneData(entity, SEARCH_USER_COURSE);
	}

	/***
	 * 根据章节ID检索章节信息
	 * 
	 * @param entity
	 * @return
	 */
	private ChapterEntity searchChapter(ChapterEntity entity) {
		return commonDao.searchOneData(entity, SEARCH_CHAPTER_BY_PK);
	}
    /**
                  * 根据评价id查询评价信息
     * @param id
     * @param type
     * @return
     */
	public TbInstitutionEvalutionManagerView searchEvalutionById(String id,String type) {
		TbInstitutionEvalutionManagerView view  = new TbInstitutionEvalutionManagerView();
		view.setId(id);
		view = commonDao.searchOneData(view, TpDbConstants.USER_SEARCH_NAME_BY_EVALUTION);
		String activitys = "";
		//判断类型
		if (PAGE_TYPE_EDIT.equals(type)) {
			if (FLAG.equals(view.getFlagNote())) {
				activitys += FLAG_NOTE + ",";
			}
			if (FLAG.equals(view.getFlagVote())) {
				activitys += FLAG_VOTE + ",";
			}
			if (FLAG.equals(view.getFlagQa())) {
				activitys += FLAG_QA + ",";
			}
			if (FLAG.equals(view.getFlagDiscuss())) {
				activitys += FLAG_DISCUSS + ",";
			}
		}
		view.setActivitys(activitys);
		return view;
	}
     /**
                     * 查询学校关联的分类信息
      * @param entity
      * @return
      */
	public List<TagLinkInstitutionEntity> searchList(TagLinkInstitutionEntity entity) {
		return commonDao.searchList(entity, SQL_SEARCH_LINK_TAG_TYPE);
	}
    /**
                  * 更新评价状态
     * @param id
     * @param status
     */
	public void publicEvalution(String id, String status) {
		TbInstitutionEvalutionManagerView view = new TbInstitutionEvalutionManagerView();
		view.setId(id);
		if (COURSE_STATUS_NOT.equals(status)) {
			view.setStatus(COURSE_STATUS_PUBLIC);
		} else {
			view.setStatus(COURSE_STATUS_NOT);
		}
		commonDao.updateData(view, UPDATE_EVALUTION_STATUS);
		
	}
}
