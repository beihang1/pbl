package com.cloudsoaring.web.institution.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cloudsoaring.web.common.service.BaseService;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.institution.entity.TagInstitutionEntity;
/**
 * 标签tag_institution逻辑实现
 * 常量接口CourseConstants
 * @author Admin
 *
 */
@Service
public class TagInstitutionService extends BaseService implements CourseConstants {
	
    /**
     * 查询学校一级分类方法
     * @param isentity
     * @return
     */
	public TagInstitutionEntity searchFirstData(TagInstitutionEntity isentity) {
		//设置排序字段和顺序
		isentity.setSortName("create_date");
		isentity.setSortOrder("desc");
		//单条数据查询
		return commonDao.searchOneData(isentity, SEARCH_TAG_LIST_BYTYPE_INSTITUTION);
	}
	/**
	 * 查询全部学校分类信息
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<TagInstitutionEntity> searchTagInstitutionAll() {
		//实例化标签分类对象
		TagInstitutionEntity tag = new TagInstitutionEntity();		
		//查询集合tag_institution数据表
		List<TagInstitutionEntity> tags = commonDao.searchList(tag, SCHOOL_FIRST_PBL_TAG_LIST);
		return tags;		
	}
	/**
	 * 根据tagId查询学校表相关信息
	 * @param tagId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public TagInstitutionEntity searchTagInstitution(String tagId) {
		//实例化标签分类对象
		TagInstitutionEntity tag = new TagInstitutionEntity();		
		tag.setTagId(tagId);
		TagInstitutionEntity tags = commonDao.searchOneData(tag, SEARCH_TAG_LIST_BYTYPE_INSTITUTION);
		//返回结果
		return tags;		
	}
}
