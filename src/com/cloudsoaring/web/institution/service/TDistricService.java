package com.cloudsoaring.web.institution.service;

import org.springframework.stereotype.Service;

import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.institution.entity.TDistricEntity;
import com.cloudsoaring.web.trainingplatform.constants.TpConstants;

/***
 * 区域管理相关的业务处理Service
 */
@Service
@SuppressWarnings("unchecked")
public class TDistricService extends FileService implements TpConstants{
     /**
                    * 分页查询区域信息
      * @param entity
      * @return
      */
	public ResultBean searchPage(TDistricEntity entity){
		//分页查询t_distric数据表
		return commonDao.searchList4Page(entity, INSTITUTION_DISTRIC_SEARCH);
	}
	/**
	 * 保存方法
	 * @param entity
	 */
	public void insert(TDistricEntity entity){
		//保存t_distric
		this.commonDao.insertData(entity,INSTITUTION_DISTRIC_ADD);
	}
	/**
	 * 单条查询区域方法
	 * @param id
	 * @return
	 */
	public TDistricEntity selectInstitutionManagerById(String id) {
		TDistricEntity entity = new TDistricEntity();
		entity.setId(id);
		//单挑查询t_distric
		return commonDao.searchOneData(entity, INSTITUTION_DISTRIC_SEARCH_BY_ID);
	}
	/**
	 * 更新方法
	 * @param entity
	 */
	public void update(TDistricEntity entity){
		//更新t_distric
		this.commonDao.updateData(entity, INSTITUTION_DISTRIC_UPDATE_BY_ID);
	}
}
