package com.cloudsoaring.web.institution.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.FileEntity;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseStateConstants;
import com.cloudsoaring.web.course.entity.UserMeasurementResultEntity;
import com.cloudsoaring.web.institution.entity.MeasurementInstitutionEntity;
import com.cloudsoaring.web.institution.entity.TagInstitutionEntity;
import com.cloudsoaring.web.taxonomy.constant.TaxonomyConstants;
import com.cloudsoaring.web.taxonomy.entity.TagInstitutionEntityView;
import com.cloudsoaring.web.taxonomy.entity.TagInstitutionView;
import com.cloudsoaring.web.taxonomy.entity.TagTypeInstitutionView;
import com.cloudsoaring.web.taxonomy.entity.TbInstitutionEvalutionManagerView;
import com.cloudsoaring.web.teacherevalution.entity.MeasurementTeacherEntity;

/**
 * 课程分类管理
 * 
 * @author JGJ
 *
 */
@Service
public class TaxonomyInstitutionService extends FileService implements TaxonomyConstants {
	/**
	 * 标签一览
	 * 
	 * @param BusinessType
	 *            业务类型
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ResultBean searchTagList2(TagInstitutionEntityView tag) {
		ResultBean result = new ResultBean();
		//判断分类类型是否为空
		if (StringUtil.isEmpty(tag.getBusinessType())) {
			tag.setBusinessType(CourseStateConstants.TAG_BUSSINESS_TYPE_PLAN_INSTITUTION);
		}
		tag.setFlag("1");
		//查询学校分类信息
		result = commonDao.searchList4Page(tag, SEARCH_TAG_LIST_BYTYPE_INSTITUTION);
		List<TagInstitutionEntityView> list = (List<TagInstitutionEntityView>) result.getData();
		if (list.size() == 0) {
			return result;
		}
		List<TagInstitutionEntityView> listAll = new ArrayList<TagInstitutionEntityView>();
		//循环遍历分类信息
		for (TagInstitutionEntityView tagw : list) {
			listAll.add(tagw);
			for (TagInstitutionEntityView taglist : tagw.getList()) {
				//设置分类类型
				taglist.setType(CourseStateConstants.TAG_TYPE_RE);
				listAll.add(taglist);
			}

		}
        //将结果返回结果集
		result.setData(listAll);

		return result;
	}
	/**
	 * 保存新增标签
	 * 
	 * @param tagEntity
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saveTag.action")
	@ResponseBody
	public ResultBean saveTag(TagInstitutionEntityView tagEntity) throws Exception {
		ResultBean result = new ResultBean();
		//随机生成分类id
		tagEntity.setTagId(IDGenerator.genUID());
		FileEntity resultData = processRequestFile("f1", "image");
		if (resultData != null) {
			tagEntity.setIco(resultData.getFileId());
		}
        //判断父类id是否为空
		if (StringUtil.isEmpty(tagEntity.getParentId())) {

			tagEntity.setBusinessType(StringUtil.getOrElse(
					tagEntity.getBusinessType(),
					CourseStateConstants.TAG_BUSSINESS_TYPE_INSTITUTION));
            //保存分类信息
			commonDao.insertData(tagEntity, INSERT_TAG_FA_INSTITUTION);
		} else {
			assertNotEmpty(tagEntity.getTagTypeId(), "标签类Id不能为空");
			commonDao.insertData(tagEntity, INSERT_TAG_INSTITUTION);
		}
        //返回执行结果
		return result;

	}
	
	/**
	 * 保存新增标签
	 * 
	 * @param tagEntity
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saveTag2.action")
	@ResponseBody
	public ResultBean saveTag2(TagInstitutionEntityView tagEntity) throws Exception {
		ResultBean result = new ResultBean();
		//随机生成id
		tagEntity.setTagId(IDGenerator.genUID());
		FileEntity resultData = processRequestFile("f1", "image");
		if (resultData != null) {
			tagEntity.setIco(resultData.getFileId());
		}
        //是否存在父级id
		if (StringUtil.isEmpty(tagEntity.getParentId())) {
            //设置标签类型
			tagEntity.setBusinessType(StringUtil.getOrElse(
					tagEntity.getBusinessType(),
					CourseStateConstants.TAG_BUSSINESS_TYPE_PLAN_INSTITUTION));
            //保存
			commonDao.insertData(tagEntity, INSERT_TAG_FA_INSTITUTION);
		} else {
			assertNotEmpty(tagEntity.getTagTypeId(), "标签类Id不能为空");
			commonDao.insertData(tagEntity, INSERT_TAG_INSTITUTION);
		}

		return result;

	}

	/**
	 * 删除标签
	 * 
	 * @param tagIds
	 * @return
	 * @throws Exception
	 */
	public ResultBean delTag(String tagId, String ico) throws Exception {
		ResultBean result = new ResultBean();
		//批量删除的标签id字符串集合
		String[] tagIds = StringUtil.split(tagId, ",");
		if (tagIds != null&&tagIds.length>0) {
			//查询标签是否存在关联
			int count = commonDao.searchCount(tagIds, SEARCH_TAG_LINK_COUNT_INSTITUTION);
			if (count > 0) {
				//存在
				return result.setStatus(false).setMessages("不能删除正在使用的标签");
			}
			//查询标签是否存在父级
			int countOne = commonDao.searchCount(tagIds, SEARCH_TAG_COUNT);
			if (countOne > 0) {
				//存在
				return result.setStatus(false).setMessages("不能删除父标签");
			}
            //删除标签
			commonDao.deleteData(tagIds, DELECTE_TAG_INSTITUTION);
			String[] icos = StringUtil.split(ico, ",");
			for (String ic : icos) {
				super.deleteFileById(ic, "image");
			}
		}
		//返回结果
		return result;

	}

	/**
	 * 编辑标签
	 * 
	 * @param TagEntity
	 * @return
	 * @throws Exception
	 */
	public ResultBean updateTag(TagInstitutionEntity tagEntity) throws Exception {
		ResultBean result = new ResultBean();
		//获取图片信息
		FileEntity resultData = processRequestFile("f1", "image");
		if (resultData != null && StringUtil.isNotEmpty(resultData.getFileId())) {
			super.deleteFileById(tagEntity.getIco(), "image");
			tagEntity.setIco(resultData.getFileId());
		}
		//更新标签内容
		commonDao.updateData(tagEntity, UPDATE_TAG_INSTITUTION);
		return result;

	}

	/**
	 * 检索子标签类集合
	 * 
	 * @param businessType
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<TagTypeInstitutionView> searchTagTypeList(String businessType) {
        //返回查询结果
		return commonDao.searchList(StringUtil.getOrElse(businessType,
				CourseStateConstants.TAG_BUSSINESS_TYPE_INSTITUTION),
				SQL_SEARCH_TAG_TYPE_LIST_INSTITUTION);

	}

	/**
	 * 检索子标签类
	 * 
	 * @param businessType
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<TagTypeInstitutionView> searchTagTypeList2(String businessType) {
        //返回查询结果
		return commonDao.searchList(StringUtil.getOrElse(businessType,
				CourseStateConstants.TAG_BUSSINESS_TYPE_PLAN_INSTITUTION),
				SQL_SEARCH_TAG_TYPE_LIST_INSTITUTION);

	}
    /**
     * 查询一级标签
     * @param tagQuery
     * @return
     */
	public List<TagInstitutionView> searchTagInstitutionList(TagInstitutionView tagQuery) {
		//设置分类类型查询结果
		tagQuery.setTagTypeId("3");
		tagQuery.setBusinessType("2");
		return commonDao.searchList(tagQuery, SQL_SEARCH_FIRST_TAG_TYPE_INSTITUTION);
	}
    /**
     * 查询二级标签
     * @param tagQuery
     * @return
     */
	public List<TagInstitutionView> searchTagInstitutionSecondList(TagInstitutionView tagQuery) {
		//设置分类类型查询结果
		tagQuery.setTagTypeId("4");
		tagQuery.setBusinessType("2");
		return commonDao.searchList(tagQuery, SQL_SEARCH_SECOND_TAG_TYPE_INSTITUTION);
	}
    /**
     * 查询评价管理列表
     * @param queryInfo
     * @return
     */
	public ResultBean listSearch(TbInstitutionEvalutionManagerView queryInfo) {
		return commonDao.searchList4Page(queryInfo, SELECT_INSTITUTION_LIST_MANAGER);
	}
    /**
               *  查询学校评价列表方法包括已办和待办
     * @param queryInfo
     * @return
     */
	@SuppressWarnings("unchecked")
	public ResultBean listMeasurementSearch(MeasurementInstitutionEntity queryInfo) {
		ResultBean result = new ResultBean();
		//获取登录信息
		String userId=WebContext.getSessionUserId();
		List<MeasurementInstitutionEntity> list = new ArrayList<MeasurementInstitutionEntity>();
		//分页查询学校评价列表
		result = commonDao.searchList4Page(queryInfo, SELECT_INSTITUTION_QUETION_LIST_MANAGER);
		Object object = result.getData();
		list = (List<MeasurementInstitutionEntity>) object;
		//循环遍历结果
		if(!list.isEmpty()) {
			for(int i=0;i<list.size();i++) {
				//查询是否评价
				String id = list.get(i).getId();
				UserMeasurementResultEntity entity = new UserMeasurementResultEntity();
				//设置评价主键
				entity.setMeasurementId(id);
				//设置评价账号
				entity.setCreateUser(userId);
				entity = commonDao.searchOneData(entity, SELECT_QUETION_RESULT_LIST_MANAGER);
				//为空评价的赋值
				if(null==entity) {
					list.get(i).setFlag("0");
				}else {
					//已评价的赋值
					list.get(i).setFlag("1");
				}
			}
		}
		result.setData((Object)list);
		return result;
				
	}
	/**
	 @SuppressWarnings("unchecked")
	public ResultBean listMeasurementSearch1(MeasurementInstitutionEntity queryInfo) {
		ResultBean result = new ResultBean();
		//获取登录信息
		String userId=WebContext.getSessionUserId();
		List<MeasurementInstitutionEntity> list = new ArrayList<MeasurementInstitutionEntity>();
		//分页查询学校评价列表
		result = commonDao.searchList4Page(queryInfo, SELECT_INSTITUTION_QUETION_LIST_MANAGER);
		Object object = result.getData();
		list = (List<MeasurementInstitutionEntity>) object;
		//循环遍历结果
		if(!list.isEmpty()) {
			for(int i=0;i<list.size();i++) {
				//查询是否评价
				String id = list.get(i).getId();
				UserMeasurementResultEntity entity = new UserMeasurementResultEntity();
				//设置评价主键
				entity.setMeasurementId(id);
				//设置评价账号
				entity.setCreateUser(userId);
				entity = commonDao.searchOneData(entity, SELECT_QUETION_RESULT_LIST_MANAGER);
				//为空评价的赋值
				if(null==entity) {
					list.get(i).setFlag("0");
				}else {
					//已评价的赋值
					list.get(i).setFlag("1");
				}
			}
		}
		result.setData((Object)list);
		return result;
				
	}
		@SuppressWarnings("unchecked")
	public ResultBean listSchoolMeasurement(MeasurementInstitutionEntity queryInfo) {
		ResultBean result = new ResultBean();
		//获取登录信息
		String userId=WebContext.getSessionUserId();
		List<MeasurementInstitutionEntity> list = new ArrayList<MeasurementInstitutionEntity>();
		//分页查询学校评价列表
		result = commonDao.searchList4Page(queryInfo, SELECT_INSTITUTION_SCHOOL_QUETION_LIST_MANAGER);
		Object object = result.getData();
		list = (List<MeasurementInstitutionEntity>) object;
		//循环遍历结果
		if(!list.isEmpty()) {
			for(int i=0;i<list.size();i++) {
				String id = list.get(i).getId();
				//查询是否评价
				UserMeasurementResultEntity entity = new UserMeasurementResultEntity();
				entity.setMeasurementId(id);
				entity.setCreateUser(userId);
				entity = commonDao.searchOneData(entity, SELECT_QUETION_RESULT_LIST_MANAGER);
				//为空评价的赋值
				if(null==entity) {
					list.get(i).setFlag("0");
					list.get(i).setFlagName("未评价");
				}else {
					//已评价的赋值
					list.get(i).setFlag("1");
					list.get(i).setFlagName("已评价");
				}
			}
		}
		//返回处理后的评价信息
		result.setData((Object)list);
		return result;
				
	}
	public ResultBean searchInstitution(TagInstitutionEntityView tag) {
		ResultBean result = new ResultBean();
		//判断分类类型是否为空
		if (StringUtil.isEmpty(tag.getBusinessType())) {
			tag.setBusinessType(CourseStateConstants.TAG_BUSSINESS_TYPE_PLAN_INSTITUTION);
		}
		//分页查询分类数据
		result = commonDao.searchList4Page(tag, SCHOOL_TAG_LIST_BYTYPE_INSTITUTION);
		List<TagInstitutionEntityView> list = (List<TagInstitutionEntityView>) result.getData();
		//为空数据处理
		if (list.size() == 0) {
			return result;
		}
		//查询不为空的数据进行遍历复制
		List<TagInstitutionEntityView> listAll = new ArrayList<TagInstitutionEntityView>();
		for (TagInstitutionEntityView tagw : list) {
			listAll.add(tagw);
			for (TagInstitutionEntityView taglist : tagw.getList()) {
				taglist.setType(CourseStateConstants.TAG_TYPE_RE);
				//将查询处理后的结果添加到要返回的对象里
				listAll.add(taglist);
			}

		}
       
		result.setData(listAll);
        //返回遍历结果
		return result;
	}
	public List<TagInstitutionView> searchTag(TagInstitutionView tagQuery) {
		//设置tagTypeId为4的数据
		tagQuery.setTagTypeId("4");
		tagQuery.setBusinessType("2");
		return commonDao.searchList(tagQuery, SCHOOL_SEARCH_FIRST_TAG_TYPE_INSTITUTION);
	}
	 */
	/**
	 * 查询学校评价列表方法包括已办和待办
	 * @param queryInfo
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ResultBean listSchoolMeasurementSearch(MeasurementInstitutionEntity queryInfo) {
		ResultBean result = new ResultBean();
		//获取登录信息
		String userId=WebContext.getSessionUserId();
		List<MeasurementInstitutionEntity> list = new ArrayList<MeasurementInstitutionEntity>();
		//分页查询学校评价列表
		result = commonDao.searchList4Page(queryInfo, SELECT_INSTITUTION_SCHOOL_QUETION_LIST_MANAGER);
		Object object = result.getData();
		list = (List<MeasurementInstitutionEntity>) object;
		//循环遍历结果
		if(!list.isEmpty()) {
			for(int i=0;i<list.size();i++) {
				String id = list.get(i).getId();
				//查询是否评价
				UserMeasurementResultEntity entity = new UserMeasurementResultEntity();
				entity.setMeasurementId(id);
				entity.setCreateUser(userId);
				entity = commonDao.searchOneData(entity, SELECT_QUETION_RESULT_LIST_MANAGER);
				//为空评价的赋值
				if(null==entity) {
					list.get(i).setFlag("0");
					list.get(i).setFlagName("未评价");
				}else {
					//已评价的赋值
					list.get(i).setFlag("1");
					list.get(i).setFlagName("已评价");
				}
			}
		}
		//返回处理后的评价信息
		result.setData((Object)list);
		return result;
				
	}
	/**
	 * 查询tag_type_institution的分类标签数据
	 * @param tagQuery
	 * @return
	 */
	public List<TagInstitutionView> searchTagInstitutionList1(TagInstitutionView tagQuery) {
		//设置tagTypeId为4的数据
		tagQuery.setTagTypeId("4");
		tagQuery.setBusinessType("2");
		return commonDao.searchList(tagQuery, SCHOOL_SEARCH_FIRST_TAG_TYPE_INSTITUTION);
	}
    /**
                  * 查询学校分类标签tb_institution_tag表
     * @param tag
     * @return
     */
	public ResultBean searchInstitutionTagList2(TagInstitutionEntityView tag) {
		ResultBean result = new ResultBean();
		//判断分类类型是否为空
		if (StringUtil.isEmpty(tag.getBusinessType())) {
			tag.setBusinessType(CourseStateConstants.TAG_BUSSINESS_TYPE_PLAN_INSTITUTION);
		}
		//分页查询分类数据
		result = commonDao.searchList4Page(tag, SCHOOL_TAG_LIST_BYTYPE_INSTITUTION);
		List<TagInstitutionEntityView> list = (List<TagInstitutionEntityView>) result.getData();
		//为空数据处理
		if (list.size() == 0) {
			return result;
		}
		//查询不为空的数据进行遍历复制
		List<TagInstitutionEntityView> listAll = new ArrayList<TagInstitutionEntityView>();
		for (TagInstitutionEntityView tagw : list) {
			listAll.add(tagw);
			for (TagInstitutionEntityView taglist : tagw.getList()) {
				taglist.setType(CourseStateConstants.TAG_TYPE_RE);
				//将查询处理后的结果添加到要返回的对象里
				listAll.add(taglist);
			}

		}
       
		result.setData(listAll);
        //返回遍历结果
		return result;
	}
    /**
                   * 查询待办tb_measurement_institution数据集合信息方法
     * @param queryInfo
     * @return
     */
	public ResultBean listdbSearch(MeasurementInstitutionEntity queryInfo) {
		ResultBean result = new ResultBean();
		//获取登录信息
		String userId=WebContext.getSessionUserId();
		//声明待办对象集合
		List<MeasurementInstitutionEntity> yplist = new ArrayList<MeasurementInstitutionEntity>();
		List<MeasurementInstitutionEntity> list = new ArrayList<MeasurementInstitutionEntity>();
		//分页查询数据
		result = commonDao.searchList4Page(queryInfo, SELECT_INSTITUTION_DB_LIST_MANAGER);
		Object object = result.getData();
		//循环遍历分页结果并赋值
		list = (List<MeasurementInstitutionEntity>) object;
		if(!list.isEmpty()) {
			for(int i=0;i<list.size();i++) {
				String id = list.get(i).getId();
				//查询是否已经评价方法
				UserMeasurementResultEntity entity = new UserMeasurementResultEntity();
				entity.setMeasurementId(id);
				entity.setCreateUser(userId);
				entity = commonDao.searchOneData(entity, SELECT_QUETION_RESULT_LIST_MANAGER);
				//对象为空为待办
				if(null==entity) {
					list.get(i).setFlag("0");
					list.get(i).setFlagName("未评价");
					yplist.add(list.get(i));
				}
			}
		}
		result.setData((Object)yplist);
		return result;
	}
	/**
	 * 查询已办tb_measurement_institution数据集合信息方法
	 * @param queryInfo
	 * @return
	 */
	public ResultBean listybSearch(MeasurementInstitutionEntity queryInfo) {
		ResultBean result = new ResultBean();
		//获取登录信息
		String userId=WebContext.getSessionUserId();
		//声明已办对象集合
		List<MeasurementInstitutionEntity> yplist = new ArrayList<MeasurementInstitutionEntity>();
		List<MeasurementInstitutionEntity> list = new ArrayList<MeasurementInstitutionEntity>();
		//分页查询数据
		result = commonDao.searchList4Page(queryInfo, SELECT_INSTITUTION_DB_LIST_MANAGER);
		Object object = result.getData();
		//循环遍历分页结果并赋值
		list = (List<MeasurementInstitutionEntity>) object;
		if(!list.isEmpty()) {
			for(int i=0;i<list.size();i++) {
				String id = list.get(i).getId();
				//查询是否已经评价方法
				UserMeasurementResultEntity entity = new UserMeasurementResultEntity();
				entity.setMeasurementId(id);
				entity.setCreateUser(userId);
				entity = commonDao.searchOneData(entity, SELECT_QUETION_RESULT_LIST_MANAGER);
				//对象不空为已办
				if(null!=entity) {
					list.get(i).setFlag("1");
					list.get(i).setFlagName("已评价");
					yplist.add(list.get(i));
				}
			}
		}
		result.setData((Object)yplist);
		return result;
	}
}
