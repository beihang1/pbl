package com.cloudsoaring.web.institution.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.CompressConfig;
import com.cloudsoaring.web.bus.entity.FileEntity;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseDbConstants;
import com.cloudsoaring.web.course.entity.MeasurementEntity;
import com.cloudsoaring.web.course.entity.TagLinkEntity;
import com.cloudsoaring.web.course.view.CourseManagerView;
import com.cloudsoaring.web.course.view.InstitutionManagerView;
import com.cloudsoaring.web.institution.entity.InstitutionEntity;
import com.cloudsoaring.web.institution.entity.TagInstitutionEntity;
import com.cloudsoaring.web.institution.entity.TagLinkInstitutionEntity;
import com.cloudsoaring.web.institution.entity.TbInstitutionEvalutionEntity;
import com.cloudsoaring.web.taxonomy.entity.TbInstitutionEvalutionManagerView;
import com.cloudsoaring.web.trainingplatform.constants.TpConstants;
import com.cloudsoaring.web.trainingplatform.utils.TpConfigUtil;

/***
 * 班级管理相关的业务处理Service
 */
@Service
@SuppressWarnings("unchecked")
public class InstitutionService extends FileService implements TpConstants{
    /**
     * 分页查询学校信息
     * @param entity
     * @return
     */
	public ResultBean searchInstitutionList(InstitutionEntity entity){
		//分页查询tb_institution数据表信息
		return commonDao.searchList4Page(entity, INSTITUTION_SEARCH);
	}
	/**
	 * 保存学校方法
	 * @param entity 学校对象参数
	 * @param config 图片处理
	 * @throws Exception
	 */
	public void insertInstitution(InstitutionEntity entity,CompressConfig config) throws Exception{
		//图片信息处理
		FileEntity image = processRequestFile("imageP", FILE_TYPE_IMAGE);
		//压缩图片
		compress(image, true, config);
		//判断图片是否为空
		if (image != null) {
			entity.setImage(image.getFileId());
		}
		//随机生成主键信息
		entity.setId(IDGenerator.genUID());
		//设置创建时间
		entity.setCreateTime(new Date());
		//设置创建人员
		entity.setCreateUser(WebContext.getSessionUserId());
		//保存到tb_institution数据表
		this.commonDao.insertData(entity,INSTITUTION_ADD);
	}
	/**
	 * 根据学校id查询学校信息
	 * @param id
	 * @return
	 */
	public InstitutionEntity selectInstitutionById(String id) {
		//实例化查询对象
		InstitutionEntity entity = new InstitutionEntity();
		entity.setId(id);
		//查询tb_institution数据表
		return commonDao.searchOneData(entity, INSTITUTION_SEARCH_BY_ID);
	}
	/**
	 * 更新学校信息
	 * @param entity
	 * @param config
	 * @throws Exception
	 */
	public void updateInstitution(InstitutionEntity entity,CompressConfig config) throws Exception {
		//处理对象信息
		FileEntity image = processRequestFile("imageP", FILE_TYPE_IMAGE);
		//压缩图像
		compress(image, true, config);
		//图像是否为空
		if (image != null) {
			entity.setImage(image.getFileId());
		}
		//设置跟新信息账号
		entity.setUpdateUser(WebContext.getSessionUserId());
		//更新时间
		entity.setUpdateTime(new Date());
		//创建时间
		entity.setCreateTime(new Date());
		//跟新数据库
		this.commonDao.updateData(entity, INSTITUTION_UPDATE_BY_ID);
	}
	/**
	 *根据学校id 删除学校信息
	 * @param entity
	 */
	public void deleteInstitutionById(InstitutionEntity entity) {
		//删除tb_institution信息
		this.commonDao.deleteData(entity, INSTITUTION_DELETE_BY_ID);
	}
    /**
             * 导入学校信息
     * @param excel
     * @return
     * @throws Exception
     */
	public ResultBean importInstitution(File excel) throws Exception {
		assertNotGuest();
		//设置文件输出对象
		FileOutputStream thumbOutstream = null;
		try {
			//实例化map对象
			Map<String, File> imgs = new HashMap<String, File>();
			//设置对象格式
			imgs.put("xlsx", excel);
            //判断表格是否为空
			if (excel != null) {
				//导入模板信息
				return doImportInstitution(excel, imgs);
			} else {
				//为空返回
				return ResultBean.error("在Zip文件未找到要导入的Excel数据文件");
			}
		} finally {
			//关闭文件输出流
			IOUtils.closeQuietly(thumbOutstream);
		}
	}
	/**
	 * 导入学校信息
	 * @param excel 文件对象
	 * @param imgs 
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes" })
	private ResultBean doImportInstitution(File excel, final Map<String, File> imgs) throws Exception {
		//设置文件输入流
		InputStream input = null;
		String alertMsg=";";
		try {
			input = new FileInputStream(excel);
			//创建表格对象
			XSSFWorkbook workbook = 
					new XSSFWorkbook(input);
			//处理excel对象信息
			XSSFSheet sheet = workbook.getSheetAt(0);
			int firstRowNum = 1;
			//获取sheet中最后一行行号
			int lastRowNum = sheet.getLastRowNum();
			//声明数据库保存的对象
			List<InstitutionManagerView> viewList = new ArrayList<InstitutionManagerView>();
			for (int i = firstRowNum; i <=lastRowNum; i++) {
				//从表格取数据放置到对象集合中
				XSSFRow row = sheet.getRow(i);
				InstitutionManagerView institutionManagerView = new InstitutionManagerView();
				if(row.getCell(0)!=null){
					row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
					//判断必填项是否为空
					if(StringUtils.isBlank(row.getCell(0).getStringCellValue())) {
						alertMsg+="第"+i+"行的"+"第1列填写的数据内容不能为空";
						continue;
					}
					institutionManagerView.setInstitutionName(row.getCell(0).getStringCellValue());
				}
				if(row.getCell(1)!=null){
					row.getCell(1).setCellType(Cell.CELL_TYPE_STRING);
					//判断是教育局还是学校
					if("教育局".equals(row.getCell(1).getStringCellValue()) || "学校".equals(row.getCell(1).getStringCellValue())) {
						institutionManagerView.setInstitutionType(row.getCell(1).getStringCellValue());
					}else {
						alertMsg+="第"+i+"行的"+"第2列填写的数据内容不对请填写模板提示内容";
						continue;
					}
					
				}
				//设置单元格类型字符串类型，设置学校简介
				if(row.getCell(2)!=null){
					row.getCell(2).setCellType(Cell.CELL_TYPE_STRING);
					
					institutionManagerView.setInstitutionAbbreviation(row.getCell(2).getStringCellValue());
				}
				//设置学校地址
				if(row.getCell(3)!=null){
					row.getCell(3).setCellType(Cell.CELL_TYPE_STRING);
					institutionManagerView.setInstitutionAddress(row.getCell(3).getStringCellValue());
				}
				//设置学校准确地址
				if(row.getCell(4)!=null){
					row.getCell(4).setCellType(Cell.CELL_TYPE_STRING);
					institutionManagerView.setInstitutionSpecificAddress(row.getCell(4).getStringCellValue());
				}
				//设置门户地址
				if(row.getCell(5)!=null){
					row.getCell(5).setCellType(Cell.CELL_TYPE_STRING);
					institutionManagerView.setPortalAddress(row.getCell(5).getStringCellValue());
				}
				//设置学校类型
				if(row.getCell(6)!=null){
					row.getCell(6).setCellType(Cell.CELL_TYPE_STRING);
					institutionManagerView.setNature(row.getCell(6).getStringCellValue());
				}
				//设置联系电话
				if(row.getCell(7)!=null){
					row.getCell(7).setCellType(Cell.CELL_TYPE_STRING);
					institutionManagerView.setTelphone(row.getCell(7).getStringCellValue());
				}
				//设置联系邮件
				if(row.getCell(8)!=null){
					row.getCell(8).setCellType(Cell.CELL_TYPE_STRING);
					institutionManagerView.setEmail(row.getCell(8).getStringCellValue());
				}
				//设置门户类型
				if(row.getCell(9)!=null){
					row.getCell(9).setCellType(Cell.CELL_TYPE_STRING);
					institutionManagerView.setPortalType(row.getCell(9).getStringCellValue());
				}
				//设置是否合作
				if(row.getCell(10)!=null){
					row.getCell(10).setCellType(Cell.CELL_TYPE_STRING);
					institutionManagerView.setIfCooperation(row.getCell(10).getStringCellValue());
				}
				//设置学校类型
				if(row.getCell(11)!=null){
					row.getCell(11).setCellType(Cell.CELL_TYPE_STRING);
					institutionManagerView.setSchoolType(row.getCell(11).getStringCellValue());
				}
				//设置学校编码
				if(row.getCell(12)!=null){
					row.getCell(12).setCellType(Cell.CELL_TYPE_STRING);
					institutionManagerView.setSchoolCode(row.getCell(12).getStringCellValue());
				}
				//设置隶属关系,学校专有字段：一般、省直、市直
				if(row.getCell(13)!=null){
					row.getCell(13).setCellType(Cell.CELL_TYPE_STRING);
					institutionManagerView.setAffiliation(row.getCell(13).getStringCellValue());
				}
				//设置是否教育局
				if(row.getCell(14)!=null){
					row.getCell(14).setCellType(Cell.CELL_TYPE_STRING);
					institutionManagerView.setEdution(row.getCell(14).getStringCellValue());
				}
				viewList.add(institutionManagerView);
			}
				// 插入操作
				for (InstitutionManagerView cr : viewList) {
					InstitutionManagerView cmv = cr;
					//根据机构类型判断是否是教育局保存信息
					if(StringUtil.isNotBlank(cmv.getInstitutionType()) && "教育局".equals(cmv.getInstitutionType())) {
						InstitutionEntity institutionEntity = new InstitutionEntity();
						institutionEntity.setInstitutionName(cmv.getInstitutionName());
						institutionEntity.setInstitutionType(cmv.getInstitutionType());
						//查询是否存在该机构
						InstitutionEntity entity = commonDao.searchOneData(institutionEntity, INSTITUTION_SEARCH);
						if(null!=entity) {
							//存在更新
							cmv.setId(entity.getId());
							cmv.setUpdateDate(new Date());
							commonDao.updateData(cmv, INSTITUTION_UPDATE_BY_ID);
						}else {
							//不存在保存
							cmv.setId(IDGenerator.genUID());
							cmv.setInstitutionType("教育局");
							cmv.setCreateDate(new Date());
							commonDao.insertData(cmv, INSTITUTION_ADD);
						}
						
					}
					//根据机构类型判断是否是学校保存信息
					if(StringUtil.isNotBlank(cmv.getInstitutionType()) && "学校".equals(cmv.getInstitutionType()) && StringUtil.isNotBlank(cmv.getEdution()) ) {
						InstitutionEntity institutionEntity = new InstitutionEntity();
						institutionEntity.setInstitutionName(cmv.getEdution());
						institutionEntity.setInstitutionType("教育局");
						//查询该学校是否存在
						InstitutionEntity entity = commonDao.searchOneData(institutionEntity, INSTITUTION_SEARCH);
						String institutionParentId = "";
						if(null!=entity) {
							//存在更新学校信息
							institutionParentId = entity.getId();
							InstitutionEntity entity1 = new InstitutionEntity();
							entity1.setInstitutionName(cmv.getInstitutionName());
							entity1.setInstitutionType("学校");
							entity1.setInstitutionParentId(institutionParentId);
							//查询学校
							InstitutionEntity tity = commonDao.searchOneData(entity1, INSTITUTION_SEARCH);
							if(null!=tity) {
								//不存在保存信息
								cmv.setId(tity.getId());
								cmv.setInstitutionParentId(institutionParentId);
								cmv.setUpdateDate(new Date());
								cmv.setUpdateUser(WebContext.getSessionUserId());
								commonDao.updateData(cmv, INSTITUTION_UPDATE_BY_ID);
							}else {
								cmv.setId(IDGenerator.genUID());
								cmv.setInstitutionParentId(institutionParentId);
								cmv.setCreateDate(new Date());
								cmv.setCreateUser(WebContext.getSessionUserId());
								commonDao.insertData(cmv, INSTITUTION_ADD);
							}
							
						}else {
							InstitutionManagerView view = new InstitutionManagerView();
							String id = IDGenerator.genUID();
							view.setId(id);
							view.setInstitutionName(cmv.getEdution());
							view.setInstitutionType("教育局");
							view.setCreateDate(new Date());
							view.setCreateUser(WebContext.getSessionUserId());
							//保存教育局信息
							commonDao.insertData(view, INSTITUTION_ADD);
							cmv.setId(IDGenerator.genUID());
							cmv.setInstitutionParentId(institutionParentId);
							cmv.setCreateDate(new Date());
							cmv.setCreateUser(WebContext.getSessionUserId());
							//保存学校信息
							commonDao.insertData(cmv, INSTITUTION_ADD);
							}
						}
					}
					
//				}
				ResultBean.success().setMessages(alertMsg);
			return ResultBean.success().setData(viewList);
		} finally {
			IOUtils.closeQuietly(input);
		}

	}
    /**
     * 导出方法
     * @param entity
     * @return
     * @throws Exception
     */
	public List<InstitutionManagerView> exportInstitution(InstitutionEntity entity) throws Exception {
//		 List<InstitutionEntity> enityList = new ArrayList<InstitutionEntity>();
//		    List<InstitutionManagerView> view = new ArrayList<InstitutionManagerView>();
//		    enityList = commonDao.searchList(entity, INSTITUTION_EXPORT_SEARCH);
//		    if(null!=enityList && enityList.size()>0) {
//		    	for(int i=0;i<enityList.size();i++) {
//		    		InstitutionManagerView maganerView = new InstitutionManagerView();
//		    		InstitutionEntity entity1 = new InstitutionEntity();
//		    		entity1 = enityList.get(i);
//		    		maganerView.setId(entity1.getId());
//		    		maganerView.setInstitutionName(entity1.getInstitutionName());
//		    		maganerView.setInstitutionType(entity1.getInstitutionType());
//		    		if(StringUtil.isNotBlank(entity1.getEmail())) {
//		    			maganerView.setEmail(entity1.getEmail());
//		    		}
//                if(StringUtil.isNotBlank(entity1.getAffiliation())) {
//             	   maganerView.setAffiliation(entity1.getAffiliation());
//		    		}
//                if(StringUtil.isNotBlank(entity1.getIfCooperation())) {
//             	   maganerView.setIfCooperation(entity1.getIfCooperation());
//		    		}
//                if(StringUtil.isNotBlank(entity1.getInstitutionAbbreviation())) {
//             	   maganerView.setInstitutionAbbreviation(entity1.getInstitutionAbbreviation());
//		    		}
//                if(StringUtil.isNotBlank(entity1.getInstitutionAddress())) {
//             	   maganerView.setInstitutionAddress(entity1.getInstitutionAddress());
//		    		}
//                if(StringUtil.isNotBlank(entity1.getInstitutionSpecificAddress())) {
//             	   maganerView.setInstitutionSpecificAddress(entity1.getInstitutionSpecificAddress());
//		    		}
//                if(StringUtil.isNotBlank(entity1.getNature())) {
//             	   maganerView.setNature(entity1.getNature());
//		    		}
//                if(StringUtil.isNotBlank(entity1.getPortalAddress())) {
//             	   maganerView.setPortalAddress(entity1.getPortalAddress());
//		    		}
//                if(StringUtil.isNotBlank(entity1.getPortalType())) {
//             	   maganerView.setPortalType(entity1.getPortalType());
//		    		}
//                if(StringUtil.isNotBlank(entity1.getSchoolCode())) {
//             	   maganerView.setSchoolCode(entity1.getSchoolCode());
//		    		}
//                if(StringUtil.isNotBlank(entity1.getSchoolType())) {
//             	   maganerView.setSchoolType(entity1.getSchoolType());
//		    		}
//                if(StringUtil.isNotBlank(entity1.getTelphone())) {
//             	   maganerView.setTelphone(entity1.getTelphone());
//		    		}
//                
//		    		view.add(maganerView);
//		    	}
//		    	
//		    }
//				return view;
		//实例化学校集合
		    List<InstitutionEntity> enityList = new ArrayList<InstitutionEntity>();
		    List<InstitutionManagerView> view = new ArrayList<InstitutionManagerView>();
		    //查询学校信息
		    enityList = commonDao.searchList(entity, INSTITUTION_EXPORT_SEARCH);
		    //判断查询结果是否为空
		    if(null!=enityList && enityList.size()>0) {
		    	//循环遍历结果
		    	for(int i=0;i<enityList.size();i++) {
		    		InstitutionManagerView maganerView = new InstitutionManagerView();
		    		InstitutionEntity entity1 = new InstitutionEntity();
		    		entity1 = enityList.get(i);
		    		maganerView.setId(entity1.getId());
		    		maganerView.setInstitutionName(entity1.getInstitutionName());
		    		maganerView.setInstitutionType(entity1.getInstitutionType());
		    		//邮箱
		    		if(StringUtil.isNotBlank(entity1.getEmail())) {
		    			maganerView.setEmail(entity1.getEmail());
		    		}
		    		//隶属关系
                   if(StringUtil.isNotBlank(entity1.getAffiliation())) {
                	   maganerView.setAffiliation(entity1.getAffiliation());
		    		}
                   //是否合作
                   if(StringUtil.isNotBlank(entity1.getIfCooperation())) {
                	   maganerView.setIfCooperation(entity1.getIfCooperation());
		    		}
                   //机构简称
                   if(StringUtil.isNotBlank(entity1.getInstitutionAbbreviation())) {
                	   maganerView.setInstitutionAbbreviation(entity1.getInstitutionAbbreviation());
		    		}
                   //机构地址
                   if(StringUtil.isNotBlank(entity1.getInstitutionAddress())) {
                	   maganerView.setInstitutionAddress(entity1.getInstitutionAddress());
		    		}
                   //机构具体地址
                   if(StringUtil.isNotBlank(entity1.getInstitutionSpecificAddress())) {
                	   maganerView.setInstitutionSpecificAddress(entity1.getInstitutionSpecificAddress());
		    		}
                   //机构性质
                   if(StringUtil.isNotBlank(entity1.getNature())) {
                	   maganerView.setNature(entity1.getNature());
		    		}
                   //门户地址
                   if(StringUtil.isNotBlank(entity1.getPortalAddress())) {
                	   maganerView.setPortalAddress(entity1.getPortalAddress());
		    		}
                   //门户类型
                   if(StringUtil.isNotBlank(entity1.getPortalType())) {
                	   maganerView.setPortalType(entity1.getPortalType());
		    		}
                   //学校编码
                   if(StringUtil.isNotBlank(entity1.getSchoolCode())) {
                	   maganerView.setSchoolCode(entity1.getSchoolCode());
		    		}
                   //获取学校类型
                   if(StringUtil.isNotBlank(entity1.getSchoolType())) {
                	   maganerView.setSchoolType(entity1.getSchoolType());
		    		}
                   //获取手机信息
                   if(StringUtil.isNotBlank(entity1.getTelphone())) {
                	   maganerView.setTelphone(entity1.getTelphone());
		    		}
                   
		    		view.add(maganerView);
		    	}
		    	
		    }
				return view;
	}
    /**
     * 获取所有学校信息
     * @param query
     * @return
     */
	public ResultBean getTeacherAll(InstitutionEntity query) {
		query.setInstitutionType("学校");
		return commonDao.searchList4Page(query, INSTITUTION_SEARCH);
	}
  /**
   * 根据学校id查询
   * @param id
   * @return
   */
	public ResultBean searchSchool(String id) {
		InstitutionEntity entity = new InstitutionEntity();
		ResultBean result = new ResultBean();
		  entity.setId(id);
		result.setData(commonDao.searchOneData(entity, INSTITUTION_SEARCH_BY_ID));
		return result;
	}
    /**
     * 保存课程分类
     * @param data
     */
	public void insertCourseCatagory(TbInstitutionEvalutionManagerView data) {
		        //随机生成id
				data.setId(IDGenerator.genUID());
				// 通过积分显示价格
				if (data.getPrice() != null) {
					if (TpConfigUtil.displayPriceByPoints()) {
						data.setPrice(data.getPrice().divide(new BigDecimal(TpConfigUtil.getPointsForSmallChange())));
					}
				}
				//设置机构评价对象
				TbInstitutionEvalutionManagerView view = splitActivity(data.getActivitys());
				data.setFlagDiscuss(view.getFlagDiscuss());
				data.setFlagNote(view.getFlagNote());
				data.setFlagQa(view.getFlagQa());
				data.setFlagVote(view.getFlagVote());
				commonDao.insertData(data, INSTITUTION_INSERT);
				// 新增分类信息
				if (StringUtil.isNotEmpty(data.getFirstTag())) {
					insertCatagorys(data.getId(), data.getFirstTag(), data.getSecondTag());
				}
				
	}
	/**
	 * 获取活动标识
	 * 
	 * @param activitys 活动标识值
	 * @return 返回活动标识
	 */
	private TbInstitutionEvalutionManagerView splitActivity(String activitys) {
		TbInstitutionEvalutionManagerView view = new TbInstitutionEvalutionManagerView();

		view.setFlagNote(activitys.indexOf(FLAG_NOTE) > -1 ? FLAG : FLAG_NOT);
		view.setFlagDiscuss(activitys.indexOf(FLAG_DISCUSS) > -1 ? FLAG : FLAG_NOT);
		view.setFlagQa(activitys.indexOf(FLAG_QA) > -1 ? FLAG : FLAG_NOT);
		view.setFlagVote(activitys.indexOf(FLAG_VOTE) > -1 ? FLAG : FLAG_NOT);

		return view;
	}
    /**
     * 更新课程
     * @param data
     */
	public void updateCourseAll(TbInstitutionEvalutionManagerView data) {
		TbInstitutionEvalutionManagerView view = splitActivity(data.getActivitys());
		data.setFlagDiscuss(view.getFlagDiscuss());
		data.setFlagNote(view.getFlagNote());
		data.setFlagQa(view.getFlagQa());
		data.setFlagVote(view.getFlagVote());
		commonDao.updateData(data, INSTITUTION_UPDATE);
		// 更新分类
		if (StringUtil.isNotEmpty(data.getFirstTag())) {
			deleteCategorys(data.getId());
		}

		insertCatagorys(data.getId(), data.getFirstTag(), data.getSecondTag());
		
	}
		
	/**
	 * 评价分类插入
	 * 
	 * @param courseId   任务ID
	 * @param firstTag   一级标签ID
	 * @param secondTags 二级标签id
	 */
	private void insertCatagorys(String courseId, String firstTag, String secondTag) {
		TagLinkInstitutionEntity first = new TagLinkInstitutionEntity();
		first.setLinkId(courseId);
		first.setTagId(firstTag);
		first.setLinkType(LINK_TYPE_EVALUTION);
		super.insert(first, CourseDbConstants.TAG_INSERT_EVALUTION);
		TagLinkEntity second = new TagLinkEntity();
		second.setTagId(secondTag);
		second.setParentTagId(firstTag);
		second.setLinkId(courseId);
		second.setLinkType(LINK_TYPE_EVALUTION);
		super.insert(second, CourseDbConstants.TAG_INSERT_EVALUTION);
	}

	/**
	   * 评价分类删除
	 * 
	 * @param courseId 任务ID
	 */
	private void deleteCategorys(String courseId) {
		TagLinkInstitutionEntity tagLinkEntity = new TagLinkInstitutionEntity();
		tagLinkEntity.setLinkId(courseId);
		tagLinkEntity.setLinkType(LINK_TYPE_EVALUTION);
		super.delete(tagLinkEntity, CourseDbConstants.SQL_RAG_DELETE_EVALUTION);
	}
    /**
                 * 删除评价信息
     * @param id
     */
	public void deleteEvalution(String id) {
		//根据评价id删除
		TbInstitutionEvalutionEntity entity = new TbInstitutionEvalutionEntity();
		entity.setId(id);
		this.commonDao.deleteData(entity, INSTITUTION_EVALUTION_DELETE_BY_ID);
		//删除关联的标签信息
		TagLinkInstitutionEntity tagLinkEntity = new TagLinkInstitutionEntity();
		tagLinkEntity.setLinkId(id);
		tagLinkEntity.setLinkType(LINK_TYPE_EVALUTION);
		super.delete(tagLinkEntity, CourseDbConstants.SQL_RAG_DELETE_EVALUTION);
		
	}
    /**
     * 根据关联id删除
     * @param linkId
     * @param linkType
     * @param measuermentTypeQuestionnaire
     */
	public void delEvalutionMeasure(String linkId, String linkType, String measuermentTypeQuestionnaire) {
		          // 删除tb_measurement tb_question tb_question_option tb_user_measuerment_answer
				// tb_user_measurement_result
				MeasurementEntity measure = new MeasurementEntity();
				measure.setLinkId(linkId);
				measure.setLinkType(linkType);
				measure.setMeasureType(measuermentTypeQuestionnaire);
				//数据库删除操作
				commonDao.deleteData(measure, CourseDbConstants.SQL_DELETE_EVALUTION_MEASURE_BY_COURSER);
	}
  /**
   * 查询所有机构信息集合
   * @param entity
   * @return
   */
	public ResultBean searchInstitutionEvalutionList(InstitutionEntity entity) {
		return commonDao.searchList4Page(entity, INSTITUTION_SCHOOL_SEARCH);
	}
    /**
     * 查询所有学校信息的分页
     * @param query
     * @return
     */
	public ResultBean getEvalutionTeacherAll(InstitutionEntity query) {
		query.setInstitutionType("学校");
		return commonDao.searchList4Page(query, INSTITUTION_EVALUTION_SCHOOL_SEARCH);
	}
	  /**
                      * 查询所有学校信息
     * @param entity
     * @return
     */
	public ResultBean searchInstitutionEvalutionList1(InstitutionEntity entity) {
		return commonDao.searchList4Page(entity, INSTITUTION_SCHOOL_ALL_SEARCH);
	}
	 /**
	     * 查询所有学校信息
	* @param entity
	* @return
	*/
	public ResultBean searchInstitutionManagerEvalutionList(InstitutionEntity entity) {
		return commonDao.searchList4Page(entity, INSTITUTION_ORGAN_SCHOOL_SEARCH);
	}
	 /**
	     * 查询所有学校评价信息
	* @param entity
	* @return
	*/
	public ResultBean searchInstitutionTeacherEvalutionList(InstitutionEntity entity) {
		return commonDao.searchList4Page(entity, INSTITUTION_TEACHER_SCHOOL_SEARCH);
	}
	/**
	     * 查询所有学校信息
	* @param entity
	* @return
	*/
	public List<InstitutionEntity> seacherSchoolLocationList(InstitutionEntity entity) {
		return commonDao.searchList(entity, INSTITUTION_PERSON_TEACHER_SCHOOL_SEARCH);
	}
	/**
	     * 查询所有学校信息
	* @param entity
	* @return
	*/
	public List<InstitutionEntity> searchEdutionSchoolList(InstitutionEntity entity) {
		return commonDao.searchList(entity, INSTITUTION_EDUTION_TEACHER_SCHOOL_SEARCH);
	}
	/**
	     * 查询所有学校信息
	* @param entity
	* @return
	*/
	public InstitutionEntity seacherPersonEdution(InstitutionEntity entity) {
		return commonDao.searchOneData(entity, INSTITUTION_EDUTION_SCHOOL_SEARCH);
	}
	/**
	     * 查询所有学校信息
	* @param entity
	* @return
	*/
	public InstitutionEntity searchEdution(InstitutionEntity entity) {
		return commonDao.searchOneData(entity, INSTITUTION_EDUTION_SEARCH);
	}
    /**
     * 导出学校信息
     * @param entity
     * @return
     */
	public List<InstitutionManagerView> exportManagerInstitution(InstitutionEntity entity) {
		//根据查询条件查询所有信息
		  List<InstitutionEntity> enityList = new ArrayList<InstitutionEntity>();
		    List<InstitutionManagerView> view = new ArrayList<InstitutionManagerView>();
		    enityList = commonDao.searchList(entity, INSTITUTION_ORGAN_SCHOOL_SEARCH);
		    if(null!=enityList && enityList.size()>0) {
		    	//循环遍历查询结果
		    	for(int i=0;i<enityList.size();i++) {
		    		InstitutionManagerView maganerView = new InstitutionManagerView();
		    		InstitutionEntity entity1 = new InstitutionEntity();
		    		entity1 = enityList.get(i);
		    		maganerView.setId(entity1.getId());
		    		maganerView.setInstitutionName(entity1.getInstitutionName());
		    		maganerView.setInstitutionType(entity1.getInstitutionType());
		    		//邮箱
		    		if(StringUtil.isNotBlank(entity1.getEmail())) {
		    			maganerView.setEmail(entity1.getEmail());
		    		}
		    		//隶属关系
                 if(StringUtil.isNotBlank(entity1.getAffiliation())) {
              	   maganerView.setAffiliation(entity1.getAffiliation());
		    		}
                 //是否合作
                 if(StringUtil.isNotBlank(entity1.getIfCooperation())) {
              	   maganerView.setIfCooperation(entity1.getIfCooperation());
		    		}
                 //机构简称
                 if(StringUtil.isNotBlank(entity1.getInstitutionAbbreviation())) {
              	   maganerView.setInstitutionAbbreviation(entity1.getInstitutionAbbreviation());
		    		}
                 //机构地址
                 if(StringUtil.isNotBlank(entity1.getInstitutionAddress())) {
              	   maganerView.setInstitutionAddress(entity1.getInstitutionAddress());
		    		}
                 //机构具体地址
                 if(StringUtil.isNotBlank(entity1.getInstitutionSpecificAddress())) {
              	   maganerView.setInstitutionSpecificAddress(entity1.getInstitutionSpecificAddress());
		    		}
                 //性质
                 if(StringUtil.isNotBlank(entity1.getNature())) {
              	   maganerView.setNature(entity1.getNature());
		    		}
                 //门户地址
                 if(StringUtil.isNotBlank(entity1.getPortalAddress())) {
              	   maganerView.setPortalAddress(entity1.getPortalAddress());
		    		}
                 //门户类型
                 if(StringUtil.isNotBlank(entity1.getPortalType())) {
              	   maganerView.setPortalType(entity1.getPortalType());
		    		}
                 //学校代码
                 if(StringUtil.isNotBlank(entity1.getSchoolCode())) {
              	   maganerView.setSchoolCode(entity1.getSchoolCode());
		    		}
                 //机构类型
                 if(StringUtil.isNotBlank(entity1.getSchoolType())) {
              	   maganerView.setSchoolType(entity1.getSchoolType());
		    		}
                 //手机号码
                 if(StringUtil.isNotBlank(entity1.getTelphone())) {
              	   maganerView.setTelphone(entity1.getTelphone());
		    		}
		    		view.add(maganerView);
		    	}
		    }
				return view;
	}
    /**
     * 查询待办tb_measurement_institution数据集合信息方法
* @param queryInfo
* @return
*/
/*public ResultBean listdbSearch(MeasurementInstitutionEntity queryInfo) {
ResultBean result = new ResultBean();
//获取登录信息
String userId=WebContext.getSessionUserId();
//声明待办对象集合
List<MeasurementInstitutionEntity> yplist = new ArrayList<MeasurementInstitutionEntity>();
List<MeasurementInstitutionEntity> list = new ArrayList<MeasurementInstitutionEntity>();
//分页查询数据
result = commonDao.searchList4Page(queryInfo, SELECT_INSTITUTION_DB_LIST_MANAGER);
Object object = result.getData();
//循环遍历分页结果并赋值
list = (List<MeasurementInstitutionEntity>) object;
if(!list.isEmpty()) {
for(int i=0;i<list.size();i++) {
	String id = list.get(i).getId();
	//查询是否已经评价方法
	UserMeasurementResultEntity entity = new UserMeasurementResultEntity();
	entity.setMeasurementId(id);
	entity.setCreateUser(userId);
	entity = commonDao.searchOneData(entity, SELECT_QUETION_RESULT_LIST_MANAGER);
	//对象为空为待办
	if(null==entity) {
		list.get(i).setFlag("0");
		list.get(i).setFlagName("未评价");
		yplist.add(list.get(i));
	}
}
}
result.setData((Object)yplist);
return result;
}*/
/**
* 查询已办tb_measurement_institution数据集合信息方法
* @param queryInfo
* @return
*/
/*public ResultBean listybSearch(MeasurementInstitutionEntity queryInfo) {
ResultBean result = new ResultBean();
//获取登录信息
String userId=WebContext.getSessionUserId();
//声明已办对象集合
List<MeasurementInstitutionEntity> yplist = new ArrayList<MeasurementInstitutionEntity>();
List<MeasurementInstitutionEntity> list = new ArrayList<MeasurementInstitutionEntity>();
//分页查询数据
result = commonDao.searchList4Page(queryInfo, SELECT_INSTITUTION_DB_LIST_MANAGER);
Object object = result.getData();
//循环遍历分页结果并赋值
list = (List<MeasurementInstitutionEntity>) object;
if(!list.isEmpty()) {
for(int i=0;i<list.size();i++) {
	String id = list.get(i).getId();
	//查询是否已经评价方法
	UserMeasurementResultEntity entity = new UserMeasurementResultEntity();
	entity.setMeasurementId(id);
	entity.setCreateUser(userId);
	entity = commonDao.searchOneData(entity, SELECT_QUETION_RESULT_LIST_MANAGER);
	//对象不空为已办
	if(null!=entity) {
		list.get(i).setFlag("1");
		list.get(i).setFlagName("已评价");
		yplist.add(list.get(i));
	}
}
}
result.setData((Object)yplist);
return result;
}*/
    /**
     * 查询待办tb_measurement_institution数据集合信息方法
* @param queryInfo
* @return
*/
/*public ResultBean listdbSearch(MeasurementInstitutionEntity queryInfo) {
ResultBean result = new ResultBean();
//获取登录信息
String userId=WebContext.getSessionUserId();
//声明待办对象集合
List<MeasurementInstitutionEntity> yplist = new ArrayList<MeasurementInstitutionEntity>();
List<MeasurementInstitutionEntity> list = new ArrayList<MeasurementInstitutionEntity>();
//分页查询数据
result = commonDao.searchList4Page(queryInfo, SELECT_INSTITUTION_DB_LIST_MANAGER);
Object object = result.getData();
//循环遍历分页结果并赋值
list = (List<MeasurementInstitutionEntity>) object;
if(!list.isEmpty()) {
for(int i=0;i<list.size();i++) {
	String id = list.get(i).getId();
	//查询是否已经评价方法
	UserMeasurementResultEntity entity = new UserMeasurementResultEntity();
	entity.setMeasurementId(id);
	entity.setCreateUser(userId);
	entity = commonDao.searchOneData(entity, SELECT_QUETION_RESULT_LIST_MANAGER);
	//对象为空为待办
	if(null==entity) {
		list.get(i).setFlag("0");
		list.get(i).setFlagName("未评价");
		yplist.add(list.get(i));
	}
}
}
result.setData((Object)yplist);
return result;
}*/
	/**
	* 查询已办tb_measurement_institution数据集合信息方法
	* @param queryInfo
	* @return
	*/
	/*public ResultBean listybSearch(MeasurementInstitutionEntity queryInfo) {
	ResultBean result = new ResultBean();
	//获取登录信息
	String userId=WebContext.getSessionUserId();
	//声明已办对象集合
	List<MeasurementInstitutionEntity> yplist = new ArrayList<MeasurementInstitutionEntity>();
	List<MeasurementInstitutionEntity> list = new ArrayList<MeasurementInstitutionEntity>();
	//分页查询数据
	result = commonDao.searchList4Page(queryInfo, SELECT_INSTITUTION_DB_LIST_MANAGER);
	Object object = result.getData();
	//循环遍历分页结果并赋值
	list = (List<MeasurementInstitutionEntity>) object;
	if(!list.isEmpty()) {
	for(int i=0;i<list.size();i++) {
		String id = list.get(i).getId();
		//查询是否已经评价方法
		UserMeasurementResultEntity entity = new UserMeasurementResultEntity();
		entity.setMeasurementId(id);
		entity.setCreateUser(userId);
		entity = commonDao.searchOneData(entity, SELECT_QUETION_RESULT_LIST_MANAGER);
		//对象不空为已办
		if(null!=entity) {
			list.get(i).setFlag("1");
			list.get(i).setFlagName("已评价");
			yplist.add(list.get(i));
		}
	}
	}
	result.setData((Object)yplist);
	return result;
	}*/
}
