package com.cloudsoaring.web.institution.service;

import org.springframework.stereotype.Service;

import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.institution.entity.InstitutionOrganEntity;
import com.cloudsoaring.web.trainingplatform.constants.TpConstants;

/***
 * 班级管理相关的业务处理Service
 */
@Service
@SuppressWarnings("unchecked")
public class InstitutionOrganService extends FileService implements TpConstants{
    /**
     * 分页查询结构
     * @param entity
     * @return
     */
	public ResultBean searchInstitutionOrganList(InstitutionOrganEntity entity){
		//分页查询组织结构信息
		return commonDao.searchList4Page(entity, INSTITUTION_ORGAN_SEARCH);
	}
	/**
	 * 保存机构管理信息方法
	 * @param entity
	 */
	public void insertInstitutionOrgan(InstitutionOrganEntity entity){
		//保存机构信息
		this.commonDao.insertData(entity,INSTITUTION_ORGAN_ADD);
	}
	/**
	 * 根据主键查询机构信息
	 * @param id
	 * @return
	 */
	public InstitutionOrganEntity selectInstitutionOrganById(String id) {
		//实例化机构在组织信息
		InstitutionOrganEntity entity = new InstitutionOrganEntity();
		entity.setId(id);
		//查询机构信息
		return commonDao.searchOneData(entity, INSTITUTION_ORGAN_SEARCH_BY_ID);
	}
	/**
	 * 更新机构管理方法
	 * @param entity
	 */
	public void updateInstitutionOrgan(InstitutionOrganEntity entity){
		//更新机构信息
		this.commonDao.updateData(entity, INSTITUTION_ORGAN_UPDATE_BY_ID);
	}
}
