package com.cloudsoaring.web.institution.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.common.excel.ExcelDataBind;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.utils.FileUtil;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.view.InstitutionManagerView;
import com.cloudsoaring.web.course.view.TagView;
import com.cloudsoaring.web.institution.entity.InstitutionEntity;
import com.cloudsoaring.web.institution.service.InstitutionService;
import com.cloudsoaring.web.trainingplatform.constants.TpConstants;

/**
 * 机构列表controller控制层
 * 主要包括页面的增删改方法
 * 以及导入导出方法
 * 集成基础BaseController
 * 实现接口TpConstants
 * @RequestMapping请求路径
 */
@Controller
@RequestMapping("/manager/institution")
public class InstitutionController extends BaseController implements TpConstants{
     //注入InstitutionService的逻辑实现层
	//@Autowired自动注入bean对象
	 @Autowired
	 private InstitutionService institutionService;
	 
	    /**
	     * 初始化机构列表
	     * @param condition
	     * @return
	     */
	 	@RequestMapping("/institutionlist.action")
		public ModelAndView institutionlist(InstitutionEntity condition){
	 		//学校列表页
	        ModelAndView result = new ModelAndView("institution/manager/institution_list");
	        //设置每页显示的条数
			condition.setPageSize(10);
			//返回页面数据对象
	        result.addObject("condition", condition);
	        //返回结果
			return result;
		}
	 	
	 	/**
		 * 分页查询学校信息
		 * @param entity
		 * @return 返回结果
		 */
		@RequestMapping("/searchInstitution.action")
		@ResponseBody
		public ResultBean searchNotice(InstitutionEntity entity){
			//查询tb_institution数据表信息
			return institutionService.searchInstitutionList(entity);
		}
		
		/**
		 * 跳转到添加页面
		 * @param view 入参
		 * @return
		 */
		@RequestMapping("/institutionadd.action")
		public ModelAndView institutionAdd(TagView view){
			//学校添加页面
			ModelAndView mv = new ModelAndView("institution/manager/Institution_add");
			//返回页面信息
			return mv;
		}
		
		/**
		 * 保存新增学校信息
		 * @param entity
		 * @return 返回结果
		 */
		@RequestMapping("/insertInstitution.action")
		@ResponseBody
		public ResultBean insertInstitution(InstitutionEntity entity) throws Exception {
			//保存学校信息
			//getCompressConfigByName("imageP")为处理图像的方法
			institutionService.insertInstitution(entity,getCompressConfigByName("imageP"));
			//保存成功信息
			return ResultBean.success(MSG_ADD_GRADE_SUCCESS);
		}
		
		/**
		 * 跳转到班级编辑页面
		 * @param id
		 * @return 返回结果
		 */
		@RequestMapping("/institutionEdit.action")
		public ModelAndView institutionEdit(String id){
			//根据主键查询学校信息
			InstitutionEntity entity = institutionService.selectInstitutionById(id);
			//学校编辑页面
			ModelAndView mv = new ModelAndView("institution/manager/institution_edit");
			//返回前台对象
			mv.addObject("institution", entity);
			//返回页面
			return mv;
		}
		
		/**
		 * 编辑保存
		 * @param entity
		 * @return 返回结果
		 */
		@RequestMapping("/updateInstitution.action")
		@ResponseBody
		public ResultBean updateInstitution(InstitutionEntity entity) throws Exception{
			//更新机构信息
			//getCompressConfigByName("imageP")为处理图像的方法
			institutionService.updateInstitution(entity,getCompressConfigByName("imageP"));
			//返回编辑信息状态
			return ResultBean.success(EDIT_GRADE_SUCCESS);
		}
		
		/**
		 * 删除班级
		 * @param entity
		 * @return 返回结果
		 */
		@RequestMapping("/deleteInstitutionById.action")
		@ResponseBody
		public ResultBean deleteGradeById(InstitutionEntity entity)throws Exception{
			//根据主键删除机构信息
			institutionService.deleteInstitutionById(entity);
			//返回删除信息状态
			return ResultBean.success(DELETE_GRADE_SUCCESS);
		}
		/**
		 * 添加学校信息
		 * @param institutionParentId 父级id
		 * @return
		 * @throws Exception
		 */
		@RequestMapping("/addInstitution.action")
		public ModelAndView addInstitution(String institutionParentId) throws Exception {
			//实例化机构对象
			InstitutionEntity entity = new InstitutionEntity();
			//传入父级参数
			entity.setInstitutionParentId(institutionParentId);
			//添加学校页面
			ModelAndView mv = new ModelAndView("institution/manager/institution_addSchool");
			//封装前台返回的对象
			mv.addObject("institution", entity);
			//返回结果
			return mv;
		}	
		/**
		 * 增加学校方法
		 * @param entity 入参
		 * @return
		 * @throws Exception
		 */
		@RequestMapping("/addJyInstitution.action")
		public ModelAndView addJyInstitution(InstitutionEntity entity) throws Exception {
			//添加学校页面路径
			ModelAndView mv = new ModelAndView("institution/manager/institution_add");
			//返回前台对象
			mv.addObject("institution", entity);
			//返回结果
			return mv;
		}
		/**
		 * 导入页面初始化
		 * @return 返回结果
		 */
		@RequestMapping("/importInit.action")
		public String importInit(){
			//信息导入页面
			return "institution/manager/institution_import";
		}
	    /**
	     * 导入机构
	     * @param institution 结构实体类对象
	     * @param request 请求
	     * @param response响应
	     * @return 返回结果
	     * @throws Exception 异常处理
	     */
	    @RequestMapping("/importUpload.action")
	    @ResponseBody
	    public ResultBean importCourse(@RequestParam("institution") CommonsMultipartFile institution,HttpServletRequest request, HttpServletResponse response) throws Exception {
	    	//定义字节流、
	    	InputStream input = null;
			try{
				//获取导入文件信息流
				input = institution.getInputStream();
				//将字节流转化成文件对象
				File targetFile = FileUtil.writeToTmpFile(input);
				//导入学校机构信息
				return institutionService.importInstitution(targetFile);
			}finally{
				//关闭输入流
				IOUtils.closeQuietly(input);
			}
	   }
	    /**
	     * 批量导出机构
	     * @param entity 入参
	     * @return
	     * @throws Exception
	     */
	    @RequestMapping("/exportInstitution.action")
	    @ResponseBody
	    public List<InstitutionManagerView> exportInstitution(InstitutionEntity entity) throws Exception{
	    	//根据筛选条件查询学校信息
	    	List<InstitutionManagerView> user = institutionService.exportInstitution(entity);
	       //表格处理对象
	        ExcelDataBind edbUser = new ExcelDataBind(user);
	        //字节输入流
	        InputStream is = null;
	        //字节输出流
	        OutputStream os = null;
	        try {
	        	//时间格式化
	            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	            //时间转化成日期
	            String date = sdf.format(new Date());
	            //设置导出表格名称
	            String fileName = URLEncoder.encode("组织机构" + date +".xlsx", "UTF-8");
	            //重置响应信息
	            WebContext.getResponse().reset();
	            //设置导出模板
	            is = InstitutionService.class.getResourceAsStream("institutionExportTemplate.xlsx");
	            //设置消息头名称
	            WebContext.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
	            //设置响应的字节编码
	            WebContext.getResponse().setContentType(" application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8");
	            //处理excel信息
	            edbUser.bind(is,WebContext.getResponse().getOutputStream());
	        } catch(IOException ex){
	            //抛出异常信息
	        } finally{
	        	//关闭字符输入流
	            IOUtils.closeQuietly(is);
	            //关闭字节输出流
	            IOUtils.closeQuietly(os);
	        }
	        //返回
	        return null;
	    }
	    /**
	     * 学校首页
	     * @return
	     */
	    @RequestMapping("/institutionBack.action")
	 		public ModelAndView homePage() {
	    	//首页页面路径文件
	 			ModelAndView result = new ModelAndView("/institution/manager/institutionBack");
	 			return result;
	 }
}
