package com.cloudsoaring.web.course.constant;
/**
 * 
 * @author Admin
 *
 */
public interface CourseConfigNames {

	/**每隔多长时间更新一次视频学习进度*/
	public static final String CONFIG_UPDATE_USER_STUDY_PROGRESS_TIME = "update.user.study.progress.time";
}
