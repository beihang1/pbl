package com.cloudsoaring.web.course.constant;
/**
 * 
 * @author Admin
 *
 */
public interface CourseConstants extends CourseConfigNames, CourseDbConstants,
		CourseLogConstants, CourseDefaultValues, CourseMsgNames,
		CourseStateConstants {
	public static final String FILE_TYPE_HOMEWORK = "homework";//文件类型
	public static final String SUM_KBN_EARN = "A";//角色
	public static final String PLAN_CATEGORY_PLAN = "1";//一级分类
	public static final String PLAN_CATEGORY_CASE = "2";//二级分类
	public static final String FILE_TYPE_ZIP = "users";//文件压缩
	public static final String DELETE_GRADE_SUCCESS="delete.grade.success";//数据删除成功标识
	public static final String MSG_ADD_GRADE_SUCCESS = "msg.add.grade.success";//年级添加成功标识
	public static final String EDIT_GRADE_SUCCESS = "edit.grade.success";//年级编辑成功标识
}
