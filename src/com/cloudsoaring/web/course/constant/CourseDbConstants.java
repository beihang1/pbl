package com.cloudsoaring.web.course.constant;
/**
 * 
 * @author Admin
 *
 */
public interface CourseDbConstants {

	// ////////////标签///////////////////////
	/** 标签类列表 */
	public static final String SEARCH_TAG_TYPE_LIST = "TagView.searchTagTypeList";
	/** 标签类单项 */
	public static final String SEARCH_TAG_TYPE_ONE = "TagView.searchTagTypeOne";
	/** 根据linkId检索标签类及子标签 */
	public static final String SEARCH_TAG_BY_LINK_ID = "TagMap.searchTagListByLinkId";
	/** 标签列表 */
	public static final String SEARCH_TAG_LIST_BYTYPE="TagMap.searchTagListByType";
	/** 标签机构列表 */
	public static final String SEARCH_TAG_LIST_BYTYPE_INSTITUTION="TagInstitutionMap.select";
	/** 根据条件查询标签 */
	public static final String SEARCH_TAG_LIST_BY = "TagMap.searchTagListBy";
	/** 根据ID条件查询标签 */
	public static final String SEARCH_TAG_ONE_BY = "TagMap.searchTagOneBy";
	/** 根据ID条件查询一个 */
	public static final String SEARCH_TAG_BY_PK = "TagMap.selectByPk";	
	/** 根据标签名称查询Id */
    public static final String SQL_SEARCH_TAG_BY_NAME = "TagMap.selectByName";  
	/** 根据ID条件查询标签名 */
	public static final String SEARCH_TAG_NAME_BY_TAG_ID = "TagMap.searchTagNameByTagId";
	/** 根据TYPEID条件查询标签 */
	public static final String  SEARCH_TAG_LIST_BY_TYPE_ID="TagMap.searchAllType";
	/** 标签列表 */
	public static final String SEARCH_TAG_LIST = "TagMap.searchTagList";
	/**删除标签*/
	public static final String SQL_RAG_DELETE="TagView.deleteByCourse";
	/**删除评价标签*/
	public static final String SQL_RAG_DELETE_EVALUTION="TagLinkInstitution.deleteByCourse";
	/** 根据tagids检索taglink */
	public static final String SEARCH_TAG_LINK_BY_TAG_IDS = "TagLink.selectTagLinkByTagIds";
	/** 根据taglink检索tagName */
	public static final String SEARCH_TAG_NAME_BY_TAG_LINK = "TagLink.selectTagName";
	/** 保存数据 */
	public static final String INSERT_TAG_LINK = "TagLink.insert";
	/** 删除标签数据 */
	public static final String DELETE_TAG_LINK_ALL_BY_LINKID = "TagLink.deleteAllByLinkId";
	/** 根据标签类的业务类型检索标签类及其子标签 */
	public static final String TAG_TYPE_AND_SUB_TAG_SEARCH_BY_BUSSINESS_TYPE = "TagView.selectTagAndSubTagByBussinessType";
	/** 检索数据 */
	public static final String SEARCH_TAG_LINK_ALL_LINKID = "TagLink.selectAllByLinkId";
	/**检索课程、计划的全部标签*/
	public static final String SELECT_TAG_NAME_BY_BUSINESS_TYPE = "TagMap.selectTagNameByBusinessType";
	/**检索全部标签*/
	public static final String SEARCH_TAGALL = "TagMap.selectTagAll";
	// ////////////评价/////////////////////////
	/** 课程/章节-评价列表检索 */
	public static final String SCORE_DATA_SEARCH = "ScoreData.selectScoreDataView";
	/*** 检索用户是否评价章节或课程 */
	public static final String CHECK_USER_SCORE = "ScoreData.checkUserScore";
	/** 课程/章节-评价数据检索 */
	public static final String SCORE_BY_LINKID = "Score.select";
	/** 检索评价主题 */
	public static final String SEARCH_SCORE = "Score.select";
	/** 插入评价主题 */
	public static final String INSERT_SCORE = "Score.insert";
	/** 插入评价数据信息 */
	public static final String INSERT_SCORE_DATA = "ScoreData.insert";
	/** 计算平均分 */
	public static final String SQL_SEARCH_SCORE_AVG="ScoreData.searchScoreAvg";
	
	/** 更新平均分 */
	public static final String SQL_UPDATE_SCORE="Score.updateScore";
	/** 更新评价主题中的平均分 */
	public static final String UPDATE_SCORE_AVG = "Score.updateScoreAvg";
	/**更新辅导分数*/
	public static final String UPDATE_TUTOR_SCORE = "TutorDef.updateTutorScore";
	// //////////////课程//////////////////////
	/** 根据ID检索课程 */
	public static final String SEARCH_COURSE_BY_PK = "Course.selectByPk";
	public static final String SEARCH_COURSE_BY_ID = "Course.selectById";
	/** 根据课程ID检索课程详情 */
	public static final String SEARCH_COURSE_DETAIL_BY_ID = "Course.selectCourseDetailByCourseId";
	/** 根据资源ID查找资源详情 */
	public static final String SEARCH_RESOURCE_DETAIL_BY_ID = "Resource.selectByPk";
	public static final String SEARCH_RESOURCE_DETAIL_BY_ID_TWO = "Resource.selectByPkTwo";
	/** 根据UserId获取userName */
	public static final String SEARCH_USERNAME_BY_ID = "Resource.selectUserNameByUserId";
	/** 根据课程ID查询课程是否存在 */
	public static final String SEARCH_COUNT_COURSE_BY_ID = "Course.searchCountByCourseId";
	/** 根据ID检索推荐课程 */
	public static final String SEARCH_RECOMMEND = "Course.selectParentTagIdByLinkId";
	/** 检索课程列表 */
	public static final String SELECT_COURSE_LIST_ALL = "Course.selectCourseListAll";
	/** 系统人员登录保存信息 */
	public static final String INSERT_SYS_USER = "SysUser.insert";
	/** 操作系统日志保存方法 */
	public static final String INSERT_USER_OPER = "UserOper.insert";
	/** 系统日志保存方法 */
	public static final String INSERT_USER_LOG = "UserLog.insert";
	/**根据用户名查询用户表信息方法  */
	public static final String SEARCH_USER_BY_USERNAME = "UserManagement.selectUserByUsername";
	/** 检索课程 */
	public static final String SELECT_COURSE_LIST = "Course.selectCourse";
	/** 检索未进入课程的方法 */
	public static final String SELECT_NO_PERMISSIONS_COURSE_LIST = "Course.selectCourseNoPermissions";
	/** 检索计划课程 */
	public static final String SELECT_COURSE_LIST_PLAN = "Course.selectCourseByPlan";
	/** 检索导师课程 */
	public static final String SELECT_TEACHER_COURSE_LIST = "Course.selectTeacherCourse";
	/**插叙学生课程*/
	public static final String SELECT_STUDENT_COURSE_LISTS = "Course.selectStudentCourses";
	
	/** 检索导师ID */
	public static final String SQL_SEACH_TEACHER_ID="Course.seacheTeacherId";
	/**更新课程*/
	public static final String UPDATE_COURSE = "Course.update";
	/** 更新课程的评价人数 */
	public static final String UPDATE_COURSE_NUM_SCORE_USER = "Course.updateCourseNumScoreUser";
	/** 课程学习数+1 */
	public static final String UPDATE_COURSE_NUMSTUDY = "Course.updateCourseNumStudy";

	/** 课程浏览数+1 */
	public static final String UPDATE_COURSE_NUMVIEW = "Course.updateCourseViewNum";
	/** 课程收藏数+1 */
	public static final String UPDATE_COURSE_NUM_FAVORITE_ADD = "Course.updateCourseNumFavoriteAdd";
	/** 课程收藏数-1 */
	public static final String UPDATE_COURSE_NUM_FAVORITE_SUB = "Course.updateCourseNumFavoriteSub";
	/** 检索课程列表-后台管理 */
    public static final String SELECT_COURSE_LIST_MANAGER = "Course.selectManagerList";
	/**精品课程推荐或取消*/
    public static final String UPDATE_FLAGBOUTIQUE = "Course.updateFlagBoutique";
    /**首页轮播课程推荐或取消*/
    public static final String UPDATE_FLAGHOME = "Course.updateFlagHome";
    /**课程发布/取消发布*/
    public static final String UPDATE_STATUS = "Course.updateStatus";
    public static final String UPDATE_END_STATUS = "Course.updateEndStatus";
    /**更新分享状态方法*/
    public static final String UPDATE_SHARE_STATUS = "Course.updateShareStatus";
    /**更新图书馆状态方法*/
    public static final String UPDATE_LIB_FLAG = "Course.updateLibStatus";
    /**添加学员*/
    public static final String SQL_USER_COURSE_INSERT= "UserCourse.insert";
    /**更新学员*/
    public static final String SQL_USER_COURSE_UPPDATE= "UserCourse.update";
    public static final String SQL_USER_COURSE_SEARCH_COUNT= "UserCourse.selectCount";
    /**添加学员*/
    public static final String SQL_USER_ALL= "Course.seachUserAll"; 
    /**添加学校老师学员*/
    public static final String SQL_SCHOOL_USER_ALL= "Course.seachStudentUserAll"; 
    /**根据用户名查询用户信息*/
    public static final String SQL_SEARCH_MOBILE= "Course.seachUserByUserName"; 
    /** 插入课程 */
    public static final String COURSE_INSERT = "Course.insert";
    /**判断是否教师*/
    public static final String SQL_IS_TEACHHER_USER = "Course.isTeacher";
    /** 删除课程 */
    public static final String SQL_COURSE_DELETE = "Course.delete";
    /** 删除课程关联的计划 */
    public static final String SQL_DELETE_PLAN_COURSE="PlanCourse.deletePlanCourseByCourseId";
    /** 删除课程关联的章节 */
    public static final String SQL_DELETE_CHAPTER_COURSE="Chapter.deleteChapterCourseByCourseId";
    /**删除学员*/
    public static final String SQL_DELETE_USER_COURSE="UserCourse.delete";
    /** 删除参加课程的学员 */
    public static final String SQL_DELETE_USER_COURSE_BY_COURSER="UserCourse.deleteByCourseId";
    /** 删除课程下的作业 */
    public static final String SQL_DELETE_HOME_WORK_BY_COURSER="Homework.deleteHomeWorkByCourseId";
    /** 删除课程下的问卷 */
    public static final String SQL_DELETE_MEASURE_BY_COURSER="Measurement.deleteMeasureByLinkId";
    /**删除问卷、问答、测试、练习、考试以及相关的用户操作*/
    public static final String SQL_DEL_MEASURE="Measurement.delMeasure";
    /** 删除课程下的问答 */
    public static final String SQL_DELETE_QA_QUESTION_BY_COURSER="QaQuestion.deleteQaQuestionByCourseId";
    /**删除课程下的笔记*/
    public static final String SQL_DELETE_NOTE_BY_COURSER="Note.deleteNoteByCourseId";
    /**删除课程下的评论*/
    public static final String SQL_DELETE_DISCUSS_BY_COURSER="Discuss.deleteDiscussByCourseId";
    /** 更新课程 */
    public static final String  SQL_UPDATE_COURSE= "Course.updateCourseById";
    /**标签列表*/
    public static final String SQL_SEARCH_FIRST_TAG_TYPE="TagView.searchFirstTagType";
    /**查询标签关联表*/
    public static final String SQL_SEARCH_LINK_TAG_TYPE="TagLinkInstitution.selectAllByLinkId";
    /**标签机构列表*/
    public static final String SQL_SEARCH_FIRST_TAG_TYPE_INSTITUTION="TagTypeInstitution.searchFirstTagType";
    /**二级标签列表*/
    public static final String SQL_SEARCH_SECOND_TAG_TYPE="TagView.searchSecondTagType";
    /**二级标签列表*/
    public static final String SQL_SEARCH_SECOND_TAG_TYPE_INSTITUTION="TagTypeInstitution.searchSecondTagType";
    /** 我的课程检索  */
    public static final String SELECT_USER_COURSE_LIST = "Course.selectUserCourseList";
    /** 我的课程检索（全部）  */
    public static final String SELECT_USER_COURSE_ALL_LIST = "Course.selectUserCourseAllList";
    public static final String USER_SEARCH_CONDITION="Course.seachTeacher";
    /** 测试开启 */
    public static final String SQL_SEACH_FLAG_TEST = "Measurement.seachFlagTest";
    /** 作业开启 */
    public static final String SQL_SEACH_FLAG_TASK = "Homework.seachFlagTask";
    /**检索已参加该课程的人员*/
    public static final String SQL_USER_SEARCH_JOIN="Course.seachUserForJoin"; 
    /** 根据教师ID选择教师信息 */
    public static final String USER_SEARCH_ONE="Course.seachTeacherOne";
    /**课程新增：插入标签*/
    public static final String TAG_INSERT="TagLink.insertCourseTag";
    /**评价新增：插入标签*/
    public static final String TAG_INSERT_EVALUTION="TagLinkInstitution.insertCourseTag";
    /**查询当前 课程的标签*/
    public static final String SQL_SEARCH_TAG_BY_COURSE_ID="TagView.searchTagByCourseId";
    /**课程是否存在*/
    public static final String SQL_IS_COURSE_EXIST = "Course.isCourseExist";
    /**资源是否存在*/
    public static final String SQL_IS_RESOURCE_EXIST = "Resource.isResourceExist";
    /**当前课程是否加入*/
    public static final String SQL_IS_COURSE_JOIN = "Course.isCourseJoin";
    /**开启课程问卷标识*/
    public static final String SQL_UPDATE_FLAG_QUEATION="Course.updateFlagQuestion";
    /**更新章节的考试类型*/
    public static final String SQL_UPDATE_MEASURE_TYPE = "Course.updateMeasureType";
	// //////////////章节//////////////////////
    /** 根据章节ID，课程ID 检索 下一章节详细，ID不传则检索课程第一章*/
    public static final String CHAPTER_SELECT_NEXT_CHAPTER = "Chapter.selectNextChapter";
    
    /** 根据章节ID，课程ID 检索 上一章节详细，ID不传则检索课程最后一章*/
    public static final String CHAPTER_SELECT_PREVIOUS_CHAPTER = "Chapter.selectPreviousChapter";
    
	/** 根据课程ID查询课程章节大纲 */
	public static final String SEARCH_CHAPTER_OUT_LINE = "Chapter.searchChapterOutlineList";
	/** 根据ID检索章节 */
	public static final String SEARCH_CHAPTER_BY_PK = "Chapter.selectByPk";
	/** 根据ID检索章节 */
    public static final String SQL_SEARCH_CHAPTER_BY_ID = "Chapter.seachById";
    public static final String SQL_SEARCH_CHAPTER_ONE_DATA = "Chapter.selectOneData";
	/** 检索章节列表 */
    public static final String SEARCH_CHAPTER = "Chapter.select";
    public static final String SQL_SEARCH_CHAPTER_VIEW = "Chapter.selectView";
    /** 检索章节列表 */
    public static final String INSERT_CHAPTER = "Chapter.insert";
    public static final String SQL_INSERT_CHAPTER = "Chapter.insertImport";
    /** 检索章节列表:小章节 */
    public static final String SEARCH_CHAPTER_SMALL = "Chapter.selectByChapterFlag";
	/** 根据ID检索章节详情 */
	public static final String SEARCH_CHAPTER_BY_ID = "Chapter.selectById";
	/** 未登录：查询章节列表 */
	/** 已登录：查询章节列表 */
	public static final String SEARCH_CHAPTER_LIST_LOGGED = "Chapter.searchChapterListForLogged";
	/** 已登录：查询章节列表 */
	public static final String COURSE_CHAPTER_LIST_LOGGED = "Chapter.searchCourseChapterListForLogged";
	/** 章节发布/取消发布 */
	public static final String UPDATE_CHAPTER_STATUS = "Chapter.updateStatus";
	/** 章节删除 */
	public static final String SQL_CHAPTER_DEL = "Chapter.delete";
	/** 章节更新 */
	public static final String SQL_CHAPTER_UPDATE = "Chapter.update";
	public static final String CHAPTER_SELECT_BY_ID = "Chapter.selectChapterById";
	/** 查询子章节 */
	public static final String SQL_SEARCH_SMALL_CHPATER_BY_PARENTID = "Chapter.isHasSmallChapter";
	/**章节是否存在*/
	public static final String SQL_IS_CHAPTER_EXIST = "Chapter.isChapterExist";
	/**更新章节视频地址及时长*/
	public static final String SQL_UPDATE_CHAPTER_VIDEO_INFO = "Chapter.updateVideoInfo";
	public static final String SQL_UPDATE_CHAPTER_ORDER = "Chapter.updateOrder";
	public static final String SQL_UPDATE_CHAPTER_ORDER_AND_PARENT = "Chapter.updateOrderAndParent";
	/**检索课程下的第一章*/
	public static final String SQL_SEARCH_COURSE_FIRST_CHAPTER = "Chapter.selectCourseFirstChapter";
	// /////////用户课程////////////////////////
	public static final String SEARCH_CHAPTER_LIST_BY_COURSEID = "Chapter.searchChapterListByCourseId";
	/**根据课程id查询章节集合*/
	public static final String SEARCH_CHAPTER_LIST_BY_COURSEID_USE = "Chapter.searchChapterListByCourseId";
	/** 插入用户课程 */
	public static final String INSERT_USER_COURSE = "UserCourse.insert";
	/** 检索用户课程 */
	public static final String SEARCH_USER_COURSE = "UserCourse.select";
	/** 检索用户课程 */
    public static final String SQL_SEARCH_USER_COURSE_EXPORT = "UserCourse.searchUserStudy";
    public static final String SQL_SEARCH_USER_PLAN_EXPORT = "PlanUser.searchUserStudy";
    public static final String SQL_SEARCH_USER_SCORE_EXPORT= "UserCourse.searchUserScore";
    public static final String SQL_SEARCH_USER_POINT_EXPORT = " UserPoint.selectByPk";
	/** 更新用户课程 */
	public static final String UPDATE_USER_COURSE = "UserCourse.update";
	/**更新用户课程学习进度*/
	public static final String SQL_UPDATE_USER_COURSE_PROGRESS = "Chapter.updateCourseUserCourseProgress";
	public static final String SQL_SEARCH_COURSE_BY_CHAPTER_ID= "Chapter.searchCourseByChapterId";
	/**我的投票*/
	public static final String SEARCH_USER_VOTE = "UserCourse.selectUserVote";
	/**根据用户章节检索用户课程*/
	public static final String SQL_SEARCH_USER_COURSE_BY_USER_CHAPTER = "UserCourse.selectUserCourseByUserChapter";
	/**更新用户学习耗时*/
	public static final String SQL_UPDATE_USER_COURSE_COST_TIME = "UserCourse.updateUserCourseCostTime";
	// //////////用户章节/////////////////////////
	/** 根据ID检索章节用户条数 */
	public static final String SEARCH_USER_CHAPTER_BY_ID = "UserChatper.selectByMap";
	/** 根据插入用户章节信息 */
	public static final String INSERT_USER_CHAPTER = "UserChatper.insertMap";
	/** 更新用户章节学习进度 */
	public static final String UPDATE_USER_CHAPTER_STUDY_PROGRESS = "UserChatper.updateStudyProgress";
	/** 更新用户章节上一次视频时间进度 */
	public static final String UPDATE_USER_CHAPTER_LAST_VIDEO_TIME = "UserChatper.updateLastVideoTime";
	/** 更新用户章节耗费的时间 */
	public static final String UPDATE_USER_CHAPTER_TIME_COST = "UserChatper.updateTimeCost";
	/** 判断用户章节是否存在 */
	public static final String IS_EXIST_USER_CHAPTER = "UserChatper.selectByMap";
	/** 检索用户章节 */
	public static final String SEARCH_USER_CHAPTER = "UserChatper.selectByPk";
	/**用户章节是否存在*/
	public static final String SQL_USER_CHAPTER_EXIST = "UserChatper.isUserChapterExist";
	/**更新用户章节上一次学习章节*/
	public static final String SQL_UPDATE_USER_CHAPTER_LAST_CHAPTER = "UserChatper.updateLastChapter";

	// ////////////问题///////////////////////
	/** 插入问题表 */
	public static final String INSERT_QA_QUESTION = "QaQuestion.insert";
	/** 检索问题详情 */
	public static final String SEARCH_QAW_QUESTION = "QaAnswer.selectById";

	/** 保存回答问题 */
	public static final String INSERTE_QAW_QUESTION = "QaAnswer.insert";
	
	/** 查询当前用户回答问题数 */
	public static final String SQL_SEARCH_COUNT_BY_ID="QaAnswer.searchCountById";
	
	/** 增加浏览数 */
	public static final String UPDATE_VIEW_NUM= "QaQuestion.updateViewNum";
	
	/**采纳*/
	public static final String UPDATE_ADOPT="QaQuestion.updateAdopt";
	/***/
	public static final String SQL_SEARCH_QUESTION_QA="QaQuestion.searchQuestionQa";

	// 更新问题信息删除问题选项
	public static final String QUESTION_DELET_OPTION_BY_QUESTION = "Question.deletOptionByQuestion";
	// 更新问题信息删除问题选项
	public static final String QUESTION_INSTITUTION_DELET_OPTION_BY_QUESTION = "QuestionInstitution.deletOptionByQuestion";
	public static final String QUESTION_DELET = "Question.deletOptionByQuestion";
	// 根据问题信息删除用户回答的问题
	public static final String QUESTION_DELET_USER_ANSWER_BY_QUESTION = "Question.deletUserAnswerByQuestion";
	// 根据问题信息删除用户回答的问题
	public static final String QUESTION_INSTITUTION_DELET_USER_ANSWER_BY_QUESTION = "QuestionInstitution.deletUserAnswerByQuestion";

	// ////////////讨论///////////////////////
	/** 根据ID检索评论 */
	public static final String SEARCH_DISCUSS_COURES_LIST = "Discuss.searchDiscussCoures";
	/** 根据ID检索评论内容 */
	public static final String SEARCH_DISCUSS_DATE_LIST = "DiscussData.searchDiscussData";
	/** 检索所有评论 */
	public static final String SEARCH_DISCUSS_LIST = "Discuss.selectDiscussList";
	/** 检索所有评论 */
	public static final String SEARCH_ALL_DISCUSS_LIST = "Discuss.select";
	/** 新增发布评论 */
	public static final String INSERT_DISCUSS = "Discuss.insertDiscuss";
	/** 更新发布评论 */
	public static final String UPDATE_DISCUSS = "Discuss.update";
	/** 根据ID查询发布评论条数 */
	public static final String SELECT_DISCUSS_COUNT = "Discuss.selectCount";
	/** 新增发布评论内容 */
	public static final String INSERT_DISCUSS_DATA = "DiscussData.insertDiscussData";
	/** 发布评论内容 */
	public static final String SELECT_DISCUSS_DATA = "DiscussData.select";
	/**判断评论是否存在*/
	public static final String SQL_DISCUSS_IS_EXIST = "DiscussData.isExist";
	/**插入操作记录*/
	public static final String SQL_OPERATE_RECORD = "OperateRecord.insert";
	/**评论点赞数加1*/
	public static final String SQL_UPDATE_DISCUSS_NUM_LIKE_ADD ="DiscussData.updateDiscussNumLike";
	/**检索评论*/
	public static final String SQL_DISCUSS_SELECT = "Discuss.select";
	/**删除操作表数据*/
	public static final String SQL_DELETE_OPERATE_BY_LINK_AND_TYPE = "OperateRecord.deleteByLinkAndType";
	/*** 删除讨论数据 */
	public static final String DELETE_DISCUSS_DATA = "DiscussData.delete";
	/*** 删除讨论数据 */
	public static final String DELETE_ALL_DISCUSS_DATA = "DiscussData.deleteDiscussData";
	/*** 检索讨论类型combobox */
	public static final String SEARCH_LINK_TYPE_LIST = "Discuss.selectLinkTypeList";
	/** 检索我的课程-评论 */
	public static final String SELECT_USER_DISCUSS_LIST = "Discuss.selectUserDiscussList";
	/**检索活动评论*/
	public static final String SQL_SEARCH_DISCUSS_ACTIVE_LIST = "DiscussData.searchDiscussActiveByLinkId";
	// ////////////计划///////////////////////
	/**计划是否存在*/
	public static final String SQL_IS_PLAN_EXIST = "Plan.isPlanExist";
	/**检索计划下课程用户信息*/
	public static final String SQL_SELECT_COURSE_USER_IN_PLAN = "Plan.selectCourseUserInPlan";
	/** 根据ID检索计划 */
	public static final String SEARCH_PLAN_BY_PK = "Plan.selectByPk";
	/** 检索我的学习报告完成情况 */
	public static final String SELECT_PLAN_STUDY_REPORT_INFO = "Plan.selectPlanStudyReportInfo";
	/** 检索我的学习报告七日学习情况 */
	public static final String SELECT_PLAN_SEVEN_STUDY_INFO = "Plan.selectPlanSevenStudyInfo";
	/** 更新计划的浏览数 */
	public static final String UPDATE_PLAN_VIEW_NUM = "Plan.updatePlanViewNum";
	/** 根据计划id和用户id检索计划详情（计划节点和包含的内容） */
	public static final String SEARCH_DETAIL_BY_ID = "Plan.selectDetailByPk";
	/** 计划列表 */
	public static final String SEARCH_PLAN_LIST = "Plan.searchPlanList";
	/** 检索有效计划 */
	public static final String SEARCH_VALID_PLAN_TYPE = "Plan.validplanList";
	/** 已参加的内训课程 */
	public static final String SEARCH_PLAN_LIST_TRAIN = "Plan.searchPlanTypeTrian";
	/** 已参加的非内训课程 */
	public static final String SEARCH_PLAN_LIST_NOT_TRAIN = "Plan.searchPlanTypeNotTrian";
	/** 查询计划列表 */
	public static final String SEARCH_PLAN_AND_PLANNODENUM = "Plan.selectPlanAndPlanNodeNum";
	/** 删除计划 */
	public static final String DELETE_PLAN = "Plan.delete";
	/** 删除计划步骤 */
	public static final String DELETE_PLAN_NODE_ALL = "PlanNode.deletePlanNodeAll";
	/** 删除计划课程 */
	public static final String DELETE_PLAN_COURSE_ALL = "PlanCourse.deleteCourseAll";
	/** 根据planNodeId检索课程 */
	public static final String SEARCH_PLANCOURSE_BY_PLANNODEID = "Course.selectCourseByPlanNodeId";
	/** 新增计划 */
	public static final String INSERT_PLAN = "Plan.insertPlan";
	/**计划学习人数加一*/
	public static final String UPDATE_NUMSTUDY1 = "Plan.updateNumStudy1";
	/** 修改计划 */
	public static final String UPDATE_PLAN = "Plan.updatePlan";
	/**查询计划更具id*/
	public static final String SELECT_PLAN = "Plan.select";
	/** 修改计划 */
	public static final String UPDATE_PLAN_NUMSTUDY = "Plan.updateNumStudy";
	/** 新增计划步骤 */
	public static final String INSERT_PLANNODE = "PlanNode.insert";
	/** 新增计划课程 */
	public static final String INSERT_PLANCOURSE = "PlanCourse.insert";
	/** 检索计划 */
	public static final String SQL_SEARCH_PLAN_BY_ID = "Plan.selectPlan";
	/** 查询一条plannode */
	public static final String SQL_SEARCH_PLANNODE_BY_PLANID = "PlanNode.selectByPlanId";
	/** 查询是否学习过计划 */
	public static final String SQL_SEARCH_STUDY_HISTORY_DATA = "PlanStudyHistory.selectStudyHistoryByPlanId";
	/** 检索最后一次学习课程ID */
	public static final String SQL_SEARCH_LAST_STUDY_COURSE_ID = "PlanStudyHistory.selectLastStudyCourseId";
	/**插入用户章节到计划学习历史表*/
	public static final String SQL_INSERT_PLAN_CHAPTER_HISTORY = "PlanStudyHistory.insert";
	/** 检索全部课程数量 */
	public static final String SEARCH_PLAN_COURSE = "PlanCourse.selectPlanCourse";
	// /////////////用户计划///////////////////
	/** 节点下的课程列表 */
	public static final String SQL_SELECT_NODE_COURSE = "PlanCourse.selectNodeCourse";
	/**用户课程*/
	public static final String SQL_SEARCH_USER_COURSE_COUNT="UserCourse.selectCountStudy";
	/** 是否参加计划 */
	public static final String SEARCH_PLAN_JOIN = "PlanUser.searchPlanJoin";
	/** 根据计划id和用户id检索用户计划表 */
	public static final String SEARCH_USER_PLAN = "PlanUser.selectByPlanIdAndUserId";
	/** 根据planId查询用户信息 */
	public static final String SEARCH_PLANUSERID_BY_PLANID = "PlanUser.selectUserByPlanId";
	/** 根据planId userId信息 */
	public static final String SQL_SEARCH_PLANUSERID_BY_PLANID_USERID = "PlanUser.select";
	/** 新增计划人员全部 */
	public static final String INSERT_PLANUSER_ALL="PlanUser.insertPlanUser";
	/** 新增计划人员 */
	public static final String INSERT_PLANUSER = "PlanUser.insert";
	/** 检索所有人员 */
	public static final String SEARCH_PLANUSER_ALL = "UserManagement.searchPlanUserAll";
	/** 检索计划管理者 */
	public static final String SEARCH_MANAGE_USER = "PlanUser.searchManageUser";
	/** 删除计划下所有用户 */
	public static final String DELETE_PLAN_USER_ALL = "PlanUser.deleteUserAll";
	public static final String SQL_DELETE_PLAN_USER= "PlanUser.deleteUserById";
	/** 删除计划下所有用户 */
	public static final String DELETE_PLAN_USER = "PlanUser.delete";
	/** 查询计划用户 */
	public static final String SEARCH_PLAN_USER_IDS = "PlanUser.selectPlanUserIds";
	/** 查询所有用户数量 */
	public static final String SEARCH_PLAN_USER_COUNT = "PlanUser.selectPlanUserCount";
	
	/** 接口-我的计划（1已参与）*/
	public static final String SELECT_USER_PLAN_JOIN_LIST = "Plan.selectUserPlanJoinList";
	
	/** 接口-我的计划（1已参与）：标签=全部 */
	public static final String SELECT_USER_PLAN_JOIN_ALL_LIST = "Plan.selectUserPlanJoinAllList";
	
	/** 接口-我的计划（1已完成）*/
	public static final String SELECT_USER_PLAN_FINISH_LIST = "Plan.selectUserPlanFinishList";
	
	/** 接口-我的计划（1已完成）：标签=全部 */
	public static final String SELECT_USER_PLAN_FINISH_ALL_LIST = "Plan.selectUserPlanFinishAllList";
	
	
	// /////////笔记/////////////////////////
	/** 推荐课程笔记 */
	public static final String SEARCH_NOTE = "NoteData.selectNoteList";
	/**检索课程的笔记*/
	public static final String SEARCH_COURSE_NOTE = "NoteData.selecCoursetNoteList";
	/** 用户采集笔记列表 */
	public static final String SEARCH_USER_COLLECT = "NoteData.selectUserCollectList";
	/** 用户笔记列表 */
	public static final String SEARCH_USER_NOTE = "NoteData.selectUserNoteList";
	/**检索笔记*/
	public static final String SQL_SEARCH_NOTE = "Note.select";
	/**判断笔记是否可以删除*/
	public static final String SQL_NOTE_DATA_CAN_DELETE = "NoteData.canDeleteById";
	/**新增笔记*/
	public static final String SQL_INSERT_NOTE = "Note.insert";
	/**新增笔记数据*/
	public static final String SQL_INSERT_NOTE_DATA = "NoteData.insert";
	/** 根据ID笔记点赞 */
	public static final String NOTE_LIKE = "NoteData.noteClickLike";
	/** 根据ID笔记采集 */
	public static final String NOTE_COLLECT = "NoteData.noteCollect";
	/** 问卷 */
	public static final String MEASUREMENT_SELECT = "Measurement.select";
	/** 问卷 */
	public static final String MEASUREMENT_INSERT = "Measurement.insert";
	/**查询问卷：LinkId*/
	public static final String SQL_SEACH_ONE_MEASURE="Measurement.seachOneByLinkId";
	/**更新问卷*/
	public static final String SQL_UPDATE_MEASURE="Measurement.update";
	/**更新问卷*/
	public static final String SQL_UPDATE_INSTITUTION_MEASURE="MeasurementInstitution.update";
	/**更新考试*/
    public static final String SQL_UPDATE_MEASURE_POINT="Measurement.updatePoint";
	/**查询问卷是否存在*/
    public static final String SQL_SEACH_ONE_IS_MEASURE="Measurement.seachIsByLinkId";
	
	public static final String SQL_SEACH_MEASURE_BY_COURSE_ID="Measurement.seachMeasureByCourseId";
	/** 判断是否为教师 */
	public static final String SQL_IS_TEACHHER = "NoteData.isTeacher";
	/**检索笔记数据是否存在*/
	public static final String SQL_IS_NOTE_DATA_EXIST = "NoteData.isExist";
	/**根据ID检索笔记数据*/
	public static final String SQL_SEARCH_NOTE_DATA_BY_PK = "NoteData.selectByPk";
	/**根据ID删除笔记数据*/
	public static final String SQL_DELETE_BY_PK = "NoteData.delete";
	/**删除操作表关于对应笔记内容数据*/
	public static final String SQL_DELETE_OPERATE_RECORD_BY_LINK = "OperateRecord.deleteByLink";
	// 获取课程下的最大章节数
	public static final String CHAPTER_SEARCH_MAX_ORDER = "Chapter.searchMaxOrder";
	/** 问卷题目 */
	public static final String QUESTION_SELECT_VIEW = "Question.selectView";
	/** 问卷题目 */
	public static final String QUESTION_SELECT = "Question.select";
	
	public static final String QUESTION_UPDATE = "Question.update";
	/** 提交问卷问题参与人数加1 */
	public static final String QUESTION_UPDATE_NUM = "Question.updateNumUser";
	public static final String QUESTION_MAX_ORDER = "Question.searchMaxOrder";
	/** 问卷题目加选项 */
	public static final String QUESTION_OPTION_SELECT = "QuestionOption.select";
	/** 问卷选项数据修改 */
	public static final String QUESTION_OPTION_UPDATE = "QuestionOption.update";
	
	public static final String QUESTION_OPTION_UPDATE_NUM = "QuestionOption.updateNum";
	/**问卷提交answer*/
	public static final String MEASUERMENT_ANSWER_INSERT = "UserMeasuermentAnswer.insert";
	/**问卷提交result*/
	public static final String MEASUERMENT_RESULT_INSERT = "UserMeasurementResult.insert";
	// ////////////案例//////////////////////
	/** 根据课程/章节ID检索案例 */
	public static final String SEARCH_SHARE_LIST = "Share.selectById";
	/** 我的案例 */
	public static final String SELECT_MY_SHARE = "Share.selectMyShare";
	
	/** 检索问题选项 */
	public static final String MEASUERMENT_SEARCH_QUESTION_LIST_BY_MEASUREMENT = "Measurement.selectQuestionResult";
	/** 检索问题 */
	public static final String MEASUERMENT_SEARCH_QUESTION_OPTIONS_LIST = "QuestionOption.select";
	/** 检索问题 */
	public static final String MEASUERMENT_INSTITUTION_SEARCH_QUESTION_OPTIONS_LIST = "QuestionOptionInstitution.select";
	// 检索问题
	public static final String MEASUERMENT_SEARCH_QUESTION_LIST = "Question.select";
	// 检索问题
	public static final String MEASUERMENT_INSTITUTION_SEARCH_QUESTION_LIST = "QuestionInstitution.select";
	public static final String MEASUERMENT_ANSWER_DELETE = "UserMeasuermentAnswer.delete";
	public static final String USER_MEASUERMENT_ANSWER_INSERT = "UserMeasuermentAnswer.insert";
	// 插入考试答案历史表
	public static final String USER_MEASUREMENT_ANSWER_INSERTHIS = "UserMeasuermentAnswer.insertHis";
	// 更新考试结果历史表
	public static final String USER_MEASUREMENT_RESULT_UPDATEHIS = "UserMeasurementResult.updateHis";
	// 根据主键删除用户回答信息
	public static final String USER_MEASUERMENT_RESULT_DELETE = "UserMeasurementResult.delete";
	public static final String USER_MEASUERMENT_RESULT_INSERT = "UserMeasurementResult.insert";
	// 插入考试结果历史表
	public static final String USER_MEASUREMENT_RESULT_INSERTHIS = "UserMeasurementResult.insertHis";
	/** 注册用户 */
	public static final String INSERT_HSUSER = "User.insertUser";
	/** 更新用户课程投票 **/
	public static final String UPDATE_USER_COURSE_VOTE = "UserCourse.updateVote";
	/** 更新课程投票信息 **/
	public static final String UPDATE_COURSE_VOTE = "Course.updateVote";
	/** 检索课程投票人数 **/
	public static final String SELECT_USER_COURSE_VOTE_COUNT = "UserCourse.selectCountVoteByChapter";
	/** 更新章节投票信息 **/
	public static final String UPDATE_CHAPTER_VOTE = "Chapter.updateVote";
	/** 更新用户章节投票 **/
	public static final String UPDATE_USER_CHAPTER_VOTE = "UserChatper.updateVote";
	/** 检索章节投票人数 **/
	public static final String SELECT_USER_CHAPTER_VOTE_COUNT = "UserChatper.selectCountVoteByChapter";
	/** 检索用户答题情况 */
	public static final String SQL_MEASUERMENT_USER_SEARCH = "UserMeasurementResult.selectMeasurementUser";
	public static final String SQL_MEASUERMENT_SEARCH_QUESTION_LIST_BY_MEASUREMENT = "Measurement.selectQuestionResult";
	//删除某个用户的答题
    public static final String SQL_MEASUREMENT_DELETE_ANSWER_OF_USER = "Measurement.deleteAnswerOfUser";
	/** 检索测试 */
	public static final String SQL_MEASUERMENT_SEARCH = "Measurement.select";
	/** 检索测试 */
	public static final String SQL_INSTITUTION_MEASUERMENT_SEARCH = "MeasurementInstitution.select";
	/** 检索用户测试数据 */
	public static final String SQL_USER_MEASUERMENT_SELECT = "UserMeasurementResult.select";
	/**添加作业*/
	public static final String INSERT_HOME_WORK="Homework.insert";
	/**
	 * 查询我的作业
	 */
	public static final String SEARCH_HOME_WORK_ONE="HomeworkAnswer.selectMyHomework";
	
	/**
	 * 查询我的作业和课程
	 */
	public static final String SEARCH_HOME_WORK_TITLE="Homework.selectHomeworkTitle";
	
	public static final String SEARCH_HOME_WORK_BY_LINK_ID = "Homework.select";
	/**查询作业列表*/
	public static final String HOME_WORK = "Homework.selectHomeWorkPage";
	/**查询作业列表*/
	public static final String UAER_HOME_WORK = "Homework.selectHomeWorkUserPage";
	public static final String TEAM_HOME_WORK = "Homework.selectHomeWorkTeamPage";
	public static final String TEAM_UNCOMMIT_HOME_WORK = "Homework.selectHomeWorkTeamUnCommit";
	/**查询学生作业列表*/
	public static final String STUDENT_HOME_WORK = "Homework.selectStudentHomeWorkUserPage";
	/**查询学生作业详情*/
	public static final String STUDENT_HOME_WORK_DETAIL = "Homework.searchStudentHomeWorkUser";
	/**查询作业列表*/
	public static final String UAER_ANS_HOME_WORK = "HomeworkAnswer.update";
	/**查询作业列表*/
	public static final String DELETE_UAER_ANS_HOME_WORK = "HomeworkAnswer.delete";
	/**查询作业列表*/
	public static final String INSERT_UAER_ANS_HOME_WORK = "HomeworkAnswer.insertAnswer";
	public static final String SELECT_UAER_ANS_HOME_WORK = "HomeworkAnswer.select";
	/**
	 * 查询我的作业和章节
	 */
	public static final String SEARCH_HOME_WORK_TITLE_C="Homework.selectHomeworkTitleC";
	/**
	 * 查询设置的作业
	 */
	public static final String SEARCH_HOME_WORK="Homework.selectHomeWork";
	
	/**
	 * 编辑的作业
	 */
	public static final String UPDATE_HOME_WORK="Homework.updateHomeWork";
	
	/**
	 * 查询设置的作业
	 */
	public static final String UPLOAD_HOME_WORK="HomeworkAnswer.insertAnswer";
	/**
	 * 保存unity的传来的作业数据
	 */
	public static final String SAVE_HOME_WORK="HomeworkAnswer.insertAnswerByUnity";
	/**
	 * 保存pc端的答题数据
	 */
	public static final String SAVE_HOME_WORK_PC="HomeworkAnswer.insertAnswerByPC";
	
	/**
	 * 用户是否点赞
	 */
	public static final String SEARCH_DISCUSS_ISLIKE="OperateRecord.seachCount";
	
	/**根据课程ID检索课程所有数据*/
	public static final String SEARCH_COURSE_DATA = "Course.searchCourseData";
	/**根据章节ID检索章节的所有数据*/
	public static final String SEARCH_CHAPTER_DATA = "Chapter.searchChapterData";
	
	
	/**
	 * 参加课程的用户
	 */
	public static final String SEARCH_USER_COUSE="UserCourse.searchUserCouse";
	/**检索课程中的好/中/差评票数 */
	public static final String SEARCH_COUNT_VOTE_COURSE="UserCourse.searchCountVoteNumber";
	/**检索章节中的好/中/差评票数 */
	public static final String SEARCH_COUNT_VOTE_CHAPTER="UserChatper.searchCountVoteNumber";
	
	/**
	 * 查询全部用户的答题信息
	 */
	public static final String SEARCH_TEACHER_USER_HOME="HomeworkAnswer.selectAllUserHomework";
	/**
	 * 参加章节的用户
	 */
	public static final String SEARCH_CHAPTER_USER="UserChatper.searchChapterUser";
	
	/**
	 * 提交的作业
	 */
	public static final String SEARCH_USER_HOME="HomeworkAnswer.selectUserHomework";
	/**
	 * 查询全部用户的答题信息
	 */
	public static final String SEARCH_ALL_USER_HOME="HomeworkAnswer.selectAllUserHomework";
	/**
	 * 查询全部组群的答题信息
	 */
	public static final String SEARCH_ALL_TEAM_HOME="HomeworkAnswer.selectAllTeamHomework";
	/**
	 * 查询某个组群的答题信息
	 */
	public static final String SEARCH_TEAM_USERS_HOME_BY_USER ="HomeworkAnswer.selectTeamUsersHomework";
	
	public static final String SEARCH_HOME_WORK_BY_ID ="HomeworkAnswer.selectByPkForView";
	
	public static final String SEARCH_USER_HOME_BY_LINKID="Homework.selectHomeWorkByLinkId";
	
	/**
	 * 删除的作业
	 */
	public static final String UPDATE_HOME_WORK_AN="HomeworkAnswer.updateHomeWorkAnwer";
	
	public static final String UPDATE_HOME_WORK_SCORE="HomeworkAnswer.updateHomeWorkAnwerById";
	
	public static final String UPDATE_HOME_WORK_ANSWER_STATUS="HomeworkAnswer.updateHomeWorkAnwerStatus";
	
	/**
	 * 更新作业
	 */
	
	public static final String SELECT_HOME_WORK="HomeworkAnswer.selectAnswer";
	
	public static final String SELECT_HOME_WORK_BY_ID="HomeworkAnswer.selectByPk";
	
	public static final String UPDATE_USER_HOME_WORK="HomeworkAnswer.updateAnswer";
	
	public static final String UPDATE_TEACHER_REVIEW_HOME_WORK="HomeworkAnswer.updateTeacherReview";
	
	public static final String UPDATE_USER_HOME_WORK_LIKE="HomeworkAnswer.updateHomeWorkAnwerLike";
	
	public static final String SEARCH_HOME_BY_USER_CHAPTER ="HomeworkAnswer.selectHomeworkByUserIdChapterId";
	/**
	 * 我的作业
	 */
	public static final String SEARCH_USER_HOMEWORK="HomeworkAnswer.selectUserHomeworks";
	/** 根据课程ID获取作业 */
	public static final String SEARCH_USER_HOMEWORK_BY_COURSEID="HomeworkAnswer.selectHomeworksByCourseId";
	
	// //////////////应聘讲师//////////////////////
	/**应聘讲师*/
	public static final String INSERT_TEACHER_APPLY="TeacherApply.insert";
	/**检索讲师提交信息*/
	public static final String SEARCH_TEACHER_APPLY_ALL="TeacherApply.selectAll";
	/**检索讲师提交信息*/
	public static final String SEARCH_TEACHER_APPLY_BY_PK="TeacherApply.selectByPk";
	/**检索讲师提交信息*/
	public static final String UPDATE_TEACHER_APPLY_APPROVE="TeacherApply.updateTeacherApplyApprove";
	/**检索讲师最近提交信息的状态*/
	public static final String SEARCH_TEACHER_APPLY_STATUS="TeacherApply.selectTeacherApplyStatus";
	/**添加UserRole信息*/
	public static final String INSERT_USER_ROLE="UserRole.insert";
	/**检索UserRole信息*/
	public static final String SEARCH_USER_ROLE="UserRole.select";
	/**添加SocialMaster社交导师信息*/
	public static final String INSERT_SOCIALMASTER="SocialMaster.insert";
	/**检索用户积分*/
	public static final String SELECT_USER_POINT="UserPoint.select";
	
	// ////////////案例///////////////////////
	/** 根据ID检索案例 */
	public static final String SEARCH_CASE_BY_PK = "Case.selectByPk";
	/** 检索我的学习报告完成情况 */
	public static final String SELECT_CASE_STUDY_REPORT_INFO = "Case.selectCaseStudyReportInfo";
	/** 检索我的学习报告七日学习情况 */
	public static final String SELECT_CASE_SEVEN_STUDY_INFO = "Case.selectCaseSevenStudyInfo";
	/** 更新案例的浏览数 */
	public static final String UPDATE_CASE_VIEW_NUM = "Case.updateCaseViewNum";
	/** 案例列表 */
	public static final String SEARCH_CASE_LIST = "Case.searchCaseList";
	/** 案例列表 */
	public static final String SEARCH_CASE_TYPE = "Case.searchCaseType";
	/** 检索有效案例 */
	public static final String SEARCH_VALID_CASE_TYPE = "Case.validCaseList";
	/** 已参加的内训课程 */
	//public static final String SEARCH_CASE_LIST_TRAIN = "Case.searchCaseTypeTrian";
	/** 已参加的非内训课程 */
	//public static final String SEARCH_CASE_LIST_NOT_TRAIN = "Case.searchCaseTypeNotTrian";
	/** 查询案例列表 */
	public static final String SEARCH_CASE_AND_CASENODENUM = "Case.selectCaseAndCaseNodeNum";
	/** 删除案例 */
	public static final String DELETE_CASE = "Case.delete";
	/** 删除案例步骤 */
	public static final String DELETE_CASE_NODE_ALL = "CaseNode.deleteCaseNodeAll";
	/** 删除案例课程 */
	public static final String DELETE_CASE_COURSE_ALL = "CaseCourse.deleteCourseAll";
	/** 根据planNodeId检索课程 */
	public static final String SEARCH_CASECOURSE_BY_CASENODEID = "Course.selectCourseByCaseNodeId";
	
	public static final String SEARCH_COURSE = "Course.select";
	/** 新增案例 */
	public static final String INSERT_CASE = "Case.insertCase";
	/** 修改案例 */
	public static final String UPDATE_CASE = "Case.update";
	/**查询计划更具id*/
	public static final String SELECT_CASE = "Case.select";
	/** 修改计划 */
	public static final String UPDATE_CASE_NUMSTUDY = "Case.updateNumStudy";
	/** 新增案例步骤 */
	public static final String INSERT_CASENODE = "CaseNode.insert";
	/** 新增案例课程 */
	public static final String INSERT_CASECOURSE = "CaseCourse.insert";
	/** 检索案例 */
	public static final String SQL_SEARCH_CASE_BY_ID = "Case.selectCase";
	/** 查询一条plannode */
	public static final String SQL_SEARCH_CASENODE_BY_CASEID = "CaseNode.selectByCaseId";
	/** 查询是否学习过案例 */
	public static final String SQL_SEARCH_CASE_STUDY_HISTORY_DATA = "CaseStudyHistory.selectStudyHistoryByCaseId";
	/** 检索最后一次学习课程ID */
	public static final String SQL_SEARCH_CASE_LAST_STUDY_COURSE_ID = "CaseStudyHistory.selectLastStudyCourseId";
	/** 检索全部课程数量 */
	public static final String SEARCH_CASE_COURSE_COUNT = "CaseCourse.selectCaseCourseCount";
	// /////////////用户案例///////////////////
	/** 节点下的课程列表 */
	public static final String SQL_SELECT_CASE_NODE_COURSE = "CaseCourse.selectNodeCourse";
	/**用户课程*/
	public static final String SQL_SEARCH_CASE_USER_COURSE_BYPK="UserCourse.selectByPk";
	/** 是否参加案例 */
	public static final String SEARCH_CASE_JOIN = "CaseUser.searchCaseJoin";
	/** 根据计划id和用户id检索用户计划表 */
	public static final String SEARCH_USER_CASE = "CaseUser.selectByCaseIdAndUserId";
	/** 根据planId查询用户信息 */
	public static final String SEARCH_CASEUSERID_BY_CASEID = "CaseUser.selectUserByCaseId";
	/** 根据planId userId信息 */
	public static final String SQL_SEARCH_CASEUSERID_BY_CASEID_USERID = "CaseUser.select";
	/** 新增案例人员全部 */
	public static final String INSERT_CASEUSER_ALL="CaseUser.insertCaseUser";
	/** 新增案例人员 */
	public static final String INSERT_CASEUSER = "CaseUser.insert";
	/** 删除案例下所有用户 */
	public static final String DELETE_CASE_USER_ALL = "CaseUser.deleteUserAll";
	/** 删除计划下所有用户 */
	public static final String DELETE_CASE_USER = "CaseUser.delete";
	/** 查询案例用户 */
	public static final String SEARCH_CASE_USER_IDS = "CaseUser.selectCaseUserIds";
	/** 查询所有用户数量 */
	public static final String SEARCH_CASE_USER_COUNT = "CaseUser.selectCaseUserCount";	
	/** 接口-我的案例（1已参与）*/
	public static final String SELECT_USER_CASE_JOIN_LIST = "Case.selectUserCaseJoinList";	
	/** 接口-我的案例（1已参与）：标签=全部 */
	public static final String SELECT_USER_CASE_JOIN_ALL_LIST = "Case.selectUserCaseJoinAllList";	
	/** 接口-我的案例（1已完成）*/
	public static final String SELECT_USER_CASE_FINISH_LIST = "Case.selectUserCaseFinishList";	
	/** 接口-我的案例（1已完成）：标签=全部 */
	public static final String SELECT_USER_CASE_FINISH_ALL_LIST = "Case.selectUserCaseFinishAllList";
	/**根据课程检索当前用户所加入的包含当前课程的计划*/
	public static final String SQL_SELECT_USER_PLAN_BY_COURSE = "PlanUser.selectUserPlanByCourse";
	/**根据关键字检索课程列表*/
	public static final String SQL_SEARCH_LIST_BY_KEY = "Course.selectListByKey";
	
	/**显微镜课程插入数据*/
	public static final String INSERT_ER = "CourseExperimentalResult.insert";
	/**显微镜课程-根据人员ID查询人员姓名*/
	public static final String ER_SELECT_GETUSERNAME = "CourseExperimentalResult.selectUserNameByUserId";
	/**显微镜课程-人员步骤得分情况*/
	public static final String ER_SELECT_USERSTEPSCORE = "CourseExperimentalResult.selectUserStepScore";
	public static final String ER_SELECT_USERTENTIMESCORE = "CourseExperimentalResult.selectUserTenTimeScore";
	public static final String ER_SELECT_USEROPERATIONDETAIL = "CourseExperimentalResult.seachUserOperationDetail";
	public static final String ER_SELECT_USERALLSTEPSCORE = "CourseExperimentalResult.selectUserAllStepScore";
	/**全景课程-人员得分情况*/
	public static final String VR_SELECT_USERSTEPSCORE = "HomeworkAnswer.selectUserScore";
	
	public static final String INSERT_SCENE = "SceneInfo.insertSceneInfo";
	public static final String DELETE_ALL_SCENE = "SceneInfo.deleteSceneInfo";
	public static final String SELECT_SCENE_BY_USER = "SceneInfo.selectSceneInfo";
	public static final String SELECT_SCENE_BY_ID = "SceneInfo.selectSceneInfoById";
	
	public static final String MODEL_INSERT = "ModelInfo.insertModelInfo";
	public static final String MODEL_UPDATE_SCENE = "ModelInfo.updateSecenInfo";
	public static final String MODEL_UPDATE = "ModelInfo.updateModelInfo";
	public static final String MODEL_UPDATE_PC = "ModelInfo.updateModelInfoPC";
	public static final String MODEL_DELETE_ALL = "ModelInfo.deleteModelInfo";
	public static final String MODEL_SELECT_ALL = "ModelInfo.selectModelInfo";
	public static final String MODEL_SELECT_BY_MODELID = "ModelInfo.selectModelInfoBymodelId";
	public static final String MODEL_SELECT_ONE = "ModelInfo.selectModelInfoOne";
	public static final String MODEL_SELECT_TWO = "ModelInfo.selectModelInfoTwo";
	public static final String MODEL_SELECT_THREE = "ModelInfo.selectModelInfoThree";
	
	public static final String MODEL_SELECT_BY_USER = "ModelInfo.selectModelInfoByUserId";
	public static final String MODEL_SELECT_BY_NAME = "ModelInfo.selectModelInfoByName";
	public static final String MODEL_DELETE_BY_ID = "ModelInfo.delete";
	public static final String MODEL_SELECT_BY_USER_CHAPTER = "ModelInfo.selectByUserIdAndChapter";
	
	public static final String MODEL_SELECT_PRACTICE_BY_USER_CHAPTER = "ModelInfo.selectPracticeByUserIdAndChapter";
	
	public static final String FORCE_INSERT = "Force.insert";
	public static final String FORCE_SELECT_LASTTEN = "Force.selectLastTen";
	
	public static final String AC_SELECT_COURSE_TEACHER = "CourseExperimentalResult.selectACCourseTeacherId";
	public static final String AC_SELECT_USERALLSTEPSCORE = "CourseExperimentalResult.selectACUserAllStepScore";
	public static final String AC_SELECT_USERSTEPSCORE = "CourseExperimentalResult.selectACUserStepScore";
	public static final String INSERT_AC = "CourseExperimentalResult.insertAC";
	
	public static final String SELECT_SYS_USER = "SysUser.select";
	public static final String SELECT_SYS_USER_BY_LOGIN = "SysUser.selectByLogin";
	
	public static final String SELECT_USER_LOG = "UserLog.searchAllUser";
	public static final String SELECT_USER_BY_LOGIN = "UserLog.searchUser";
	
	public static final String COURSE_MESSAGE_INSERT = "CourseMessage.insert";
	public static final String COURSE_MESSAGE_SELECT_BY_COURSE_ID = "CourseMessage.selectByCourseId";
	public static final String COURSE_MESSAGE_SELECT_BY_STUDENT_ID = "CourseMessage.selectByStuId";
	public static final String COURSE_MESSAGE_SELECT_BY_PK = "CourseMessage.selectByPk";
	public static final String COURSE_MESSAGE_UPDATE = "CourseMessage.update";
	public static final String COURSE_MESSAGE_UPDATE1 = "CourseMessage.update1";
	public static final String COURSE_MESSAGE_SELECT_STUDENT_STATUS = "CourseMessage.selectStuStaus";
	public static final String COURSE_SELECT_BY_ID = "Course.selectByPk";
	public static final String COURSE_SELECT_BY_ID_SCHOOL ="Course.selectByPkCourseId";
	
	public static final String HOME_WORK_ANSWER_QUESTION_INSERT = "HomeworkAnswerQuestion.insert";
	
	public static final String HOME_WORK_ANSWER_QUESTION_SEARCH = "HomeworkAnswerQuestion.searchQuestionList";
	
	public static final String UPDATE_COURSE_SHARE = "Course.updateCourseShare";
	
	public static final String UPDATE_COURSE_FACTORY = "Course.updateCourseFactory";
	
	public static final String UPDATE_COURSE_PUBLIC = "Course.updateCoursePublic";
	
	public static final String SELECT_TEACHER_COURSE_LISTS = "Course.selectTeacherCourses";
	public static final String SELECT_USER_COURSE_LISTS = "UserCourse.selectUserCourse";
	public static final String SELECT_USER_ROLE = "UserRole.select";
	
	public static final String USER_SEARCH_NAME_BY_USERID = "UserManagement.searchUserNameByUserId";
	
	public static final String USER_INSERT_OUT = "UserManagement.insertUserByOut";
	
	public static final String USER_ROLE_INSERT_OUT = "UserManagement.insertRoleDataUserById";
	
	public static final String USER_ROLE_SELECT_BY_USERID = "UserRole.select";
	
	public static final String ARCHIVES_INSERT = "Archives.insert"; 
	
	public static final String ARCHIVES_SELECT = "Archives.select";
	public static final String ARCHIVES_DISCUSS_SELECT = "ArchivesDiscuss.select";
	public static final String ARCHIVES_DISCUSS_INSERT = "ArchivesDiscuss.insert";
	
	public static final String SQL_COURSE_TEAM_SELECT = "CourseTeam.select"; 
	public static final String SQL_COURSE_TEAM_INSERT= "CourseTeam.insert";
	public static final String SQL_COURSE_TEAM_UPDATE= "CourseTeam.update";
	public static final String SQL_COURSE_TEAM_SELECT_BY_ID = "CourseTeam.selectTeamById"; 
	public static final String SQL_COURSE_TEAM_DELETE_BY_ID = "CourseTeam.deleteTeamById";
	public static final String SQL_COURSE_TEAM_SELECT_BY_TEAMLEADER = "CourseTeam.selectTeamByTeamLeader"; 
	public static final String SQL_COURSE_TEAM_SELECT_BY_TEAM_USER = "CourseTeam.selectTeamByTeamUser"; 
	public static final String SQL_COURSE_TEAM_SELECT_BY_COURSEID = "CourseTeam.selectByCourseId"; 
	
	public static final String SQL_COURSE_TEAM_USER_SELECT = "CourseTeamUser.searchUser"; 
	public static final String SQL_COURSE_TEAM_USER_SELECT_BY_USERID = "CourseTeamUser.searchUserByUserId"; 
	public static final String SQL_COURSE_TEAM_USER_SELECT_UNIQUE = "CourseTeamUser.selectCount";
	public static final String SQL_COURSE_TEAM_USER_SELECT_MAX_NO = "CourseTeamUser.searchMaxNo";
	public static final String SQL_COURSE_TEAM_USER_INSERT = "CourseTeamUser.insert";
	public static final String SQL_COURSE_TEAM_USER_DELETE = "CourseTeamUser.delete";
	
	public static final String SEARCH_TEAM_HOME="HomeworkAnswer.selectTeamHomework";
	public static final String SEARCH_USER_HOME_BY_USER ="HomeworkAnswer.selectUserHomeworkByUsers";
	
	public static final String SQL_SEACH_MEASURE_BY_EVALUTION_ID="MeasurementInstitution.seachMeasureByCourseId";
	/** 检索问题选项 */
	public static final String MEASUERMENT_SEARCH_QUESTION_LIST_BY_MEASUREMENT_INSTITUTION = "MeasurementInstitution.selectQuestionResult";
	/** 检索问题 */
	public static final String MEASUERMENT_SEARCH_QUESTION_EVALUTION_OPTIONS_LIST = "QuestionOptionInstitution.select";
	/** 评价问卷 */
	public static final String MEASUREMENT_INSTITUTION_INSERT = "MeasurementInstitution.insert";
	/**开启评价问卷标识*/
    public static final String SQL_UPDATE_FLAG_QUEATION_EVALUTION="TbInstitutionEvalution.updateFlagQuestion";
    /**问卷的最大数*/
    public static final String QUESTION_EVALUTION_MAX_ORDER = "QuestionInstitution.searchMaxOrder";
    /**更新问题信息删除问题选项*/
 	public static final String QUESTION_DELET_OPTION_BY_QUESTION_INSTITUTION = "QuestionInstitution.deletOptionByQuestion";
 	/**评价发布/取消发布*/
    public static final String UPDATE_EVALUTION_STATUS = "TbInstitutionEvalution.updateStatus";
    /** 问卷 */
	public static final String MEASUREMENT_EVALUTION_SELECT = "MeasurementInstitution.select";
	/** 问卷题目 */
	public static final String QUESTION_EVALUTION_SELECT_VIEW = "QuestionInstitution.selectView";
	/** 问卷题目加选项 */
	public static final String QUESTION_EVALUTION_OPTION_SELECT = "QuestionOptionInstitution.select";
	/**更新评论选项*/
	public static final String QUESTION_EVALUTION_OPTION_UPDATE_NUM = "QuestionOptionInstitution.updateNum";
	/** 提交问卷问题参与人数加1 */
	public static final String QUESTION_EVALUTION_UPDATE_NUM = "QuestionInstitution.updateNumUser";
	public static final String SQL_EVALUTION_MEASUERMENT_SEARCH_QUESTION_LIST_BY_MEASUREMENT = "MeasurementInstitution.selectQuestionResult";
	/** 检索问题 */
	public static final String MEASUERMENT_EVALUTION_SEARCH_QUESTION_OPTIONS_LIST = "QuestionOptionInstitution.select";
	/** 根据ID检索机构 */
	public static final String SEARCH_INSTITUTION_EVALUTION_BY_PK = "TbInstitutionEvalution.selectByPk";
	/** 检索评价用户答题情况 */
	public static final String SQL_INSTITUTION_MEASUERMENT_USER_SEARCH = "UserMeasurementResult.selectInstitutionMeasurementUser";
	/** 删除评价下的问卷 */
    public static final String SQL_DELETE_EVALUTION_MEASURE_BY_COURSER="MeasurementInstitution.deleteMeasureByLinkId";
	/** 检索评价用户答题情况 */
	public static final String SQL_TEACHER_MEASUERMENT_USER_SEARCH = "UserMeasurementResult.selectTeacherMeasurementUser";
	/**分享课程*/
	public static final String SHARE_SELECT_USERSTEPSCORE = "HomeworkAnswer.update";
	/**查询最大置顶排序数*/
	public static final String SHARE_MAX_SELECT_USERSTEPSCORE = "HomeworkAnswer.selectMaxTopNum";
	/**删除作业*/
	public static final String DELETE_SELECT_USERSTEPSCORE = "HomeworkAnswer.delete";
	/**删除作业*/
	public static final String COURSE_SELECT_USERSTEPSCORE = "HomeworkAnswer.selectCourseId";
	/**分享课程*/
	public static final String SHARE_COURSE_SELECT_USERSTEPSCORE = "HomeworkAnswer.updateShare";
	/**取消分享课程*/
	public static final String SHARE_CANCLE_SELECT_USERSTEPSCORE = "HomeworkAnswer.updateCancleShare";
	/**置顶课程*/
	public static final String TOP_SELECT_USERSTEPSCORE = "HomeworkAnswer.updateTop";
	/**取消置顶课程*/
	public static final String TOP_CANCLE_SELECT_USERSTEPSCORE = "HomeworkAnswer.updateCancleTop";
	/**查询学校*/
	public static final String SELECT_SCHOOL = "Institution.selectInstitution";
	/**查询老师学校*/
	public static final String SELECT_TEACHER_SCHOOL = "Institution.selectTeacherInstitution";
	/**查询学生学校*/
	public static final String SELECT_STUDENT_SCHOOL = "Institution.selectStudentInstitution";
	/** 标签类列表 */
	public static final String SCHOOL_TAG_TYPE_LIST = "TagTypeInstitution.searchTagTypeList";
	/** 标签列表 */
	public static final String SCHOOL_TAG_LIST = "TagInstitutionMap.searchTagList";
	/** pbl标签列表 */
	public static final String SCHOOL_PBL_TAG_LIST = "TagInstitutionMap.searchTagList3";
	/** pbl标签列表 */
	public static final String SCHOOL_FIRST_PBL_TAG_LIST = "TagInstitutionMap.searchTagList4";
	/** 标签列表 */
	public static final String SCHOOL_ALL_TAG_LIST = "TagInstitutionMap.searchTagList";
	/** 标签一级列表 */
	public static final String SCHOOL_TAG_ALL_LIST = "TagInstitutionMap.selectTagAlllist";
	//站内信
	public static final String MESSAGE_SELECT_BY_STUDENT_ID = "CourseMessage.selectByFrontStuId";
	//全部站内信
	public static final String MESSAGE_ALL_SELECT_BY_STUDENT_ID = "CourseMessage.selectByFrontAllStuId";
	 /** 我的课程检索（全部）  */
    public static final String SELECT_USER_COURSE_ALL_UNFINISH_LIST = "Course.selectUserCourseAllUnfinishList";
    /**作业列表*/
	public static final String ZUOYE_COURSE_SELECT_USERSTEPSCORE = "HomeworkAnswer.selectZuoye";
	/** 检索章节考试 */
	public static final String EXAM_SEARCH_CHAPTER_BY_PK = "Chapter.selectExamList";
	/** 检索全部考试 */
	public static final String EXAM_ALL_SEARCH_CHAPTER_BY_PK = "Chapter.selectAllExamList";
	/** 检索全部考试 */
	public static final String EXAM_ALL_SEARCH_BY_PK = "Chapter.selectOneExamData";
	//插入活动记录
	public static final String CHAPTER_INSERT = "ChapterActive.insert";
	//更新活动记录
	public static final String CHAPTER_UPDATE = "ChapterActive.update";
	//查询活动记录
	public static final String CHAPTER_SELECT = "ChapterActive.select";
	//删除活动记录
	public static final String CHAPTER_DELETE = "ChapterActive.delete";
	//查询课程章节
	public static final String CHAPTER_ALL_SELECT = "Chapter.selectByCourseId";
	//查询课程所有活动记录
	public static final String CHAPTER_ALL_CHAPTER_SELECT = "ChapterActive.selectAllCourseChapter";
	
	public static final String MSG_USER_COURSE_BY_ID = "CourseMessage.selectByUserCourse";
	/** 搜索全部管理员表 */
	public static final String MANAGER_USER_LIST = "ManagerUser.searchAllUser";
	/** 根据用户id搜索用户 */
	public static final String MANAGER_USER_SEARCH = "ManagerUser.searchByUserId";
	/** 插入管理员表 */
	public static final String MANAGER_USER_INSERT = "ManagerUser.insert";
	/** 更新管理员表 */
	public static final String MANAGER_USER_UPDATE = "ManagerUser.update";
	/** 删除管理员表 */
	public static final String MANAGER_USER_DELETE = "ManagerUser.delete";
	/**保存 */
	public static final String COURSE_APPROVE_INSERT = "CourseApprove.insert";
	/**查询审核信息*/
	public static final String COURSE_APPROVE_SELECT = "CourseApprove.select";
	/**根据id查询*/
	public static final String COURSE_APPROVE_SELECT_BY_PK = "CourseApprove.selectByPk";
	/**更新*/
	public static final String COURSE_APPROVE_UPDATE = "CourseApprove.update";
	//查询作业
	public static final String COURSE_MESSAGE_SELECT = "Homework.selectByPk";
	//删除站内信
	public static final String COURSE__MESSAGE_DELETE = "CourseMessage.delete";
	
	public static final String COURSE_ASSIST_INSERT = "CourseAssist.insert";
	public static final String COURSE_ASSIST_DELETE_BY_COURSE_ID = "CourseAssist.deleteByCourseId";
	public static final String COURSE_ASSIST_SELECT_BY_COURSE_ID = "CourseAssist.selectByCourseId";
	/**加入课程*/
	public static final String SQL_USER_COURSE_COUNT= "UserCourse.selectUserCount";
	public static final String SQL_SCHOOL_COURSE_COUNT= "Course.selectSchoolCount";
	public static final String SQL_SCHOOL_STUDIED_COURSE_COUNT= "Course.selectStudiedCourseCount";
	public static final String SQL_SCHOOL_STUDYING_COURSE_COUNT= "Course.selectStudyingCourseCount";
	/**完成课程*/
	public static final String SQL_COURSE_END_COUNT= "Course.endCourse";
	/**创建课程*/
	public static final String COURSE_SELECT_BY_ID_TEACHER= "Course.selectCourseByTeacherId";
	/**课程作业*/
	public static final String COURSE_SELECT_BY_ID_TEACHER_END= "Course.selectCourseByTeacherIdEnd";
	/**完成作业*/
	public static final String SQL_FINISHED_HOME_WORK_COUNT= "HomeworkAnswer.finishedHomeWork";
	/***/
	public static final String SQL_TEACHER_FINISHED_HOME_WORK_COUNT= "HomeworkAnswer.teacherFinishedHomeWork";
	/**为批改作业*/
	public static final String TEACHER_HOME_WORK = "Homework.selectTeacherHomeWorkUserPage";
	//查询评价
	public static final String SEARCH_COURSE_USER_EVALUTION_LIST = "CourseUserEvalution.searchEvalution";
	//保存评价
	public static final String SEARCH_COURSE_USER_EVALUTION_INSERT = "CourseUserEvalution.insert";
	//更新评价
	public static final String SEARCH_COURSE_USER_EVALUTION_UPDATE = "CourseUserEvalution.update";
	//根据主键删除
	public static final String DELETE_COURSE_USER_EVALUTION_ID = "CourseUserEvalution.deleteById";
	//根据课程id删除
	public static final String DELETE_COURSE_USER_EVALUTION_COURSEID = "CourseUserEvalution.deleteByCourseId";
	//根据课程id和用户id查询
	public static final String DELETE_COURSE_USER_EVALUTION_COURSEID_USERID = "CourseUserEvalution.searchEvalutionGroup";
	/** 检索用户课程 */
	public static final String SEARCH_USER_COURSE_NAME = "UserCourse.selectName";
	/**根据课程、用户、以及维度删除*/
	public static final String DELETE_EVALUTION = "CourseUserEvalution.deleteByEntity";
	/**根据课程数量*/
	public static final String SELECT_EVALUTION_COUNT = "CourseUserEvalution.searchCourseCount";
	/**根据课程数量*/
	public static final String SELECT_EVALUTION_WEIDU = "CourseUserEvalution.searchCourseWeidu";
	/**获取学生的平均分数*/
	public static final String SELECT_EVALUTION_SCORE = "CourseUserEvalution.searchPjScore";
	public static final String SEARCH_NAMES_BY_ANSWER_ID = "HomeworkAnswer.selectNameById";
	
	public static final String USER_INFO_INSERT = "UserInfo.insert";
	public static final String USER_INFO_SELECT = "UserInfo.searchAll";
	public static final String USER_INFO_SELECT_BY_ID = "UserInfo.searchByPk";
	public static final String USER_INFO_UPDATE = "UserInfo.update";
	public static final String USER_INFO_DELETE = "UserInfo.delete";
	
	
	public static final String BBS_INSERT = "Bbs.insert";
	public static final String BBS_SELECT = "Bbs.select";
	public static final String BBS_SELECT_BY_PK = "Bbs.selectByPk";
	public static final String BBS_DELETE_BY_PK = "Bbs.delete";
	public static final String BBS_UPDATE = "Bbs.update";
	
	public static final String BBS_REPLY_SELECT = "BbsReply.select";
	public static final String BBS_REPLY_INSERT = "BbsReply.insert";
	public static final String BBS_REPLY_DELETE_BY_PK = "BbsReply.delete";
	/**查询纬度平均分*/
	public static final String SELECT_EVALUTION_PJ_WEIDU = "CourseUserEvalution.searchCourseByWeiduAndUserId";
	/**获取课程最短时间*/
	public static final String SELECT_EVALUTION_PJ_TIME = "CourseUserEvalution.searchCourseByUserIdAndTime";
	//查询测评数据前两条数据
	public static final String SEARCH_EVALUTION_TIME_LIST = "EvalutionTime.searchEvalutionTime";
	//查询测评一条数据数据
	public static final String SEARCH_EVALUTION_TIME_ONE_LIST = "EvalutionTime.searchEvalutionOneTime";
	//插入测评数据
	public static final String SEARCH_EVALUTION_TIME_INSERT = "EvalutionTime.insert";
	//更新测评数据
	public static final String SEARCH_EVALUTION_TIME_UPDATE = "EvalutionTime.update";
	//删除测评数据
    public static final String SEARCH_EVALUTION_TIME_DELETE = "EvalutionTime.deleteById";
    /**查询学校*/
    public static final String USER_SEARCH_BY_USERID_SCHOOL = "UserManagement.searchUser";
	/** 标签列表 */
	public static final String SEARCH_TAG_FIRST_LIST = "TagMap.searchTagList1";
	/** 标签列表 */
	public static final String SEARCH_TAG_FLAG_LIST = "TagMap.searchTagList2";
	/**我的测评*/
	public static final String SELECT_EVALUTION_MY= "CourseUserEvalution.searchEvalutionMyInfo";
}
