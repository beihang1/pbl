package com.cloudsoaring.web.course.constant;
/**
 * 
 * @author Admin
 *
 */
public interface CourseDefaultValues {
	/**积分类型-问题悬赏*/
	public static final String DEF_POINTS_TYPE_D0005="D0005";
	/**加入计划*/
	public static final String DEF_POINTS_TYPE_D0004="D0004";
	/**加入课程*/
	public static final String DEF_POINTS_TYPE_D0003="D0003";
	/**老师角色*/
	public static final String ROLE_TEACHER_ROLE = "04";
	
}
