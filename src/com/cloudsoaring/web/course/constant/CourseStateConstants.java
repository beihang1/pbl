package com.cloudsoaring.web.course.constant;
/**
 * 
 */
import com.cloudsoaring.web.common.dao.ibatis.plugin.MybatisConstant;
/**
 * 课程状态接口
 * @author Admin
 *
 */
@MybatisConstant
public interface CourseStateConstants {

    /** 计划：内训  */
    public static final String PLAN_TYPE_TRAIN="0";
    /** 计划：创业  */
    public static final String PLAN_TYPE_BUSINESS="1";
    /** 是否收藏课程-是 */
    public static final String ALREADY_SAVE = "1";
	/** 是否收藏课程-否 */
	public static final String NOT_SAVE = "2";
	/**是否加入-是*/
	public static final String ALREADY_JOIN_COURSE = "1";
	/**是否加入-否*/
	public static final String NOT_JOIN_COURSE = "2";
	/**是否评价-否*/
	public static final String NOT_SCORE_COURSE = "2";
	/**是否评价-是*/
	public static final String ALREADY_SCORE_COURSE = "1";
	/** 课程状态 - 发布 */
	public static final String COURSE_STATUS_PUBLIC = "1";
	/** 课程状态-未发布 */
	public static final String COURSE_STATUS_NOT_PUBLIC = "2";
	/** 课程状态-未发布 */
    public static final String COURSE_STATUS_NOT = "0";
    /** 课程结束状态-未结束 */
    public static final String COURSE_END_STATUS_NOT = "0";
    /** 课程结束状态-已结束 */
    public static final String COURSE_END_STATUS_YES = "1";
    /** 1：精品课，2：课工场，3：公开课，10：课工场复制出来的课，11：新创建的私有课，20：图书馆，30：试听课*/
    public static final String COURSE_SHARE_FLAG_ONE = "1";
    /** 1：精品课，2：课工场，3：公开课，10：课工场复制出来的课，11：新创建的私有课，20：图书馆，30：试听课*/
    public static final String COURSE_SHARE_FLAG_TWO = "2";
    /** 1：精品课，2：课工场，3：公开课，10：课工场复制出来的课，11：新创建的私有课，20：图书馆，30：试听课*/
    public static final String COURSE_SHARE_FLAG_THREE = "3";
    /** 1：精品课，2：课工场，3：公开课，10：课工场复制出来的课，11：新创建的私有课，20：图书馆，30：试听课*/
    public static final String COURSE_SHARE_FLAG_TEN = "10";
    /** 1：精品课，2：课工场，3：公开课，10：课工场复制出来的课，11：新创建的私有课，20：图书馆，30：试听课*/
    public static final String COURSE_SHARE_FLAG_ELEVEN = "11";
    /** 1：精品课，2：课工场，3：公开课，10：课工场复制出来的课，11：新创建的私有课，20：图书馆，30：试听课*/
    public static final String COURSE_SHARE_FLAG_TWENTY = "20";
    /** 1：精品课，2：课工场，3：公开课，10：课工场复制出来的课，11：新创建的私有课，20：图书馆，30：试听课*/
    public static final String COURSE_SHARE_FLAG_THIRTY = "30";
    
    
	/** 章节状态 - 发布 */
	public static final String CHAPTER_STATUS_PUBLIC = "1";
	/** 章节状态 - 未发布 */
    public static final String CHAPTER_STATUS_NOT_PUBLIC = "0";
	/** 链接类型 - COURSE(课程) */
	public static final String LINK_TYPE_COURSE = "COURSE";
	/** 链接类型 - COURSE(课程) */
	public static final String LINK_TYPE_RESOURCE = "SHARE";
	/** 链接类型 - CHAPTER(章节) */
	public static final String LINK_TYPE_CHAPTER = "CHAPTER";
	/** 链接类型 - 教师审核 */
	public static final String LINK_TYPE_TEACHERAPPLY = "TEACHERAPPLY";
	/** 链接类型 - 教师 */
	public static final String LINK_TYPE_TEACHER = "TEACHER";
	/** 链接类型 - TUTOR(辅导) */
	public static final String LINK_TYPE_TUTOR = "TUTOR";
	/**是否免费-免费*/
	public static final String FLAG_FREE = "1";
	/**是否免费-收费*/
	public static final String FLAG_FEE = "0";
	/** 标签类-业务类型-课程 */
	public static final String TAG_BUSSINESS_TYPE_COURSE = "1";
	/** 标签类-业务类型-机构 */
	public static final String TAG_BUSSINESS_TYPE_INSTITUTION = "1";
	/** 标签类-业务类型-计划 */
	public static final String TAG_BUSSINESS_TYPE_PLAN = "2";
	/** 标签类-机构类型-计划 */
	public static final String TAG_BUSSINESS_TYPE_PLAN_INSTITUTION = "2";
	/** 标签类-业务类型-forum */
	public static final String TAG_BUSSINESS_TYPE_FORUM = "5";
	/** 标签类-业务类型-notice */
	public static final String TAG_BUSSINESS_TYPE_NOTICE = "6";
	/** 标签类-业务类型-公告 */
	//public static final String TAG_BUSSINESS_TYPE_FORUM = "6";
	/** 标签-锁定标识-已锁定 */
	public static final String FLAG_LOCK = "2";
	/** 标签-锁定标识-未锁定 */  
	public static final String FLAG_UNLOCK = "1";
	/** 标签-删除标识-已删除 */
	public static final String FLAG_DEL = "2";
	/** 标签-删除标识-未删除 */
	public static final String FLAG_UNDEL = "1";
	/** 是否检索标签-0：否 */
	public static final String MSG_E_TAG_FLAG_NO = "2";
	/** 是否检索标签-1：是 */
	public static final String MSG_E_TAG_FLAG = "1";
	/**考试状态 - 结束*/
	public static final String EXAM_STATUS_END = "2";
	/**考试状态 - 开始*/
	public static final String EXAM_STATUS_START = "1";
	
	
	
	/**章节类型(0:大纲，1:普通，2:考试，3：练习，4：视频,5:VR)-视频*/
	public static final String CHAPTER_TYPE_TITLE = "0";
	/**章节类型(0:大纲，1:普通，2:考试，3：练习，4：视频,5:VR)-视频*/
	public static final String CHAPTER_TYPE_NORMAL = "1";
	/**章节类型(0:大纲，1:普通，2:考试，3：练习，4：视频,5:VR)-视频*/
	public static final String CHAPTER_TYPE_EXAM = "2";
	/**章节类型(0:大纲，1:普通，2:考试，3：练习，4：视频,5:VR)-视频*/
	public static final String CHAPTER_TYPE_MEASUERMENT = "3";
	/**章节类型(0:大纲，1:普通，2:考试，3：练习，4：视频,5:VR)-视频*/
	public static final String CHAPTER_TYPE_VIDEO = "4";
	/**章节类型(0:大纲，1:普通，2:考试，3：练习，4：视频,5:VR)-视频*/
	public static final String CHAPTER_TYPE_VR = "5";
	/**当前用户是否登录-已登录*/
	public static final String USER_LOGING_ALREADY = "1";
	/**当前用户是否登录-未登录*/
	public static final String USER_LOGING_NOT = "2";
	/**标签类ID-方向-课程*/
	public static final String TAG_TYPE_ID_DIRECTIVE_COURSE = "1";
	/**标签类ID-分类-课程*/
	public static final String TAG_TYPE_ID_CATAGORY_COURSE = "2";
	/**投票类型-课程*/
	public static final String VOTE_LINK_TYPE_COURSE = "COURSE";
	/**投票类型-章节*/
	public static final String VOTE_LINK_TYPE_CHAPTER = "CHAPTER";
	/**投票选项 - 好评*/
	public static final String VOTE_OPTION_GOOD = "4";
	/**投票选项 - 中评*/
	public static final String VOTE_OPTION_NORMAL = "2";
	/**投票选项 - 差评*/
	public static final String VOTE_OPTION_NOT_GOOD = "1";
	
	/**是否是删除的标签*/
	public static final String TAG_DEL_ISNOT = "1";
	/**是否是删除的标签*/
	public static final String TAG_DEL_RE = "0";
	/**操作类型-采集 2*/
	public static final String OPERATE_TYPE = "2";
	/**采集的笔记*/
	public static final String USER_COLLECT_NOTE = "1";
	/**课程状态*/
    public static final String COURSE_STATUS="course_status";
    /**作业状态*/
    public static final String HOME_WORK_STATUS="home_work_status";
    /**分享状态状态*/
    public static final String COURSE_SHARE_FLAG="course_share_flag";
    /**课程是否结束*/
    public static final String COURSE_END_STATUS="course_end_status";
    /**课程类型*/
    public static final String COURSE_SELF_TYPE="course_self_type";
    /**是否考试*/
    public static final String COURSE_EXAM="course_exam";
    /**是否考试-是*/
    public static final String COURSE_EXAM_IS="1";
    /**是否考试-否*/
    public static final String COURSE_EXAM_NOT="0";
    /**是否推荐*/
    public static final String FLAG_BOUTIQUE="flag_boutique";
    /**课程状态*/
    public static final String COURSE_RECOMMEND="course_recommend";
    /**老师角色*/
    public static final String TEACHER_ROLE = "04";
    /**管理员角色*/
    public static final String ADMINISTRATOR_ROLE = "01";
    /**管理员或者教师角色的用户类型*/
    public static final String USER_KBN_A = "A";
    /**课程范围*/
    public static final String COURSE_CROWD_TYPE="course_crowd_type";
    /**course_location*/
    public static final String COURSE_LOCATION="course_location";
    /**activity_range*/
    public static final String ACTIVITY_RANGE="activity_range";
    /**review_type*/
    public static final String REVIEW_TYPE = "review_type";
    /**review_total*/
    public static final String REVIEW_TOTAL = "review_total";
    /**chapter_is_maker*/
    public static final String CHAPTER_IS_MAKER = "chapter_is_maker";
    
    /**课程：私有课*/
    public static final String COURSE_CROWD_TRAIN="0";
    /**课程：私有课*/
    public static final String COURSE_CROWD_TRAIN_CH="私有课";
    /**课程：公开课*/
    public static final String COURSE_CROWD_NOT_TRAIN="1";
    /**课程：公开课*/
    public static final String COURSE_CROWD_NOT_TRAIN_CH="公开课";
    /**课程状态*/
    public static final String COURSE_STATUS_PROGRESS = "course_status_progress";
    
    public static final String COURSE_TYPE = "course_type";
    /**课程状态-连载中*/
    public static final String COURSE_STATUS_CONTINU = "1";
    /**课程状态-连载中*/
    public static final String COURSE_STATUS_CONTINU_CH = "连载中";
    /**课程状态-已完成*/
    public static final String COURSE_STATUS_FINISH = "2";
    /**课程状态-已完成*/
    public static final String COURSE_STATUS_FINISH_CH = "已完成";
    /**课程状态-已结束*/
    public static final String COURSE_STATUS_OVER = "3";
    /**课程状态-已结束*/
    public static final String COURSE_STATUS_OVER_CH = "已结束";
    /**课程状态-已加入*/
    public static final String COURSE_STATUS_ADDED = "1";
    /**是否按步骤*/
    public static final String COURSE_IS_STEP = "course_is_step";
    /**是否按步骤-是*/
    public static final String FLAG_ORDER_STUDY_IS="1";
    /**是否按步骤-是*/
    public static final String FLAG_ORDER_STUDY_IS_CH="是";
    /**是否按步骤-否*/
    public static final String FLAG_ORDER_STUDY_NOT="0";
    /**是否按步骤-否*/
    public static final String FLAG_ORDER_STUDY_NOT_CH="否";
    /**是否必修课程-是*/
    public static final String FLAG_REQUIRED_IS="1";
    /**是否必修课程-是*/
    public static final String FLAG_REQUIRED_IS_CH="是";
    /**是否必修课程-否*/
    public static final String FLAG_REQUIRED_NOT="0";
    /**是否必修课程-否*/
    public static final String FLAG_REQUIRED_NOT_CH="否";
    /**是否可拖动进度-是*/
    public static final String FLAG_SKIP_VIDEO_IS="1";
    /**是否可拖动进度-是*/
    public static final String FLAG_SKIP_VIDEO_IS_CH="是";
    /**是否可拖动进度-否*/
    public static final String FLAG_SKIP_VIDEO_NOT="否";
    /**是否可拖动进度-否*/
    public static final String FLAG_SKIP_VIDEO_NOT_CH="0";
    /**阶段*/
    public static final String COURSE_STAGE="course_stage";
    /**学校阶段*/
    public static final String SCHOOL_STAGE="school_stage";
    /**学校课程阶段*/
    public static final String SCHOOL_COURSE_STAGE="school_course_stage";
    /**初级阶段*/
    public static final String PROMARY_STAGE="1";
    /**初级阶段*/
    public static final String PROMARY_STAGE_CH="初级阶段";
    /**中级阶段*/
    public static final String MIDDLE_STAGE="2";
    /**中级阶段*/
    public static final String MIDDLE_STAGE_CH="中级阶段";
    /**实践阶段*/
    public static final String PRACTICE_STAGE="3";
    /**实践阶段*/
    public static final String PRACTICE_STAGE_CH="实践阶段";
    /**问题类型  - 单选*/
    public static final String QUESTION_TYPE_RADIO = "1";
    /**问题类型  - 多选*/
    public static final String QUESTION_TYPE_MULT = "2";
    /**问题类型  - 判断*/
    public static final String QUESTION_TYPE_JUDGMENT = "3";
    /**问题类型  -填空*/
    public static final String QUESTION_TYPE_TK = "3";
    /**问题类型  -问答*/
    public static final String QUESTION_TYPE_WD = "4";
    
    /**我的课程-完成情况-（已收藏 1、已学 2、已学完 3）*/
    public static final String COURSE_FINISH_TYPE_1="1";
    /**我的课程-完成情况-（已收藏 1、已学 2、已学完 3）*/
    public static final String COURSE_FINISH_TYPE_2="2";
    /**我的课程-完成情况-（已收藏 1、已学 2、已学完 3）*/
    public static final String COURSE_FINISH_TYPE_3="3";
    
    /**我的课程-话题分类-（我的话题1，参与话题2）*/
    public static final String COURSE_DISCUSS_TYPE_1="1";
    /**我的课程-话题分类-（我的话题1，参与话题2）*/
    public static final String COURSE_DISCUSS_TYPE_2="2";
    
    /**我的课程-案例分类-（我的案例1，赞过的案例2）*/
    public static final String COURSE_CASE_TYPE_1="1";
    /**我的课程-案例分类-（我的案例1，赞过的案例2）*/
    public static final String COURSE_CASE_TYPE_2="2";
    /**1级大章节*/
    public static final String BIG_CHAPTER = "1";
    /**2级小章节*/
    public static final String SMALL_CHAPTER = "0";
    /**3级子章节*/
    public static final String THREE_CHAPTER = "2";

    
    /**章节类型*/
    public static final String CHAPTER_TYPE="chapter_type";
    public static final String COURSE_DEPT_STUDY ="course_dept_study";
	/**笔记点赞*/
    public static final String OPERATE_LIKE="1";
	/**笔记采集*/
    public static final String OPERATE_COLLECT="2";
    /**评论点赞*/
    public static final String OPERATE_LIKE_DISCUSS = "3";
	
    /** 辅导点赞 */
    public static final String OPERATE_LIKE_TUTOR = "4";
    
    /**笔记检索-全部*/
    public static final String NOTE_SEARCH_ALL = "0";
    /**笔记检索-精华*/
    public static final String NOTE_SEARCH_LIKE = "1";
    
    /**标签SUB*/
    public static final String TAG_TYPE_RE = "SUB";
    
    /**模块开启状态-开启*/
    public static final String FLAG_ON = "1";
    /**模块开启状态-关闭*/
    public static final String FLAG_OFF = "0";
    /**用户是否答过题-已答*/
    public static final String FLAG_USER_ANSWER_ALREADY = "1";
    /**用户是否答过题-未答*/
    public static final String FLAG_USER_ANSWER_NOT = "0";
    
	/**是否按順序学习-是*/
	public static final String STUDY_ORDER_YES = "0";
	/**是否按順序学习-否*/
	public static final String STUDY_ORDER_NO = "1";
	/**是否推荐精品课程-未推荐*/
	public static final String NOT_FLAG_BOUTIQUED="0";
	/**是否推荐精品课程-已推荐*/
    public static final String FLAG_BOUTIQUED="1";
    /**是否推荐首页轮播-未推荐*/
    public static final String NOT_HOME_CAROUSEL="0";
    /**是否推荐首页轮播-已推荐*/
    public static final String IS_HOME_CAROUSEL="1";
    /**活动标识：笔记*/
    public static final String FLAG_NOTE="1";
    /**活动标识：1开启 0关闭*/
    public static final String FLAG="1";
    public static final String FLAG_NOT="0";
    /**活动标识：投票flagVote*/
    public static final String FLAG_VOTE="4";
    /**活动标识：问答flagQa*/
    public static final String FLAG_QA="3";
    /**活动标识：评论flagDiscuss*/
    public static final String FLAG_DISCUSS="2";
	/**审批教师状态*/
    public static final String TEACHERAPPLY_STATUS="teacherApply_status";
    /**是否开始学习-0未学*/
	public static final String STATE_NOT_ALREADY_START_STUDY = "0";
	/**是否开始学习-1已学*/
	public static final String STATE_ALREADY_START_STUDY = "1";
	/**已参与计划*/
	public static final String STATE_PLAN_TYPE_JOIN = "1";
	/**已完成计划*/
	public static final String STATE_PLAN_TYPE_FINISH = "2";
	/**已参与计划app端*/
	public static final String STATE_PLAN_TYPE_JOIN_APP = "3";
	/**查看全部权限*/
	public static final String ALL_LIST="02-alllist";
	/**课程新增权限*/
    public static final String COURSE_ADD="02-ADD";
	/**页面类型：编辑*/
    public static final String PAGE_TYPE_EDIT="edit";
    /**页面类型：查看*/
    public static final String PAGE_TYPE_VIEW="view";
	
    /**教师审批同意*/
    public static final String TEACHERAPPLY_AGREE="2";
    /**教师审批不同意*/
    public static final String TEACHERAPPLY_DISAGREE="1";
    /**教师tag在linkTypeL类型*/
    public static final String TEACHERAPPLY_LINKTYPE="TEACHERAPPLY";
    /**教师未审批*/
    public static final String TEACHERAPPLY_NOTAPPROVE="0";
    
    /**计划在linkTypeL类型*/
    public static final String PLAN_LINKTYPE="PLAN";
    /**被动加入计划*/
    public static final String PLAN_JOINPLAN_PASSIVE="0";
    /**主动加入计划*/
    public static final String PLAN_JOINPLAN_ACTIVE="1";
    /**社交老师表状态开*/
    public static final String SOCIALMASTER_STATUS_OPEN = "1";
    
    

    //非考试的测试类型
    public static final String MEASUERMENT_TYPE = "MEASUERMENT_TYPE";
    //非考试的测试类型 - 测试
    public static final String MEASUERMENT_TYPE_MEASUERMENT = "1";
    //非考试的测试类型 - 考试
    public static final String MEASUERMENT_TYPE_EXAM = "2";
    //非考试的测试类型 - 问卷
    public static final String MEASUERMENT_TYPE_QUESTIO= "3";
	
	/**考试的测试类型 - 测试提交的类型**/
	public static final String MEASUERMENT_SUBMIT_TYPE = "MEASUERMENT_SUBMIT_TYPE";
	/**考试的测试类型 - 测试提交的类型 - PC端手工提交**/
	public static final String MEASUERMENT_SUBMIT_TYPE_PC_MANUAL = "1";
	/**考试的测试类型 - 测试提交的类型 - PC端自动提交**/
	public static final String MEASUERMENT_SUBMIT_TYPE_PC_AUTO = "2";
	/**考试的测试类型 - 测试提交的类型 - APP端手工提交**/
	public static final String MEASUERMENT_SUBMIT_TYPE_APP_MANUAL = "3";
	/**考试的测试类型 - 测试提交的类型 - APP端自动提交**/
	public static final String MEASUERMENT_SUBMIT_TYPE_APP_AUTO = "4";
	/**考试的测试类型 - 测试提交的类型 - PC端考试过程提交**/
	public static final String MEASUERMENT_SUBMIT_TYPE_PC_PROCESS_SUBMIT = "5";
	/**考试的测试类型 - 测试提交的类型 - APP端考试过程提交**/
	public static final String MEASUERMENT_SUBMIT_TYPE_APP_PROCESS_SUBMIT = "6";
	
	/**计划状态*/
    public static final String PLAN_STATUS="plan_status";
    /**课程学习进度100*/
    public static final int COURSE_PROGRESS=100;
    /**课程学习进度0*/
    public static final int COURSE_ZERO=0;
    /**计划节点可学习*/
    public static final String PLAN_NODE_FLAG_YES="1";
    /**计划节点不可学习*/
    public static final String PLAN_NODE_FLAG_NO="0";
    /**搜索类型-课程*/
    public static final String SEARCH_COURSE_BY_KEY = "1";
    public static final String OPTION_ORDER= "OPTION_ORDER";
    /**活动状态*/
    public static final String ACTIVE_STATUS = "ACTIVE_STATUS";
    /**活动-是否直播*/
    public static final String ACTIVE_PLAY_LIVE = "ACTIVE_PLAY_LIVE";
    /**NETDIST*/
    public static final String FILE_TYPE_NETDISK = "NETDIST";
    /**必修课*/
    public static final String  FLAG_REQUIRED="FLAG_REQUIRED";
    /**阶段*/
    public static final String RESOURCE_STAGE="RESOURCE_STAGE";
    /**课程来源*/
    public static final String COURSE_SOURCE="COURSE_SOURCE";
    /**课程来源-前台导师*/
    public static final String COURSE_SOURCE_T="COURSE_SOURCE_T";
    /**性别*/
    public static final String PERSON_SEX="person_sex";
    /** 链接类型 -评价) */
	public static final String LINK_TYPE_EVALUTION = "EVALUTION";
	/** 标签类-业务类型-计划 */
	public static final String TAG_SCHOOL_TYPE_PLAN = "2";
	/**课程待办信息*/
	public static final String COURSE_MESSAGE_TYPE_ONE = "1";
	/**课程待办信息*/
	public static final String COURSE_MESSAGE_TYPE_TWO = "2";
	/**课程待办信息*/
	public static final String COURSE_MESSAGE_TYPE_THREE = "3";
	/**课程待办信息*/
	public static final String COURSE_MESSAGE_TYPE_FOUR = "4";
	/**作业提交状态*/
	public static final String HOMEWORK_ANSWER_STATUS_ONE = "1";
	/**作业提交状态*/
	public static final String HOMEWORK_ANSWER_STATUS_TWO = "2";
	/**作业提交状态*/
	public static final String HOMEWORK_ANSWER_STATUS_THREE = "3";

}