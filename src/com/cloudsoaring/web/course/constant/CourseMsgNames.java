package com.cloudsoaring.web.course.constant;
/**
 * 
 * @author Admin
 *
 */
public interface CourseMsgNames {
	/** ID为空 */
	public static final String MSG_E_NULL_ID = "msg.e.nullid";
	/**课程名称*/
	public static final String CENTER_COURSE_TITLE="center.coursetitle";
	/**课程列表*/
	public static final String CENTER_COURSE_MANAGEMENT="center.courseManagement";
	/**授课教师*/
	public static final String COURSE_BY_TEACHER="course.teacher";
	/**课程类别：公开课*/
    public static final String COURSE_PUBLIC="course.public";
    /**课程类别：私有课*/
    public static final String COURSE_PRIVATE="course.private";
	/**创建日期*/
    public static final String CENTER_DATE="course.createdate";
    /**推荐精品课程*/
    public static final String RECOMMEND_GOOD_COURSE="center.recommendGoodCourse";
    /**取消精品课程*/
    public static final String CANCEL_GOOD_COURSE="center.cancelGoodCourse";
    /**授课信息*/
    public static final String CENTER_COURSE_INFO="center.teachingInfo";
    /**已推荐*/
    public static final String RECOMMEND="center.recommend";
    /**未推荐*/
    public static final String NOT_RECOMMEND="center.notRecommend";
    /**确定要取消精品课程吗？*/
    public static final String CENTER_CONFIRM_CANCEL_PUSH_HOME="center.confirmCancelPushHome";
    /**确定要推荐精品课程吗？*/
    public static final String CENTER_CONFIRM_PUSH_HOME="center.confirmPushHome";
    /**确定要推荐首页轮播吗？*/
    public static final String CENTER_CONFIRM_PUSH_HOME_BANNER="center.confirmPushHomeBanner";
    /**确定要取消首页轮播吗？*/
    public static final String CENTER_CONFIRM_CANCEL_PUSH_HOME_BANNER="center.confirmCancelPushHomeBanner";
    /**推荐首页轮播*/
    public static final String RECOMMEND_HOME_BANNER="center.recommendHomeBanner";
    /**取消首页轮播*/
    public static final String CANCEL_HOME_BANNER="center.cancelHomeBanner";
    /**删除*/
    public static final String CENTER_DELETE="center.delete";
    /**删除学员*/
    public static final String CENTER_DELETE_STUDENT="center.deleteStudent";
    /**删除学生*/
    public static final String CENTER_DELETE_STUDENTS="center.deleteStudents";
    /**编辑*/
    public static final String CENTER_EDIT="center.edit";
    /**查看*/
    public static final String CENTER_VIEW="center.view";
    /**确认要复制课程吗*/
    public static final String CENTER_SURE_COPY_COURSE="center.copy";
    /**保存成功*/
    public static final String CENTER_SAVE_SUCCESS="center.save_success";
    /**是否确认删除当前课程？*/
    public static final String CENTER_SURE_DELETE_COURSE="center.sureDeleteCourse";
    /**是否确认删除当前章节？*/
    public static final String CENTER_SURE_DELETE_CHAPTER="center.sureDeleteChapter";
    /**请选择删除数据*/
    public static final String MSG_SELECT_DELETE_DATA = "select.delete.data";
    /**请选择一条数据*/
    public static final String MSG_SELECT_ONE_DATA = "select.one.data";
    
	/** 课程不存在 */
	public static final String MSG_E_COURSE_NOT_EXIST = "msg.e.coursenotexist";
	/** 用户未收藏*/
	public static final String MSG_E_USER_NOT_FAVORITE = "msg.e.usernotfavorite";
	/**测试不存在*/
	public static final String MSG_E_MEAS_NOT_EXIST = "msg.e.measnotexist";
	/**题目不存在*/
	public static final String MSG_E_QUESTION_NOT_EXIST = "msg.e.questionnotexist";
	/**选项不存在*/
	public static final String MSG_E_OPTION_NOT_EXIST = "msg.e.optionnotexist";
	/**发布*/
    public static final String CENTER_RELEASE="center.release";
    /**取消发布*/
    public static final String CENTER_NOT_RELEASE="center.notRelease";
    /**是否确认发布当前课程？*/
    public static final String CENTER_CONFIRM_PUBLIC="center.confirmPublic";
    /**是否确认取消发布当前课程？*/
    public static final String CENTER_CONFIRM_NOT_PUBLIC="center.confirmNotPublic";
    /**章节内容*/
    public static final String CENTER_CHAPTERS="center.Chapters";
    /**添加课程*/
    public static final String CENTER_ADD_COURSES="center.addCourses";
	/** 章节不存在 */
	public static final String MSG_E_CHAPTER_NOT_EXIST = "msg.e.chapternotexist";
	/**课程状态*/
    public static final String CENTER_COURSE_STATE="center.courseState";
    /**课程状态Serial:连载中*/
    public static final String CENTER_COURSE_STATUS_SERIAL="center.serial";
    /**课程状态complete:已完成*/
    public static final String CENTER_COURSE_STATUS_COMPLETE="center.complete";
    /**课程状态end:已结束*/
    public static final String CENTER_COURSE_STATUS_END="center.end";
    /**课程类别*/
    public static final String CENTER_COURSE_TYPE="center.courseType";
    /**课程中存在章节，不允许删除not allow*/
    public static final String MSG_NOT_ALLOW_DELETE="msg.notAllow";
    /**章节包含子章节，不允许删除*/
    public static final String NOT_ALLOW_DELETE_CHAPTER="msg.notAllowChapter";
    /**没有权限操作此课程*/
    public static final String NOT_ALLOW_COURSE="msg.notAllowCourse";
    /**没有权限操作此章节！*/
    public static final String NOT_ALLOW_CHAPTER="msg.notAllowOpChapter";
    /**考试结束时间已过,不可编辑!*/
    public static final String NOT_ALLOW_EDIT="msg.notAllowEdit";
    /**标签分类：方向*/
    public static final String TAG_TYPE_FIRST="msg.tagTypeFirst";
    /**标签分类：分类*/
    public static final String TAG_TYPE_SECOND="msg.tagTypeSecond";
	/** 用户尚未登录 */
	public static final String NULL_USER_ID = "msg.e.userIdNull";
	/** 用户ID为空 */
	public static final String USER_ID_NULL = "msg.e.NulluserId";
	/**删除失败!用户不可以删除自己!*/
	public static final String CAN_NOT_DELETE_YOURSELF = "msg.user.cannot.delete.yourself";
	/**有几门课程*/
	public static final String MSG_C_COURSE = "msg.c.course";
	/** 显示数量不能为空并且只能为大于零整数 */
	public static final String MSG_E_PAGE_NUM = "msg.e.pageNumType";
	/**问答类型错误*/
	public static final String MSG_E_TYPE_ERROR="msg.e.typeError";
	/** 课程ID为空 */
	public static final String MSG_E_CID_NULL = "msg.e.courseIdNull";
	/**评价链接ID为空**/
	public static final String MSG_E_DISCUSS_ID_NULL= "msg.e.discussidnull";
	/** 接口传值错误 */
	public static final String MSG_E_INTERFACE_VALUE = "msg.e.interfaceValue";
	/** 找不到该笔记！*/
	public static final String MSG_E_NO_NOTE = "msg.e.noNote";
	
	public static final String MSG_E_NO_HOME_WORK = "msg.e.noHomeWork";
	
	/** 找不到该计划*/
	public static final String MSG_E_NO_PLAN = "msg.e.noPlan";
	/** 该计划下的节点或课程为空*/
	public static final String MSG_E_NO_PLAN_NULL = "msg.e.noPlanNull";

	/** 链接类型错误 */
	public static final String MSG_E_ERROR_LINK_TYPE = "msg.e.linkTypeError";
	/**题目或选项为空*/
	public static final String MSG_E_QUESTION_OPTION_NULL="msg.e.questionoptionNull";
	/**选项为空*/
	public static final String MSG_E_OPTION_NULL="msg.e.optionNull";
	/** 课程或章节不存在 */
	public static final String MSG_E_COURSE_CHAPTER_NOT_EXIST = "msg.e.coures.chapter.not";

	/** 评论发表成功 */
	public static final String MSG_E_DISCUSS_S ="msg.e.descuss.s";

	/** 标签数据传入错误 */
	public static final String MSG_E_TAG_ERROR = "msg.e.tag.error";
	
	/** 计划不存在 */
	public static final String MSG_E_PLAN_NOT_EXIST = "msg.e.plannotexist";
	
	/** 该计划当前用户已经添加 */
	public static final String MSG_E_PLAN_JOIN = "msg.e.planJoin";
	
	/** 用户退出计划失败 */
	public static final String MSG_E_PLAN_EXIT_FAIL = "msg.e.planExitFail";
	
	/** 问答标题不能为空 */
	public static final String MSG_E_FAQ_TITLE_NULL = "msg.e.faqTitleNull";
	
	/** 验证码错误 */
	public static final String MSG_E_CODENUMBER_EXIST = "error.codeNumber.exist";
	
	/** 请输入信息 */
	public static final String MSG_E_MESSAGE_EXIST = "error.message.exist";
	/**您使用的浏览器版本较低，建议您升级到*/
	public static final String LOW_BROWSER_VERSION = "browser.lowversion";
	/**Internet Explorer 9 */
	public static final String IE_9 = "browser.ie9";
	/**及以上版本*/
	public static final String FLOOR_BROWSER_VERSION = "browser.floorbrowserversion";
	/**关闭*/
	public static final String BROWSER_TIP_CLOSE = "browser.closetip";
	/**课程期限*/
    public static final String CENTER_COURSE_PERIOD="center.coursePeriod";
    /**学习时长*/
    public static final String CENTER_LEARNING_TIME="center.learningTime";
    /**分类*/
    public static final String CENTER_CLASSIFICATION="center.classification";
    /**标题*/
    public static final String CENTER_TITLE="center.title";
    /**课程内容*/
    public static final String CENTER_COURSE_CONTENT="center.courseContent";
    /**课程详情*/
    public static final String CENTER_COURSE_DETAIL="center.courseDetail";
    /**添加题目*/
    public static final String CENT_ADD_QUESTION="center.addQuestion";
    /**添加*/
    public static final String CENTER_ADD_TO="center.addTo";
    /**题目*/
    public static final String CENTER_TOPIC="center.topic";
    /**问题*/
    public static final String CENTER_THE_PROBLEM="center.theProblem";
    /**请输入题目*/
    public static final String CENTER_PLEASE_ENTER_A_QUESTION="center.pleaseEnterAQuestion";
    /**正确选项*/
    public static final String CENTER_CORRECT_OPTION="center.correctOption";
    /**A选项*/
    public static final String CENTER_A_OPTION="center.aOption";
    /**B选项*/
    public static final String CENTER_B_OPTION="center.bOption";
    /**C选项*/
    public static final String CENTER_C_OPTION="center.cOption";
    /**D选项*/
    public static final String CENTER_D_OPTION="center.dOption";
	
    /**问卷*/
    /**全选*/
    public static final String MSG_QUESTIONNAIRE_SELECTALL = "msg.questionnaire.selectAll";
	/**编号*/
	public static final String MSG_QUESTIONNAIRE_NUM = "msg.questionnaire.num";
	/**问卷标题*/
	public static final String MSG_QUESTIONNAIRE_TITILE = "msg.questionnaire.titile";
	/**发起人*/
	public static final String MSG_QUESTIONNAIRE_CREATEUSER = "msg.questionnaire.createUser";
	/**发起日期*/
	public static final String MSG_QUESTIONNAIRE_CREATEDATE = "msg.questionnaire.createDate";
	/**发出份数*/
	public static final String MSG_QUESTIONNAIRE_RESULTCNT = "msg.questionnaire.resultCnt";
	/**收回份数*/
	public static final String MSG_QUESTIONNAIRE_ANSWERCNT = "msg.questionnaire.answerCnt";
	
	/**问题一*/
	public static final String MSG_QUESTION_ONE = "msg.question.first";
	/**问题二*/
	public static final String MSG_QUESTION_TWO = "msg.question.second";
	/**问题三*/
	public static final String MSG_QUESTION_THREE = "msg.question.three";
	/**问题四*/
	public static final String MSG_QUESTION_FOUR = "msg.question.four";
	/**问题五*/
	public static final String MSG_QUESTION_FIVE = "msg.question.five";
	/**问题六*/
	public static final String MSG_QUESTION_SIX = "msg.question.six";
	/**问题七*/
	public static final String MSG_QUESTION_SEVEN = "msg.question.seven";
	/**问题八*/
	public static final String MSG_QUESTION_EIGHT = "msg.question.eight";
	/**问题九*/
	public static final String MSG_QUESTION_NINE = "msg.question.nine";
	/**问题十*/
	public static final String MSG_QUESTION_TEN = "msg.question.ten";
	/**选项A*/
	public static final String MSG_OPTION_A = "msg.option.a";
	/**选项B*/
	public static final String MSG_OPTION_B = "msg.option.b";
	/**选项C*/
	public static final String MSG_OPTION_C = "msg.option.c";
	/**选项D*/
	public static final String MSG_OPTION_D = "msg.option.d";
	/**选项E*/
	public static final String MSG_OPTION_E = "msg.option.e";
	/**选项F*/
	public static final String MSG_OPTION_F = "msg.option.f";
	/**选项G*/
	public static final String MSG_OPTION_G = "msg.option.g";
	/**选项H*/
	public static final String MSG_OPTION_H = "msg.option.h";
	/**选项I*/
	public static final String MSG_OPTION_I = "msg.option.i";
	/**选项J*/
	public static final String MSG_OPTION_J = "msg.option.j";
	
	/** 我的课程完成类型为空 */
	public static final String MSG_E_COURSE_FINISH_TYPE_NULL = "msg.e.courseFinishTypeNull";
	
	/** 我的课程话题分类为空 */
	public static final String MSG_E_DISCUSS_TYPE_NULL = "msg.e.discussTypeNull";
	
	/** 我的课程案例分类为空 */
	public static final String MSG_E_CASE_TYPE_NULL = "msg.e.caseTypeNull";
	
	/** 我的计划分类为空 */
	public static final String MSG_E_PLAN_TYPE_NULL = "msg.e.planTypeNull";

	/** 我的笔记分类为空 */
	public static final String MSG_E_NOTE_TYPE_NULL = "msg.e.noteTypeNull";

	/** 我的问答分类为空 */
	public static final String MSG_E_FAQ_TYPE_NULL = "msg.e.faqTypeNull";
	//对
    public static final String CENT_OPTION_RIGHT = "center.option.right";
    //错
    public static final String CENT_OPTION_ERROR = "center.option.error";
    /**最后的问题不能删除*/
    public static final String CENTER_LAST_QUESTION_NOT_DELETED="center.lastQuestionNotDeleted";
    
    /**没有测试*/
	public static final String NO_MEASUREMENT = "course.nomeasurement";
	/**单选题*/
	public static final String SIGLE_SELECT = "course.singleselect";
	/**多选题*/
	public static final String MORE_SELECT = "course.moreselect";
	/**填空题*/
	public static final String TK_SELECT = "course.tianselect";
	/**问答题*/
	public static final String JD_SELECT = "course.wentselect";
	/**判断题*/
	public static final String JUDGMENT = "course.judgment";
	/**提交成功*/
	public static final String SUBMIT_OK = "course.submitok";
	/**倒计时*/
	public static final String COUNT_DOWN = "course.countdown";
	/**时*/
	public static final String HOUR = "course.hour";
	/**分*/
	public static final String MINS = "course.mins";
	/**秒*/
	public static final String SECONDS = "course.seconds";
	/**距考试结束还有10分钟*/
	public static final String LAST_TEN_MINS = "course.lasttenmins";
	/**距考试结束还有1分钟，请尽快提交*/
	public static final String LAST_ONE_MINS = "course.lastonemins";
	/**考试结束*/
	public static final String EXAM_OVER = "course.examover";
	/**没有考试*/
	public static final String NO_EXAM = "course.noexam";
	/**本次考试共*/
	public static final String TOTAL_QUESTION = "course.totalquestion";
	/**题*/
	public static final String TI = "course.ti";
	/**答对*/
	public static final String ANSWER_RIGHT = "course.answerright";
	/**考试*/
	public static final String EXAM = "course.exam";
	/**开始考试*/
	public static final String START_EXAM = "course.startexam";
	/**提交*/
	public static final String SUBMIT = "course.submit";
	/**尚未登录，请登录后开始考试*/
	public static final String NO_LOGIN_TO_LOGIN = "course.nologintologin";
	/**加入考试*/
	public static final String JOIN_EXAM = "course.joinexam";
	/**已加入考试*/
	public static final String ALREADY_JOIN_EXAM = "course.alreadyjoinexam";
	/**收藏考试*/
	public static final String SAVE_EXAM = "course.saveexam";
	/**考试描述*/
	public static final String EXAM_DESC = "course.examdesc";
	/**考试简介*/
	public static final String EXAM_CONTENT = "course.examcontent";
	/**考试章节*/
	public static final String EXAM_CHAPTER = "course.examchapter";
	/**考试时间*/
	public static final String EXAM_TIME = "exam.time";
	/**考试进度*/
	public static final String EXAM_STATUS = "exam.status";
	/**未开始*/
	public static final String NO_START = "exam.nostart";
	/**进行中*/
	public static final String EXAMING = "exam.examing";
	/**已结束*/
	public static final String EXAM_END = "exam.examend";
	/**考试总时长*/
	public static final String EXAM_TOTAL_TIME = "exam.totaltime";
	/**剩余考试时长*/
	public static final String EXAM_HAS_TIME = "exam.hastime";
	/**已答/总题数*/
	public static final String EXAM_SELECT_NUM = "exam.selectnum";
	/**考*/
	public static final String EXAM_KAO = "exam.kao";
	
	///////////////////////前端 页面NAV区域模块名///////////////////////
	/**课程*/
	public static final String TXT_NAV_COURSE = "txt.nav.course";
	/**计划*/
	public static final String TXT_NAV_PLAN = "txt.nav.plan";
	/**圈子*/
	public static final String TXT_NAV_CIRCLE = "txt.nav.circle";
	/**论坛*/
	public static final String TXT_NAV_DISCUSS = "txt.nav.discuss";
	/**社交*/
	public static final String TXT_NAV_FRIEND = "txt.nav.friend";
	/**案例分享*/
	public static final String TXT_NAV_CASE_SHARE = "txt.nav.caseshare";
	/**辅导*/
	public static final String TXT_NAV_COACH = "txt.nav.coach";
	/**个人中心*/
	public static final String TXT_NAV_PERSONPAGE = "txt.nav.personpage";
	/**老师首页*/
	public static final String TXT_NAV_TEACHER = "老师首页";
	
	///////////////////////前端 页面NAV区域模块是否显示///////////////////////	
	/**课程-显示开关*/
	public static final String TXT_NAV_COURSE_ON_OFF = "txt.nav.course.onoff";
	/**计划-显示开关*/
	public static final String TXT_NAV_PLAN_ON_OFF = "txt.nav.plan.onoff";
	/**圈子-显示开关*/
	public static final String TXT_NAV_CIRCLE_ON_OFF = "txt.nav.circle.onoff";
	/**论坛-显示开关*/
	public static final String TXT_NAV_DISCUSS_ON_OFF = "txt.nav.discuss.onoff";
	/**社交-显示开关*/
	public static final String TXT_NAV_FRIEND_ON_OFF = "txt.nav.friend.onoff";
	/**案例分享-显示开关*/
	public static final String TXT_NAV_CASE_SHARE_ON_OFF = "txt.nav.caseshare.onoff";
	/**辅导-显示开关*/
	public static final String TXT_NAV_COACH_ON_OFF = "txt.nav.coach.onoff";
	/**个人中心-显示开关*/
	public static final String TXT_NAV_PERSONPAGE_ON_OFF = "txt.nav.personpage.onoff";
	/**老师首页 -显示开关*/
	public static final String TXT_NAV_TEACHER_ON_OFF = "true";
	
	//////////////////////前端章节详情/////////////////////////////////////
	/**章节详情-章节*/
	public static final String TXT_CHAPTER = "txt.chapter";
	/**章节详情-笔记*/
	public static final String TXT_NOTE = "txt.note";
	/**章节详情-提问*/
	public static final String TXT_QUESTION = "txt.question";
	/**章节详情-正在学*/
	public static final String TXT_STUDY_NOW = "txt.studynow";
	/**章节列表-最近学习*/
	public static final String TXT_LAST_STUDY = "txt.laststudy";
	/**章节详情-源自：*/
	public static final String TXT_SRC_FROM = "txt.srcfrom";
	/**章节详情-关注*/
	public static final String TXT_ATTENTION = "txt.attention";
	/**章节详情-已关注*/
	public static final String TXT_ATTENTION_ALREADY = "txt.attentionalready";
	/**章节详情-分享*/
	public static final String TXT_SHARE = "txt.share";
	/**模块-课程章节*/
	public static final String TXT_MODE_CHAPTER = "txt.modechapter";
	/**模块-课程介绍*/
	public static final String TXT_MODE_COURSE = "txt.modecourse";
	/**模块-章节介绍*/
	public static final String TXT_MODE_CHAPTER_SUMMARY = "txt.modechaptersummary";
	/**模块-笔记*/
	public static final String TXT_MODE_NOTE = "txt.modenote";
	/**模块-评论*/
	public static final String TXT_MODE_DISCUSS = "txt.modediscuss";
	/**模块-测试*/
	public static final String TXT_MODE_MEASUREMENT = "txt.modemeasurement";
	/**模块-问答*/
	public static final String TXT_MODE_QUESTION = "txt.modequestion";
	/**模块-投票*/
	public static final String TXT_MODE_VOTE = "txt.modevote";
	/**模块-作业*/
	public static final String TXT_MODE_WORK = "txt.modework";

	public static final String TXT_MODE_WORK_TITLE = "txt.modeworktitle";
	
	/**章节介绍*/
	public static final String TXT_CHAPTER_SUMMARY = "txt.chaptersummary";
	/**资料下载*/
	public static final String TXT_DOWNLOAD_SRC = "txt.downloadsrc";
	/**下载*/
	public static final String TXT_DOWNLOAD = "txt.download";
	/**该课的同学还学过*/
	public static final String TXT_OTHER_COURSE = "txt.othercourse";
	//////////////////////计划后台/////////////////////////////////////
	/**计划名称*/
	public static final String TXT_PLAN_TITLE = "txt.plan.title";
	/**计划介绍*/
	public static final String TXT_PLAN_CONTENT = "txt.plan.content";
	/**计划类别*/
	public static final String TXT_PLAN_TYPE = "txt.plan.type";
	/**计划封面*/
	public static final String TXT_PLAN_PICTURE = "txt.plan.picture";
	/**学习时间*/
	public static final String TXT_PLAN_PLANTIME = "txt.plan.plantime";
	/**是否按步骤学习*/
	public static final String TXT_PLAN_STUDYMODEL = "txt.plan.studymodel";
	/**计划状态*/
	public static final String TXT_PLAN_PLANSTATUS = "txt.plan.planstatus";
	/**是否促销*/
	public static final String TXT_PLAN_FLAGDISCOUNT = "txt.plan.flagdiscount";
	/**打折策略*/
	public static final String TXT_PLAN_NUMDISCOUNT = "txt.plan.numdiscount";
	/**指定计划管理者*/
	public static final String TXT_PLAN_MANAGEUSER = "txt.plan.manageuser";
	/**标签分类*/
	public static final String TXT_PLAN_TAG = "txt.plan.tag";
	/**关键字*/
	public static final String TXT_PLAN_TAGNAMES = "txt.plan.tagnames";
	/**步骤名称*/
	public static final String TXT_PLAN_PLANNODENAME = "txt.plan.plannodename";
	/**步骤描述*/
	public static final String TXT_PLAN_PLANNODECONTENT = "txt.plan.plannodecontent";
	/**是否必修*/
	public static final String TXT_PLAN_FLAGREQUIRED = "txt.plan.flagrequired";
	/**价格*/
	public static final String TXT_PLAN_PRICE = "txt.plan.price";
	/**积分*/
	public static final String TXT_PLAN_POINTS = "txt.plan.points";
	/**课程情况*/
	public static final String TXT_PLAN_COURSESITUATION = "txt.plan.coursesituation";
	/**昵称*/
	public static final String TXT_PLAN_PERSON = "txt.plan.person";
	/**电话*/
	public static final String TXT_PLAN_PHONE = "txt.plan.phone";
	/**邮件*/
	public static final String TXT_PLAN_EMAIL= "txt.plan.email";
	/**学习计划*/
	public static final String TXT_PLAN_STUDYPLAN= "txt.plan.studyplan";
	/**参加该计划*/
	public static final String TXT_PLAN_JOINPLAN= "txt.plan.joinplan";
	/**参与者信息*/
	public static final String TXT_PLAN_PLANUSER= "txt.plan.planuser";
	/**参与者信息*/
	public static final String TXT_PLAN_PLANSHOPPING= "txt.plan.planshopping";
	/**积分支付*/
	public static final String TXT_PLAN_PAYPLANPOINTS=  "txt.plan.payplanpoints";
	/**你的积分总额*/
	public static final String TXT_PLAN_POINTSTOTAL=  "txt.plan.pointstotal";
	/**支付后积分余额*/
	public static final String TXT_PLAN_SURPLUSPOINT=  "txt.plan.surpluspoint";
	/**立即支付*/
	public static final String TXT_PLAN_PAY=  "txt.plan.pay";
	/**关键字之间请以空格区分！*/
	public static final String TXT_PLAN_TAGNAMES_PROMPTLANGUAGE=  "txt.plan.tagnames.promptlanguage";
	/**元*/
	public static final String TXT_PLAN_ELEMENT= "txt.plan.element";
	/**折扣*/
	public static final String TXT_PLAN_DISCOUNT= "txt.plan.discount";
	/**无折扣*/
	public static final String TXT_PLAN_NOTDISCOUNT= "txt.plan.notdiscount";
	
	/**该计划为必修，不可退出*/
	public static final String MSG_E_PLAN_FLAGREQUIRED = "msg.e.plan.flagrequired";
	
	
	/**案例名称*/
	public static final String TXT_CASE_TITLE = "txt.case.title";
	/**案例介绍*/
	public static final String TXT_CASE_CONTENT = "txt.case.content";
	/**案例类别*/
	public static final String TXT_CASE_TYPE = "txt.case.type";
	/**案例封面*/
	public static final String TXT_CASE_PICTURE = "txt.case.picture";
	/**案例状态*/
	public static final String TXT_CASE_PLANSTATUS = "txt.case.planstatus";
	/**指定计划管理者*/
	public static final String TXT_CASE_MANAGEUSER = "txt.case.manageuser";
	/**学习计划*/
	public static final String TXT_CASE_STUDYPLAN= "txt.case.studycase";
	/**参加该计划*/
	public static final String TXT_CASE_JOINPLAN= "txt.case.joincase";
	/**该计划为必修，不可退出*/
	public static final String MSG_E_CASE_FLAGREQUIRED = "msg.e.case.flagrequired";
	/**确认退出案例*/
	public static final String MSG_EXIT_CASE = "msg.exit.case";
	
	/**学习时间*/
	public static final String TXT_CASE_PLANTIME = "txt.plan.plantime";
	/**是否按步骤学习*/
	public static final String TXT_CASE_STUDYMODEL = "txt.plan.studymodel";
	/**是否促销*/
	public static final String TXT_CASE_FLAGDISCOUNT = "txt.plan.flagdiscount";
	/**打折策略*/
	public static final String TXT_CASE_NUMDISCOUNT = "txt.plan.numdiscount";
	/**标签分类*/
	public static final String TXT_CASE_TAG = "txt.plan.tag";
	/**关键字*/
	public static final String TXT_CASE_TAGNAMES = "txt.plan.tagnames";
	/**步骤名称*/
	public static final String TXT_CASE_PLANNODENAME = "txt.plan.plannodename";
	/**步骤描述*/
	public static final String TXT_CASE_PLANNODECONTENT = "txt.plan.plannodecontent";
	/**是否必修*/
	public static final String TXT_CASE_FLAGREQUIRED = "txt.plan.flagrequired";
	/**价格*/
	public static final String TXT_CASE_PRICE = "txt.plan.price";
	/**积分*/
	public static final String TXT_CASE_POINTS = "txt.plan.points";
	/**课程情况*/
	public static final String TXT_CASE_COURSESITUATION = "txt.plan.coursesituation";
	/**昵称*/
	public static final String TXT_CASE_PERSON = "txt.plan.person";
	/**电话*/
	public static final String TXT_CASE_PHONE = "txt.plan.phone";
	/**邮件*/
	public static final String TXT_CASE_EMAIL= "txt.plan.email";
	/**参与者信息*/
	public static final String TXT_CASE_PLANUSER= "txt.plan.planuser";
	/**参与者信息*/
	public static final String TXT_CASE_PLANSHOPPING= "txt.plan.planshopping";
	/**积分支付*/
	public static final String TXT_CASE_PAYPLANPOINTS=  "txt.plan.payplanpoints";
	/**你的积分总额*/
	public static final String TXT_CASE_POINTSTOTAL=  "txt.plan.pointstotal";
	/**支付后积分余额*/
	public static final String TXT_CASE_SURPLUSPOINT=  "txt.plan.surpluspoint";
	/**立即支付*/
	public static final String TXT_CASE_PAY=  "txt.plan.pay";
	/**关键字之间请以空格区分！*/
	public static final String TXT_CASE_TAGNAMES_PROMPTLANGUAGE=  "txt.plan.tagnames.promptlanguage";
	/**元*/
	public static final String TXT_CASE_ELEMENT= "txt.plan.element";
	/**折扣*/
	public static final String TXT_CASE_DISCOUNT= "txt.plan.discount";
	/**无折扣*/
	public static final String TXT_CASE_NOTDISCOUNT= "txt.plan.notdiscount";
	
	
	
	
	/**课程ID为空*/
	public static final String MSG_E_EMPTY_COURSE_ID = "msg.e.empty.course.id";
	/**课程ID为空*/
	public static final String MSG_E_EMPTY_RESOURCE_ID = "msg.e.empty.resource.id";
	/**该课程下无章节*/
	public static final String MSG_E_EMPTY_COURSE_NOCHAPTER = "msg.e.empty.course.nochapter";	
	/**标签业务类型为空*/
	public static final String MSG_E_EMPTY_BUSSINESS_TYPE = "msg.e.empty.bussiness.type";
	/**章节ID为空*/
	public static final String MSG_E_EMPTY_CHAPTER_ID = "msg.e.empty.chapter.id";
	/**课程不存在*/
	public static final String MSG_E_EMPTY_COURSE = "msg.e.empty.course";
	/**资源不存在*/
	public static final String MSG_E_EMPTY_RESOURCE = "msg.e.empty.resource";
	/**章节不存在*/
	public static final String MSG_E_EMPTY_CHAPTER = "msg.e.empty.chapter";
	/**用户章节不存在*/
	public static final String MSG_E_EMPTY_USER_CHAPTER = "msg.e.empty.user.chapter";
	/**评论ID不存在*/
	public static final String MSG_E_EMPTY_DISCUSS_ID = "msg.e.empty.discuss.id";
	/**笔记ID为空*/
	public static final String MSG_E_EMPTY_NOTE_ID = "msg.e.empty.note.id";
	/**笔记内容不存在！*/
	public static final String MSG_E_EMPTY_NOTE_DATA = "msg.e.empty.note.data";
	/**没有权限删除笔记内容！*/
	public static final String MSG_E_NOT_ACCESS_DELETE_NOTE_DATA = "msg.e.not.access.delete.note.data";
	/**链接ID为空*/
	public static final String MSG_E_EMPTY_LINK_ID = "msg.e.empty.link.id";
	/**链接类型为空*/
	public static final String MSG_E_EMPTY_LINK_TYPE = "msg.e.empty.link.type";
	/**剩余积分不够*/
	public static final String MSG_E_POINT_NOT_ENOUGH = "msg.e.point.not.enough";
	/**笔记内容不能为空*/
	public static final String MSG_E_EMPTY_NOTE_CONTENT = "msg.e.empty.note.content";
	
	/**视频ID为空*/
	public static final String MSG_E_EMPTY_VIDEO_ID = "msg.e.empty.video.id";
	/**视频时长为空*/
	public static final String MSG_E_EMPTY_VIDEO_DURATION = "msg.e.empty.video.duration";
	/**支付成功*/
	public static final String MSG_PAY_SUCCESS = "msg.pay.success";
	
	/**审批不通过*/
	public static final String MSG_TEACHERAPPLY_DISAGREE = "msg.teacherApply.disAgree";
	/**审批通过*/
	public static final String MSG_TEACHERAPPLY_AGREE = "msg.teacherApply.agree";
	/**确认退出计划*/
	public static final String MSG_EXIT_PLAN = "msg.exit.plan";
	
	/**用户课程不存在*/
	public static final String MSG_EMPTY_USER_COURSE = "msg.empty.user.course";
	/**{0}回复了您的评论*/
	public static final String MSG_SOMEONE_REPLY_DISCUSS = "msg.someone.reply.discuss";
	/***/
	public static final String MSG_INPUT_CODE = "msg.input.code";
	/***/
	public static final String MSG_CODE = "msg.code";
}
