package com.cloudsoaring.web.course.view;

import com.cloudsoaring.web.course.entity.UserMeasurementResultEntity;


/**
 * 用户测试结果视图
 * @author JGJ
 *
 */
@SuppressWarnings("serial")
public class UserMeasurementResultView extends UserMeasurementResultEntity {
	//问题总数
	private int questionCount;
	//剩余时间-秒单位
	private long residualTime;

	public int getQuestionCount() {
		return questionCount;
	}

	public void setQuestionCount(int questionCount) {
		this.questionCount = questionCount;
	}

	public long getResidualTime() {
		return residualTime;
	}

	public void setResidualTime(long residualTime) {
		this.residualTime = residualTime;
	}
}
