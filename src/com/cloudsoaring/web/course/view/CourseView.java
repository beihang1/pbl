package com.cloudsoaring.web.course.view;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.cloudsoaring.web.bus.entity.TagEntity;
import com.cloudsoaring.web.course.entity.CourseEntity;
import com.cloudsoaring.web.course.entity.QuestionEntity;

/**
 * 课程View
 * @author liuyanshuang
 *
 */
public class CourseView extends CourseEntity{

	private static final long serialVersionUID = 1L;
	
	/**封面*/
	private String pictureId;
	/**课程标签以逗号分隔的字符串*/
	private String tagString;
	/**章节中包含的视频总数*/
	private int chapterVideoCount;
	/**测试题数量*/
	private int measurmentCount;
	/**平均评分*/
	private BigDecimal averageScore;
	/**内容实用平均分*/
	private BigDecimal contentScore;
	/**通俗易懂平均分*/
	private BigDecimal simpleScore;
	/**逻辑清晰平均分*/
	private BigDecimal logicScore;
	/**讲师姓名*/
	private String teacherName;
	/**教师简介*/
	private String teacherIntroduct;
	/**老师头像*/
	private String tearcherImage;
	/**难度*/
	private String difficulty;
	/**用户是否加入课程*/
	private BigDecimal joinCourse;
	/**加入课程时间*/
	private java.util.Date joinDate;
	/**用户投票选项*/
	private String vote;
	/**投票时间*/
	private java.util.Date voteDate;
	/**是否开启测试*/
	private String flagTest;
	/**是否开启作业*/
	private String flagHomeWork;
	/**用户是否关注/收藏课程*/
	private BigDecimal favorite;
	/**收藏时间*/
	private java.util.Date favoriteDate;
	/**用户是否评价过该课程*/
	private BigDecimal scoreCourse;
	/*** 评价内容 */
	private String voteContent;
	/**最近学习的章节ID*/
	private String lastChapterId;
	/**最近学习的章节标题*/
	private String lastChapterTitle;
	/**课程学习进度(百分比）*/
	private BigDecimal studyProgress;
	/**当前用户是否登录，1：登录，2：未登录*/
	private BigDecimal userLogin;
	/**学习用时(秒)*/
	private java.math.BigDecimal timeCost;
	/**步骤ID*/
	private String planNodeId;
	
	/**课程最后学习时间*/
	private Date lastStudyDate;
	/**方向标签*/
	private TagEntity directTag;
	/**分类标签*/
	private TagEntity categoryTag;
	/**教师职位*/
	private String position;
	/**标签ID*/
	private String tagId;
	/**父类标签ID*/
	private String parentTagId;
	
	/**标签名称*/
	private String tagName;
	
	/**用户ID*/
	private String userId;
	
	/**课程完成情况*/
	private String finishType;
	
	/**根据linkID检索的问卷列表*/
	private List<QuestionEntity> questionList;
	/**用户是否完成问卷 0未完成 1已完成*/
	private String finishQuestionnaireFlag;
	/**课程是否存在问卷 0否 1是*/
	private String existQuestionnaireFlag;
	/**一级标签*/
    private String firstTag;    
    private String firstTagName;
    /**二级标签*/
    private String secondTag;   
    private String secondTagName;
    
    /**课程总章节数--我的课程-已参与 的百分比=课程中已完成章节数/课程总章节数*/
	private BigDecimal allChapterNum;
	
	/**课程完成章节数--我的课程-已参与 的百分比=课程中已完成章节数/课程总章节数*/
	private BigDecimal finishChapterNum;
	/**推荐度NUM_RECOMMEND*/
    private java.math.BigDecimal numRecommend;
    /**课程须知*/
	/*private String notice;
	*//**课程目标*//*
	private String target;*/
    //老师名称
    private String userName;
    private String schoolNatrul;//学校性质
    private String tagsId;//分类id
    
	public java.math.BigDecimal getNumRecommend() {
        return numRecommend;
    }
    public void setNumRecommend(java.math.BigDecimal numRecommend) {
        this.numRecommend = numRecommend;
    }
    public String getFirstTag() {
		return firstTag;
	}
	public void setFirstTag(String firstTag) {
		this.firstTag = firstTag;
	}
	public String getFirstTagName() {
		return firstTagName;
	}
	public void setFirstTagName(String firstTagName) {
		this.firstTagName = firstTagName;
	}
	public String getSecondTag() {
		return secondTag;
	}
	public void setSecondTag(String secondTag) {
		this.secondTag = secondTag;
	}
	public String getSecondTagName() {
		return secondTagName;
	}
	public void setSecondTagName(String secondTagName) {
		this.secondTagName = secondTagName;
	}
	public List<QuestionEntity> getQuestionList() {
		return questionList;
	}
	public void setQuestionList(List<QuestionEntity> questionList) {
		this.questionList = questionList;
	}
	public String getFinishType() {
		return finishType;
	}
	public void setFinishType(String finishType) {
		this.finishType = finishType;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Date getLastStudyDate() {
		return lastStudyDate;
	}
	public void setLastStudyDate(Date lastStudyDate) {
		this.lastStudyDate = lastStudyDate;
	}
	public String getTagId() {
		return tagId;
	}
	public void setTagId(String tagId) {
		this.tagId = tagId;
	}
	public String getTagName() {
		return tagName;
	}
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	public String getTagString() {
		return tagString;
	}
	public void setTagString(String tagString) {
		this.tagString = tagString;
	}
	public int getChapterVideoCount() {
		return chapterVideoCount;
	}
	public void setChapterVideoCount(int chapterVideoCount) {
		this.chapterVideoCount = chapterVideoCount;
	}
	public int getMeasurmentCount() {
		return measurmentCount;
	}
	public void setMeasurmentCount(int measurmentCount) {
		this.measurmentCount = measurmentCount;
	}
	public BigDecimal getAverageScore() {
		return averageScore;
	}
	public void setAverageScore(BigDecimal averageScore) {
		this.averageScore = averageScore;
	}
	public BigDecimal getContentScore() {
		return contentScore;
	}
	public void setContentScore(BigDecimal contentScore) {
		this.contentScore = contentScore;
	}
	public BigDecimal getSimpleScore() {
		return simpleScore;
	}
	public void setSimpleScore(BigDecimal simpleScore) {
		this.simpleScore = simpleScore;
	}
	public BigDecimal getLogicScore() {
		return logicScore;
	}
	public void setLogicScore(BigDecimal logicScore) {
		this.logicScore = logicScore;
	}
	public String getTeacherName() {
		return teacherName;
	}
	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}
	public String getTeacherIntroduct() {
		return teacherIntroduct;
	}
	public void setTeacherIntroduct(String teacherIntroduct) {
		this.teacherIntroduct = teacherIntroduct;
	}
	public BigDecimal getJoinCourse() {
		return joinCourse;
	}
	public void setJoinCourse(BigDecimal joinCourse) {
		this.joinCourse = joinCourse;
	}
	public BigDecimal getFavorite() {
		return favorite;
	}
	public void setFavorite(BigDecimal favorite) {
		this.favorite = favorite;
	}
	public BigDecimal getScoreCourse() {
		return scoreCourse;
	}
	public void setScoreCourse(BigDecimal scoreCourse) {
		this.scoreCourse = scoreCourse;
	}
	public String getLastChapterId() {
		return lastChapterId;
	}
	public void setLastChapterId(String lastChapterId) {
		this.lastChapterId = lastChapterId;
	}
	public String getLastChapterTitle() {
		return lastChapterTitle;
	}
	public void setLastChapterTitle(String lastChapterTitle) {
		this.lastChapterTitle = lastChapterTitle;
	}
	public BigDecimal getStudyProgress() {
		return studyProgress;
	}
	public void setStudyProgress(BigDecimal studyProgress) {
		this.studyProgress = studyProgress;
	}
	public BigDecimal getUserLogin() {
		return userLogin;
	}
	public void setUserLogin(BigDecimal userLogin) {
		this.userLogin = userLogin;
	}
	public java.math.BigDecimal getTimeCost() {
		return timeCost;
	}
	public void setTimeCost(java.math.BigDecimal timeCost) {
		this.timeCost = timeCost;
	}
	public String getDifficulty() {
		return difficulty;
	}
	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}
	public java.util.Date getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(java.util.Date joinDate) {
		this.joinDate = joinDate;
	}
	public String getVote() {
		return vote;
	}
	public void setVote(String vote) {
		this.vote = vote;
	}
	public java.util.Date getVoteDate() {
		return voteDate;
	}
	public void setVoteDate(java.util.Date voteDate) {
		this.voteDate = voteDate;
	}
	public java.util.Date getFavoriteDate() {
		return favoriteDate;
	}
	public void setFavoriteDate(java.util.Date favoriteDate) {
		this.favoriteDate = favoriteDate;
	}
	public String getVoteContent() {
		return voteContent;
	}
	public void setVoteContent(String voteContent) {
		this.voteContent = voteContent;
	}
	public String getPlanNodeId() {
		return planNodeId;
	}
	public void setPlanNodeId(String planNodeId) {
		this.planNodeId = planNodeId;
	}
	public String getTearcherImage() {
		return tearcherImage;
	}
	public void setTearcherImage(String tearcherImage) {
		this.tearcherImage = tearcherImage;
	}
	public String getFinishQuestionnaireFlag() {
		return finishQuestionnaireFlag;
	}
	public void setFinishQuestionnaireFlag(String finishQuestionnaireFlag) {
		this.finishQuestionnaireFlag = finishQuestionnaireFlag;
	}
	public String getExistQuestionnaireFlag() {
		return existQuestionnaireFlag;
	}
	public void setExistQuestionnaireFlag(String existQuestionnaireFlag) {
		this.existQuestionnaireFlag = existQuestionnaireFlag;
	}
	public String getParentTagId() {
		return parentTagId;
	}
	public void setParentTagId(String parentTagId) {
		this.parentTagId = parentTagId;
	}
	public TagEntity getDirectTag() {
		return directTag;
	}
	public void setDirectTag(TagEntity directTag) {
		this.directTag = directTag;
	}
	public TagEntity getCategoryTag() {
		return categoryTag;
	}
	public void setCategoryTag(TagEntity categoryTag) {
		this.categoryTag = categoryTag;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public BigDecimal getAllChapterNum() {
		return allChapterNum;
	}
	public void setAllChapterNum(BigDecimal allChapterNum) {
		this.allChapterNum = allChapterNum;
	}
	public BigDecimal getFinishChapterNum() {
		return finishChapterNum;
	}
	public void setFinishChapterNum(BigDecimal finishChapterNum) {
		this.finishChapterNum = finishChapterNum;
	}
	public String getFlagTest() {
		return flagTest;
	}
	public void setFlagTest(String flagTest) {
		this.flagTest = flagTest;
	}
	public String getFlagHomeWork() {
		return flagHomeWork;
	}
	public void setFlagHomeWork(String flagHomeWork) {
		this.flagHomeWork = flagHomeWork;
	}
	/**封面*/
	public String getPictureId(){
		return this.pictureId;
	}
	/**封面*/
	public void setPictureId(String pictureId){
		this.pictureId = pictureId;
	}
	/**课程须知*/
	/*public String getNotice(){
		return this.notice;
	}*/
	/**课程须知*//*
	public void setNotice(String notice){
		this.notice = notice;
	}
	*//**课程目标*//*
	public String getTarget(){
		return this.target;
	}
	*//**课程目标*//*
	public void setTarget(String target){
		this.target = target;
	}*/
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getSchoolNatrul() {
		return schoolNatrul;
	}
	public void setSchoolNatrul(String schoolNatrul) {
		this.schoolNatrul = schoolNatrul;
	}
	public String getTagsId() {
		return tagsId;
	}
	public void setTagsId(String tagsId) {
		this.tagsId = tagsId;
	}
	
}
