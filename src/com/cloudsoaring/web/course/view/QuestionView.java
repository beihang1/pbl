package com.cloudsoaring.web.course.view;

import java.util.ArrayList;
import java.util.List;

import com.cloudsoaring.web.course.entity.QuestionEntity;
import com.cloudsoaring.web.course.entity.QuestionOptionEntity;

@SuppressWarnings("serial")
public class QuestionView extends QuestionEntity {
	
	/**问卷标题*/
	private String titile;
	/**当前用户*/
	private String userId;
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getTitile() {
		return titile;
	}

	public void setTitile(String titile) {
		this.titile = titile;
	}

	/**题目list*/
	List<QuestionOptionEntity> option = new ArrayList<QuestionOptionEntity>();

	public List<QuestionOptionEntity> getOption() {
		return option;
	}

	public void setOption(List<QuestionOptionEntity> option) {
		this.option = option;
	}
	
}
