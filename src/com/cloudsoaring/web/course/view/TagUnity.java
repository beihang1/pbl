package com.cloudsoaring.web.course.view;

public class TagUnity {
	
	private String tagId;
	private String tagName;
	private String parentId;
	private String imageId;
	private String imageUrl;
	private String imageName;
	private String fileUrl;
	private String fileId;
	private String fileName;
	private String resourceId;
	private String briefInTroduction;
	private String linkType;
	private String tagTypeId;
	
	public String getTagTypeId() {
		return tagTypeId;
	}
	public void setTagTypeId(String tagTypeId) {
		this.tagTypeId = tagTypeId;
	}
	public String getLinkType() {
		return linkType;
	}
	public void setLinkType(String linkType) {
		this.linkType = linkType;
	}
	public String getBriefInTroduction() {
		return briefInTroduction;
	}
	public void setBriefInTroduction(String briefInTroduction) {
		this.briefInTroduction = briefInTroduction;
	}
	public String getTagId() {
		return tagId;
	}
	public void setTagId(String tagId) {
		this.tagId = tagId;
	}
	public String getTagName() {
		return tagName;
	}
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getImageId() {
		return imageId;
	}
	public void setImageId(String imageId) {
		this.imageId = imageId;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public String getFileUrl() {
		return fileUrl;
	}
	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	public String getResourceId() {
		return resourceId;
	}
	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
