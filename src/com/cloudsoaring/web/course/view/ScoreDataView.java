package com.cloudsoaring.web.course.view;

import com.cloudsoaring.web.course.entity.ScoreDataEntity;

public class ScoreDataView extends ScoreDataEntity {
	private static final long serialVersionUID = 1L;
	//头像
	private String headIMG;
	//用户名
	private String userName;
	public String getHeadIMG() {
		return headIMG;
	}
	public void setHeadIMG(String headIMG) {
		this.headIMG = headIMG;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}
