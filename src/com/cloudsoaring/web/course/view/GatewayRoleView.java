package com.cloudsoaring.web.course.view;

import java.io.Serializable;

import com.cloudsoaring.web.bus.entity.RoleEntity;

/**
 * 权限系统-gateway管理-角色设置需要用到的类，其中包含角色信息、角色的权限信息
 * @author liuyanshuang
 *
 */
public class GatewayRoleView implements Serializable{

	private static final long serialVersionUID = 1L;

	/**角色信息Entity*/
	private RoleEntity roleEntity;
	/**角色对应的权限范围字符串**/
	private String roleModuleStr;
	
	public RoleEntity getRoleEntity() {
		return roleEntity;
	}
	public void setRoleEntity(RoleEntity roleEntity) {
		this.roleEntity = roleEntity;
	}
	public String getRoleModuleStr() {
		return roleModuleStr;
	}
	public void setRoleModuleStr(String roleModuleStr) {
		this.roleModuleStr = roleModuleStr;
	}
}
