package com.cloudsoaring.web.course.view;

import java.util.ArrayList;
import java.util.List;

import com.cloudsoaring.web.common.entity.BaseEntity;
import com.cloudsoaring.web.course.entity.DiscussDataEntity;

/**   
 * @Title: Entity
 * @Description: tb_discuss
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class DiscussView extends BaseEntity{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**讨论标题*/
	private String title;
	/**链接ID*/
	private String linkId;
	/**链接类型(COURSE:课程,CHAPTER:章节)*/
	private String linkType;
	/**点赞数*/
	private java.math.BigDecimal numLike;
	
	/**是否可删除笔记*/
	private Boolean canDelete = false;
	/**用户是否点赞，值为0时，未点赞*/
	private Integer userLike;
	
	private String createTutorDate;
	//回复评论
	private String replyId;
	//回复人姓名
	private String replyName;
	//回复内容
	private String replyContent;
	//回复时间
	private String replyDate;
	//回复id
	private String targetDiscussUserId;
	//是否回复
	private String isReply;
	//回复人头像
	private String replyImage;
	public Boolean getCanDelete() {
		return canDelete;
	}

	public void setCanDelete(Boolean canDelete) {
		this.canDelete = canDelete;
	}

	public Integer getUserLike() {
		return userLike;
	}

	public void setUserLike(Integer userLike) {
		this.userLike = userLike;
	}

	/**讨论ID*/
	private String discussId;
	/**发表用户ID*/
	private String userId;
	/**评论内容*/
	private String content;
	/**要回复的评论ID*/
	private String targetDiscussId;
	/**要回复的用户ID*/
	private String targetUserId;
	/**要回复人的姓名*/
	private String targetPersonName;
	
	/**用户名*/
	private String userName;
	
	/**姓名*/
	private String personName;
	
	/**头像*/
	private String image;
	
	/**源自课程/章节 名称*/
	private String courseTitle;
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getCourseTitle() {
		return courseTitle;
	}

	public void setCourseTitle(String courseTitle) {
		this.courseTitle = courseTitle;
	}

	private List<DiscussDataEntity>DiscussDataList=new ArrayList<DiscussDataEntity>();

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLinkId() {
		return linkId;
	}

	public void setLinkId(String linkId) {
		this.linkId = linkId;
	}

	public String getLinkType() {
		return linkType;
	}

	public void setLinkType(String linkType) {
		this.linkType = linkType;
	}

	public List<DiscussDataEntity> getDiscussDataList() {
		return DiscussDataList;
	}

	public void setDiscussDataList(List<DiscussDataEntity> discussDataList) {
		DiscussDataList = discussDataList;
	}

	public String getDiscussId() {
		return discussId;
	}

	public void setDiscussId(String discussId) {
		this.discussId = discussId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTargetDiscussId() {
		return targetDiscussId;
	}

	public void setTargetDiscussId(String targetDiscussId) {
		this.targetDiscussId = targetDiscussId;
	}

	public String getTargetUserId() {
		return targetUserId;
	}

	public void setTargetUserId(String targetUserId) {
		this.targetUserId = targetUserId;
	}

	public java.math.BigDecimal getNumLike() {
		return numLike;
	}

	public void setNumLike(java.math.BigDecimal numLike) {
		this.numLike = numLike;
	}

	public String getTargetPersonName() {
		return targetPersonName;
	}

	public void setTargetPersonName(String targetPersonName) {
		this.targetPersonName = targetPersonName;
	}

    public String getCreateTutorDate() {
        return createTutorDate;
    }

    public void setCreateTutorDate(String createTutorDate) {
        this.createTutorDate = createTutorDate;
    }

	public String getReplyName() {
		return replyName;
	}

	public void setReplyName(String replyName) {
		this.replyName = replyName;
	}

	public String getReplyContent() {
		return replyContent;
	}

	public void setReplyContent(String replyContent) {
		this.replyContent = replyContent;
	}

	public String getReplyDate() {
		return replyDate;
	}

	public void setReplyDate(String replyDate) {
		this.replyDate = replyDate;
	}

	public String getTargetDiscussUserId() {
		return targetDiscussUserId;
	}

	public void setTargetDiscussUserId(String targetDiscussUserId) {
		this.targetDiscussUserId = targetDiscussUserId;
	}

	public String getReplyId() {
		return replyId;
	}

	public void setReplyId(String replyId) {
		this.replyId = replyId;
	}

	public String getIsReply() {
		return isReply;
	}

	public void setIsReply(String isReply) {
		this.isReply = isReply;
	}

	public String getReplyImage() {
		return replyImage;
	}

	public void setReplyImage(String replyImage) {
		this.replyImage = replyImage;
	}

	public void setReply(String isReply) {
		this.isReply = isReply;
	}
	
	
	
}
