package com.cloudsoaring.web.course.view;

public class ChapterUnity {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private java.lang.String title;
	private java.lang.String chapterId;
	
	public java.lang.String getTitle() {
		return title;
	}
	public void setTitle(java.lang.String title) {
		this.title = title;
	}
	public java.lang.String getChapterId() {
		return chapterId;
	}
	public void setChapterId(java.lang.String chapterId) {
		this.chapterId = chapterId;
	}
	

}
