package com.cloudsoaring.web.course.view;


public class TeacherView extends com.cloudsoaring.web.bus.entity.UserEntity{
    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;
    
    private String roleId;
    private String roleName;
    private String roleTeacherId;
    private String roleAdministratorId;
    private String schoolName;
    public String getRoleId() {
        return roleId;
    }
    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }
    public String getRoleName() {
        return roleName;
    }
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
    public String getRoleTeacherId() {
        return roleTeacherId;
    }
    public void setRoleTeacherId(String roleTeacherId) {
        this.roleTeacherId = roleTeacherId;
    }
    public String getRoleAdministratorId() {
        return roleAdministratorId;
    }
    public void setRoleAdministratorId(String roleAdministratorId) {
        this.roleAdministratorId = roleAdministratorId;
    }
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
    
    
}
