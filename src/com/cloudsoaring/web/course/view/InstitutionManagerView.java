package com.cloudsoaring.web.course.view;

import java.util.Date;
import java.util.List;

import com.cloudsoaring.common.annotation.ClassDefine;
import com.cloudsoaring.common.annotation.FieldDefine;
import com.cloudsoaring.common.validate.FieldType;
import com.cloudsoaring.web.common.entity.BaseEntity;
/**
 * tb_institution 组织结构基本信息表
 * @author lijianlong 2020/05/15
 *
 */
@ClassDefine
public class InstitutionManagerView extends BaseEntity {

	/** serialVersionUID*/
	private static final long serialVersionUID = 1L;
	/** 机构名称*/
	@FieldDefine(description="机构名称",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String institutionName;
	/** 机构类型,0:教育局,1:学校*/
	@FieldDefine(description="机构类型",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String  institutionType;
	/** 学校对应教育局类别*/
	private String  institutionParentId;
	/** 机构简称*/
	@FieldDefine(description="机构简称",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String  institutionAbbreviation;
	/** 机构地址*/
	@FieldDefine(description="机构地址",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String  institutionAddress;
	/** 机构具体地址*/
	@FieldDefine(description="机构具体地址",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String  institutionSpecificAddress;
	/** 门户地址*/
	@FieldDefine(description="门户地址",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String  portalAddress;
	/** 性质,学校专有字段：公办、民办*/
	@FieldDefine(description="性质",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String  nature;
	/** 联系电话*/
	@FieldDefine(description="联系电话",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String  telphone;
	/** 电子邮箱*/
	@FieldDefine(description="电子邮箱",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String  email;
	/** 门户类型,教育局专有字段：自主配置、顶级门户*/
	@FieldDefine(description="门户类型",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String  portalType;
	/** 是否合作,学校专有字段*/
	@FieldDefine(description="是否合作",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String  ifCooperation;
	/** 学校类型,学校专有字段*/
	@FieldDefine(description="学校类型",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String  schoolType;
	/** 学校代码,学校专有字段*/
	@FieldDefine(description="学校代码",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String  schoolCode;
	/** 隶属关系,学校专有字段：一般、省直、市直*/
	@FieldDefine(description="隶属关系",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String  affiliation;
	/**所属教育局*/
	@FieldDefine(description="所属教育局",maxLength="10",columnIndex="B",rowIndex="8" ,required=true,type=FieldType.String)
	private String  edution;
	/** 导入创建人员*/
	private String  createUser;
	/** 导入更新人员*/
	private String  updateUser;
	/** 创建时间*/
	private Date  createTime;
	/** 更新时间*/
	private Date  updateTime;
	/** */
	private String  image;
	/** */
	private String  firstTag;
	private String  secondTag;
	private List<InstitutionManagerView> institutionList;
	public String getInstitutionName() {
		return institutionName;
	}
	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}
	public String getInstitutionType() {
		return institutionType;
	}
	public void setInstitutionType(String institutionType) {
		this.institutionType = institutionType;
	}
	public String getInstitutionParentId() {
		return institutionParentId;
	}
	public void setInstitutionParentId(String institutionParentId) {
		this.institutionParentId = institutionParentId;
	}
	public String getInstitutionAbbreviation() {
		return institutionAbbreviation;
	}
	public void setInstitutionAbbreviation(String institutionAbbreviation) {
		this.institutionAbbreviation = institutionAbbreviation;
	}
	public String getInstitutionAddress() {
		return institutionAddress;
	}
	public void setInstitutionAddress(String institutionAddress) {
		this.institutionAddress = institutionAddress;
	}
	public String getInstitutionSpecificAddress() {
		return institutionSpecificAddress;
	}
	public void setInstitutionSpecificAddress(String institutionSpecificAddress) {
		this.institutionSpecificAddress = institutionSpecificAddress;
	}
	public String getPortalAddress() {
		return portalAddress;
	}
	public void setPortalAddress(String portalAddress) {
		this.portalAddress = portalAddress;
	}
	public String getNature() {
		return nature;
	}
	public void setNature(String nature) {
		this.nature = nature;
	}
	public String getTelphone() {
		return telphone;
	}
	public void setTelphone(String telphone) {
		this.telphone = telphone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPortalType() {
		return portalType;
	}
	public void setPortalType(String portalType) {
		this.portalType = portalType;
	}
	public String getIfCooperation() {
		return ifCooperation;
	}
	public void setIfCooperation(String ifCooperation) {
		this.ifCooperation = ifCooperation;
	}
	public String getSchoolType() {
		return schoolType;
	}
	public void setSchoolType(String schoolType) {
		this.schoolType = schoolType;
	}
	public String getSchoolCode() {
		return schoolCode;
	}
	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}
	public String getAffiliation() {
		return affiliation;
	}
	public void setAffiliation(String affiliation) {
		this.affiliation = affiliation;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getEdution() {
		return edution;
	}
	public void setEdution(String edution) {
		this.edution = edution;
	}
	public List<InstitutionManagerView> getInstitutionList() {
		return institutionList;
	}
	public void setInstitutionList(List<InstitutionManagerView> institutionList) {
		this.institutionList = institutionList;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getFirstTag() {
		return firstTag;
	}
	public void setFirstTag(String firstTag) {
		this.firstTag = firstTag;
	}
	public String getSecondTag() {
		return secondTag;
	}
	public void setSecondTag(String secondTag) {
		this.secondTag = secondTag;
	}
	
	

}
