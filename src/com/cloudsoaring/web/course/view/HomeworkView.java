package com.cloudsoaring.web.course.view;

import java.util.List;

import com.cloudsoaring.web.course.entity.HomeworkEntity;

public class HomeworkView extends HomeworkEntity{
	
	private static final long serialVersionUID = 1L;
	//作业
	/**HOMEWORK_ID*/
	private String homeworkId;
	/**ANSWER_USER*/
	private String answerUser;
	/**SCORE*/
	private String score;
	/**TEACHER_COMMENT*/
	private String teacherComment;
	/**SCORE*/
	private String teacherScore;
	/**TEACHER_COMMENT*/
	private String teacherComments;
	/**CONTENT*/
	private String answerContent;
	/**附件ID*/
	private String answerFileId;
	/**附件ID*/
	private String answerFileName;
	/**课程名称*/
	private String courseTitle;
	/***/
	private String fileRealName;
	/***/
	private boolean isdown=false;
	/**姓名*/
	private String userName;
	/**性别*/
	private String sex;
	/***/
	private String audioType;
	/***/
	private String audioId;
	/***/
	private String audioContent;
	
	/**LINK_TYPE*/
	private String linkType;
	/**LINK_ID*/
	private String linkId;
	/***/
	private String pageType;
	/***/
	private String courseId;
	/***/
	private java.math.BigDecimal likeNum;
	/***/
	private String createTime;
	//分享标识：0取消分享，1分享
	private String share;
	//置顶标识：0取消置顶标识，1置顶标识
	private String top;
	//置顶标识排序字段
	private Integer topNum;
	/***/
	private Integer teacher;
	/***/
	private List<String> userCourseList;
	//课程名字
	private String courseName;
	//章节名称
	private String chapterName;
	//年级名称
	private String gradeName;
	//班级名称
	private String className;
	//作业创建时间
	private String createStartDate;
	//作业创建时间
	private String createEndDate;
	//用户id
	private String userId;
	//用户名称
	private String personName;
	//教师评论
	private String teaEva;
	//学生评论
	private String stu;
	private String status;
	//学生提交作业内容
	private String studentComment;
	//老师点评内容
	private String teacherEvalution;
	//作业title
	private String title;
	private String answerId;
	//老师id
	private String teacherId;
	//作业提交id
	private String homeanwserId;
	//是否编辑作业
	private String examStatus;
	//章节id
	private String chapterId;
	//作业答题主键
    private String homeworkIds;
    //创建时间
    private String crDate;
    //作业状态
    private String evStatus;
    //创客编程章节标志0否1是
    private String isMakerFlag;
    private String teacherName;//教师名称
    private String image;//头像
	
	public String getIsMakerFlag() {
		return isMakerFlag;
	}
	public void setIsMakerFlag(String isMakerFlag) {
		this.isMakerFlag = isMakerFlag;
	}
	
	public List<String> getUserCourseList() {
		return userCourseList;
	}
	public void setUserCourseList(List<String> userCourseList) {
		this.userCourseList = userCourseList;
	}
	
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getFileRealName() {
		return fileRealName;
	}
	public void setFileRealName(String fileRealName) {
		this.fileRealName = fileRealName;
	}
	public java.math.BigDecimal getLikeNum() {
		return likeNum;
	}
	public void setLikeNum(java.math.BigDecimal likeNum) {
		this.likeNum = likeNum;
	}
	public String getCourseId() {
		return courseId;
	}
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
	public String getPageType() {
		return pageType;
	}
	public void setPageType(String pageType) {
		this.pageType = pageType;
	}
	/**LINK_TYPE*/
	public String getLinkType(){
		return this.linkType;
	}
	/**LINK_TYPE*/
	public void setLinkType(String linkType){
		this.linkType = linkType;
	}
	/**LINK_ID*/
	public String getLinkId(){
		return this.linkId;
	}
	/**LINK_ID*/
	public void setLinkId(String linkId){
		this.linkId = linkId;
	}
	
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public boolean isIsdown() {
		return isdown;
	}
	public void setIsdown(boolean isdown) {
		this.isdown = isdown;
	}
	public String getCourseTitle() {
		return courseTitle;
	}
	public void setCourseTitle(String courseTitle) {
		this.courseTitle = courseTitle;
	}
	public String getHomeworkId() {
		return homeworkId;
	}
	public void setHomeworkId(String homeworkId) {
		this.homeworkId = homeworkId;
	}
	public String getAnswerUser() {
		return answerUser;
	}
	public void setAnswerUser(String answerUser) {
		this.answerUser = answerUser;
	}
	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}
	public String getTeacherComment() {
		return teacherComment;
	}
	public void setTeacherComment(String teacherComment) {
		this.teacherComment = teacherComment;
	}
	public String getAnswerContent() {
		return answerContent;
	}
	public void setAnswerContent(String answerContent) {
		this.answerContent = answerContent;
	}
	public String getAnswerFileId() {
		return answerFileId;
	}
	public void setAnswerFileId(String answerFileId) {
		this.answerFileId = answerFileId;
	}
	
	public String getAudioType() {
		return audioType;
	}
	public void setAudioType(String audioType) {
		this.audioType = audioType;
	}
	public String getAudioId() {
		return audioId;
	}
	public void setAudioId(String audioId) {
		this.audioId = audioId;
	}
	public String getAudioContent() {
		return audioContent;
	}
	public void setAudioContent(String audioContent) {
		this.audioContent = audioContent;
	}
	public String getAnswerFileName() {
		return answerFileName;
	}
	public void setAnswerFileName(String answerFileName) {
		this.answerFileName = answerFileName;
	}
	public String getTeacherScore() {
		return teacherScore;
	}
	public void setTeacherScore(String teacherScore) {
		this.teacherScore = teacherScore;
	}
	public String getTeacherComments() {
		return teacherComments;
	}
	public void setTeacherComments(String teacherComments) {
		this.teacherComments = teacherComments;
	}
	public String getShare() {
		return share;
	}
	public void setShare(String share) {
		this.share = share;
	}
	public String getTop() {
		return top;
	}
	public void setTop(String top) {
		this.top = top;
	}
	public Integer getTopNum() {
		return topNum;
	}
	public void setTopNum(Integer topNum) {
		this.topNum = topNum;
	}
	public Integer getTeacher() {
		return teacher;
	}
	public void setTeacher(Integer teacher) {
		this.teacher = teacher;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getChapterName() {
		return chapterName;
	}
	public void setChapterName(String chapterName) {
		this.chapterName = chapterName;
	}
	public String getGradeName() {
		return gradeName;
	}
	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getCreateStartDate() {
		return createStartDate;
	}
	public void setCreateStartDate(String createStartDate) {
		this.createStartDate = createStartDate;
	}
	public String getCreateEndDate() {
		return createEndDate;
	}
	public void setCreateEndDate(String createEndDate) {
		this.createEndDate = createEndDate;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getTeaEva() {
		return teaEva;
	}
	public void setTeaEva(String teaEva) {
		this.teaEva = teaEva;
	}
	public String getStu() {
		return stu;
	}
	public void setStu(String stu) {
		this.stu = stu;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStudentComment() {
		return studentComment;
	}
	public void setStudentComment(String studentComment) {
		this.studentComment = studentComment;
	}
	public String getTeacherEvalution() {
		return teacherEvalution;
	}
	public void setTeacherEvalution(String teacherEvalution) {
		this.teacherEvalution = teacherEvalution;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	
	public String getAnswerId() {
		return answerId;
	}
	public void setAnswerId(String answerId) {
		this.answerId = answerId;
	}
	public String getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}
	public String getHomeanwserId() {
		return homeanwserId;
	}
	public void setHomeanwserId(String homeanwserId) {
		this.homeanwserId = homeanwserId;
	}
	public String getExamStatus() {
		return examStatus;
	}
	public void setExamStatus(String examStatus) {
		this.examStatus = examStatus;
	}
	public String getChapterId() {
		return chapterId;
	}
	public void setChapterId(String chapterId) {
		this.chapterId = chapterId;
	}
	public String getHomeworkIds() {
		return homeworkIds;
	}
	public void setHomeworkIds(String homeworkIds) {
		this.homeworkIds = homeworkIds;
	}
	public String getCrDate() {
		return crDate;
	}
	public void setCrDate(String crDate) {
		this.crDate = crDate;
	}
	public String getEvStatus() {
		return evStatus;
	}
	public void setEvStatus(String evStatus) {
		this.evStatus = evStatus;
	}
	public String getTeacherName() {
		return teacherName;
	}
	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	
	
}
