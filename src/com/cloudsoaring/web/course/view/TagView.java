package com.cloudsoaring.web.course.view;

import java.util.ArrayList;
import java.util.List;

import com.cloudsoaring.web.bus.entity.TagEntity;

public class TagView extends com.cloudsoaring.web.bus.entity.TagEntity {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/** 标签类ID */
	private String tagTypeId;
	/** 标签类名 */
	private String tagTypeName;
	/** 标签类型,1单值，2区间 */
	private String tagClass;
	/** 父标签类ID */
	private String parentId;
	/** 是否锁定 */
	private String flagLock;
	/** 删除标记 */
	private String flagDel;
	/** 备注 */
	private String remarks;
	/** 业务类型 */
	private String businessType;
	/** 链接ID */
	private String linkId;
	/**链接类型*/
    private String linkType;
	/** 父标签类ID */
	private String parentTagId;
	/**标签*/
	private List<TagEntity> list=new ArrayList<TagEntity>();
	
	public String getLinkId() {
        return linkId;
    }

    public void setLinkId(String linkId) {
        this.linkId = linkId;
    }

    public String getTagTypeId() {
        return tagTypeId;
    }

    public void setTagTypeId(String tagTypeId) {
		this.tagTypeId = tagTypeId;
	}

	public String getTagTypeName() {
		return tagTypeName;
	}

	public void setTagTypeName(String tagTypeName) {
		this.tagTypeName = tagTypeName;
	}

	public String getTagClass() {
		return tagClass;
	}

	public void setTagClass(String tagClass) {
		this.tagClass = tagClass;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getFlagLock() {
		return flagLock;
	}

	public void setFlagLock(String flagLock) {
		this.flagLock = flagLock;
	}

	public String getFlagDel() {
		return flagDel;
	}

	public void setFlagDel(String flagDel) {
		this.flagDel = flagDel;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getBusinessType() {
		return businessType;
	}

	public List<TagEntity> getList() {
		return list;
	}

	public void setList(List<TagEntity> list) {
		this.list = list;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	/**
	 * @return the parentTagId
	 */
	public String getParentTagId() {
		return parentTagId;
	}

	/**
	 * @param parentTagId the parentTagId to set
	 */
	public void setParentTagId(String parentTagId) {
		this.parentTagId = parentTagId;
	}

	/**
	 * @return the linkType
	 */
	public String getLinkType() {
		return linkType;
	}

	/**
	 * @param linkType the linkType to set
	 */
	public void setLinkType(String linkType) {
		this.linkType = linkType;
	}

}
