package com.cloudsoaring.web.course.view;

import java.util.List;

import com.cloudsoaring.web.course.entity.ChapterEntity;
import com.cloudsoaring.web.course.entity.QuestionEntity;

/**   
 * @Title: 章节列表
 * @Description: tb_user_chatper  tb_chapter
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class ChapterView extends ChapterEntity{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    //章节
    /**课程名称*/
	private String courseTitle;
	/**是否收藏*/
	private java.math.BigDecimal favorite;
    /**用户ID*/
    private String userId;
    /**课程学习进度(百分比）*/
    private java.math.BigDecimal studyProgress;
    /**测试And考试*/
    private String measurementId;
	public String userName;
	//用户头像
	private String image;
	/**总分设定*/
    private java.math.BigDecimal totalPoints;
    /**及格分数*/
    private java.math.BigDecimal standardPoints;
	/**问题列表*/
    private List<QuestionEntity> questionList;
    /**视频学习时间戳*/ 
    private java.math.BigDecimal lastVideoTime;
    /**封面名称*/
    private String pictureName;
    /**视频名称*/
    private String videoName;
    /**活动标签*/
    private String activitys;
    private String uploadVideoFile;
    /**废弃 的视频ID*/
    private String oldVid;
    //是否课程创建的考试
    private String isCourse;
    
    private String isMakerFlag;
    
    public String getIsMakerFlag() {
		return isMakerFlag;
	}
	public void setIsMakerFlag(String isMakerFlag) {
		this.isMakerFlag = isMakerFlag;
	}
	private String isTransForm;
	
	public String getIsTransForm() {
		return isTransForm;
	}
	public void setIsTransForm(String isTransForm) {
		this.isTransForm = isTransForm;
	}
	/**
     * 是否可学习,默认都可学习
     */
    private String flagStudy = "1";
    /**
     * 用户课程最后的学习章节
     */
    private String lastChapterId;
    /**
     * 课程按照顺序学习的记录
     */
    private String flagOrderStudy;
    /**是否必修课程*/
    private String flagRequired;
    //学生提交内容
    private String studentComment;
    
    public String getFlagRequired() {
        return flagRequired;
    }
    public void setFlagRequired(String flagRequired) {
        this.flagRequired = flagRequired;
    }
    public String getOldVid() {
        return oldVid;
    }
    public void setOldVid(String oldVid) {
        this.oldVid = oldVid;
    }
    public String getLastChapterId() {
		return lastChapterId;
	}
	public void setLastChapterId(String lastChapterId) {
		this.lastChapterId = lastChapterId;
	}
	public String getFlagOrderStudy() {
		return flagOrderStudy;
	}
	public void setFlagOrderStudy(String flagOrderStudy) {
		this.flagOrderStudy = flagOrderStudy;
	}
	private List<ChapterView> chileChapter;
    
    public String getVideoName() {
        return videoName;
    }
    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }
    public String getUploadVideoFile() {
        return uploadVideoFile;
    }
    public void setUploadVideoFile(String uploadVideoFile) {
        this.uploadVideoFile = uploadVideoFile;
    }
    public String getActivitys() {
        return activitys;
    }
    public void setActivitys(String activitys) {
        this.activitys = activitys;
    }
    public String getPictureName() {
        return pictureName;
    }
    public void setPictureName(String pictureName) {
        this.pictureName = pictureName;
    }
    public String getMeasurementId() {
        return measurementId;
    }
    public void setMeasurementId(String measurementId) {
        this.measurementId = measurementId;
    }
    public List<QuestionEntity> getQuestionList() {
        return questionList;
    }
    public void setQuestionList(List<QuestionEntity> questionList) {
        this.questionList = questionList;
    }
    public java.math.BigDecimal getLastVideoTime() {
		return lastVideoTime;
	}
	public void setLastVideoTime(java.math.BigDecimal lastVideoTime) {
		this.lastVideoTime = lastVideoTime;
	}
	public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
  
    public java.math.BigDecimal getStudyProgress() {
        return studyProgress;
    }
    public void setStudyProgress(java.math.BigDecimal studyProgress) {
        this.studyProgress = studyProgress;
    }
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getCourseTitle() {
		return courseTitle;
	}
	public void setCourseTitle(String courseTitle) {
		this.courseTitle = courseTitle;
	}
	public java.math.BigDecimal getFavorite() {
		return favorite;
	}
	public void setFavorite(java.math.BigDecimal favorite) {
		this.favorite = favorite;
	}
    public java.math.BigDecimal getTotalPoints() {
        return totalPoints;
    }
    public void setTotalPoints(java.math.BigDecimal totalPoints) {
        this.totalPoints = totalPoints;
    }
    public java.math.BigDecimal getStandardPoints() {
        return standardPoints;
    }
    public void setStandardPoints(java.math.BigDecimal standardPoints) {
        this.standardPoints = standardPoints;
    }
	public List<ChapterView> getChileChapter() {
		return chileChapter;
	}
	public void setChileChapter(List<ChapterView> chileChapter) {
		this.chileChapter = chileChapter;
	}
	public String getFlagStudy() {
		return flagStudy;
	}
	public void setFlagStudy(String flagStudy) {
		this.flagStudy = flagStudy;
	}
	public String getIsCourse() {
		return isCourse;
	}
	public void setIsCourse(String isCourse) {
		this.isCourse = isCourse;
	}
	public String getStudentComment() {
		return studentComment;
	}
	public void setStudentComment(String studentComment) {
		this.studentComment = studentComment;
	}
   
    
}
