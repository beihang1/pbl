package com.cloudsoaring.web.course.view;

import java.util.List;

public class PlayerData {
	private static final long serialVersionUID = 1L;
	public List<MyGameObject> gameObjects;
    public int currentModelIndex;
    public String selectedId;
    public String sceneId;
	public List<MyGameObject> getGameObjects() {
		return gameObjects;
	}
	public void setGameObjects(List<MyGameObject> gameObjects) {
		this.gameObjects = gameObjects;
	}
	public int getCurrentModelIndex() {
		return currentModelIndex;
	}
	public void setCurrentModelIndex(int currentModelIndex) {
		this.currentModelIndex = currentModelIndex;
	}
	public String getSelectedId() {
		return selectedId;
	}
	public void setSelectedId(String selectedId) {
		this.selectedId = selectedId;
	}
	public String getSceneId() {
		return sceneId;
	}
	public void setSceneId(String sceneId) {
		this.sceneId = sceneId;
	}
    
    
}
