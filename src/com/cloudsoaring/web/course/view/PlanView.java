package com.cloudsoaring.web.course.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cloudsoaring.web.common.entity.BaseEntity;
import com.cloudsoaring.web.course.entity.PlanNodeEntity;
/**
 * 
 * 计划列表
 * @author lijuan
 */
public class PlanView extends BaseEntity{
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String id;
    /**封面图片ID*/
    private String pictureId;
    //分类：1计划，2案例
    private String category;
    /**简介*/
    private String content;
    /**计划类别（内训/创业）*/
    private String type;
    /**是否必修*/
    private String flagRequired;
    /**浏览数*/
    private java.math.BigDecimal viewNum;
    /**标题*/
    private String title;
    /**学习人数*/
    private java.math.BigDecimal numStudy;
    /**该计划下所含的计划阶段*/
    private List<PlanNodeEntity> planNodeList;
    
    /**参与的用户ID*/
    private String userId;
    /**参与时间*/
    private java.util.Date recordTime;
    /**计划ID*/
    private String planId;
    /**是否参加学习*/
    private Boolean isStudy;
    /**课程数*/
    private java.math.BigDecimal numCourse;
 
    /**节点数量*/
    private String planNodeNumber; 
    /**节点状态*/
    private String planNodeName;
    /**节点简介*/
    private String planContent;
    /**节点Id*/
    private String planNodeId;
    /**课程标题*/
    private String courseTitle;
    /**课程状态*/
    private String courseStatus;
    /**课程Id*/
    private String courseId;
    /**标签*/
    List<TagView> tagViews=new ArrayList<TagView>();
    
    /**积分*/
	private java.math.BigDecimal price;
	
	/**开始学习时间*/
	private Date planStart;
	
	/**结束学习时间*/
	private Date planEnd;
	
	/**计划管理员*/
	private String manageUser;
	
	private String manageUserPersonName;
	
	/**折扣数*/
	private java.math.BigDecimal numDiscount;
	
	/**是否促销 0为是 1为否*/
	private java.math.BigDecimal flagDiscount;
	/**学习模式*/
	private String studyModel;
    /**计划关联的标签ID*/
	private String tagIds;
	
	/**计划关联的标签NAME*/
	private String tagNames;

	/**标签ID*/
	private String tagId;

	/**标签名称*/
	private String tagName;	

	/**计划总章节数--我的计划-已参与 的百分比=计划的课程中已完成章节数/计划的课程总章节数*/
	private BigDecimal allChapterNum;
	
	/**计划完成章节数--我的计划-已参与 的百分比=计划的课程中已完成章节数/计划的课程总章节数*/
	private BigDecimal finishChapterNum;
	
	/**计划学习耗时*/
	private BigDecimal timeCostSum;

	/**计划最后学习时间*/
	private Date studyDateMax;
	
	/**计划状态*/
	private String planStatus;
	
	/**按照参与时间将计划分组的list*/
	private List<PlanView> recordPlanList;
	
	/**参与时间str*/
	private String recordTimeStr;
	
	/**父类标签*/
	private String parentTagId;
	
	/**标签类型*/
	private String linkType;
	/**课程来源*/
    private String courseSource;
    /**供应商*/
    private String courseSupplier;
    
    public String getCourseSource() {
        return courseSource;
    }

    public void setCourseSource(String courseSource) {
        this.courseSource = courseSource;
    }

    public String getCourseSupplier() {
        return courseSupplier;
    }

    public void setCourseSupplier(String courseSupplier) {
        this.courseSupplier = courseSupplier;
    }
    public String getRecordTimeStr() {
		return recordTimeStr;
	}
	public void setRecordTimeStr(String recordTimeStr) {
		this.recordTimeStr = recordTimeStr;
	}
	public List<PlanView> getRecordPlanList() {
		return recordPlanList;
	}
	public void setRecordPlanList(List<PlanView> recordPlanList) {
		this.recordPlanList = recordPlanList;
	}
	public String getTagName() {
		return tagName;
	}
	public String getPlanStatus() {
		return planStatus;
	}
	public void setPlanStatus(String planStatus) {
		this.planStatus = planStatus;
	}
	public BigDecimal getTimeCostSum() {
		return timeCostSum;
	}
	public void setTimeCostSum(BigDecimal timeCostSum) {
		this.timeCostSum = timeCostSum;
	}
	public Date getStudyDateMax() {
		return studyDateMax;
	}
	public void setStudyDateMax(Date studyDateMax) {
		this.studyDateMax = studyDateMax;
	}
	public BigDecimal getAllChapterNum() {
		return allChapterNum;
	}
	public void setAllChapterNum(BigDecimal allChapterNum) {
		this.allChapterNum = allChapterNum;
	}
	public BigDecimal getFinishChapterNum() {
		return finishChapterNum;
	}
	public void setFinishChapterNum(BigDecimal finishChapterNum) {
		this.finishChapterNum = finishChapterNum;
	}
	
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	public String getTagId() {
		return tagId;
	}
	public void setTagId(String tagId) {
		this.tagId = tagId;
	}
	public java.math.BigDecimal getNumDiscount() {
		return numDiscount;
	}
	public void setNumDiscount(java.math.BigDecimal numDiscount) {
		this.numDiscount = numDiscount;
	}
	public String getTagIds() {
		return tagIds;
	}
	public void setTagIds(String tagIds) {
		this.tagIds = tagIds;
	}
	public String getTagNames() {
		return tagNames;
	}
	public void setTagNames(String tagNames) {
		this.tagNames = tagNames;
	}
	public Date getPlanStart() {
		return planStart;
	}
	public void setPlanStart(Date planStart) {
		this.planStart = planStart;
	}
	public Date getPlanEnd() {
		return planEnd;
	}
	public void setPlanEnd(Date planEnd) {
		this.planEnd = planEnd;
	}
	public String getManageUser() {
		return manageUser;
	}
	public void setManageUser(String manageUser) {
		this.manageUser = manageUser;
	}
	public java.math.BigDecimal getFlagDiscount() {
		return flagDiscount;
	}
	public void setFlagDiscount(java.math.BigDecimal flagDiscount) {
		this.flagDiscount = flagDiscount;
	}
	public String getStudyModel() {
		return studyModel;
	}
	public void setStudyModel(String studyModel) {
		this.studyModel = studyModel;
	}
	public List<TagView> getTagViews() {
        return tagViews;
    }
    public void setTagViews(List<TagView> tagViews) {
        this.tagViews = tagViews;
    }
    public java.math.BigDecimal getNumCourse() {
        return numCourse;
    }
    public void setNumCourse(java.math.BigDecimal numCourse) {
        this.numCourse = numCourse;
    }
    public Boolean getIsStudy() {
        return isStudy;
    }
    public void setIsStudy(Boolean isStudy) {
        this.isStudy = isStudy;
    }
   
    public String getPictureId() {
        return pictureId;
    }
    public void setPictureId(String pictureId) {
        this.pictureId = pictureId;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getFlagRequired() {
        return flagRequired;
    }
    public void setFlagRequired(String flagRequired) {
        this.flagRequired = flagRequired;
    }
    public java.math.BigDecimal getViewNum() {
        return viewNum;
    }
    public void setViewNum(java.math.BigDecimal viewNum) {
        this.viewNum = viewNum;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public java.math.BigDecimal getNumStudy() {
        return numStudy;
    }
    public void setNumStudy(java.math.BigDecimal numStudy) {
        this.numStudy = numStudy;
    }
    public List<PlanNodeEntity> getPlanNodeList() {
        return planNodeList;
    }
    public void setPlanNodeList(List<PlanNodeEntity> planNodeList) {
        this.planNodeList = planNodeList;
    }
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public java.util.Date getRecordTime() {
        return recordTime;
    }
    public void setRecordTime(java.util.Date recordTime) {
        this.recordTime = recordTime;
    }
    public String getPlanId() {
        return planId;
    }
    public void setPlanId(String planId) {
        this.planId = planId;
    }
	public String getPlanNodeNumber() {
		return planNodeNumber;
	}
	public void setPlanNodeNumber(String planNodeNumber) {
		this.planNodeNumber = planNodeNumber;
	}
	public String getPlanNodeName() {
		return planNodeName;
	}
	public void setPlanNodeName(String planNodeName) {
		this.planNodeName = planNodeName;
	}
	public String getPlanContent() {
		return planContent;
	}
	public void setPlanContent(String planContent) {
		this.planContent = planContent;
	}
	public String getPlanNodeId() {
		return planNodeId;
	}
	public void setPlanNodeId(String planNodeId) {
		this.planNodeId = planNodeId;
	}
	public String getCourseTitle() {
		return courseTitle;
	}
	public void setCourseTitle(String courseTitle) {
		this.courseTitle = courseTitle;
	}
	public String getCourseStatus() {
		return courseStatus;
	}
	public void setCourseStatus(String courseStatus) {
		this.courseStatus = courseStatus;
	}
	public String getCourseId() {
		return courseId;
	}
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
	public java.math.BigDecimal getPrice() {
		return price;
	}
	public void setPrice(java.math.BigDecimal price) {
		this.price = price;
	}
	public String getParentTagId() {
		return parentTagId;
	}
	public void setParentTagId(String parentTagId) {
		this.parentTagId = parentTagId;
	}
	public String getLinkType() {
		return linkType;
	}
	public void setLinkType(String linkType) {
		this.linkType = linkType;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getManageUserPersonName() {
		return manageUserPersonName;
	}
	public void setManageUserPersonName(String manageUserPersonName) {
		this.manageUserPersonName = manageUserPersonName;
	}
}
