package com.cloudsoaring.web.course.view;

import java.util.List;

public class MyGameObject {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	public String id;
    public String prefabId;
    public List<Float> position;
    public List<Float> rotation;
    public String localScale;
    public List<Float> color;
    public String imageUrl;
    public String resourceUrl;
    public String imageName;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPrefabId() {
		return prefabId;
	}
	public void setPrefabId(String prefabId) {
		this.prefabId = prefabId;
	}
	public List<Float> getPosition() {
		return position;
	}
	public void setPosition(List<Float> position) {
		this.position = position;
	}
	public List<Float> getRotation() {
		return rotation;
	}
	public void setRotation(List<Float> rotation) {
		this.rotation = rotation;
	}
	public String getLocalScale() {
		return localScale;
	}
	public void setLocalScale(String localScale) {
		this.localScale = localScale;
	}
	public List<Float> getColor() {
		return color;
	}
	public void setColor(List<Float> color) {
		this.color = color;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getResourceUrl() {
		return resourceUrl;
	}
	public void setResourceUrl(String resourceUrl) {
		this.resourceUrl = resourceUrl;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}



}
