package com.cloudsoaring.web.course.view;

import java.sql.Date;
import java.util.List;

public class UserView extends com.cloudsoaring.web.bus.entity.UserEntity{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    /**课程ID*/
    private String courseId;
    
    /**总积分*/
	private Long totalPoints; 
	
	private String userName1;
	
	private String personName1;
	
	private String departmentName1;
	
	private java.util.Date recordTime;
	/**参加计划时间*/
	private Date recordTimePlan;
	/**课程下的所有章节数*/
	private String allChapterNum;
	/**已学习完成的章节数*/
	private String finishChapterNum;
	
	private String timeCost;
	   /**课程标题*/
	private String courseTitle;
	   /**课程标题*/
    private String ssCourseTitle;
	   /**计划标题*/
	private String planTitle;
	
	private  String chapterTitle;
	   /**考试章节的分数*/
	private String scores;
	   /**用户积分*/
	private String userPoint;
	/**课程信息*/
	private Date startDate;
	private Date endDate;
	private String crowdType;
	private Date joinDate;
	private java.math.BigDecimal cNumStudy;
	 /**计划信息*/
	 /** 学习人数 */
	private java.math.BigDecimal numStudy;
	/** 开始学习时间 */
    private Date planStart;

    /** 结束学习时间 */
    private Date planEnd;
    
	private List<UserView> userCourseList;
	private List<UserView> userPlanList;
	private List<UserView> userScoreList;
	//学校id
	private String schoolId;
	//年级id
	private String gradeId;
	//班级id
	private String classId;
   
    public java.math.BigDecimal getcNumStudy() {
        return cNumStudy;
    }
    public void setcNumStudy(java.math.BigDecimal cNumStudy) {
        this.cNumStudy = cNumStudy;
    }
    public java.math.BigDecimal getNumStudy() {
        return numStudy;
    }
    public void setNumStudy(java.math.BigDecimal numStudy) {
        this.numStudy = numStudy;
    }
    public Date getPlanStart() {
        return planStart;
    }
    public void setPlanStart(Date planStart) {
        this.planStart = planStart;
    }
    public Date getPlanEnd() {
        return planEnd;
    }
    public void setPlanEnd(Date planEnd) {
        this.planEnd = planEnd;
    }
    public Date getStartDate() {
        return startDate;
    }
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    public Date getEndDate() {
        return endDate;
    }
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    public Date getJoinDate() {
        return joinDate;
    }
    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }
    public String getCrowdType() {
        return crowdType;
    }
    public void setCrowdType(String crowdType) {
        this.crowdType = crowdType;
    }
    public List<UserView> getUserCourseList() {
        return userCourseList;
    }
    public void setUserCourseList(List<UserView> userCourseList) {
        this.userCourseList = userCourseList;
    }
    public List<UserView> getUserPlanList() {
        return userPlanList;
    }
    public void setUserPlanList(List<UserView> userPlanList) {
        this.userPlanList = userPlanList;
    }
    public List<UserView> getUserScoreList() {
        return userScoreList;
    }
    public void setUserScoreList(List<UserView> userScoreList) {
        this.userScoreList = userScoreList;
    }
    public String getSsCourseTitle() {
        return ssCourseTitle;
    }
    public void setSsCourseTitle(String ssCourseTitle) {
        this.ssCourseTitle = ssCourseTitle;
    }
    public String getCourseTitle() {
        return courseTitle;
    }
    public void setCourseTitle(String courseTitle) {
        this.courseTitle = courseTitle;
    }
    public String getPlanTitle() {
        return planTitle;
    }
    public void setPlanTitle(String planTitle) {
        this.planTitle = planTitle;
    }
    public String getScores() {
        return scores;
    }
    public void setScores(String scores) {
        this.scores = scores;
    }
    public String getUserPoint() {
        return userPoint;
    }
    public void setUserPoint(String userPoint) {
        this.userPoint = userPoint;
    }
    public String getTimeCost() {
        return timeCost;
    }
    public void setTimeCost(String timeCost) {
        this.timeCost = timeCost;
    }
    public String getAllChapterNum() {
        return allChapterNum;
    }
    public void setAllChapterNum(String allChapterNum) {
        this.allChapterNum = allChapterNum;
    }
    public String getFinishChapterNum() {
        return finishChapterNum;
    }
    public void setFinishChapterNum(String finishChapterNum) {
        this.finishChapterNum = finishChapterNum;
    }
    public String getDepartmentName1() {
        return departmentName1;
    }
    public void setDepartmentName1(String departmentName1) {
        this.departmentName1 = departmentName1;
    }
    public String getUserName1() {
        return userName1;
    }
    public void setUserName1(String userName1) {
        this.userName1 = userName1;
    }
   
    public String getPersonName1() {
        return personName1;
    }
    public void setPersonName1(String personName1) {
        this.personName1 = personName1;
    }
    public Long getTotalPoints() {
		return totalPoints;
	}
	public void setTotalPoints(Long totalPoints) {
		this.totalPoints = totalPoints;
	}
	public String getCourseId() {
        return courseId;
    }
    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }
	public java.util.Date getRecordTime() {
		return recordTime;
	}
	public void setRecordTime(java.util.Date recordTime) {
		this.recordTime = recordTime;
	}
    public String getChapterTitle() {
        return chapterTitle;
    }
    public void setChapterTitle(String chapterTitle) {
        this.chapterTitle = chapterTitle;
    }
    public Date getRecordTimePlan() {
        return recordTimePlan;
    }
    public void setRecordTimePlan(Date recordTimePlan) {
        this.recordTimePlan = recordTimePlan;
    }
	public String getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
	}
	public String getGradeId() {
		return gradeId;
	}
	public void setGradeId(String gradeId) {
		this.gradeId = gradeId;
	}
	public String getClassId() {
		return classId;
	}
	public void setClassId(String classId) {
		this.classId = classId;
	} 
	
}
