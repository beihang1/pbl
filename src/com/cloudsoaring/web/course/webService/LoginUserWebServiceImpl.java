package com.cloudsoaring.web.course.webService;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.UserEntity;
import com.cloudsoaring.web.course.entity.CourseTeamEntity;
import com.cloudsoaring.web.course.entity.CourseTeamUserEntity;
import com.cloudsoaring.web.course.service.CourseService;
import com.cloudsoaring.web.trainingplatform.constants.TpDbConstants;
import com.cloudsoaring.web.trainingplatform.entity.TpUserEntity;
import com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity;
import com.cloudsoaring.web.trainingplatform.service.UserManagementService;
import net.sf.json.JSONObject;

@Service
@WebService
public class LoginUserWebServiceImpl extends WebContext implements TpDbConstants{
	@Autowired
	private UserManagementService userService;
	@Autowired
	private CourseService courseService;
	
	/**
	 * @param jsonData
	 * @return
	 * @throws Exception 
	 */
	@WebMethod
	@WebResult(name="result")
	public String checkUser(String jsonData) throws Exception {
		TpUserEntity entity = new TpUserEntity();
		JSONObject json= JSONObject.fromObject(jsonData);
		String userName = json.getString("username").toString();
		entity.setUserName(userName);
		String password = json.getString("password").toString();
		entity.setPassword(MD5(password));
		UserEntity resultEntity  = userService.checkUserByName(entity);
		if(resultEntity !=null) {
			JSONObject jsonReturn = new JSONObject();
			List<UserRoleEntity> userRoleList = userService.getRoleByUserId(resultEntity.getUserId());
			if(userRoleList.size() > 0) {
				String roleStr = "";
				for(UserRoleEntity role : userRoleList) {
					roleStr += role.getRoleId();
				}
				if(roleStr.contains("04")) {
					jsonReturn.put("userId", resultEntity.getUserId());
					jsonReturn.put("teacherFlag", true);
					return jsonReturn.toString();
				}else {
					CourseTeamEntity team = courseService.getCourseTeamByUserId(resultEntity.getUserId(), null);
					if(team != null) {
						jsonReturn.put("teamLeaderFlag", true);
						jsonReturn.put("teamId", team.getId());
					}else {
						jsonReturn.put("teamLeaderFlag", false);
					}
					CourseTeamUserEntity teamUser = courseService.getTeamUserByUserId(resultEntity.getUserId(),null);
					if(teamUser != null) {
						jsonReturn.put("userNo", teamUser.getUserNo());
						jsonReturn.put("teamId", teamUser.getTeamId());
					}else {
						jsonReturn.put("userNo", 0);
					}
					jsonReturn.put("userId", resultEntity.getUserId());
					jsonReturn.put("teacherFlag", false);
					return jsonReturn.toString();
				}
			}else {
				CourseTeamEntity team = courseService.getCourseTeamByUserId(resultEntity.getUserId(), null);
				if(team != null) {
					jsonReturn.put("teamLeaderFlag", true);
					jsonReturn.put("teamId", team.getId());
				}else {
					jsonReturn.put("teamLeaderFlag", false);
				}
				CourseTeamUserEntity teamUser = courseService.getTeamUserByUserId(resultEntity.getUserId(),null);
				if(teamUser != null) {
					jsonReturn.put("userNo", teamUser.getUserNo());
					jsonReturn.put("teamId", teamUser.getTeamId());
				}else {
					jsonReturn.put("userNo", 0);
				}
				jsonReturn.put("userId", resultEntity.getUserId());
				jsonReturn.put("teacherFlag", false);
				return jsonReturn.toString();
			}
		}else {
			 return "error";
		}
    }
	
	public static void main(String[] args) {
		 MD5("123");
	}
	 private static String MD5(String sourceStr) {
	        String result = "";
	        try {
	            MessageDigest md = MessageDigest.getInstance("MD5");
	            md.update(sourceStr.getBytes());
	            byte b[] = md.digest();
	            int i;
	            StringBuffer buf = new StringBuffer("");
	            for (int offset = 0; offset < b.length; offset++) {
	                i = b[offset];
	                if (i < 0)
	                    i += 256;
	                if (i < 16)
	                    buf.append("0");
	                buf.append(Integer.toHexString(i));
	            }
	            result = buf.toString();
	        } catch (NoSuchAlgorithmException e) {
	            System.out.println(e);
	        }
	        return result;
	    }
}
