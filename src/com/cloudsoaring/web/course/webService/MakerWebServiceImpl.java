package com.cloudsoaring.web.course.webService;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.FileEntity;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseStateConstants;
import com.cloudsoaring.web.course.controller.PropertyUtil;
import com.cloudsoaring.web.course.entity.CourseEntity;
import com.cloudsoaring.web.course.entity.CourseTeamEntity;
import com.cloudsoaring.web.course.entity.CourseTeamUserEntity;
import com.cloudsoaring.web.course.entity.ModelInfo;
import com.cloudsoaring.web.course.entity.UserLogEntity;
import com.cloudsoaring.web.course.entity.UserOperEntity;
import com.cloudsoaring.web.course.service.ChapterService;
import com.cloudsoaring.web.course.service.CourseService;
import com.cloudsoaring.web.course.service.ModelInfoService;
import com.cloudsoaring.web.course.view.ChapterUnity;
import com.cloudsoaring.web.course.view.ChapterView;
import com.cloudsoaring.web.course.view.HomeworkView;
import com.cloudsoaring.web.dropbox.entity.FileDirectoryEntity;
import com.cloudsoaring.web.dropbox.service.DropBoxService;
import com.cloudsoaring.web.trainingplatform.constants.TpDbConstants;
import com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity;
import com.cloudsoaring.web.trainingplatform.service.UserManagementService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import sun.misc.BASE64Decoder; 


@Service
@WebService
public class MakerWebServiceImpl extends WebContext implements TpDbConstants {

	@Autowired
	private UserManagementService userService;
	@Autowired
	private CourseService courseService;
	@Autowired
	private ChapterService chapterService;
	@Autowired
	private DropBoxService boxService;
	@Autowired
	private ModelInfoService modelInfoService;
	@Autowired
	private FileService fileService;

	@WebMethod
	@WebResult(name = "result")
	public String getChapterListByUser(String userId) throws Exception {
		List<ChapterUnity> chapterUnityList = new ArrayList<ChapterUnity>();
		String courseId = courseService.getCourseIdByUser(userId);
		if (!"".equals(courseId)) {
			// Todo 一个学生加入多个课程
			if (courseId.equals("more")) {
			} else {
				List<ChapterView> chapterList = chapterService.getChapterListByCourseIdUse(courseId);
				if (chapterList.size() > 0) {
					for (ChapterView chapter : chapterList) {
						ChapterUnity chapterUnity = new ChapterUnity();
						if (chapter.getChapterFlag().toString().equals(CourseStateConstants.BIG_CHAPTER)) {
							chapterUnity.setTitle(chapter.getTitle());
							for (ChapterView view : chapterList) {
								if (view.getChapterFlag().toString().equals(CourseStateConstants.SMALL_CHAPTER)
										&& view.getParentId().equals(chapter.getId())
										&& view.getTitle().equals("第二节 随堂练习")) {
									chapterUnity.setChapterId(view.getId());
								} else {
									// chapterUnity.setChapterId("");
								}
							}
							chapterUnityList.add(chapterUnity);
						}
					}
				}
			}
		} else {
			return "this user did not join the course";
		}
		JSONArray json = JSONArray.fromObject(chapterUnityList);
		return json.toString();
	}

	@WebMethod
	@WebResult(name = "result")
	public String saveUserChapterHomeWork(String jsonData1, String jsonData2, String userId, String chapterId,String type,String fileContent)
			throws Exception {
		/**
		 * 1.根据userId和chapterId判断是否有已上传的作业 
		 * 2.若没有，则保存 
		 * 3.若有，则判断作业有无老师评分
		 * 4.若有，则无法上传
		 * 5.若无，则更新作业
		 * 
		 * 6.type,1：学生；2：小组；3：教师
		 */
		String userName = "";
		if(type.equals("1") || type.equals("3")) {
			userName = courseService.getUserName(userId);
		}else if(type.equals("2")) {
			userName = courseService.getTeamName(userId);
		}
		//String parentId = chapterService.getChapterPar(chapterId);
		String chapterName = chapterService.getChapterTitle(chapterId);
		List<HomeworkView> homeworkList = courseService.getHomeListByUserIdChapterId(userId, chapterId);
		if (homeworkList.size() > 0) {
			JSONObject json = JSONObject.fromObject(jsonData1);
			JSONArray jsonArray = (JSONArray) json.get("gameObjects");
			HomeworkView homeworkView = homeworkList.get(0);
			if ((homeworkView.getScore() != null && !"".equals(homeworkView.getScore())) || 
					(homeworkView.getTeacherComment() != null && !"".equals(homeworkView.getTeacherComment()))) {
				return "grading has been completed";
			} else {
				// 更新数据
				if (jsonArray.size() == 0) {
					return "no scene infomation";
				} else {
					ModelInfo modelInfoEntity = new ModelInfo();
					modelInfoEntity.setUserId(userId);
					modelInfoEntity.setChapterId(chapterId);
					modelInfoEntity.setFlag("1");
					List<ModelInfo> list = modelInfoService.getModelInfoByUserId(modelInfoEntity);
					if (list.size() > 0) {
						modelInfoEntity.setModelId(list.get(0).getModelId());
					}
					JSONArray jsonNewArray = new JSONArray();
					for (int i = 0; i < jsonArray.size(); i++) {
						JSONObject job = jsonArray.getJSONObject(i);
						String prefabId = job.get("prefabId").toString();
						FileDirectoryEntity entity = boxService.getFileDirectoryByFileId(prefabId);
						if (entity != null) {
							job.put("imageName", entity.getFileName());
							job.put("imageUrl", getIpAndPort() + "/pblMgr/pub/course/download.action?fileId="
									+ entity.getPictureId() + "&fileType=IMAGE");
							job.put("resourceUrl", getIpAndPort() + "/pblMgr/pub/course/download.action?fileId="
									+ prefabId + "&fileType=NETDIST");
						}
						jsonNewArray.add(job);
					}
					json.put("gameObjects", jsonNewArray);
					final BASE64Decoder decoder = new BASE64Decoder();
					String modelInfo = new String(decoder.decodeBuffer(jsonData2), "UTF-8");
					modelInfoEntity.setModelInfo(modelInfo);
					modelInfoEntity.setSceneInfo(json.toString());
					modelInfoEntity.setModelName(chapterName);
					modelInfoService.updateModelInfoPC(modelInfoEntity);
					HomeworkView view = new HomeworkView();
					view.setAnswerUser(userId);
					List<HomeworkView> homeList = courseService.getHomeWorkByChapterId(chapterId);
					if (homeList.size() > 0) {
						view.setHomeworkId(homeList.get(0).getId());
					}
					view.setLinkId(chapterId);
					view.setAudioType(type);
					FileEntity fileEntity = new FileEntity();
					fileEntity.setContentType("dat");
					fileEntity.setCreateUser(userId);
					fileEntity.setCreateDate(new Date());
					fileEntity.setFileId(IDGenerator.genUID());
					fileEntity.setFileName(userName + "_" + chapterName + "_作业");
					//File file = getFile(modelInfo + json.toString(), fileEntity.getFileName().trim() + ".dat");
					File file = getFile(fileContent, fileEntity.getFileName().trim() + ".dat");
					String path = file.getPath().substring(file.getPath().indexOf("homework"), file.getPath().length())
							.replace("\\", "/");
					fileEntity.setFileSize(file.length());
					fileEntity.setPath(path);
					fileEntity.setType("homework");
					fileService.save(fileEntity);
					view.setFileId(fileEntity.getFileId());
					ResultBean result = courseService.saveSecen(view);
					if (result.isStatus()) {
						return "success";
					}
				}
			}
		} else {
			JSONObject json = JSONObject.fromObject(jsonData1);
			JSONArray jsonArray = (JSONArray) json.get("gameObjects");
			// 新增数据
			if (jsonArray.size() == 0) {
				return "no scene infomation";
			} else {
				ModelInfo modelInfoEntity = new ModelInfo();
				JSONArray jsonNewArray = new JSONArray();
				for (int i = 0; i < jsonArray.size(); i++) {
					JSONObject job = jsonArray.getJSONObject(i);
					String prefabId = job.get("prefabId").toString();
					FileDirectoryEntity entity = boxService.getFileDirectoryByFileId(prefabId);
					if (entity != null) {
						job.put("imageName", entity.getFileName());
						job.put("imageUrl", getIpAndPort() + "/pblMgr/pub/course/download.action?fileId="
								+ entity.getPictureId() + "&fileType=IMAGE");
						job.put("resourceUrl", getIpAndPort() + "/pblMgr/pub/course/download.action?fileId="
								+ prefabId + "&fileType=NETDIST");
					}
					jsonNewArray.add(job);
				}
				json.put("gameObjects", jsonNewArray);
				final BASE64Decoder decoder = new BASE64Decoder();
				String modelInfo = new String(decoder.decodeBuffer(jsonData2), "UTF-8");
				modelInfoEntity.setModelInfo(modelInfo);
				modelInfoEntity.setChapterId(chapterId);
				modelInfoEntity.setSceneInfo(json.toString());
				modelInfoEntity.setUserId(userId);
				modelInfoEntity.setModelName(chapterName);
				modelInfoEntity.setFlag("1");
				modelInfoEntity.setCreateUser(userId);
				String id = modelInfoService.insertModelInfo(modelInfoEntity);
				HomeworkView view = new HomeworkView();
				view.setAnswerUser(userId);
				List<HomeworkView> list = courseService.getHomeWorkByChapterId(chapterId);
				if (list.size() > 0) {
					view.setHomeworkId(list.get(0).getId());
				}
				view.setLinkId(chapterId);
				view.setAudioType(type);
				view.setStatus(HOMEWORK_ANSWER_STATUS_ONE);
				FileEntity fileEntity = new FileEntity();
				fileEntity.setContentType("dat");
				fileEntity.setCreateUser(userId);
				fileEntity.setCreateDate(new Date());
				fileEntity.setFileId(IDGenerator.genUID());
				fileEntity.setFileName(userName + "_" + chapterName + "_作业");
				File file = getFile(modelInfo + json.toString(), fileEntity.getFileName().trim() + ".dat");
				String path = file.getPath().substring(file.getPath().indexOf("homework"), file.getPath().length())
						.replace("\\", "/");
				fileEntity.setFileSize(file.length());
				fileEntity.setPath(path);
				fileEntity.setType("homework");
				fileService.save(fileEntity);
				view.setFileId(fileEntity.getFileId());
				ResultBean result = courseService.saveSecen(view);
				if (result.isStatus()) {
					return "success";
				}
			}
		}
		return "success";
	}

	@WebMethod
	@WebResult(name = "result")
	public String saveUserCoursePractice(String jsonData1, String jsonData2,String userId,String courseId) throws UnsupportedEncodingException, IOException {
		JSONObject json = JSONObject.fromObject(jsonData1);
		JSONArray jsonArray = (JSONArray) json.get("gameObjects");
		// 新增数据
		if (jsonArray.size() == 0) {
			return "no scene infomation";
		} else {
			ModelInfo modelInfoEntity = new ModelInfo();
			JSONArray jsonNewArray = new JSONArray();
			for (int i = 0; i < jsonArray.size(); i++) {
				JSONObject job = jsonArray.getJSONObject(i);
				String prefabId = job.get("prefabId").toString();
				FileDirectoryEntity entity = boxService.getFileDirectoryByFileId(prefabId);
				if (entity != null) {
					job.put("imageName", entity.getFileName());
					job.put("imageUrl", getIpAndPort() + "/pblMgr/pub/course/download.action?fileId="
							+ entity.getPictureId() + "&fileType=IMAGE");
					job.put("resourceUrl", getIpAndPort() + "/pblMgr/pub/course/download.action?fileId="
							+ prefabId + "&fileType=NETDIST");
				}
				jsonNewArray.add(job);
			}
			json.put("gameObjects", jsonNewArray);
			final BASE64Decoder decoder = new BASE64Decoder();
			String modelInfo = new String(decoder.decodeBuffer(jsonData2), "UTF-8");
			modelInfoEntity.setModelInfo(modelInfo);
			modelInfoEntity.setChapterId(courseId);
			modelInfoEntity.setSceneInfo(json.toString());
			modelInfoEntity.setUserId(userId);
			//modelInfoEntity.setModelName(chapterName);
			modelInfoEntity.setFlag("0");
			modelInfoService.insertModelInfo(modelInfoEntity);
			return "success";
		}
	}
	
	@WebMethod
	@WebResult(name = "result")
	public String getWorkSpaceModel(String userId, String chapterId) {
		JSONObject jo = new JSONObject();
		ModelInfo modelInfoEntity = new ModelInfo();
		modelInfoEntity.setUserId(userId);
		modelInfoEntity.setChapterId(chapterId);
		modelInfoEntity.setFlag("1");
		List<ModelInfo> list = modelInfoService.getModelInfoByUserId(modelInfoEntity);
		if (list.size() > 0) {
			ModelInfo modelInfo = list.get(0);
			jo.put("modelInfo", modelInfo.getModelInfo());
			jo.put("sceneInfo", modelInfo.getSceneInfo());
			return jo.toString();
		} else {
			return "no homework";
		}
	}

	/**
	 * 根据课程ID和章节ID获取已提交作业的学生信息
	 * 
	 * @param courseId
	 * @param chapterId
	 * @return String
	 * type,1：学生；2：小组；3：教师
	 */
	@WebMethod
	@WebResult(name = "result")
	public String getHomeWorkUserById(String courseId, String chapterId,String teamId,String type) {
		List<HomeworkView> homeworkList = null;
		JSONObject jo = new JSONObject();
		if(chapterId == null || "".equals(chapterId) ) {
			return "no homework";
		}else {
			if(teamId != null && !"".equals(teamId)) {
				homeworkList = courseService.gethomeworkById(courseId, chapterId,teamId,type);
				List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
				if (homeworkList.size() > 0) {
					SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					for (HomeworkView view : homeworkList) {
						Map<String, Object> courseERMap = new HashMap<String, Object>();
						courseERMap.put("userName", view.getCreateUser());
						courseERMap.put("userId", view.getAnswerUser());
						courseERMap.put("createTime", formatter.format(view.getCreateDate()));
						courseERMap.put("homeworkId", view.getId());
						if(view.getScore() != null && !"".equals(view.getScore())) {
							courseERMap.put("scoreFlag", true);
						}else {
							courseERMap.put("scoreFlag", false);
						}
						resultList.add(courseERMap);
					}
					jo.put("resultList", resultList);
					return jo.toString();
				} else {
					return "no students";
				}
			}
			List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
			homeworkList = courseService.gethomeworkById(courseId, chapterId,type);
			if (homeworkList.size() > 0) {
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				for (HomeworkView view : homeworkList) {
					Map<String, Object> courseERMap = new HashMap<String, Object>();
					courseERMap.put("userName", view.getCreateUser());
					courseERMap.put("userId", view.getAnswerUser());
					courseERMap.put("createTime", formatter.format(view.getCreateDate()));
					courseERMap.put("homeworkId", view.getId());
					if(view.getScore() != null && !"".equals(view.getScore())) {
						courseERMap.put("scoreFlag", true);
					}else {
						courseERMap.put("scoreFlag", false);
					}
					resultList.add(courseERMap);
				}
				jo.put("resultList", resultList);
				return jo.toString();
			} else {
				return "no students";
			}
		}
	}

	/**
	 * @param userId
	 * @param chapterId
	 * @return
	 *  type,1：学生；2：小组；3：教师
	 */
	@WebMethod
	@WebResult(name = "result")
	//public String getHomeWorkUserByUCId(String userId, String chapterId,String type) 
	public String getHomeWorkUserByUCId(String userId, String chapterId) {
		if(chapterId == null || "".equals(chapterId)) {
			return "no homework";
		}else {
			JSONObject jo = new JSONObject();
			List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
			List<HomeworkView> homeworkList = courseService.gethomeworkByUCId(userId, chapterId);
			if (homeworkList.size() > 0) {
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				for (HomeworkView view : homeworkList) {
					Map<String, Object> courseERMap = new HashMap<String, Object>();
					courseERMap.put("userName", view.getCreateUser());
					courseERMap.put("userId", view.getAnswerUser());
					courseERMap.put("createTime", formatter.format(view.getCreateDate()));
					courseERMap.put("homeworkId", view.getId());
					if(view.getScore() != null && !"".equals(view.getScore())) {
						courseERMap.put("scoreFlag", true);
					}else {
						courseERMap.put("scoreFlag", false);
					}
					resultList.add(courseERMap);
				}
				jo.put("resultList", resultList);
				return jo.toString();
			} else {
				return "no students";
			}
		}
	}
	
	@WebMethod
	@WebResult(name = "result")
	public String getUserPracticesNo(String courseId,String userId) {
		JSONObject jo = new JSONObject();
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		ModelInfo modelInfo = new ModelInfo();
		modelInfo.setChapterId(courseId);
		modelInfo.setUserId(userId);
		modelInfo.setFlag("0");
		List<ModelInfo> list = modelInfoService.getUserPracticeList(modelInfo);
		if(list.size() > 0) {
			if(list.size() > 3) {
				list = list.subList(0, 3);
				for(ModelInfo model : list) {
					Map<String, Object> courseERMap = new HashMap<String, Object>();
					courseERMap.put("practiceId", model.getModelId());
					resultList.add(courseERMap);
				}
				jo.put("resultList", resultList);
				return jo.toString();
			}else {
				for(ModelInfo model : list) {
					Map<String, Object> courseERMap = new HashMap<String, Object>();
					courseERMap.put("practiceId", model.getModelId());
					resultList.add(courseERMap);
				}
				jo.put("resultList", resultList);
				return jo.toString();
			}
		}else {
			return "no practices";
		}
	}
	
	@WebMethod
	@WebResult(name = "result")
	public String getModelInfoById(Integer No) {
		JSONObject jo = new JSONObject();
		ModelInfo info = modelInfoService.getModelInfo(No);
		if(info != null) {
			jo.put("modelInfo", info.getModelInfo());
			jo.put("sceneInfo", info.getSceneInfo());
			return jo.toString();
		}else {
			return "no model";
		}
	}
	/**
	 * 根据homeworkId保存教师评分和评语
	 * 
	 * @param homeworkId
	 * @param score
	 * @param teacherComment
	 * @return String
	 */
	@WebMethod
	@WebResult(name = "result")
	public String saveScoreById(String homeworkId, Integer score, String teacherComment) {
		HomeworkView home = new HomeworkView();
		home.setId(homeworkId);
		home.setScore(score.toString());
		home.setTeacherComment(teacherComment);
		return courseService.saveScoreById(home);
	}

	/**
	 * 获取教师对应的课程列表
	 * @param userId
	 * @return String
	 */
	@WebMethod
	@WebResult(name = "result")
	public String getCourseListByUserId(String userId) {
		JSONObject jo = new JSONObject();
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		//List<CourseEntity> courseList = courseService.getCourseListByUserId(userId);
		List<CourseEntity> courseList = courseService.getCourseListByStudentId(userId);
		if (courseList.size() > 0) {
			for (CourseEntity entity : courseList) {
				Map<String, Object> courseERMap = new HashMap<String, Object>();
				courseERMap.put("courseId", entity.getId());
				courseERMap.put("courseName", entity.getTitle());
				resultList.add(courseERMap);
			}
			jo.put("resultList", resultList);
			return jo.toString();
		} else {
			return "no course";
		}
	}
	
	
	@WebMethod
	@WebResult(name = "result")
	public String getCourseListByStudentId(String userId) {
		JSONObject jo = new JSONObject();
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		List<CourseEntity> courseList = courseService.getCourseListByStudentId(userId);
		if (courseList.size() > 0) {
			for (CourseEntity entity : courseList) {
				Map<String, Object> courseERMap = new HashMap<String, Object>();
				courseERMap.put("courseId", entity.getId());
				courseERMap.put("courseName", entity.getTitle());
				resultList.add(courseERMap);
			}
			jo.put("resultList", resultList);
			return jo.toString();
		} else {
			return "no course";
		}
	}
	/**
	 * 根据课程ID来获取章节列表信息
	 * @param courseId
	 * @return
	 */
	@WebMethod
	@WebResult(name = "result")
	public String getChapterListByCourseId(String courseId) {
		JSONObject jo = new JSONObject();
		List<ChapterUnity> chapterUnityList = new ArrayList<ChapterUnity>();
		List<ChapterView> chapterList = chapterService.getChapterListByCourseIdUse(courseId);
		if (chapterList.size() > 0) {
			for (ChapterView chapter : chapterList) {
				ChapterUnity chapterUnity = new ChapterUnity();
				if (chapter.getChapterFlag().toString().equals(CourseStateConstants.BIG_CHAPTER)) {
					chapterUnity.setTitle(chapter.getTitle());
					chapterUnity.setChapterId(chapter.getId());
					/*for (ChapterView view : chapterList) {
						if (view.getChapterFlag().toString().equals(CourseStateConstants.SMALL_CHAPTER)
								&& view.getParentId().equals(chapter.getId())
								&& view.getTitle().equals("第二节 随堂练习")) {
							chapterUnity.setChapterId(view.getId());
						}
					}*/
					chapterUnityList.add(chapterUnity);
				}
			}
			jo.put("resultList", chapterUnityList);
			return jo.toString();
		}else {
			return "no chapter";
		}
	}

	
	@WebMethod
	@WebResult(name = "result")
	public String getTeamList(String courseId) {
		JSONObject jo = new JSONObject();
		List<CourseTeamEntity> teamList = courseService.getCourseTeamListByCourseId(courseId);
		if(teamList.size() > 0) {
			jo.put("resultList", teamList);
			return jo.toString();
		}else {
			return "no course team";
		}
	}
	
	@WebMethod
	@WebResult(name = "result")
	public void saveUserOperationLog(String userId,String browserInfo,String operDetail,String operSite,String IpInfo) {
		UserOperEntity entity = new UserOperEntity();
		entity.setUserId(userId);
		if(userId != null) {
			entity.setOperUser(courseService.getUserName(userId));
		}
		entity.setSysInfo(browserInfo);
		entity.setOperDetail(operDetail);
		entity.setOperSite(operSite);
		entity.setOperTime(new Date());
		entity.setOperIP(IpInfo);
		courseService.inserUserOperPC(entity);
	}
	
	@WebMethod
	@WebResult(name = "result")
	public void saveUserLogin(String userId,String IpInfo) {
		UserLogEntity log = new UserLogEntity();
		log.setOperIP(IpInfo);
		log.setLogType("登录");
		log.setUserId(userId);
		if(userId != null) {
			log.setUserName(courseService.getUserName(userId));
		}
		log.setOperTime(new Date());
		courseService.insertUserLogPC(log);
	}
	
	@WebMethod
	@WebResult(name="result")
	public String getUserRole(String userId,String courseId) throws Exception {
		JSONObject jsonReturn = new JSONObject();
		List<UserRoleEntity> userRoleList = userService.getRoleByUserId(userId);
		if(userRoleList.size() > 0) {
			String roleStr = "";
			for(UserRoleEntity role : userRoleList) {
				roleStr += role.getRoleId();
			}
			if(roleStr.contains("04")) {
				jsonReturn.put("userId", userId);
				jsonReturn.put("teacherFlag", true);
				return jsonReturn.toString();
			}else {
				CourseTeamEntity team = courseService.getCourseTeamByUserId(userId,courseId);
				if(team != null) {
					jsonReturn.put("teamLeaderFlag", true);
					jsonReturn.put("teamId", team.getId());
				}else {
					jsonReturn.put("teamLeaderFlag", false);
				}
				CourseTeamUserEntity teamUser = courseService.getTeamUserByUserId(userId,courseId);
				if(teamUser != null) {
					jsonReturn.put("userNo", teamUser.getUserNo());
					jsonReturn.put("teamId", teamUser.getTeamId());
				}else {
					jsonReturn.put("userNo", 0);
				}
				jsonReturn.put("userId", userId);
				jsonReturn.put("teacherFlag", false);
				return jsonReturn.toString();
			}
		}else {
			CourseTeamEntity team = courseService.getCourseTeamByUserId(userId,courseId);
			if(team != null) {
				jsonReturn.put("teamLeaderFlag", true);
				jsonReturn.put("teamId", team.getId());
			}else {
				jsonReturn.put("teamLeaderFlag", false);
			}
			CourseTeamUserEntity teamUser = courseService.getTeamUserByUserId(userId,courseId);
			if(teamUser != null) {
				jsonReturn.put("userNo", teamUser.getUserNo());
				jsonReturn.put("teamId", teamUser.getTeamId());
			}else {
				jsonReturn.put("userNo", 0);
			}
			jsonReturn.put("userId", userId);
			jsonReturn.put("teacherFlag", false);
			return jsonReturn.toString();
		}
    }
	
	private String getIpAndPort() {
		return PropertyUtil.getProperty("unity.context.root");
	}

	private static String MD5(String sourceStr) {
        String result = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(sourceStr.getBytes());
            byte b[] = md.digest();
            int i;
            StringBuffer buf = new StringBuffer("");
            for (int offset = 0; offset < b.length; offset++) {
                i = b[offset];
                if (i < 0)
                    i += 256;
                if (i < 16)
                    buf.append("0");
                buf.append(Integer.toHexString(i));
            }
            result = buf.toString();
        } catch (NoSuchAlgorithmException e) {
            System.out.println(e);
        }
        return result;
    }
	
	private static File getFile(String modelInfo, String fileName) {
		PrintWriter pfp = null;
		File file = null;
		String path = PropertyUtil.getProperty("res.storage.homework");
		try {
			File dir = new File(path);
			if (!dir.exists() && dir.isDirectory()) {// 判断文件目录是否存在
				dir.mkdirs();
			}
			file = new File(path + "\\" + fileName);
			pfp = new PrintWriter(file);
			pfp.print(modelInfo);
			pfp.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (pfp != null) {
				try {
					pfp.close();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}
		return file;
	}
}
