package com.cloudsoaring.web.course.webService;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.course.controller.PropertyUtil;
import com.cloudsoaring.web.course.entity.ModelInfo;
import com.cloudsoaring.web.course.entity.TagLinkEntity;
import com.cloudsoaring.web.course.service.CourseService;
import com.cloudsoaring.web.course.service.ModelInfoService;
import com.cloudsoaring.web.course.view.TagUnity;
import com.cloudsoaring.web.course.view.TagView;
import com.cloudsoaring.web.dropbox.entity.FileDirectoryEntity;
import com.cloudsoaring.web.dropbox.service.DropBoxService;
import com.cloudsoaring.web.trainingplatform.constants.TpDbConstants;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 *在线编辑器获取标签
 *2019-04-11
 *@author fanglili
 *@version 1.0
 */
@Service
@WebService
public class TagWebServiceImpl extends WebContext implements TpDbConstants{
	
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	 @Autowired
	 private CourseService courseService;
	 @Autowired
	 private DropBoxService boxService;
	 @Autowired
	 private ModelInfoService modelInfoService;
	
	/**
	 * 获取资源一级标签
	 * @return List<TagUnity>
	 * @throws Exception 
	 */
	@WebMethod
	@WebResult(name="result")
	public String getFirstTagList() throws Exception {
		List<TagUnity> tagUnityList = new ArrayList<TagUnity>();
		TagView tagQuery = new TagView();
		List<TagView> tagsList = courseService.searchTagList(tagQuery);
		if(tagsList.size() > 0) {
			for(TagView view : tagsList) {
				TagUnity tag = new TagUnity();
				tag.setTagId(view.getTagId());
				tag.setTagName(view.getTagName());
				tag.setTagTypeId(view.getTagTypeId());
				tagUnityList.add(tag);
			}
		}else {
			return "error";
		}
		 JSONArray json = JSONArray.fromObject(tagUnityList);   
		return json.toString();
    }
	
	/**
	 * 获取资源一级标签对应的二级标签
	 * @return List<TagUnity>
	 * @throws Exception 
	 */
	@WebMethod
	@WebResult(name="result")
	public String getSecondTagListByParentId(String id) throws Exception {
		List<TagUnity> tagUnityList = new ArrayList<TagUnity>();
		List<TagView> tagsList = courseService.searchSecondTagList(id);
		if(tagsList.size() > 0) {
			for(TagView view : tagsList) {
				TagUnity tag = new TagUnity();
				tag.setTagId(view.getTagId());
				tag.setTagName(view.getTagName());
				tag.setParentId(view.getParentId());
				tagUnityList.add(tag);
			}
		}else {
			return "error";
		}
		JSONArray json = JSONArray.fromObject(tagUnityList); 
		return json.toString();
    }
	
	/**
	 * 根据标签获取资源列表
	 * @return List<TagUnity>
	 * @throws Exception 
	 */
	@WebMethod
	@WebResult(name="result")
	public String getResourceListByTags(String firstTagId, String secondTagId) throws Exception {
		List<TagUnity> tagUnityList = new ArrayList<TagUnity>();
		List<FileDirectoryEntity> fileDirectoryList = boxService.getResourceListByTags(firstTagId, secondTagId);
		if(fileDirectoryList.size() > 0) {
			for(FileDirectoryEntity entity : fileDirectoryList) {
				TagUnity tag = new TagUnity();
				// FileEntity file  = fileService.getFile(entity.getPictureId() ,"image");
				// String url = PropertyUtil.getProperty("res.context.root");
				// tag.setImageId(entity.getFileId());
				// tag.setImageUrl(url+file.getPath());
				//tag.setImageUrl(getIpAndPort()+"/pblMgr/pub/course/download.action?fileId="+entity.getPictureId()+"&fileType=IMAGE");
				// tag.setImageName(entity.getFileName());
				tag.setResourceId(entity.getId());
				tag.setImageId(entity.getPictureId());
				tag.setFileId(entity.getFileId());
				tag.setFileName(entity.getFileName());
				tag.setBriefInTroduction(entity.getBriefInTroduction() == null ? "--":entity.getBriefInTroduction());
				tag.setImageUrl(getIpAndPort()+"/pblMgr/pub/course/download.action?fileId="+entity.getPictureId()+"&fileType=IMAGE");
				tag.setFileUrl(getIpAndPort()+"/pblMgr/pub/course/download.action?fileId="+entity.getFileId()+"&fileType=NETDIST");
				 tagUnityList.add(tag);
			}
		}else {
			return "error";
		}
		JSONArray json = JSONArray.fromObject(tagUnityList); 
		return json.toString();
    }
	
	/**
	 *获取所有资源列表
	 * @return List<TagUnity>
	 * @throws Exception 
	 */
	@WebMethod
	@WebResult(name="result")
	public String getAllResourceList() throws Exception {
		List<TagUnity> tagUnityList = new ArrayList<TagUnity>();
		List<FileDirectoryEntity> fileDirectoryList = boxService.getResourceListByTags(null, null);
		if(fileDirectoryList.size() > 0) {
			for(FileDirectoryEntity entity : fileDirectoryList) {
				TagUnity tag = new TagUnity();
				//tag.setImageId(entity.getFileId());
				//tag.setImageId(entity.getId());
				tag.setResourceId(entity.getId());
				tag.setImageId(entity.getPictureId());
				tag.setFileId(entity.getFileId());
				//tag.setImageName(entity.getFileName());
				tag.setFileName(entity.getFileName());
				tag.setBriefInTroduction(entity.getBriefInTroduction() == null ? "--":entity.getBriefInTroduction());
				tag.setImageUrl(getIpAndPort()+"/pblMgr/pub/course/download.action?fileId="+entity.getPictureId()+"&fileType=IMAGE");
				tag.setFileUrl(getIpAndPort()+"/pblMgr/pub/course/download.action?fileId="+entity.getFileId()+"&fileType=NETDIST");
				tagUnityList.add(tag);
			}
		}else {
			return "error";
		}
		JSONArray json = JSONArray.fromObject(tagUnityList); 
		return json.toString();
    }
	
	@WebMethod
	@WebResult(name="result")
	public String getResourceListByDate(String startTime) throws Exception {
		Date startDate = sdf.parse(startTime);
		Date endDate = new Date();
		List<TagUnity> tagUnityList = new ArrayList<TagUnity>();
		List<FileDirectoryEntity> fileDirectoryList = boxService.getResourceListByDate(startDate, endDate);
		if(fileDirectoryList.size() > 0) {
			for(FileDirectoryEntity entity : fileDirectoryList) {
				TagUnity tag = new TagUnity();
				tag.setResourceId(entity.getId());
				tag.setImageId(entity.getPictureId());
				tag.setFileId(entity.getFileId());
				tag.setFileName(entity.getFileName());
				tag.setBriefInTroduction(entity.getBriefInTroduction() == null ? "--":entity.getBriefInTroduction());
				tag.setImageUrl(getIpAndPort()+"/pblMgr/pub/course/download.action?fileId="+entity.getPictureId()+"&fileType=IMAGE");
				tag.setFileUrl(getIpAndPort()+"/pblMgr/pub/course/download.action?fileId="+entity.getFileId()+"&fileType=NETDIST");
				tagUnityList.add(tag);
			}
		}else {
			return "error";
		}
		JSONArray json = JSONArray.fromObject(tagUnityList); 
		return json.toString();
    }
	
	@WebMethod
	@WebResult(name="result")
	public String getTagListByResourceId(String resourceId) {
		List<TagUnity> tagUnityList = new ArrayList<TagUnity>();
		List<TagLinkEntity> tagLinkList = boxService.getTagListByResourceId(resourceId);
		if(tagLinkList.size() >0) {
			for(TagLinkEntity entity : tagLinkList) {
				TagUnity tag = new TagUnity();
				tag.setTagId(entity.getTagId());
				tag.setResourceId(entity.getLinkId());
				tag.setParentId(entity.getParentTagId());
				tag.setLinkType(entity.getLinkType());
				tag.setTagName(entity.getTagName());
				tagUnityList.add(tag);
			}
		}
		JSONArray json = JSONArray.fromObject(tagUnityList); 
		return json.toString();
	}
	
	/**
	 * 根据fileId获取资源详情信息
	 * @return List<TagUnity>
	 * @throws Exception 
	 */
	@WebMethod
	@WebResult(name="result")
	public String getResourceDetailByFileId(String fileId) throws Exception {
		if(fileId != null) {
			return getIpAndPort()+"/pblMgr/pub/course/download.action?fileId="+fileId+"&fileType=NETDIST";
		}else {
			return "error";
		}
//		FileEntity file  = fileService.getFile(fileId ,"NETDIST");
//		//String url = PropertyUtil.getProperty("res.context.root");
//		if(file != null) {
//			return url+file.getPath();
//		}else {
//			return "error";
//		}
//		
    }
	
	private String getIpAndPort() {
		return PropertyUtil.getProperty("unity.context.root");
	}
	/**
	 * 保存unity的场景
	 * @param playData
	 * @return
	 */
	@WebMethod
	@WebResult(name="result")
	public String saveScene(String playData,String modelId,String modelName) {
		//List<SceneInfo> list = new ArrayList<SceneInfo>();
		JSONObject json = JSONObject.fromObject(playData);
		JSONArray jsonArray = (JSONArray) json.get("gameObjects");
		JSONArray jsonNewArray = new JSONArray();
		//String currentModelIndex = json.getString("currentModelIndex").toString();
		//String selectedId = json.getString("selectedId").toString();
		//String sceneId = json.getString("sceneId").toString();
		if(jsonArray.size() == 0) {
			ModelInfo modelInfo = new ModelInfo();
			if(modelId != null && !"".equals(modelId)) {
				modelInfo.setModelId(modelId);
				modelInfo.setSceneInfo(playData);
				modelInfoService.updateSceneInfo(modelInfo);
			}
			return "success";
		}else {
			for(int i=0;i<jsonArray.size();i++){
			    JSONObject job = jsonArray.getJSONObject(i);
			    //String id = job.get("id").toString();
			    String prefabId = job.get("prefabId").toString();
			    FileDirectoryEntity entity = boxService.getFileDirectoryByFileId(prefabId);
			    if(entity != null) {
			    	job.put("imageName", entity.getFileName());
			    	job.put("imageUrl", getIpAndPort()+"/pblMgr/pub/course/download.action?fileId="+entity.getPictureId()+"&fileType=IMAGE");
			    	job.put("resourceUrl", getIpAndPort()+"/pblMgr/pub/course/download.action?fileId="+prefabId+"&fileType=NETDIST");
				}
			    jsonNewArray.add(job);
//			    String position = job.get("position").toString();
//			    String rotation = job.get("rotation").toString();
//			    String localScale = job.get("localScale").toString();
//			    String color = job.get("color").toString();
//			    if(position != null && !"".equals(position)) {
//			    	obj.setPosition(position);
//			    }
//			    if(rotation != null && !"".equals(rotation)) {
//			    	obj.setRotation(rotation);
//			    }
//			    if(localScale != null && !"".equals(localScale)) {
//			    	scene.setLocalScale(localScale);
//			    }
//			    if(color != null && !"".equals(color)) {
//			    	 scene.setColor(color);
//			    }
//			    SceneInfo scene = new SceneInfo();
//			    scene.setId(id);
//			    scene.setPrefabId(prefabId);

//			    list.add(scene);
			  }
			json.put("gameObjects", jsonNewArray);
			/**
			 *  根据modelId判断是修改还是新增
			 * 新增返回modelId
			 */
			ModelInfo modelInfo = new ModelInfo();
			if(modelId != null && !"".equals(modelId)) {
				modelInfo.setModelId(modelId);
				modelInfo.setSceneInfo(json.toString());
				modelInfoService.updateSceneInfo(modelInfo);
			}
			return "success";
		}
//		if(list.size() > 0) {
//			sceneInfoService.deleteSceneInfo();
//			for(SceneInfo info : list) {
//				info.setCurrentModelIndex(currentModelIndex);
//				info.setSelectedId(selectedId);
//				info.setSceneId(sceneId);
//				//获取资源path
////				FileEntity file  = fileService.getFile(info.getPrefabId() ,"NETDIST");
////				String url = PropertyUtil.getProperty("res.context.root");
////				if(file != null) {
////					info.setResourceUrl(url+file.getPath());
////				}
//				//获取图片path和资源名称
//				FileDirectoryEntity entity = boxService.getFileDirectoryByFileId(info.getPrefabId());
//				if(entity != null) {
//					info.setImageName(entity.getFileName());
//					//FileEntity fileImg  = fileService.getFile(entity.getPictureId() ,"image");
//					//info.setImageUrl(url+fileImg.getPath());
//					info.setImageUrl(getIpAndPort()+"/pblMgr/pub/course/download.action?fileId="+entity.getPictureId()+"&fileType=IMAGE");
//					info.setResourceUrl(getIpAndPort()+"/pblMgr/pub/course/download.action?fileId="+info.getPrefabId()+"&fileType=NETDIST");
//				}
//				sceneInfoService.insertSceneInfo(info);
//			}
//			return "success";
//		}else {
//			return "error";	
//		}
	}
	
	/**
	 * 获取unity场景信息
	 * @return String
	 */
	@WebMethod
	@WebResult(name="result")
	public String getScene(String modelId) {
		ModelInfo modelInfo =  modelInfoService.getModelInfo(modelId);
		if(modelInfo != null) {
			JSONObject json = new JSONObject();
			json.put("modelId", modelInfo.getModelId());
			json.put("sceneInfo", modelInfo.getSceneInfo());
			return json.toString();
		}else {
			return "error";
		}
//		PlayerData playerData = new PlayerData();
//		int currentModelIndex  = -1;
//		String selectedId = "";
//		String sceneId = "";
//		List<MyGameObject> gameObjects = new ArrayList<MyGameObject>();
//		List<SceneInfo> list = sceneInfoService.getSceneInfoList();
//		if(list.size() > 0) {
//			for(SceneInfo info : list) {
//				MyGameObject obj = new MyGameObject();
//				obj.setId(info.getId());
//				obj.setPrefabId(info.getPrefabId());
//				String position = info.getPosition();
//				if(position !=null && !"".equals(position)) {
//					List<Float> listFloat = strToList(position);
//					obj.setPosition(listFloat);
//				}
//				String rotation = info.getRotation();
//				if(rotation !=null && !"".equals(rotation)) {
//					List<Float> listFloat = strToList(rotation);
//					obj.setRotation(listFloat);
//				}
//				obj.setLocalScale(info.getLocalScale() == null ? "" :info.getLocalScale());
//				String color = info.getColor();
//				if(color !=null && !"".equals(color) && !"[]".equals(color)) {
//					List<Float> listFloat = strToList(color);
//					obj.setColor(listFloat);
//				}
//				obj.setImageUrl(info.getImageUrl() == null ? "" :info.getImageUrl());
//				obj.setResourceUrl(info.getResourceUrl() == null ? "" :info.getResourceUrl());
//				obj.setImageName(info.getImageName() == null ? "" :info.getImageName());
//				gameObjects.add(obj);
//				if(info.getCurrentModelIndex() !=null) {
//					currentModelIndex = Integer.parseInt(info.getCurrentModelIndex());
//				}else {
//					
//				}
//				selectedId = info.getSelectedId();
//				sceneId = info.getSceneId();
//			}
//			
//			playerData.setCurrentModelIndex(currentModelIndex);
//			playerData.setSelectedId(selectedId == null? "":selectedId);
//			playerData.setSceneId(sceneId == null? "":sceneId);
//			playerData.setGameObjects(gameObjects);
//			JSONObject json = JSONObject.fromObject(playerData);
//			return json.toString();
//		}else {
//			return "error";
//		}
	}
	
	/**
	 * 保存unity动画信息
	 * @param savingInfos
	 * @return String
	 */
	@WebMethod
	@WebResult(name="result")
	public String saveWorkspaceModel( String modelData) {
		JSONObject json = JSONObject.fromObject(modelData);
		if(json.size() > 0) {
			String modelInfoDetail = json.get("modelInfo").toString();
			String  modelId = json.get("modelId").toString();
			String modelName = json.get("modelName").toString();
			ModelInfo modelInfo = new ModelInfo();
			modelInfo.setModelInfo(modelInfoDetail);
			modelInfo.setModelName(modelName);
			/**
			 *  根据modelId判断是修改还是新增
			 * 新增返回modelId
			 */
			if(modelId != null && !"".equals(modelId)) {
				modelInfo.setModelId(modelId);
				modelInfoService.updateModelInfo(modelInfo);
			}else {
				return modelInfoService.insertModelInfo(modelInfo);
			}
			return "success";
		}else{
			return "error";
		}
		
	}
	
	/**
	 * 获取unity动画信息
	 * @return String
	 */
	@WebMethod
	@WebResult(name="result")
	public String getWorkspaceModel(String modelId) {
		ModelInfo modelInfo =  modelInfoService.getModelInfo(modelId);
		if(modelInfo != null) {
			JSONObject json = new JSONObject();
			json.put("modelId", modelInfo.getModelId());
			json.put("modelInfo", modelInfo.getModelInfo());
			return json.toString();
		}else {
			return "error";
		}
	}
	/**
	 * s转换为List<Float>
	 * @param s
	 * @return List<Float>
	 */
//	private List<Float> strToList(String s){
//		s = s.substring(1, s.length()-1);
//		List<Float> list = new ArrayList<Float>();
//		String[] strArr = s.split(",");
//		if(strArr.length > 0) {
//			for(String str : strArr) {
//				Float f = Float.parseFloat(str);
//				list.add(f);
//			}
//		}
//		return list;
//	}
}
