package com.cloudsoaring.web.course.service;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.MD5Util;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.exception.BusinessException;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.UserEntity;
import com.cloudsoaring.web.common.service.BaseService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;

@Service
public class HsUserService extends BaseService implements CourseConstants{

	/**
	 * 用户注册
	 * @author LILIANG
	 * @return UserEntity
	 */
	public void doRegist(UserEntity user,String codeNumber) {
		if(codeNumber != null && user.getUserName() != null && user.getUserName() !=""
				&& user.getEmail() != null && user.getEmail() != "" && user.getPassword() != null && user.getPassword() != ""){
			//忽略验证码大小写
			if (!((String)WebContext.session("codeNumber")).equalsIgnoreCase(codeNumber)) {
				throw new BusinessException(getMessage(MSG_E_CODENUMBER_EXIST));
			}
			if (userNameExist(user)) {
				throw new BusinessException(getMessage(MSG_E_USERNAME_EXIST, user.getUserName()));

			}
			if (emailExist(user)) {
				throw new BusinessException(getMessage(MSG_E_EMAIL_EXIST, user.getEmail()));
			}
			if(StringUtil.isNotEmpty(user.getPassword())){
				try{
					user.setPassword(MD5Util.getPassword4MD5(user.getPassword()));
				}catch(Exception ex){
					
				}
			}
			String userName=StringUtil.trim(user.getUserName());
			user.setUserName(userName);
			user.setUserId(IDGenerator.genUID());
			//插入
			commonDao.insertData(user, INSERT_HSUSER);
		}else{
			throw new BusinessException(getMessage(MSG_E_MESSAGE_EXIST));
		}
	}
	
	/**
	 * 生成验证码
	 * @author LILIANG
	 * @return 
	 */
	public void getCodeNumber(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		int width = 90;//定义图片的width
	    int height = 20;//定义图片的height
	    int codeCount = 4;//定义图片上显示验证码的个数
	    int xx = 15;
	    int fontHeight = 18;
	    int codeY = 16;
	    char[] codeSequence = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
	            'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
	            'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
		
		
		// 定义图像buffer
        BufferedImage buffImg = new BufferedImage(width, height,BufferedImage.TYPE_INT_RGB);
        //Graphics2D gd = buffImg.createGraphics();
        //Graphics2D gd = (Graphics2D) buffImg.getGraphics();
        Graphics gd = buffImg.getGraphics();
        // 创建一个随机数生成器类
        Random random = new Random();
        // 将图像填充为白色
        gd.setColor(Color.WHITE);
        gd.fillRect(0, 0, width, height);
        // 创建字体，字体的大小应该根据图片的高度来定。
        Font font = new Font("Fixedsys", Font.BOLD, fontHeight);
        // 设置字体。
        gd.setFont(font);
        // 画边框。
        gd.setColor(Color.BLACK);
        gd.drawRect(0, 0, width - 1, height - 1);
        // 随机产生40条干扰线，使图象中的认证码不易被其它程序探测到。
        gd.setColor(Color.BLACK);
        for (int i = 0; i < 40; i++) {
            int x = random.nextInt(width);
            int y = random.nextInt(height);
            int xl = random.nextInt(12);
            int yl = random.nextInt(12);
            gd.drawLine(x, y, x + xl, y + yl);
        }
        // randomCode用于保存随机产生的验证码，以便用户登录后进行验证。
        StringBuffer randomCode = new StringBuffer();
        int red = 0, green = 0, blue = 0; 
        // 随机产生codeCount数字的验证码。
        for (int i = 0; i < codeCount; i++) {
            // 得到随机产生的验证码数字。
            String code = String.valueOf(codeSequence[random.nextInt(36)]);
            // 产生随机的颜色分量来构造颜色值，这样输出的每位数字的颜色值都将不同。
            red = random.nextInt(255);
            green = random.nextInt(255);
            blue = random.nextInt(255);
 
            // 用随机产生的颜色将验证码绘制到图像中。
            gd.setColor(new Color(red, green, blue));
            gd.drawString(code, (i + 1) * xx, codeY);
 
            // 将产生的四个随机数组合在一起。
            randomCode.append(code);
        }
        // 将四位数字的验证码保存到Session中。
        HttpSession session = req.getSession();
        System.out.print(randomCode);
        session.setAttribute("codeNumber", randomCode.toString());
 
        // 禁止图像缓存。
        resp.setHeader("Pragma", "no-cache");
        resp.setHeader("Cache-Control", "no-cache");
        resp.setDateHeader("Expires", 0);
 
        resp.setContentType("image/jpeg");
 
        // 将图像输出到Servlet输出流中。
        ServletOutputStream sos = resp.getOutputStream();
        ImageIO.write(buffImg, "jpeg", sos);
        sos.close();
	}
	
	
	public Boolean emailExist(UserEntity user){
		int cnt = commonDao.searchCount(user, SQL_USER_EMAIL_EXIST);
		return cnt > 0;
	}
	
	public Boolean userNameExist(UserEntity user){
		int cnt = commonDao.searchCount(user, SQL_USER_NAME_EXIST);
		return cnt > 0;
	}
}
