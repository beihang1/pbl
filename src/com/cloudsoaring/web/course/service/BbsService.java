package com.cloudsoaring.web.course.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.common.constant.Constants;
import com.cloudsoaring.web.common.service.BaseService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.BbsEntity;
import com.cloudsoaring.web.course.entity.BbsReplyEntity;
import com.cloudsoaring.web.course.entity.ForceEntity;
import com.cloudsoaring.web.course.entity.NoteDataEntity;

/***
 * 力的作用数据处理Service
 * @author fanglili
 */
@Service
public class BbsService extends BaseService implements CourseConstants,Constants{

	public void saveBbs(BbsEntity entity) {
		entity.setType("1");
		entity.setCreateUser(WebContext.getSessionUserId());
		entity.setCreateTime(new Date());
		commonDao.insertData(entity, BBS_INSERT);
	}
	
	public void updateBbs(BbsEntity entity) {
		entity.setUpdateTime(new Date());
		commonDao.updateData(entity, BBS_UPDATE);
	}
	
	public ResultBean bbsList(BbsEntity bbs){
		// 精华
		if(NOTE_SEARCH_LIKE.equals(bbs.getPageType())){
			//bbs.setSortName("likeNum");
			bbs.setCreateUser(WebContext.getSessionUserId());
			bbs.setSortName("createTime");
			bbs.setSortOrder("DESC");
		} else {
			// 默认全部 
			bbs.setSortName("createTime");
			bbs.setSortOrder("DESC");
		}
		return searchList4Page(bbs,BBS_SELECT);
	}
	
	public ResultBean bbsReplyList(BbsReplyEntity bbsReply){
		bbsReply.setSortName("createTime");
		bbsReply.setSortOrder("DESC");
		return searchList4Page(bbsReply,BBS_REPLY_SELECT);
	}
	
	public BbsEntity getBbsByPk(String id) {
		BbsEntity entity  = new BbsEntity();
		entity.setId(id);
		return commonDao.searchOneData(entity, BBS_SELECT_BY_PK);
	}
	
	public ResultBean deleteById(String id) throws Exception {
		ResultBean result = new ResultBean();
		commonDao.deleteData(id, BBS_DELETE_BY_PK);
		result.setStatus(true);
		result.setMessages("删除成功");
		return result;
	}

	public ResultBean deleteReplyById(String id) throws Exception {
		ResultBean result = new ResultBean();
		commonDao.deleteData(id, BBS_REPLY_DELETE_BY_PK);
		result.setStatus(true);
		result.setMessages("删除成功");
		return result;
	}
	
	public void saveBbsReply(BbsReplyEntity entity) {
		entity.setCreateUser(WebContext.getSessionUserId());
		entity.setCreateTime(new Date());
		commonDao.insertData(entity, BBS_REPLY_INSERT);
	}
}
