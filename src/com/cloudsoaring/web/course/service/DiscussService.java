package com.cloudsoaring.web.course.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.service.MessageService;
import com.cloudsoaring.web.common.service.BaseService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.ChapterEntity;
import com.cloudsoaring.web.course.entity.CourseEntity;
import com.cloudsoaring.web.course.entity.CourseMessageEntity;
import com.cloudsoaring.web.course.entity.DiscussDataEntity;
import com.cloudsoaring.web.course.entity.DiscussEntity;
import com.cloudsoaring.web.course.entity.OperateRecordEntity;
import com.cloudsoaring.web.course.view.DiscussView;

@Service
public class DiscussService extends BaseService implements CourseConstants {

	@Autowired
	private CourseService courseService;

	@Autowired
	private ChapterService chapterService;
	
	@Autowired
	private MessageService messageService;

	/***
	 * 评论查询 DiscussView linkId:'链接ID',linkType:'链接类型'
	 * 
	 * @return DiscussView
	 */
	public ResultBean searchDiscuss4Page(DiscussView discuss) {
		ResultBean result = new ResultBean();
		// 链接ID为空
		assertNotEmpty(discuss.getLinkId(), "MSG_E_NULL_ID");
		// 链接类型为空
		assertNotEmpty(discuss.getLinkType(), "MSG_E_NULL_LINK_TYPE");

		// 链接类型错误：不等于COURSE or CHAPTER
		if (!discuss.getLinkType().equalsIgnoreCase(LINK_TYPE_COURSE)
				&& !discuss.getLinkType().equalsIgnoreCase(LINK_TYPE_CHAPTER)) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_ERROR_LINK_TYPE));
			return result;
		}

		// 课程不存在
		if (discuss.getLinkType().equalsIgnoreCase(LINK_TYPE_COURSE)) {
			assertExist(discuss.getLinkId(), SQL_IS_COURSE_EXIST,
					MSG_E_COURSE_NOT_EXIST);
		}

		// 章节不存在
		if (LINK_TYPE_CHAPTER.equalsIgnoreCase(discuss.getLinkType())) {
			assertExist(discuss.getLinkId(), SQL_IS_CHAPTER_EXIST,
					MSG_E_CHAPTER_NOT_EXIST);
		}
		discuss.setUserId(WebContext.getSessionUserId());
		List<DiscussView> disviewList = new ArrayList<DiscussView>();
		List<DiscussView> disList = new ArrayList<DiscussView>();
		result = commonDao.searchList4Page(discuss, SEARCH_DISCUSS_DATE_LIST);
		disList = (List<DiscussView>) commonDao.searchList4Page(discuss, SEARCH_DISCUSS_DATE_LIST).getData();
		if(null!=disList && disList.size()>0) {
			for(int i=0;i<disList.size();i++) {
				if(StringUtil.isBlank(disList.get(i).getTargetDiscussUserId())) {
					DiscussView view =new DiscussView();
					String isReply="0";
					view = disList.get(i);
					String id = disList.get(i).getId();
					List<DiscussDataEntity> dataList = new ArrayList<DiscussDataEntity>();
					DiscussDataEntity dataEntity = new DiscussDataEntity();
					dataEntity.setTargetDiscussUserId(id);
					dataList = commonDao.searchList(dataEntity, SELECT_DISCUSS_DATA);
					if(null!=dataList && dataList.size()>0) {
						view.setReplyContent(dataList.get(0).getContent());
						view.setReplyDate(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dataList.get(0).getCreateDate()));
						view.setReplyName(dataList.get(0).getPersonName());
						isReply="1";
						view.setIsReply(isReply);
						view.setReplyId(dataList.get(0).getId());
						view.setReplyImage(dataList.get(0).getReplyImage());
					}else {
						view.setIsReply(isReply);
					}
					disviewList.add(view);
				}
				
			}
		}
		return result.setData(disviewList);

	}

	/**
	 * 发表评论
	 * 
	 * @param discuss
	 * @return ResultBean
	 */
	public ResultBean publishDiscuss(DiscussView discuss) {
		ResultBean result = new ResultBean();
		// 内容为空
		if (StringUtil.isEmpty(discuss.getContent())) {
			result.setStatus(false);
			result.setMessages("评论内容不能为空");
			return result;
		}

		assertNotGuest();
		// 链接ID为空
		assertNotEmpty(discuss.getLinkId(), "MSG_E_NULL_ID");
		// 链接类型为空
		assertNotEmpty(discuss.getLinkType(), "MSG_E_NULL_LINK_TYPE");

		// 链接类型错误：不等于COURSE or CHAPTER
		if (!discuss.getLinkType().equalsIgnoreCase(LINK_TYPE_COURSE)
				&& !discuss.getLinkType().equalsIgnoreCase(LINK_TYPE_CHAPTER)) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_ERROR_LINK_TYPE));
			return result;
		}

		// 课程不存在
		if (discuss.getLinkType().equalsIgnoreCase(LINK_TYPE_COURSE)) {
			assertExist(discuss.getLinkId(), SQL_IS_COURSE_EXIST,
					MSG_E_COURSE_NOT_EXIST);
		}

		// 章节不存在
		if (discuss.getLinkType().equalsIgnoreCase(LINK_TYPE_CHAPTER)) {
			assertExist(discuss.getLinkId(), SQL_IS_CHAPTER_EXIST,
					MSG_E_CHAPTER_NOT_EXIST);
		}

		DiscussEntity de = new DiscussEntity();
		de.setLinkId(discuss.getLinkId());
		de.setLinkType(discuss.getLinkType());
		// 检索该业务下是否存在评论
		de = commonDao.searchOneData(de, SQL_DISCUSS_SELECT);
		if (de == null) {
			// 评论不存在，插入
			de = new DiscussEntity();
			de.setId(IDGenerator.genUID());
			de.setLinkId(discuss.getLinkId());
			de.setLinkType(discuss.getLinkType());
			de.setCreateUser(WebContext.getSessionUserId());
			if (discuss.getLinkType().equals(LINK_TYPE_COURSE)) {
				// 课程评论
				CourseEntity course = new CourseEntity();
				course.setId(discuss.getLinkId());
				course = commonDao.searchOneData(course, SEARCH_COURSE_BY_PK);
				if (course != null) {
					de.setTitle(course.getTitle());
				}
			} else if (discuss.getLinkType().equals(LINK_TYPE_CHAPTER)) {
				// 章节评论
				ChapterEntity chapter = new ChapterEntity();
				chapter.setId(discuss.getLinkId());
				chapter = commonDao
						.searchOneData(chapter, SEARCH_CHAPTER_BY_PK);
				if (chapter != null) {
					de.setTitle(chapter.getTitle());
				}
			}
			commonDao.insertData(de, INSERT_DISCUSS);
		}
		// 新增评论数据信息
		discuss.setDiscussId(de.getId());
		discuss.setId(IDGenerator.genUID());
		discuss.setUserId(WebContext.getSessionUserId());
		if(StringUtil.isNotBlank(discuss.getReplyId())) {
			discuss.setTargetDiscussUserId(discuss.getReplyId());
		}
		commonDao.insertData(discuss, INSERT_DISCUSS_DATA);
		//发系统消息
		messageService.sendSystemMessage(discuss.getTargetUserId(), getMessage(MSG_SOMEONE_REPLY_DISCUSS, WebContext.getSessionUser().getPersonName()));
		return result;
	}

	/**
	 *  检索我的评论
	 * 
	 * @param discuss 评论entity
	 *            
	 * @return 我的评论列表
	 * @author WUXIAOXIANG
	 */
	public ResultBean userDiscussList(DiscussView discuss) {
		// 判断用户是否登录
		assertNotGuest();
		// 当前登录用户
		discuss.setUserId(WebContext.getSessionUserId());
		discuss.setSortName("createDate");
		return commonDao.searchList4Page(discuss, SELECT_USER_DISCUSS_LIST);
	}

	/**
	 * 评论点赞
	 * 
	 * @param id
	 * @return
	 */
	public ResultBean likeDiscuss(String id) {
		ResultBean result = new ResultBean();
		// 链接ID为空
		if (StringUtil.isEmpty(id)) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_NULL_ID));
			return result;
		}
		if (WebContext.isGuest()) {
			result.setStatus(false);
			result.setMessages(getMessage(NULL_USER_ID));
			return result;
		}
		// 判断评论是否存在
		if (!isExistDiscuss(id)) {
			result.setStatus(false);
			result.setMessages("该评论不存在！");
			return result;
		}
		// 点赞
		OperateRecordEntity or = new OperateRecordEntity();
		or.setId(IDGenerator.genUID());
		or.setLink(id);
		or.setOperateType(OPERATE_LIKE_DISCUSS);
		or.setOperateUser(WebContext.getSessionUserId());
		or.setCreateUser(WebContext.getSessionUserId());
		commonDao.insertData(or, SQL_OPERATE_RECORD);
		commonDao.updateData(id, SQL_UPDATE_DISCUSS_NUM_LIKE_ADD);
		result.setStatus(true);
		return result;
	}

	/**
	 * 判断评论是否存在
	 * 
	 * @param id
	 * @return true:存在，false:不存在
	 */
	private boolean isExistDiscuss(String id) {
		DiscussDataEntity d = new DiscussDataEntity();
		d.setId(id);
		int count = commonDao.searchCount(d, SQL_DISCUSS_IS_EXIST);
		return count > 0;
	}

	/***
	 * 删除评论数据
	 */
	public ResultBean deleteDiscuss(String id) {
		assertNotEmpty(id, MSG_E_EMPTY_DISCUSS_ID);
		DiscussDataEntity dd = new DiscussDataEntity();
		dd.setId(id);
		commonDao.deleteData(dd, DELETE_DISCUSS_DATA);
		// 删除点赞的数据
		OperateRecordEntity or = new OperateRecordEntity();
		or.setLink(id);
		or.setOperateType(OPERATE_LIKE_DISCUSS);
		commonDao.deleteData(or, SQL_DELETE_OPERATE_BY_LINK_AND_TYPE);
		return new ResultBean().setStatus(true);
	}
}
