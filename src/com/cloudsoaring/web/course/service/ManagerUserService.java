package com.cloudsoaring.web.course.service;

import org.springframework.stereotype.Service;

import com.cloudsoaring.web.common.service.BaseService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.ManagerUserEntity;


/**
 * 
 * @author ljl 前台测试信息
 */
@Service
public class ManagerUserService extends BaseService implements CourseConstants {

    /**
                    * 分页查询管理员信息
     * @param entity
     * @return
     */
	public ResultBean searchPage(ManagerUserEntity entity){
		return commonDao.searchList4Page(entity, MANAGER_USER_LIST);
	}
	/**
	 * 保存管理员信息表
	 * @param entity
	 */
	public void insert(ManagerUserEntity entity){
		this.commonDao.insertData(entity,MANAGER_USER_INSERT);
	}
	/**
	 * 根据员工账号查询是否是管理员
	 * @param id 员工账号
	 * @return
	 */
	public ManagerUserEntity searchByUserId(String id) {
		ManagerUserEntity entity = new ManagerUserEntity();
		entity.setUserId(id);
		return commonDao.searchOneData(entity, MANAGER_USER_SEARCH);
	}
	/**
	 * 更新管理员信息表
	 * @param entity
	 */
	public void update(ManagerUserEntity entity){
		this.commonDao.updateData(entity, MANAGER_USER_UPDATE);
	}
	/**
	 * 删除管理员信息方法
	 * @param entity
	 */
	public void delete(ManagerUserEntity entity){
		this.commonDao.deleteData(entity, MANAGER_USER_DELETE);
	}
}
