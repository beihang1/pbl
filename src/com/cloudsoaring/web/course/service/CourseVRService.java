package com.cloudsoaring.web.course.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;
import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.common.constant.Constants;
import com.cloudsoaring.web.common.service.BaseService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.CourseExperimentalResultEntity;
import com.cloudsoaring.web.course.entity.HomeworkAnswerEntity;
import com.cloudsoaring.web.course.entity.HomeworkEntity;
import com.cloudsoaring.web.course.view.ChapterView;
import com.cloudsoaring.web.course.view.HomeworkView;

import net.sf.json.JSONObject;

/***
 * 粤语-饮早茶课程数据处理Service
 * @author fanglili
 */
@Service
public class CourseVRService extends BaseService implements CourseConstants,Constants{

	/**
	 * 获取所有人员得分情况
	 * @param null
	 * @return List<HomeworkView>
	 */
	@SuppressWarnings("unchecked")
	public List<HomeworkView> getUserStepScore(){
		return commonDao.searchList(null, VR_SELECT_USERSTEPSCORE);
	}
	@SuppressWarnings("unchecked")
	public ResultBean seacherHomeworkPage(HomeworkView homeworkView){ 
		ResultBean result = new ResultBean();
		List<HomeworkView> viewList = new ArrayList<HomeworkView>();
		result = commonDao.searchList4Page(homeworkView, HOME_WORK);
		viewList = (List<HomeworkView>) commonDao.searchList4Page(homeworkView, HOME_WORK).getData();
		if(null!=viewList && viewList.size()>0) {
			for(int i=0;i<viewList.size();i++) {
				String homeworkId = viewList.get(i).getId();
				List<HomeworkAnswerEntity> answerList = new ArrayList<HomeworkAnswerEntity>();
				HomeworkAnswerEntity entity = new HomeworkAnswerEntity();
				entity.setHomeworkId(homeworkId);
				answerList = commonDao.searchList(entity, SELECT_UAER_ANS_HOME_WORK);
				if(null!=answerList && answerList.size()>0) {
					viewList.get(i).setExamStatus("1");
				}else {
					viewList.get(i).setExamStatus("0");
				}
			}
		}
		result.setData(viewList);
		return result;
	}
	@SuppressWarnings("unchecked")
	public ResultBean seacherHomeworkUserPage(HomeworkView homeworkView){
		ResultBean result = new ResultBean();
		result = commonDao.searchList4Page(homeworkView, UAER_HOME_WORK);
		List<HomeworkView> viewList= commonDao.searchList(homeworkView, UAER_HOME_WORK);
		List<HomeworkView> teamList= commonDao.searchList(homeworkView, TEAM_HOME_WORK);
		//List<HomeworkView> uncommitteamList= commonDao.searchList(homeworkView, TEAM_UNCOMMIT_HOME_WORK);
		if(teamList.size() > 0) {
			viewList.addAll(teamList);
		}
		/*if(uncommitteamList.size() >0) {
			viewList.addAll(uncommitteamList);
		}*/
		if(null!=viewList && viewList.size()>0) {
			for(int i=0;i<viewList.size();i++) {
				viewList.get(i).setTitle(homeworkView.getTitle());
			}
		}
		result.setData(viewList);
		return result;
	}
	public void update(HomeworkView homeworkView) {
		commonDao.updateData(homeworkView, UAER_ANS_HOME_WORK);
	}
	public void deleteAnswer(HomeworkView homeworkView) {
		commonDao.deleteData(homeworkView, DELETE_UAER_ANS_HOME_WORK);
	}
	public void insertAnswer(HomeworkView homeworkView) {
		commonDao.insertData(homeworkView, INSERT_UAER_ANS_HOME_WORK);
	}
	public HomeworkAnswerEntity selectOneAnswer(HomeworkAnswerEntity homeworkView) {
		return commonDao.searchOneData(homeworkView, SELECT_UAER_ANS_HOME_WORK);
	}
	public HomeworkView selectHomework(HomeworkView homeworkView) {
		return commonDao.searchOneData(homeworkView, SEARCH_USER_HOME_BY_LINKID);
	}
	@SuppressWarnings("unchecked")
	public ResultBean seacherStudentHomeworkUserPage(HomeworkView homeworkView){
		return commonDao.searchList4Page(homeworkView, STUDENT_HOME_WORK);
	}
	public HomeworkView searchStudentHomeWork(HomeworkView home) {
		return commonDao.searchOneData(home, STUDENT_HOME_WORK_DETAIL);
	}
	public List<HomeworkView> seacherStudentHomeworkUser(HomeworkView homeworkView) {
		// TODO Auto-generated method stub
		return commonDao.searchList(homeworkView, STUDENT_HOME_WORK);
	}
	
}
