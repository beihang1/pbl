package com.cloudsoaring.web.course.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.CourseUserEvalutionEntity;
import com.cloudsoaring.web.trainingplatform.entity.TpUserEntity;

/***
 * 模块相关的业务处理Service
 * 
 * @author ljl
 *
 */
@Service
@SuppressWarnings("unchecked")
public class CourseUserEvalutionService extends FileService implements CourseConstants {
	/**
	 * 批量查询
	 * @param courseUserEvalutionEntity
	 * @return
	 */
	public List<CourseUserEvalutionEntity> searchInfo(CourseUserEvalutionEntity courseUserEvalutionEntity) {
		return commonDao.searchList(courseUserEvalutionEntity, SEARCH_COURSE_USER_EVALUTION_LIST);
	}
	/**
	 * 查询
	 * @param courseUserEvalutionEntity
	 * @return
	 */
	public CourseUserEvalutionEntity searchOneInfo(CourseUserEvalutionEntity courseUserEvalutionEntity) {
		return commonDao.searchOneData(courseUserEvalutionEntity, SEARCH_COURSE_USER_EVALUTION_LIST);
	}
	/**
	 * 根据主键查询
	 * @param id
	 * @return
	 */
	public CourseUserEvalutionEntity searchById(String id) {
		CourseUserEvalutionEntity courseUserEvalutionEntity = new CourseUserEvalutionEntity();
		courseUserEvalutionEntity.setId(id);
		return commonDao.searchOneData(courseUserEvalutionEntity, SEARCH_COURSE_USER_EVALUTION_LIST);
	}
	 

	/**
	 * 单条插入
	 * @param courseUserEvalutionEntity
	 */
	public void insert(CourseUserEvalutionEntity courseUserEvalutionEntity){
		courseUserEvalutionEntity.setId(IDGenerator.genUID());
		courseUserEvalutionEntity.setCreateTime(new Date());
		commonDao.insertData(courseUserEvalutionEntity, SEARCH_COURSE_USER_EVALUTION_INSERT);
	}
	/**
	 * 批量查询
	 * @param courseUserEvalutionEntity
	 */
	public void insertBatch(CourseUserEvalutionEntity courseUserEvalutionEntity){
		if(null!=courseUserEvalutionEntity && courseUserEvalutionEntity.getEvalutionList()!=null && courseUserEvalutionEntity.getEvalutionList().size()>0 ) {
			for(int i=0;i<courseUserEvalutionEntity.getEvalutionList().size();i++) {
				CourseUserEvalutionEntity entity = courseUserEvalutionEntity.getEvalutionList().get(i);
				entity.setId(IDGenerator.genUID());
				courseUserEvalutionEntity.setCreateTime(new Date());
				commonDao.insertData(entity, SEARCH_COURSE_USER_EVALUTION_INSERT);
			}
		}
		
	}
	/**
	 * 单条更新
	 * @param id
	 */
	public void update(String id){
		CourseUserEvalutionEntity courseUserEvalutionEntity = new CourseUserEvalutionEntity();
		courseUserEvalutionEntity.setId(id);
		courseUserEvalutionEntity.setCreateTime(new Date());
		commonDao.updateData(courseUserEvalutionEntity, SEARCH_COURSE_USER_EVALUTION_UPDATE);
	}
	/**
	 * 批量更新
	 * @param courseUserEvalutionEntity
	 */
	public void updateBatch(CourseUserEvalutionEntity courseUserEvalutionEntity){
		if(null!=courseUserEvalutionEntity && courseUserEvalutionEntity.getEvalutionList()!=null && courseUserEvalutionEntity.getEvalutionList().size()>0 ) {
			for(int i=0;i<courseUserEvalutionEntity.getEvalutionList().size();i++) {
				CourseUserEvalutionEntity entity = courseUserEvalutionEntity.getEvalutionList().get(i);
				courseUserEvalutionEntity.setCreateTime(new Date());
				commonDao.updateData(entity, SEARCH_COURSE_USER_EVALUTION_UPDATE);
			}
		}
	}
	/**
	 * 根据主键删除
	 * @param id
	 */
	public void deleteById(String id){
		CourseUserEvalutionEntity courseUserEvalutionEntity = new CourseUserEvalutionEntity();
		courseUserEvalutionEntity.setId(id);
		commonDao.deleteData(courseUserEvalutionEntity, DELETE_COURSE_USER_EVALUTION_ID);
	}
	/**
	 * 根据课程主键删除
	 * @param courseId
	 */
	public void deleteByCourseId(String courseId){
		CourseUserEvalutionEntity courseUserEvalutionEntity = new CourseUserEvalutionEntity();
		courseUserEvalutionEntity.setCourseId(courseId);
		commonDao.deleteData(courseUserEvalutionEntity, DELETE_COURSE_USER_EVALUTION_COURSEID);
	}
	public List<CourseUserEvalutionEntity> searchWdList(CourseUserEvalutionEntity entity1) {
		return commonDao.searchList(entity1, DELETE_COURSE_USER_EVALUTION_COURSEID_USERID);
	}
	public void deleteByEntity(CourseUserEvalutionEntity evalution) {
		commonDao.deleteData(evalution, DELETE_EVALUTION);
		
	}
	public Integer getCourseCount(String userId) {
		CourseUserEvalutionEntity entity = new CourseUserEvalutionEntity();
		entity.setUserId(userId);
		return commonDao.searchCount(entity, SELECT_EVALUTION_COUNT);
	}
	public Double getCourseWeidu(String userId, String weidu) {
		Double score=0.0;
		CourseUserEvalutionEntity entity = new CourseUserEvalutionEntity();
		entity.setUserId(userId);
		entity.setWeidu(weidu);
		CourseUserEvalutionEntity entity1 = new CourseUserEvalutionEntity();
		entity1 = commonDao.searchOneData(entity, SELECT_EVALUTION_WEIDU);
		if(null==entity1) {
			score=0.0;
		}else if(null!=entity1 && null!=entity1.getScore()) {
			score =  Double.valueOf(entity1.getScore().toString());
		}
		return score;
		
	}
	public Double getPjScore(String sessionUserId) {
		CourseUserEvalutionEntity entity = new CourseUserEvalutionEntity();
		entity.setUserId(sessionUserId);
		CourseUserEvalutionEntity entity1 = new CourseUserEvalutionEntity();
		entity1 = commonDao.searchOneData(entity, SELECT_EVALUTION_SCORE);
		if(null==entity1) {
			return null;
		}else {
			return Double.valueOf(entity1.getScore().toString());
		}
	}
	public Double getCourseWeiduByGroup(String userId, String weidu) {
		Double score=70.0;
		CourseUserEvalutionEntity entity = new CourseUserEvalutionEntity();
		entity.setUserId(userId);
		entity.setWeidu(weidu);
		CourseUserEvalutionEntity entity1 = new CourseUserEvalutionEntity();
		entity1 = commonDao.searchOneData(entity, SELECT_EVALUTION_PJ_WEIDU);
		if(null==entity1) {
			score=70.0;
		}else if(null!=entity1 && null!=entity1.getScore()) {
			score =  Double.valueOf(entity1.getScore().toString());
		}
		return score;
	}
	public CourseUserEvalutionEntity searchCourseByUserIdAndTime(CourseUserEvalutionEntity entity) {
		return commonDao.searchOneData(entity, SELECT_EVALUTION_PJ_TIME);
	}
	public CourseUserEvalutionEntity searchWdInfo(CourseUserEvalutionEntity entity1) {
		// TODO Auto-generated method stub
		return commonDao.searchOneData(entity1, DELETE_COURSE_USER_EVALUTION_COURSEID_USERID);
	}
	public List<CourseUserEvalutionEntity> getMyEvalutionInfo(CourseUserEvalutionEntity courseUserEvalutionEntity) {
		return commonDao.searchList(courseUserEvalutionEntity, SELECT_EVALUTION_MY);
	}
	public ResultBean getMyInfo(CourseUserEvalutionEntity courseUserEvalutionEntity) {
		ResultBean result = new ResultBean();
		result = commonDao.searchList4Page(courseUserEvalutionEntity, SELECT_EVALUTION_MY);
		return result;
	}
	
}

