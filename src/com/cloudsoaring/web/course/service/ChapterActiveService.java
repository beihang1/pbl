package com.cloudsoaring.web.course.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.ChapterActiveEntity;

/**
 * 章节
 * 
 * @author 
 * @date 2020-06-16 */
@Service
public class ChapterActiveService extends FileService implements CourseConstants {
	
	/**
	 * 分页查询活动记录
	 * @param entity
	 * @return
	 */
	public ResultBean searchListPage(ChapterActiveEntity entity){
		return commonDao.searchList4Page(entity, CHAPTER_SELECT);
	}
	/**
	 * 查询章节活动记录
	 * @param entity
	 * @return
	 */
	public List<ChapterActiveEntity> searchList(ChapterActiveEntity entity){
		return commonDao.searchList(entity, CHAPTER_SELECT);
	}
	/**
	 * 保存章节活动信息
	 * @param entity
	 */
	public void insertInfo(ChapterActiveEntity entity){
		//随机生成章节活动记录主键
		entity.setId(IDGenerator.genUID());
		entity.setCreateDate(new Date());
		//设置活动创建对象账号
		entity.setCreateUser(WebContext.getSessionUserId());
		this.commonDao.insertData(entity,CHAPTER_INSERT);
	}
	/**
	 * 更新活动记录
	 * @param entity
	 */
	public void updateInfoById(ChapterActiveEntity entity){
		//设置更新人账号
		entity.setUpdateUser(WebContext.getSessionUserId());
		//设置更新时间
		entity.setUpdateDate(new Date());
		this.commonDao.updateData(entity, CHAPTER_UPDATE);
	}
	/**
	 * 查询活动记录
	 * @param entity
	 * @return
	 */
	public ResultBean searchAllCourseChapterPage(ChapterActiveEntity entity){
		ResultBean result = new ResultBean(); 
		List<ChapterActiveEntity> entityList = new ArrayList<ChapterActiveEntity>();
		//查询活动记录
		entityList = (List<ChapterActiveEntity>) commonDao.searchList4Page(entity, CHAPTER_ALL_CHAPTER_SELECT).getData();
		//循环遍历活动记录
		if(null!=entityList && entityList.size()>0) {
			for(int i=0;i<entityList.size();i++) {
				//设置时间格式返回给前台
				entityList.get(i).setCreateDateStr(new SimpleDateFormat("yyyy-MM-dd").format(entityList.get(i).getCreateDate()));
			}
		}
		result.setData(entityList);
		return result;
	}
	/**
	 * 根据活动主键删除活动记录方法
	 * @param entity
	 */
	public void deleteInfo(ChapterActiveEntity entity){
		this.commonDao.insertData(entity,CHAPTER_DELETE);
	}
}
