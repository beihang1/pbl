package com.cloudsoaring.web.course.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.BigDecimalUtil;
import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.common.service.BaseService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.QaAnswerEntity;
import com.cloudsoaring.web.course.entity.QaQuestionEntity;
import com.cloudsoaring.web.points.constant.PointConstants;
import com.cloudsoaring.web.points.entity.UserPoint;
import com.cloudsoaring.web.points.service.PointService;

/***
 * 课程/章节问答模块相关的业务处理Service
 * 
 * @author WUXIAOXIANG
 *
 */
@Service
public class FaqService extends BaseService implements CourseConstants {
	@Autowired
	private CourseService courseService;

	@Autowired
	private ChapterService chapterService;

	@Autowired
	private PointService pointService;

	/**
	 * 检索问答列表
	 * 
	 * @param qaQuestionEntity
	 *            问答entity：包含linkId 链接ID；linkType 链接类型(COURSE:课程,CHAPTER:章节);
	 *            pageSize每页显示行数, pageNumber当前页
	 * @return 课程/章节问答列表
	 * @author WUXIAOXIANG
	 */
	public ResultBean searchFagList(QaQuestionEntity qaQuestionEntity) {

		ResultBean result = new ResultBean();

		// 链接ID为空
		assertNotEmpty(qaQuestionEntity.getLinkId(), MSG_E_EMPTY_LINK_ID);
		// 链接类型为空
		assertNotEmpty(qaQuestionEntity.getLinkType(), MSG_E_EMPTY_LINK_TYPE);
		// 链接类型错误：不等于COURSE or CHAPTER
		if (!LINK_TYPE_COURSE.equalsIgnoreCase(qaQuestionEntity.getLinkType())
				&& !LINK_TYPE_CHAPTER.equalsIgnoreCase(qaQuestionEntity
						.getLinkType())) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_ERROR_LINK_TYPE));
			return result;
		}
		// 课程不存在
		if (qaQuestionEntity.getLinkType().equalsIgnoreCase(LINK_TYPE_COURSE)) {
			assertExist(qaQuestionEntity.getLinkId(), SQL_IS_COURSE_EXIST,
					MSG_E_COURSE_NOT_EXIST);
		}
		// 章节不存在
		if (qaQuestionEntity.getLinkType().equalsIgnoreCase(LINK_TYPE_CHAPTER)) {
			assertExist(qaQuestionEntity.getLinkId(), SQL_IS_CHAPTER_EXIST,
					MSG_E_CHAPTER_NOT_EXIST);
		}

		// 根据linkId 链接ID；linkType 链接类型，检索问答列表
		return super.searchList4Page(qaQuestionEntity,
				"QaQuestion.searchFagList");
	}

	/**
	 * 插入问答
	 * 
	 * @param linkId
	 *            链接ID
	 * @param linkType
	 *            链接类型(COURSE:课程,CHAPTER:章节)
	 * @param title
	 *            问题标题
	 * @param content
	 *            问题内容
	 * @param sendPoint
	 *            送出积分
	 * @return 是否插入成功
	 * @author WUXIAOXIANG
	 */
	public ResultBean insertQaQuestion(String linkId, String linkType,
			String title, String content, String sendPoint) {

		ResultBean result = new ResultBean();

		// 判断用户是否登录
		assertNotGuest();
		// 链接ID为空
		assertNotEmpty(linkId, MSG_E_EMPTY_LINK_ID);
		// 链接类型为空
		assertNotEmpty(linkType, MSG_E_EMPTY_LINK_TYPE);
		// 链接类型错误：不等于COURSE or CHAPTER
		if (!LINK_TYPE_COURSE.equalsIgnoreCase(linkType)
				&& !LINK_TYPE_CHAPTER.equalsIgnoreCase(linkType)) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_ERROR_LINK_TYPE));
			return result;
		}
		// 课程不存在
		if (linkType.equalsIgnoreCase(LINK_TYPE_COURSE)) {
			assertExist(linkId, SQL_IS_COURSE_EXIST, MSG_E_COURSE_NOT_EXIST);
		}
		// 章节不存在
		if (linkType.equalsIgnoreCase(LINK_TYPE_CHAPTER)) {
			assertExist(linkId, SQL_IS_CHAPTER_EXIST,
					MSG_E_CHAPTER_NOT_EXIST);
		}
		// 问答标题为空
		assertNotEmpty(title, MSG_E_FAQ_TITLE_NULL);

		// 
		UserPoint use = pointService.fetchPoint(WebContext.getSessionUserId());
		// 剩余积分不够
		sendPoint = (sendPoint==null?"0":sendPoint);
		if (BigDecimalUtil.compare4Small(use.getPoints(), Long.valueOf(sendPoint).intValue())) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_POINT_NOT_ENOUGH));
			return result;
		}
		pointService.consumePoint(PointConstants.SYSTEM_CODE_HS,
				WebContext.getSessionUserId(), DEF_POINTS_TYPE_D0005,
				new BigDecimal(Long.valueOf(sendPoint)), null, null);
		QaQuestionEntity qaQuestionEntity = new QaQuestionEntity();
		qaQuestionEntity.setId(IDGenerator.genUID());
		qaQuestionEntity.setLinkId(linkId);
		qaQuestionEntity.setLinkType(linkType.toUpperCase());
		qaQuestionEntity.setTitle(title);
		qaQuestionEntity.setContent(content);
		qaQuestionEntity.setSendPoint(new BigDecimal(sendPoint));
		qaQuestionEntity.setCreateUser(WebContext.getSessionUserId());
		qaQuestionEntity.setUpdateUser(WebContext.getSessionUserId());
		qaQuestionEntity.setFlagBest("1");
		commonDao.insertData(qaQuestionEntity, INSERT_QA_QUESTION);

		result.setStatus(true);

		return result;
	}

	/**
	 * 检索问答详情
	 * 
	 * @param linkId
	 *            : // 链接ID linkType // 课程COURSE ；章节CHAPTER qaQuestionId // 问题ID
	 * @return QaAnswerEntity
	 */
	public ResultBean questionDetail(QaQuestionEntity qaQuestionEntity) {
		ResultBean result = new ResultBean();
		// 从session中取出登录用户的ID，判断用户是否登录

		if (WebContext.isGuest()) {
			result.setStatus(false);
			result.setMessages(getMessage(NULL_USER_ID));
			return result;
		}

		// 链接ID为空
		if (StringUtil.isEmpty(qaQuestionEntity.getLinkId())) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_NULL_ID));
			return result;
		}
		assertNotEmpty(qaQuestionEntity.getQaQuestionId(),MSG_E_NULL_ID);
		// 链接类型为空
		if (StringUtil.isEmpty(qaQuestionEntity.getLinkType())) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_EMPTY_LINK_TYPE));
			return result;
		}

		// 链接类型错误：不等于COURSE or CHAPTER
		if (!qaQuestionEntity.getLinkType().equalsIgnoreCase(LINK_TYPE_COURSE)
				&& !qaQuestionEntity.getLinkType().equalsIgnoreCase(
						LINK_TYPE_CHAPTER)) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_ERROR_LINK_TYPE));
			return result;
		}

		// 课程不存在
		if (qaQuestionEntity.getLinkType().equalsIgnoreCase(LINK_TYPE_COURSE)) {
			assertExist(qaQuestionEntity.getLinkId(), SQL_IS_COURSE_EXIST,
					MSG_E_COURSE_NOT_EXIST);
		}

		// 章节不存在
		if (qaQuestionEntity.getLinkType().equalsIgnoreCase(LINK_TYPE_CHAPTER)) {
			assertExist(qaQuestionEntity.getLinkId(), SQL_IS_CHAPTER_EXIST,
					MSG_E_CHAPTER_NOT_EXIST);
		}
		qaQuestionEntity.setUserId(WebContext.getSessionUserId());
		return commonDao.searchList4Page(qaQuestionEntity, SEARCH_QAW_QUESTION);

	}

	/**
	 * 我的问,答
	 */
	public ResultBean userFaqList(String type,Integer pageSize,Integer pageNumber) {
		ResultBean result = new ResultBean();
		if(pageSize==null){
			pageSize=20;
		}
		if(pageNumber==null){
			pageNumber=1;
		}
		// 判断用户是否登录
		if (WebContext.isGuest()) {
			result.setStatus(false);
			result.setMessages(getMessage(NULL_USER_ID));
			return result;
		}
		String userId = WebContext.getSessionUserId();
		if (type == null || type == "") {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_TYPE_ERROR));
			return result;
		}
		// 判断是我的问还是我的答
		if (type.equals("0")) {
			// 根据用户id，检索问答列表
			QaQuestionEntity qaQuestionEntity = new QaQuestionEntity();
			qaQuestionEntity.setCreateUser(userId);
			qaQuestionEntity.setPageNumber(pageNumber);
			qaQuestionEntity.setPageSize(pageSize);
			return super.searchList4Page(qaQuestionEntity,
					"QaQuestion.searchMyFagList");
		} else {
			//
			QaAnswerEntity qaAnswerEntity = new QaAnswerEntity();
			qaAnswerEntity.setUserId(userId);
			qaAnswerEntity.setPageNumber(pageNumber);
			qaAnswerEntity.setPageSize(pageSize);
			return super.searchList4Page(qaAnswerEntity,
					"QaQuestion.searchMyAnsList");
		}

	}

	/**
	 * 查询问题
	 * 
	 * @param linkId
	 * @param linkType
	 * @param qaQuestionId
	 * @return
	 */
	public QaQuestionEntity searchOneQaQuestion(String linkId, String linkType,
			String qaQuestionId) {
		QaQuestionEntity qaQuestionEntity = new QaQuestionEntity();
		qaQuestionEntity.setLinkId(linkId);
		qaQuestionEntity.setLinkType(linkType);
		qaQuestionEntity.setId(qaQuestionId);
		QaQuestionEntity qaQuestion = commonDao.searchOneData(qaQuestionEntity,
				"QaQuestion.searchFagList");
		return qaQuestion;
	}

	/**
	 * 回答
	 * 
	 * @retrun 返回结果
	 */
	public ResultBean publishAnswer(QaAnswerEntity qaAnswerEntity) {
		ResultBean result = new ResultBean();
		assertNotGuest();
		qaAnswerEntity.setUserId(WebContext.getSessionUserId());
		qaAnswerEntity.setId(IDGenerator.genUID());
		commonDao.insertData(qaAnswerEntity, INSERTE_QAW_QUESTION);
		result.setMessages("回复成功");
		return result;
	}

	/**
	 * 浏览数增加
	 */
	public void updateViewNum(String qaQuestionId) {
		if(StringUtil.isNotEmpty(qaQuestionId)) {
			QaQuestionEntity qa = new QaQuestionEntity();
			qa.setId(qaQuestionId);
			commonDao.updateData(qa, UPDATE_VIEW_NUM);
		}

	}

	/**
	 * 采纳问题回答
	 * 
	 * @param linkId 链接ID
	 * @param linkType 链接类型
	 * @param qaQuestionId 问题ID
	 * @param sendPoint 积分
	 * @param id 回答ID
	 * @param userId 回答用户ID
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ResultBean adoptQuestion(String linkId, String linkType,
			String qaQuestionId, String sendPoint, String id, String userId) {
		ResultBean result = new ResultBean();
		QaQuestionEntity qa = new QaQuestionEntity();
		qa.setLinkId(linkId);
		qa.setLinkType(linkType);
		qa.setUserId(WebContext.getSessionUserId());
		qa.setId(qaQuestionId);
		List<QaQuestionEntity> list =commonDao.searchList(qa, SQL_SEARCH_QUESTION_QA);
		QaQuestionEntity qs=null;
		if(list.size()>0){
			qs=list.get(0);
		}else{
			result.setMessages("非法操作").setStatus(false);
			return result;
		}
		if(StringUtil.isNotEmpty(qs.getAdoptAnswerId())){
			result.setMessages("非法操作").setStatus(false);
			return result;
		}
		QaAnswerEntity qas=new QaAnswerEntity();
		qas.setId(id);
		qas.setUserId(WebContext.getSessionUserId());
		int count=commonDao.searchCount(qas, SQL_SEARCH_COUNT_BY_ID);
		if(count>0){
			result.setMessages("非法操作").setStatus(false);
			return result;
		}
		qa.setAdoptAnswerId(id);
		qa.setFlagBest("0");
		// 给用户增加积分
		pointService.earnPoint(PointConstants.SYSTEM_CODE_HS, userId, "D0005",
				qs.getSendPoint(), null, null);
		commonDao.updateData(qa, UPDATE_ADOPT);
		return result;
	}
}
