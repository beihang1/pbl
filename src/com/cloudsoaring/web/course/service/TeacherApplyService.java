package com.cloudsoaring.web.course.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.exception.BusinessException;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.TagEntity;
import com.cloudsoaring.web.bus.service.MessageService;
import com.cloudsoaring.web.common.service.BaseService;
import com.cloudsoaring.web.common.utils.ConfigUtil;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.TagLinkEntity;
import com.cloudsoaring.web.course.entity.TeacherApplyEntity;
import com.cloudsoaring.web.trainingplatform.constants.TpDbConstants;
import com.cloudsoaring.web.trainingplatform.entity.TopicEntity;
import com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity;
import com.cloudsoaring.web.trainingplatform.service.UserManagementService;

/***
 * 课程模块相关的业务处理Service
 * 
 * @author LILIANG
 *
 */
@Service
public class TeacherApplyService extends BaseService implements CourseConstants{

	/**消息处理Service*/
	@Autowired
	private MessageService messageService;
	
	@Autowired
	private UserManagementService userManagementService;
	
	/**
	 * 保存应聘讲师提交信息
	 * @author LILIANG
	 * @param TeacherApplyEntity
	 * @return
	 */
	public ResultBean saveApplyTeacher(TeacherApplyEntity teacherApplyEntity){
		ResultBean result = new ResultBean();	
		// 判断用户是否登录
		assertNotGuest();
		 
		if (teacherApplyEntity.getTags() == null || teacherApplyEntity.getTags() == "" ) {	 
			throw new BusinessException("保存失败,请选择标签！");
		}
		
		teacherApplyEntity.setId(IDGenerator.genUID());	
		teacherApplyEntity.setUserId(WebContext.getSessionUserId());
		teacherApplyEntity.setStatus(TEACHERAPPLY_NOTAPPROVE);
		commonDao.insertData(teacherApplyEntity, INSERT_TEACHER_APPLY);
		String[] tag = StringUtil.split(teacherApplyEntity.getTags(), ",");
		List<String> tagList = new ArrayList<String>();
		for (String str : tag){
			tagList.add(str);
		}		
		for(int i=0;i<tagList.size();i++){
			TagLinkEntity tagLinkEntity = new TagLinkEntity();
			tagLinkEntity.setLinkId(teacherApplyEntity.getId());
			tagLinkEntity.setTagId(tagList.get(i));
			tagLinkEntity.setLinkType(TEACHERAPPLY_LINKTYPE);
			commonDao.insertData(tagLinkEntity, INSERT_TAG_LINK);
		}
		String[] topic = StringUtil.split(teacherApplyEntity.getTopicContents(), ",");
		List<String>  topics=new ArrayList<String>();
		for (String str : topic){
		    topics.add(str);
        }
		for(int i=0;i<topics.size();i++){
		    TopicEntity to=new TopicEntity();
		    to.setTeacherId(WebContext.getSessionUserId());
		    to.setId(IDGenerator.genUID());  
		    to.setTopicContent(topics.get(i));
		    commonDao.insertData(to, TpDbConstants.SQL_INSERT_TOPIC);
		}
		return result;		
	}
	
	/**
	 * 检索该教师审批状态
	 * @author LILIANG
	 * @param TeacherApplyEntity
	 * @return 判断用户名是否存在 true存在/false不存在
	 */
	@SuppressWarnings("unchecked")
    public boolean searchTeacherApplyStatus(TeacherApplyEntity teacherApplyEntity){
		teacherApplyEntity.setUserId(WebContext.getSessionUserId());
		TeacherApplyEntity teacherApply = commonDao.searchOneData(teacherApplyEntity, SEARCH_TEACHER_APPLY_STATUS);
		UserRoleEntity role = new UserRoleEntity();
		role.setUserId(WebContext.getSessionUserId());
		List<UserRoleEntity> userRole = commonDao.searchList(role, SEARCH_USER_ROLE);
		boolean flag = false;
		for(int i=0;i<userRole.size();i++){
			if(ROLE_TEACHER_ROLE.equals(userRole.get(i).getRoleId())){
				flag = true;
				break;
			}
		}
		if(teacherApply == null && flag == true){
			return true;
		}else if(teacherApply != null){
			if((TEACHERAPPLY_NOTAPPROVE).equals(teacherApply.getStatus()) || flag == true){
				return true;
			}
		}				
		return false;
	}
	
	/**
	 * 后台-聘请讲师审批管理-列表分页检索
	 * @author LILIANG
	 * @param TeacherApplyEntity 类型：TeacherApplyEntity
	 * @return 检索所有的教师信息
	 */
	public ResultBean search(TeacherApplyEntity teacherApplyEntity){		
		return commonDao.searchList4Page(teacherApplyEntity, SEARCH_TEACHER_APPLY_ALL);
	}
	
	/**
	 * 后台-聘请讲师审批管理-查看页面
	 * @author LILIANG
	 * @param id 类型：TeacherApplyEntity
	 * @return TeacherApplyEntity
	 */
	@SuppressWarnings({ "rawtypes" })
    public TeacherApplyEntity searchDetail(String id,String userId){
		TeacherApplyEntity teacherApplyEntity = new TeacherApplyEntity();		
		teacherApplyEntity =  commonDao.searchOneData(id, SEARCH_TEACHER_APPLY_BY_PK);
		List tagNameList = commonDao.searchList(id, SEARCH_TAG_NAME_BY_TAG_LINK);
		String tagName = (String) tagNameList.get(0);
		for(int i=1;i<tagNameList.size();i++){
			tagName = tagName + " , " + tagNameList.get(i);
		}
		TopicEntity ttEntity=new TopicEntity();
		ttEntity.setTeacherId(userId);
		List topics=commonDao.searchList(ttEntity,TpDbConstants.SQL_SEARCH_TOPIC_BY_USER);
		if(topics.size()>0){
		    String topicContent = (String) topics.get(0);
	        for(int i=1;i<topics.size();i++){
	            topicContent = topicContent + " , " + topics.get(i);
	        }
	        teacherApplyEntity.setTopicContents(topicContent);
		}
		teacherApplyEntity.setTagNames(tagName);
		
 		return teacherApplyEntity;		
	}
	
	/**
	 * 后台-聘请讲师审批管理-审批同意
	 * @author LILIANG
	 * @param id 类型：TeacherApplyEntity
	 * @return  
	 */
	@SuppressWarnings("unchecked")
	public ResultBean saveAgreeApprove(TeacherApplyEntity teacherApplyEntity){		
		ResultBean result = new ResultBean();
		TeacherApplyEntity teacherApply =  commonDao.searchOneData(teacherApplyEntity.getId(), SEARCH_TEACHER_APPLY_BY_PK);
		List<TagLinkEntity> tagIds = commonDao.searchList(teacherApplyEntity.getId(), SEARCH_TAG_LINK_ALL_LINKID);
		for(int i=0;i<tagIds.size();i++){
			TagEntity tag = commonDao.searchOneData(tagIds.get(i).getTagId().toString(), SEARCH_TAG_BY_PK);
			TagLinkEntity tagLinkEntity = new TagLinkEntity();
			tagLinkEntity.setLinkId(teacherApply.getUserId());
			tagLinkEntity.setTagId((String) tagIds.get(i).getTagId());
			tagLinkEntity.setParentTagId(tag.getParentId());
			tagLinkEntity.setLinkType(LINK_TYPE_TEACHER);
			commonDao.insertData(tagLinkEntity, INSERT_TAG_LINK);
		}
		
		//认证为老师
		userManagementService.teacherAccount(teacherApply.getUserId());
		
		//更新teacherApply表
		teacherApplyEntity.setApplyUser(WebContext.getSessionUserId());
		teacherApplyEntity.setStatus(TEACHERAPPLY_AGREE);
		update(teacherApplyEntity, UPDATE_TEACHER_APPLY_APPROVE);
		
		//添加社交教师表
		messageService.sendSystemMessage( WebContext.getSessionUserId(), MSG_PAY_SUCCESS);
		return result;
	}
	
	/**
	 * 后台-聘请讲师审批管理-审批不同意
	 * @author LILIANG
	 * @param TeacherApplyEntity
	 * @return 
	 */
	public ResultBean saveDisAgreeApprove(TeacherApplyEntity teacherApplyEntity){		
		ResultBean result = new ResultBean();	
		teacherApplyEntity.setApplyUser(WebContext.getSessionUserId());
		teacherApplyEntity.setStatus(TEACHERAPPLY_DISAGREE);
		update(teacherApplyEntity, UPDATE_TEACHER_APPLY_APPROVE);
		messageService.sendSystemMessage( WebContext.getSessionUserId(), MSG_PAY_SUCCESS);
		return result;
	}
}
