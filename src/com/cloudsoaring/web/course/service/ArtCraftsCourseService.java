package com.cloudsoaring.web.course.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;
import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.common.constant.Constants;
import com.cloudsoaring.web.common.service.BaseService;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.CourseExperimentalResultEntity;
import com.cloudsoaring.web.course.entity.SysUserEntity;
import com.cloudsoaring.web.course.entity.UserLogEntity;

import net.sf.json.JSONObject;

/***
 * 显微镜课程数据处理Service
 * @author fanglili
 */
@Service
public class ArtCraftsCourseService extends BaseService implements CourseConstants,Constants{
	/**
	 * 保存显微镜操作信息
	 * @param entity
	 */
	public String insertExperimentalResult(CourseExperimentalResultEntity entity) {
		entity.setId(IDGenerator.genUID());
		/**1.根据步骤ID设置步骤内容*/
		if(entity.getFirstStepId() != null && entity.getFirstStepId().equals("1")) {
			entity.setFirstStepName("模拟制作");
			if(entity.getSecondStepId() != null && entity.getSecondStepId().equals("11")) {
				entity.setSecondStepName("拼接组合");
			}
		}
		entity.setCourseID("d1945029-2eb4-4f0c-9873-5e39c2d1649b");
		entity.setChapterId("8c3490e7-e4ec-4312-9402-e6ea6d51a964");
		String userName = commonDao.searchOneData(entity.getUserId(), ER_SELECT_GETUSERNAME);
		entity.setUserName(userName);
		entity.setCreateTime(new Date());
		return commonDao.insertData(entity, INSERT_AC).toString();
	}
	

	/**
	 * 获取所有人员步骤得分情况
	 * @param null
	 * @return List<CourseExperimentalResultEntity>
	 */
	@SuppressWarnings("unchecked")
	public List<CourseExperimentalResultEntity> getUserStepScore(){
		return commonDao.searchList(null, ER_SELECT_USERSTEPSCORE);
	}
	
	/**
	 * 获取所有人员所有步骤平均分情况
	 * @param null
	 * @return List<CourseExperimentalResultEntity>
	 */
	@SuppressWarnings("unchecked")
	public List<CourseExperimentalResultEntity> getUserAllStepScore(){
		/**
		 * 根据登录用户显示结果列表
		 * 1. 若为课程老师或者系统管理员，显示所有同学信息信息
		 * 2. 若为同学仅显示自己的成绩信息
		 * */
		String loginUserId = WebContext.getSessionUserId();
		String courseTeacherId = commonDao.searchOneData(null, AC_SELECT_COURSE_TEACHER);
		if(loginUserId.equals(courseTeacherId) || loginUserId.equals("admin")) {
			return commonDao.searchList(null, AC_SELECT_USERALLSTEPSCORE);
		}else {
			CourseExperimentalResultEntity entity = new CourseExperimentalResultEntity();
			entity.setUserId(loginUserId);
			return commonDao.searchList(entity, AC_SELECT_USERSTEPSCORE);
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public List<SysUserEntity> getSysUser(){
		String loginUserId = WebContext.getSessionUserId();
		if(loginUserId.equals("506380c0-62fa-4352-a226-1803a0838759") || loginUserId.equals("admin")) {
			SysUserEntity entity = new SysUserEntity();
			return commonDao.searchList(entity, SELECT_SYS_USER);
		}else {
			SysUserEntity entity = new SysUserEntity();
			entity.setUserId(loginUserId);
			return commonDao.searchList(entity, SELECT_SYS_USER_BY_LOGIN);
		}
	}
	
	
	public List<UserLogEntity> getUserLog(){
		String loginUserId = WebContext.getSessionUserId();
		if(loginUserId.equals("506380c0-62fa-4352-a226-1803a0838759") || loginUserId.equals("admin")) {
			SysUserEntity entity = new SysUserEntity();
			return commonDao.searchList(entity, SELECT_USER_LOG);
		}else {
			UserLogEntity entity = new UserLogEntity();
			entity.setUserId(loginUserId);
			return commonDao.searchList(entity, SELECT_USER_BY_LOGIN);
		}
	}
	/**
	 * 获取近10次成绩折线图
	 * @param null
	 * @return List<CourseExperimentalResultEntity>
	 */
	@SuppressWarnings("unchecked")
	public List<CourseExperimentalResultEntity> getUserTenTimesScore(String userId){
		CourseExperimentalResultEntity entity = new CourseExperimentalResultEntity();
		entity.setUserId(userId);
		return commonDao.searchList(entity, ER_SELECT_USERTENTIMESCORE);
	}
	
	/**
	 * 获取操作详情
	 * @param null
	 * @return List<CourseExperimentalResultEntity>
	 */
	@SuppressWarnings("unchecked")
	public List<CourseExperimentalResultEntity> getUserOperationDetail(String userId, Date enterTime){
		CourseExperimentalResultEntity entity = new CourseExperimentalResultEntity();
		entity.setUserId(userId);
		entity.setEnterTime(enterTime);
		return commonDao.searchList(entity, ER_SELECT_USEROPERATIONDETAIL);
	}
	
}
