package com.cloudsoaring.web.course.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.TagEntity;
import com.cloudsoaring.web.common.service.BaseService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.constant.CourseDbConstants;
import com.cloudsoaring.web.course.constant.CourseStateConstants;
import com.cloudsoaring.web.course.view.TagView;
import com.cloudsoaring.web.institution.entity.InstitutionEntity;
import com.cloudsoaring.web.institution.entity.TagInstitutionEntity;
import com.cloudsoaring.web.taxonomy.entity.TagEntityView;
import com.cloudsoaring.web.taxonomy.entity.TagInstitutionEntityView;
import com.cloudsoaring.web.taxonomy.entity.TagInstitutionView;
import com.cloudsoaring.web.trainingplatform.entity.TpUserEntity;

@Service
public class TagService extends BaseService implements CourseConstants {

	/***
	 * 标签查询
	 * 
	 * @param flag 0查询标签 1不查询标签
	 * 		businessType 业务类型
	 * @return ResultBean
	 */
	@SuppressWarnings("unchecked")
	public ResultBean searchTag(String flag,String businessType ) {
		ResultBean result = new ResultBean();
		TagView tew = new TagView();
		tew.setBusinessType(StringUtil.getOrElse(businessType, TAG_BUSSINESS_TYPE_COURSE));				
		List<TagView> tagTypeList = commonDao.searchList(tew,
				SEARCH_TAG_TYPE_LIST);
		TpUserEntity tpUserEntity = new TpUserEntity();
		tpUserEntity.setUserId(WebContext.getSessionUserId());
		String schoolName="";
		TpUserEntity userEntity= commonDao.searchOneData(tpUserEntity, USER_SEARCH_BY_USERID_SCHOOL);
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getSchoolName())) {
			schoolName = userEntity.getSchoolName();
		}
		TagEntityView tagEntityView = new TagEntityView();
		tagEntityView.setSchoolName(schoolName);
		/*List<TagEntity> listAll = commonDao.searchList(tagEntityView,
				SEARCH_TAG_LIST);*/
		List<TagEntity> listAll = commonDao.searchList(tagEntityView,
				SEARCH_TAG_FIRST_LIST);
		/*		List<TagEntity> listAll = commonDao.searchList(null,
						SEARCH_TAG_FIRST_LIST);*/
		Map<String, List<TagEntity>> map = new HashMap<String, List<TagEntity>>();
		if (StringUtils.isEmpty(flag)) {
			flag = MSG_E_TAG_FLAG;
		}
		if (flag.equals(MSG_E_TAG_FLAG)) {
			if (tagTypeList != null && tagTypeList.size() != 0) {
				if (listAll != null && listAll.size() != 0) {
					for (TagEntity tt : listAll) {
						String key = tt.getTagTypeId();
						if (map.containsKey(key)) {
							List<TagEntity> list = map.get(key);
							list.add(tt);
						} else {
							List<TagEntity> list = new ArrayList<TagEntity>();
							list.add(tt);
							map.put(key, list);
						}

					}
				}
				for (TagView tagType : tagTypeList) {
					String key = tagType.getTagTypeId();
					if (map.containsKey(key)) {
						List<TagEntity> list = map.get(key);
						tagType.setList(list);
					}
				}
			}
		}
		result.setData(tagTypeList);
		return result;
	}

	/***
	 * 标签查询
	 * 
	 * @param flag 0查询标签 1不查询标签
	 * 		businessType 业务类型
	 * @return ResultBean
	 */
	@SuppressWarnings("unchecked")
	public ResultBean searchTag2(String flag,String businessType ,String schoolName,String shareFlag) {
		ResultBean result = new ResultBean();
		TagView tew = new TagView();
		tew.setBusinessType(StringUtil.getOrElse(businessType, TAG_BUSSINESS_TYPE_PLAN));	
		tew.setTagTypeId("3");
		List<TagView> tagTypeList = commonDao.searchList(tew,
				SEARCH_TAG_TYPE_LIST);
		TagEntityView tagEntityView = new TagEntityView();
		
		List<TagEntity> listAll = new ArrayList<TagEntity>();
		if("1".equals(shareFlag) || "2".equals(shareFlag) || "3".equals(shareFlag)) {
			tagEntityView.setShareFlag(shareFlag);
			listAll = commonDao.searchList(tagEntityView,
					SEARCH_TAG_FLAG_LIST);
		}else {
			tagEntityView.setSchoolName(schoolName);
			listAll = commonDao.searchList(tagEntityView,
					SEARCH_TAG_LIST);
		}
		
		Map<String, List<TagEntity>> map = new HashMap<String, List<TagEntity>>();
		if (StringUtils.isEmpty(flag)) {
			flag = MSG_E_TAG_FLAG;
		}
		if (flag.equals(MSG_E_TAG_FLAG)) {
			if (tagTypeList != null && tagTypeList.size() != 0) {
				if (listAll != null && listAll.size() != 0) {
					for (TagEntity tt : listAll) {
						String key = tt.getTagTypeId();
						if (map.containsKey(key)) {
							List<TagEntity> list = map.get(key);
							list.add(tt);
						} else {
							List<TagEntity> list = new ArrayList<TagEntity>();
							list.add(tt);
							map.put(key, list);
						}

					}
				}
				for (TagView tagType : tagTypeList) {
					String key = tagType.getTagTypeId();
					if (map.containsKey(key)) {
						List<TagEntity> list = map.get(key);
						tagType.setList(list);
					}
				}
			}
		}
		result.setData(tagTypeList);
		return result;
	}
	
	/**
	 * 标签查询
	 * 
	 * @param tagTypeId
	 * @param parentTagId
	 * @return ResultBean
	 */
	@SuppressWarnings("unchecked")
	public ResultBean searchSubCategory(String tagTypeId, String parentTagId) {
		ResultBean result = new ResultBean();
		if (StringUtils.isEmpty(tagTypeId)) {
			return result.setStatus(false).setMessages(
					getMessage(MSG_E_TAG_ERROR));
		}
		
		String[] tagIds = StringUtil.split(parentTagId, ",");		
		Map<String, Object> param = new HashMap<String, Object>();
		if(tagIds != null && tagIds.length>0){
			param.put("parentId", tagIds);
		}else{
			param.put("parentId", null);
		}
		
		param.put("tagTypeId", tagTypeId);
		List<TagEntity> list = commonDao.searchList(param,
				SEARCH_TAG_LIST_BY);		
		result.setData(list);
		return result;
	}

	/**
	 * 根据标签类的业务类型检索该业务类型（课程、计划等）下的标签
	 * 
	 * @param bussinessType
	 *            标签类-业务类型
	 * @return ResultBean对象，数据：List<TagView>
	 */
	@SuppressWarnings("unchecked")
	public ResultBean searchTag(String bussinessType) {
		assertNotEmpty(bussinessType, MSG_E_EMPTY_BUSSINESS_TYPE);
		// 检索计划相关的标签类及子标签
		TagView tag = new TagView();
		tag.setBusinessType(bussinessType);
		tag.setFlagDel(CourseStateConstants.FLAG_UNDEL);
		List<TagView> list = commonDao
				.searchList(
						tag,
						CourseDbConstants.TAG_TYPE_AND_SUB_TAG_SEARCH_BY_BUSSINESS_TYPE);
		ResultBean result = new ResultBean();
		result.setStatus(true);
		result.setData(list);
		return result;
	}

	/**
	 * 根据链接ID，标签类ID，父标签ID检索标签列表
	 * 
	 * @param linkId
	 *            链接ID，
	 * @param tagTypeId
	 *            标签类ID
	 * @param parentId
	 *            父标签ID
	 * @return List<TagEntity> 标签集合
	 */
	@SuppressWarnings("unchecked")
	public List<TagEntity> searchTagByLinkId(String linkId, String tagTypeId,
			String parentId) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("linkId", linkId);
		map.put("tagTypeId", tagTypeId);
		map.put("parentId", parentId);
		return commonDao.searchList(map, SEARCH_TAG_BY_LINK_ID);
	}
	
	/**
	 * 检索全部的标签
	 * @author LILIANG
	 *
	 */
	@SuppressWarnings("unchecked")
	public List<TagEntity> searchTagAll() {
		TagEntity tag = new TagEntity();		
		List<TagEntity> tags = commonDao.searchList(tag, SEARCH_TAGALL);
		return tags;		
	}	
	
	/**
	 * 标签一览手机端
	 * @param 
	 * @return ResultBean
	 */
	public ResultBean searchCateTagList(TagEntityView tagEntity){
		if (StringUtil.isEmpty(tagEntity.getBusinessType())) {
			tagEntity.setBusinessType(CourseStateConstants.TAG_BUSSINESS_TYPE_COURSE);
		}
		tagEntity.setSortName("createDate");
		tagEntity.setSortOrder("desc");
		return commonDao.searchList4Page(tagEntity, SEARCH_TAG_LIST_BYTYPE);
		
	}

	public ResultBean searchSchhoolCategory2(String flag, String businessType) {
		ResultBean result = new ResultBean();
		TagInstitutionView tew = new TagInstitutionView();
		tew.setBusinessType(StringUtil.getOrElse(businessType, TAG_SCHOOL_TYPE_PLAN));				
		List<TagInstitutionView> tagTypeList = commonDao.searchList(tew,
				SCHOOL_TAG_TYPE_LIST);
		List<TagInstitutionEntity> listAll = commonDao.searchList(null,
				SCHOOL_TAG_LIST);
		Map<String, List<TagInstitutionEntity>> map = new HashMap<String, List<TagInstitutionEntity>>();
		if (StringUtils.isEmpty(flag)) {
			flag = MSG_E_TAG_FLAG;
		}
		if (flag.equals(MSG_E_TAG_FLAG)) {
			if (tagTypeList != null && tagTypeList.size() != 0) {
				if (listAll != null && listAll.size() != 0) {
					for (TagInstitutionEntity tt : listAll) {
						String key = tt.getTagTypeId();
						if (map.containsKey(key)) {
							List<TagInstitutionEntity> list = map.get(key);
							list.add(tt);
						} else {
							List<TagInstitutionEntity> list = new ArrayList<TagInstitutionEntity>();
							list.add(tt);
							map.put(key, list);
						}

					}
				}
				for (TagInstitutionView tagType : tagTypeList) {
					String key = tagType.getTagTypeId();
					if (map.containsKey(key)) {
						List<TagInstitutionEntity> list = map.get(key);
						tagType.setList(list);
					}
				}
			}
		}
		result.setData(tagTypeList);
		return result;
	}

	public ResultBean searchSchhoolCategory(String flag, String businessType) {
		ResultBean result = new ResultBean();
		TagInstitutionView tew = new TagInstitutionView();
		tew.setBusinessType(StringUtil.getOrElse(businessType, TAG_SCHOOL_TYPE_PLAN));				
		List<TagInstitutionView> tagTypeList = commonDao.searchList(tew,
				SCHOOL_TAG_TYPE_LIST);
		List<TagInstitutionEntity> listAll = commonDao.searchList(null,
				SCHOOL_TAG_LIST);
		Map<String, List<TagInstitutionEntity>> map = new HashMap<String, List<TagInstitutionEntity>>();
		if (StringUtils.isEmpty(flag)) {
			flag = MSG_E_TAG_FLAG;
		}
		if (flag.equals(MSG_E_TAG_FLAG)) {
			if (tagTypeList != null && tagTypeList.size() != 0) {
				if (listAll != null && listAll.size() != 0) {
					for (TagInstitutionEntity tt : listAll) {
						String key = tt.getTagTypeId();
						if (map.containsKey(key)) {
							List<TagInstitutionEntity> list = map.get(key);
							list.add(tt);
						} else {
							List<TagInstitutionEntity> list = new ArrayList<TagInstitutionEntity>();
							list.add(tt);
							map.put(key, list);
						}

					}
				}
				for (TagInstitutionView tagType : tagTypeList) {
					String key = tagType.getTagTypeId();
					if (map.containsKey(key)) {
						List<TagInstitutionEntity> list = map.get(key);
						tagType.setList(list);
					}
				}
			}
		}
		result.setData(tagTypeList);
		return result;
	}

	public ResultBean searchSchhoolCategory3(String flag, String businessType) {
		ResultBean result = new ResultBean();
		TagInstitutionView tew = new TagInstitutionView();
		tew.setBusinessType(StringUtil.getOrElse(businessType, TAG_SCHOOL_TYPE_PLAN));	
		//只获取一级标签
		tew.setTagTypeId("3");
		List<TagInstitutionView> tagTypeList = commonDao.searchList(tew,
				SCHOOL_TAG_TYPE_LIST);
		List<TagInstitutionEntity> listAll = commonDao.searchList(null,
				SCHOOL_PBL_TAG_LIST);
		Map<String, List<TagInstitutionEntity>> map = new HashMap<String, List<TagInstitutionEntity>>();
		if (StringUtils.isEmpty(flag)) {
			flag = MSG_E_TAG_FLAG;
		}
		if (flag.equals(MSG_E_TAG_FLAG)) {
			if (tagTypeList != null && tagTypeList.size() != 0) {
				if (listAll != null && listAll.size() != 0) {
					for (TagInstitutionEntity tt : listAll) {
						String key = tt.getTagTypeId();
						if (map.containsKey(key)) {
							List<TagInstitutionEntity> list = map.get(key);
							list.add(tt);
						} else {
							List<TagInstitutionEntity> list = new ArrayList<TagInstitutionEntity>();
							list.add(tt);
							map.put(key, list);
						}

					}
				}
				for (TagInstitutionView tagType : tagTypeList) {
					String key = tagType.getTagTypeId();
					if (map.containsKey(key)) {
						List<TagInstitutionEntity> list = map.get(key);
						tagType.setList(list);
					}
				}
			}
		}
		result.setData(tagTypeList);
		return result;
	}
	@SuppressWarnings("unchecked")
	public TagEntity searchOneTag(String tagId) {
		TagEntity tag = new TagEntity();	
		tag.setTagId(tagId);
		TagEntity tags = commonDao.searchOneData(tag, SEARCH_TAG_NAME_BY_TAG_ID);
		return tags;		
	}
}
