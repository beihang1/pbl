package com.cloudsoaring.web.course.service;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.stream.FileImageInputStream;

import org.apache.commons.io.IOUtils;
import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.common.excel.BusinessHandler;
import com.cloudsoaring.common.excel.ExcelImportResult;
import com.cloudsoaring.common.excel.ExcelImportUtil;
import com.cloudsoaring.common.excel.RowValidatedResult;
import com.cloudsoaring.common.multimedia.ThumbnailUtil;
import com.cloudsoaring.common.utils.BigDecimalUtil;
import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.exception.BusinessException;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.FileEntity;
import com.cloudsoaring.web.bus.entity.TagEntity;
import com.cloudsoaring.web.bus.entity.UserEntity;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.bus.service.MessageService;
import com.cloudsoaring.web.common.constant.Constants;
import com.cloudsoaring.web.common.utils.EntityUtil;
import com.cloudsoaring.web.common.utils.FileUtil;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.constant.CourseDbConstants;
import com.cloudsoaring.web.course.constant.CourseStateConstants;
import com.cloudsoaring.web.course.controller.PropertyUtil;
import com.cloudsoaring.web.course.entity.ArchivesDiscussEntity;
import com.cloudsoaring.web.course.entity.ArchivesEntity;
import com.cloudsoaring.web.course.entity.ChapterActiveEntity;
import com.cloudsoaring.web.course.entity.ChapterEntity;
import com.cloudsoaring.web.course.entity.CourseApproveEntity;
import com.cloudsoaring.web.course.entity.CourseAssistTeacherEntity;
import com.cloudsoaring.web.course.entity.CourseEntity;
import com.cloudsoaring.web.course.entity.CourseMessageEntity;
import com.cloudsoaring.web.course.entity.CourseTeamEntity;
import com.cloudsoaring.web.course.entity.CourseTeamUserEntity;
import com.cloudsoaring.web.course.entity.CourseUserEvalutionEntity;
import com.cloudsoaring.web.course.entity.DiscussEntity;
import com.cloudsoaring.web.course.entity.HomeworkAnswerEntity;
import com.cloudsoaring.web.course.entity.HomeworkAnswerQuestionEntity;
import com.cloudsoaring.web.course.entity.HomeworkEntity;
import com.cloudsoaring.web.course.entity.MeasurementEntity;
import com.cloudsoaring.web.course.entity.NoteEntity;
import com.cloudsoaring.web.course.entity.PlanCourseEntity;
import com.cloudsoaring.web.course.entity.QaQuestionEntity;
import com.cloudsoaring.web.course.entity.QuestionEntity;
import com.cloudsoaring.web.course.entity.QuestionOptionEntity;
import com.cloudsoaring.web.course.entity.ScoreDataEntity;
import com.cloudsoaring.web.course.entity.ScoreEntity;
import com.cloudsoaring.web.course.entity.SysUserEntity;
import com.cloudsoaring.web.course.entity.TagLinkEntity;
import com.cloudsoaring.web.course.entity.TeacherApplyEntity;
import com.cloudsoaring.web.course.entity.UserCourseEntity;
import com.cloudsoaring.web.course.entity.UserInfoEntity;
import com.cloudsoaring.web.course.entity.UserLogEntity;
import com.cloudsoaring.web.course.entity.UserMeasurementResultEntity;
import com.cloudsoaring.web.course.entity.UserOperEntity;
import com.cloudsoaring.web.course.view.ChapterView;
import com.cloudsoaring.web.course.view.CourseManagerView;
import com.cloudsoaring.web.course.view.CourseView;
import com.cloudsoaring.web.course.view.HomeworkView;
import com.cloudsoaring.web.course.view.ScoreDataView;
import com.cloudsoaring.web.course.view.TagView;
import com.cloudsoaring.web.course.view.TeacherView;
import com.cloudsoaring.web.course.view.UserView;
import com.cloudsoaring.web.institution.entity.InstitutionEntity;
import com.cloudsoaring.web.pointmanager.constant.PointStateConstants;
import com.cloudsoaring.web.pointmanager.service.PointManagerService;
import com.cloudsoaring.web.points.constant.PointConstants;
import com.cloudsoaring.web.points.constant.PointDbConstants;
import com.cloudsoaring.web.points.entity.PointTypeEntity;
import com.cloudsoaring.web.points.entity.UserPoint;
import com.cloudsoaring.web.points.entity.UserPointEntity;
import com.cloudsoaring.web.points.service.PointService;
import com.cloudsoaring.web.taxonomy.entity.TagInstitutionView;
import com.cloudsoaring.web.trainingplatform.constants.TpDbConstants;
import com.cloudsoaring.web.trainingplatform.entity.TpUserEntity;
import com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity;
import com.cloudsoaring.web.trainingplatform.utils.TpConfigUtil;

/***
 * 模块相关的业务处理Service
 * 
 * @author liuyanshuang
 *
 */
@Service
@SuppressWarnings("unchecked")
public class CourseService extends FileService implements CourseConstants {

	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	@Autowired
	private ChapterService chapterService;
	@Autowired
	private PointService pointService;
	@Autowired
	private PointManagerService pointManagerService;
	@Autowired
	private FileService fileService;
	/** 消息处理Service */
	@Autowired
	private MessageService messageService;

	/**
	 * 根据userId获取课程列表信息
	 * @param userId
	 * @return List<CourseEntity>
	 */
	public List<CourseEntity> getCourseListByUserId(String userId) {
		CourseEntity entity = new CourseEntity();
		entity.setCreateUser(userId);
		return commonDao.searchList(entity, SEARCH_COURSE);
	}
	 
	/**
	 * 根据userId获取个人档案列表
	 * @param userId
	 * @return List<ArchivesEntity>
	 */
	public List<ArchivesEntity> getArchivesList(String userId){
		List<ArchivesEntity> archivesList = new ArrayList<ArchivesEntity>();
		ArchivesEntity entity = new ArchivesEntity();
		entity.setUserId(userId);
		List<ArchivesEntity> list = commonDao.searchList(entity, ARCHIVES_SELECT);
		if(list.size() > 0) {
			for(ArchivesEntity archives : list) {
				ArchivesDiscussEntity discuss = new ArchivesDiscussEntity();
				discuss.setArchivesId(archives.getId());
				List<ArchivesDiscussEntity> discussList = commonDao.searchList(discuss, ARCHIVES_DISCUSS_SELECT);
				archives.setDiscussList(discussList);
				archivesList.add(archives);
			}
		}
		return archivesList;
	}
	
	/**
	 * 保存个人档案评论
	 * @param entity
	 * @return ResultBean
	 */
	public ResultBean saveDiscuss(ArchivesDiscussEntity entity) {
		ResultBean result = new ResultBean();
		entity.setUserId(WebContext.getSessionUserId());
		entity.setArchivesId(entity.getId());
		entity.setId(IDGenerator.genUID());
		entity.setCreateTime(new Date());
		commonDao.insertData(entity, ARCHIVES_DISCUSS_INSERT);
		result.setStatus(true);
		return result;
	}
	
	/**
	 * 根据userId获取用户已经加入的课程列表
	 * @param userId
	 * @return List<UserCourseEntity>
	 */
	public List<UserCourseEntity> getUCListByUserId(String userId) {
		UserCourseEntity entity = new UserCourseEntity();
		entity.setUserId(userId);
		return commonDao.searchList(entity, SELECT_USER_COURSE_LISTS);
	}
	
	/**
	 * 课程添加学员
	 * @param courseId
	 * @param userIds
	 */
	public void saveUser(String courseId, String... userIds) {
		if (userIds != null) {
			UserCourseEntity user = new UserCourseEntity();
			for (String userId : userIds) {
				if (StringUtil.isNotEmpty(userId) && StringUtil.isNotEmpty(courseId)) {
					if (isExitStudent(userId, courseId)) {
						throw new BusinessException("选择的学员中有已经参加了此课程！");
					}
					user.setCourseId(courseId);
					user.setUserId(userId);
					user.setJoinCourse(BigDecimal.ONE);
					user.setJoinType(BigDecimal.ZERO);
					commonDao.insertData(user, SQL_USER_COURSE_INSERT);
				} else {
					throw new BusinessException("学员添加失败");
				}
			}
		}
	}
	
	/**
	 * 保存个人档案信息
	 * @param archives
	 */
	public void saveArchives(ArchivesEntity archives) {
		archives.setId(IDGenerator.genUID());
		archives.setUserId(WebContext.getSessionUserId());
		archives.setUserName(WebContext.getSessionUser().getPersonName());
		archives.setCreateDate(new Date());
		//todo 根据content判断类型
		if(archives.getContent().indexOf(".mp4") > 0) {
			String fileId = archives.getContent().substring(archives.getContent().indexOf("fileId=") + 7, archives.getContent().indexOf("&fileName"));
			/*String content = "<video controls=\"controls\">"
					+ "<source src=" + PropertyUtil.getProperty("res.context.root")
					+ fileService.getFile(fileId, "video").getPath() + " type=\"video/mp4\">" + "</video>";*/
			String content = archives.getContent().replaceAll("</?[^>]+>", "");
			archives.setContent(content);
			archives.setImageId(fileId);
			archives.setExitImage("2");
			archives.setInfo(sdf.format(new Date())+"，发布视频信息");
		}else if(archives.getContent().indexOf("file/image.action?fileId") > 0) {
			String fileId = archives.getContent().substring(archives.getContent().indexOf("fileId=") + 7, archives.getContent().indexOf("&fileName"));
			String content = archives.getContent().replaceAll("</?[^>]+>", "");
			archives.setContent(content);
			archives.setImageId(fileId);
			archives.setExitImage("1");
			archives.setInfo(sdf.format(new Date())+"，发布图片信息");
			
		}else {
			//String content = archives.getContent().substring(archives.getContent().indexOf("<p>")+3, archives.getContent().indexOf("</p>"));
			String content = archives.getContent().replaceAll("</?[^>]+>", "");
			//archives.setContent(content);
			archives.setExitImage("0");
			archives.setInfo(sdf.format(new Date())+"，发布文字信息"+content);
		}
		commonDao.insertData(archives, ARCHIVES_INSERT);
	}

	/**
	 * 根据userRole，userTeacherRole，loginUser保存用户角色
	 * @param userRole
	 * @param userTeacherRole
	 * @param loginUser
	 */
	public void saveUserAndRole(UserRoleEntity userRole,UserRoleEntity userTeacherRole,TpUserEntity loginUser) {
		UserEntity user = commonDao.searchOneData(loginUser, USER_SEARCH_NAME_BY_USERID);
		if(user == null) {
			//loginUser.setUserKbn("C");
			//commonDao.insertData(loginUser, USER_INSERT_OUT);
			commonDao.insertData(loginUser, TpDbConstants.USER_INSERT);
			if(userRole.getRoleId() != null) {
				commonDao.insertData(userRole, USER_ROLE_INSERT_OUT);
			}
			if(userTeacherRole.getRoleId() != null) {
				commonDao.insertData(userTeacherRole, USER_ROLE_INSERT_OUT);
			}
		}else {
			loginUser.setUserId(user.getUserId());
			commonDao.updateData(loginUser, TpDbConstants.USER_UPDATE);
		}
	}
	
	/**
	 * 根据userId获取用户角色列表
	 * @param userId
	 * @return List<UserRoleEntity>
	 */
	public List<UserRoleEntity> getRoleByUserId(String userId) {
		UserRoleEntity role = new UserRoleEntity();
		role.setUserId(userId);
		return commonDao.searchList(role, USER_ROLE_SELECT_BY_USERID);
	}
	
	/**
	 * 根据courseId，content保存课程消息
	 * @param courseId
	 * @param content
	 */
	public void saveMessage(String courseId, String content) {
		UserView view = new UserView();
		view.setCourseId(courseId);
		List<UserView> userList = commonDao.searchList(view, SQL_USER_SEARCH_JOIN);
		if(userList.size() > 0) {
			for(UserView user:userList) {
				CourseMessageEntity entity = new CourseMessageEntity();
				entity.setContent(content);
				entity.setCourseId(courseId);
				entity.setStudentId(user.getUserId());
				entity.setStuStatus("0");//0:未查看;1:已查看
				entity.setCreateTime(new Date());
				entity.setCreateUser(WebContext.getSessionUserId());
				entity.setId(IDGenerator.genUID());
				entity.setCheckStatus("-2");
				entity.setType("-1");
				commonDao.insertData(entity, COURSE_MESSAGE_INSERT);
			}
		}
	}
	
	/**
	 * 查询课工厂方法
	 * @param courseId 课程id
	 * @param content 课程内容
	 * @param type 课程类型
	 * @return
	 */
	public ResultBean factory(String courseId,String content,String type) {
		ResultBean result = new ResultBean();
		CourseApproveEntity entity = new CourseApproveEntity();
		//随机生成主键id
		entity.setId(IDGenerator.genUID());
		entity.setCourseId(courseId);
		//设置用户账号
		entity.setUserId(WebContext.getSessionUserId());
		entity.setContent(content);
		entity.setType(type);
		//设置审批状态
		entity.setApproveStatus(COURSE_END_STATUS_NOT);
		entity.setCreateTime(new Date());
		//保存
		commonDao.insertData(entity, COURSE_APPROVE_INSERT);
		result.setMessages(getMessage(MSG_S_SAVE));
		return result;
	}

	/**
	 * 根据id，shareFlag保存审批信息
	 * @param id
	 * @param shareFlag
	 * @throws Exception 
	 */
	public void factoryCourse(String id,String shareFlag) throws Exception {
		CourseManagerView query = new CourseManagerView();
		query.setId(id);
		// 课程信息
		CourseManagerView result = commonDao.searchOneData(query, SEARCH_COURSE_BY_ID);
		TagView tag = new TagView();
		tag.setLinkId(id);
		tag.setLinkType(LINK_TYPE_COURSE);
		// 课程标签信息
		List<TagView> tagList = commonDao.searchList(tag, CourseDbConstants.SQL_SEARCH_TAG_BY_COURSE_ID);
		if (tagList != null && tagList.size() > 0) {
			for (int i = 0; i < tagList.size(); i++) {
				if (tagList.get(i).getParentTagId() == null) {
					result.setFirstTag(tagList.get(i).getTagId());
				} else {
					result.setSecondTag(tagList.get(i).getTagId());
				}
			}
		}
		EntityUtil.copyEntity(result, query);
		query.setId(IDGenerator.genUID());
		query.setStatus(COURSE_END_STATUS_YES);
		query.setStartDate(sdf.format(new Date()));
		query.setEndDate("2099-12-31");
		query.setCrowdType("1");
		query.setTeacherId(result.getTeacherId());
		query.setEndFlag(query.getEndFlag());
		query.setLibraryFlag("0");
		query.setClassFlag("0");
		query.setShareFlag(shareFlag);
		 /** 1：精品课，2：课工场，3：公开课，10：课工场复制出来的课，11：新创建的私有课，20：图书馆，30：试听课*/
		if(shareFlag.equals(COURSE_SHARE_FLAG_ONE)) {
			query.setTitle(result.getTitle()+"_（精品课）");
		}else if(shareFlag.equals(COURSE_SHARE_FLAG_TWO)) {
			query.setTitle(result.getTitle()+"_（课工场）");
		}else if(shareFlag.equals(COURSE_SHARE_FLAG_THREE)) {
			query.setTitle(result.getTitle()+"_（公开课）");
		}
		query.setCourseLocation(result.getCourseLocation() == null ? "0" : result.getCourseLocation());
		
		FileEntity file = fileService.getFile(query.getPictureId(), "image");
		File picFile = new File(PropertyUtil.getProperty("res.storage.root")+file.getPath());
		FileInputStream is = new FileInputStream(picFile);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] buff = new byte[1024];
		int len = 0;
		while ((len = is.read(buff)) != -1) {
		    bos.write(buff, 0, len);
		}
		String newId = IDGenerator.genUID();
		// 得到图片的字节数组
		byte[] resultByte = bos.toByteArray();
		OutputStream os = new FileOutputStream(PropertyUtil.getProperty("res.storage.pic")+newId+".png");
		os.write(resultByte, 0, resultByte.length);
		os.flush();
		os.close();
		FileEntity newPic = new FileEntity();
		newPic.setContentType("png");
		newPic.setCreateUser(WebContext.getSessionUserId());
		newPic.setCreateDate(new Date());
		newPic.setFileId(newId);
		newPic.setFileName(newId + ".png");
		newPic.setFileSize(file.getFileSize());
		newPic.setPath("/image/pic/"+newId+".png");
		newPic.setType("image");
		fileService.save(newPic);
		query.setPictureId(newId);
		commonDao.insertData(query, COURSE_INSERT);
		joinUserCourse(query.getId());
		List<ChapterView> chapterList = chapterService.getChapterListByCourseId(id);
		//保存原有课程章节
		saveFactoryCourseChapter(chapterList,query.getId());
		// 新增分类信息
		if (StringUtil.isNotEmpty(query.getFirstTag())) {
			insertCatagorys(query.getId(), query.getFirstTag(), query.getSecondTag());
		}
	}
	
	/**
	 * 根据courseId获取课程信息列表
	 * @param courseId
	 * @return ResultBean
	 */
	public ResultBean getMessageListByCourseId(String courseId){
		ResultBean result = new ResultBean();
		CourseMessageEntity entity = new CourseMessageEntity();
		entity.setCourseId(courseId);
		List<CourseMessageEntity> list = commonDao.searchList(entity, COURSE_MESSAGE_SELECT_BY_COURSE_ID);
		List<CourseMessageEntity> resultList = new ArrayList<CourseMessageEntity>();
		if(list.size() > 0) {
			for(CourseMessageEntity msg : list) {
				TpUserEntity student = commonDao.searchOneData(msg.getStudentId(), TpDbConstants.USER_SEARCH_NAME_BY_USERID);
				TpUserEntity createUser = commonDao.searchOneData(msg.getCreateUser(), TpDbConstants.USER_SEARCH_NAME_BY_USERID);
				msg.setStudentId(student.getPersonName());
				msg.setCreateUser(createUser.getPersonName());
				resultList.add(msg);
			}
		}
		
		result.setData(resultList);
		return result;
	}
	
	/**
	 *根据userId获取用户信息
	 * @param userId
	 * @return TpUserEntity
	 */
	public TpUserEntity getUserByUserId(String userId) {
		return commonDao.searchOneData(userId, TpDbConstants.USER_SEARCH_NAME_BY_USERID);
	}
	
	/**
	 * 学员是否参与
	 * 
	 * @param userId   学员ID
	 * @param courseId ID
	 * @return
	 */
	public Boolean isExitStudent(String userId, String courseId) {
		UserCourseEntity entity = new UserCourseEntity();
		entity.setUserId(userId);
		entity.setCourseId(courseId);
		int count = commonDao.searchCount(entity, SQL_USER_COURSE_SEARCH_COUNT);
		if (count > 0) {
			return true;
		}
		return false;
	}

	/**
	 * 检索编辑/查看所需要的的信息
	 * 
	 * @param id ID
	 * @return 返回结果
	 */
	public CourseManagerView searchCourse4EditById(String id, String type) {
		CourseManagerView query = new CourseManagerView();
		query.setId(id);
		CourseManagerView result = commonDao.searchOneData(query, SEARCH_COURSE_BY_ID);
		if (PAGE_TYPE_VIEW.equals(type)) {
			if (result.getCrowdType() != null) {
				if (COURSE_CROWD_NOT_TRAIN.equals(result.getCrowdType())) {
					result.setCrowdType(getMessage(COURSE_PUBLIC));
				} else if (COURSE_CROWD_TRAIN.equals(result.getCrowdType())) {
					result.setCrowdType(getMessage(COURSE_PRIVATE));
				}
			}
			if (result.getCourseStatus() != null) {
				if (COURSE_STATUS_CONTINU.equals(result.getCourseStatus())) {
					result.setCourseStatus(getMessage(CENTER_COURSE_STATUS_SERIAL));
				} else if (COURSE_STATUS_FINISH.equals(result.getCourseStatus())) {
					result.setCourseStatus(getMessage(CENTER_COURSE_STATUS_COMPLETE));
				} else if (COURSE_STATUS_OVER.equals(result.getCourseStatus())) {
					result.setCourseStatus(CENTER_COURSE_STATUS_END);
				}
			}
		}
		// 通过积分显示价格
		if (result.getPrice() != null) {
			if (TpConfigUtil.displayPriceByPoints()) {
				result.setPrice(BigDecimalUtil.ignoreZeroScale(
						result.getPrice().multiply(new BigDecimal(TpConfigUtil.getPointsForSmallChange()))));
			}
		}
		String activitys = "";
		if (PAGE_TYPE_EDIT.equals(type)) {
			if (FLAG.equals(result.getFlagNote())) {
				activitys += FLAG_NOTE + ",";
			}
			if (FLAG.equals(result.getFlagVote())) {
				activitys += FLAG_VOTE + ",";
			}
			if (FLAG.equals(result.getFlagQa())) {
				activitys += FLAG_QA + ",";
			}
			if (FLAG.equals(result.getFlagDiscuss())) {
				activitys += FLAG_DISCUSS + ",";
			}
		}
		result.setActivitys(activitys);
		// 标签信息
		TagView tag = new TagView();
		tag.setLinkId(id);
		tag.setLinkType(LINK_TYPE_COURSE);
		List<TagView> tagList = commonDao.searchList(tag, CourseDbConstants.SQL_SEARCH_TAG_BY_COURSE_ID);
		if (tagList != null && tagList.size() > 0) {
			for (int i = 0; i < tagList.size(); i++) {
				if (tagList.get(i).getParentTagId() == null) {
					result.setFirstTag(tagList.get(i).getTagId());
					result.setFirstTagName(tagList.get(i).getTagName());
				} else {
					result.setSecondTag(tagList.get(i).getTagId());
					result.setSecondTagName(tagList.get(i).getTagName());
				}
			}
		}
		// 教师信息
		List<TeacherView> teacherList = getTeacherList();
		for (TeacherView teacher : teacherList) {
			if (teacher.getUserId().equals(result.getTeacherId())) {
				if (teacher.getSex() != null && teacher.getSex().equals("0")) {
					teacher.setSex("女");
				} else if (teacher.getSex() != null && teacher.getSex().equals("1")) {
					teacher.setSex("男");
				}
				result.setTeacher(teacher);
			}
		}
		// 章节信息
		if (PAGE_TYPE_VIEW.equals(type)) {
			ChapterView chapter = new ChapterView();
			chapter.setCourseId(id);
			chapter.setSortName("CHAPTER_ORDER");
			chapter.setSortOrder("asc");
			List<ChapterView> chapterList = commonDao.searchList(chapter, SQL_SEARCH_CHAPTER_VIEW);
			List<ChapterView> bigChapter = new ArrayList<ChapterView>();
			List<ChapterView> smallChapter = new ArrayList<ChapterView>();
			for (ChapterView view : chapterList) {
				// view=commonDao.searchOneData(view, SQL_SEARCH_CHAPTER_ONE_DATA);
				/*
				 * if (StringUtil.isNotEmpty(view.getContent())) { String content =
				 * getTextFromHtml(view.getContent()); view.setContent(content); }
				 */
				if (view.getChapterFlag().compareTo(BigDecimal.ONE) == 0) {
					bigChapter.add(view);
					result.setChapterBigList(bigChapter);
				} else if (view.getChapterFlag().compareTo(BigDecimal.ZERO) == 0) {
					smallChapter.add(view);
					result.setChapterSmallList(smallChapter);
				}
			}
		}
		// 检索测试题
		MeasurementEntity measurementQuery = new MeasurementEntity();
		measurementQuery.setLinkType(LINK_TYPE_COURSE);
		measurementQuery.setLinkId(id);
		measurementQuery.setMeasureType(MEASUERMENT_TYPE_MEASUERMENT);
		List<QuestionEntity> measurementResult = (List<QuestionEntity>) search(measurementQuery,
				MEASUERMENT_SEARCH_QUESTION_LIST_BY_MEASUREMENT);
		// 检索测试题中的选项
		for (QuestionEntity quest : measurementResult) {
			QuestionOptionEntity optionQuery = new QuestionOptionEntity();
			optionQuery.setQuestionId(quest.getId());
			optionQuery.setSortName("sortOrder");
			optionQuery.setSortOrder("asc");
			List<QuestionOptionEntity> options = (List<QuestionOptionEntity>) search(optionQuery,
					MEASUERMENT_SEARCH_QUESTION_OPTIONS_LIST);
			quest.setOptions(options);
		}
		result.setQuestionList(measurementResult);
		return result;
	}

	/**
	 * 
	 * *去掉字符串中的html标签的特殊字符
	 * @param htmlStr 需要去除标签的字符串
	 * @return
	 */
	public static String getTextFromHtml(String htmlStr) {
		htmlStr = delHTMLTag(htmlStr);
		htmlStr = htmlStr.replaceAll("&nbsp;", "");
		htmlStr = htmlStr.replaceAll("&quot;", "");
		htmlStr = htmlStr.replaceAll("&lt;", "");
		htmlStr = htmlStr.replaceAll("&gt;", "");
		htmlStr = htmlStr.substring(0, htmlStr.indexOf("。") + 1);
		return htmlStr;
	}

	/**
	 * 
	 * 去掉字符串中的html标签
	 * 
	 * @param htmlStr 需要去除标签的字符串
	 * @return
	 */
	public static String delHTMLTag(String htmlStr) {
		String regEx_script = "<script[^>]*?>[\\s\\S]*?<\\/script>"; // 定义script的正则表达式
		String regEx_style = "<style[^>]*?>[\\s\\S]*?<\\/style>"; // 定义style的正则表达式
		String regEx_html = "<[^>]+>"; // 定义HTML标签的正则表达式

		Pattern p_script = Pattern.compile(regEx_script, Pattern.CASE_INSENSITIVE);
		Matcher m_script = p_script.matcher(htmlStr);
		htmlStr = m_script.replaceAll(""); // 过滤script标签

		Pattern p_style = Pattern.compile(regEx_style, Pattern.CASE_INSENSITIVE);
		Matcher m_style = p_style.matcher(htmlStr);
		htmlStr = m_style.replaceAll(""); // 过滤style标签

		Pattern p_html = Pattern.compile(regEx_html, Pattern.CASE_INSENSITIVE);
		Matcher m_html = p_html.matcher(htmlStr);
		htmlStr = m_html.replaceAll(""); // 过滤html标签

		return htmlStr.trim(); // 返回文本字符串
	}

	/**
	 * 
	 * 检索列表
	 * 
	 * @param queryInfo
	 * @return
	 */
	public ResultBean listSearch(CourseManagerView queryInfo) {
		ResultBean result = new ResultBean();
		if (!WebContext.hasAccess(ALL_LIST)) {
			//queryInfo.setTeacherId(WebContext.getSessionUserId());
		}
		String role = geRoleByLoginUser();
		if(role.equals("t") && !WebContext.getSessionUserId().equals("2542668")) {
			queryInfo.setTeacherId(WebContext.getSessionUserId());
		}
		if (COURSE_EXAM_IS.equals(queryInfo.getIsExam())) {
			queryInfo.setMeasureType("2");
		} else if (COURSE_EXAM_NOT.equals(queryInfo.getIsExam())) {
			queryInfo.setMeasureType("1");
		}
		//queryInfo.setShareFlag(COURSE_SHARE_FLAG_TWO);
		if(queryInfo.getSelfType() != null) {
			//1:私有课、2:课工场、3:图书馆、4:精品课、5:试听课
			if("1".equals(queryInfo.getSelfType())) {
				queryInfo.setShareFlag(COURSE_SHARE_FLAG_ELEVEN);
			}else if("2".equals(queryInfo.getSelfType())) {
				queryInfo.setShareFlag(COURSE_SHARE_FLAG_TWO);
			}if("3".equals(queryInfo.getSelfType())) {
				queryInfo.setLibraryFlag("1");
			}if("4".equals(queryInfo.getSelfType())) {
				queryInfo.setShareFlag(COURSE_SHARE_FLAG_ONE);
			}if("5".equals(queryInfo.getSelfType())) {
				queryInfo.setCourseLocation("1");
			}
			if("6".equals(queryInfo.getSelfType())) {
				queryInfo.setShareFlag(COURSE_SHARE_FLAG_THREE);
			}
		}
		result = commonDao.searchList4Page(queryInfo, SELECT_COURSE_LIST_MANAGER);
		List<CourseManagerView> viewList = (List<CourseManagerView>) result.getData();
		if(null!=viewList && viewList.size()>0) {
			for(int i=0;i<viewList.size();i++){
				String courseId = viewList.get(i).getId();
				String isCourse="1";
				ChapterView view = new ChapterView();
				view.setCourseId(courseId);
				view.setIsCourse("1");
				ChapterView chapterView= commonDao.searchOneData(view, EXAM_ALL_SEARCH_BY_PK);
				if(null!=chapterView && StringUtil.isNotBlank(chapterView.getIsCourse())) {
					 viewList.get(i).setIsCourse(chapterView.getIsCourse());
					 viewList.get(i).setLinkId(chapterView.getId());
				}
			}
		}
		result.setData(viewList);
		return result;
	}

	/**
	 * 
	 * 删除课程信息
	 * 
	 * @param cIds 课程id id
	 */
	public ResultBean deleteCourse(String cIds) {
		ResultBean result = new ResultBean();
		String[] ids = StringUtil.split(cIds, ",");
		if (ids != null) {
			for (String id : ids) {
				if (StringUtil.isNotEmpty(id)) {
					CourseManagerView view = new CourseManagerView();
					ChapterEntity chapterQuery = new ChapterEntity();
					view.setId(id);
					chapterQuery.setCourseId(id);
					// 是否存在章节
					int chapterCount = searchTotal(chapterQuery);
					// 有章节不给删除
					if (chapterCount != 0) {
						result.setStatus(false);
						result.setMessages(getMessage("课程下有章节，请先删除章节信息！"));
						return result;
						//throw new BusinessException("课程下有章节，请先删除章节信息！");
					}
					commonDao.deleteData(view, SQL_COURSE_DELETE);
					deleteCourseByCourseId(id);
				}
			}
		}
		return result;
	}

	/**
	 * 课程删除模块
	 * 
	 * @param courseId 课程id
	 */
	public void deleteCourseByCourseId(String courseId) {
		// 删除计划
		delPlan(courseId);
		// 删除学员+投票
		delUserCourse(courseId);
		// 删除作业
		delHomeWork(courseId, LINK_TYPE_COURSE);
		// 删除问卷
		delMeasureCourse(courseId, LINK_TYPE_COURSE);
		// 删除标签
		deleteCategorys(courseId);
		// 删除测试
		delTestCourse(courseId, LINK_TYPE_COURSE);
		// 删除问答
		delQaQuestion(courseId, LINK_TYPE_COURSE);
		// 删除评论
		delDiscuss(courseId, LINK_TYPE_COURSE);
		// 删除笔记
		delNote(courseId, LINK_TYPE_COURSE);
	}

	/**
	 * 删除课程下的评论
	 * 
	 * @param courseId 课程ID
	 */
	public void delDiscuss(String courseId, String linkType) {
		DiscussEntity discuss = new DiscussEntity();
		discuss.setLinkId(courseId);
		discuss.setLinkType(linkType);
		commonDao.deleteData(discuss, SQL_DELETE_DISCUSS_BY_COURSER);
	}

	/**
	 * 删除课程下的笔记
	 * 
	 * @param courseId 课程ID
	 */
	public void delNote(String courseId, String linkType) {
		NoteEntity note = new NoteEntity();
		note.setLinkId(courseId);
		note.setLinkType(linkType);
		commonDao.deleteData(note, SQL_DELETE_NOTE_BY_COURSER);
	}

	/**
	 * 删除课程问答
	 * 
	 * @param courseId课程ID
	 */
	public void delQaQuestion(String courseId, String linkType) {
		QaQuestionEntity qaQuestion = new QaQuestionEntity();
		qaQuestion.setLinkId(courseId);
		qaQuestion.setLinkType(linkType);
		commonDao.deleteData(qaQuestion, SQL_DELETE_QA_QUESTION_BY_COURSER);
	}

	/**
	 * 删除课程标签
	 * 
	 * @param courseId 课程ID
	 */
	public void delTestCourse(String courseId, String linkType) {
		MeasurementEntity measure = new MeasurementEntity();
		measure.setLinkId(courseId);
		measure.setLinkType(linkType);
		measure.setMeasureType(MEASUERMENT_TYPE_MEASUERMENT);
		commonDao.deleteData(measure, SQL_DELETE_MEASURE_BY_COURSER);
	}

	/**
	 * 删除课程相关的计划
	 * 
	 * @param courseId 课程id
	 */
	public void delPlan(String courseId) {
		PlanCourseEntity planCourse = new PlanCourseEntity();
		planCourse.setCourseId(courseId);
		commonDao.deleteData(planCourse, SQL_DELETE_PLAN_COURSE);
	}

	/**
	 * 删除课程下的章节信息
	 * 
	 * @param courseId 课程ID
	 */
	public void delChapter(String courseId) {
		ChapterEntity chapter = new ChapterEntity();
		chapter.setCourseId(courseId);
		commonDao.deleteData(chapter, SQL_DELETE_CHAPTER_COURSE);
	}

	/**
	 * 删除参加课程的学员信息
	 * 
	 * @param courseId 课程ID
	 */
	public void delUserCourse(String courseId) {
		UserCourseEntity userCourse = new UserCourseEntity();
		userCourse.setCourseId(courseId);
		commonDao.deleteData(userCourse, SQL_DELETE_USER_COURSE_BY_COURSER);
	}

	/**
	 *  删除课程相关的作业
	 * @param courseId 课程id
	 * @param linkType  关联类型
	 */
	public void delHomeWork(String courseId, String linkType) {
		HomeworkEntity homework = new HomeworkEntity();
		homework.setLinkId(courseId);
		homework.setLinkType(LINK_TYPE_COURSE);
		commonDao.deleteData(homework, SQL_DELETE_HOME_WORK_BY_COURSER);
	}

	/**
	 * 
	 * 删除课程相关的问卷
	 * 
	 * @param courseId 课程id
	 */
	public void delMeasureCourse(String courseId, String linkType) {
		MeasurementEntity measure = new MeasurementEntity();
		measure.setLinkId(courseId);
		measure.setLinkType(linkType);
		measure.setMeasureType(MEASUERMENT_TYPE_QUESTIO);
		commonDao.deleteData(measure, SQL_DELETE_MEASURE_BY_COURSER);
	}

	/**
	 * 删除问卷、测试、练习、考试以及相关的用户操作
	 * 
	 * @param linkId      链接id
	 * @param linkType    链接类型
	 * @param measureType 测验类型
	 */
	public void delMeasure(String linkId, String linkType, String measureType) {
		// 删除tb_measurement tb_question tb_question_option tb_user_measuerment_answer
		// tb_user_measurement_result
		MeasurementEntity measure = new MeasurementEntity();
		measure.setLinkId(linkId);
		measure.setLinkType(linkType);
		measure.setMeasureType(measureType);
		commonDao.deleteData(measure, SQL_DELETE_MEASURE_BY_COURSER);
	}

	/**
	 * 
	 * 删除参加课程的学员
	 * 
	 * @param userId   学员ID
	 * @param courseId 课程ID
	 */
	public void deleteStudent(String userId, String courseId) {
		String[] userIds = StringUtil.split(userId, ",");
		if (userIds != null) {
			for (String id : userIds) {
				if (StringUtil.isNotEmpty(id)) {
					UserCourseEntity userCourse = new UserCourseEntity();
					userCourse.setUserId(id);
					userCourse.setCourseId(courseId);
					commonDao.deleteData(userCourse, SQL_DELETE_USER_COURSE);
				}
			}
		}
	}

	/**
	 * 课程精品推荐/取消
	 * 
	 * @param id           课程ID
	 * @param flagBoutique 是否推荐：0-未推荐 1-已推荐
	 */
	public void pushHome(String id, String flagBoutique) {
		CourseManagerView view = new CourseManagerView();
		view.setId(id);
		if (NOT_FLAG_BOUTIQUED.equals(flagBoutique)) {
			view.setFlagBoutique(FLAG_BOUTIQUED);
		} else {
			view.setFlagBoutique(NOT_FLAG_BOUTIQUED);
		}
		commonDao.updateData(view, UPDATE_FLAGBOUTIQUE);
	}

	/**
	 * 
	 * 首页轮播推荐/取消
	 * 
	 * @param id               课程ID
	 * @param flagHomeCarousel 是否推荐：0-未推荐 1-已推荐
	 */
	public void pushHomeBanner(String id, String flagHomeCarousel) {
		CourseManagerView view = new CourseManagerView();
		view.setId(id);
		if (NOT_HOME_CAROUSEL.equals(flagHomeCarousel)) {
			view.setFlagHomeCarousel(IS_HOME_CAROUSEL);
		} else {
			view.setFlagHomeCarousel(NOT_HOME_CAROUSEL);
		}
		commonDao.updateData(view, UPDATE_FLAGHOME);
	}

	/**
	 * 课程发布/取消发布
	 * 
	 * @param id     课程ID
	 * @param status 课程状态（0：未发布，1：已发布）
	 */
	public void publicCourse(String id, String status) {
		CourseManagerView view = new CourseManagerView();
		view.setId(id);
		if (COURSE_STATUS_NOT.equals(status)) {
			view.setStatus(COURSE_STATUS_PUBLIC);
			view.setClassFlag("1");
		} else {
			view.setStatus(COURSE_STATUS_NOT);
			view.setClassFlag("0");
		}
		commonDao.updateData(view, UPDATE_STATUS);
	}

	/**
	 * 结束课程
	 * @param id
	 * @param status
	 */
	public void endCourse(String id) {
		CourseManagerView view = new CourseManagerView();
		view.setId(id);
		view.setEndFlag(COURSE_END_STATUS_YES);
		//更新课程状态
		commonDao.updateData(view, UPDATE_END_STATUS);
	}
	/**
	 * 图书馆课程复制
	 * @param id
	 * @throws Exception 
	 */
	public void toLibCourse(String id) throws Exception {
		CourseManagerView view = new CourseManagerView();
		view.setId(id);
		//view.setShareFlag(COURSE_SHARE_FLAG_EIGHT);
		//view.setLibraryFlag("1");
		//commonDao.updateData(view, UPDATE_LIB_FLAG);
		CourseManagerView query = new CourseManagerView();
		query.setId(id);
		// 课程信息
		CourseManagerView result = commonDao.searchOneData(query, SEARCH_COURSE_BY_ID);
		TagView tag = new TagView();
		tag.setLinkId(id);
		tag.setLinkType(LINK_TYPE_COURSE);
		// 课程标签信息
		List<TagView> tagList = commonDao.searchList(tag, CourseDbConstants.SQL_SEARCH_TAG_BY_COURSE_ID);
		//循环遍历对象
		if (tagList != null && tagList.size() > 0) {
			for (int i = 0; i < tagList.size(); i++) {
				if (tagList.get(i).getParentTagId() == null) {
					result.setFirstTag(tagList.get(i).getTagId());
				} else {
					result.setSecondTag(tagList.get(i).getTagId());
				}
			}
		}
		EntityUtil.copyEntity(result, query);
		query.setId(IDGenerator.genUID());
		query.setStatus(query.getStatus());
		//复制课程标题设置
		query.setTitle(query.getTitle()+"_（图书馆）");
		query.setStartDate(sdf.format(new Date()));
		//设置课程到期时间
		query.setEndDate("2099-12-31");
		//设置课程范围
		query.setCrowdType("1");
		query.setTeacherId(result.getTeacherId());
		query.setEndFlag(COURSE_END_STATUS_NOT);
		query.setLibraryFlag(COURSE_END_STATUS_YES);
		query.setClassFlag("0");
		query.setShareFlag(query.getShareFlag());
		query.setCourseLocation(result.getCourseLocation() == null ? "0" : result.getCourseLocation());
		FileEntity file = fileService.getFile(query.getPictureId(), "image");
		File picFile = new File(PropertyUtil.getProperty("res.storage.root")+file.getPath());
		FileInputStream is = new FileInputStream(picFile);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] buff = new byte[1024];
		int len = 0;
		while ((len = is.read(buff)) != -1) {
		    bos.write(buff, 0, len);
		}
		String newId = IDGenerator.genUID();
		// 得到图片的字节数组
		byte[] resultByte = bos.toByteArray();
		OutputStream os = new FileOutputStream(PropertyUtil.getProperty("res.storage.pic")+newId+".png");
		os.write(resultByte, 0, resultByte.length);
		os.flush();
		os.close();
		FileEntity newPic = new FileEntity();
		newPic.setContentType("png");
		newPic.setCreateUser(WebContext.getSessionUserId());
		newPic.setCreateDate(new Date());
		newPic.setFileId(newId);
		newPic.setFileName(newId + ".png");
		newPic.setFileSize(file.getFileSize());
		newPic.setPath("/image/pic/"+newId+".png");
		newPic.setType("image");
		fileService.save(newPic);
		query.setPictureId(newId);
		commonDao.insertData(query, COURSE_INSERT);
		joinUserCourse(query.getId());
		List<ChapterView> chapterList = chapterService.getChapterListByCourseId(id);
		//保存原有课程章节
		saveFactoryCourseChapter(chapterList,query.getId());
		// 新增分类信息
		if (StringUtil.isNotEmpty(query.getFirstTag())) {
			insertCatagorys(query.getId(), query.getFirstTag(), query.getSecondTag());
		}
	}
	
	/**
	 * 
	 * 课程删除
	 * 
	 * @param id 课程ID
	 * @throws Exception
	 */
	public void delCourseById(String id) throws Exception {
		ChapterEntity chapterQuery = new ChapterEntity();
		chapterQuery.setCourseId(id);
		int chapterCount = searchTotal(chapterQuery);
		if (chapterCount != 0) {
			throw new BusinessException(getMessage(MSG_NOT_ALLOW_DELETE));
		}
		CourseEntity entity = new CourseEntity();
		entity.setId(id);
		delete(entity);
	}

	public ResultBean searchTeam(CourseTeamEntity query) {
		return commonDao.searchList4Page(query, SQL_COURSE_TEAM_SELECT);
	}

	public CourseTeamEntity getCourseTeamById(String id) {
		CourseTeamEntity entity = new CourseTeamEntity();
		entity.setId(id);
		return commonDao.searchOneData(entity, SQL_COURSE_TEAM_SELECT_BY_ID);
	}
	
	public List<UserView> getCourseUserList(String courseId) {
		UserView view = new UserView();
		view.setCourseId(courseId);
		return commonDao.searchList(view, SQL_USER_SEARCH_JOIN);
	}
	
	public void deleteCourseIeamById(String id) {
		CourseTeamUserEntity user = new CourseTeamUserEntity();
		user.setTeamId(id);
		commonDao.deleteData(user, SQL_COURSE_TEAM_USER_DELETE);
		CourseTeamEntity entity = new CourseTeamEntity();
		entity.setId(id);
		commonDao.deleteData(entity, SQL_COURSE_TEAM_DELETE_BY_ID);
	}
	
	public void saveTeamUser(String teamId, String... userIds) {
		if (userIds != null) {
			CourseTeamUserEntity user = new CourseTeamUserEntity();
			for (String userId : userIds) {
				if (StringUtil.isNotEmpty(userId) && StringUtil.isNotEmpty(teamId)) {
					if (isExitTeamStudent(userId, teamId)) {
						throw new BusinessException("选择的组员中有已经参加了此小组！");
					}
					user.setTeamId(teamId);
					Integer maxOrder = commonDao.searchOneData(user,SQL_COURSE_TEAM_USER_SELECT_MAX_NO);
					if(maxOrder != null) {
						user.setUserNo(maxOrder+1);
					}else {
						user.setUserNo(1);
					}
					user.setUserId(userId);
					user.setCreateTime(new Date());
					user.setCreateUser(WebContext.getSessionUserId());
					commonDao.insertData(user, SQL_COURSE_TEAM_USER_INSERT);
				} else {
					throw new BusinessException("组员添加失败");
				}
			}
		}
	}
	
	public Boolean isExitTeamStudent(String userId, String teamId) {
		CourseTeamUserEntity entity = new CourseTeamUserEntity();
		entity.setUserId(userId);
		entity.setTeamId(teamId);
		int count = commonDao.searchCount(entity, SQL_COURSE_TEAM_USER_SELECT_UNIQUE);
		if (count > 0) {
			return true;
		}
		return false;
	}
	
	public void saveTeam(CourseTeamEntity team) {
		if(team.getId() != null && !"".equals(team.getId())) {
			team.setUpdateTime(new Date());
			team.setUpdateUser(WebContext.getSessionUserId());
			commonDao.updateData(team, SQL_COURSE_TEAM_UPDATE);
		}else {
			team.setId(IDGenerator.genUID());
			team.setCreateTime(new Date());
			team.setCreateUser(WebContext.getSessionUserId());
			commonDao.insertData(team, SQL_COURSE_TEAM_INSERT);
		}
	}
	
	public void deleteTeamStudent(String userId, String teamId) {
		String[] userIds = StringUtil.split(userId, ",");
		if (userIds != null) {
			for (String id : userIds) {
				if (StringUtil.isNotEmpty(id)) {
					CourseTeamUserEntity user = new CourseTeamUserEntity();
					user.setUserId(id);
					user.setTeamId(teamId);
					commonDao.deleteData(user, SQL_COURSE_TEAM_USER_DELETE);
				}
			}
		}
	}
	
	/**
	 * 添加页面的标签列表 searchTagList
	 */
	public List<TagView> searchTagList(TagView tagQuery) {
		// tagQuery.setTagTypeName(CourseMsgNames.TAG_TYPE_FIRST);
		// tagQuery.setTagTypeName("方向");
		tagQuery.setTagTypeId(TAG_TYPE_ID_DIRECTIVE_COURSE);
		tagQuery.setBusinessType("1");
		return commonDao.searchList(tagQuery, SQL_SEARCH_FIRST_TAG_TYPE);
	}

	/**
	 * 添加页面的标签列表 searchTagList
	 */
	public List<TagView> searchTagList2(TagView tagQuery) {

		tagQuery.setTagTypeId("3");
		tagQuery.setBusinessType("2");
		return commonDao.searchList(tagQuery, SQL_SEARCH_FIRST_TAG_TYPE);
	}
	
	/**
	 * 添加页面的标签列表
	 */
	public List<TagView> searchTagSecondList(TagView tagQuery) {
		// tagQuery.setTagTypeName(CourseMsgNames.TAG_TYPE_SECOND);
		// tagQuery.setTagTypeName("分类");
		tagQuery.setTagTypeId(TAG_TYPE_ID_CATAGORY_COURSE);
		tagQuery.setBusinessType("1");
		return commonDao.searchList(tagQuery, SQL_SEARCH_FIRST_TAG_TYPE);
	}

	/**
	 * 添加页面的标签列表
	 */
	public List<TagView> searchTagSecondList2(TagView tagQuery) {
		// tagQuery.setTagTypeName(CourseMsgNames.TAG_TYPE_SECOND);
		// tagQuery.setTagTypeName("分类");
		tagQuery.setTagTypeId("4");
		tagQuery.setBusinessType("2");
		return commonDao.searchList(tagQuery, SQL_SEARCH_FIRST_TAG_TYPE);
	}
	
	/**
	 * 添加页面的标签列表：二级标签 searchTagList
	 */
	public List<TagView> searchSecondTagList(String id) {
		if(id != null && !"".equals(id)) {
			TagView tagQuery = new TagView();
			tagQuery.setParentId(id);
			List<TagView> list = new ArrayList<TagView>();
			// tagQuery.setTagTypeName("分类");
			if (id == null) {
				return list;
			} else {
				tagQuery.setTagTypeId(TAG_TYPE_ID_CATAGORY_COURSE);
				tagQuery.setBusinessType("1");
				return commonDao.searchList(tagQuery, SQL_SEARCH_SECOND_TAG_TYPE);
			}
		}else {
			return null;
		}
	}

	public List<TagView> searchSecondTagList2(String id) {
		TagView tagQuery = new TagView();
		tagQuery.setParentId(id);
		List<TagView> list = new ArrayList<TagView>();
		// tagQuery.setTagTypeName("分类");
		if (id == null) {
			return list;
		} else {
			tagQuery.setTagTypeId("4");
			tagQuery.setBusinessType("2");
			return commonDao.searchList(tagQuery, SQL_SEARCH_SECOND_TAG_TYPE);
		}

	}

	/**
	 * 教师列表
	 * 
	 * @return 返回结果
	 */
	public List<TeacherView> getTeacherList() {
		TeacherView query = new TeacherView();
		// 教师和管理员的用户角色A
		query.setUserKbn(CourseStateConstants.USER_KBN_A);
		query.setRoleId(CourseStateConstants.TEACHER_ROLE);
		List<TeacherView> result = search(query, USER_SEARCH_CONDITION);
		return result;
	}

	/**
	 * 教师列表
	 * 
	 * @return 返回结果
	 */
	public ResultBean getTeacherAll(TeacherView query) {
		// 教师和管理员的用户角色A
		TpUserEntity LogingUser = commonDao.searchOneData(WebContext.getSessionUserId(), TpDbConstants.USER_SEARCH_NAME_BY_USERID);
		query.setSchoolName(LogingUser.getSchoolName());
		query.setUserKbn(CourseStateConstants.USER_KBN_A);
		query.setRoleId(CourseStateConstants.TEACHER_ROLE);
		return commonDao.searchList4Page(query, USER_SEARCH_CONDITION);
	}

	public CourseTeamEntity getCourseTeamByUserId(String userId,String courseId) {
		CourseTeamEntity entity = new CourseTeamEntity();
		entity.setTeamLeader(userId);
		entity.setCourseId(courseId);
		return commonDao.searchOneData(entity, SQL_COURSE_TEAM_SELECT_BY_TEAMLEADER);
	}
	
	
	public CourseTeamUserEntity getTeamUserByUserId(String userId,String courseId) {
		CourseTeamUserEntity entity = new CourseTeamUserEntity();
		entity.setUserId(userId);
		entity.setUpdateUser(courseId);
		return commonDao.searchOneData(entity, SQL_COURSE_TEAM_USER_SELECT_BY_USERID);
	}
	
	/**
	 * 参与课程的人员
	 * 
	 * @param id 课程ID
	 * @return
	 */
	public ResultBean searchUser(UserView query) {
		return commonDao.searchList4Page(query, SQL_USER_SEARCH_JOIN);
	}

	public ResultBean searchUser(CourseTeamUserEntity entity) {
		return commonDao.searchList4Page(entity, SQL_COURSE_TEAM_USER_SELECT);
	}
	
	/**
	 * 所有人员
	 * 
	 * @return 返回人员列表信息
	 */
	public ResultBean searchUserAll(UserView query) {
		return commonDao.searchList4Page(query, SQL_USER_ALL);
	}

	/**
	 * 保存课程所有信息
	 * 
	 * @param data 课程对象
	 * @throws Exception
	 */
	public void insertCourseCatagory(CourseManagerView data) throws Exception {

		// 图片处理
		FileEntity pictureFile = processRequestFile("picture", Constants.FILE_TYPE_IMAGE);
		// 缩略图
		compress(pictureFile, false);

		if (pictureFile != null) {
			data.setPictureId(pictureFile.getFileId());
		}
		CourseManagerView view = splitActivity(data.getActivitys());
		if (data.getPrice().compareTo(BigDecimal.ZERO) == 0) {
			data.setFreeFlag(BigDecimal.ONE);
		} else {
			data.setFreeFlag(BigDecimal.ZERO);
		}
		/*data.setFlagDiscuss(view.getFlagDiscuss());
		data.setFlagNote(view.getFlagNote());
		data.setFlagQa(view.getFlagQa());
		data.setFlagVote(view.getFlagVote());*/
		data.setFlagDiscuss("1");
		data.setFlagNote("0");
		data.setFlagQa("0");
		data.setFlagVote("0");
		data.setId(IDGenerator.genUID());
		data.setStatus(COURSE_STATUS_NOT);
		data.setEndFlag(COURSE_END_STATUS_NOT);
		data.setLibraryFlag("0");
		data.setClassFlag("0");
		data.setShareFlag(COURSE_SHARE_FLAG_ELEVEN);
		// 学习人数：默认0
		data.setNumStudy(BigDecimal.ZERO);
		// 关注人数：默认0
		data.setNumFavorite(BigDecimal.ZERO);
		// 浏览人数：默认0
		data.setNumView(BigDecimal.ZERO);
		// 评价人数：默认0
		data.setNumScoreUser(BigDecimal.ZERO);
		// 总投票数：默认0
		data.setTotalVote(BigDecimal.ZERO);
		// 通过积分显示价格
		if (data.getPrice() != null) {
			if (TpConfigUtil.displayPriceByPoints()) {
				data.setPrice(data.getPrice().divide(new BigDecimal(TpConfigUtil.getPointsForSmallChange())));
			}
		}
		if (StringUtil.isEmpty(data.getTeacherId())) {
			if (isTeacherOne()) {
				data.setTeacherId(WebContext.getSessionUserId());
			} else {
				throw new BusinessException("保存失败,请选择授课教师！");
			}
		}
		data.setStartDate(sdf.format(new Date()));
		data.setEndDate("2099-12-31");
		data.setCrowdType("1");
		data.setTeacherId(WebContext.getSessionUserId());
		if(StringUtil.isBlank(data.getWeidu())) {
			data.setWeidu(null);
		}else {
			data.setWeidu(data.getWeidu());
		}
		commonDao.insertData(data, COURSE_INSERT);
		if(data.getUserIdList().size() > 0) {
			for(String userId : data.getUserIdList()) {
				CourseAssistTeacherEntity assistTeacher = new CourseAssistTeacherEntity();
				assistTeacher.setId(IDGenerator.genUID());
				assistTeacher.setCourseId(data.getId());
				assistTeacher.setTeacherId(userId);
				assistTeacher.setCreateTime(new Date());
				assistTeacher.setCreateUser(WebContext.getSessionUserId());
				commonDao.insertData(assistTeacher, COURSE_ASSIST_INSERT);
			}
		}
		joinUserCourse(data.getId());
		// 新增分类信息
		if (StringUtil.isNotEmpty(data.getFirstTag())) {
			insertCatagorys(data.getId(), data.getFirstTag(), data.getSecondTag());
		}
		// 保存问题
		if (data.getQuestionList().size() > 0) {
			// 新增测试信息
			MeasurementEntity measurement = new MeasurementEntity();
			measurement.setId(IDGenerator.genUID());
			measurement.setTitile(data.getTitle());
			measurement.setLinkId(data.getId());
			measurement.setMeasureType(MEASUERMENT_TYPE_MEASUERMENT);
			measurement.setLinkType(LINK_TYPE_COURSE);
			// commonDao.insertData(measurement, MEASUREMENT_INSERT);
			insert(measurement);
			// 新增问题
			insertQuestion(data.getQuestionList(), measurement.getId());
		}
		// 为索引设置用户名
		if (WebContext.getSessionUser() != null) {
			data.setPersonName(WebContext.getSessionUser().getUserName());
		}
	}

	/**
	 * 判断当前用户是否是教师
	 * 
	 * @return true 是教师 false不是教师
	 */
	public boolean isTeacherOne() {
		TeacherView query = new TeacherView();
		query.setUserId(WebContext.getSessionUserId());
		query.setRoleId(ROLE_TEACHER_ROLE);
		// 教师和管理员的用户角色A
		query.setUserKbn(CourseStateConstants.USER_KBN_A);
		int count = commonDao.searchCount(query, SQL_IS_TEACHHER_USER);
		return count > 0;
	}

	/**
	 * 更新课程学习
	 * 
	 * @param course
	 * @throws Exception
	 */
	public void updateCourseAll(CourseManagerView course) throws Exception {
		// 图片处理
		FileEntity pictureFile = processRequestFile("picture", Constants.FILE_TYPE_IMAGE);
		// 缩略图
		compress(pictureFile, false);
		CourseManagerView oldCourse = searchCourse4EditById(course.getId(), PAGE_TYPE_EDIT);

		if (!WebContext.hasAccess(ALL_LIST) && !oldCourse.getTeacherId().equals(WebContext.getSessionUserId())) {
			throw new BusinessException(getMessage(NOT_ALLOW_COURSE));
		}
		if (pictureFile != null) {
			oldCourse.setPictureId(pictureFile.getFileId());
		}
		if (course.getPrice().compareTo(BigDecimal.ZERO) == 0) {
			oldCourse.setFreeFlag(BigDecimal.ONE);
		} else {
			oldCourse.setFreeFlag(BigDecimal.ZERO);
		}
		// 通过积分显示价格
		if (course.getPrice() != null) {
			if (TpConfigUtil.displayPriceByPoints()) {
				oldCourse.setPrice(course.getPrice().divide(new BigDecimal(TpConfigUtil.getPointsForSmallChange())));
			} else {
				oldCourse.setPrice(course.getPrice());
			}
		}
		CourseManagerView view = splitActivity(course.getActivitys());
		oldCourse.setFlagDiscuss("1");
		oldCourse.setFlagNote("0");
		oldCourse.setFlagQa("0");
		oldCourse.setFlagVote("0");
		oldCourse.setTeacherId(course.getTeacherId());
		oldCourse.setContent(course.getContent());
		oldCourse.setCourseDetail(course.getCourseDetail());
		oldCourse.setTitle(course.getTitle());
		oldCourse.setCourseStatus(course.getCourseStatus());
		oldCourse.setMeasureType(course.getMeasureType());
		oldCourse.setCourseStage(course.getCourseStage());
		oldCourse.setFlagOrderStudy(course.getFlagOrderStudy());
		oldCourse.setFlagRequired(course.getFlagRequired());
		oldCourse.setFlagSkipVideo(course.getFlagSkipVideo());
		oldCourse.setCrowdType(course.getCrowdType());
		oldCourse.setSummary(course.getSummary());
		oldCourse.setTarget(course.getTarget());
		oldCourse.setNotice(course.getNotice());
		oldCourse.setSupplier(course.getSupplier());
		oldCourse.setCourseRource(course.getCourseRource());
		oldCourse.setNumRecommend(course.getNumRecommend());
		oldCourse.setCourseType(course.getCourseType());
		oldCourse.setClassName(course.getClassName() == null ? "":course.getClassName());
		oldCourse.setGradeName(course.getGradeName() == null ? "":course.getGradeName());
		if(StringUtil.isBlank(course.getWeidu())) {
			oldCourse.setWeidu(null);//增加维度评价
		}else {
			oldCourse.setWeidu(course.getWeidu());//增加维度评价	
		}
		oldCourse.setSchoolNatrul(course.getSchoolNatrul() == null ? "":course.getSchoolNatrul());
		oldCourse.setTagsId(course.getTagsId() == null ? "":course.getTagsId());
		if (StringUtil.isEmpty(course.getTeacherId())) {
			if (isTeacherOne()) {
				course.setTeacherId(WebContext.getSessionUserId());
			} else {
				throw new BusinessException("保存失败,请选择授课教师！");
			}
		} else {
			oldCourse.setTeacherId(course.getTeacherId());
		}
		commonDao.updateData(oldCourse, SQL_UPDATE_COURSE);
		CourseAssistTeacherEntity deleteAssistTeacher = new CourseAssistTeacherEntity();
		deleteAssistTeacher.setCourseId(oldCourse.getId());
		commonDao.deleteData(deleteAssistTeacher, COURSE_ASSIST_DELETE_BY_COURSE_ID);
		if(course.getUserIdList().size() > 0) {
			for(String userId : course.getUserIdList()) {
				CourseAssistTeacherEntity assistTeacher = new CourseAssistTeacherEntity();
				assistTeacher.setId(IDGenerator.genUID());
				assistTeacher.setCourseId(oldCourse.getId());
				assistTeacher.setTeacherId(userId);
				assistTeacher.setCreateTime(new Date());
				assistTeacher.setCreateUser(WebContext.getSessionUserId());
				commonDao.insertData(assistTeacher, COURSE_ASSIST_INSERT);
			}
		}

		// 更新分类
		if (StringUtil.isNotEmpty(oldCourse.getFirstTag())) {
			deleteCategorys(oldCourse.getId());
		}
        //注掉编辑课程分类关联的tb_tag_link
		//insertCatagorys(oldCourse.getId(), course.getFirstTag(), course.getSecondTag());

		//insertCatagorys(oldCourse.getId(), course.getFirstTag(), course.getSecondTag());

		// 保存问题
		if (StringUtil.isEmpty(course.getMeasureId())) {
			MeasurementEntity measurement = new MeasurementEntity();
			measurement.setId(IDGenerator.genUID());
			measurement.setTitile(course.getTitle());
			measurement.setLinkId(course.getId());
			measurement.setMeasureType(oldCourse.getMeasureType());
			measurement.setLinkType(LINK_TYPE_COURSE);
			measurement.setMeasureType(MEASUERMENT_TYPE_MEASUERMENT);
			insert(measurement);
			insertQuestion(course.getQuestionList(), measurement.getId());
		} else {
			updateQuestion(course.getMeasureId(), course.getQuestionList(), oldCourse.getQuestionList());
		}
		// 为索引设置用户名
		if (WebContext.getSessionUser() != null) {
			oldCourse.setPersonName(WebContext.getSessionUser().getUserName());
		}
	}

	/**
	 * 获取活动标识
	 * 
	 * @param activitys 活动标识值
	 * @return 返回活动标识
	 */
	private CourseManagerView splitActivity(String activitys) {
		CourseManagerView view = new CourseManagerView();

		view.setFlagNote(activitys.indexOf(FLAG_NOTE) > -1 ? FLAG : FLAG_NOT);
		view.setFlagDiscuss(activitys.indexOf(FLAG_DISCUSS) > -1 ? FLAG : FLAG_NOT);
		view.setFlagQa(activitys.indexOf(FLAG_QA) > -1 ? FLAG : FLAG_NOT);
		view.setFlagVote(activitys.indexOf(FLAG_VOTE) > -1 ? FLAG : FLAG_NOT);

		return view;
	}

	/**
	 * 保存问题
	 * 
	 * @param measurementId   测试ID
	 * @param newQuestionList 新问题内容
	 * @param oldQuestionList 旧问题内容
	 */
	public void updateQuestion(String measurementId, List<QuestionEntity> newQuestionList,
			List<QuestionEntity> oldQuestionList) {
		Map<String, QuestionEntity> questionMap = getMapQuestionByID(newQuestionList);
		Map<String, QuestionEntity> oldQuestionMap = getMapQuestionByID(oldQuestionList);

		for (QuestionEntity oldQuestion : oldQuestionList) {
			// 正确答案的ID
			String referenceAnswerId = "";
			// 问题为旧问题
			if (questionMap.containsKey(oldQuestion.getId())) {
				QuestionEntity newQuestionEntity = questionMap.get(oldQuestion.getId());
				// 更新选项问题
				referenceAnswerId = updateOption(oldQuestion, newQuestionEntity.getReferenceAnswerId(),
						newQuestionEntity);
				oldQuestion.setQuestionType(newQuestionEntity.getQuestionType());
				oldQuestion.setScore(newQuestionEntity.getScore());
				oldQuestion.setContent(newQuestionEntity.getContent());
				oldQuestion.setQuestionOrder(newQuestionEntity.getQuestionOrder());
				oldQuestion.setReferenceAnswerId(referenceAnswerId);
				this.update(oldQuestion);

			} else {
				delete(oldQuestion);
				delete(oldQuestion, QUESTION_DELET_OPTION_BY_QUESTION);
				delete(oldQuestion, QUESTION_DELET_USER_ANSWER_BY_QUESTION);
			}
		}
		for (QuestionEntity newQuestion : newQuestionList) {
			// 正确答案的ID
			String referenceAnswerId = "";
			// 问题为新问题
			if (!oldQuestionMap.containsKey(newQuestion.getId())) {

				newQuestion.setId(IDGenerator.genUID());
				newQuestion.setMeasureId(measurementId);
				referenceAnswerId = insertQuestionOption(newQuestion, newQuestion.getReferenceAnswerId());

				newQuestion.setReferenceAnswerId(referenceAnswerId);
				insert(newQuestion);
			}
		}
	}

	/**
	 * 以问题ID为MAP
	 * 
	 * @param questions
	 * @return 返回结果
	 */
	private Map<String, QuestionEntity> getMapQuestionByID(List<QuestionEntity> questions) {
		Map<String, QuestionEntity> result = new HashMap<String, QuestionEntity>();
		// 如果问题列表为空，则已空的MAP返回
		if (questions != null) {
			for (QuestionEntity question : questions) {
				result.put(question.getId(), question);
			}
		}
		return result;
	}

	/**
	 * 更新选项
	 * 
	 * @param oldQuestion
	 * @param referenceAnswerNo 正确答案序号
	 * @param newQuestion
	 * @return 正确答案的ID
	 */
	private String updateOption(QuestionEntity oldQuestion, String referenceAnswerNo, QuestionEntity newQuestion) {
		String referenceAnswerId = "";
		// 旧问题选项的ID
		// key为选项位置， value为选项ID
		Map<String, String> oldOptionIdMap = new HashMap<String, String>();
		if (oldQuestion.getOptions() != null) {
			for (int i = 0; i < oldQuestion.getOptions().size(); i++) {
				QuestionOptionEntity option = oldQuestion.getOptions().get(i);
				oldOptionIdMap.put(i + "", option.getId());
			}
		}
		// 删除旧问题
		commonDao.deleteData(oldQuestion, QUESTION_DELET_OPTION_BY_QUESTION);

		// 插入新问题
		if (newQuestion.getOptions() != null) {
			for (int i = 0; i < newQuestion.getOptions().size(); i++) {
				QuestionOptionEntity newOption = newQuestion.getOptions().get(i);
				// 新问题选项，相同位置的option必须沿用之前老的ID
				if (oldOptionIdMap.containsKey(i + "")) {
					newOption.setId(oldOptionIdMap.get(i + ""));
				} else {
					newOption.setId(IDGenerator.genDomID());
				}
				newOption.setQuestionId(oldQuestion.getId());
				insert(newOption);
				if (StringUtil.isNotEmpty(referenceAnswerNo) && referenceAnswerNo.contains(i + "")) {
					referenceAnswerId = referenceAnswerId + "," + newOption.getId();
				}
			}
		}
		if (StringUtil.isNotEmpty(referenceAnswerId)) {
			referenceAnswerId = referenceAnswerId.substring(1);
		}
		return referenceAnswerId;
	}

	/**
	 * 插入问题
	 * 
	 * @param newQuestionList
	 * @param measurementId
	 */
	public void insertQuestion(List<QuestionEntity> newQuestionList, String measurementId) {
		QuestionEntity entity = new QuestionEntity();
		entity.setMeasureId(measurementId);
		Integer maxOrder = commonDao.searchOneData(entity, QUESTION_MAX_ORDER);
		if (maxOrder == null) {
			maxOrder = 0;
		}
		Integer order = maxOrder + 1;
		for (QuestionEntity questionData : newQuestionList) {
			questionData.setId(IDGenerator.genUID());
			questionData.setMeasureId(measurementId);
			questionData.setQuestionOrder(new BigDecimal(order));
			order++;
			// 参考答案序号,等选项保存后再保存问题
			int ii = 0;
			String referenceAnswerNo = null;
			if (questionData != null && StringUtil.isNotEmpty(questionData.getReferenceAnswerId())) {
				String[] ss = questionData.getReferenceAnswerId().split(",");
				for (String gg : ss) {
					ii = Integer.parseInt(gg) - 1;
					referenceAnswerNo += String.valueOf(ii) + ",";
				}
			}
			String referenceAnswerId = null;
			List<QuestionOptionEntity> options = questionData.getOptions();
			if (options != null) {
				referenceAnswerId = insertQuestionOption(questionData, referenceAnswerNo);
			}
			if(options != null && StringUtil.isNotBlank(options.get(0).getId())) {
				referenceAnswerId = options.get(0).getId();
			}
			questionData.setReferenceAnswerId(referenceAnswerId);
			insert(questionData);
		}
	}

	/**
	 * 插入问题选项
	 * 
	 * @param newQuestion
	 * @param referenceAnswerNo
	 * @return 返回结果
	 */
	private String insertQuestionOption(QuestionEntity newQuestion, String referenceAnswerNo) {

		List<QuestionOptionEntity> options = newQuestion.getOptions();
		String referenceAnswerId = "";
		if (options != null) {
			// 保存问题选项
			int optionSize = options.size();

			for (int i = 0; i < optionSize; i++) {
				QuestionOptionEntity option = options.get(i);
				option.setId(IDGenerator.genUID());
				option.setQuestionId(newQuestion.getId());
				option.setOptionOrder(new BigDecimal(i + ""));
				insert(option);
				if (StringUtil.isNotEmpty(referenceAnswerNo) && referenceAnswerNo.contains(i + "")) {
					referenceAnswerId = referenceAnswerId + "," + option.getId();
				}
			}
			if (StringUtil.isNotEmpty(referenceAnswerId)) {
				referenceAnswerId = referenceAnswerId.substring(1);
			}
		}
		return referenceAnswerId;
	}

	/**
	 * 课程分类插入
	 * 
	 * @param courseId   课程ID
	 * @param firstTag   一级标签ID
	 * @param secondTags 二级标签id
	 */
	private void insertCatagorys(String courseId, String firstTag, String secondTag) {
		TagLinkEntity first = new TagLinkEntity();
		first.setLinkId(courseId);
		first.setTagId(firstTag);
		first.setLinkType(LINK_TYPE_COURSE);
		super.insert(first, CourseDbConstants.TAG_INSERT);
		TagLinkEntity second = new TagLinkEntity();
		second.setTagId(secondTag);
		second.setParentTagId(firstTag);
		second.setLinkId(courseId);
		second.setLinkType(LINK_TYPE_COURSE);
		super.insert(second, CourseDbConstants.TAG_INSERT);
	}

	/**
	 * 课程分类删除
	 * 
	 * @param courseId 课程ID
	 */
	private void deleteCategorys(String courseId) {
		TagLinkEntity tagLinkEntity = new TagLinkEntity();
		tagLinkEntity.setLinkId(courseId);
		tagLinkEntity.setLinkType(LINK_TYPE_COURSE);
		super.delete(tagLinkEntity, CourseDbConstants.SQL_RAG_DELETE);
	}

	/**
	 * 教师列表
	 * 
	 * @return 返回结果
	 */
	public ResultBean searchTeacher(String teacherId) {
		TeacherView query = new TeacherView();
		ResultBean result = new ResultBean();
		// 教师和管理员的用户角色A
		query.setUserKbn(CourseStateConstants.USER_KBN_A);
		query.setRoleId(CourseStateConstants.TEACHER_ROLE);
		query.setUserId(teacherId);
		query = searchOneData(query, USER_SEARCH_ONE);
		return result.setData(query);
	}

	/**
	 * 课程详情页面，用户点击体验课程/加入课程，若用户是第一次点击，则创建一条用户课程数据
	 * 
	 * @param courseId 课程id
	 * @return 是否加入课程成功
	 * @author WUXIAOXIANG
	 */
	public ResultBean joinCourse(String courseId) {

		ResultBean result = new ResultBean();
		// 课程id为空
		assertNotEmpty(courseId, MSG_E_CID_NULL);
		// 课程不存在
		assertExist(courseId, SQL_IS_COURSE_EXIST, MSG_E_COURSE_NOT_EXIST);
		// 判断用户是否登录
		assertNotGuest();

		ResultBean course = searchCourseDetail(courseId);
		CourseView courseView = (CourseView) course.getData();
		String userId = WebContext.getSessionUserId();

		CourseView query = new CourseView();
		query.setId(courseId);
		query.setUserId(userId);
		assertNotExist(query, SQL_IS_COURSE_JOIN, "课程已加入");

		if (FLAG_FREE.equals(courseView.getFreeFlag()) || courseView.getPrice() == null
				|| 0 == courseView.getPrice().intValue()) {
			// 加入免费
			joinUserCourse(courseId);
			// 更新表，学习人数 +1
			commonDao.updateData(courseId, UPDATE_COURSE_NUMSTUDY);
			result.setStatus(true);
		} else {
			// 加入收费
			String typeCode = DEF_POINTS_TYPE_D0003;
			BigDecimal points = courseView.getPrice().multiply(new BigDecimal(TpConfigUtil.getPointsForSmallChange()));
			UserPoint point = pointService.consumePoint(PointConstants.SYSTEM_CODE_HS, userId, typeCode, points, null,
					null);
			if (point.isStatus()) {
				joinUserCourse(courseId);
				// 更新表，学习人数 +1
				commonDao.updateData(courseId, UPDATE_COURSE_NUMSTUDY);
				messageService.sendSystemMessage(WebContext.getSessionUserId(), getMessage(MSG_PAY_SUCCESS, "课程"));
				// 根据课程ID，查询导师ID
				CourseEntity entity = commonDao.searchOneData(courseId, SQL_SEACH_TEACHER_ID);
				pointManagerService.pointDivided(entity.getTeacherId(), points.toString(), "A0003");
				result.setStatus(true);
			} else {
				result.setMessages(point.getMessages());
				result.setStatus(false);
			}
		}
		return result;

	}
	
	/**
	 * 是否加入课程
	 * @param courseId
	 * @param userId
	 * @return
	 */
	public boolean isJoinCourse(String courseId,String userId) {
		CourseView query = new CourseView();
		query.setId(courseId);
		query.setUserId(userId);
		int i = commonDao.searchCount(query, SQL_IS_COURSE_JOIN);
		if(i > 0) {
			return true;
		}else {
			return false;
		}
	}
	/**
	 * 加入课程
	 * @param courseId
	 * @param id
	 * @return
	 */
	public ResultBean joinCourse(String courseId,String id) {
		CourseMessageEntity msg = selectMessageById(id);
		ResultBean result = new ResultBean();
		// 课程id为空
		assertNotEmpty(courseId, MSG_E_CID_NULL);
		// 课程不存在
		assertExist(courseId, SQL_IS_COURSE_EXIST, MSG_E_COURSE_NOT_EXIST);
		// 判断用户是否登录
		assertNotGuest();

		ResultBean course = searchCourseDetail(courseId);
		CourseView courseView = (CourseView) course.getData();
		String userId = msg.getCreateUser();

		CourseView query = new CourseView();
		query.setId(courseId);
		query.setUserId(userId);
		assertNotExist(query, SQL_IS_COURSE_JOIN, "课程已加入");

		if (FLAG_FREE.equals(courseView.getFreeFlag()) || courseView.getPrice() == null
				|| 0 == courseView.getPrice().intValue()) {
			// 加入免费
			joinUserCourse(courseId,userId);
			// 更新表，学习人数 +1
			commonDao.updateData(courseId, UPDATE_COURSE_NUMSTUDY);
			result.setStatus(true);
		} else {
			// 加入收费
			String typeCode = DEF_POINTS_TYPE_D0003;
			BigDecimal points = courseView.getPrice().multiply(new BigDecimal(TpConfigUtil.getPointsForSmallChange()));
			UserPoint point = pointService.consumePoint(PointConstants.SYSTEM_CODE_HS, userId, typeCode, points, null,
					null);
			if (point.isStatus()) {
				joinUserCourse(courseId,userId);
				// 更新表，学习人数 +1
				commonDao.updateData(courseId, UPDATE_COURSE_NUMSTUDY);
				messageService.sendSystemMessage(WebContext.getSessionUserId(), getMessage(MSG_PAY_SUCCESS, "课程"));
				// 根据课程ID，查询导师ID
				CourseEntity entity = commonDao.searchOneData(courseId, SQL_SEACH_TEACHER_ID);
				pointManagerService.pointDivided(entity.getTeacherId(), points.toString(), "A0003");
				result.setStatus(true);
			} else {
				result.setMessages(point.getMessages());
				result.setStatus(false);
			}
		}
		CourseMessageEntity entity = new CourseMessageEntity();
		entity.setContent("教师已同意你加入课程，请前往课程进行学习！");
		entity.setCourseId(courseId);
		entity.setStudentId(msg.getCreateUser());
		entity.setStuStatus("0");//0:未查看;1:已查看
		entity.setCreateTime(new Date());
		entity.setCreateUser(msg.getCreateUser());
		entity.setId(IDGenerator.genUID());
		entity.setCheckStatus("0");
		entity.setType(COURSE_MESSAGE_TYPE_FOUR);
		commonDao.insertData(entity, COURSE_MESSAGE_INSERT);
		updateMsgForCourse1(id);
		return result;

	}
	public CourseEntity getTeacherId(String courseId) {
		// 根据课程ID，查询导师ID
		return commonDao.searchOneData(courseId, SQL_SEACH_TEACHER_ID);
	}
	public ResultBean applyJoinCourse(String courseId) {
		ResultBean result = new ResultBean();
		CourseMessageEntity entity = new CourseMessageEntity();
		CourseEntity course = commonDao.searchOneData(courseId, COURSE_SELECT_BY_ID);
		entity.setContent(WebContext.getSessionUser().getPersonName()+"申请加入"+course.getTitle()+"!");
		entity.setCourseId(courseId);
		entity.setStudentId(course.getTeacherId());
		entity.setStuStatus("0");//0:未查看;1:已查看
		entity.setCreateTime(new Date());
		entity.setCreateUser(WebContext.getSessionUserId());
		entity.setId(IDGenerator.genUID());
		entity.setCheckStatus("-2");
		entity.setType(COURSE_MESSAGE_TYPE_FOUR);
		commonDao.insertData(entity, COURSE_MESSAGE_INSERT);
		result.setStatus(true);
		return result;
	}
	/**
	 * 加入userCourse
	 * 
	 * @param courseId
	 * @return
	 */
	public void joinUserCourse(String courseId) {
		UserCourseEntity uc = new UserCourseEntity();
		uc.setCourseId(courseId);
		uc.setUserId(WebContext.getSessionUserId());

		// 检索用户是否存在
		UserCourseEntity uu = commonDao.searchOneData(uc, SEARCH_USER_COURSE);

		uc.setJoinCourse(new BigDecimal(ALREADY_JOIN_COURSE));
		uc.setJoinDate(new Date());
		uc.setUpdateUser(uc.getUserId());

		// 将的第一章传给LastChapterId
		String chapter = commonDao.searchOneData(courseId, SQL_SEARCH_COURSE_FIRST_CHAPTER);
		if (chapter != null) {
			uc.setLastChapterId(chapter);
		}

		if (uu == null) {
			// 不存在，插入新数据
			uc.setCreateUser(uc.getUserId());
			commonDao.insertData(uc, INSERT_USER_COURSE);
		} else {
			// 存在，更新数据
			commonDao.updateData(uc, UPDATE_USER_COURSE);
		}
	}

	/**
	 * 加入userCourse
	 * 
	 * @param courseId
	 * @return
	 */
	public void joinUserCourse(String courseId,String userId) {
		UserCourseEntity uc = new UserCourseEntity();
		uc.setCourseId(courseId);
		uc.setUserId(userId);

		// 检索用户是否存在
		UserCourseEntity uu = commonDao.searchOneData(uc, SEARCH_USER_COURSE);

		uc.setJoinCourse(new BigDecimal(ALREADY_JOIN_COURSE));
		uc.setJoinDate(new Date());
		uc.setUpdateUser(uc.getUserId());

		// 将的第一章传给LastChapterId
		String chapter = commonDao.searchOneData(courseId, SQL_SEARCH_COURSE_FIRST_CHAPTER);
		if (chapter != null) {
			uc.setLastChapterId(chapter);
		}

		if (uu == null) {
			// 不存在，插入新数据
			uc.setCreateUser(uc.getUserId());
			commonDao.insertData(uc, INSERT_USER_COURSE);
		} else {
			// 存在，更新数据
			commonDao.updateData(uc, UPDATE_USER_COURSE);
		}
	}
	
	/**
	 * 推荐，学习某一，系统推荐当前下方向标签的最热
	 * 
	 * @param displayNum
	 * @param coursesId
	 * @return
	 */
	public ResultBean recommendedCourses(String displayNum, String courseId) {
		ResultBean result = new ResultBean();
		// 从session中取出登录用户的ID，判断用户是否登录
		if (WebContext.isGuest()) {
			result.setStatus(false);
			result.setMessages(getMessage(NULL_USER_ID));
			return result;
		}
		if (courseId == null || courseId.equals("")) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_CID_NULL));
			return result;
		}

		if (displayNum == null || displayNum.equals("") || StringUtil.toInteger(displayNum) == 0) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_PAGE_NUM));
			return result;
		}
		CourseEntity coursesEntity = new CourseEntity();
		coursesEntity.setId(courseId);
		coursesEntity.setPageNumber(1);
		coursesEntity.setPageSize(StringUtil.toInteger(displayNum));
		coursesEntity.setSortOrder("DESC");
		coursesEntity.setSortName("numStudy");
		return super.searchList4Page(coursesEntity, SEARCH_RECOMMEND);
	}

	/**
	 * 评价列表
	 * 
	 * @param coursesId
	 * @return
	 */
	public ResultBean courseEvaluationList(ScoreEntity scoreEntity) {
		String linkId = scoreEntity.getLinkId();
		String linkType = scoreEntity.getLinkType();
		ResultBean result = new ResultBean();
		// 链接ID为空
		assertNotEmpty(linkId, MSG_E_NULL_ID);
		// 链接类型为空
		assertNotEmpty(linkType, MSG_E_EMPTY_LINK_TYPE);
		// 链接类型错误：不等于COURSE
		if (!linkType.equalsIgnoreCase(LINK_TYPE_COURSE)) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_ERROR_LINK_TYPE));
			return result;
		}
		// 不存在
		if (linkType.equalsIgnoreCase(LINK_TYPE_COURSE)) {
			assertExist(linkId, SQL_IS_COURSE_EXIST, MSG_E_COURSE_NOT_EXIST);
		}
		scoreEntity = commonDao.searchOneData(scoreEntity, CourseDbConstants.SCORE_BY_LINKID);
		// 不存在
		if (scoreEntity == null) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_COURSE_NOT_EXIST));
			return result;
		}
		ScoreDataView scoreDataView = new ScoreDataView();
		scoreDataView.setScoreId(scoreEntity.getId());
		return commonDao.searchList4Page(scoreDataView, CourseDbConstants.SCORE_DATA_SEARCH);
	}

	/**
	 * 判断用户是否收藏该，true:已收藏，false:未收藏
	 * 
	 * @param uc
	 * @return 返回结果
	 * @author liuzhen
	 */
	public boolean isSaveCourse(UserCourseEntity uc) {
		uc = commonDao.searchOneData(uc, SEARCH_USER_COURSE);
		return (uc != null && uc.getFavorite() != null && uc.getFavorite().equals(ALREADY_SAVE));
	}

	/**
	 * 收藏/关注
	 * 
	 * @param courseId
	 * @author liuzhen
	 */
	public ResultBean collectCourse(String courseId) {
		ResultBean result = new ResultBean();
		// id为空
		assertNotEmpty(courseId, MSG_E_CID_NULL);
		// 不存在
		// 已发布，检索信息
		assertExist(courseId, SQL_IS_COURSE_EXIST, MSG_E_COURSE_NOT_EXIST);
		// 判断用户是否登录
		assertNotGuest();

		UserCourseEntity uc = new UserCourseEntity();
		uc.setCourseId(courseId);
		uc.setUserId(WebContext.getSessionUserId());
		// 检索用户是否存在
		UserCourseEntity uu = commonDao.searchOneData(uc, SEARCH_USER_COURSE);
		uc.setFavorite(new BigDecimal(ALREADY_SAVE));
		uc.setFavoriteDate(new Date());
		uc.setUpdateUser(uc.getUserId());
		if (uu == null) {
			// 不存在，插入新数据
			uc.setCreateUser(uc.getUserId());
			commonDao.insertData(uc, INSERT_USER_COURSE);
		} else {
			// 存在，更新数据
			commonDao.updateData(uc, UPDATE_USER_COURSE);
		}
		// 收藏数+1
		commonDao.updateData(courseId, UPDATE_COURSE_NUM_FAVORITE_ADD);
		result.setStatus(true);
		return result;
	}

	/**
	 * 用户取消收藏
	 * 
	 * @param courseId Id
	 * @return
	 */
	public ResultBean cancelCollectCourse(String courseId) {
		ResultBean result = new ResultBean();
		// id为空
		assertNotEmpty(courseId, MSG_E_CID_NULL);
		// 不存在
		assertExist(courseId, SQL_IS_COURSE_EXIST, MSG_E_COURSE_NOT_EXIST);
		// 判断用户是否登录
		assertNotGuest();

		UserCourseEntity uc = new UserCourseEntity();
		uc.setCourseId(courseId);
		uc.setUserId(WebContext.getSessionUserId());
		// 检索用户是否存在
		UserCourseEntity uu = commonDao.searchOneData(uc, SEARCH_USER_COURSE);
		uc.setFavorite(new BigDecimal(NOT_SAVE));
		uc.setFavoriteDate(new Date());
		uc.setUpdateUser(uc.getUserId());
		if (uu == null) {
			// 不存在用户未收藏
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_USER_NOT_FAVORITE));
		} else {
			// 存在，更新数据
			commonDao.updateData(uc, UPDATE_USER_COURSE);
			// 收藏人数-1
			commonDao.updateData(courseId, UPDATE_COURSE_NUM_FAVORITE_SUB);
		}
		result.setStatus(true);
		return result;
	}

	/**
	 * 对评价，插入评价信息
	 * 
	 * @param scoreEntity scoreDataEntity
	 * @author WUXIAOXIANG
	 */
	public ResultBean insertEvaluateCourse(ScoreEntity scoreEntity, ScoreDataEntity scoreDataEntity) {
		String linkId = scoreEntity.getLinkId();
		String linkType = scoreEntity.getLinkType();

		ResultBean result = new ResultBean();

		// 判断用户是否登录
		assertNotGuest();
		// 链接ID为空
		assertNotEmpty(linkId, MSG_E_EMPTY_LINK_ID);
		// 链接类型为空
		assertNotEmpty(linkType, MSG_E_EMPTY_LINK_TYPE);
		// 链接类型错误：不等于COURSE
		if (!LINK_TYPE_COURSE.equalsIgnoreCase(linkType)) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_ERROR_LINK_TYPE));
			return result;
		}
		// 不存在
		assertExist(linkId, SQL_IS_COURSE_EXIST, MSG_E_COURSE_NOT_EXIST);

		// 如果评价主题表中没有该下的数据，插入评价主题表
		ScoreEntity exsitScore = new ScoreEntity();
		exsitScore.setLinkId(scoreEntity.getLinkId());
		exsitScore.setLinkType(scoreEntity.getLinkType());
		exsitScore = commonDao.searchOneData(exsitScore, SEARCH_SCORE);
		if (exsitScore == null) {
			scoreEntity.setId(IDGenerator.genUID());
			scoreEntity.setScore(scoreDataEntity.getScore());
			scoreEntity.setSubScore1(scoreDataEntity.getSubScore1());
			scoreEntity.setSubScore2(scoreDataEntity.getSubScore2());
			scoreEntity.setSubScore3(scoreDataEntity.getSubScore3());
			scoreEntity.setCreateUser(WebContext.getSessionUserId());
			scoreEntity.setUpdateUser(WebContext.getSessionUserId());
			commonDao.insertData(scoreEntity, INSERT_SCORE);
		}
		// 插入评价数据表
		scoreDataEntity.setId(IDGenerator.genUID());
		scoreDataEntity.setScoreId(scoreEntity.getId());
		scoreDataEntity.setUserId(WebContext.getSessionUserId());
		scoreDataEntity.setCreateUser(WebContext.getSessionUserId());
		scoreDataEntity.setUpdateUser(WebContext.getSessionUserId());
		commonDao.insertData(scoreDataEntity, INSERT_SCORE_DATA);

		// 如果评价主题中存在该下的数据，计算平均分，更新评价主题表中的评价分字段，
		if (exsitScore != null) {
			commonDao.updateData(exsitScore, UPDATE_SCORE_AVG);
		}

		// 更新表的评价人数 +1
		commonDao.updateData(scoreEntity.getLinkId(), UPDATE_COURSE_NUM_SCORE_USER);

		result.setStatus(true);
		return result;
	}

	/**
	 * 检索 包括分类 directionTagId：方向标签ID categoryTagId： 分类标签ID
	 * courseStage:难度设置(0初级,1中级,2高级) sortType: new/hot 最新/最热 pageSize:(每页显示行数)
	 * pageNumber:1//(当前页)
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 */
	
	public String getCourseIdByUser(String userId) {
		String courseId = "";
		UserCourseEntity uCourse = new UserCourseEntity();
		uCourse.setUserId(userId);
		List<UserCourseEntity> ucList = commonDao.searchList(userId, SELECT_USER_COURSE_LISTS);
		if(ucList.size() == 1) {
			courseId = ucList.get(0).getCourseId();
		}else if(ucList.size() > 1) {
			//Todo 一个学生加入多个课程
			courseId = "more";
		}
		return courseId;
	}
	
	public ResultBean courseList(CourseManagerView course, String directionTagId, String categoryTagId,
			String courseStage, String sortType, Integer pageSize, Integer pageNumber,String schoolName) {
		ResultBean result = new ResultBean();
		/**
		 * 1.获取当前登录人员userId
		 * 2.根据userId查询user_role表中数据，role_id为04的是教师角色
		 * 3.判断登录用户是否为教师，若为教师，则查询course表中教师为当前登录用户的courseId
		 * 4.若登录用户是学生帐户，则查询user_course中此用户已加入课程的courseId
		 * 5.若登录用户为系统管理员，则查询所有课程
		 */
		course.setTagId(categoryTagId);
		course.setParentTagId(directionTagId);
		course.setCourseStage(courseStage);
		course.setLinkType("COURSE");
		course.setCourseLocation(COURSE_END_STATUS_NOT);
		if (sortType != null) {
			if (sortType.equals("new")) {
				course.setSortName("createDate");
				course.setSortOrder("DESC");
				course.setLibraryFlag("1");
			}
			if (sortType.equals("hot")) {
				course.setSortName("updateDate");
				course.setSortOrder("DESC");
				course.setLibraryFlag("0");
				course.setShareFlag(COURSE_SHARE_FLAG_ELEVEN);
			}
		}

		if (pageSize != null) {
			course.setPageSize(pageSize);
		}
		if (pageNumber != null) {
			course.setPageNumber(pageNumber);
		}
		result = searchList4Page(course, SELECT_COURSE_LIST);
		return result;
	}

	public List<CourseManagerView> mobileCourseList(CourseView course, String directionTagId, String categoryTagId,
			String courseStage, String sortType, String schoolName) {
		/**
		 * 1.获取当前登录人员userId
		 * 2.根据userId查询user_role表中数据，role_id为04的是教师角色
		 * 3.判断登录用户是否为教师，若为教师，则查询course表中教师为当前登录用户的courseId
		 * 4.若登录用户是学生帐户，则查询user_course中此用户已加入课程的courseId
		 * 5.若登录用户为系统管理员，则查询所有课程
		 */
		course.setTagId(categoryTagId);
		course.setParentTagId(directionTagId);
		course.setCourseStage(courseStage);
		course.setCourseLocation(COURSE_END_STATUS_NOT);
		if (sortType != null) {
			if (sortType.equals("new")) {
				course.setSortName("createDate");
				course.setSortOrder("DESC");
				course.setLibraryFlag("1");
			}
			if (sortType.equals("hot")) {
				course.setSortName("updateDate");
				course.setSortOrder("DESC");
				course.setLibraryFlag("0");
				course.setShareFlag(COURSE_SHARE_FLAG_ELEVEN);
			}
		}
		return commonDao.searchList(course, SELECT_COURSE_LIST);
	}
	
	public List<CourseEntity> getCourseListByStudentId(String userId) {
		CourseEntity entity = new CourseEntity();
		entity.setTeacherId(userId);
		 return commonDao.searchList(userId, SELECT_STUDENT_COURSE_LISTS);
	}
	
	public List<CourseTeamEntity> getCourseTeamListByCourseId(String courseId){
		CourseTeamEntity entity = new CourseTeamEntity();
		entity.setCourseId(courseId);
		return commonDao.searchList(entity, SQL_COURSE_TEAM_SELECT);
	}
	
	public ResultBean coursePublicList(CourseManagerView course,String sortType, Integer pageSize, Integer pageNumber) {
		ResultBean result = new ResultBean();
		course.setLinkType("COURSE");
		if (pageSize != null) {
			course.setPageSize(pageSize);
		}
		if (pageNumber != null) {
			course.setPageNumber(pageNumber);
		}
		result = searchList4Page(course, SELECT_NO_PERMISSIONS_COURSE_LIST);
		return result;
	}
	
	
	public List<CourseManagerView> mobileCoursePublicList(CourseManagerView course) {
		return commonDao.searchList(course, SELECT_NO_PERMISSIONS_COURSE_LIST);
	}
	
	@SuppressWarnings("unused")
	private boolean getUserRole(String userId) {
		UserRoleEntity ur = new UserRoleEntity();
		ur.setUserId(userId);
		ur.setRoleId("04");
		List<UserRoleEntity> list = commonDao.searchList(ur, SELECT_USER_ROLE);
		if(list.size() > 0) {
			return true;
		}else {
			return false;
		}
	}
	
	/**
	 * 检索教师信息
	 * 
	 * @author LILIANG
	 * @return UserEntity
	 */
	public ResultBean teacherInfo(UserEntity user) {
		ResultBean result = new ResultBean();
		String id = user.getUserId();
		// 判断用户是否登录
		assertNotGuest();
		// 链接ID为空
		if (StringUtil.isEmpty(id)) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_NULL_ID));
			return result;
		}
		result.setData(commonDao.searchOneData(user, SQL_USER_SEARCH));
		return result;
	}

	public void inserUserOperPC(UserOperEntity entity) {
		commonDao.insertData(entity, INSERT_USER_OPER);
	}
	
	public void insertUserLogPC(UserLogEntity entity) {
		commonDao.insertData(entity, INSERT_USER_LOG);
	}
	
	public List<HomeworkView> getHomeWorkByChapterId(String chapterId){
		HomeworkView homework = new HomeworkView();
		homework.setLinkId(chapterId);
		homework.setLinkType("CHAPTER");
		return commonDao.searchList(homework, SEARCH_USER_HOME_BY_LINKID);
	}
	
	public ResultBean saveSecen(HomeworkView homeworkView) throws Exception {
		ResultBean result = new ResultBean();
		HomeworkView homework = null;
		List<HomeworkView> list = commonDao.searchList(homeworkView, SEARCH_USER_HOME_BY_LINKID);
		if (list.size() > 0) {
			homework = list.get(0);
		}
		HomeworkAnswerEntity entity = commonDao.searchOneData(homeworkView, SELECT_HOME_WORK);
		if(entity != null) {
			entity.setAnswerUser(homeworkView.getAnswerUser());
			entity.setHomeworkId(homework.getId());
			entity.setFileId(homeworkView.getFileId());
			commonDao.updateData(entity, UPDATE_USER_HOME_WORK);
		}else {
			homeworkView.setId(IDGenerator.genUID());
			homeworkView.setHomeworkId(homework.getId());
			commonDao.insertData(homeworkView, SAVE_HOME_WORK_PC);
		}
		result.setStatus(true);
		result.setMessages("作业上传成功");

		return result;
	}
	
	/**
	 * 检索与教师相关的信息
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 */
	public ResultBean teacherCourseList(CourseEntity course) {
		ResultBean result = new ResultBean();
		String teacherId = course.getTeacherId();
		// 判断用户是否登录
		assertNotGuest();
		// 链接ID为空
		if (StringUtil.isEmpty(teacherId)) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_NULL_ID));
			return result;
		}
		course.setStatus(COURSE_STATUS_PUBLIC);
		/*course.setEndDate(new Date());*/
		course.setCrowdType(COURSE_CROWD_NOT_TRAIN);
		return commonDao.searchList4Page(course, SELECT_COURSE_LIST_ALL);
	}

	public void insertSysUser(String proName,String IpInfo) {
		SysUserEntity entity = new SysUserEntity();
		String userId = WebContext.getSessionUserId();
		String userName = WebContext.getSessionUser().getPersonName();
		entity.setUserId(userId);
		entity.setUserName(userName);
		entity.setOperIp(IpInfo);
		if(proName.equals("1")) {
			entity.setProName("应用支撑平台");
		}else if(proName.equals("2")) {
			entity.setProName("资源应用平台");
		}else if(proName.equals("3")) {
			entity.setProName("决策分析平台");
		}
		
		entity.setSystemTime(new Date());
		commonDao.insertData(entity, INSERT_SYS_USER);
	}
	
	public void inserUserOper(UserOperEntity entity) {
		String userId = WebContext.getSessionUserId();
		String userName = WebContext.getSessionUser().getPersonName();
		entity.setUserId(userId);
		entity.setOperUser(userName);
		commonDao.insertData(entity, INSERT_USER_OPER);
	}
	
	public void insertUserLog(UserLogEntity entity) {
		entity.setOperTime(new Date());
		commonDao.insertData(entity, INSERT_USER_LOG);
	}
	
	public TpUserEntity getUserByUsername(String username) {
		TpUserEntity entity = new TpUserEntity();
		entity.setUserName(username);
		return commonDao.searchOneData(entity, SEARCH_USER_BY_USERNAME);
	}
	/**
	 * 浏览数+1
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 */
	public ResultBean addCourseViewNum(CourseEntity course) {
		ResultBean result = new ResultBean();
		// 不存在或未发布
		assertNotEmpty(course.getId(), MSG_E_NULL_ID);
		// 不存在
		assertExist(course.getId(), SQL_IS_COURSE_EXIST, MSG_E_COURSE_NOT_EXIST);
		commonDao.updateData(course, UPDATE_COURSE_NUMVIEW);
		return result;
	}

	/**
	 * 根据ID，检索详情，包括基本信息， 用户信息，用户是否登录，当前用户是否对评价、关注等信息
	 * 
	 * @param courseId String ID
	 * @return ResultBean data:CourseView
	 */
	public ResultBean searchCourseDetail(String courseId) {
		PointTypeEntity ptr = (PointTypeEntity) WebContext.session(PointStateConstants.POINT_TYPE_VOTE);
		if (ptr == null) {
			PointTypeEntity ptc = new PointTypeEntity(PointConstants.SYSTEM_CODE_HS,
					PointStateConstants.POINT_TYPE_VOTE);
			ptc.setSumKbn(SUM_KBN_EARN);
			ptr = pointManagerService.searchOneData(ptc);
			WebContext.session(PointStateConstants.POINT_TYPE_VOTE, ptr);
		}
		assertNotEmpty(courseId, MSG_E_EMPTY_COURSE_ID);
		// 已发布，检索信息
		CourseView view = new CourseView();
		view.setId(courseId);
		assertExist(view, SQL_IS_COURSE_EXIST, MSG_E_EMPTY_COURSE);
		view.setUserId(WebContext.getSessionUserId());
		view = commonDao.searchOneData(view, SEARCH_COURSE_DETAIL_BY_ID);
		// 浏览数+1
		CourseEntity c = new CourseEntity();
		c.setId(courseId);
		addCourseViewNum(c);
		// 判断用户是否登录
		if (WebContext.isGuest()) {
			// 用户未登录
			view.setUserLogin(new BigDecimal(USER_LOGING_NOT));
			view.setJoinCourse(new BigDecimal(NOT_JOIN_COURSE));
		} else {
			// 用户已登录
			view.setUserLogin(new BigDecimal(USER_LOGING_ALREADY));
			// 检索用户
			UserCourseEntity uc = new UserCourseEntity();
			uc.setUserId(WebContext.getSessionUserId());
			uc.setCourseId(courseId);
			uc = searchUserCourse(uc);
			if (uc == null) {
				// 用户不存在
				// 新增用户
				uc = new UserCourseEntity();
				uc.setUserId(WebContext.getSessionUserId());
				uc.setCourseId(courseId);
				commonDao.insertData(uc, INSERT_USER_COURSE);
				// 用户未加入
				view.setJoinCourse(new BigDecimal(NOT_JOIN_COURSE));
				// 未收藏
				view.setFavorite(new BigDecimal(NOT_SAVE));
				// 未评价
				// view.setScoreCourse(new BigDecimal(NOT_SCORE_COURSE));
				// 未投票
				view.setVote("");
			} else {
				// 是否加入
				view.setJoinCourse(new BigDecimal(StringUtil.getOrElse(uc.getJoinCourse(), NOT_JOIN_COURSE)));
				view.setJoinDate(uc.getJoinDate());
				// 是否关注收藏
				view.setFavorite(uc.getFavorite());
				view.setFavoriteDate(uc.getFavoriteDate());

				// 是否投票
				view.setVote(uc.getVote());
				view.setVoteDate(uc.getVoteDate());
				view.setVoteContent(uc.getVoteContent());
				// 最近学习的章节ID
				view.setLastChapterId(uc.getLastChapterId());
				// 最近学习的章节标题
				ChapterEntity chapter = new ChapterEntity();
				chapter.setId(uc.getLastChapterId());
				chapter = searchChapter(chapter);
				if (chapter != null) {
					view.setLastChapterTitle(chapter.getTitle());
				} else {
					view.setLastChapterTitle("");
				}
				// 学习进度(百分比）
				view.setStudyProgress(uc.getStudyProgress());
				// 学习用时(秒)
				view.setTimeCost(uc.getTimeCost());
				MeasurementEntity query = new MeasurementEntity();
				query.setUserId(WebContext.getSessionUserId());
				query.setLinkId(courseId);
				int userMeasurement = searchOneData(query, "UserMeasurementResult.selectUserMeasurmentCount");
				view.setFinishQuestionnaireFlag(userMeasurement > 0 ? "1" : "0");

				if (StringUtil.isEmpty(view.getLastChapterId())) {
					String lastChapterId = commonDao.searchOneData(courseId, SQL_SEARCH_COURSE_FIRST_CHAPTER);
					view.setLastChapterId(lastChapterId);
				}
			}
		}
		if(view.getShareUserId() != null) {
			TpUserEntity user = commonDao.searchOneData(view.getShareUserId(), TpDbConstants.USER_SEARCH_NAME_BY_USERID);
			view.setShareUserId(user.getPersonName());
		}
		ResultBean result = new ResultBean();
		result.setStatus(true);
		result.setData(view);
		return result;
	}

	public ResultBean immediatePayDetail(String courseId) {
		ResultBean result = new ResultBean();
		assertNotEmpty(courseId, MSG_E_EMPTY_COURSE_ID);
		// 已发布，检索信息
		CourseView view = new CourseView();
		view.setId(courseId);
		assertExist(view, SQL_IS_COURSE_EXIST, MSG_E_EMPTY_COURSE);
		view = commonDao.searchOneData(view, SEARCH_COURSE_DETAIL_BY_ID);
		// 检索标签
		TagView tag = new TagView();
		tag.setLinkId(courseId);
		tag.setLinkType(LINK_TYPE_COURSE);
		List<TagView> tagList = commonDao.searchList(tag, CourseDbConstants.SQL_SEARCH_TAG_BY_COURSE_ID);
		if (tagList != null && tagList.size() > 0) {
			for (int i = 0; i < tagList.size(); i++) {
				if (tagList.get(i).getParentTagId() == null) {
					view.setFirstTag(tagList.get(i).getTagId());
					view.setFirstTagName(tagList.get(i).getTagName());
				} else {
					view.setSecondTag(tagList.get(i).getTagId());
					view.setSecondTagName(tagList.get(i).getTagName());
				}
			}
		}
		// 检索参与者的总积分
		UserPoint userPoint = pointService.fetchPoint(WebContext.getSessionUserId());
		// 计算支付后积分余额
		BigDecimal remainPoints = userPoint.getPoints()
				.subtract(view.getPrice().multiply(new BigDecimal(TpConfigUtil.getPointsForSmallChange())));
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("courseView", view);
		map.put("userPoint", userPoint);
		map.put("remainPoints", remainPoints);
		result.setData(map);
		result.setStatus(true);
		return result;
	}

	/**
	 * 判断是否发布
	 * 
	 * @param courseId ID
	 * @return true:已发布 false:未发布
	 */
	@SuppressWarnings("unused")
	private boolean isCoursePublish(String courseId) {
		CourseEntity c = new CourseEntity();
		c.setId(courseId);
		c.setStatus(COURSE_STATUS_PUBLIC);
		CourseEntity course = commonDao.searchOneData(c, SELECT_COURSE_LIST);
		if (course == null) {
			return false;
		}
		return true;
	}

	/***
	 * 检索用户
	 * 
	 * @param entity 用户Entity对象
	 * @return UserCourseEntity对象
	 */
	public UserCourseEntity searchUserCourse(UserCourseEntity entity) {
		// 不存在
		assertExist(entity.getCourseId(), SQL_IS_COURSE_EXIST, MSG_E_COURSE_NOT_EXIST);
		return commonDao.searchOneData(entity, SEARCH_USER_COURSE);
	}

	/***
	 * 根据章节ID检索章节信息
	 * 
	 * @param entity
	 * @return
	 */
	private ChapterEntity searchChapter(ChapterEntity entity) {
		return commonDao.searchOneData(entity, SEARCH_CHAPTER_BY_PK);
	}

	/**
	 * 检索用户评价数据
	 * 
	 * @param map 检索参数 linkId,userId,linkType
	 * @return true:评价过，false:未评价过
	 */
	public boolean checkUserScore(Map<String, Object> map) {
		int count = commonDao.searchCount(map, CHECK_USER_SCORE);
		return count > 0;
	}

	/**
	 * 根据ID建设
	 * 
	 * @param courseId ID
	 * @return CourseEntity
	 */
	public CourseEntity searchCourseById(String courseId) {
		// 不存在
		assertExist(courseId, SQL_IS_COURSE_EXIST, MSG_E_COURSE_NOT_EXIST);
		CourseEntity entity = new CourseEntity();
		entity.setId(courseId);
		return commonDao.searchOneData(entity, SEARCH_COURSE_BY_PK);
	}

	/**
	 * 检索我的列表（检索用户，包括已关注、已学、已学完等检索条件以及检索列表中每一项的问卷）
	 * 
	 * @param finishType 完成情况（已收藏 1、已学2、3 已学完）
	 * @return 我的列表
	 * @author WUXIAOXIANG
	 */
	public ResultBean userCourseList(String finishType, Integer pageSize, Integer pageNumber) {

		ResultBean result = new ResultBean();

		// 判断用户是否登录
		assertNotGuest();
		// 我的完成类型为空
//		assertNotEmpty(finishType, MSG_E_COURSE_FINISH_TYPE_NULL);

		// 当前登录用户
		CourseView courseView = new CourseView();
		courseView.setUserId(WebContext.getSessionUserId());
		courseView.setFinishType(finishType);
		courseView.setPageNumber(pageNumber);
		courseView.setPageSize(pageSize);
		// 课程list
		result = commonDao.searchList4Page(courseView, SELECT_USER_COURSE_ALL_LIST);
		return result;
	}
	
	public ResultBean userCompulsoryList(String courseType,Integer pageSize, Integer pageNumber) {
		ResultBean result = new ResultBean();
		// 判断用户是否登录
		assertNotGuest();
		// 当前登录用户
		CourseView courseView = new CourseView();
		courseView.setUserId(WebContext.getSessionUserId());
		courseView.setCourseType(courseType);
		courseView.setPageNumber(pageNumber);
		courseView.setPageSize(pageSize);
		// 课程list
		result = commonDao.searchList4Page(courseView, SELECT_USER_COURSE_ALL_LIST);
		return result;
	}
	
	/**
	 * 设置作业
	 * 
	 * @param linkId   链接ID
	 * @param linkType 链接类型
	 * @param title    题目标题
	 * @param content  题目内容
	 * @return
	 */
	public ResultBean saveTask(HomeworkView home) {

		ResultBean result = ResultBean.success();

		List<HomeworkView> list = commonDao.searchList(home, SEARCH_HOME_WORK);

		if (list.size() > 0) {
			commonDao.updateData(home, UPDATE_HOME_WORK);
		} else {
			home.setId(IDGenerator.genUID());
			commonDao.insertData(home, INSERT_HOME_WORK);
		}
		return result;
	}

	public ResultBean saveShare(CourseManagerView courseShare) {
		ResultBean result = ResultBean.success();
		courseShare.setShareFlag(COURSE_SHARE_FLAG_ONE);
		courseShare.setShareTime(new Date());
		courseShare.setShareUserId(WebContext.getSessionUserId());
		commonDao.updateData(courseShare, UPDATE_COURSE_SHARE);
		return result;
	}
	
	public ResultBean saveShare(CourseManagerView courseShare,CourseApproveEntity entity) throws Exception {
		ResultBean result = ResultBean.success();
		/*courseShare.setShareTime(new Date());
		courseShare.setShareUserId(WebContext.getSessionUserId());
		commonDao.updateData(courseShare, UPDATE_COURSE_SHARE);
		updateMsgForCourse(id);*/
		factoryCourse(courseShare.getId(),COURSE_SHARE_FLAG_ONE);
		updatApproveForCourse(entity);
		return result;
	}
	
	public ResultBean saveFactory(CourseManagerView courseShare,CourseApproveEntity entity) throws Exception {
		ResultBean result = ResultBean.success();
		factoryCourse(courseShare.getId(),COURSE_SHARE_FLAG_TWO);
//		courseShare.setShareTime(new Date());
//		courseShare.setShareUserId(WebContext.getSessionUserId());
//		commonDao.updateData(courseShare, UPDATE_COURSE_FACTORY);
		updatApproveForCourse(entity);
		
		return result;
	}
	
	public ResultBean saveCoursePublic(CourseManagerView courseShare,CourseApproveEntity entity) throws Exception {
		ResultBean bean = ResultBean.success();
		factoryCourse(courseShare.getId(),COURSE_SHARE_FLAG_THREE);
		//updateMsgForCourse(id);
		updatApproveForCourse(entity);
		return bean;
	}
	
	public ResultBean saveLib(CourseManagerView courseShare,String id) throws Exception {
		ResultBean bean = ResultBean.success();
		CourseManagerView query = new CourseManagerView();
		query.setId(courseShare.getId());
		// 课程信息
		CourseManagerView result = commonDao.searchOneData(query, SEARCH_COURSE_BY_ID);
		TagView tag = new TagView();
		tag.setLinkId(id);
		tag.setLinkType(LINK_TYPE_COURSE);
		// 课程标签信息
		List<TagView> tagList = commonDao.searchList(tag, CourseDbConstants.SQL_SEARCH_TAG_BY_COURSE_ID);
		if (tagList != null && tagList.size() > 0) {
			for (int i = 0; i < tagList.size(); i++) {
				if (tagList.get(i).getParentTagId() == null) {
					result.setFirstTag(tagList.get(i).getTagId());
				} else {
					result.setSecondTag(tagList.get(i).getTagId());
				}
			}
		}
		EntityUtil.copyEntity(result, query);
		query.setId(IDGenerator.genUID());
		query.setStatus(COURSE_END_STATUS_YES);
		query.setTitle(query.getTitle());
		query.setStartDate(sdf.format(new Date()));
		query.setEndDate("2099-12-31");
		query.setCrowdType("1");
		query.setTeacherId(result.getTeacherId());
		query.setEndFlag(result.getEndFlag());
		query.setLibraryFlag("0");
		query.setClassFlag("0");
		query.setShareFlag(COURSE_SHARE_FLAG_TWENTY);
		query.setCourseLocation(result.getCourseLocation() == null ? "0" : result.getCourseLocation());
		query.setLibraryFlag(COURSE_END_STATUS_YES);
		FileEntity file = fileService.getFile(query.getPictureId(), "image");
		File picFile = new File(PropertyUtil.getProperty("res.storage.root")+file.getPath());
		FileInputStream is = new FileInputStream(picFile);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] buff = new byte[1024];
		int len = 0;
		while ((len = is.read(buff)) != -1) {
		    bos.write(buff, 0, len);
		}
		String newId = IDGenerator.genUID();
		// 得到图片的字节数组
		byte[] resultByte = bos.toByteArray();
		OutputStream os = new FileOutputStream(PropertyUtil.getProperty("res.storage.pic")+newId+".png");
		os.write(resultByte, 0, resultByte.length);
		os.flush();
		os.close();
		FileEntity newPic = new FileEntity();
		newPic.setContentType("png");
		newPic.setCreateUser(WebContext.getSessionUserId());
		newPic.setCreateDate(new Date());
		newPic.setFileId(newId);
		newPic.setFileName(newId + ".png");
		newPic.setFileSize(file.getFileSize());
		newPic.setPath("/image/pic/"+newId+".png");
		newPic.setType("image");
		fileService.save(newPic);
		query.setPictureId(newId);
		commonDao.insertData(query, COURSE_INSERT);
		joinUserCourse(query.getId());
		List<ChapterView> chapterList = chapterService.getChapterListByCourseId(id);
		//保存原有课程章节
		saveFactoryCourseChapter(chapterList,query.getId());
		// 新增分类信息
		if (StringUtil.isNotEmpty(query.getFirstTag())) {
			insertCatagorys(query.getId(), query.getFirstTag(), query.getSecondTag());
		}
		updateMsgForCourse(id);
		return bean;
	}
	
	public ResultBean savePublic(CourseManagerView courseShare,String id) {
		ResultBean result = ResultBean.success();
		courseShare.setShareTime(new Date());
		courseShare.setShareUserId(WebContext.getSessionUserId());
		commonDao.updateData(courseShare, UPDATE_COURSE_PUBLIC);
		updateMsgForCourse(id);
		return result;
	}
	
	/**
	 * 检索章节作业
	 */
	public HomeworkView searchCoChHomeWork(HomeworkView home) {
		List<HomeworkView> list = commonDao.searchList(home, SEARCH_HOME_WORK);
		if (list.size() > 0) {
			return list.get(0);
		} else {
			return home;
		}
	}

	/**
	 * 检索我的作业
	 * 
	 * @param linkId
	 * @param linkType
	 * @param
	 * @return
	 */
	public ResultBean honeworkDetail(HomeworkView homeworkView) {
		ResultBean result = new ResultBean();

		// 链接ID为空
		if (StringUtil.isEmpty(homeworkView.getLinkId())) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_NULL_ID));
			return result;
		}

		// 链接类型为空
		if (StringUtil.isEmpty(homeworkView.getLinkType())) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_EMPTY_LINK_TYPE));
			return result;
		}

		// 链接类型错误：不等于COURSE or CHAPTER
		if (!homeworkView.getLinkType().equalsIgnoreCase(LINK_TYPE_COURSE)
				&& !homeworkView.getLinkType().equalsIgnoreCase(LINK_TYPE_CHAPTER)) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_ERROR_LINK_TYPE));
			return result;
		}

		// 不存在
		if (homeworkView.getLinkType().equalsIgnoreCase(LINK_TYPE_COURSE)) {
			assertExist(homeworkView.getLinkId(), SQL_IS_COURSE_EXIST, MSG_E_COURSE_NOT_EXIST);
		}
		// 章节不存在
		if (LINK_TYPE_CHAPTER.equalsIgnoreCase(homeworkView.getLinkType())) {
			assertExist(homeworkView.getLinkId(), SQL_IS_CHAPTER_EXIST, MSG_E_CHAPTER_NOT_EXIST);
		}
		homeworkView.setAnswerUser(WebContext.getSessionUserId());

		HomeworkView homework = null;
		List<HomeworkView> list = commonDao.searchList(homeworkView, SEARCH_HOME_WORK_ONE);
		if (list.size() > 0) {
			homework = list.get(0);
		}
		if (homework == null) {
			List<HomeworkView> lis = commonDao.searchList(homeworkView, SEARCH_HOME_WORK);
			if (lis.size() > 0) {
				homework = lis.get(0);
			}
		}
		result.setData(homework);

		return result;
	}

	public ResultBean honeworkList(HomeworkView homeworkView){
		assertNotEmpty(homeworkView.getLinkId(), MSG_E_NO_HOME_WORK);
		ResultBean result = new ResultBean();
		// 优秀
		if(NOTE_SEARCH_LIKE.equals(homeworkView.getPageType())){
			homeworkView.setSortName("likeNum");
			homeworkView.setSortOrder("DESC");
		} else {
			// 默认全部 
			homeworkView.setSortName("createDate");
			homeworkView.setSortOrder("DESC");
		}
		String loginUserId = WebContext.getSessionUserId();
		HomeworkView viewEntity = new HomeworkView();
		viewEntity.setLinkId(homeworkView.getLinkId());
		viewEntity = commonDao.searchOneData(viewEntity, COURSE_SELECT_USERSTEPSCORE);
		if(null!=viewEntity) {
			homeworkView.setCourseId(viewEntity.getCourseId());
		}
		String courseTeacherId = commonDao.searchOneData(homeworkView.getCourseId(), AC_SELECT_COURSE_TEACHER);
		if(loginUserId.equals(courseTeacherId) || loginUserId.equals("admin")) {
			result = searchList4Page(homeworkView,SEARCH_ALL_USER_HOME);
		}else {
			homeworkView.setAnswerUser(loginUserId);
			result = searchList4Page(homeworkView,SEARCH_USER_HOME);
		}
		return result;
	}
	
	public List<HomeworkView> gethomeworkReviewListJson(HomeworkView homeworkView){
		List<HomeworkView> workList  = new ArrayList<HomeworkView>();
		HomeworkView home = new HomeworkView();
		home.setLinkId(homeworkView.getLinkId());
		home.setLinkType(homeworkView.getLinkType());
		home.setAudioType(homeworkView.getAudioType());
		/**
		 * 根据登录用户显示结果列表
		 * 1. 若为课程老师或者系统管理员，显示所有同学信息信息
		 * 2. 若为同学仅显示自己的成绩信息
		 * 3.若为课程小组组长，显示小组所有成员成绩信息
		 * update,4，若为同学，显示自己的成绩及小组其他成员的成绩信息
		 * */
		String loginUserId = WebContext.getSessionUserId();
		String courseTeacherId = commonDao.searchOneData(homeworkView.getCourseId(), AC_SELECT_COURSE_TEACHER);
		CourseTeamEntity team = new CourseTeamEntity();
		team.setCourseId(homeworkView.getCourseId());
		CourseTeamEntity entity = null;
		List<CourseTeamUserEntity> teamUserList = null;
		List<String> userCourseList = new ArrayList<String>();
		if(loginUserId.equals(courseTeacherId) || loginUserId.equals("2542668")) {
			team.setCourseId(homeworkView.getCourseId());
			List<CourseTeamEntity> teamList = commonDao.searchList(team, SQL_COURSE_TEAM_SELECT);
			if(teamList.size() > 0 ) {
				for(CourseTeamEntity t: teamList) {
					userCourseList.add(t.getId());
				}
				home.setUserCourseList(userCourseList);
				home.setAudioType("2");
				workList = commonDao.searchList(home, SEARCH_TEAM_HOME);
				
			}
			home.setAudioType("1");
			List<HomeworkView> workList1 = commonDao.searchList(home, SEARCH_ALL_USER_HOME);
			workList.addAll(workList1);
		}
		if(homeworkView.getAudioType().equals("2")) {
			if(loginUserId.equals("2542668") || loginUserId.equals(courseTeacherId)) {
			}else {
				team.setTeamLeader(loginUserId);
				entity = commonDao.searchOneData(team, SQL_COURSE_TEAM_SELECT_BY_TEAMLEADER);
				if(entity != null) {
					//获取小组作业
					userCourseList.add(entity.getId());
					home.setUserCourseList(userCourseList);
					workList = commonDao.searchList(home, SEARCH_TEAM_HOME);
					//获取小组成员作业
					CourseTeamUserEntity user = new CourseTeamUserEntity();
					user.setTeamId(entity.getId());
					teamUserList = commonDao.searchList(user, SQL_COURSE_TEAM_USER_SELECT);
					if(teamUserList.size()> 0) {
						userCourseList.clear();
						for(CourseTeamUserEntity u : teamUserList) {
							if(!u.getUserId().equals(loginUserId)) {
								userCourseList.add(u.getUserId());
							}
						}
						home.setUserCourseList(userCourseList);
						home.setAudioType("1");
						List<HomeworkView> workList1 = commonDao.searchList(home, SEARCH_USER_HOME_BY_USER);
						workList.addAll(workList1);
					}
				}else {
					team.setTeamLeader(loginUserId);
					entity = commonDao.searchOneData(team, SQL_COURSE_TEAM_SELECT_BY_TEAM_USER);
					if(entity != null) {
						CourseTeamUserEntity user = new CourseTeamUserEntity();
						user.setTeamId(entity.getId());
						teamUserList = commonDao.searchList(user, SQL_COURSE_TEAM_USER_SELECT);
						if(teamUserList.size()> 0) {
							userCourseList.clear();
							for(CourseTeamUserEntity u : teamUserList) {
								if(!u.getUserId().equals(loginUserId)) {
									userCourseList.add(u.getUserId());
								}
							}
							home.setUserCourseList(userCourseList);
							home.setAudioType("1");
							workList = commonDao.searchList(home, SEARCH_USER_HOME_BY_USER);
						}
					}
				}
			}
			
		}else if(homeworkView.getAudioType().equals("1")){
			if(loginUserId.equals(courseTeacherId) || loginUserId.equals("2542668")) {
			}else {
				userCourseList.add(loginUserId);
				home.setUserCourseList(userCourseList);
				workList = commonDao.searchList(home, SEARCH_USER_HOME_BY_USER);
			}
		}
		return workList;
	}
	
	public HomeworkEntity getHomeWorkByLinkId(HomeworkView homeworkView) {
		assertNotEmpty(homeworkView.getLinkId(), MSG_E_NO_HOME_WORK);
		return commonDao.searchOneData(homeworkView, SEARCH_HOME_WORK_BY_LINK_ID);
	}
	
	public HomeworkView getHomeWorkById(String id) {
		HomeworkView entity = new HomeworkView();
		entity.setId(id);
		entity.setStatus(HOMEWORK_ANSWER_STATUS_TWO);
		commonDao.updateData(entity, UPDATE_HOME_WORK_ANSWER_STATUS);
		return commonDao.searchOneData(entity, SEARCH_HOME_WORK_BY_ID);
	}
	
	public HomeworkAnswerEntity getCourseNameByAnswerId(String id) {
		HomeworkAnswerEntity entity = new HomeworkAnswerEntity();
		entity.setId(id);
		return commonDao.searchOneData(entity, SEARCH_NAMES_BY_ANSWER_ID);
	}
	
	public HomeworkView viewHomeWorkById(String id) {
		HomeworkView entity = new HomeworkView();
		entity.setId(id);
		return commonDao.searchOneData(entity, SEARCH_HOME_WORK_BY_ID);
	}
	/**
	 * 上传作业文件
	 * 
	 * @param homeworkView
	 * @return
	 * @throws Exception
	 */
	public ResultBean uploadDocument(HomeworkView homeworkView) throws Exception {
		ResultBean result = new ResultBean();
		assertNotGuest();
		FileEntity resultData = processRequestFile("homework", FILE_TYPE_HOMEWORK);
		if (resultData == null) {
			return result.setStatus(false).setMessages("请上传文件");

		}
		HomeworkView homework = null;
		List<HomeworkView> lis = commonDao.searchList(homeworkView, SEARCH_HOME_WORK);
		if (lis.size() > 0) {
			homework = lis.get(0);
		}
		homeworkView.setId(IDGenerator.genUID());
		homeworkView.setAnswerUser(WebContext.getSessionUserId());
		homeworkView.setFileId(resultData.getFileId());
		homeworkView.setHomeworkId(homework.getId());
		commonDao.insertData(homeworkView, UPLOAD_HOME_WORK);

		result.setStatus(true);
		result.setMessages("作业上传成功");

		return result;
	}

	/**
	 * 上传作业文件
	 * 
	 * @param homeworkView
	 * @return
	 * @throws Exception
	 */
	public ResultBean saveAudio(HomeworkView homeworkView) throws Exception {
		ResultBean result = new ResultBean();
		HomeworkView homework = null;
		List<HomeworkView> list = commonDao.searchList(homeworkView, SEARCH_USER_HOME_BY_LINKID);
		if (list.size() > 0) {
			homework = list.get(0);
		}
		HomeworkAnswerEntity entity = commonDao.searchOneData(homeworkView, SELECT_HOME_WORK);
		if(entity != null) {
			entity.setAnswerUser(homeworkView.getAnswerUser());
			entity.setHomeworkId(homework.getId());
			entity.setFileId(homeworkView.getFileId());
			entity.setAudioId(homeworkView.getAudioId());
			entity.setAudioType(homeworkView.getAudioId());
			entity.setAudioContent(homeworkView.getAudioContent());
			commonDao.updateData(entity, UPDATE_USER_HOME_WORK);
		}else {
			homeworkView.setId(IDGenerator.genUID());
			homeworkView.setHomeworkId(homework.getId());
			commonDao.insertData(homeworkView, SAVE_HOME_WORK);
		}
		result.setStatus(true);
		result.setMessages("作业上传成功");

		return result;
	}
	
	public List<HomeworkView> getHomeListByUserIdChapterId(String userId,String chapterId){
		HomeworkView home = new HomeworkView();
		home.setLinkId(chapterId);
		home.setAnswerUser(userId);
		return commonDao.searchList(home, SEARCH_HOME_BY_USER_CHAPTER);
	}
	
	public ResultBean saveHomeWorkAnswer(HomeworkView homeworkView) throws Exception {
		FileEntity resultData = processRequestFile("file2", "homework");
		if(resultData != null) {
			homeworkView.setFileId(resultData.getFileId());
		}
		homeworkView.setAnswerUser(WebContext.getSessionUserId());
		ResultBean result = new ResultBean();
		HomeworkView homework = null;
		List<HomeworkView> list = commonDao.searchList(homeworkView, SEARCH_USER_HOME_BY_LINKID);
		if (list.size() > 0) {
			homework = list.get(0);
			homeworkView.setHomeworkId(homework.getId());
		}
		HomeworkAnswerEntity entity = commonDao.searchOneData(homeworkView, SELECT_HOME_WORK);
		if(entity != null) {
			entity.setAnswerUser(homeworkView.getAnswerUser());
			entity.setHomeworkId(homework.getId());
			entity.setFileId(homeworkView.getFileId());
			entity.setContent(homeworkView.getAnswerContent());
			entity.setUpdateUser(WebContext.getSessionUserId());
			entity.setUpdateDate(new Date());
			commonDao.updateData(entity, UPDATE_USER_HOME_WORK);
		}else {
			homeworkView.setId(IDGenerator.genUID());
			homeworkView.setHomeworkId(homework.getId());
			homeworkView.setCreateDate(new Date());
			homeworkView.setCreateUser(WebContext.getSessionUserId());
			homeworkView.setContent(homeworkView.getAnswerContent());
			commonDao.insertData(homeworkView, UPLOAD_HOME_WORK);
		}
		result.setStatus(true);
		result.setMessages("作业上传成功");

		return result;
	}
	
	public ResultBean saveHomeWorkById(HomeworkView homeworkView) throws Exception {
		ResultBean result = new ResultBean();
		FileEntity resultData = processRequestFile("file1", "homework");
		if(resultData != null) {
			homeworkView.setFileId(resultData.getFileId());
		}
		HomeworkAnswerEntity entity = commonDao.searchOneData(homeworkView, SELECT_HOME_WORK);
		if(entity != null) {
			entity.setFileId(homeworkView.getFileId());
			entity.setContent(homeworkView.getAnswerContent());
			entity.setUpdateUser(WebContext.getSessionUserId());
			entity.setUpdateDate(new Date());
			commonDao.updateData(entity, UPDATE_USER_HOME_WORK);
		}
		result.setStatus(true);
		result.setMessages("作业上传成功");
		return result;
	}
	
	public ResultBean saveHomeWorkReviewById(HomeworkView homeworkView) throws Exception {
		ResultBean result = new ResultBean();
		HomeworkAnswerEntity entity = commonDao.searchOneData(homeworkView, SELECT_HOME_WORK);
		if(entity != null) {
			entity.setTeacherEvalution(homeworkView.getContent());
			entity.setStatus(HOMEWORK_ANSWER_STATUS_THREE);
			entity.setScore(homeworkView.getScore());
			entity.setTeacherComment(homeworkView.getTeacherComment());
			commonDao.updateData(entity, UPDATE_TEACHER_REVIEW_HOME_WORK);
		}
		result.setStatus(true);
		result.setMessages("作业点评成功");
		return result;
	}
	
	public ResultBean saveHomeWorkAnswerQuestion(HomeworkAnswerQuestionEntity aq) throws Exception {
		ResultBean result = new ResultBean();
		aq.setId(IDGenerator.genUID());
		aq.setCreateDate(new Date());
		aq.setCreateUser(WebContext.getSessionUserId());
		commonDao.insertData(aq, HOME_WORK_ANSWER_QUESTION_INSERT);
		result.setStatus(true);
		result.setMessages("评论保存成功");
		return result;
	}
	
	public ResultBean searchAnswerQuestionList(HomeworkAnswerQuestionEntity aq) {
		//ResultBean result = new ResultBean();
		//HomeworkAnswerQuestionEntity entity = new HomeworkAnswerQuestionEntity();
		//entity.setAnswerId(answerId);
		//result = commonDao.searchList4Page(entity, HOME_WORK_ANSWER_QUESTION_SEARCH);
		return super.searchList4Page(aq,
				HOME_WORK_ANSWER_QUESTION_SEARCH);
		//return result;
	}
	
	public ResultBean likeHomeWorkById(String id ) throws Exception {
		ResultBean result = new ResultBean();
		HomeworkView homeworkView = new HomeworkView();
		homeworkView.setId(id);
		HomeworkAnswerEntity entity = commonDao.searchOneData(homeworkView, SELECT_HOME_WORK_BY_ID);
		if(entity != null) {
			BigDecimal likeNum = entity.getLikeNum();
			if(likeNum != null) {
				homeworkView.setLikeNum(new BigDecimal(likeNum.intValue()+1));
			}else {
				homeworkView.setLikeNum(new BigDecimal(1));
			}
		}
		commonDao.updateData(homeworkView, UPDATE_USER_HOME_WORK_LIKE);
		result.setStatus(true);
		result.setMessages("点赞成功");
		return result;
	}
	
	public List<HomeworkView> gethomeworkById(String courseId,String linkId,String type){
		HomeworkView home = new HomeworkView();
		home.setLinkId(linkId);
		home.setLinkType("CHAPTER");
		home.setAudioType(type);
		if(type.equals("1")) {
			return commonDao.searchList(home, SEARCH_ALL_USER_HOME);
		}else if(type.equals("2")) {
			return commonDao.searchList(home, SEARCH_ALL_TEAM_HOME);
		}else {
			return null;
		}
	}
	
	public String saveScoreById(HomeworkView home) {
		int i = commonDao.updateData(home, UPDATE_HOME_WORK_SCORE);
		if(i > 0) {
			return "success";
		}else {
			return "error";
		}
	}
	
	public HomeworkView searchworkDetail(String courseId, String linkType) {
		Map<String, String> map = new HashMap<String, String>();
		map.put("linkId", courseId);
		map.put("linkType", linkType);
		HomeworkView homework = null;

		if (linkType.equalsIgnoreCase(LINK_TYPE_COURSE)) {

			List<HomeworkView> list = commonDao.searchList(map, SEARCH_HOME_WORK_TITLE);
			if (list.size() > 0) {
				homework = list.get(0);
			}
		} else {
			List<HomeworkView> list = commonDao.searchList(map, SEARCH_HOME_WORK_TITLE_C);
			if (list.size() > 0) {
				homework = list.get(0);
			}
		}

		if (homework == null) {
			homework = new HomeworkView();
		}

		return homework;
	}

	public List<HomeworkView> gethomeworkByUCId(String userId,String linkId){
		HomeworkView home = new HomeworkView();
		home.setLinkId(linkId);
		home.setLinkType("CHAPTER");
		if(!userId.equals("-1")) {
			home.setAnswerUser(userId);
		}
		return commonDao.searchList(home, SEARCH_ALL_USER_HOME);
	}
	
	public List<HomeworkView> gethomeworkById(String courseId,String linkId,String teamId,String type){
		HomeworkView home = new HomeworkView();
		home.setLinkId(linkId);
		home.setLinkType("CHAPTER");
		home.setAudioType(type);
		CourseTeamUserEntity user = new CourseTeamUserEntity();
		user.setTeamId(teamId);
		List<CourseTeamUserEntity> teamUserList = commonDao.searchList(user, SQL_COURSE_TEAM_USER_SELECT);
		List<String> userCourseList = new ArrayList<String>();
		if(teamUserList.size()> 0) {
			for(CourseTeamUserEntity u : teamUserList) {
				userCourseList.add(u.getUserId());
			}
			home.setUserCourseList(userCourseList);
			return commonDao.searchList(home, SEARCH_TEAM_USERS_HOME_BY_USER);
		}else {
			return null;
		}
	}
	
	/**
	 * 
	 * @param linkId
	 * @param linkType
	 * @return
	 */
	public ResultBean homeworkReviewList(HomeworkView home,String path) {
		ResultBean result = new ResultBean();

		if (home.getLinkType().equalsIgnoreCase(LINK_TYPE_COURSE)) {
			// 全部参加的用户
			result = commonDao.searchList4Page(home, SEARCH_USER_COUSE);

			//List<UserEntity> userList = (List<UserEntity>) result.getData();
			// 该的全部提交的作业
			List<HomeworkView> workList = commonDao.searchList(home, SEARCH_TEACHER_USER_HOME);
			// 返回的数据
			//List<HomeworkView> homeList = getHomeList(userList, workList);
			return result.setData(workList);
		} else if (home.getLinkType().equalsIgnoreCase(LINK_TYPE_CHAPTER)) {
			// 全部参加章节的用户
			result = commonDao.searchList4Page(home, SEARCH_CHAPTER_USER);

			//List<UserEntity> userList = (List<UserEntity>) result.getData();
			// 该章节的全部提交的作业
			List<HomeworkView> workList = commonDao.searchList(home, SEARCH_TEACHER_USER_HOME);
			for(HomeworkView view : workList) {
				FileEntity file = getFile(view.getAnswerFileId(), FILE_TYPE_HOMEWORK);
				view.setAnswerFileName(path+"/"+file.getPath());
				view.setIsdown(true);
			}
			// 返回的数据
			//List<HomeworkView> homeList = getHomeList(userList, workList);
			return result.setData(workList);
		}
		return result.setStatus(false).setMessages("类型不存在");

	}

	public ResultBean gethomeworkReviewList(HomeworkView home) {
		ResultBean result = new ResultBean();
		List<HomeworkView> workList = commonDao.searchList(home, SEARCH_USER_HOME);
		if(workList.size() > 0) {
			for(HomeworkView view : workList) {
				FileEntity file = getFile(view.getAnswerFileId(), FILE_TYPE_HOMEWORK);
				view.setAnswerFileName(file.getPath());
			}
		}
		
		return result.setData(workList);

	}
	
	public List<HomeworkView> gethomeworkReviewListJson(String linkId,String linkType,String path){
		HomeworkView home = new HomeworkView();
		home.setLinkId(linkId);
		home.setLinkType(linkType);
		List<HomeworkView> workList = commonDao.searchList(home, SEARCH_USER_HOME);
		if(workList.size() > 0) {
			for(HomeworkView view : workList) {
				FileEntity file = getFile(view.getAnswerFileId(), FILE_TYPE_HOMEWORK);
				view.setAnswerFileName(path+"/"+file.getPath());
			}
		}
		return workList;
	}
	
	public List<HomeworkView> gethomeworkReviewListJson(String courseId,String linkId,String linkType,String path){
		List<HomeworkView> workList  = new ArrayList<HomeworkView>();
		HomeworkView home = new HomeworkView();
		home.setLinkId(linkId);
		home.setLinkType(linkType);
		/**
		 * 根据登录用户显示结果列表
		 * 1. 若为课程老师或者系统管理员，显示所有同学信息信息
		 * 2. 若为同学仅显示自己的成绩信息
		 * */
		String loginUserId = WebContext.getSessionUserId();
		String courseTeacherId = commonDao.searchOneData(courseId, AC_SELECT_COURSE_TEACHER);
		if(loginUserId.equals(courseTeacherId) || loginUserId.equals("admin")) {
			workList = commonDao.searchList(home, SEARCH_ALL_USER_HOME);
		}else {
			home.setAnswerUser(loginUserId);
			workList = commonDao.searchList(home, SEARCH_USER_HOME);
		}
		if(workList.size() > 0) {
			for(HomeworkView view : workList) {
				FileEntity file = getFile(view.getAnswerFileId(), FILE_TYPE_HOMEWORK);
				view.setAnswerFileName(path+"/"+file.getPath());
			}
		}
		return workList;
	}
	
	private List<HomeworkView> getHomeList(List<UserEntity> userList, List<HomeworkView> workList) {
		List<HomeworkView> homeList = new ArrayList<HomeworkView>();
		Map<String, HomeworkView> map = new HashMap<String, HomeworkView>();
		if (workList != null && workList.size() != 0) {
			for (HomeworkView homee : workList) {
				String key = homee.getAnswerUser();
				map.put(key, homee);
			}
		}
		if (userList != null && userList.size() != 0) {
			for (UserEntity user : userList) {
				if (map.containsKey(user.getUserId())) {
					for(HomeworkView homee : workList) {
						HomeworkView hom = homee;
						hom.setUserName(user.getUserName());
						hom.setSex(user.getSex());
						hom.setIsdown(true);
						homeList.add(hom);
					}
				} else {
					HomeworkView hom = new HomeworkView();
					hom.setUserName(user.getUserName());
					hom.setSex(user.getSex());
					homeList.add(hom);
				}
			}
		}		return homeList;
	}

	public ResultBean saveHomeworkReview(HomeworkView home) {
		ResultBean result = new ResultBean();
		commonDao.updateData(home, UPDATE_HOME_WORK_AN);
		return result;
	}

	/**
	 * 复制课程
	 * 
	 * @param id ID
	 * @throws Exception 
	 */
	public void copyCourse(String id) throws Exception {
		CourseManagerView query = new CourseManagerView();
		query.setId(id);
		// 课程信息
		CourseManagerView result = commonDao.searchOneData(query, SEARCH_COURSE_BY_ID);
		TagView tag = new TagView();
		tag.setLinkId(id);
		tag.setLinkType(LINK_TYPE_COURSE);
		// 课程标签信息
		List<TagView> tagList = commonDao.searchList(tag, CourseDbConstants.SQL_SEARCH_TAG_BY_COURSE_ID);
		if (tagList != null && tagList.size() > 0) {
			for (int i = 0; i < tagList.size(); i++) {
				if (tagList.get(i).getParentTagId() == null) {
					result.setFirstTag(tagList.get(i).getTagId());
				} else {
					result.setSecondTag(tagList.get(i).getTagId());
				}
			}
		}
		EntityUtil.copyEntity(result, query);
		FileEntity file = fileService.getFile(query.getPictureId(), "image");
		File picFile = new File(PropertyUtil.getProperty("res.storage.root")+file.getPath());
		FileInputStream is = new FileInputStream(picFile);
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] buff = new byte[1024];
		int len = 0;
		while ((len = is.read(buff)) != -1) {
		    bos.write(buff, 0, len);
		}
		String newId = IDGenerator.genUID();
		// 得到图片的字节数组
		byte[] resultByte = bos.toByteArray();
		OutputStream os = new FileOutputStream(PropertyUtil.getProperty("res.storage.pic")+newId+".png");
		os.write(resultByte, 0, resultByte.length);
		os.flush();
		os.close();
		FileEntity newPic = new FileEntity();
		newPic.setContentType("png");
		newPic.setCreateUser(WebContext.getSessionUserId());
		newPic.setCreateDate(new Date());
		newPic.setFileId(newId);
		newPic.setFileName(newId + ".png");
		newPic.setFileSize(file.getFileSize());
		newPic.setPath("/image/pic/"+newId+".png");
		newPic.setType("image");
		fileService.save(newPic);
		query.setId(IDGenerator.genUID());
		query.setPictureId(newPic.getFileId());
		query.setStatus(COURSE_STATUS_NOT);
		query.setTitle(query.getTitle() + "-副本");
		query.setStatus(result.getStatus());
		query.setStartDate(sdf.format(new Date()));
		query.setEndDate("2099-12-31");
		query.setCrowdType("1");
		query.setTeacherId(WebContext.getSessionUserId());
		query.setEndFlag(COURSE_END_STATUS_NOT);
		query.setLibraryFlag("0");
		query.setClassFlag("0");
		query.setShareFlag(COURSE_SHARE_FLAG_ELEVEN);
		commonDao.insertData(query, COURSE_INSERT);
		List<ChapterView> chapterList = chapterService.getChapterListByCourseId(id);
		//保存原有课程章节
		saveCopyCourseChapter(chapterList,query.getId());
		// 新增分类信息
		if (StringUtil.isNotEmpty(query.getFirstTag())) {
			insertCatagorys(query.getId(), query.getFirstTag(), query.getSecondTag());
		}
	}
    /**
     * 课程复制
     * @param id
     */
	public void copyCourseByHtml(String id) {
		CourseManagerView query = new CourseManagerView();
		query.setId(id);
		// 课程信息
		CourseManagerView result = commonDao.searchOneData(query, SEARCH_COURSE_BY_ID);
		TagView tag = new TagView();
		tag.setLinkId(id);
		tag.setLinkType(LINK_TYPE_COURSE);
		// 课程标签信息
		List<TagView> tagList = commonDao.searchList(tag, CourseDbConstants.SQL_SEARCH_TAG_BY_COURSE_ID);
		if (tagList != null && tagList.size() > 0) {
			for (int i = 0; i < tagList.size(); i++) {
				if (tagList.get(i).getParentTagId() == null) {
					result.setFirstTag(tagList.get(i).getTagId());
				} else {
					result.setSecondTag(tagList.get(i).getTagId());
				}
			}
		}
		EntityUtil.copyEntity(result, query);
		query.setId(IDGenerator.genUID());
		query.setStatus(COURSE_STATUS_NOT);
		if(result.getShareFlag().equals(COURSE_SHARE_FLAG_ONE)) {
			query.setTitle(query.getTitle() + "_（精品课）");
		}else if(result.getShareFlag().equals(COURSE_SHARE_FLAG_TWO)) {
			query.setTitle(query.getTitle() + "_（课工场）");
		}
		if(result.getLibraryFlag().equals(ALREADY_SCORE_COURSE)) {
			query.setTitle(query.getTitle() + "_（图书馆）");
		}
		//query.setStatus(result.getStatus());
		//query.setShareFlag(COURSE_SHARE_FLAG_TEN);
		query.setStatus(COURSE_STATUS_NOT);
		query.setShareFlag(COURSE_SHARE_FLAG_ELEVEN);
		query.setCourseLocation(result.getCourseLocation());
		//query.setClassFlag(result.getClassFlag());
		query.setClassFlag("0");
		query.setTeacherId(WebContext.getSessionUserId());
		commonDao.insertData(query, COURSE_INSERT);
		List<ChapterView> chapterList = chapterService.getChapterListByCourseId(id);
		//保存原有课程章节
		saveCopyCourseChapter(chapterList,query.getId());
		// 新增分类信息
		/*if (StringUtil.isNotEmpty(query.getFirstTag())) {
			insertCatagorys(query.getId(), query.getFirstTag(), query.getSecondTag());
		}*/
	}
	
	/**
	 * 
	 * 根据linkId查询问卷信息
	 * 
	 * @param id
	 * @return
	 */
	public MeasurementEntity seachMeasureByCourseId(String id) {
		MeasurementEntity entity = new MeasurementEntity();
		entity.setLinkId(id);
		entity.setLinkType(LINK_TYPE_COURSE);
		entity.setMeasureType(MEASUERMENT_TYPE_QUESTIO);
		entity = commonDao.searchOneData(entity, SQL_SEACH_MEASURE_BY_COURSE_ID);
		List<QuestionEntity> measurementResult = (List<QuestionEntity>) search(entity,
				MEASUERMENT_SEARCH_QUESTION_LIST_BY_MEASUREMENT);
		// 检索测试题中的选项
		for (QuestionEntity quest : measurementResult) {
			QuestionOptionEntity optionQuery = new QuestionOptionEntity();
			optionQuery.setQuestionId(quest.getId());
			optionQuery.setSortName("sortOrder");
			optionQuery.setSortOrder("asc");
			List<QuestionOptionEntity> options = (List<QuestionOptionEntity>) search(optionQuery,
					MEASUERMENT_SEARCH_QUESTION_OPTIONS_LIST);
			quest.setOptions(options);
		}
		entity.setQuestionList(measurementResult);
		return entity;

	}

	/**
	 * 添加问卷
	 * 
	 * @param measure
	 */

	public void saveMeasure(MeasurementEntity measure) {
		measure.setId(IDGenerator.genUID());
		measure.setMeasureType(MEASUERMENT_TYPE_QUESTIO);
		measure.setLinkType(LINK_TYPE_COURSE);
		if (isHasMeasure(measure) == 0) {
			commonDao.insertData(measure, MEASUREMENT_INSERT);
			CourseEntity entity = new CourseEntity();
			entity.setFlagQuestionnaire(FLAG);
			entity.setId(measure.getLinkId());
			commonDao.updateData(entity, SQL_UPDATE_FLAG_QUEATION);
			// 保存问题
			if (measure.getQuestionList() != null) {
				insertQuestion(measure.getQuestionList(), measure.getId());
			}
		} else {
			throw new BusinessException("此课程下已经存在问卷，不允许添加！");
		}
	}

	/**
	 * 问卷存在
	 * 
	 * @param measure
	 * @return
	 */
	public int isHasMeasure(MeasurementEntity measure) {
		return commonDao.searchCount(measure, SQL_SEACH_ONE_IS_MEASURE);
	}

	/**
	 * 更新问卷信息
	 * 
	 * @param measure
	 */
	public void updateMeasure(MeasurementEntity measure) {
		MeasurementEntity oldEntity = seachMeasureByCourseId(measure.getLinkId());
		oldEntity.setTitile(measure.getTitile());
		commonDao.updateData(oldEntity, SQL_UPDATE_MEASURE);
		// 保存问题
		if (measure.getQuestionList() != null) {
			updateQuestion(measure.getId(), measure.getQuestionList(), oldEntity.getQuestionList());
		}

	}

	/**
	 * 判断当前登录用户是否为当前章节教师角色
	 * 
	 * @return true:是教师，false:不是教师
	 */
	public boolean isTeacher(String teacherId) {
		UserRoleEntity ur = new UserRoleEntity();
		ur.setUserId(WebContext.getSessionUserId());
		ur.setRoleId(ROLE_TEACHER_ROLE);
		int count = commonDao.searchCount(ur, SQL_IS_TEACHHER);
		//return count > 0 && WebContext.getSessionUserId().equals(teacherId);
		return count > 0;
	}

	/**
	 * 保存应聘讲师提交信息
	 * 
	 * @param
	 * @return
	 */
	public ResultBean saveApplyTeacher(TeacherApplyEntity teacherApplyEntity) {
		ResultBean result = new ResultBean();

		// 判断用户是否登录
		if (WebContext.isGuest()) {
			result.setStatus(false);
			result.setMessages(getMessage(NULL_USER_ID));
			return result;
		}

		teacherApplyEntity.setId(IDGenerator.genUID());
		teacherApplyEntity.setUserId(WebContext.getSessionUserId());
		commonDao.insertData(teacherApplyEntity, INSERT_TEACHER_APPLY);
		String[] tag = StringUtil.split(teacherApplyEntity.getTags(), ",");
		List<String> tagList = new ArrayList<String>();
		for (String str : tag) {
			tagList.add(str);
		}
		for (int i = 0; i < tagList.size(); i++) {
			TagLinkEntity tagLinkEntity = new TagLinkEntity();
			tagLinkEntity.setLinkId(teacherApplyEntity.getId());
			tagLinkEntity.setTagId(tagList.get(i));
			tagLinkEntity.setLinkType("TEACHERAPPLY");
			commonDao.insertData(tagLinkEntity, INSERT_TAG_LINK);
		}
		return result;
	}

	public ResultBean userHomeworkList(HomeworkView homeworkView, Integer pageSize, Integer pageNumber) {
		// 判断用户是否登录
		assertNotGuest();

		// 当前登录用户
		String userId = WebContext.getSessionUserId();
		homeworkView.setAnswerUser(userId);
		homeworkView.setPageNumber(pageNumber);
		homeworkView.setPageSize(pageSize);
		return commonDao.searchList4Page(homeworkView, SEARCH_USER_HOMEWORK);
	}

	public ResultBean getAlluserHomeworkList(HomeworkView homeworkView, Integer pageSize, Integer pageNumber) {
		// 判断用户是否登录
		assertNotGuest();
		// 当前登录用户
		String userId = WebContext.getSessionUserId();
		homeworkView.setAnswerUser(userId);
		homeworkView.setPageNumber(pageNumber);
		homeworkView.setPageSize(pageSize);
		return commonDao.searchList4Page(homeworkView, SEARCH_USER_HOMEWORK_BY_COURSEID);
	}

	public void saveCopyCourseChapter(List<ChapterView> chapterList, String courseId) {
		if(chapterList.size() > 0) {
			for(ChapterView firstView : chapterList) {
				if(firstView.getChapterFlag().toString().equals(CourseStateConstants.BIG_CHAPTER)) {
					// 1级大章节，1
					String id = firstView.getId();
					String firstId = IDGenerator.genUID();
					ChapterEntity firstChapter = new ChapterEntity();
					firstChapter.setCourseId(courseId);
					firstChapter.setId(firstId);
					firstChapter.setChapterOrder(firstView.getChapterOrder() );
					firstChapter.setTitle(firstView.getTitle() == null ? "" : firstView.getTitle());
					firstChapter.setContent(firstView.getContent()  == null ? "" : firstView.getContent());
					firstChapter.setContentApp(firstView.getContentApp()  == null ? "" : firstView.getContentApp());
					firstChapter.setChapterFlag(firstView.getChapterFlag());
					firstChapter.setStatus(firstView.getStatus()  == null ? "" : firstView.getStatus());
					firstChapter.setMeasureType(firstView.getMeasureType()  == null ? "" : firstView.getMeasureType());
					firstChapter.setTeacherId(firstView.getTeacherId()  == null ? "" : firstView.getTeacherId());
					firstChapter.setFlagDiscuss(firstView.getFlagDiscuss()  == null ? "" : firstView.getFlagDiscuss());
					firstChapter.setFlagQa(firstView.getFlagQa()  == null ? "" : firstView.getFlagQa());
					firstChapter.setFlagVote("0");
					//学习人数:默认0
					firstChapter.setNumStudy(BigDecimal.ZERO);
					//浏览人数:默认0
					firstChapter.setNumView(BigDecimal.ZERO);
					//评价人数:默认0
					firstChapter.setNumScoreUser(BigDecimal.ZERO);
					firstChapter.setCreateDate(new Date());
					firstChapter.setCreateUser(WebContext.getSessionUserId());
					commonDao.insertData(firstChapter, INSERT_CHAPTER);
					for(ChapterView secondView : chapterList) {
						if(secondView.getChapterFlag().toString().equals(CourseStateConstants.SMALL_CHAPTER) && secondView.getParentId().equals(id)) {
							// 2级小章节，0
							//String secId = secondView.getId();
							ChapterEntity secondChapter = new ChapterEntity();
							String secondId = IDGenerator.genUID();
							secondChapter.setCourseId(courseId);
							secondChapter.setId(secondId);
							secondChapter.setChapterOrder(secondView.getChapterOrder() );
							secondChapter.setTitle(secondView.getTitle() == null ? "" : secondView.getTitle());
							secondChapter.setContent(secondView.getContent()  == null ? "" : secondView.getContent());
							secondChapter.setContentApp(secondView.getContentApp()  == null ? "" : secondView.getContentApp());
							secondChapter.setChapterFlag(secondView.getChapterFlag());
							secondChapter.setStatus(secondView.getStatus()  == null ? "" : secondView.getStatus());
							secondChapter.setMeasureType(secondView.getMeasureType()  == null ? "" : secondView.getMeasureType());
							secondChapter.setTeacherId(secondView.getTeacherId()  == null ? "" : secondView.getTeacherId());
							secondChapter.setFlagDiscuss(secondView.getFlagDiscuss()  == null ? "" : secondView.getFlagDiscuss());
							secondChapter.setFlagQa(secondView.getFlagQa()  == null ? "" : secondView.getFlagQa());
							secondChapter.setFlagVote("0");
							//学习人数:默认0
							secondChapter.setNumStudy(BigDecimal.ZERO);
							//浏览人数:默认0
							secondChapter.setNumView(BigDecimal.ZERO);
							//评价人数:默认0
							secondChapter.setNumScoreUser(BigDecimal.ZERO);
							secondChapter.setCreateDate(new Date());
							secondChapter.setCreateUser(WebContext.getSessionUserId());
							secondChapter.setParentId(firstId);
							commonDao.insertData(secondChapter, INSERT_CHAPTER);
							for(ChapterView threeView : chapterList) {
								if(threeView.getChapterFlag().toString().equals(CourseStateConstants.THREE_CHAPTER) && threeView.getParentId().equals(secondId)) {
									// 3级子章节，2
									ChapterEntity threeChapter = new ChapterEntity();
									String threeId = IDGenerator.genUID();
									threeChapter.setCourseId(courseId);
									threeChapter.setId(threeId);
									threeChapter.setChapterOrder(threeView.getChapterOrder() );
									threeChapter.setTitle(threeView.getTitle() == null ? "" : threeView.getTitle());
									threeChapter.setContent(threeView.getContent()  == null ? "" : threeView.getContent());
									threeChapter.setContentApp(threeView.getContentApp()  == null ? "" : threeView.getContentApp());
									threeChapter.setChapterFlag(threeView.getChapterFlag());
									threeChapter.setStatus(threeView.getStatus()  == null ? "" : threeView.getStatus());
									threeChapter.setMeasureType(threeView.getMeasureType()  == null ? "" : threeView.getMeasureType());
									threeChapter.setTeacherId(threeView.getTeacherId()  == null ? "" : threeView.getTeacherId());
									threeChapter.setFlagDiscuss(threeView.getFlagDiscuss()  == null ? "" : threeView.getFlagDiscuss());
									threeChapter.setFlagQa(threeView.getFlagQa()  == null ? "" : threeView.getFlagQa());
									threeChapter.setFlagVote("0");
									//学习人数:默认0
									threeChapter.setNumStudy(BigDecimal.ZERO);
									//浏览人数:默认0
									threeChapter.setNumView(BigDecimal.ZERO);
									//评价人数:默认0
									threeChapter.setNumScoreUser(BigDecimal.ZERO);
									threeChapter.setCreateDate(new Date());
									threeChapter.setCreateUser(WebContext.getSessionUserId());
									threeChapter.setParentId(secondId);
									commonDao.insertData(threeChapter, INSERT_CHAPTER);
								}
							}
						}
					}
				}
			}
		}
	}
	
	public void saveFactoryCourseChapter(List<ChapterView> chapterList, String courseId) {
		if(chapterList.size() > 0) {
			for(ChapterView firstView : chapterList) {
				if(firstView.getChapterFlag().toString().equals(CourseStateConstants.BIG_CHAPTER)) {
					// 1级大章节，1
					String id = firstView.getId();
					String firstId = IDGenerator.genUID();
					ChapterEntity firstChapter = new ChapterEntity();
					firstChapter.setCourseId(courseId);
					firstChapter.setId(firstId);
					firstChapter.setChapterOrder(firstView.getChapterOrder() );
					firstChapter.setTitle(firstView.getTitle() == null ? "" : firstView.getTitle());
					firstChapter.setContent(firstView.getContent()  == null ? "" : firstView.getContent());
					firstChapter.setContentApp(firstView.getContentApp()  == null ? "" : firstView.getContentApp());
					firstChapter.setChapterFlag(firstView.getChapterFlag());
					firstChapter.setStatus("1");
					firstChapter.setMeasureType(firstView.getMeasureType()  == null ? "" : firstView.getMeasureType());
					firstChapter.setTeacherId(firstView.getTeacherId()  == null ? "" : firstView.getTeacherId());
					firstChapter.setFlagDiscuss(firstView.getFlagDiscuss()  == null ? "" : firstView.getFlagDiscuss());
					firstChapter.setFlagQa(firstView.getFlagQa()  == null ? "" : firstView.getFlagQa());
					firstChapter.setFlagVote("0");
					//学习人数:默认0
					firstChapter.setNumStudy(BigDecimal.ZERO);
					//浏览人数:默认0
					firstChapter.setNumView(BigDecimal.ZERO);
					//评价人数:默认0
					firstChapter.setNumScoreUser(BigDecimal.ZERO);
					firstChapter.setCreateDate(new Date());
					firstChapter.setCreateUser(WebContext.getSessionUserId());
					commonDao.insertData(firstChapter, INSERT_CHAPTER);
					ChapterActiveEntity firstEntity = new ChapterActiveEntity();
					firstEntity.setChapterId(id);
					List<ChapterActiveEntity> firstActiveList = commonDao.searchList(firstEntity, CHAPTER_ALL_CHAPTER_SELECT);
					if(firstActiveList.size() >0) {
						for(ChapterActiveEntity active : firstActiveList) {
							active.setId(IDGenerator.genUID());
							active.setChapterId(firstId);
							active.setCreateDate(new Date());
							//设置活动创建对象账号
							active.setCreateUser(WebContext.getSessionUserId());
							commonDao.insertData(active,CHAPTER_INSERT);
							}
					}
					for(ChapterView secondView : chapterList) {
						if(secondView.getChapterFlag().toString().equals(CourseStateConstants.SMALL_CHAPTER) && secondView.getParentId().equals(id)) {
							// 2级小章节，0
							//String secId = secondView.getId();
							ChapterEntity secondChapter = new ChapterEntity();
							String secondId = IDGenerator.genUID();
							secondChapter.setCourseId(courseId);
							secondChapter.setId(secondId);
							secondChapter.setChapterOrder(secondView.getChapterOrder() );
							secondChapter.setTitle(secondView.getTitle() == null ? "" : secondView.getTitle());
							secondChapter.setContent(secondView.getContent()  == null ? "" : secondView.getContent());
							secondChapter.setContentApp(secondView.getContentApp()  == null ? "" : secondView.getContentApp());
							secondChapter.setChapterFlag(secondView.getChapterFlag());
							secondChapter.setStatus("1");
							secondChapter.setMeasureType(secondView.getMeasureType()  == null ? "" : secondView.getMeasureType());
							secondChapter.setTeacherId(secondView.getTeacherId()  == null ? "" : secondView.getTeacherId());
							secondChapter.setFlagDiscuss(secondView.getFlagDiscuss()  == null ? "" : secondView.getFlagDiscuss());
							secondChapter.setFlagQa(secondView.getFlagQa()  == null ? "" : secondView.getFlagQa());
							secondChapter.setFlagVote("0");
							//学习人数:默认0
							secondChapter.setNumStudy(BigDecimal.ZERO);
							//浏览人数:默认0
							secondChapter.setNumView(BigDecimal.ZERO);
							//评价人数:默认0
							secondChapter.setNumScoreUser(BigDecimal.ZERO);
							secondChapter.setCreateDate(new Date());
							secondChapter.setCreateUser(WebContext.getSessionUserId());
							secondChapter.setParentId(firstId);
							commonDao.insertData(secondChapter, INSERT_CHAPTER);
							ChapterActiveEntity secondEntity = new ChapterActiveEntity();
							secondEntity.setChapterId(secondView.getId());
							List<ChapterActiveEntity> secondEntityActiveList = commonDao.searchList(secondEntity, CHAPTER_ALL_CHAPTER_SELECT);
							if(secondEntityActiveList.size() >0) {
								for(ChapterActiveEntity active : secondEntityActiveList) {
									active.setId(IDGenerator.genUID());
									active.setChapterId(secondId);
									active.setCreateDate(new Date());
									//设置活动创建对象账号
									active.setCreateUser(WebContext.getSessionUserId());
									commonDao.insertData(active,CHAPTER_INSERT);
									}
							}
							for(ChapterView threeView : chapterList) {
								if(threeView.getChapterFlag().toString().equals(CourseStateConstants.THREE_CHAPTER) && threeView.getParentId().equals(secondId)) {
									// 3级子章节，2
									ChapterEntity threeChapter = new ChapterEntity();
									String threeId = IDGenerator.genUID();
									threeChapter.setCourseId(courseId);
									threeChapter.setId(threeId);
									threeChapter.setChapterOrder(threeView.getChapterOrder() );
									threeChapter.setTitle(threeView.getTitle() == null ? "" : threeView.getTitle());
									threeChapter.setContent(threeView.getContent()  == null ? "" : threeView.getContent());
									threeChapter.setContentApp(threeView.getContentApp()  == null ? "" : threeView.getContentApp());
									threeChapter.setChapterFlag(threeView.getChapterFlag());
									threeChapter.setStatus("1");
									threeChapter.setMeasureType(threeView.getMeasureType()  == null ? "" : threeView.getMeasureType());
									threeChapter.setTeacherId(threeView.getTeacherId()  == null ? "" : threeView.getTeacherId());
									threeChapter.setFlagDiscuss(threeView.getFlagDiscuss()  == null ? "" : threeView.getFlagDiscuss());
									threeChapter.setFlagQa(threeView.getFlagQa()  == null ? "" : threeView.getFlagQa());
									threeChapter.setFlagVote("0");
									//学习人数:默认0
									threeChapter.setNumStudy(BigDecimal.ZERO);
									//浏览人数:默认0
									threeChapter.setNumView(BigDecimal.ZERO);
									//评价人数:默认0
									threeChapter.setNumScoreUser(BigDecimal.ZERO);
									threeChapter.setCreateDate(new Date());
									threeChapter.setCreateUser(WebContext.getSessionUserId());
									threeChapter.setParentId(secondId);
									commonDao.insertData(threeChapter, INSERT_CHAPTER);
									ChapterActiveEntity threeEntity = new ChapterActiveEntity();
									threeEntity.setChapterId(threeView.getId());
									List<ChapterActiveEntity> threeEntityActiveList = commonDao.searchList(threeEntity, CHAPTER_ALL_CHAPTER_SELECT);
									if(threeEntityActiveList.size() >0) {
										for(ChapterActiveEntity active : threeEntityActiveList) {
											active.setId(IDGenerator.genUID());
											active.setChapterId(threeId);
											active.setCreateDate(new Date());
											//设置活动创建对象账号
											active.setCreateUser(WebContext.getSessionUserId());
											commonDao.insertData(active,CHAPTER_INSERT);
											}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	/**
	 * 检索手机端导师个人中心的列表
	 * 
	 * @param pageSize   (每页显示行数)
	 * @param pageNumber (当前页)
	 * @return CourseEntity
	 * @author WUXIAOXIANG
	 */
	public ResultBean teacherCourseAppList(Integer pageSize, Integer pageNumber) {
		ResultBean result = new ResultBean();
		// 判断用户是否登录
		assertNotGuest();

		CourseManagerView course = new CourseManagerView();
		if (pageSize != null) {
			course.setPageSize(pageSize);
		}
		if (pageNumber != null) {
			course.setPageNumber(pageNumber);
		}
		// 导师上传的
		course.setCreateUser(WebContext.getSessionUserId());
		result = searchList4Page(course, SELECT_TEACHER_COURSE_LIST);
		return result;
	}

	/**
	 * 课程总投票数+1
	 * 
	 * @param courseId
	 */
	public void addVoteTotal(String courseId) {
		CourseEntity info = new CourseEntity();
		info.setId(courseId);
		update(info, "Course.addTotalVote");
	}

	/**
	 * 课程好评投票数+1
	 * 
	 * @param courseId
	 */
	public void addVoteGood(String courseId) {
		CourseEntity info = new CourseEntity();
		info.setId(courseId);
		update(info, "Course.addGoodVote");
	}

	/**
	 * 课程普通投票数+1
	 * 
	 * @param courseId
	 */
	public void addVoteNormal(String courseId) {
		CourseEntity info = new CourseEntity();
		info.setId(courseId);
		update(info, "Course.addNormalVote");
	}

	/**
	 * 得到用户测试结果信息
	 * 
	 * @param linkId
	 * @param linkType
	 * @return
	 */
	public ResultBean searchUserMeasurementResult(MeasurementEntity query) {
		UserMeasurementResultEntity userResult = new UserMeasurementResultEntity();
		EntityUtil.copyDBEntity(query, userResult);
		userResult.setSortOrder(query.getSortOrder());
		userResult.setSortName(query.getSortName());
		userResult.setPageNumber(query.getPageNumber());
		userResult.setPageSize(query.getPageSize());
		userResult.setMeasurementId(query.getId());
		ResultBean meaResult = searchUserMeasurementResultByEntiyPage(userResult);
		return meaResult;
	}

	public ResultBean searchUserMeasurementResultByEntiyPage(UserMeasurementResultEntity query) {
		return searchList4Page(query, SQL_MEASUERMENT_USER_SEARCH);
	}

	/**
	 * 课程导入：解压缩
	 * 
	 * @param input 要解压的文件
	 * @return
	 * @throws Exception
	 */
	public ResultBean importCourse(InputStream input) throws Exception {
		assertNotGuest();
		File targetFile = FileUtil.writeToTmpFile(input);
		// 解析Zip
		ZipFile zip = null;
		FileOutputStream thumbOutstream = null;
		try {
			zip = new ZipFile(targetFile);
			Enumeration<ZipEntry> entries = zip.getEntries();
			File excel = null;
			Map<String, File> imgs = new HashMap<String, File>();
			while (entries.hasMoreElements()) {
				ZipEntry zipEntry = entries.nextElement();
				if (zipEntry.isDirectory()) {
					continue;
				}
				String subFileName = zipEntry.getName();
				String subFileExt = StringUtil.getFileExtName(subFileName);
				File file = FileUtil.writeToTmpFile(zip.getInputStream(zipEntry));
				if ("xls".equalsIgnoreCase(subFileExt) || "xlsx".equalsIgnoreCase(subFileExt)) {
					excel = file;
				} else if (ThumbnailUtil.isImage(subFileName)) {
					imgs.put(subFileName, file);
				}
			}
			if (excel != null) {
				return doImportCourse(excel, imgs);
			} else {
				return ResultBean.error("在Zip文件未找到要导入的Excel数据文件");
			}
		} finally {
			ZipFile.closeQuietly(zip);
			IOUtils.closeQuietly(thumbOutstream);
		}
	}

	/**
	 * check课程标签
	 * 
	 * @param tagFirst
	 * @param tagSecond
	 * @return
	 */
	public CourseManagerView checkTag(String tagFirst, String tagSecond) {
		CourseManagerView view = new CourseManagerView();
		TagEntity tagSecondEntity = new TagEntity();
		TagEntity tagFirstEntity = new TagEntity();
		tagSecondEntity.setTagName(tagSecond);
		tagFirstEntity.setTagName(tagFirst);
		tagSecondEntity = commonDao.searchOneData(tagSecondEntity, SQL_SEARCH_TAG_BY_NAME);
		tagFirstEntity = commonDao.searchOneData(tagFirstEntity, SQL_SEARCH_TAG_BY_NAME);
		if (tagSecondEntity != null && StringUtil.isNotEmpty(tagSecondEntity.getParentId()) && tagFirstEntity != null) {
			if (tagSecondEntity.getParentId().equals(tagFirstEntity.getTagId())) {
				view.setFirstTag(tagFirstEntity.getTagId());
				view.setSecondTag(tagSecondEntity.getTagId());
				return view;
			}
			return null;
		} else {
			return null;
		}

	}

	/**
	 * 课程导入文件处理
	 * 
	 * @param excel
	 * @param imgs
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes" })
	private ResultBean doImportCourse(File excel, final Map<String, File> imgs) throws Exception {
		InputStream input = null;
		try {
			input = new FileInputStream(excel);
			final ExcelImportResult<CourseManagerView> courseResult = ExcelImportUtil
					.importExcel(CourseManagerView.class, new BusinessHandler<CourseManagerView>() {
						@Override
						public Boolean businessValidate(RowValidatedResult<CourseManagerView> validatedResult) {
							CourseManagerView view = validatedResult.getData();
							// 课程分类check
							String tagFirst = view.getFirstTag();
							String tagSecond = view.getSecondTag();
							if (StringUtil.isNotEmpty(tagFirst) && StringUtil.isNotEmpty(tagSecond)) {
								CourseManagerView course = checkTag(tagFirst, tagSecond);
								if (course != null) {
									view.setFirstTag(course.getFirstTag());
									view.setSecondTag(course.getSecondTag());
								} else {
									validatedResult.setResult(false).setMessage("课程分类不存在！");
									return false;
								}
							}
							// 封面check
							String picturePathString = view.getPictureId();
							if (!imgs.containsKey(picturePathString)) {
								validatedResult.setResult(false).setMessage("在Zip包中未找到封面:" + picturePathString);
								return false;
							}
							// 导师Id:根据导师名称获取导师IDSQL_SEARCH_MOBILE
							if (StringUtil.isNotEmpty(view.getTeacherId())) {
								UserView user = searchTeacherId(view.getTeacherId());
								if (user == null) {
									validatedResult.setResult(false).setMessage("导师信息不存在！");
									return false;
								} else {
									view.setTeacherId(user.getUserId());
								}
							}
							return true;
						}

						@Override
						public void doBusiness(RowValidatedResult<CourseManagerView> rowValidResult) {
							CourseManagerView view = rowValidResult.getData();
							view.setId(IDGenerator.genUID());
							// 课程状态处理
							if (COURSE_STATUS_CONTINU_CH.equals(view.getCourseStatus())) {
								view.setCourseStatus(COURSE_STATUS_CONTINU);
							} else if (COURSE_STATUS_FINISH_CH.equals(view.getCourseStatus())) {
								view.setCourseStatus(COURSE_STATUS_FINISH);
							} else if (COURSE_STATUS_OVER_CH.equals(view.getCourseStatus())) {
								view.setCourseStatus(COURSE_STATUS_OVER);
							}
							// 课程阶段处理
							if (StringUtil.isNotEmpty(view.getCourseStage())) {
								if (PROMARY_STAGE_CH.equals(view.getCourseStage())) {
									view.setCourseStage(PROMARY_STAGE);
								} else if (MIDDLE_STAGE_CH.equals(view.getCourseStage())) {
									view.setCourseStage(MIDDLE_STAGE);
								} else if (PRACTICE_STAGE_CH.equals(view.getCourseStage())) {
									view.setCourseStage(PRACTICE_STAGE);
								}
							}
							// 是否按步骤
							if (StringUtil.isNotEmpty(view.getFlagOrderStudy())) {
								if (FLAG_ORDER_STUDY_IS_CH.equals(view.getFlagOrderStudy())) {
									view.setFlagOrderStudy(FLAG_ORDER_STUDY_IS);
								} else if (FLAG_ORDER_STUDY_NOT_CH.equals(view.getFlagOrderStudy())) {
									view.setFlagOrderStudy(FLAG_ORDER_STUDY_NOT);
								}
							}
							// 是否必修课程
							if (StringUtil.isNotEmpty(view.getFlagRequired())) {
								if (FLAG_REQUIRED_IS_CH.equals(view.getFlagRequired())) {
									view.setFlagRequired(FLAG_REQUIRED_IS);
								} else if (FLAG_REQUIRED_NOT_CH.equals(view.getFlagRequired())) {
									view.setFlagRequired(FLAG_REQUIRED_NOT);
								}
							}
							// 是否可拖动进度
							if (StringUtil.isNotEmpty(view.getFlagSkipVideo())) {
								if (FLAG_SKIP_VIDEO_IS_CH.equals(view.getFlagSkipVideo())) {
									view.setFlagSkipVideo(FLAG_SKIP_VIDEO_IS);
								} else if (FLAG_SKIP_VIDEO_NOT_CH.equals(view.getFlagSkipVideo())) {
									view.setFlagSkipVideo(FLAG_SKIP_VIDEO_NOT);
								}
							}
							// 课程类别
							if (StringUtil.isNotEmpty(view.getCrowdType())) {
								if (COURSE_CROWD_TRAIN_CH.equals(view.getCrowdType())) {
									view.setCrowdType(COURSE_CROWD_TRAIN);
								} else if (COURSE_CROWD_NOT_TRAIN_CH.equals(view.getCrowdType())) {
									view.setCrowdType(COURSE_CROWD_NOT_TRAIN);
								}
							}
							// 是否免费标识
							if (view.getPrice().compareTo(BigDecimal.ZERO) == 0) {
								view.setFreeFlag(BigDecimal.ONE);
							} else {
								view.setFreeFlag(BigDecimal.ZERO);
							}
							// 默认活动标签开启
							view.setFlagVote(FLAG);// 投票
							view.setFlagNote(FLAG);// 笔记
							view.setFlagQa(FLAG);// 问答
							view.setFlagDiscuss(FLAG);// 评论
							view.setStatus(COURSE_STATUS_NOT);

						}

					}, input, false, "课程分类");
			ExcelImportResult<ChapterEntity> chapterResult = null;
			// 导入章节
			InputStream cinput = null;
			try {
				cinput = new FileInputStream(excel);
				chapterResult = ExcelImportUtil.importExcel(ChapterEntity.class, new BusinessHandler<ChapterEntity>() {
					@Override
					public Boolean businessValidate(RowValidatedResult<ChapterEntity> validatedResult) {
						return true;
					}

					@Override
					public void doBusiness(RowValidatedResult<ChapterEntity> rowValidResult) {
						if (courseResult != null) {
							RowValidatedResult<CourseManagerView> courseRow = courseResult
									.getValidatedRows(rowValidResult.getSheetName(), 0);
							if (courseRow != null) {
								// courseId
								CourseManagerView cmv = courseRow.getData();
								if (cmv != null) {
									// 处理章节
									rowValidResult.getData().setCourseId(cmv.getId());
									rowValidResult.getData().setTeacherId(cmv.getTeacherId());
								}
							}
						}
					}

				}, cinput, true, "课程分类");

			} finally {
				IOUtils.closeQuietly(cinput);
			}
			List<ExcelImportResult> results = new ArrayList<ExcelImportResult>();
			results.add(courseResult);
			results.add(chapterResult);
			if (courseResult.isValid() && chapterResult.isValid()) {
				// 插入操作
				for (RowValidatedResult<CourseManagerView> cr : courseResult.getValidatedRows()) {
					CourseManagerView cmv = cr.getData();
					// 处理文件
					String imgPathString = cmv.getPictureId();
					FileEntity pictureEntity = processFile(imgs.get(imgPathString), FILE_TYPE_IMAGE);
					if (pictureEntity != null) {
						cmv.setPictureId(pictureEntity.getFileId());
					}
					// 插入课程
					commonDao.insertData(cmv, "Course.importCourse");
					// 新增分类信息
					if (StringUtil.isNotEmpty(cmv.getFirstTag())) {
						insertCatagorys(cmv.getId(), cmv.getFirstTag(), cmv.getSecondTag());
					}
				}
				for (RowValidatedResult<ChapterEntity> cpr : chapterResult.getValidatedRows()) {
					ChapterEntity cView = cpr.getData();
					cView.setId(IDGenerator.genUID());
					cView.setChapterFlag(new BigDecimal(BIG_CHAPTER));
					ChapterEntity queryChapter = new ChapterEntity();
					queryChapter.setCourseId(cView.getCourseId());
					Integer maxOrder = commonDao.searchOneData(queryChapter, CHAPTER_SEARCH_MAX_ORDER);
					if (maxOrder == null) {
						maxOrder = 0;
					}
					cView.setFlagDiscuss(FLAG);
					cView.setFlagNote(FLAG);
					cView.setFlagQa(FLAG);
					cView.setFlagVote(FLAG);
					// 学习人数:默认0
					cView.setNumStudy(BigDecimal.ZERO);
					// 浏览人数:默认0
					cView.setNumView(BigDecimal.ZERO);
					// 评价人数:默认0
					cView.setNumScoreUser(BigDecimal.ZERO);
					cView.setChapterOrder(new BigDecimal(maxOrder + 1));
					cView.setStatus(CourseStateConstants.CHAPTER_STATUS_NOT_PUBLIC);
					// CHAPTER_TYPE_VIDEO
					cView.setMeasureType(CHAPTER_TYPE_VIDEO);
					// 插入章节
					commonDao.insertData(cView, SQL_INSERT_CHAPTER);
				}
			}
			return ResultBean.success().setData(results);
		} finally {
			IOUtils.closeQuietly(input);
		}

	}

	/**
	 * 
	 * 根据导师名称获取ID
	 * 
	 * @param userName
	 * @return
	 */
	public UserView searchTeacherId(String userName) {
		UserView user = new UserView();
		user.setUserName(userName);
		return commonDao.searchOneData(user, CourseDbConstants.SQL_SEARCH_MOBILE);
	}

	/**
	 * 根据关键字检索课程列表
	 * 
	 * @param pageSize   每页多少条记录
	 * @param pageNumber 当前页
	 * @param type       检索类型 ，1：课程
	 * @param key
	 * @return
	 */
	public ResultBean searchListByKey(Integer pageSize, Integer pageNumber, String type, String key) {
		assertNotEmpty(key, "关键字为空");
		assertNotEmpty(type, "搜索类型为空");
		if (pageSize == null) {
			pageSize = 20;
		}
		if (pageNumber == null) {
			pageNumber = 1;
		}
		// 判断type
		if (SEARCH_COURSE_BY_KEY.equals(type)) {
			CourseEntity course = new CourseEntity();
			course.setTitle(key);
			course.setPageNumber(pageNumber);
			course.setPageSize(pageSize);
			return commonDao.searchList4Page(course, SQL_SEARCH_LIST_BY_KEY);
		} else {
			ResultBean result = new ResultBean();
			result.setStatus(false);
			result.setMessages("搜索类型未知");
			return result;
		}
	}

	public String getTeamName(String teamId) {
		CourseTeamEntity entity = new CourseTeamEntity();
		entity.setId(teamId);
		CourseTeamEntity team = commonDao.searchOneData(entity, SQL_COURSE_TEAM_SELECT_BY_ID);
		if(team != null) {
			return team.getTeamName();
		}else {
			return null;
		}
	}
	
	public String getUserName(String userId) {
		TpUserEntity user = commonDao.searchOneData(userId, TpDbConstants.USER_SEARCH_NAME_BY_USERID);
		if(user != null) {
			return user.getPersonName();
		}else {
			return null;
		}
	}
	
	public UserView exportUserInfo(String userId) {
		// 课程
		List<UserView> course = commonDao.searchList(userId, SQL_SEARCH_USER_COURSE_EXPORT);
		for (int i = 0; i < course.size(); i++) {
			UserView ccUserView = course.get(i);
			if (COURSE_CROWD_TRAIN.equals(ccUserView.getCrowdType())) {
				ccUserView.setCrowdType(COURSE_CROWD_TRAIN_CH);
			} else if (COURSE_CROWD_NOT_TRAIN.equals(ccUserView.getCrowdType())) {
				ccUserView.setCrowdType(COURSE_CROWD_NOT_TRAIN_CH);
			} else {
				ccUserView.setCrowdType("");
			}
		}
		// 计划
		List<UserView> plan = commonDao.searchList(userId, SQL_SEARCH_USER_PLAN_EXPORT);
		// 考试
		List<UserView> scoreList = commonDao.searchList(userId, SQL_SEARCH_USER_SCORE_EXPORT);
		List<UserView> scores = new ArrayList<UserView>();
		for (int i = 0; i < scoreList.size(); i++) {
			UserView ssUserView = scoreList.get(i);
			if (ssUserView != null && StringUtil.isNotEmpty(ssUserView.getSsCourseTitle())
					&& StringUtil.isNotEmpty(ssUserView.getChapterTitle())) {
				scores.add(ssUserView);
			}
		}
		// 个人积分
		UserPointEntity point = commonDao.searchOneData(userId, PointDbConstants.SQL_USER_POINT_SELECT_BY_KEY);
		TpUserEntity user = commonDao.searchOneData(userId, TpDbConstants.USER_SEARCH_NAME_BY_USERID);
		UserView view = new UserView();
		view.setUserName(user.getUserName());
		view.setPersonName(user.getPersonName());
		if (point != null && point.getPoints() != null) {
			view.setUserPoint(point.getPoints().toString());
		}
		view.setUserCourseList(course);
		view.setUserPlanList(plan);
		view.setUserScoreList(scores);
		return view;
	}

	/**
	 * 判断是否是管理员
	 * 
	 * @return
	 */
	public static Boolean isAdministrator() {
		if (WebContext.hasAccess(COURSE_ADD)) {
			return true;
		}
		return false;
	}
	/**
	 * 日志操作页面方法
	 * @param user
	 * @return
	 */
	public ResultBean search4Page(UserOperEntity user) {
		ResultBean result = new ResultBean();
		//判断操作账号是否为空
		if (StringUtil.isNotEmpty(user.getOperUser())) {
			user.setOperUser(StringUtil.trim(user.getOperUser()));
		}
		//判断操作内容是否为空
		if (StringUtil.isNotEmpty(user.getOperDetail())) {
			user.setOperDetail(StringUtil.trim(user.getOperDetail()));
		}
		String loginUserId = WebContext.getSessionUserId();
		//登录状态下是admin账号的查询
		if(loginUserId.equals("506380c0-62fa-4352-a226-1803a0838759") || loginUserId.equals("admin")) {
			result = commonDao.searchList4Page(user,TpDbConstants.ALL_USER_OPER_SEARCH);
		}else {
			//登录不是admin账号下查询当前登录人信息
			user.setUserId(loginUserId);
			result = commonDao.searchList4Page(user,TpDbConstants.USER_OPER_SEARCH);
		}
		return result;
	}
	
	public ResultBean searchMsg4Page(CourseMessageEntity entity) {
		ResultBean result = new ResultBean();
		entity.setStudentId(WebContext.getSessionUserId());
		//List<CourseMessageEntity> list = commonDao.searchList(entity, COURSE_MESSAGE_SELECT_BY_STUDENT_ID);
		//List<CourseMessageEntity> returnList= new ArrayList<CourseMessageEntity>();
		/*if(list.size() > 0) {
			for(CourseMessageEntity msg : list) {
				TpUserEntity createUser = commonDao.searchOneData(msg.getCreateUser(), TpDbConstants.USER_SEARCH_NAME_BY_USERID);
				msg.setCreateUser(createUser.getPersonName());
				CourseEntity course = commonDao.searchOneData(msg.getCourseId(), COURSE_SELECT_BY_ID);
				msg.setCourseId(course.getTitle());
				returnList.add(msg);
			}
		}*/
		result = commonDao.searchList4Page(entity,COURSE_MESSAGE_SELECT_BY_STUDENT_ID);
		return result;
	}
	
	public ResultBean approveSearch(CourseApproveEntity entity) {
		ResultBean result = new ResultBean();
		result = commonDao.searchList4Page(entity,COURSE_APPROVE_SELECT);
		return result;
	}
	/**
	 * 分页查询作业结果
	 * @return
	 */
	public ResultBean searchUserHomeWork4Page() {
		ResultBean result = new ResultBean();
		List<HomeworkView> workList  = new ArrayList<HomeworkView>();
		HomeworkView home = new HomeworkView();
		//设置查询参数
		home.setAnswerUser(WebContext.getSessionUserId());
		home.setSortName("createDate");
		home.setSortOrder("Desc");
		workList = commonDao.searchList(home, SEARCH_USER_HOME);
		//循环遍历集合
		if(workList.size() > 0) {
			for(HomeworkView view : workList) {
				FileEntity file = getFile(view.getAnswerFileId(), FILE_TYPE_HOMEWORK);
				//获取路径
				view.setAnswerFileName(PropertyUtil.getProperty("res.context.root")+"/"+file.getPath());
			}
		}
		result.setData(workList);
		return result;
	}
	
	public void updateMsg(String id) {
		CourseMessageEntity entity = new CourseMessageEntity();
		entity.setId(id);
		entity.setStuStatus("1");
		commonDao.updateData(entity, COURSE_MESSAGE_UPDATE1);
	}
	
	public void updateMsgForCourse(String id) {
		CourseMessageEntity entity = new CourseMessageEntity();
		entity.setId(id);
		entity.setStuStatus("1");
		entity.setCheckStatus("1");
		commonDao.updateData(entity, COURSE_MESSAGE_UPDATE);
	}
	
	public void updateMsgForCourse1(String id) {
		CourseMessageEntity entity = new CourseMessageEntity();
		entity.setId(id);
		entity.setStuStatus("1");
		entity.setCheckStatus("0");
		commonDao.updateData(entity, COURSE_MESSAGE_UPDATE);
	}
	
	public void saveCheckNo(String id) {
		CourseMessageEntity msg = selectMessageById(id);
		CourseMessageEntity newMsg = new CourseMessageEntity();
		newMsg.setContent("教师已驳回你加入课程的申请！");
		newMsg.setCourseId(msg.getCourseId());
		newMsg.setStudentId(msg.getCreateUser());
		newMsg.setStuStatus("0");//0:未查看;1:已查看
		newMsg.setCreateTime(new Date());
		newMsg.setCreateUser(msg.getCreateUser());
		newMsg.setId(IDGenerator.genUID());
		newMsg.setCheckStatus("1");
		newMsg.setType(COURSE_MESSAGE_TYPE_FOUR);
		commonDao.insertData(newMsg, COURSE_MESSAGE_INSERT);
	}
	public void updateApproveForCourse(CourseApproveEntity entity,String content) {
		/*CourseMessageEntity entity = new CourseMessageEntity();
		entity.setId(id);
		entity.setStuStatus("1");
		entity.setCheckStatus("1");
		commonDao.updateData(entity, COURSE_MESSAGE_UPDATE);*/
		entity.setApproveStatus(COURSE_END_STATUS_YES);
		entity.setApprovedUserId(WebContext.getSessionUserId());
		commonDao.updateData(entity, COURSE_APPROVE_UPDATE);
		CourseMessageEntity msg = new CourseMessageEntity();
		msg.setContent(entity.getContent()+"，该条审批已驳回，驳回理由："+content);
		msg.setCourseId(entity.getCourseId());
		msg.setStudentId(entity.getUserId());
		msg.setStuStatus("0");//0:未查看;1:已查看
		msg.setCreateTime(new Date());
		msg.setCreateUser(WebContext.getSessionUserId());
		msg.setId(IDGenerator.genUID());
		msg.setCheckStatus("-2");
		msg.setType("-1");
		commonDao.insertData(msg, COURSE_MESSAGE_INSERT);
	}
	
	public void updatApproveForCourse(CourseApproveEntity entity) {
		entity.setApproveStatus(COURSE_END_STATUS_YES);
		entity.setApprovedUserId(WebContext.getSessionUserId());
		commonDao.updateData(entity, COURSE_APPROVE_UPDATE);
		CourseMessageEntity msg = new CourseMessageEntity();
		msg.setContent(entity.getContent()+",该条申请已同意。");
		msg.setCourseId(entity.getCourseId());
		msg.setStudentId(entity.getUserId());
		msg.setStuStatus("0");//0:未查看;1:已查看
		msg.setCreateTime(new Date());
		msg.setCreateUser(WebContext.getSessionUserId());
		msg.setId(IDGenerator.genUID());
		msg.setCheckStatus("-2");
		msg.setType("-1");
		commonDao.insertData(msg, COURSE_MESSAGE_INSERT);
	}
	
	public CourseMessageEntity selectMessageById(String id) {
		CourseMessageEntity entity = new CourseMessageEntity();
		entity.setId(id);
		return commonDao.searchOneData(entity, COURSE_MESSAGE_SELECT_BY_PK);
	}
	
	public CourseApproveEntity selectApproveById(String id) {
		CourseApproveEntity entity = new CourseApproveEntity();
		entity.setId(id);
		return commonDao.searchOneData(entity, COURSE_APPROVE_SELECT_BY_PK);
	}
	
	
	public UserInfoEntity getUserInfoById(String id) {
		UserInfoEntity entity = new UserInfoEntity();
		entity.setId(id);
		return commonDao.searchOneData(entity, USER_INFO_SELECT_BY_ID);
	}
	
	public ResultBean saveUserInfoById(UserInfoEntity userInfo) throws Exception {
		ResultBean result = new ResultBean();
		userInfo.setId(IDGenerator.genUID());
		userInfo.setCreateDate(new Date());
		userInfo.setUserId(WebContext.getSessionUserId());
		commonDao.insertData(userInfo, USER_INFO_INSERT);
		result.setStatus(true);
		result.setMessages("记录信息保存成功");
		return result;
	}
	
	public ResultBean updateUserInfoById(UserInfoEntity userInfo) throws Exception {
		ResultBean result = new ResultBean();
		commonDao.updateData(userInfo, USER_INFO_UPDATE);
		result.setStatus(true);
		result.setMessages("记录信息保存成功");
		return result;
	}
	
	public ResultBean deleteUserInfoById(String id) throws Exception {
		ResultBean result = new ResultBean();
		commonDao.deleteData(id, USER_INFO_DELETE);
		result.setStatus(true);
		result.setMessages("记录信息保存成功");
		return result;
	}
	
	public List<UserInfoEntity> getUserInfoList(String title,String year) {
		UserInfoEntity entity = new UserInfoEntity();
		entity.setUserId(WebContext.getSessionUserId());
		entity.setTitle(title);
		if(year != null) {
			entity.setCreateStartDate(year+"-01-01 00:00:00");
			entity.setCreateEndDate(year+"-12-31 23:59:59");
		}
		return commonDao.searchList(entity, USER_INFO_SELECT);
	}
	
	/**
	 * 获取状态
	 * @return
	 */
	public int getStuStatus() {
		//审批对象
		CourseMessageEntity entity = new CourseMessageEntity();
		//教师id
		entity.setStudentId(WebContext.getSessionUserId());
		//状态
		entity.setStuStatus("0");
		//查询tb_course_message集合
		List<CourseMessageEntity> list = commonDao.searchList(entity, COURSE_MESSAGE_SELECT_STUDENT_STATUS);
		//判断结果返回尺寸大小
		if(list.size() > 0) {
			return list.size();
		}else {
			return 0;
		}
	}
    /**
     * 作业分享
     * @param id 作业主键
     */
	public void shareHomeWorkById(String id) {
		//实例化作业答案实体类
		HomeworkAnswerEntity homeworkView = new HomeworkAnswerEntity();
		//作业主键
		homeworkView.setId(id);
		//分享标识
		homeworkView.setShare("1");
		//更新数据库表
		commonDao.updateData(homeworkView, SHARE_COURSE_SELECT_USERSTEPSCORE);
	}
    /**
     * 取消作业分享
     * @param id 作业主键
     */
	public void cancleShareHomeWorkById(String id) {
		HomeworkAnswerEntity homeworkView = new HomeworkAnswerEntity();
		homeworkView.setId(id);
		//取消分享表示
		homeworkView.setShare("0");
		//更新作业答案数据表
		commonDao.updateData(homeworkView, SHARE_CANCLE_SELECT_USERSTEPSCORE);
		
	}
    /**
     * 作业指定功能
     * @param id
     */
	public void topHomeWorkById(String id) {
		//实例化作业答案结果
		HomeworkAnswerEntity entity = new HomeworkAnswerEntity();
		//主键
		entity.setId(id);
		//查询数据最大排序数
		Integer maxTopNum = commonDao.searchOneData(entity, SHARE_MAX_SELECT_USERSTEPSCORE);
		HomeworkAnswerEntity homeworkView = new HomeworkAnswerEntity();
		homeworkView.setId(id);
		if(null==maxTopNum) {
			maxTopNum = 0;
		}
		//设置置顶标识
		homeworkView.setTop("1");
		//排序数设置
		homeworkView.setTopNum(maxTopNum+1);
		//更新作业答案结果
		commonDao.updateData(homeworkView, TOP_SELECT_USERSTEPSCORE);
		
	}
    /**
     * 取消作业置顶
     * @param id 作业主键
     */
	public void cancleTopHomeWorkById(String id) {
		HomeworkAnswerEntity homeworkView = new HomeworkAnswerEntity();
		homeworkView.setId(id);
		//取消置顶
		homeworkView.setTop("0");
		commonDao.updateData(homeworkView, TOP_CANCLE_SELECT_USERSTEPSCORE);
		
	}
    /**
     * 删除作业
     * @param id
     */
	public void deleteWork(String id) {
		HomeworkAnswerEntity homeworkView = new HomeworkAnswerEntity();
		//作业主键
		homeworkView.setId(id);
		//删除作业
		commonDao.deleteData(homeworkView, DELETE_SELECT_USERSTEPSCORE);
		
	}
    /**
     * 查询问题选项
     * @param entity
     * @return
     */
	public QuestionOptionEntity selectOne(QuestionOptionEntity entity) {
		return commonDao.searchOneData(entity, QUESTION_OPTION_SELECT);
	}
    /**
     * 查询问题
     * @param questionEntity
     * @return
     */
	public QuestionEntity setQuestionOneData(QuestionEntity questionEntity) {
		return commonDao.searchOneData(questionEntity, QUESTION_SELECT);
	}
    /**
     * 查询学校列表
     * @param course 课程对象
     * @param directionTagId 一级分类
     * @param categoryTagId 二级分类
     * @param courseStage 课程阶段
     * @param sortType 排序
     * @param pageSize 页面展示数据
     * @param pageNumber 页码
     * @return
     */
	public ResultBean schoolList(InstitutionEntity course, String directionTagId, String categoryTagId,
			String courseStage, String sortType, Integer pageSize, Integer pageNumber) {
		//设置返回结果
		ResultBean result = new ResultBean();
		//页面展示数据判断
		if (pageSize != null) {
			course.setPageSize(pageSize);
		}
		//页码判断
		if (pageNumber != null) {
			course.setPageNumber(pageNumber);
		}
		//查询tb_institution数据表
		result = searchList4Page(course, SELECT_SCHOOL);
		//返回结果
		return result;
	}
    /**
     * 查询前台课程审批信息
     * @return
     */
	public ResultBean searchMsgFrontPage() {
		ResultBean result = new ResultBean();
		//tb_course_message数据表对应的实体类
		CourseMessageEntity entity = new CourseMessageEntity();
		//暂时去掉权限
		//entity.setStudentId(WebContext.getSessionUserId());
		List<CourseMessageEntity> list = commonDao.searchList(entity, MESSAGE_SELECT_BY_STUDENT_ID);
		//声明审批集合对象
		List<CourseMessageEntity> returnList= new ArrayList<CourseMessageEntity>();
		if(list.size() > 0) {
			//循环遍历对象
			for(CourseMessageEntity msg : list) {
				//查询用户信息
				TpUserEntity createUser = commonDao.searchOneData(msg.getCreateUser(), TpDbConstants.USER_SEARCH_NAME_BY_USERID);
				//设置用户名
				msg.setCreateUser(createUser.getPersonName());
				//课程id
				msg.setCourseUsId(msg.getCourseId());
				//查询课程信息
				CourseEntity course = commonDao.searchOneData(msg.getCourseId(), COURSE_SELECT_BY_ID);
				//课程名称
				msg.setCourseId(course.getTitle());
				//添加到集合对象
				returnList.add(msg);
			}
		}
		//返回结果
		result.setData(returnList);
		return result;
	}
	
	/**
	 * 根据ID获取CourseEntity
	 * @param courseId
	 * @return
	 */
	public CourseEntity getCourseById(String courseId){
		CourseEntity entity = new CourseEntity();
		entity.setId(courseId);
		 return commonDao.searchOneData(entity, COURSE_SELECT_BY_ID);
		}
    /**
     * 查询前台全部课程审批信息
     * @return
     */
	public ResultBean searchMsgFrontAllPage() {
		ResultBean result = new ResultBean();
		//tb_course_message数据表对应的实体类
		CourseMessageEntity entity = new CourseMessageEntity();
		//暂时去掉权限
		//entity.setStudentId(WebContext.getSessionUserId());
		List<CourseMessageEntity> list = commonDao.searchList(entity, MESSAGE_ALL_SELECT_BY_STUDENT_ID);
		//声明审批集合对象
		List<CourseMessageEntity> returnList= new ArrayList<CourseMessageEntity>();
		if(list.size() > 0) {
			//循环遍历对象
			for(CourseMessageEntity msg : list) {
				//查询用户信息
				TpUserEntity createUser = commonDao.searchOneData(msg.getCreateUser(), TpDbConstants.USER_SEARCH_NAME_BY_USERID);
				//设置用户名
				msg.setCreateUser(createUser.getPersonName());
				//课程id
				msg.setCourseUsId(msg.getCourseId());
				//查询课程信息
				CourseEntity course = commonDao.searchOneData(msg.getCourseId(), COURSE_SELECT_BY_ID);
				//课程名称
				msg.setCourseId(course.getTitle());
				//添加到集合对象
				returnList.add(msg);
			}
		}
		//返回结果
		result.setData(returnList);
		return result;
	}
    /**
     * 检索我的课程
     * @param finishType
     * @param pageSize
     * @param pageNumber
     * @return
     */
	public ResultBean userCourseUnFinishList(String finishType, Integer pageSize, Integer pageNumber) {
		ResultBean result = new ResultBean();

		// 判断用户是否登录
		assertNotGuest();
		// 我的完成类型为空
//		assertNotEmpty(finishType, MSG_E_COURSE_FINISH_TYPE_NULL);

		// 当前登录用户
		CourseView courseView = new CourseView();
		courseView.setUserId(WebContext.getSessionUserId());//当前登录信息
		courseView.setFinishType(finishType);//完成类型
		courseView.setPageNumber(pageNumber);//页码
		courseView.setPageSize(pageSize);//每页显示多少数据
		// 课程list
		result = commonDao.searchList4Page(courseView, SELECT_USER_COURSE_ALL_UNFINISH_LIST);
		//返回结果
		return result;
	}
    /**
     * 查询作业列表
     * @return
     */
	public ResultBean userZyList() {
		ResultBean result = new ResultBean();
		// 判断用户是否登录
		assertNotGuest();
		// 我的完成类型为空
//		assertNotEmpty(finishType, MSG_E_COURSE_FINISH_TYPE_NULL);
		// 当前登录用户
		HomeworkAnswerEntity courseView = new HomeworkAnswerEntity();
		courseView.setAnswerUser(WebContext.getSessionUserId());
		//设置分页参数
		courseView.setPageNumber(1);
		courseView.setPageSize(5);//每页显示多少数据
		// 作业list
		result = commonDao.searchList4Page(courseView, ZUOYE_COURSE_SELECT_USERSTEPSCORE);
		//返回结果
		return result;
	}
    /**
     * 根据用户id查询用户信息
     * @param uid
     * @return
     */
	public TpUserEntity searchInfoMessage(String uid) {
		return commonDao.searchOneData(uid, USER_SEARCH_NAME_BY_USERID);
	}
    /**
     * 根据课程id查询章节信息
     * @param entity
     * @return
     */
	public List<ChapterEntity> selectByCourseId(ChapterEntity entity) {
		// TODO Auto-generated method stub
		//查询并获取集合
		return commonDao.searchList(entity, CHAPTER_ALL_SELECT);
	}
	/**
	 * 查询课程审核信息
	 * @param courseId
	 * @return
	 */
	public List<CourseMessageEntity> getUserMsgList(String courseId){
		//实例化审核信息表
		CourseMessageEntity msg = new CourseMessageEntity();
		//课程主键
		msg.setCourseId(courseId);
		//msg.setStudentId(WebContext.getSessionUserId());
		//创建者工号
		msg.setCreateUser(WebContext.getSessionUserId());
		//课程类型
		msg.setType(COURSE_MESSAGE_TYPE_FOUR);
		//查询并返回结果
		return commonDao.searchList(msg, MSG_USER_COURSE_BY_ID);
	}
    /**
     * 查询学校二级分类
     * @param id
     * @return
     */
	public List<TagInstitutionView> searchInstitutionSecondTagList2(String id) {
		//实例化分类对象
		TagInstitutionView tagQuery = new TagInstitutionView();
		//id为父级id
		tagQuery.setParentId(id);
		List<TagInstitutionView> list = new ArrayList<TagInstitutionView>();
		// tagQuery.setTagTypeName("分类");
		//判断id是否为空
		if (id == null) {
			//返回空集合
			return list;
		} else {
			//设置分类字段
			tagQuery.setTagTypeId("4");
			tagQuery.setBusinessType("2");
			//查询分类集合
			return commonDao.searchList(tagQuery, SQL_SEARCH_SECOND_TAG_TYPE_INSTITUTION);
		}
	}
	/**
	 * 根据登陆者信息查询角色信息
	 * @return
	 */
	@SuppressWarnings("unused")
	private String geRoleByLoginUser() {
		String result = "";
		//获取当前登录对象
		UserEntity user = WebContext.getSessionUser();
		//判断登录的用户账号是否是游离状态
    	if(!user.getUserName().equals("guest")) {
    		//获取角色集合
    		List<UserRoleEntity> roleList = getRoleByUserId(user.getUserId());
    		if(roleList.size() > 0) {
    			//循环角色集合
    			for(UserRoleEntity role : roleList) {
    				//判断角色是否是01.01代表老师
    				if(role.getRoleId().equals("01")) {
    					result = "m";
    					//04代表学生
    				}else if(role.getRoleId().equals("04")) {
    					result = "t";
    				}
    			}
    			//如果集合为空，将角色设置成学生
    		}else {
    			result = "s";
    		}
    	}else {
    		//未登录，退出
    		result = "logout";
    	}
    	//返回结果
		return result;
	}
    /**
     * 查询学校下的全部教师
     * @param course 课程对象
     * @param directionTagId 一级标签
     * @param categoryTagId 二级标签
     * @param courseStage 状态
     * @param sortType
     * @param pageSize 页面显示数据
     * @param pageNumber 页码
     * @return
     */
	public ResultBean schooTeacherlList(InstitutionEntity course, String directionTagId, String categoryTagId,
			String courseStage, String sortType, Integer pageSize, Integer pageNumber) {
		ResultBean result = new ResultBean();
		//判断页面数据是否为空
		if (pageSize != null) {
			course.setPageSize(pageSize);
		}
		//判断页码是否为空
		if (pageNumber != null) {
			course.setPageNumber(pageNumber);
		}
		//查询学生信息
		result = searchList4Page(course, SELECT_TEACHER_SCHOOL);
		return result;
	}
    /**
     * 查询学校下的全部学生 
     * @param course 课程对象
     * @param directionTagId 一级标签
     * @param categoryTagId 二级标签
     * @param courseStage 状态
     * @param sortType
     * @param pageSize 页面显示数据
     * @param pageNumber 页码
     * @return
     */
	public ResultBean schooStudentlList(InstitutionEntity course, String directionTagId, String categoryTagId,
			String courseStage, String sortType, Integer pageSize, Integer pageNumber) {
		ResultBean result = new ResultBean();
		//判断页面数据是否为空
		if (pageSize != null) {
			course.setPageSize(pageSize);
		}
		//判断页码是否为空
		if (pageNumber != null) {
			course.setPageNumber(pageNumber);
		}
		//查询学生信息
		result = searchList4Page(course, SELECT_STUDENT_SCHOOL);
		return result;
	}
	
	public InstitutionEntity mobileSchooStudentlList(InstitutionEntity course, String directionTagId, String categoryTagId,
		String courseStage, String sortType) {
		//查询学生对应学校信息
		return commonDao.searchOneData(course, SELECT_STUDENT_SCHOOL);
	}
	
	public InstitutionEntity mobileSchooTeacherlList(InstitutionEntity course, String directionTagId, String categoryTagId,
			String courseStage, String sortType) {
			//查询学生对应学校信息
			return commonDao.searchOneData(course, SELECT_TEACHER_SCHOOL);
		}
	
    /**
     * 查询学校下的课程
     * @param entity
     * @return
     */
	public CourseEntity searchCourse(CourseEntity entity) {
		//返回结果
		return commonDao.searchOneData(entity, COURSE_SELECT_BY_ID_SCHOOL);
	}
    /**
     * 查询全部学生
     * @param query
     * @return
     */
	public ResultBean searchStudentUserAll(UserView query) {
		//返回结果
		return commonDao.searchList4Page(query, SQL_SCHOOL_USER_ALL);
	}
    /**
     * 查询章节答案
     * @param chapterId
     * @return
     */
	public HomeworkView searchOneAnswer(String chapterId) {
		//实例化作业对象
		HomeworkView view = new HomeworkView();
		//查询章节id
		view.setLinkId(chapterId);
		//创建人
		view.setAnswerUser(WebContext.getSessionUserId());
		//返回查询结果
		return commonDao.searchOneData(view, SEARCH_HOME_WORK_ONE);
	}
    /**
     * 批量点评实现方法
     * @param homework 入参对象
     * @return
     */
	public ResultBean saveAllHomeWorkReviewById(HomeworkView homework) {
		ResultBean result = new ResultBean();
		//收集前台传来的作业主键
		String ids[] = homework.getHomeworkIds().split(",");
		if(null!=ids) {
			for(int i=0;i<ids.length;i++) {
				//根据作业id查询作业对象内容
				homework.setId(ids[i]);
				HomeworkAnswerEntity entity = commonDao.searchOneData(homework, SELECT_HOME_WORK);
				//作业不为空的更新点评内容到老师评价内容字段
				if(entity != null) {
					//赋值点评内容
					entity.setTeacherEvalution(homework.getContent());
					//设置点评状态
					//作业状态，1待查看；2：已查看未评价；3：已评价
					entity.setStatus(HOMEWORK_ANSWER_STATUS_THREE);
					//更新点评
					commonDao.updateData(entity, UPDATE_TEACHER_REVIEW_HOME_WORK);
				}
			}
		}
		//设置点评状态
		result.setStatus(true);
		//设置点评信息
		result.setMessages("作业点评成功");
		//返回结果
		return result;
	}
    /**
                    * 一键催交保存方法
     * @param userIds  学生id集合字符串
     * @param courseId  课程id
     * @param homeworkId  作业id
     * @return
     */
	public ResultBean saveACjById(String userIds,String courseId,String homeworkId) {
		ResultBean result = new ResultBean();
		//根据作业id获取作业相关内容
		HomeworkEntity view = new HomeworkEntity();
		//作业id
		view.setId(homeworkId);
		view = commonDao.searchOneData(view, COURSE_MESSAGE_SELECT);
		//将学生id拆分遍历
		String userids[] =userIds.split(",");
		for(int i=0;i<userids.length;i++) {
			String userid = userids[i];
			/**
			 * 根据学生id和课程id判断学生是否收到来自代办的信息
			 * 设置学生主键
			 * 课程主键
			 * 查询课程信息并返回
			 */
			CourseMessageEntity message = new CourseMessageEntity();
			message.setStudentId(userid);
			message.setCourseId(courseId);
			message = commonDao.searchOneData(message, MSG_USER_COURSE_BY_ID);
			//如果之前有代办信息删除，目的只保留来自课程信息的一跳代办信息
			if(null!=message) {
				//删除课程信息
				commonDao.deleteData(message, COURSE__MESSAGE_DELETE);
			}
			//课程信息对象
			CourseMessageEntity entity = new CourseMessageEntity();
			//课程主键
			entity.setCourseId(courseId);
			//设置代办提醒信息
			entity.setContent("您有一条关于："+view.getContent()+"的作业,请前往课程章节下提交作业");
			//学生账号
			entity.setStudentId(userids[i]);
			//0:未查看;1:已查看
			entity.setStuStatus("1");
			entity.setCreateTime(new Date());
			//人员账号
			entity.setCreateUser(WebContext.getSessionUserId());
			//随机主键
			entity.setId(IDGenerator.genUID());
			//设置审核状态默认审核通过
			////0:同意;1:驳回,-2:初始状态
			entity.setCheckStatus("0");
			//将状态设置初始状态
			//type,1发布，2分享，3分享至精品课,4申请加入课程
			entity.setType("-1");
			//重新插入代办信息
			commonDao.insertData(entity, COURSE_MESSAGE_INSERT);
		}
		//返回结果
		return result;
		
	}
    /**
                   * 判断用户是否加入课程方法
     * @param UserCourseEntity uc传入参数
     * @return
     */
	public UserCourseEntity searchOneCourseIdData(UserCourseEntity uc) {
		// 检索用户是否存在
		UserCourseEntity uu = commonDao.searchOneData(uc, SEARCH_USER_COURSE);
		//返回加入课程的实例对象
		return uu;
	}
	/**
	 * 根据课程id查询信息
	 * @param courseId 课程主键
	 * @return
	 */
	public CourseEntity getCourseId(String courseId) {
		/*
		 * 实例化课程对象
		 * 根据课程id查询课程信息
		 * 并返回课程结果
		 */
		CourseEntity entity = new CourseEntity();
		entity.setId(courseId);
		return commonDao.searchOneData(entity, SEARCH_COURSE);
	}
	
	public List<CourseAssistTeacherEntity> getCourseAssistTeacherList(String courseId){
		CourseAssistTeacherEntity entity = new CourseAssistTeacherEntity();
		entity.setCourseId(courseId);
		return commonDao.searchList(entity, COURSE_ASSIST_SELECT_BY_COURSE_ID);
	}
	public Integer getUserCourseCount() {
		UserCourseEntity entity = new UserCourseEntity();
		entity.setUserId(WebContext.getSessionUserId());
		return commonDao.searchCount(entity, SQL_USER_COURSE_COUNT);
	}
	
	
	public Integer getSchoolCourseCount() {
		TpUserEntity entity = new TpUserEntity();
		entity.setUserId(WebContext.getSessionUserId());
		TpUserEntity user = commonDao.searchOneData(entity, TpDbConstants.USER_SEARCH_CONDITION);
		CourseEntity course = new CourseEntity();
		course.setSchoolNatrul(user.getSchoolName());
		return commonDao.searchCount(course, SQL_SCHOOL_COURSE_COUNT);
	}
	
	public Integer getCourseStudiedCount() {
		TpUserEntity entity = new TpUserEntity();
		entity.setUserId(WebContext.getSessionUserId());
		TpUserEntity user = commonDao.searchOneData(entity, TpDbConstants.USER_SEARCH_CONDITION);
		CourseEntity course = new CourseEntity();
		course.setSchoolNatrul(user.getSchoolName());
		return commonDao.searchCount(course, SQL_SCHOOL_STUDIED_COURSE_COUNT);
	}
	
	public Integer getStudyingCourseCount() {
		TpUserEntity entity = new TpUserEntity();
		entity.setUserId(WebContext.getSessionUserId());
		TpUserEntity user = commonDao.searchOneData(entity, TpDbConstants.USER_SEARCH_CONDITION);
		CourseEntity course = new CourseEntity();
		course.setSchoolNatrul(user.getSchoolName());
		return commonDao.searchCount(course, SQL_SCHOOL_STUDYING_COURSE_COUNT);
	}
	
	public Integer getCourseEndCount() {
		return commonDao.searchCount(null, SQL_COURSE_END_COUNT);
	}
	
	public Integer getFinishedHomeWorkCount() {
		HomeworkAnswerEntity entity = new HomeworkAnswerEntity();
		entity.setAnswerUser(WebContext.getSessionUserId());
		return commonDao.searchCount(entity, SQL_FINISHED_HOME_WORK_COUNT);
	}
	
	public Integer getUncommitedHomeWork(){
		HomeworkView homeworkView = new HomeworkView();
		int count = 0;
		homeworkView.setCreateUser(WebContext.getSessionUserId());
		List<HomeworkView> list = commonDao.searchList(homeworkView, STUDENT_HOME_WORK);
		if(list.size() > 0) {
			for(HomeworkView view : list) {
				if(view.getStatus() == null || "".equals(view.getStatus())) {
					count++;
				}
			}
		}
		return count;
	}
	public Integer getUserCreateCourseCount(String userId) {
		CourseEntity course = new CourseEntity();
		course.setTeacherId(userId);
		return commonDao.searchOneData(course, COURSE_SELECT_BY_ID_TEACHER);
	}

	public Integer getCourseCreateEndCount(String userId) {
		CourseEntity course = new CourseEntity();
		course.setTeacherId(userId);
		return commonDao.searchOneData(course, COURSE_SELECT_BY_ID_TEACHER_END);
	}

	public Integer getTeacherFinishedHomeWorkCount(String userId) {
		CourseEntity course = new CourseEntity();
		course.setTeacherId(userId);
		return commonDao.searchCount(course, SQL_TEACHER_FINISHED_HOME_WORK_COUNT);
	}

	public Integer getUnevalutionHomeWork(String userId) {
		int count = 0;
		CourseEntity course = new CourseEntity();
		course.setTeacherId(userId);
		List<HomeworkView> list = commonDao.searchList(course, TEACHER_HOME_WORK);
		if(list.size() > 0) {
			for(HomeworkView view : list) {
				if(view.getStatus() == null || "".equals(view.getStatus())) {
					count++;
				}
			}
		}
		return count;
	}
    /**
     * 查询课程下的学生
     * @param uc
     * @return
     */
	public List<UserCourseEntity> searchCourseUserList(UserCourseEntity uc) {
		List<UserCourseEntity> uu = commonDao.searchList(uc, SEARCH_USER_COURSE_NAME);
		return uu;
	}
    /**
     * 根据课程主键查询维度
     * @param entity
     * @return
     */
	public CourseEntity searchByCourseId(CourseEntity entity) {
		return commonDao.searchOneData(entity, SEARCH_COURSE);
	}
	/**
	 * 
	 * @param course
	 * @param directionTagId
	 * @param categoryTagId
	 * @param courseStage
	 * @param sortType
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 */
	public List<InstitutionEntity> schooMobilelList(InstitutionEntity course, String directionTagId, String categoryTagId,
			String courseStage, String sortType, Integer pageSize, Integer pageNumber) {
		//设置返回结果
		List<InstitutionEntity>  institutionList = new ArrayList<InstitutionEntity>();
		//查询tb_institution数据表
		institutionList = commonDao.searchList(course, SELECT_SCHOOL);
		//返回结果
		return institutionList;
	}
	/**
	 * 
	 * @param course
	 * @param directionTagId
	 * @param categoryTagId
	 * @param courseStage
	 * @param sortType
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 */
	public List<InstitutionEntity> schoolMobileTeacherlList(InstitutionEntity course, String directionTagId, String categoryTagId,
			String courseStage, String sortType, Integer pageSize, Integer pageNumber) {
		List<InstitutionEntity>  institutionList = new ArrayList<InstitutionEntity>();

		//查询学生信息
		institutionList = commonDao.searchList(course, SELECT_TEACHER_SCHOOL);
		return institutionList;
	}
	/**
	 * 
	 * @param course
	 * @param directionTagId
	 * @param categoryTagId
	 * @param courseStage
	 * @param sortType
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 */
	public List<InstitutionEntity> schoolMobileStudentlList(InstitutionEntity course, String directionTagId, String categoryTagId,
			String courseStage, String sortType, Integer pageSize, Integer pageNumber) {
		List<InstitutionEntity>  institutionList = new ArrayList<InstitutionEntity>();
		//查询学生信息
		institutionList = commonDao.searchList(course, SELECT_STUDENT_SCHOOL);
		return institutionList;
	}
    /**
                  * 站内信列表
     * @param entity
     * @return
     */
	public List<CourseMessageEntity> searchMsgList(CourseMessageEntity entity) {
		entity.setStudentId(WebContext.getSessionUserId());
		List<CourseMessageEntity> messageList = new ArrayList<CourseMessageEntity>();
		entity.setSortName("create_time");
		entity.setSortOrder("desc");
		messageList = commonDao.searchList(entity,COURSE_MESSAGE_SELECT_BY_STUDENT_ID);
		if(null!=messageList && messageList.size()>0) {
			 for(int i=0;i<messageList.size();i++) {
				 if(StringUtil.isNotBlank(messageList.get(i).getCheckStatus())){
				        if("-2".equals(messageList.get(i).getCheckStatus())){
				        	messageList.get(i).setStatus("未审批");
				        }else if("0".equals(messageList.get(i).getCheckStatus())){
				        	messageList.get(i).setStatus("已通过");
				        }else if("1".equals(messageList.get(i).getCheckStatus())){
				        	messageList.get(i).setStatus("已驳回");
				        }
				    }
				 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				 messageList.get(i).setDate(sdf.format(messageList.get(i).getCreateTime()));
			 }
		}
		return messageList;
	}

}

