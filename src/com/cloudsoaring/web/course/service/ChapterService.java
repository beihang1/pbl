package com.cloudsoaring.web.course.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.exception.BusinessException;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.utils.ConfigUtil;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.common.utils.CommonUtils;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.constant.CourseStateConstants;
import com.cloudsoaring.web.course.entity.ChapterEntity;
import com.cloudsoaring.web.course.entity.ChapterFileEntity;
import com.cloudsoaring.web.course.entity.CourseEntity;
import com.cloudsoaring.web.course.entity.CourseMessageEntity;
import com.cloudsoaring.web.course.entity.MeasurementEntity;
import com.cloudsoaring.web.course.entity.PlanStudyHistoryEntity;
import com.cloudsoaring.web.course.entity.QuestionEntity;
import com.cloudsoaring.web.course.entity.QuestionOptionEntity;
import com.cloudsoaring.web.course.entity.UserChatperEntity;
import com.cloudsoaring.web.course.view.ChapterView;
import com.cloudsoaring.web.course.view.CourseManagerView;
import com.cloudsoaring.web.course.view.CourseView;
import com.cloudsoaring.web.points.entity.UserPoint;
import com.cloudsoaring.web.points.service.PointService;

/**
 * 章节
 * 
 * @author lijuan
 * @date 2016-03-01
 */
@Service
public class ChapterService extends FileService implements CourseConstants {
	@Autowired
	private CourseService courseService;
	@Autowired
    private PointService pointService;
	private ResultBean setMessages;
	/**
	 * 
	 * 删除章节信息
	 * 
	 * @param cIds
	 */
	public void deleteChapter(String cIds) {
		String[] ids = StringUtil.split(cIds, ",");
		if (ids != null) {
			for (String id : ids) {
				if (StringUtil.isNotEmpty(id)) {
					ChapterView view = new ChapterView();
					view.setId(id);
					if (isHasSmallChapter(id)) {
						throw new BusinessException(getMessage(NOT_ALLOW_DELETE_CHAPTER));
					}
					commonDao.deleteData(view, SQL_CHAPTER_DEL);
					delChapter(id);
				}
			}
		}
	}
	/**
	 * 删除章节模块
	 * @param id 章节ID
	 */
	public void delChapter(String linkId){
	    //删除测试
	    courseService.delMeasure(linkId, LINK_TYPE_CHAPTER, MEASUERMENT_TYPE_MEASUERMENT);
	    //删除作业
	    courseService.delHomeWork(linkId,LINK_TYPE_CHAPTER);
	    //删除问答
	    courseService.delQaQuestion(linkId, LINK_TYPE_CHAPTER);
	    //删除评论
	    courseService.delDiscuss(linkId, LINK_TYPE_CHAPTER);
	    //删除笔记
	    courseService.delNote(linkId, LINK_TYPE_CHAPTER);
	    //删除学员+投票
	    courseService.delUserCourse(linkId);
	}
	/**
	 * 
	 * 判断是否存在子章节
	 * 
	 * @param id
	 * @return
	 */
	public Boolean isHasSmallChapter(String id) {
		ChapterView view = new ChapterView();
		view.setParentId(id);
		int count = commonDao.searchCount(view,SQL_SEARCH_SMALL_CHPATER_BY_PARENTID);
		if (count > 0) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * 章节取消发布/发布
	 * 
	 * @param id
	 * @param status
	 */
	public void publicChapter(String id, String status) {
		ChapterView view = new ChapterView();
		view.setId(id);
		if (status.equals(CHAPTER_STATUS_PUBLIC)) {
			view.setStatus(CHAPTER_STATUS_NOT_PUBLIC);
		} else {
			view.setStatus(CHAPTER_STATUS_PUBLIC);
		}
		commonDao.updateData(view, UPDATE_CHAPTER_STATUS);
	}
	/**
	 * 章节添加
	 * @param chapter 章节对象
	 * @param videoLength  视频长度
	 * @throws Exception
	 */
	public void insertChapterInfoAll(ChapterView chapter,String videoLength) throws Exception {
		// 排序
		ChapterEntity queryChapter = new ChapterEntity();
		queryChapter.setCourseId(chapter.getCourseId());
		//查询章节的最大顺序号
		Integer maxOrder = commonDao.searchOneData(queryChapter,CHAPTER_SEARCH_MAX_ORDER);
		if (maxOrder == null) {
			maxOrder = 0;
		}
		//设置新插入的顺序号
		chapter.setChapterOrder(new BigDecimal(maxOrder + 1));
		//时长转成秒数
		if(StringUtil.isNotEmpty(videoLength)){
		   String length=CommonUtils.formatFromSeconds(videoLength);
	       chapter.setVideoLength(new BigDecimal(length));
		}
		//差分章节活动记录字段
		ChapterView view = splitActivity(chapter.getActivitys());
		chapter.setFlagDiscuss(view.getFlagDiscuss());
		chapter.setFlagNote(view.getFlagNote());
		chapter.setFlagQa(view.getFlagQa());
		chapter.setFlagVote(view.getFlagVote());
		//学习人数:默认0
		chapter.setNumStudy(BigDecimal.ZERO);
		//浏览人数:默认0
		chapter.setNumView(BigDecimal.ZERO);
		//评价人数:默认0
		chapter.setNumScoreUser(BigDecimal.ZERO);
		chapter.setId(IDGenerator.genUID());
		if(StringUtil.isNotBlank(chapter.getStatus()) && "1".equals(chapter.getStatus())) {
			chapter.setStatus("1");
		}else {
			chapter.setStatus(CourseStateConstants.CHAPTER_STATUS_NOT_PUBLIC);
		}
		chapter.setTeacherId(WebContext.getSessionUserId());
		chapter.setFlagDiscuss("1");
		//查询tb_chapter数据表信息
		commonDao.insertData(chapter, INSERT_CHAPTER);
			// 保存问题
			if (chapter.getQuestionList().size()>0  ) {
			      MeasurementEntity measurementEntity = new MeasurementEntity();
		          measurementEntity.setTotalPoints(chapter.getTotalPoints());
		          measurementEntity.setStandardPoints(chapter.getStandardPoints());
		          //生成随机主键
		          measurementEntity.setId(IDGenerator.genUID());
		          measurementEntity.setTitile(chapter.getTitle());
		          measurementEntity.setLinkId(chapter.getId());
		          measurementEntity.setMeasureType(MEASUERMENT_TYPE_MEASUERMENT);
		          measurementEntity.setLinkType(LINK_TYPE_CHAPTER);
		          commonDao.insertData(measurementEntity, MEASUREMENT_INSERT);
			  //新增测试信息
				courseService.insertQuestion(chapter.getQuestionList(),
						measurementEntity.getId());
		}
		//考试章节更新课程MEASURE_TYPE
		CourseManagerView course=new CourseManagerView();
		course.setId(chapter.getCourseId());
		if("2".equals(chapter.getMeasureType())){
		   course.setMeasureType("2");
		}else{
		     course=commonDao.searchOneData(course, SEARCH_COURSE_BY_ID);
		    if(StringUtil.isEmpty(course.getMeasureType()) || !course.getMeasureType().equals("2")){
		       course.setMeasureType("1"); 
		    }
		}
		//更新信息
		 commonDao.updateData(course, SQL_UPDATE_MEASURE_TYPE);       
	}

	/**
	 * 获取活动标识
	 * 
	 * @param activitys
	 *            活动标识值
	 * @return
	 */
	private ChapterView splitActivity(String activitys) {
		ChapterView view = new ChapterView();
		  view.setFlagNote(activitys.indexOf(FLAG_NOTE)>-1?FLAG:FLAG_NOT);
	        view.setFlagDiscuss(activitys.indexOf(FLAG_DISCUSS)>-1?FLAG:FLAG_NOT);
	        view.setFlagQa(activitys.indexOf(FLAG_QA)>-1?FLAG:FLAG_NOT);
	        view.setFlagVote(activitys.indexOf(FLAG_VOTE)>-1?FLAG:FLAG_NOT);
		return view;
	}
	/**
	 * 更新章节信息
	 * 
	 * @param chapter章节对象
	 * @throws Exception
	 */
	public void updateChapterAll(ChapterView chapter,String videoLength) throws Exception {
		ChapterView oldChapter = searchCourse4EditById(chapter.getId(),PAGE_TYPE_EDIT,chapter.getMeasureType());
		if (!WebContext.hasAccess(ALL_LIST)
				&& !oldChapter.getCreateUser().equals(WebContext.getSessionUserId())) {
			throw new BusinessException(getMessage(NOT_ALLOW_CHAPTER));
		}
		ChapterView view = splitActivity(chapter.getActivitys());
		oldChapter.setFlagDiscuss(view.getFlagDiscuss());
		oldChapter.setFlagNote(view.getFlagNote());
		oldChapter.setFlagQa(view.getFlagQa());
		oldChapter.setFlagVote(view.getFlagVote());
		if(StringUtil.isNotEmpty(videoLength)){
		    String length=CommonUtils.formatFromSeconds(videoLength);
		    oldChapter.setVideoLength(new BigDecimal(length));
		}
		oldChapter.setVideoName(chapter.getVideoName());
		oldChapter.setContent(chapter.getContent());
		oldChapter.setTitle(chapter.getTitle());
		oldChapter.setMeasureType(chapter.getMeasureType());
		oldChapter.setEffectiveStartDate(chapter.getEffectiveStartDate());
		oldChapter.setEffectiveEndDate(chapter.getEffectiveEndDate());
		oldChapter.setKeyWord(chapter.getKeyWord());
		oldChapter.setSummary(chapter.getSummary());
		if (oldChapter.getTeacherId() == null) {
		    oldChapter.setTeacherId(WebContext.getSessionUserId());
		}
		oldChapter.setVideoId(chapter.getVideoId());
		oldChapter.setFlagDiscuss("1");
		oldChapter.setIsMakerFlag(chapter.getIsMakerFlag());
		commonDao.updateData(oldChapter, SQL_CHAPTER_UPDATE);
		//保存废弃的视频ID
		if(StringUtil.isNotEmpty(chapter.getOldVid())){
		    ChapterFileEntity file= new ChapterFileEntity();
		    file.setChapterId(chapter.getId());
		    file.setFileId(chapter.getOldVid());
		    insert(file);
		}
		// 保存问题
		if (chapter.getQuestionList().size()>0) {
			String id = "";
			for (QuestionEntity question : oldChapter.getQuestionList()) {
				id = question.getMeasureId();
			}
			if (StringUtil.isEmpty(id)) {
				MeasurementEntity measurement = new MeasurementEntity();
				measurement.setId(IDGenerator.genUID());
				measurement.setTitile(chapter.getTitle());
				measurement.setLinkId(chapter.getId());
				measurement.setTotalPoints(chapter.getTotalPoints());
				measurement.setStandardPoints(chapter.getStandardPoints());
				if(CHAPTER_TYPE_EXAM.equals(chapter.getMeasureType())){
			      measurement.setMeasureType(chapter.getMeasureType());
				}else{
                  measurement.setMeasureType(MEASUERMENT_TYPE_MEASUERMENT);
				}
				measurement.setLinkType(LINK_TYPE_CHAPTER);
				commonDao.insertData(measurement, MEASUREMENT_INSERT);
				courseService.insertQuestion(chapter.getQuestionList(),measurement.getId());
			} else {
			    MeasurementEntity measurement = new MeasurementEntity();
			    measurement.setId(id);
			    measurement.setTotalPoints(chapter.getTotalPoints());
                measurement.setStandardPoints(chapter.getStandardPoints());
			    commonDao.updateData(measurement, SQL_UPDATE_MEASURE_POINT) ;
				courseService.updateQuestion(id, chapter.getQuestionList(),oldChapter.getQuestionList());
			}
		}

	}

	/**
	 * 根据章节主键查询章节信息
	 * @param chapterId
	 * @return
	 */
	public ChapterEntity selectChapteById(String chapterId) {
		ChapterView view = new ChapterView();
		view.setId(chapterId);
		//返回查询结果
		return commonDao.searchOneData(view, CHAPTER_SELECT_BY_ID);
	}
	/**
	 * 
	 * 章节编辑/详情页面信息查询
	 * 
	 * @param id
	 *            章节ID
	 * @param args
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ChapterView searchCourse4EditById(String id, String args,String measureType) {
		ChapterView query = new ChapterView();
		query.setId(id);
		if(StringUtil.isNotEmpty(measureType)){
		    if(measureType.equals(CHAPTER_TYPE_EXAM)){
	            query.setMeasureType(CHAPTER_TYPE_EXAM); 
	        }else{
	            query.setMeasureType(MEASUERMENT_TYPE_MEASUERMENT);
	        }
		}
		
		ChapterView result = commonDao.searchOneData(query,SQL_SEARCH_CHAPTER_BY_ID);
		Date date = new Date();
		if (args.equals(PAGE_TYPE_EDIT)) {
			String activitys="";
            if(FLAG.equals(result.getFlagNote())){
                activitys +=FLAG_NOTE+",";
            }
            if(FLAG.equals(result.getFlagVote())){
                activitys += FLAG_VOTE+",";
            }
            if(FLAG.equals(result.getFlagQa())){
                activitys += FLAG_QA+",";
            }
            if(FLAG.equals(result.getFlagDiscuss())){
                activitys += FLAG_DISCUSS+",";
            }
	        result.setActivitys(activitys);
		}
		// 检索测试题
    		MeasurementEntity measurementQuery = new MeasurementEntity();
    		measurementQuery.setLinkType(LINK_TYPE_CHAPTER);
    		measurementQuery.setLinkId(id);
    		measurementQuery.setMeasureType(MEASUERMENT_TYPE_MEASUERMENT);
    		if(StringUtil.isNotEmpty(query.getMeasureType())){
    			measurementQuery.setMeasureType(CHAPTER_TYPE_EXAM);
    		}
    		
    		List<QuestionEntity> measurementResult = (List<QuestionEntity>) search(
    				measurementQuery,MEASUERMENT_SEARCH_QUESTION_LIST_BY_MEASUREMENT);
    		// 设置测试ID
    		if (measurementResult != null && measurementResult.size() > 0) {
    			result.setMeasurementId(measurementResult.get(0).getMeasureId());
    		}
    		// 检索测试题中的选项
    		for (QuestionEntity quest : measurementResult) {
    			QuestionOptionEntity optionQuery = new QuestionOptionEntity();
    			optionQuery.setQuestionId(quest.getId());
    			List<QuestionOptionEntity> options = (List<QuestionOptionEntity>) search(
    					optionQuery, MEASUERMENT_SEARCH_QUESTION_OPTIONS_LIST);
    			quest.setOptions(options);
    		}
    		result.setQuestionList(measurementResult);
		return result;
	}

	/**
	 * 查询章节信息
	 * 
	 * @param courseId
	 *            课程ID
	 * @return
	 */
	public ChapterView searchView(String courseId) {
		ChapterView view = new ChapterView();
		view.setCourseId(courseId);
		return commonDao.searchOneData(view, SEARCH_CHAPTER_BY_PK);
	}

	/**
	 * 
	 * 查询章节列表
	 * 
	 * @param courseId
	 *            课程ID
	 * @return
	 */
	@SuppressWarnings("unchecked")
    public ResultBean searchList(ChapterEntity entity) {
		entity.setPageSize(100);
		ResultBean result = commonDao.searchList4Page(entity, SEARCH_CHAPTER);
		List<ChapterEntity> chapterList = (List<ChapterEntity>) result.getData();
		if (chapterList == null || chapterList.size() == 0) {
			return result;
		}
		List<ChapterEntity> list = new ArrayList<ChapterEntity>();
		for(ChapterEntity c : chapterList){
		    if(new BigDecimal(BIG_CHAPTER).equals(c.getChapterFlag())){
		        list.add(c);
		        for(ChapterEntity sub : chapterList){
		            //找二级章节
		            if(new BigDecimal(SMALL_CHAPTER).equals(sub.getChapterFlag()) && (sub.getParentId() != null) && (sub.getParentId()).equals(c.getId())){
		                list.add(sub);
		                for(ChapterEntity three : chapterList){
		                    //找三级章节
		                    if(new BigDecimal(THREE_CHAPTER).equals(three.getChapterFlag()) && (three.getParentId() != null) && three.getParentId().equals(sub.getId())){
		                        list.add(three);
		                    }
		                }
		            }
		        }
		    }
		}
		result.setData(list);
		return result;
	}

	/**
	 * 查询章节大纲
	 * 
	 * @param courseId
	 *            课程ID
	 * @return
	 */
	public ResultBean searchChapterOutlineList(String courseId) {
		ResultBean result = new ResultBean();
		// 课程ID不能为空
		if (StringUtil.isEmpty(courseId)) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_NULL_ID));
			return result;
		}
		// 根据课程ID，判断是否有此课程
		CourseView view = new CourseView();
		view.setId(courseId);
		assertExist(view, SQL_IS_COURSE_EXIST, MSG_E_COURSE_NOT_EXIST);
		ChapterEntity entity = new ChapterEntity();
		entity.setCourseId(courseId);
		// 大章节
		entity.setChapterFlag(new BigDecimal(1));
		return commonDao.searchList4Page(entity, SEARCH_CHAPTER_OUT_LINE);
	}


	/***
	 * 根据课程ID检索课程下的章节列表
	 * 
	 * @param courseId
	 *            课程ID
	 * @return 章节集合
	 */
	public List<ChapterView> chapterList(String courseId) {
		List<ChapterView> result = null;
		String userId = "";
		if(!WebContext.isGuest()){
			userId = WebContext.getSessionUserId();
		}
		// 用户未登录
		result = searchChapterList(courseId, userId);
		
		
		//按照顺序学习
		if(!WebContext.isGuest() && result != null && result.size() != 0 
				&&"1".equals(result.get(0).getFlagOrderStudy())){
			//是否已找到
			Boolean isFind  = false;
			//是否需要继续找下一个章节
			Boolean isCanFind = false;
			for(ChapterView one : result){
				if(!isFind && !isCanFind && StringUtil.isEmpty(one.getLastChapterId())){//课程最后学习的一章为空，则找到下一章开始学习
					isFind = true;
					isCanFind = true;
				} else if(isFind && !isCanFind){//已找到，并已经找到下一章，则剩下的所有章节都置为不可学
					one.setFlagStudy("0");
				}else if(isFind && isCanFind){ //已找到最后一章，但还需要找下一章可以学的
					if(!CourseStateConstants.CHAPTER_TYPE_TITLE.equals(one.getMeasureType())) {
						isCanFind = false;
					} 
					one.setFlagStudy("1");
				} else if(!isFind && !isCanFind){ //没有找到最后一章
					if(one.getId().equals(one.getLastChapterId())){
						isFind = true;
						if(one.getStudyProgress() != null && new BigDecimal(100).compareTo(one.getStudyProgress()) == 0) {
							isCanFind = true;
						}
						one.setFlagStudy("1");
					}
				}

			}
		}
		
		return result;
	}
	/***
	 * 根据课程ID检索课程下的章节列表
	 * 
	 * @param courseId
	 *            课程ID
	 * @return 章节集合
	 */
	public List<ChapterView> chapterList1(String courseId,String measureType) {
		List<ChapterView> result = null;
		String userId = "";
		if(!WebContext.isGuest()){
			userId = WebContext.getSessionUserId();
		}
		// 用户未登录
		result = searchChapterList1(courseId, userId,measureType);
		
		
		//按照顺序学习
		if(!WebContext.isGuest() && result != null && result.size() != 0 
				&&"1".equals(result.get(0).getFlagOrderStudy())){
			//是否已找到
			Boolean isFind  = false;
			//是否需要继续找下一个章节
			Boolean isCanFind = false;
			for(ChapterView one : result){
				if(!isFind && !isCanFind && StringUtil.isEmpty(one.getLastChapterId())){//课程最后学习的一章为空，则找到下一章开始学习
					isFind = true;
					isCanFind = true;
				} else if(isFind && !isCanFind){//已找到，并已经找到下一章，则剩下的所有章节都置为不可学
					one.setFlagStudy("0");
				}else if(isFind && isCanFind){ //已找到最后一章，但还需要找下一章可以学的
					if(!CourseStateConstants.CHAPTER_TYPE_TITLE.equals(one.getMeasureType())) {
						isCanFind = false;
					} 
					one.setFlagStudy("1");
				} else if(!isFind && !isCanFind){ //没有找到最后一章
					if(one.getId().equals(one.getLastChapterId())){
						isFind = true;
						if(one.getStudyProgress() != null && new BigDecimal(100).compareTo(one.getStudyProgress()) == 0) {
							isCanFind = true;
						}
						one.setFlagStudy("1");
					}
				}

			}
		}
		
		return result;
	}
	public List<ChapterView> getChapterListByCourseId(String courseId) {
		List<ChapterView> result = null;
		String userId = "";
		// 用户未登录
		if (StringUtil.isEmpty(courseId)) {
			return null;
		}
		// 根据课程ID，判断是否有此课程
		CourseView courseview = new CourseView();
		courseview.setId(courseId);
		ChapterView view = new ChapterView();
		view.setCourseId(courseId);
		result = commonDao.searchList(view, SEARCH_CHAPTER_LIST_BY_COURSEID);
		return result;
	}
	
	public List<ChapterView> getChapterListByCourseIdUse(String courseId) {
		List<ChapterView> result = null;
		String userId = "";
		// 用户未登录
		if (StringUtil.isEmpty(courseId)) {
			return null;
		}
		// 根据课程ID，判断是否有此课程
		CourseView courseview = new CourseView();
		courseview.setId(courseId);
		ChapterView view = new ChapterView();
		view.setCourseId(courseId);
		result = commonDao.searchList(view, SEARCH_CHAPTER_LIST_BY_COURSEID_USE);
		return result;
	}
	
	/***
	 * 根据课程ID检索课程下的章节列表(树形结构)	
	 * @param courseId
	 *            课程ID
	 * @return 章节集合
	 */
	public List<ChapterView> chapterList2Tree(String courseId) {
		List<ChapterView> result = chapterList(courseId);
		
		//树形结构,默认顺序都已排好
		if(result != null) {
			//父低下的列表
			Map<String, List<ChapterView>> parentMap = new HashMap<String, List<ChapterView>>();
			List<ChapterView> root = new ArrayList<ChapterView>();
			for(ChapterView one : result) {
				String parentKey = one.getParentId();
				if(StringUtil.isNotEmpty(parentKey)){
					List<ChapterView> chileList = parentMap.get(parentKey);
					if(chileList == null){
						chileList = new ArrayList<ChapterView>();
						parentMap.put(parentKey, chileList);
					}
					chileList.add(one);
				} else {
					root.add(one);
				}
			}
			
			//设置节点下的子
			for(ChapterView one : result){
				one.setChileChapter(parentMap.get(one.getId()));
			}
			
			//设置返回值
			result = root;
		}
		return result;
	}
	
	

	/**
	 * 章节列表（未登录）
	 * 
	 * @param courseId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<ChapterView> searchChapterList(String courseId,String userId) {
		// 课程ID不能为空
		if (StringUtil.isEmpty(courseId)) {
			return null;
		}
		// 根据课程ID，判断是否有此课程
		CourseView courseview = new CourseView();
		courseview.setId(courseId);
		assertExist(courseview, SQL_IS_COURSE_EXIST, MSG_E_COURSE_NOT_EXIST);
		ChapterView view = new ChapterView();
		view.setCourseId(courseId);
		view.setUserId(userId);
		return commonDao.searchList(view, SEARCH_CHAPTER_LIST_LOGGED);
	}
	/**
	 * 章节列表（未登录）
	 * 
	 * @param courseId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<ChapterView> searchChapterList1(String courseId,String userId,String measureType) {
		// 课程ID不能为空
		if (StringUtil.isEmpty(courseId)) {
			return null;
		}
		// 根据课程ID，判断是否有此课程
		CourseView courseview = new CourseView();
		courseview.setId(courseId);
		assertExist(courseview, SQL_IS_COURSE_EXIST, MSG_E_COURSE_NOT_EXIST);
		ChapterView view = new ChapterView();
		view.setCourseId(courseId);
		view.setUserId(userId);
		view.setMeasureType(measureType);
		return commonDao.searchList(view, COURSE_CHAPTER_LIST_LOGGED);
	}
	/**
	 * 章节详情
	 * 
	 * @param courseId
	 * @param id
	 * @return ResultBean
	 */
	public ResultBean chapterDetail(String courseId, String id) {
		ResultBean result = new ResultBean();
		assertNotGuest();
		assertNotEmpty(courseId, MSG_E_EMPTY_COURSE_NOCHAPTER);
		
		//如果章节ID为空，则返回课程的第一个发布的章节，如果找不到，报错(课程下未发布章节)
		if(StringUtil.isEmpty(id)){
			//将的第一章传给LastChapterId
			id = commonDao.searchOneData(courseId,SQL_SEARCH_COURSE_FIRST_CHAPTER );
		}
		if(StringUtil.isEmpty(id)){			
			throw new BusinessException(getMessage(MSG_E_EMPTY_COURSE_NOCHAPTER));
		}
		
		//assertNotEmpty(id, MSG_E_EMPTY_CHAPTER_ID);
		courseExist(courseId);
		chapterExist(id);
		// 从session中取出登录用户的ID，判断用户是否登录
		String userId = WebContext.getSessionUserId();
		Map<String, String> map = new HashMap<String, String>();
		map.put("userId", userId);
		map.put("id", id);
		int count = commonDao.searchCount(map, SEARCH_USER_CHAPTER_BY_ID);
		if (count == 0) {
			UserChatperEntity user = new UserChatperEntity();
			user.setUserId(userId);
			user.setChapterId(id);
			commonDao.insertData(user, INSERT_USER_CHAPTER);
		}
		ChapterView view = commonDao.searchOneData(map, SEARCH_CHAPTER_BY_ID);		
		//更新用户上一次学习章节
		updateLastChapter(id);
		//判断是否大纲章节，大纲章节不允许进入详情页，是则提示章节不存在
		if(CHAPTER_TYPE_TITLE.equals(view.getMeasureType())){
			throw new BusinessException(getMessage(MSG_E_CHAPTER_NOT_EXIST));
		}
		//判断是否是图文章节，若是则更新用户学习进度100%
		if(CHAPTER_TYPE_NORMAL.equals(view.getMeasureType())){
			//更新用户学习进度100%
			updateChapterStudy(id, "100");
			//更新用户课程学习进度
			Map<String,Object> m = new HashMap<String,Object>();
			m.put("chapterId", id);
			m.put("userId", WebContext.getSessionUserId());
			updateUserCourseStudyProgress(m);
			//此处判断更新用户课程学习进度前检索出的标记是否代表100%，如果代表100%
		}
		result.setStatus(true);
		return result.setData(view);
	}

	/**
	 * 更新用户章节学习进度
	 * 
	 * @param chapterId
	 *            章节ID
	 * @return
	 */
	public ResultBean updateChapterStudy(String chapterId, String progress) {
		assertNotEmpty(chapterId, MSG_E_EMPTY_CHAPTER_ID);
		// 判断用户是否登录
		assertNotGuest();
		// 判断用户章节是否存在
		UserChatperEntity uc = new UserChatperEntity();
		uc.setChapterId(chapterId);
		uc.setUserId(WebContext.getSessionUserId());
		assertExist(uc, SQL_USER_CHAPTER_EXIST, MSG_E_EMPTY_USER_CHAPTER);
		uc = commonDao.searchOneData(uc, SEARCH_USER_CHAPTER);
		if(uc.getStudyProgress()==null||(new BigDecimal(100)).compareTo(uc.getStudyProgress()) == 1){
			// 更新用户学习进度
			uc.setStudyProgress(new BigDecimal(progress));
			commonDao.updateData(uc, UPDATE_USER_CHAPTER_STUDY_PROGRESS);
			//更新用户课程学习进度
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("chapterId", chapterId);
			map.put("userId", WebContext.getSessionUserId());
			commonDao.updateData(map, SQL_UPDATE_USER_COURSE_PROGRESS);
			//此处根据章节ID检索用户课程，并判断课程是否非必修课，并且此时用户课程的学习进度字段为100%，满足以上两个条件则给用户增加学完非必修课赠送的积分
			ChapterView chapter=new ChapterView();
			chapter.setId(chapterId);
			 String pointAdd=ConfigUtil.getProperty("POINT_ADD_COURSE_END");
			 ChapterView view=commonDao.searchOneData(chapter, SQL_SEARCH_COURSE_BY_CHAPTER_ID);
			 if("0".equals(view.getFlagRequired())){
			     //非必修课添加积分奖励
			     UserPoint changePointReuslt = pointService.earnPoint("HS",WebContext.getSessionUserId(), "A1002", new BigDecimal(pointAdd), chapterId,"");
			     if(!changePointReuslt.isStatus()) {
		                throw new BusinessException(changePointReuslt.getErrorCode(),changePointReuslt.getMessages());
		            }
			 }
			//判断当前章节是否在用户加入的计划中
			int count = commonDao.searchCount(map, SQL_SELECT_USER_PLAN_BY_COURSE);
			if(count > 0){
				//计划学习历史表
				PlanStudyHistoryEntity ps = new PlanStudyHistoryEntity();
				ps.setUserId(WebContext.getSessionUserId());
				ps.setChapterId(chapterId);
				ps.setRecordTime(new Date());
				commonDao.insertData(ps, SQL_INSERT_PLAN_CHAPTER_HISTORY);
			}
		}
		ResultBean result = new ResultBean();
		result.setStatus(true);
		return result;
	}
	/**
	 * 更新用户课程学习进度
	 * @param map
	 */
	public void updateUserCourseStudyProgress(Map<String,Object> map){
		int i = commonDao.updateData(map, SQL_UPDATE_USER_COURSE_PROGRESS);
		System.out.println(i);
	}

	/**
	 * 更新用户章节上一次视频时间
	 * 
	 * @param chapterId
	 *            章节ID
	 * @param time
	 *            上一次视频时间
	 * @return
	 */
	public ResultBean updateChapterLastVideoTime(String chapterId, String time) {
		assertNotEmpty(chapterId, MSG_E_EMPTY_CHAPTER_ID);
		// 判断用户是否登录
		assertNotGuest();
		// 判断用户章节是否存在
		UserChatperEntity uc = new UserChatperEntity();
		uc.setChapterId(chapterId);
		uc.setUserId(WebContext.getSessionUserId());
		assertExist(uc, SQL_USER_CHAPTER_EXIST, MSG_E_EMPTY_USER_CHAPTER);
		// 更新用户上一次视频时间
		uc.setLastVideoTime(new BigDecimal(time));
		commonDao.updateData(uc, UPDATE_USER_CHAPTER_LAST_VIDEO_TIME);
		ResultBean result = new ResultBean();
		result.setStatus(true);
		return result;
	}

	/***
	 * 更新用户学习章节耗费时间（秒）
	 * 
	 * @param chapterId
	 *            章节ID
	 * @param time
	 *            耗费时间（秒）
	 * @return
	 */
	public ResultBean updateChapterCostTime(String chapterId, String time) {
		assertNotEmpty(chapterId, MSG_E_EMPTY_CHAPTER_ID);
		// 判断用户是否登录
		assertNotGuest();
		// 判断用户章节是否存在
		UserChatperEntity uc = new UserChatperEntity();
		uc.setChapterId(chapterId);
		uc.setUserId(WebContext.getSessionUserId());
		assertExist(uc, SQL_USER_CHAPTER_EXIST, MSG_E_EMPTY_USER_CHAPTER);
		uc.setTimeCost(new BigDecimal(StringUtil.getOrElse(time)));
		commonDao.updateData(uc, UPDATE_USER_CHAPTER_TIME_COST);
		//更新用户课程学习耗时
		updateUserCourseCostTime(chapterId,time);
		ResultBean result = new ResultBean();
		result.setStatus(true);
		return result;
	}
	
	/***
	 * 更新用户课程学习耗时
	 * @param chapterId 章节ID
	 * @param time 耗时 单位秒
	 */
	private void updateUserCourseCostTime(String chapterId,String time){
		if(StringUtil.isNotEmpty(time)){
			if(time.indexOf(".") != -1){
				time = time.substring(0,time.indexOf("."));
			}
			Map<String,Object> map = new HashMap<String,Object>();
			map.put("chapterId", chapterId);
			map.put("time", Integer.valueOf(time));
			map.put("userId", WebContext.getSessionUserId());
			commonDao.updateData(map, SQL_UPDATE_USER_COURSE_COST_TIME);
		}
		
	}

	/**
	 * 判断用户章节是否存在
	 * 
	 * @param chapterId
	 *            章节ID
	 * @param userId
	 *            用户ID
	 * @return true:存在，false:不存在
	 */
	public boolean isUserChapterExist(String chapterId, String userId) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("userId", userId);
		map.put("id", chapterId);
		int count = commonDao.searchCount(map, IS_EXIST_USER_CHAPTER);
		return count > 0;
	}

	/**
	 * 教师上传视频文件
	 * 
	 * @param inputName
	 *            html file标签name属性值
	 * @param linkId
	 *            链接ID
	 * @param linkType
	 *            链接类型
	 * @return
	 */
	public ResultBean uploadVideo(String inputName, String linkId, String linkType) {
		//TODO
		// 更新章节
		return null;
	}
	
	/**
	 * 保利威视视频上传成功后更新数据库本章节视频信息
	 * @param linkId 链接ID
	 * @param linkType 链接类型
	 * @param vid 保利威视视频VID
	 * @param duration 视频时长
	 * @return
	 */
	public ResultBean updateVideoInfo(String linkId,String linkType,String vid,String duration){
		assertNotGuest();
		assertNotEmpty(linkId, MSG_E_EMPTY_CHAPTER_ID);
		assertNotEmpty(linkType, MSG_E_EMPTY_LINK_TYPE);
		assertNotEmpty(vid, MSG_E_EMPTY_VIDEO_ID);
		assertNotEmpty(duration, MSG_E_EMPTY_VIDEO_DURATION);
		//章节是否存在
		ChapterEntity entity = new ChapterEntity();
		entity.setId(linkId);
		entity.setStatus(CHAPTER_STATUS_PUBLIC);
		assertExist(entity, SQL_IS_CHAPTER_EXIST, MSG_E_EMPTY_CHAPTER);
		entity.setVideoId(vid);
		entity.setVideoLength(new BigDecimal(duration));
		//更新数据
		commonDao.updateData(entity, SQL_UPDATE_CHAPTER_VIDEO_INFO);
		return ResultBean.success();
	}

	/***
	 * 判断课程是否为空
	 * 
	 * @param courseId
	 *            课程ID
	 */
	public void courseExist(String courseId) {
		assertNotEmpty(courseId, MSG_E_EMPTY_COURSE_ID);
		CourseEntity entity = new CourseEntity();
		entity.setId(courseId);
		entity.setStatus(COURSE_STATUS_PUBLIC);
		assertExist(entity, SQL_IS_COURSE_EXIST, MSG_E_EMPTY_COURSE);
	}

	/***
	 * 判断章节是否为空
	 * 
	 * @param chapterId
	 *            课程ID
	 */
	public void chapterExist(String chapterId) {
		assertNotEmpty(chapterId, MSG_E_EMPTY_CHAPTER_ID);
		ChapterEntity entity = new ChapterEntity();
		entity.setId(chapterId);
		entity.setStatus(CHAPTER_STATUS_PUBLIC);
		assertExist(entity, SQL_IS_CHAPTER_EXIST, MSG_E_EMPTY_CHAPTER);
	}
	
	/***
	 * 更新上一次学习的章节
	 * @param chapterId 章节ID
	 */
	public void updateLastChapter(String chapterId){
		UserChatperEntity uc = new UserChatperEntity();
		uc.setUserId(WebContext.getSessionUserId());
		uc.setChapterId(chapterId);
		commonDao.updateData(uc, SQL_UPDATE_USER_CHAPTER_LAST_CHAPTER);
	}
	
    @SuppressWarnings("unchecked")
    public ResultBean preOrderChapter(String id) {
    	ResultBean result = new ResultBean();
        ChapterEntity queryChapter = new ChapterEntity();
        queryChapter.setId(id);
        ChapterEntity chapterInfo = searchOneData(queryChapter);
        ChapterEntity queryList = new ChapterEntity();
        queryList.setCourseId(chapterInfo.getCourseId());
        queryList.setParentId(chapterInfo.getParentId());
        queryList.setChapterFlag(chapterInfo.getChapterFlag());
        queryList.setSortName("CHAPTER_ORDER");
        queryList.setSortOrder("asc");
        List<ChapterEntity> chapterList = search(queryList);
        List<ChapterEntity> list = new ArrayList<ChapterEntity>();
        for(ChapterEntity c : chapterList){
            if(new BigDecimal(BIG_CHAPTER).equals(c.getChapterFlag())){
                list.add(c);
                for(ChapterEntity sub : chapterList){
                    //找二级章节
                    if(new BigDecimal(SMALL_CHAPTER).equals(sub.getChapterFlag()) && (sub.getParentId()).equals(c.getId())){
                        list.add(sub);
                        for(ChapterEntity three : chapterList){
                            //找三级章节
                            if(new BigDecimal(THREE_CHAPTER).equals(three.getChapterFlag()) && three.getParentId().equals(sub.getId())){
                                list.add(three);
                            }
                        }
                    }
                }
            }
        }
       /*List<String> orderList=new ArrayList<String>();
        for(int i=0;i<list.size();i++){
            orderList.add(list.get(i).getChapterOrder().toString());
        }
       Collections.sort(orderList); */
       Boolean flag = false;
       for(int i = 0, size = chapterList.size(); i < size; i++ ) {
    	   if(flag) {
    		   break;
    	   }
    	   ChapterEntity curChapter = chapterList.get(i);
    	   if(size == 1 && new BigDecimal(THREE_CHAPTER).equals(curChapter.getChapterFlag())) {
    		   flag = true;
    		   result.setStatus(false);
               result.setMessages(getMessage("最顶端了,无法上移！"));
               return result;
    	   }
    	   if(curChapter.getId().equals(chapterInfo.getId())) {
    		   if(i == 0 && chapterInfo.getChapterOrder().compareTo(curChapter.getChapterOrder())==0 && 
    				   new BigDecimal(SMALL_CHAPTER).equals(curChapter.getChapterFlag())){
    			   //如果是二级，则上移为父类前一个兄弟节点的最后一个子节点
    			   ChapterEntity queryAll = new ChapterEntity();
    			   queryAll.setCourseId(chapterInfo.getCourseId());
    			   queryAll.setChapterFlag(new BigDecimal(BIG_CHAPTER));
    			   queryAll.setSortName("chapterOrder");
    			   queryAll.setSortOrder("asc");
    			   List<ChapterEntity> allChapterList = commonDao.searchList(queryAll, SQL_SEARCH_CHAPTER_VIEW);
    			   for(int j = 0; j < allChapterList.size(); j++) {
    				   if(flag) {
    		    		   break;
    		    	   }
    				   if(allChapterList.get(j).getId().equals(curChapter.getParentId())) {
    					   if(j == 0) {
    						   flag = true;
    						   result.setStatus(false);
    		    			   result.setMessages(getMessage("最顶端了,无法再上移！"));
    		    			   return result; 
    					   }
    					   ChapterEntity parentPre = allChapterList.get(j -1);
    					   if(!new BigDecimal(BIG_CHAPTER).equals(parentPre.getChapterFlag())) {
    						   j--;
    						   continue;
    					   }else {
    						   parentPre = allChapterList.get(j -1);
    						   ChapterEntity query = new ChapterEntity();
    						   query.setParentId(parentPre.getId());
    						   query.setSortName("chapterOrder");
    						   query.setSortOrder("asc");
    						   List<ChapterEntity> secondList = commonDao.searchList(query, SQL_SEARCH_CHAPTER_VIEW);
    						   curChapter.setParentId(parentPre.getId());
    						   if(secondList.size() == 0) {
    							   curChapter.setChapterOrder(new BigDecimal(1));
    						   }else {
    							   curChapter.setChapterOrder(new BigDecimal(secondList.get(secondList.size()-1).getChapterOrder().intValue()+1));
    						   }
    						   commonDao.updateData(curChapter, SQL_UPDATE_CHAPTER_ORDER_AND_PARENT);
    						   flag = true;
    						   result.setStatus(true);
    						   return result;
    					   }
    				   }else {
    					   continue;
    				   }
    			   }
    		   }
    		   if(i == 0 && new BigDecimal(THREE_CHAPTER).equals(curChapter.getChapterFlag())) {
        		   flag = true;
        		   result.setStatus(false);
                   result.setMessages(getMessage("最顶端了,无法上移！"));
                   return result;
        	   }
    		   if(i == 0 && new BigDecimal(BIG_CHAPTER).equals(curChapter.getChapterFlag())) {
					flag = true;
	        		result.setStatus(false);
	                result.setMessages(getMessage("最顶端了,无法上移！"));
	                return result;
	        	 }
    		   ChapterEntity preChapter = chapterList.get(i -1);
	            /*if(!preChapter.getChapterFlag().equals(chapterInfo.getChapterFlag())){
	            	result.setStatus(false);
	            	result.setMessages(getMessage("最顶端了,无法上移！"));
	            	 return result; 
	                //throw new BusinessException(getMessage("不在同一章节级别下，不允许上移！"));
	            }*/
	            String preOrder = preChapter.getChapterOrder().toString();
	            preChapter.setChapterOrder(chapterInfo.getChapterOrder());
	            chapterInfo.setChapterOrder(new BigDecimal(preOrder));
	            commonDao.updateData(preChapter, SQL_UPDATE_CHAPTER_ORDER);
	            commonDao.updateData(chapterInfo, SQL_UPDATE_CHAPTER_ORDER);
	            flag = true;
	            //update(preChapter);
	            //update(chapterInfo);
	            result.setStatus(true);
	        }
        }
       return result;   
    }
    
    @SuppressWarnings("unchecked")
	public ResultBean nextOrderChapter(String id) {
		ResultBean result = new ResultBean();
		ChapterEntity queryChapter = new ChapterEntity();
		queryChapter.setId(id);
		ChapterEntity chapterInfo = searchOneData(queryChapter);
		ChapterEntity queryList = new ChapterEntity();
		queryList.setCourseId(chapterInfo.getCourseId());
		queryList.setParentId(chapterInfo.getParentId());
		queryList.setChapterFlag(chapterInfo.getChapterFlag());
		queryList.setSortName("CHAPTER_ORDER");
		queryList.setSortOrder("desc");
		List<ChapterEntity> chapterList = search(queryList);
		Boolean flag = false;
		for (int i = 0, size = chapterList.size(); i < size; i++) {
			if (flag) {
				break;
			}
			ChapterEntity curChapter = chapterList.get(i);
			if (size == 1 && new BigDecimal(THREE_CHAPTER).equals(curChapter.getChapterFlag())) {
				flag = true;
				result.setStatus(false);
				result.setMessages(getMessage("最底端了,无法下移！"));
				return result;
			}
			if (curChapter.getId().equals(chapterInfo.getId())) {
				if (i == 0 && chapterInfo.getChapterOrder().compareTo(curChapter.getChapterOrder()) == 0 && 
	    				   new BigDecimal(SMALL_CHAPTER).equals(curChapter.getChapterFlag())) {
					//如果是二级，则下移为父类后一个兄弟节点的第一个子节点
					ChapterEntity queryAll = new ChapterEntity();
    			   queryAll.setCourseId(chapterInfo.getCourseId());
    			   queryAll.setChapterFlag(new BigDecimal(BIG_CHAPTER));
    			   queryAll.setSortName("chapterOrder");
    			   queryAll.setSortOrder("DESC");
    			   List<ChapterEntity> allChapterList = commonDao.searchList(queryAll, SQL_SEARCH_CHAPTER_VIEW);
    			   for(int j = 0; j < allChapterList.size(); j++) {
    				   if(flag) {
    		    		   break;
    		    	   }
    				   if(allChapterList.get(j).getId().equals(curChapter.getParentId())) {
    					   if(j == 0) {
    						   flag = true;
    						   result.setStatus(false);
    		    			   result.setMessages(getMessage("最底端了,无法下移！"));
    		    			   return result; 
    					   }
    					   ChapterEntity parentPre = allChapterList.get(j -1);
    					   if(!new BigDecimal(BIG_CHAPTER).equals(parentPre.getChapterFlag())) {
    						   j--;
    						   continue;
    					   }else {
    						   parentPre = allChapterList.get(j -1);
    						   ChapterEntity query = new ChapterEntity();
    						   query.setParentId(parentPre.getId());
    						   query.setSortName("chapterOrder");
    						   query.setSortOrder("asc");
    						   List<ChapterEntity> secondList = commonDao.searchList(query, SQL_SEARCH_CHAPTER_VIEW);
    						   curChapter.setParentId(parentPre.getId());
    						   if(secondList.size() == 0) {
    							   curChapter.setChapterOrder(new BigDecimal(1));
    						   }else {
    							   curChapter.setChapterOrder(new BigDecimal(secondList.get(0).getChapterOrder().intValue()-1));
    						   }
    						   commonDao.updateData(curChapter, SQL_UPDATE_CHAPTER_ORDER_AND_PARENT);
    						   flag = true;
    						   result.setStatus(true);
    						   return result;
    					   }
    				   }else {
    					   continue;
    				   }
    			   }
				}
				if(i == 0 && new BigDecimal(THREE_CHAPTER).equals(curChapter.getChapterFlag())) {
					flag = true;
        		    result.setStatus(false);
                    result.setMessages(getMessage("最底端了,无法下移！"));
                    return result;
        	    }
				if(i == 0 && new BigDecimal(BIG_CHAPTER).equals(curChapter.getChapterFlag())) {
					flag = true;
	        		result.setStatus(false);
	                result.setMessages(getMessage("最底端了,无法下移！"));
	                return result;
	        	 }
				ChapterEntity preChapter = chapterList.get(i - 1);
				/*if (!preChapter.getChapterFlag().equals(chapterInfo.getChapterFlag())) {
					result.setStatus(false);
					result.setMessages(getMessage("最底端了,无法下移！"));
					return result;
					// throw new BusinessException(getMessage("不在同一章节级别下，不允许上移！"));
				}*/
				String preOrder = preChapter.getChapterOrder().toString();
				preChapter.setChapterOrder(chapterInfo.getChapterOrder());
				chapterInfo.setChapterOrder(new BigDecimal(preOrder));
				commonDao.updateData(preChapter, SQL_UPDATE_CHAPTER_ORDER);
				commonDao.updateData(chapterInfo, SQL_UPDATE_CHAPTER_ORDER);
				flag = true;
				// update(preChapter);
				// update(chapterInfo);
				result.setStatus(true);
			}
		}
		return result;
	}
    
    public String getChapterTitle(String chapterId) {
		 ChapterEntity chapter = new ChapterEntity();
		chapter.setId(chapterId);
		ChapterEntity chapteReturn = commonDao.searchOneData(chapter, SEARCH_CHAPTER_BY_PK);
		return chapteReturn.getTitle();
	}
    
    public String getChapterPar(String chapterId) {
		 ChapterEntity chapter = new ChapterEntity();
		chapter.setId(chapterId);
		ChapterEntity chapteReturn = commonDao.searchOneData(chapter, SEARCH_CHAPTER_BY_PK);
		return chapteReturn.getParentId();
	}
	public ResultBean myxam() {
		ChapterEntity chapter = new ChapterEntity();
		chapter.setCreateUser(WebContext.getSessionUserId());
		chapter.setPageNumber(1);
		chapter.setPageSize(5);
		ResultBean result = new ResultBean();
		result = commonDao.searchList4Page(chapter, EXAM_SEARCH_CHAPTER_BY_PK);
		return result;
	}
	/**
	 * 查询当前登录者的全部考试信息方法
	 * @param chapter  章节对象参数
	 * @param pageSize 分页显示数据
	 * @param pageNumber 分页页码
	 * @return
	 */
	public ResultBean myAllxam(ChapterEntity chapter, Integer pageSize, Integer pageNumber) {
		//设置查询参数
		chapter.setCreateUser(WebContext.getSessionUserId());
		chapter.setPageNumber(pageNumber);
		chapter.setPageSize(pageSize);
		ResultBean result = new ResultBean();
		//分页查询
		result = commonDao.searchList4Page(chapter, EXAM_ALL_SEARCH_CHAPTER_BY_PK);
		return result;
	}
	/**
	 * 根据课程id查询课程考试信息
	 * @param courseId
	 * @return
	 */
	public ChapterView seacrhExamStatus(String courseId) {
		ChapterView view = new ChapterView();
		view.setCourseId(courseId);
		//已加入课程
		view.setIsCourse("1");
		//查询章节信息
		ChapterView chapterView= commonDao.searchOneData(view, EXAM_ALL_SEARCH_BY_PK);
		return chapterView;
	}
	
}
