package com.cloudsoaring.web.course.service;

import java.util.List;

import org.springframework.stereotype.Service;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.SceneInfo;

/**
 * 
 * unity场景Service
 *
 */
@Service
public class SceneInfoService extends FileService implements CourseConstants {

	/**
	 * 保存unity场景信息
	 * @param entity
	 */
	public void insertSceneInfo(SceneInfo entity) {
		commonDao.insertData(entity, INSERT_SCENE);
	}
	
	/**
	 * 删除DB中的unity场景信息
	 */
	public void deleteSceneInfo() {
		commonDao.deleteData(null, DELETE_ALL_SCENE);
	}
	
	/**
	 * 获取场景信息
	 * @return List<SceneInfo>
	 */
	@SuppressWarnings("unchecked")
	public List<SceneInfo> getSceneInfo(SceneInfo entity){
		return commonDao.searchList(null, SELECT_SCENE_BY_ID);
	}
	
	/**
	 * 获取场景信息列表
	 * @return List<SceneInfo>
	 */
	@SuppressWarnings("unchecked")
	public List<SceneInfo> getSceneInfoList(){
		SceneInfo entity = new SceneInfo();
		entity.setCreateUser(WebContext.getSessionUser().getUserId());
		return commonDao.searchList(entity, SELECT_SCENE_BY_USER);
	}
	
}
