package com.cloudsoaring.web.course.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.EvalutionTimeEntity;

/***
 * 模块相关的业务处理Service
 * 
 * @author ljl
 *
 */
@Service
@SuppressWarnings("unchecked")
public class EvalutionTimeService extends FileService implements CourseConstants {
	/**
	 * 批量查询
	 * @param courseUserEvalutionEntity
	 * @return
	 */
	public List<EvalutionTimeEntity> searchInfoList(EvalutionTimeEntity entity) {
		return commonDao.searchList(entity, SEARCH_EVALUTION_TIME_LIST);
	}
	/**
	 * 查询
	 * @param courseUserEvalutionEntity
	 * @return
	 */
	public EvalutionTimeEntity searchOneInfo(EvalutionTimeEntity evalutionEntity) {
		return commonDao.searchOneData(evalutionEntity, SEARCH_EVALUTION_TIME_ONE_LIST);
	}
	 

	/**
	 * 单条插入
	 * @param courseUserEvalutionEntity
	 */
	public void insert(EvalutionTimeEntity evalutionTimeEntity){
		evalutionTimeEntity.setId(IDGenerator.genUID());
		evalutionTimeEntity.setCreateDate(new Date());
		commonDao.insertData(evalutionTimeEntity, SEARCH_EVALUTION_TIME_INSERT);
	}
	/**
	 * 单条更新
	 * @param id
	 */
	public void update(String id){
		EvalutionTimeEntity evalutionTimeEntity = new EvalutionTimeEntity();
		evalutionTimeEntity.setId(id);
		evalutionTimeEntity.setCreateDate(new Date());
		commonDao.updateData(evalutionTimeEntity, SEARCH_EVALUTION_TIME_UPDATE);
	}

	/**
	 * 根据主键删除
	 * @param id
	 */
	public void deleteById(String id){
		EvalutionTimeEntity evalutionTimeEntity = new EvalutionTimeEntity();
		evalutionTimeEntity.setId(id);
		commonDao.deleteData(evalutionTimeEntity, SEARCH_EVALUTION_TIME_DELETE);
	}
	
}

