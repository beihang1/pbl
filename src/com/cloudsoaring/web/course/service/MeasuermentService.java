package com.cloudsoaring.web.course.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.exception.BusinessException;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.common.service.BaseService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.common.view.ResultBean.ErrorCode;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.constant.CourseStateConstants;
import com.cloudsoaring.web.course.entity.ChapterEntity;
import com.cloudsoaring.web.course.entity.CourseEntity;
import com.cloudsoaring.web.course.entity.CourseMessageEntity;
import com.cloudsoaring.web.course.entity.MeasurementEntity;
import com.cloudsoaring.web.course.entity.QuestionEntity;
import com.cloudsoaring.web.course.entity.QuestionOptionEntity;
import com.cloudsoaring.web.course.entity.UserMeasuermentAnswerEntity;
import com.cloudsoaring.web.course.entity.UserMeasurementResultEntity;
import com.cloudsoaring.web.course.view.UserMeasurementResultView;


/**
 * 
 * @author jgj 前台测试信息
 */
@Service
public class MeasuermentService extends BaseService implements CourseConstants {
	@Autowired
	private CourseService courseService;

	@Autowired
	private ChapterService chapterService;

	/**
	 * 检索测试信息（题目及选项列表、正确答案）
	 * 
	 * @param linkId
	 * @param linkType
	 * @return
	 */
	@SuppressWarnings({ "unchecked" })
	public ResultBean searchMeasuerment(String linkId, String linkType) {
		ResultBean result = new ResultBean();
		// 从session中取出登录用户的ID，判断用户是否登录
		String userId = WebContext.getSessionUserId();
		
		assertNotEmpty(linkId, MSG_E_NULL_ID);
		assertNotEmpty(linkType, MSG_E_EMPTY_LINK_TYPE);

		// 链接类型错误：不等于COURSE or CHAPTER
		if (!linkType.equalsIgnoreCase(LINK_TYPE_COURSE)
				&& !linkType.equalsIgnoreCase(LINK_TYPE_CHAPTER)) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_ERROR_LINK_TYPE));
			return result;
		}

		// 章节不存在
		if (linkType.equalsIgnoreCase(LINK_TYPE_CHAPTER)) {
			assertExist(linkId, SQL_IS_CHAPTER_EXIST, MSG_E_CHAPTER_NOT_EXIST);
		}
		MeasurementEntity dataM = new MeasurementEntity();
		dataM.setUserId(userId);
		dataM.setLinkId(linkId);
		dataM.setLinkType(linkType);
		dataM.setMeasureType(MEASUERMENT_TYPE_MEASUERMENT);
		List<QuestionEntity> list = (List<QuestionEntity>) commonDao
				.searchList(dataM,
						MEASUERMENT_SEARCH_QUESTION_LIST_BY_MEASUREMENT);
		if (list != null && list.size() != 0) {
			for (QuestionEntity quest : list) {
				QuestionOptionEntity optionQuery = new QuestionOptionEntity();
				optionQuery.setQuestionId(quest.getId());
				List<QuestionOptionEntity> options = (List<QuestionOptionEntity>) commonDao
						.searchList(optionQuery,
								MEASUERMENT_SEARCH_QUESTION_OPTIONS_LIST);
				quest.setOptions(options);
			}
		}
		return result.setData(list);
	}

	/**
	 * 测试/考试提交
	 * 
	 * @param measureId
	 * @param ids
	 * @param userAnswerIds
	 * @param submitType
	 * @return
	 */
	@SuppressWarnings({ "unused", "unchecked" })
	public ResultBean submitMeasuerment(String linkType, String measureId,
			String ids, String userAnswerIds,
			@RequestParam(required = false) String submitType) {
		String userId = WebContext.getSessionUserId();
		Date submitDate = new Date();
		if (StringUtil.isEmpty(submitType)) {
			submitType = "0";
		}
		if (ids == null || userAnswerIds == null) {
			return ResultBean.error(getMessage("问题列表为空"));
		}
		String[] idArray = ids.split(",", -1);
		String[] userAnswerIdArray = userAnswerIds.split(";", -1);
		if (idArray.length == 0 || userAnswerIdArray.length == 0) {
			return ResultBean.error(getMessage("问题列表为空"));
		}
		if (idArray.length != userAnswerIdArray.length) {
			return ResultBean.error(getMessage("问题数与答案数不一致"));
		}
		if (StringUtil.isEmpty(measureId)) {
			return ResultBean.error(getMessage("测试答题信息为空"));
		}
		List<QuestionEntity> answerQuest = new ArrayList<QuestionEntity>();
		for (int i = 0, size = idArray.length; i < size; i++) {
			QuestionEntity question = new QuestionEntity();
			question.setId(idArray[i]);
			question.setMeasureId(measureId);
			question.setUserAnswerId(userAnswerIdArray[i]);
			answerQuest.add(question);
		}
		if (answerQuest == null || answerQuest.size() == 0) {
			throw new BusinessException("问题列表为空！");
		}

		String measurementId = answerQuest.get(0).getMeasureId();

		// 校验测试 与 类型是否匹配
		MeasurementEntity measurementQuery = new MeasurementEntity();
		measurementQuery.setId(measurementId);
		measurementQuery.setLinkType(linkType);
		List<MeasurementEntity> measurementResult = search(measurementQuery);
		if (measurementResult == null || measurementResult.size() == 0) {
			throw new BusinessException("测试信息不存在！");
		}

		MeasurementEntity measurementInfo = measurementResult.get(0);
		ExamInfo examInfo = getExamInfo(measurementInfo);
		// 如果是考试则进行时间检测
		Long residualTime = 0L;
		if (MEASUERMENT_TYPE_EXAM.equals(examInfo.measurementType)) {
			residualTime = checkExam(submitDate, measurementId, userId,
					examInfo, EXAM_STATUS_END);
		}
		// 如果是系统自动，则不允许有剩余时间大于10秒
		if ((MEASUERMENT_SUBMIT_TYPE_PC_AUTO.equals(submitType) || MEASUERMENT_SUBMIT_TYPE_APP_AUTO
				.equals(submitType)) && residualTime > 10L) {
			throw new BusinessException("答题异常，请重新答题！");
		}
		// 测试题
		QuestionEntity dbQuestionQuery = new QuestionEntity();
		dbQuestionQuery.setMeasureId(measurementId);
		dbQuestionQuery.setSortName("questionOrder");
		dbQuestionQuery.setSortOrder("asc");
		List<QuestionEntity> questionList = (List<QuestionEntity>) search(
				dbQuestionQuery, MEASUERMENT_SEARCH_QUESTION_LIST);
		Map<String, QuestionEntity> mapAnswerQuest = getMapQuestionListById(answerQuest);
		// 插入用户结果选项
		 BigDecimal goal = new BigDecimal(0);
		 BigDecimal totalNum = new BigDecimal(0);
		for (QuestionEntity question : questionList) {
			QuestionEntity answerQuestion = mapAnswerQuest
					.get(question.getId());
			QuestionEntity questionEntity = new QuestionEntity();
			questionEntity.setId(question.getId());
			questionEntity = courseService.setQuestionOneData(questionEntity);
			answerQuestion.setQuestionType(questionEntity.getQuestionType());
			if (answerQuestion != null) {
				UserMeasuermentAnswerEntity answer = new UserMeasuermentAnswerEntity();
				answer.setQuestionId(answerQuestion.getId());
				if("3".equals(answerQuestion.getQuestionType()) || "4".equals(answerQuestion.getQuestionType())) {
					QuestionOptionEntity entity = new QuestionOptionEntity();
					entity.setQuestionId(answerQuestion.getId());
					entity = courseService.selectOne(entity);
					String tkvalue = entity.getContent();
					if(tkvalue.equals(answerQuestion.getUserAnswerId())) {
						answer.setOptionId(entity.getId());
						answer.setContent(tkvalue);
						answerQuestion.setUserAnswerId(entity.getId());
					}else {
						answer.setOptionId(IDGenerator.genUID());
						answer.setContent(answerQuestion.getUserAnswerId());
					}
				}else {
					answer.setOptionId(answerQuestion.getUserAnswerId());
				}
				answer.setUserId(userId);
				// 删除旧的答案
				delete(answer, MEASUERMENT_ANSWER_DELETE);
				insert(answer, USER_MEASUERMENT_ANSWER_INSERT);
				// 插入答案历史表
				if (!MEASUERMENT_SUBMIT_TYPE_PC_PROCESS_SUBMIT
						.equals(submitType)
						&& !MEASUERMENT_SUBMIT_TYPE_APP_PROCESS_SUBMIT
								.equals(submitType)) {
					insert(answer, USER_MEASUREMENT_ANSWER_INSERTHIS);
				}
				 //计算回答是否正确
				 if(isRight(question.getReferenceAnswerId(), answerQuestion.getUserAnswerId())) {
					 goal= goal.add(question.getScore() != null ? question.getScore() : new BigDecimal(0));
					 totalNum =  totalNum.add(new BigDecimal(1));
				 }
			}

		}

		// 用户测验结果表
		UserMeasurementResultEntity resultInfo = new UserMeasurementResultEntity();
		resultInfo.setUserId(userId);
		resultInfo.setSubmitType(submitType);
		
		resultInfo.setMeasurementId(measurementId);
		// 计算总分
		resultInfo.setGoal(goal);
		// 考试
		if (MEASUERMENT_TYPE_EXAM.equals(examInfo.measurementType)) {
			// 过程提交不修改考试状态
			if (MEASUERMENT_SUBMIT_TYPE_PC_PROCESS_SUBMIT.equals(submitType)
					|| MEASUERMENT_SUBMIT_TYPE_APP_PROCESS_SUBMIT
							.equals(submitType)) {
				resultInfo.setExamStatus(EXAM_STATUS_START);
			} else {
				// 用户只需提交测试一次
				resultInfo.setExamStatus(EXAM_STATUS_END);
				resultInfo.setSubmitDate(new Date());
			}
			// 因为考试开始时已经插入UserMeasurementResult信息，故这里只做更新
			update(resultInfo);
			if (!MEASUERMENT_SUBMIT_TYPE_PC_PROCESS_SUBMIT.equals(submitType)
					&& !MEASUERMENT_SUBMIT_TYPE_APP_PROCESS_SUBMIT
							.equals(submitType)) {
				update(resultInfo, USER_MEASUREMENT_RESULT_UPDATEHIS);
			}
			// 普通测试
		} else {
			// 删除旧的答题
			delete(resultInfo, USER_MEASUERMENT_RESULT_DELETE);
			// 插入结果
			insert(resultInfo, USER_MEASUERMENT_RESULT_INSERT);

			// 插入结果
			insert(resultInfo, USER_MEASUREMENT_RESULT_INSERTHIS);
		}
		// 返回测试结果
		UserMeasurementResultView resultView = new UserMeasurementResultView();
		resultView.setGoal(resultInfo.getGoal());
		resultView.setTotalGoal(totalNum.toString());
		if (questionList != null) {
			resultView.setQuestionCount(questionList.size());
		} else {
			resultView.setQuestionCount(0);
		}
		// 如果是考试过程中的提交，不返回正确答案
		if (MEASUERMENT_SUBMIT_TYPE_PC_PROCESS_SUBMIT.equals(submitType)
				|| MEASUERMENT_SUBMIT_TYPE_APP_PROCESS_SUBMIT
						.equals(submitType)) {
			resultView.setGoal(new BigDecimal(0));
			resultView.setQuestionCount(0);
		}
		// 设置当前剩余时间
		if (MEASUERMENT_TYPE_EXAM.equals(examInfo.measurementType)) {
			resultView.setResidualTime(residualTime);
		}
		ResultBean result = new ResultBean();
		result.setData(resultView);
		return result;
	}

	/**
	 * 获取考试的相关信息
	 * 
	 * @param dataM
	 * @param examInfo
	 */
	private ExamInfo getExamInfo(MeasurementEntity dataM) {
		ExamInfo examInfo = new ExamInfo();
		if (LINK_TYPE_COURSE.equals(dataM.getLinkType())) {
			CourseEntity queryCO = new CourseEntity();
			queryCO.setId(dataM.getLinkId());
			CourseEntity course = commonDao.searchOneData(queryCO,SEARCH_COURSE_BY_PK);
			if (COURSE_STATUS_NOT_PUBLIC.equals(course.getStatus())) {
				throw new BusinessException("测试管理课程已取消发布!");
			}
			examInfo.measurementType = course.getMeasureType();
			examInfo.measureType = course.getMeasureType();
			examInfo.measureStartDate = course.getEffectiveStartDate();
			examInfo.measureEndDate = course.getEffectiveEndDate();
			examInfo.measureDuration = course.getTimeDuration();
		} else {
			ChapterEntity queryCP = new ChapterEntity();
			queryCP.setId(dataM.getLinkId());
			ChapterEntity chapter = commonDao.searchOneData(queryCP,SEARCH_CHAPTER);
			if (!CHAPTER_STATUS_PUBLIC.equals(chapter.getStatus())) {
				throw new BusinessException("测试管理课程已取消发布!");
			}
			examInfo.measurementType = chapter.getMeasureType();
			examInfo.measureType = chapter.getMeasureType();
			examInfo.measureStartDate = chapter.getEffectiveStartDate();
			examInfo.measureEndDate = chapter.getEffectiveEndDate();
			examInfo.measureDuration = chapter.getTimeDuration();
		}
		return examInfo;
	}

	@SuppressWarnings("unused")
	private static class ExamInfo {
		public String measurementType;
		public String measureType;
		public Date measureStartDate;
		public Date measureEndDate;
		public BigDecimal measureDuration;
	}

	/**
	 * 检测是否能答题
	 * 
	 * @param measureId
	 * @param userId
	 * @param examInfo
	 * @param examHandle
	 *            考试操作（开始，还是结束）
	 * @return 返回结果 剩余时间
	 */
	@SuppressWarnings("unchecked")
	private Long checkExam(Date now, String measureId, String userId,
			ExamInfo examInfo, String examHandle) {
		if (examInfo==null) {
			throw new BusinessException("无此考试信息！");
		}
		if (!MEASUERMENT_TYPE_EXAM.equals(examInfo.measurementType)) {
			throw new BusinessException("无此考试信息！");
		}
		if (examInfo.measureStartDate.after(now)) {
			throw new BusinessException("考试时间还未到，请等待！");
		}
		if (examInfo.measureEndDate.before(now)) {
			throw new BusinessException("考试时间已过，不能答题!");
		}

		// 用户结果表
		UserMeasurementResultEntity queryUMR2 = new UserMeasurementResultEntity();
		queryUMR2.setMeasurementId(measureId);
		queryUMR2.setUserId(userId);
		List<UserMeasurementResultEntity> listUMR2 = commonDao.searchList(queryUMR2,SQL_USER_MEASUERMENT_SELECT);
		// 第二次答题检测
		if (listUMR2 != null && listUMR2.size() != 0) {
			// 开始考试
			if (EXAM_STATUS_START.equals(examHandle)) {
				if (EXAM_STATUS_END.equals(listUMR2.get(0).getExamStatus())
						|| ((listUMR2.get(0).getStartDate())!=null && (listUMR2.get(0).getStartDate().getTime()
								+ examInfo.measureDuration.intValue() * 60
								* 1000 < now.getTime()))) {
					throw new BusinessException("您已参加过该考试或者考试已超时");
				}
				// 考试提交
			} else {
				if (EXAM_STATUS_END.equals(listUMR2.get(0).getExamStatus())) {
					throw new BusinessException("您已提交，请勿重复答题");
				}
				if ((listUMR2.get(0).getStartDate())!= null && listUMR2.get(0).getStartDate().getTime()
						+ examInfo.measureDuration.intValue() * 60 * 1000 + 30
						* 1000 < now.getTime()) {
					throw new BusinessException("考试已超时，禁止提交");
				}
			}
		}
		if (listUMR2 == null || listUMR2.size() == 0) {
			return examInfo.measureDuration.intValue() * 60 * 1000 + 0L;
		}
		return ((listUMR2.get(0).getStartDate() == null ) ? 0 : listUMR2.get(0).getStartDate().getTime()
				+ examInfo.measureDuration.intValue() * 60 * 1000 - now
					.getTime()) / 1000L;
	}

	/**
	 * 根据问题ID组成MAP
	 * 
	 * @param dbQuestionList
	 * @return 返回结果
	 */
	private Map<String, QuestionEntity> getMapQuestionListById(
			List<QuestionEntity> dbQuestionList) {
		Map<String, QuestionEntity> result = new HashMap<String, QuestionEntity>();
		if (dbQuestionList != null) {
			for (QuestionEntity q : dbQuestionList) {
				result.put(q.getId(), q);
			}
		}
		return result;
	}

	/**
	 * 获取当前用户数据库中的总分,通过计算用户回答的问题
	 * 
	 * @param userId
	 * @param measurementId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Integer getCurrentGoal(String userId, String linkId, String linkType) {
		MeasurementEntity dataM = new MeasurementEntity();
		dataM.setUserId(userId);
		dataM.setLinkId(linkId);
		dataM.setLinkType(linkType);
		// 检索包含用户回答结果的问题清单
		List<QuestionEntity> questionList = (List<QuestionEntity>) search(
				dataM, MEASUERMENT_SEARCH_QUESTION_LIST_BY_MEASUREMENT);

		// 插入用户结果选项
		Integer goal = 0;
		for (QuestionEntity question : questionList) {
			// 计算回答是否正确
			if (isRight(question.getReferenceAnswerId(),
					question.getUserAnswerId())) {
				goal++;
			}
		}
		return goal;
	}

	/**
	 * 判断用户选项是否正确
	 * 
	 * @param question
	 * @param answerQuestion
	 * @return 返回结果
	 */
	private boolean isRight(String referenceAnswerId, String userAnswerId) {
		if (StringUtil.isEmpty(userAnswerId)) {
			return false;
		}
		String[] questionAnswers = referenceAnswerId.split(",");
		String[] userQuestionAnswers = userAnswerId.split(",");

		if (questionAnswers.length != userQuestionAnswers.length) {
			return false;
		}
		for (String questOne : questionAnswers) {
			// 是否存在正确答案
			boolean isHave = false;
			for (String userOne : userQuestionAnswers) {
				if (userOne.equals(questOne)) {
					isHave = true;
					break;
				}
			}
			if (!isHave) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 考试开始
	 * 
	 * @param linkId 链接ID，章节/课程ID
	 * @param linkType 章节/课程 CHAPTER/COURSE
	 * @return 返回结果
	 */
	public ResultBean examStart(String linkId,String linkType) {
		if (WebContext.isGuest()) {
			ResultBean result = new ResultBean().setStatus(false).setMessages("请重新登录后尝试");
			result.setErrorCode(ErrorCode.NOT_LOGIN);
			return result;
		}
		if (StringUtil.isEmpty(linkId)) {
			return new ResultBean().setStatus(false).setMessages("当前参数错误！");
		}
		if (StringUtil.isEmpty(linkType)) {
			return new ResultBean().setStatus(false).setMessages("链接类型为空！");
		}
		ResultBean result = ResultBean.success(MSG_S_SUBMIT);
		BigDecimal time = examStart(linkId,linkType, WebContext.getSessionUserId());
		result.setData(time);
		return result;
	}

	/**
	 * 开始考试,返回剩余秒
	 * 
	 * @param linkId 链接ID 测试/考试ID
	 * @param linkType 链接类型 测试/考试，1/2
	 * @param userId 当前登录用户
	 */
	@SuppressWarnings("unchecked")
	private BigDecimal examStart(String linkId,String linkType, String userId) {
		MeasurementEntity query = new MeasurementEntity();
		query.setLinkId(linkId);
		query.setLinkType(linkType);
		List<MeasurementEntity> listM = commonDao.searchList(query,MEASUREMENT_SELECT);
		if (listM == null || listM.size() == 0) {
			throw new BusinessException("无此测试信息！");
		}

		MeasurementEntity dataM = listM.get(0);
		String measureId = dataM.getId();
		ExamInfo examInfo = getExamInfo(dataM);

		Date now = new Date();
		checkExam(now, measureId, userId, examInfo,
				CourseStateConstants.EXAM_STATUS_START);

		// 用户结果表
		UserMeasurementResultEntity queryUMR = new UserMeasurementResultEntity();
		queryUMR.setMeasurementId(measureId);
		queryUMR.setUserId(userId);
		List<UserMeasurementResultEntity> listUMR = search(queryUMR);
		// 第一次答题
		if (listUMR == null || listUMR.size() == 0) {
			queryUMR.setGoal(new BigDecimal(0));
			queryUMR.setCreateDate(now);
			queryUMR.setStartDate(now);
			queryUMR.setCreateUser(userId);
			queryUMR.setExamStatus(CourseStateConstants.EXAM_STATUS_START);
			insert(queryUMR);
			// 考试历史数据表
			insert(queryUMR, USER_MEASUREMENT_RESULT_INSERTHIS);

			// 第一次答题，并加入课程
			if (CourseStateConstants.LINK_TYPE_COURSE.equals(dataM
					.getLinkType())) {
				courseService.joinCourse(linkId);
			}
			//
			return examInfo.measureDuration.multiply(new BigDecimal(60));

		}
		// 第二次答题
		UserMeasurementResultEntity umr = listUMR.get(0);
		long result = (umr.getStartDate() == null ) ? 0 : umr.getStartDate().getTime()
				+ examInfo.measureDuration.intValue() * 60 * 1000
				- now.getTime();
		// 剩余返回时间以秒
		return new BigDecimal(result / 1000);

	}
	
	/**
	 * 获取考试题
	 * @param linkId 链接ID，章节/课程ID
	 * @param linkType 章节/课程 CHAPTER/COURSE
	 * @return ResultBean data:List<QuestionEntity>结果 题目及选项
	 */
	public ResultBean getExamList(String linkId,String linkType){
		if(WebContext.isGuest()) {
			ResultBean error = new ResultBean().setStatus(false).setMessages("请重新登录后尝试");
			error.setErrorCode(ErrorCode.NOT_LOGIN);
			return error;
		}
		if(StringUtil.isEmpty(linkId)) {
			return new ResultBean().setStatus(false).setMessages("当前参数错误！");
		}
		ResultBean result = ResultBean.success(MSG_S_SUBMIT);
		List<QuestionEntity> questionList = examGetList(linkId,linkType, WebContext.getSessionUserId());
		result.setData(questionList);
		return result;
	}
	
	/**
	 * 获取考试题
	 * @param linkId 链接ID，章节/课程ID
	 * @param linkType 章节/课程 CHAPTER/COURSE
	 * @param sessionUserId 当前登录用户ID
	 * @return List<QuestionEntity> 题目及选项
	 */
	@SuppressWarnings("unchecked")
	private List<QuestionEntity> examGetList(String linkId,String linkType, String sessionUserId) {
		MeasurementEntity query = new MeasurementEntity();
		query.setLinkId(linkId);
		query.setLinkType(linkType);
		List<MeasurementEntity> listM = search(query);
		if(listM == null || listM.size() == 0)  {
			throw new BusinessException("无此测试信息！");
		}
		
		MeasurementEntity dataM = listM.get(0);
		String measureId = dataM.getId();
		ExamInfo examInfo = getExamInfo(dataM);
		checkExam(new Date(), measureId, sessionUserId, examInfo, CourseStateConstants.EXAM_STATUS_START);
		
		List<QuestionEntity> result = (List<QuestionEntity>) search(dataM, MEASUERMENT_SEARCH_QUESTION_LIST_BY_MEASUREMENT);
		
		for(QuestionEntity quest : result) {
			//抹去正确答案
			quest.setReferenceAnswerId(null);
			
			QuestionOptionEntity optionQuery = new QuestionOptionEntity();
			optionQuery.setQuestionId(quest.getId());
			List<QuestionOptionEntity> options = (List<QuestionOptionEntity>) search(optionQuery, MEASUERMENT_SEARCH_QUESTION_OPTIONS_LIST);
			quest.setOptions(options);
		}
		return result;
	}
	
	/**
	 * 得到用户的测试结果
	 * @param um
	 * @return 返回结果
	 */
	@SuppressWarnings("unchecked")
	public UserMeasurementResultEntity findUserExamResult(UserMeasurementResultEntity um){
		List<UserMeasurementResultEntity> list = commonDao.searchList(um, SQL_MEASUERMENT_USER_SEARCH);
		return (list!=null&&list.size()>0) ? list.get(0) : null;
	}
	
	/**
	 * 查找测试
	 * @param m
	 * @return 返回结果
	 */
	@SuppressWarnings("unchecked")
	public MeasurementEntity findMeasurement(MeasurementEntity m){
		List<MeasurementEntity> ms = commonDao.searchList(m, SQL_MEASUERMENT_SEARCH);
		return (ms!=null&&ms.size()>0) ? ms.get(0) : null;
	}
	/**
     * 删除用户的考试成绩
     * @param userId 用户ID
     * @param measuermentId 测试ID
     * @author adlay
     */
    public void deleteUserMeasuerment(String userId, String measuermentId){
        //删答案
        UserMeasuermentAnswerEntity uma = new UserMeasuermentAnswerEntity();
        uma.setUserId(userId);
        uma.setMeasuermentId(measuermentId);
        this.delete(uma, SQL_MEASUREMENT_DELETE_ANSWER_OF_USER);
        //删测试
        UserMeasurementResultEntity umr = new UserMeasurementResultEntity();
        umr.setUserId(userId);
        umr.setMeasurementId(measuermentId);
        this.delete(umr);
    }
    /*
                 * 查询当前登录者的考试结果
     */
    @SuppressWarnings("unchecked")
    public List<UserMeasurementResultEntity> searchUserMeasurementResultByEntiy(UserMeasurementResultEntity query) {
        return search(query,SQL_MEASUERMENT_USER_SEARCH);
    }
    /**
     * 根据参数获取测试信息（包含用户回答信息。不区分考试还是普通测试）
     * @param userId
     * @param linkId
     * @param linkType
     * @return 返回结果
     */
    @SuppressWarnings({ "unchecked", "unused" })
    public List<QuestionEntity> searchMeasuermentNormalAndExam(String userId,
            String measurementId) {
        //补充信息
        MeasurementEntity query = new MeasurementEntity();
        query.setId(measurementId);
        query = searchOneData(query);
        if(query == null) {
            return null;
        } 
        
        query.setUserId(userId);
        ExamInfo examInfo = getExamInfo(query);
        List<QuestionEntity> result = (List<QuestionEntity>) search(query, SQL_MEASUERMENT_SEARCH_QUESTION_LIST_BY_MEASUREMENT);
        
        for(QuestionEntity quest : result) {
            QuestionOptionEntity optionQuery = new QuestionOptionEntity();
            optionQuery.setQuestionId(quest.getId());
            List<QuestionOptionEntity> options = (List<QuestionOptionEntity>) search(optionQuery, MEASUERMENT_SEARCH_QUESTION_OPTIONS_LIST);
            quest.setOptions(options);
        }
        return result;
    }

	public List<UserMeasurementResultEntity> searchInstitutionUserMeasurementResultByEntiy(
			UserMeasurementResultEntity condition) {
		return search(condition,SQL_INSTITUTION_MEASUERMENT_USER_SEARCH);
	}
    
}
