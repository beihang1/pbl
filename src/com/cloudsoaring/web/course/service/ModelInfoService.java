package com.cloudsoaring.web.course.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;
import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.ModelInfo;

/**
 * 
 * unity动画Service
 *
 */
@Service
public class ModelInfoService extends FileService implements CourseConstants {

	/**
	 * 保存unity动画信息-webGL
	 * @param entity
	 */
	public String insertModelInfo(ModelInfo entity) {
		entity.setModelId(IDGenerator.genUID());
		entity.setCreateDate(new Date());
		//entity.setCreateUser(WebContext.getSessionUser().getUserId());
		commonDao.insertData(entity, MODEL_INSERT);
		return entity.getModelId();
	}
	
	/**
	 * 保存unity动画信息-单机
	 * @param entity
	 */
	public String insertModelInfoPC(ModelInfo entity) {
		entity.setModelId(IDGenerator.genUID());
		entity.setCreateDate(new Date());
		//entity.setCreateUser(WebContext.getSessionUser().getUserId());
		commonDao.insertData(entity, MODEL_INSERT);
		return entity.getModelId();
	}
	
	/**
	 * 更新unity动画信息
	 * @param entity
	 */
	public void updateModelInfo(ModelInfo entity) {
		commonDao.updateData(entity, MODEL_UPDATE);
	}
	
	/**
	 * 更新unity场景信息
	 * @param entity
	 */
	public void updateSceneInfo(ModelInfo entity) {
		commonDao.updateData(entity, MODEL_UPDATE_SCENE);
	}
	
	/**
	 * 删除DB中的unity动画信息
	 */
	public void deleteModelInfo() {
		commonDao.deleteData(null, MODEL_DELETE_ALL);
	}
	
	/**
	 * 根据modelId获取unity动画信息
	 * @return ModelInfo
	 */
	public ModelInfo getModelInfo(String modelId){
		ModelInfo modelInfo = new ModelInfo();
		modelInfo.setModelId(modelId);
		return commonDao.searchOneData(modelInfo, MODEL_SELECT_BY_MODELID);
	}
	
	/**
	 * 根据登录用户获取unity动画信息-web端创客编程
	 * @return List<ModelInfo>
	 */
	@SuppressWarnings("unchecked")
	public List<ModelInfo> getModelInfoByUserId(){
		ModelInfo modelInfo = new ModelInfo();
		modelInfo.setCreateUser(WebContext.getSessionUser().getUserId());
		return commonDao.searchList(modelInfo, MODEL_SELECT_BY_USER);
	}
	
	public List<ModelInfo> getModelInfoByUserId(ModelInfo modelInfo){
		return commonDao.searchList(modelInfo, MODEL_SELECT_BY_USER_CHAPTER);
	}
	
	/**
	 * 根据登录用户获取unity动画信息-单机版创客编程
	 * @return List<ModelInfo>
	 */
	@SuppressWarnings("unchecked")
	public List<ModelInfo> getModelInfoByUserIdToUnity(String userId){
		ModelInfo modelInfo = new ModelInfo();
		modelInfo.setCreateUser(userId);
		return commonDao.searchList(modelInfo, MODEL_SELECT_BY_USER);
	}
	
	public List<ModelInfo> getUserPracticeList(ModelInfo modelInfo){
		return commonDao.searchList(modelInfo, MODEL_SELECT_PRACTICE_BY_USER_CHAPTER);
	}
	/**
	 * 检查modelName的唯一性
	 * @return String
	 */
	@SuppressWarnings("unchecked")
	public String checkModelNameUnique(String modelName) {
		ModelInfo modelInfo = new ModelInfo();
		modelInfo.setModelName(modelName);
		List<ModelInfo> modelList = commonDao.searchList(modelInfo, MODEL_SELECT_BY_NAME);
		if(modelList.size() > 0) {
			return "notUnique";
		}else {
			return "unique";
		}
	}
	
	/**
	 * 删除场景信息
	 * @return String
	 */
	@SuppressWarnings("unchecked")
	public boolean delete(String modelId) {
		ModelInfo modelInfo = new ModelInfo();
		modelInfo.setModelId(modelId);
		int result = commonDao.deleteData(modelInfo, MODEL_DELETE_BY_ID);
		if(result > 0) {
			return true;
		}else {
			return false;
		}
	}
	
	public void updateModelInfoPC(ModelInfo entity) {
		commonDao.updateData(entity, MODEL_UPDATE_PC);
	}
	
	public ModelInfo getModelInfo(Integer No){
		if(No.equals(1)) {
			return commonDao.searchOneData(null, MODEL_SELECT_ONE);
		}else if(No.equals(2)){
			return commonDao.searchOneData(null, MODEL_SELECT_TWO);
		}else if(No.equals(3)) {
			return commonDao.searchOneData(null, MODEL_SELECT_THREE);
		}else {
			return null;
		}
	}
}
