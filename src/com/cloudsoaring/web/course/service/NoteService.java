package com.cloudsoaring.web.course.service;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.FileEntity;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.constant.Constants;
import com.cloudsoaring.web.common.service.BaseService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.constant.CourseStateConstants;
import com.cloudsoaring.web.course.entity.ChapterEntity;
import com.cloudsoaring.web.course.entity.NoteDataEntity;
import com.cloudsoaring.web.course.entity.NoteEntity;
import com.cloudsoaring.web.course.entity.OperateRecordEntity;
import com.cloudsoaring.web.course.view.CourseView;
import com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity;
/**
 * 笔记模块操作
 * @author yuntengsoft
 *
 */
@Service
public class NoteService  extends FileService implements CourseConstants{
	/**
	 * 检索笔记列表
	 * @return
	 */
	public ResultBean noteList(NoteDataEntity nd){
		assertNotEmpty(nd.getLinkId(), MSG_E_NO_NOTE);
		ResultBean result = new ResultBean();
		// 精华
		if(NOTE_SEARCH_LIKE.equals(nd.getPageType())){
			nd.setSortName("likeNum");
			nd.setSortOrder("DESC");
		} else {
			// 默认全部 
			nd.setSortName("createDate");
			nd.setSortOrder("DESC");
		}
		
		nd.setLoginId(WebContext.getSessionUserId());
		if(LINK_TYPE_COURSE.equals(nd.getLinkType())){
			result = searchList4Page(nd,SEARCH_COURSE_NOTE);
		}else if(LINK_TYPE_CHAPTER.equals(nd.getLinkType())){
			result = searchList4Page(nd,SEARCH_NOTE);
		}
		return result;
	}
	
	/**
	 * 课程笔记：课程/章节笔记点赞
	 * @return
	 */
	public ResultBean noteClickLike(String id){
		assertNotGuest();
		assertNotEmpty(id, MSG_E_EMPTY_NOTE_ID);
		NoteDataEntity nd=new NoteDataEntity();
		nd.setId(id);
		nd=super.searchOneData(nd);
		if(nd==null){
			return ResultBean.error(MSG_E_NO_NOTE);
		}
		
		ResultBean r=new ResultBean();
		OperateRecordEntity ore=getOperateRecord(OPERATE_LIKE,nd.getId(),WebContext.getSessionUserId());
		if(ore==null){
			addOperate(nd.getId(),OPERATE_LIKE);
			nd.setLikeNum(new java.math.BigDecimal(nd.getLikeNum().intValue()+1));
			super.update(nd,NOTE_LIKE);
		}else{
			
			super.update(ore);
			r.setMessages("您已经点过赞，不需要重复点击");
		} 
		
		r.setData(nd.getLikeNum());
		return r;
	}
	
	/**
	 * 课程/章节笔记采集
	 * @return
	 */
	public ResultBean noteCollect(String id){
		assertNotGuest();
		assertNotEmpty(id, MSG_E_EMPTY_NOTE_ID);
		NoteDataEntity nd=new NoteDataEntity();
		nd.setId(id);
		nd=super.searchOneData(nd);
		if(nd==null){
			return ResultBean.error(MSG_E_NO_NOTE);
		}
		
		ResultBean r=new ResultBean();
		OperateRecordEntity ore=getOperateRecord(OPERATE_COLLECT,nd.getId(),WebContext.getSessionUserId());
		if(ore==null){
			nd.setCollectionNum(new java.math.BigDecimal(nd.getCollectionNum().intValue()+1));
			super.update(nd,NOTE_COLLECT);
			addOperate(nd.getId(),OPERATE_COLLECT);
		}else{
			r.setMessages("您已经采集过该记录，不需要重复");
			super.update(ore);
		} 
		
		r.setData(nd.getCollectionNum());
		return r;
	}

	/**
	 * 课程/章节笔记采集取消
	 * @param id 笔记数据ID NoteDataEntity的ID
	 * @return
	 */
	public ResultBean deleteNoteCollect(String id){
		assertNotGuest();
		assertNotEmpty(id, MSG_E_EMPTY_NOTE_ID);
		NoteDataEntity nd=new NoteDataEntity();
		nd.setId(id);
		nd=super.searchOneData(nd);
		if(nd==null){
			return ResultBean.error(MSG_E_NO_NOTE);
		}
		
		ResultBean r=new ResultBean();
		OperateRecordEntity ore=getOperateRecord(OPERATE_COLLECT,nd.getId(),WebContext.getSessionUserId());
		if(ore!=null){
			nd.setCollectionNum(new java.math.BigDecimal(nd.getCollectionNum().intValue()-1));
			super.update(nd,NOTE_COLLECT);
			rejectOperate(ore);
			r.setData(true);
		}else{
			r.setMessages("您还没有采集该笔记，不能进行取消操作");
			r.setData(false);
		} 
		
		return r;
	}
	private void rejectOperate(OperateRecordEntity ore){
		super.delete(ore);
	}
	
	/**
	 * 采集操作取消
	 * @param type
	 */
	public void rejectOperate(String nodeDataId,String type){
		OperateRecordEntity ore=new OperateRecordEntity();
		ore.setId(StringUtil.uuid());
		ore.setLink(nodeDataId);
		ore.setOperateUser(WebContext.getSessionUserId());
		ore.setOperateType(type);
		super.delete(ore);
	}

	
	/**
	 * 根据操作类型，操作对象，操作用户来
	 * 判断此操作是否存在
	 * @return
	 */
	private OperateRecordEntity getOperateRecord(String type,String link,String operateUserId){
		OperateRecordEntity ore=new OperateRecordEntity();
		ore.setLink(link);
		ore.setOperateUser(operateUserId);
		ore.setOperateType(type);
		return super.searchOneData(ore,"OperateRecord.searchOneByOpreate");
	}
	/**
	 * 采集操作添加
	 * @param type
	 */
	private void addOperate(String nodeDataId,String type){
		OperateRecordEntity ore=new OperateRecordEntity();
		ore.setId(StringUtil.uuid());
		ore.setLink(nodeDataId);
		ore.setOperateUser(WebContext.getSessionUserId());
		ore.setOperateType(type);
		super.insert(ore);
	}
	
	
	

	/**
	 * 我的笔记
	 * @param type
	 * 		笔记分类（我的笔记0，收藏的笔记1）
	 * @return 返回结果
	 */
	public ResultBean userNoteList(NoteDataEntity nd,String type,Integer pageSize,Integer pageNumber) {
		// 判断用户是否登录
		assertNotGuest();
		String userId = WebContext.getSessionUserId();
		nd.setUserId(userId);
		
		if(USER_COLLECT_NOTE.equals(type)){
			nd.setSortName("createDate");
			nd.setPageNumber(pageNumber);
			nd.setPageSize(pageSize);
		return super.searchList4Page(nd,SEARCH_USER_COLLECT);
		}
		else{
			nd.setSortName("createDate");
			nd.setPageNumber(pageNumber);
			nd.setPageSize(pageSize);
		return super.searchList4Page(nd,SEARCH_USER_NOTE);
		}
		
	}
	
	/**
	 * 删除我的笔记（我的笔记、采集的笔记）
	 * @param 
	 *           
	*/ 
	public ResultBean deleteUserNoteList(String id){
		ResultBean result = new ResultBean();
		// 从session中取出登录用户的ID，判断用户是否登录
		if (WebContext.isGuest()) {
			result.setStatus(false);
			result.setMessages(getMessage(NULL_USER_ID));
			return result;
		}
		if(id == null || id.equals("")){
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_INTERFACE_VALUE));
			return result;
		}
		NoteDataEntity nd=new NoteDataEntity();
		nd.setId(id);
		nd=super.searchOneData(nd);
		if(nd==null){
			return ResultBean.error(MSG_E_NO_NOTE);
		}
		ResultBean r=new ResultBean();
		if(nd!=null){
			super.update(nd);
			deleteNote(nd);
			r.setData(true);
		}
		
		return super.searchList4Page(nd,SEARCH_USER_NOTE);
	}
	private void deleteNote(NoteDataEntity nd){
		super.delete(nd);
	}
	
	public void deleteNote(String id){
		NoteDataEntity nd=new NoteDataEntity();
		nd.setId(StringUtil.uuid());
		nd.setCreateUser(WebContext.getSessionUserId());
		super.delete(nd);
	}
	
	/**
	 * 判断当前登录用户是否为教师角色
	 * @return true:是教师，false:不是教师
	 */
	public boolean isTeacher(){
		UserRoleEntity ur = new UserRoleEntity();
		ur.setUserId(WebContext.getSessionUserId());
		ur.setRoleId(ROLE_TEACHER_ROLE);
		int count = commonDao.searchCount(ur, SQL_IS_TEACHHER);
		return count > 0;
	}
	
	/**
	 * 删除笔记数据
	 * @param id 笔记数据ID
	 * @return
	 */
	public ResultBean deleteNoteById(String id){
		assertNotGuest();
		assertNotEmpty(id, MSG_E_EMPTY_NOTE_ID);
		NoteDataEntity n = new NoteDataEntity();
		n.setId(id);
		assertExist(n, SQL_IS_NOTE_DATA_EXIST, MSG_E_EMPTY_NOTE_DATA);
		ResultBean result = new ResultBean();
		n = commonDao.searchOneData(n, SQL_SEARCH_NOTE_DATA_BY_PK);
		NoteEntity note = new NoteEntity();
		note.setId(n.getNoteId());
		note = commonDao.searchOneData(note, SQL_SEARCH_NOTE);
		n.setLinkId(note.getLinkId());
		n.setLinkType(note.getLinkType());
		n.setLoginId(WebContext.getSessionUserId());
		n = commonDao.searchOneData(n, SQL_NOTE_DATA_CAN_DELETE);
		if(n==null||!n.getCanDelete()){
			result.setStatus(false).setMessages(MSG_E_NOT_ACCESS_DELETE_NOTE_DATA);
			return result;
		}
		//删除笔记
		commonDao.deleteData(n, SQL_DELETE_BY_PK);
		//删除采集、点赞等数据
		OperateRecordEntity ore = new OperateRecordEntity();
		ore.setLink(n.getId());
		ore.setOperateType(OPERATE_COLLECT);
		commonDao.deleteData(ore, SQL_DELETE_OPERATE_BY_LINK_AND_TYPE);
		result.setStatus(true);
		return result;
	}
	
	/***
	 * 新增笔记数据
	 * @param noteData 笔记数据
	 * @param linkId 链接ID
	 * @param linkType 链接类型
	 * @param title 笔记标题
	 * @return 
	 * @throws Exception 
	 */
	public ResultBean saveNote(NoteDataEntity noteData,String linkId,String linkType,String title) throws Exception{
		FileEntity resultData = processRequestFile("file1", Constants.FILE_TYPE_IMAGE);
		if(resultData != null) {
			noteData.setFileId(resultData.getFileId());
			String type = resultData.getContentType();
			if(type.toUpperCase().equals("PNG") || type.toUpperCase().equals("JPG") || type.toUpperCase().equals("JPEG") || type.toUpperCase().equals("BMP")
					|| type.toUpperCase().equals("TIFF") || type.toUpperCase().equals("GIF")) {
				noteData.setType("1");
			}else {
				noteData.setType("2");
			}
		}else {
			noteData.setType("0");//0:纯文本;1:图片;2:其他
		}
		ResultBean result = new ResultBean();
		assertNotEmpty(linkId, MSG_E_EMPTY_LINK_ID);
		assertNotEmpty(linkType, MSG_E_EMPTY_LINK_TYPE);
		// 链接类型错误：不等于COURSE or CHAPTER
		if (!linkType.equalsIgnoreCase(LINK_TYPE_COURSE)
				&& !linkType.equalsIgnoreCase(LINK_TYPE_CHAPTER)) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_ERROR_LINK_TYPE));
			return result;
		}
		if(noteData == null){
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_NO_NOTE));
			return result;
		}
		//判断linkID存不存在
		if(LINK_TYPE_COURSE.equals(linkType)){
			//判断课程是否存在
			CourseView view = new CourseView();
			view.setId(linkId);
			view.setStatus(COURSE_STATUS_PUBLIC);
			assertExist(view, SQL_IS_COURSE_EXIST, MSG_E_EMPTY_COURSE);
		}else if(LINK_TYPE_CHAPTER.equals(linkType)){
			//判断章节是否存在
			ChapterEntity entity = new ChapterEntity();
			entity.setId(linkId);
			entity.setStatus(CHAPTER_STATUS_PUBLIC);
			assertExist(entity, SQL_IS_CHAPTER_EXIST, MSG_E_EMPTY_CHAPTER);
		}
		//检索笔记是否存在
		NoteEntity ne = new NoteEntity();
		ne.setLinkId(linkId);
		ne.setLinkType(linkType);
		ne = commonDao.searchOneData(ne, SQL_SEARCH_NOTE);
		if(ne == null){
			//不存在新增
			ne = new NoteEntity();
			ne.setId(IDGenerator.genUID());
			ne.setLinkId(linkId);
			ne.setLinkType(linkType);
			ne.setTitle(title);
			ne.setCreateUser(WebContext.getSessionUserId());
			commonDao.insertData(ne, SQL_INSERT_NOTE);
		}
		//新增笔记数据
		noteData.setId(IDGenerator.genUID());
		noteData.setNoteId(ne.getId());
		noteData.setUserId(WebContext.getSessionUserId());
		//noteData.setScreenshotId(IDGenerator.genUID());
		noteData.setCreateUser(WebContext.getSessionUserId());
		noteData.setLikeNum(BigDecimal.ZERO);
		noteData.setCollectionNum(BigDecimal.ZERO);
		commonDao.insertData(noteData, SQL_INSERT_NOTE_DATA);
		//插入截图数据，待完成
		//TODO
		
		result.setStatus(true);
		return result;
	}
}
	
	