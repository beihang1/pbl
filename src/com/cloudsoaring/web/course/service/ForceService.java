package com.cloudsoaring.web.course.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.web.common.constant.Constants;
import com.cloudsoaring.web.common.service.BaseService;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.ForceEntity;

/***
 * 力的作用数据处理Service
 * @author fanglili
 */
@Service
public class ForceService extends BaseService implements CourseConstants,Constants{

	public String saveForce(ForceEntity entity) {
		entity.setId(IDGenerator.genUID());
		String userName = commonDao.searchOneData(entity.getUserId(), ER_SELECT_GETUSERNAME);
		entity.setUserName(userName);
		entity.setCreateTime(new Date());
		return commonDao.insertData(entity, FORCE_INSERT).toString();
	}
	
	@SuppressWarnings("unchecked")
	public List<ForceEntity> getLastTenForceList(String userId){
		ForceEntity entity = new ForceEntity();
		entity.setUserId(userId);	
		return commonDao.searchList(entity, FORCE_SELECT_LASTTEN);
	}
}
