package com.cloudsoaring.web.course.service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.common.service.BaseService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.constant.CourseStateConstants;
import com.cloudsoaring.web.course.entity.ChapterEntity;
import com.cloudsoaring.web.course.entity.CourseEntity;
import com.cloudsoaring.web.course.entity.CourseMessageEntity;
import com.cloudsoaring.web.course.entity.UserChatperEntity;
import com.cloudsoaring.web.course.entity.UserCourseEntity;
import com.cloudsoaring.web.pointmanager.constant.PointStateConstants;
import com.cloudsoaring.web.pointmanager.service.PointManagerService;
import com.cloudsoaring.web.points.constant.PointConstants;
import com.cloudsoaring.web.points.entity.PointTypeEntity;
import com.cloudsoaring.web.points.service.PointService;
/**
 * 投票相关业务处理类
 * @author liuyanshuang
 *
 */
@Service
public class VoteService extends BaseService implements CourseConstants{

	@Autowired
	private CourseService courseService;
	@Autowired
	private ChapterService chapterService;
	@Autowired
	private PointManagerService pointManagerService;
	@Autowired
	private PointService pointService;
	/**
	 * 
	 * @param linkId 链接ID
	 * @param voteId 投票选项ID
	 * @param linkType 链接类型
	 * @return
	 */
	public ResultBean submitVote(String linkId,String voteId,String linkType){
		if(StringUtil.isEmpty(linkType)){
			return new ResultBean().setStatus(false).setMessages("提交失败，参数为空");
		}
		if(linkType.equals(VOTE_LINK_TYPE_COURSE)){
			//课程投票
			return voteCourse(linkId,voteId);
		}else if(linkType.equals(VOTE_LINK_TYPE_CHAPTER)){
			//章节投票
			return voteChapter(linkId,voteId);
		}
		return new ResultBean().setStatus(false).setMessages("系统繁忙，请稍后再试！");
	}
	
	/**
	 * 提交课程投票
	 * @param linkId 链接ID
	 * @param voteId 投票选项ID
	 * @return
	 */
	private ResultBean voteCourse(String linkId,String voteId){
		//校验课程ID
		if(StringUtil.isEmpty(linkId)) {
			return new ResultBean().setStatus(false).setMessages("提交失败，课程ID为空！");
		}
		
		// 课程不存在
		assertExist(linkId, SQL_IS_COURSE_EXIST, MSG_E_COURSE_NOT_EXIST);
		if(WebContext.isGuest()){
			return new ResultBean().setStatus(false).setMessages("请登录后再试！");
		}
		if(!VOTE_OPTION_GOOD.equals(voteId)
				&& !VOTE_OPTION_NORMAL.equals(voteId)
				&&  !VOTE_OPTION_NOT_GOOD.equals(voteId)) {
			return ResultBean.error("此投票项不存在!");
		}
		if(VOTE_OPTION_GOOD.equals(voteId)) {
			courseService.addVoteGood(linkId);
		}
		if(VOTE_OPTION_NORMAL.equals(voteId)) {
			courseService.addVoteNormal(linkId);
		}
		courseService.addVoteTotal(linkId);
		
		pointService.earnPoint(PointConstants.SYSTEM_CODE_HS, WebContext.getSessionUserId(), PointStateConstants.POINT_TYPE_VOTE, null, null, null);
		return submitCourseVote(WebContext.getSessionUserId(),linkId,voteId);
	}
	
	/***
	 * 章节投票提交
	 * @param linkId
	 * @param voteId
	 * @return
	 */
	private ResultBean voteChapter(String linkId,String voteId){
		//校验章节ID
		if(StringUtil.isEmpty(linkId)) {
			return new ResultBean().setStatus(false).setMessages("提交失败，章节ID为空！");
		}
		// 章节不存在
		assertExist(linkId, SQL_IS_CHAPTER_EXIST, MSG_E_CHAPTER_NOT_EXIST);
		
		if(WebContext.isGuest()){
			return new ResultBean().setStatus(false).setMessages("请登录后再试！");
		}
		if(StringUtil.isEmpty(linkId)) {
			return ResultBean.error(getMessage(""));
		}
		if(!VOTE_OPTION_GOOD.equals(voteId)
				&& !VOTE_OPTION_NORMAL.equals(voteId)
				&&  !VOTE_OPTION_NOT_GOOD.equals(voteId)) {
			return new ResultBean().setStatus(false).setMessages("此投票项不存在!");
		}
		return submitChapterVote(WebContext.getSessionUserId(), linkId, voteId);
	}
	
	/**
	 * 提交选项信息
	 * @param userId
	 * @param courseId
	 * @param voteId
	 */
	private ResultBean submitCourseVote(String userId, String courseId,
			String voteId) {
		//插入自己的评分
		UserCourseEntity userCourse = new UserCourseEntity();
		userCourse.setUserId(userId);
		userCourse.setCourseId(courseId);
		userCourse.setVote(voteId);
		int updateCount = update(userCourse, UPDATE_USER_COURSE_VOTE);
		if(updateCount == 0) {
			return new ResultBean().setStatus(false).setMessages("当前用户没有课程跟踪信息！");
		}
		//检索投票信息
		UserCourseEntity userCourseQuery = new UserCourseEntity();
		userCourseQuery.setUserId(userId);
		userCourseQuery.setCourseId(courseId);
		BigDecimal voteCount  = (BigDecimal) searchOneData(userCourseQuery, SELECT_USER_COURSE_VOTE_COUNT);
		userCourseQuery.setVote(VOTE_OPTION_GOOD);
		BigDecimal voteGoodSum = (BigDecimal) searchOneData(userCourseQuery, SELECT_USER_COURSE_VOTE_COUNT);
		userCourseQuery.setVote(VOTE_OPTION_NORMAL);
		BigDecimal voteNormalSum = (BigDecimal) searchOneData(userCourseQuery, SELECT_USER_COURSE_VOTE_COUNT);
		//计算百分比
		BigDecimal CourseVoteGood = voteGoodSum.divide(voteCount,2, BigDecimal.ROUND_UP).multiply(new BigDecimal(100));
		BigDecimal CourseVoteNormal = voteNormalSum.divide(voteCount,2, BigDecimal.ROUND_UP).multiply(new BigDecimal(100));
		if(CourseVoteNormal.add(CourseVoteGood).intValue() > 100) {
			CourseVoteNormal = new BigDecimal(100).subtract(CourseVoteGood);
		}
		//更新章节平均分
		CourseEntity Course = new CourseEntity();
		Course.setId(courseId);
		Course.setVoteGood(CourseVoteGood);
		Course.setVoteNormal(CourseVoteNormal);
		update(Course, UPDATE_COURSE_VOTE);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("voteGood", CourseVoteGood);
		map.put("voteNormal", CourseVoteNormal);
		PointTypeEntity ptr = (PointTypeEntity)WebContext.session(PointStateConstants.POINT_TYPE_VOTE);
		if(ptr == null){
			PointTypeEntity ptc = new PointTypeEntity(PointConstants.SYSTEM_CODE_HS, PointStateConstants.POINT_TYPE_VOTE); 
			ptc.setSumKbn(SUM_KBN_EARN);
			ptr = pointManagerService.searchOneData(ptc);
			WebContext.session(PointStateConstants.POINT_TYPE_VOTE, ptr);
		}
		ResultBean result = new ResultBean();
		result.setStatus(true);
		result.setData(map);
		result.setMessages("恭喜你投票成功");
		return result;
	}
	
	/**
	 * 提交选项的信息
	 * @param userId
	 * @param chapterId
	 * @param voteId
	 */
	private ResultBean submitChapterVote(String userId, String chapterId,
			String voteId) {
		//插入自己的评分
		UserChatperEntity userChapter = new UserChatperEntity();
		userChapter.setUserId(userId);
		userChapter.setChapterId(chapterId);
		userChapter.setVote(voteId);
		int updateCount = update(userChapter, UPDATE_USER_CHAPTER_VOTE);
		if(updateCount == 0) {
			return new ResultBean().setStatus(false).setMessages("当前用户没有章节跟踪信息！");
		}
		//检索投票信息
		UserChatperEntity userChapterQuery = new UserChatperEntity();
		userChapterQuery.setUserId(userId);
		userChapterQuery.setChapterId(chapterId);
		BigDecimal voteCount  = (BigDecimal) searchOneData(userChapterQuery, SELECT_USER_CHAPTER_VOTE_COUNT);
		userChapterQuery.setVote(VOTE_OPTION_GOOD);
		BigDecimal voteGoodSum = (BigDecimal) searchOneData(userChapterQuery, SELECT_USER_CHAPTER_VOTE_COUNT);
		userChapterQuery.setVote(VOTE_OPTION_NORMAL);
		BigDecimal voteNormalSum = (BigDecimal) searchOneData(userChapterQuery, SELECT_USER_CHAPTER_VOTE_COUNT);
		
		//计算百分比
		BigDecimal chapterVoteGood = voteGoodSum.divide(voteCount,2, BigDecimal.ROUND_UP).multiply(new BigDecimal(100));
		BigDecimal chapterVoteNormal = voteNormalSum.divide(voteCount,2, BigDecimal.ROUND_UP).multiply(new BigDecimal(100));
		if(chapterVoteNormal.add(chapterVoteGood).intValue() > 100) {
			chapterVoteNormal = new BigDecimal(100).subtract(chapterVoteGood);
		}
		//更新章节平均分
		ChapterEntity chapter = new ChapterEntity();
		chapter.setId(chapterId);
		chapter.setVoteGood(chapterVoteGood);
		chapter.setVoteNormal(chapterVoteNormal);
		update(chapter, UPDATE_CHAPTER_VOTE);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("voteGood", chapterVoteGood);
		map.put("voteNormal", chapterVoteNormal);
		ResultBean result = new ResultBean();
		result.setStatus(true);
		result.setData(map);
		return result;
	}

	/**
	 * 投票详情
	 * @author heyaqin
	 * @param linkId 链接ID
	 * @param linkType  链接类型
	 * @return
	 */
	public ResultBean voteDetail(String linkId, String linkType) {
		ResultBean result = new ResultBean();
		//根据linkType判断是课程还是章节
		if(StringUtil.equals(linkType, CourseStateConstants.LINK_TYPE_COURSE)){
			//课程
			CourseEntity courseEntity =  new CourseEntity();
			courseEntity.setId(linkId);
			CourseEntity course = searchOneData(courseEntity,SEARCH_COURSE_DATA);
			UserCourseEntity userCourseEntity = new UserCourseEntity();
			userCourseEntity.setCourseId(linkId);
			//好评票数
			userCourseEntity.setVote(CourseStateConstants.VOTE_OPTION_GOOD);
			int goodVote = searchTotal(userCourseEntity, SEARCH_COUNT_VOTE_COURSE);
			//中评票数
			userCourseEntity.setVote(CourseStateConstants.VOTE_OPTION_NORMAL);
			int normalVote = searchTotal(userCourseEntity, SEARCH_COUNT_VOTE_COURSE);
			//差评票数
			userCourseEntity.setVote(CourseStateConstants.VOTE_OPTION_NOT_GOOD);
			int notGoodVote = searchTotal(userCourseEntity, SEARCH_COUNT_VOTE_COURSE);
			
			course.setGoodVote(goodVote);
			course.setNormalVote(normalVote);
			course.setNotGoodVote(notGoodVote);
			//总票数
			course.setTotalVote(goodVote+normalVote+notGoodVote);
			result.setData(course);
		}else if (StringUtil.equals(linkType, CourseStateConstants.LINK_TYPE_CHAPTER)) {
			//章节
			ChapterEntity chapterEntity = new ChapterEntity();
			chapterEntity.setId(linkId);
			ChapterEntity chapter = searchOneData(chapterEntity, SEARCH_CHAPTER_DATA);
			UserChatperEntity userChatperEntity = new UserChatperEntity();
			userChatperEntity.setChapterId(linkId);
			//好评票数
			userChatperEntity.setVote(CourseStateConstants.VOTE_OPTION_GOOD);
			int goodVote = searchTotal(userChatperEntity, SEARCH_COUNT_VOTE_CHAPTER);
			//中评票数
			userChatperEntity.setVote(CourseStateConstants.VOTE_OPTION_NORMAL);
			int normalVote = searchTotal(userChatperEntity, SEARCH_COUNT_VOTE_CHAPTER);
			//差评票数
			userChatperEntity.setVote(CourseStateConstants.VOTE_OPTION_NOT_GOOD);
			int notGoodVote = searchTotal(userChatperEntity, SEARCH_COUNT_VOTE_CHAPTER);
			chapter.setGoodVote(goodVote);
			chapter.setNormalVote(normalVote);
			chapter.setNotGoodVote(notGoodVote);
			//总票数
			chapter.setTotalVote(goodVote+normalVote+notGoodVote);
			result.setData(chapter);
		}
		return result;
	}
}
