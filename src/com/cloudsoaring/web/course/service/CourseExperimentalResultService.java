package com.cloudsoaring.web.course.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;
import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.common.constant.Constants;
import com.cloudsoaring.web.common.service.BaseService;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.CourseExperimentalResultEntity;
import net.sf.json.JSONObject;

/***
 * 显微镜课程数据处理Service
 * @author fanglili
 */
@Service
public class CourseExperimentalResultService extends BaseService implements CourseConstants,Constants{
	/**
	 * 保存显微镜操作信息
	 * @param entity
	 */
	public String insertExperimentalResult(CourseExperimentalResultEntity entity) {
		entity.setId(IDGenerator.genUID());
		/**1.根据步骤ID设置步骤内容*/
		if(entity.getFirstStepId() != null && entity.getFirstStepId().equals("1")) {
			entity.setFirstStepName("取镜和安放");
			if(entity.getSecondStepId() != null && entity.getSecondStepId().equals("11")) {
				entity.setSecondStepName("1.选择题：取镜方式为（）");
			}else if(entity.getSecondStepId() != null && entity.getSecondStepId().equals("12")) {
				entity.setSecondStepName("2.选择题：把显微镜放在实验台距边缘（）处");
			}else if(entity.getSecondStepId() != null && entity.getSecondStepId().equals("13")) {
				entity.setSecondStepName("");
			}else if(entity.getSecondStepId() != null && entity.getSecondStepId().equals("14")) {
				entity.setSecondStepName("");
			}
		}else if(entity.getFirstStepId().equals("2")) {
			entity.setFirstStepName("对光 ");
			if(entity.getSecondStepId() != null && entity.getSecondStepId().equals("21")) {
				entity.setSecondStepName("1.①转动转换器，使低倍物镜对准通光孔②	选择题：物镜前端与载物台要保持（）距离");
			}else if(entity.getSecondStepId() != null && entity.getSecondStepId().equals("22")) {
				entity.setSecondStepName("2.调节反光镜");
			}else if(entity.getSecondStepId() != null && entity.getSecondStepId().equals("23")) {
				entity.setSecondStepName("");
			}else if(entity.getSecondStepId() != null && entity.getSecondStepId().equals("24")) {
				entity.setSecondStepName("");
			}
		}else if(entity.getFirstStepId().equals("3")) {
			entity.setFirstStepName("观察");
			if(entity.getSecondStepId() != null && entity.getSecondStepId().equals("31")) {
				entity.setSecondStepName("1.把玻片放在载物台上，标本对准通光孔");
			}else if(entity.getSecondStepId() != null && entity.getSecondStepId().equals("32")) {
				entity.setSecondStepName("2.选择题：粗准焦螺旋在下降时，眼睛看（）部位");
			}else if(entity.getSecondStepId() != null && entity.getSecondStepId().equals("33")) {
				entity.setSecondStepName("3.转动粗准焦螺旋，调节物镜");
			}else if(entity.getSecondStepId() != null && entity.getSecondStepId().equals("34")) {
				entity.setSecondStepName("4.调节细准焦螺旋，至看清物像");
			}
		}else if(entity.getFirstStepId().equals("4")) {
			entity.setFirstStepName("练习");
			if(entity.getSecondStepId() != null && entity.getSecondStepId().equals("41")) {
				entity.setSecondStepName("1、移动玻片标本，观察物像移动方向");
			}else if(entity.getSecondStepId() != null && entity.getSecondStepId().equals("42")) {
				entity.setSecondStepName("2、选择题：在目镜中观察，物像移动方向为（）");
			}else if(entity.getSecondStepId() != null && entity.getSecondStepId().equals("43")) {
				entity.setSecondStepName("");
			}else if(entity.getSecondStepId() != null && entity.getSecondStepId().equals("44")) {
				entity.setSecondStepName("");
			}
		}
		//entity.setCourseID(courseID);
		String userName = commonDao.searchOneData(entity.getUserId(), ER_SELECT_GETUSERNAME);
		entity.setUserName(userName);
		entity.setCreateTime(new Date());
		return commonDao.insertData(entity, INSERT_ER).toString();
	}

	/**
	 * 获取所有人员步骤得分情况
	 * @param null
	 * @return List<CourseExperimentalResultEntity>
	 */
	@SuppressWarnings("unchecked")
	public List<CourseExperimentalResultEntity> getUserStepScore(){
		return commonDao.searchList(null, ER_SELECT_USERSTEPSCORE);
	}
	
	/**
	 * 获取所有人员所有步骤平均分情况
	 * @param null
	 * @return List<CourseExperimentalResultEntity>
	 */
	@SuppressWarnings("unchecked")
	public List<CourseExperimentalResultEntity> getUserAllStepScore(){
		return commonDao.searchList(null, ER_SELECT_USERALLSTEPSCORE);
	}
	
	/**
	 * 获取近10次成绩折线图
	 * @param null
	 * @return List<CourseExperimentalResultEntity>
	 */
	@SuppressWarnings("unchecked")
	public List<CourseExperimentalResultEntity> getUserTenTimesScore(String userId){
		CourseExperimentalResultEntity entity = new CourseExperimentalResultEntity();
		entity.setUserId(userId);
		return commonDao.searchList(entity, ER_SELECT_USERTENTIMESCORE);
	}
	
	/**
	 * 获取操作详情
	 * @param null
	 * @return List<CourseExperimentalResultEntity>
	 */
	@SuppressWarnings("unchecked")
	public List<CourseExperimentalResultEntity> getUserOperationDetail(String userId, Date enterTime){
		CourseExperimentalResultEntity entity = new CourseExperimentalResultEntity();
		entity.setUserId(userId);
		entity.setEnterTime(enterTime);
		return commonDao.searchList(entity, ER_SELECT_USEROPERATIONDETAIL);
	}
	
	public static void main(String[] args) {
		String s1 = "{\"Step\":\"2,22\",\"Score\":100.0,\"UsingTime\":2.1991333961486818,\"EnterTime\":\"2019-03-15 15:19:52\",\"UserActions\":\"\",\"StandardActions\":\"\"}";
		
		JSONObject json_test = JSONObject.fromObject(s1);
		//System.out.println(json_test.get("Step"));
		//System.out.println(json_test.get("Score"));
		//System.out.println(json_test.get("UsingTime"));
		//System.out.println(json_test.get("EnterTime"));
		///System.out.println(json_test.get("UserActions"));
		//System.out.println(json_test.get("StandardActions"));
		
		String s2 = "2,22";
		String[] arr= s2.split(",");
		//System.out.println(arr[0]);
		//System.out.println(arr[1]);
		
		List<String> seriesData = new ArrayList<String>();
		seriesData.add("0");
		seriesData.add("0");
		seriesData.add("0");
		seriesData.add("0");
		for(String s : seriesData) {
			System.out.println(s);
		}
		System.out.println("----------");
		seriesData.set(0, "11");
		seriesData.set(1, "22");
		seriesData.set(2, "33");
		seriesData.set(3, "44");
		for(String s : seriesData) {
			System.out.println(s);
		}
	}
}
