package com.cloudsoaring.web.course.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.common.service.BaseService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.constant.CourseDbConstants;
import com.cloudsoaring.web.course.entity.ShareEntity;

@Service
public class CaseService extends BaseService implements CourseConstants {

	@Autowired
	private CourseService courseService;

	@Autowired
	private ChapterService chapterService;

	/**
	 * 课程/章节案例列表
	 * 
	 * @param shareEntity
	 * 
	 * @return ResultBean
	 */
	public ResultBean caseList(ShareEntity shareEntity) {
		ResultBean result = new ResultBean();

		// 课程id为空
		assertNotEmpty(shareEntity.getRelationCourseId(), MSG_E_CID_NULL);
		// 课程不存在
		assertExist(shareEntity.getRelationCourseId(), SQL_IS_COURSE_EXIST,
				MSG_E_COURSE_NOT_EXIST);
		// 章节不存在
		if (StringUtil.isNotEmpty(shareEntity.getRelationCourseId())) {
			assertExist(shareEntity.getRelationChapterId(),
					SQL_IS_CHAPTER_EXIST, MSG_E_CHAPTER_NOT_EXIST);
		}
		if (StringUtil.isNotEmpty(shareEntity.getRelationCourseId())) {
			// 创建人课程
			result = commonDao.searchList4Page(shareEntity,
					CourseDbConstants.SEARCH_SHARE_LIST);
		} else if (StringUtil.isNotEmpty(shareEntity.getRelationChapterId())) {
			// 创建人章节
			result = commonDao.searchList4Page(shareEntity,
					CourseDbConstants.SEARCH_SHARE_LIST);
		} else {
			result.setStatus(false).setMessages(
					getMessage(MSG_E_COURSE_CHAPTER_NOT_EXIST));
		}
		return result;
	}

	/**
	 * 检索我的案例分享（我的案例、赞过的案例条件检索）
	 * 
	 * @param caseType
	 *            案例分类（我的案例1，赞过的案例2）
	 * @param tagId
	 *            案例标签分类id
	 * @return 我的案例分享列表
	 * @author WUXIAOXIANG
	 */
	public ResultBean userShareList(String caseType, String tagId) {

		ResultBean result = new ResultBean();
		// 判断用户是否登录
		assertNotGuest();
		// 我的课程案例分类为空
		assertNotEmpty(caseType, MSG_E_CASE_TYPE_NULL);
		HashMap<Object, Object> map = new HashMap<Object, Object>();
		result.setData(map);
		result.setStatus(true);
		return result;
	}
}
