package com.cloudsoaring.web.course.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.entity.ShareEntity;
import com.cloudsoaring.web.course.service.CaseService;

/**
 * 课程/章节案例列表
 * 
 * @author JGJ
 *
 */
@Controller
@RequestMapping("/pub/case")
public class CaseController {

	@Autowired
	private CaseService caseService;

	/**
	 * 课程/章节案例列表
	 * 
	 * @param ShareEntity
	 * @return ResultBean
	@RequestMapping("/caseList.action")
	@ResponseBody
	public ResultBean caseList(ShareEntity shareEntity) {
		return caseService.caseList(shareEntity);
	}
	 */

	/**
	 * 检索我的案例分享（我的案例、赞过的案例条件检索）
	 * 
	 * @param caseType
	 *            案例分类（我的案例1，赞过的案例2）
	 * @param tagId
	 *            案例标签分类id
	 * @return 案例列表
	 * @author WUXIAOXIANG
	@RequestMapping("/userShareList.action")
	@ResponseBody
	public ResultBean userShareList(String caseType, String tagId) {
		// 检索我的案例
		return caseService.userShareList(caseType, tagId);
	}
	 */
}
