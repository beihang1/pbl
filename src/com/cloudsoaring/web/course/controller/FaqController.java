package com.cloudsoaring.web.course.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.UserEntity;
import com.cloudsoaring.web.bus.service.UserService;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.QaAnswerEntity;
import com.cloudsoaring.web.course.entity.QaQuestionEntity;
import com.cloudsoaring.web.course.service.CourseService;
import com.cloudsoaring.web.course.service.FaqService;
import com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity;

/***
 * 课程/章节问答模块相关请求、响应控制的Controller
 * 
 * @author WUXIAOXIANG
 *
 */
@Controller
@RequestMapping("/pub/faq")
public class FaqController extends BaseController implements CourseConstants {

	@Autowired
	private FaqService faqService;

	@Autowired
	private UserService userService;
	
	@Autowired
	private CourseService courseService;

	/**
	 * 检索课程/章节问答列表
	 * 
	 * @param qaQuestionEntity
	 *            问答entity：包含linkId 链接ID；linkType 链接类型(COURSE:课程,CHAPTER:章节);
	 *            pageSize每页显示行数, pageNumber当前页
	 * @return 课程/章节问答列表
	 * @author WUXIAOXIANG
	 */
	@RequestMapping("/faqList.action")
	@ResponseBody
	public ResultBean faqList(QaQuestionEntity qaQuestionEntity) {
		return faqService.searchFagList(qaQuestionEntity);
	}

	/**
	 * 发布课程/章节问题
	 * 
	 * @param linkId
	 *            链接ID
	 * @param linkType
	 *            链接类型(COURSE:课程,CHAPTER:章节)
	 * @param title
	 *            问题标题
	 * @param content
	 *            问题内容
	 * @param sendPoint
	 *            送出积分
	 * @return 是否发布成功
	 * @author WUXIAOXIANG
	 */
	@RequestMapping("/publishQuestion.action")
	@ResponseBody
	public ResultBean publishQuestion(String linkId, String linkType,
			String title, String content, String sendPoint) {
		// 插入问答
		return faqService.insertQaQuestion(linkId, linkType, title, content,
				sendPoint);
	}

	/**
	 * 课程/章节问答-回答列表
	 * 
	 * @author JGJ
	 * @param linkId
	 *            链接ID；linkType 链接类型(COURSE:课程,CHAPTER:章节); qaQuestionId
	 * @return 返回结果
	 */
	@RequestMapping("/questionDetail.action")
	@ResponseBody
	public ResultBean questionDetail(QaQuestionEntity qaQuestionEntity) {
		return faqService.questionDetail(qaQuestionEntity);
	}

	/**
	 * 
	 * @param type 0是我的问 1是我的答
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 */
	@RequestMapping("/userFaqList.action")
	@ResponseBody
	public ResultBean userFaqList(String type,Integer pageSize,Integer pageNumber) {

		return faqService.userFaqList(type,pageSize,pageNumber);
	}

	/**
	 * 問答初始化
	 * 
	 * @param linkId
	 *         链接ID linkType 链接类型
	 * @return
	 */
	@RequestMapping("/faqInit.action")
	public ModelAndView faqCourseInit(String linkId, String linkType) {
		ModelAndView mv = new ModelAndView(
				"/course/client/course_chapter_question");
		mv.addObject("linkType", linkType);
		mv.addObject("linkId", linkId);
		return mv;
	}

	/**
	 * 問答詳情初始化
	 * 
	 * @param
	 * @return
	 */
	@RequestMapping("/faqCourseChapterList.action")
	public ModelAndView faqCourseChapterList(String linkId, String linkType,
			String qaQuestionId, String sendPoint) {
		ModelAndView mv = new ModelAndView("/course/client/question_details");
		faqService.updateViewNum(qaQuestionId);
		mv.addObject("linkId", linkId);
		mv.addObject("linkType", linkType);
		mv.addObject("qaQuestionId", qaQuestionId);
		mv.addObject("sendPoint", sendPoint);
		int num = courseService.getStuStatus();
		mv.addObject("num", num);
		UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}

	/**
	 * 问答详情头
	 * 
	 * @param linkId
	 * @param linkType
	 * @param qaQuestionId
	 * @return
	 */
	@RequestMapping("/faqCourseChapterhead.action")
	@ResponseBody
	public ResultBean faqCourseChapterhead(String linkId, String linkType,
			String qaQuestionId) {
		faqService.updateViewNum(qaQuestionId);
		ResultBean result = new ResultBean();
		QaQuestionEntity qaQuestionEntity = faqService.searchOneQaQuestion(
				linkId, linkType, qaQuestionId);
		result.setData(qaQuestionEntity);
		return result;
	}

	/**
	 * 登录用户
	 * 
	 * @param
	 * @param
	 * @param
	 * @return
	 */
	@RequestMapping("/searchUser.action")
	@ResponseBody
	public ResultBean searchUser() {
		ResultBean result = new ResultBean();
		UserEntity user = userService.searchById(WebContext.getSessionUserId());
		result.setData(user);
		return result;
	}

	/**
	 * 插入回答
	 * 
	 * @retrun 返回结果
	 */
	@RequestMapping("/publishAnswer.action")
	@ResponseBody
	public ResultBean publishAnswer(QaAnswerEntity qaAnswerEntity) {
		return faqService.publishAnswer(qaAnswerEntity);
	}

	/**
	 * 采纳回答
	 * 
	 * @param
	 * @param
	 * @param
	 * @return
	 */
	@RequestMapping("/adoptQuestion.action")
	@ResponseBody
	public ResultBean adoptQuestion(String linkId, String linkType,
			String qaQuestionId, String sendPoint, String id, String userId) {

		return faqService.adoptQuestion(linkId, linkType, qaQuestionId,
				sendPoint, id, userId);
	}
}
