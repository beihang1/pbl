package com.cloudsoaring.web.course.controller;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.common.utils.DateUtil;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.UserEntity;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.utils.ConfigUtil;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.ChapterEntity;
import com.cloudsoaring.web.course.entity.CourseEntity;
import com.cloudsoaring.web.course.entity.HomeworkAnswerEntity;
import com.cloudsoaring.web.course.entity.ManagerUserEntity;
import com.cloudsoaring.web.course.entity.MeasurementEntity;
import com.cloudsoaring.web.course.entity.NoteDataEntity;
import com.cloudsoaring.web.course.entity.NoteEntity;
import com.cloudsoaring.web.course.entity.QuestionEntity;
import com.cloudsoaring.web.course.entity.UserMeasurementResultEntity;
import com.cloudsoaring.web.course.service.ChapterService;
import com.cloudsoaring.web.course.service.CourseService;
import com.cloudsoaring.web.course.service.CourseVRService;
import com.cloudsoaring.web.course.service.ManagerUserService;
import com.cloudsoaring.web.course.service.MeasuermentService;
import com.cloudsoaring.web.course.view.ChapterView;
import com.cloudsoaring.web.course.view.CourseView;
import com.cloudsoaring.web.course.view.HomeworkView;
import com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity;

/**
 * 章节管理
 * 
 * @author lijuan
 * @date 2016-03-01
 */
@Controller
@RequestMapping("/pub/chapter")
public class ChapterController extends BaseController implements
		CourseConstants {
	@Autowired
	private ChapterService chapterService;
	@Autowired
	private CourseService courseService;
	@Autowired
	private MeasuermentService measuermentService;
	@Autowired
	private CourseVRService courseVRService;
	@Autowired
	private ManagerUserService managerUserService;
/////////////////前端请求控制////////////////////
	
	@SuppressWarnings("unchecked")
	/**
	* 前端请求-章节详情
	* @param chapterId 章节ID
	* @return 
	*/
	@RequestMapping("/toChapterDetail.action")
	public ModelAndView toChapterDetail(String courseId,String chapterId){
		//章节页面路径
		ModelAndView mv = new ModelAndView("course/client/chapter_detail");
		//查询课程下的章节内容
		ResultBean result = courseService.searchCourseDetail(courseId);
		CourseView courseView = (CourseView) result.getData();
		mv.addObject("courseView", courseView);
		mv.addObject("note", new NoteDataEntity());
		mv.addObject("homeWork", new HomeworkView());
		//查询某一个章节下的内容
		result = chapterService.chapterDetail(courseId, chapterId);
		ChapterView chapterView = (ChapterView) result.getData();
		String lingkId = chapterId;
		HomeworkView view = courseService.searchOneAnswer(chapterId);
		//返回作业对象
		if(null!=view && StringUtil.isNotBlank(view.getStudentComment())) {
			mv.addObject("zuoye", view.getStudentComment());
		}else {
			mv.addObject("zuoye", "");
		}
		
		//查询提交作业的内容
		String linkId= chapterView.getId();
		String linkType= "CHAPTER";
		HomeworkView homeworkView =new HomeworkView();
		homeworkView.setLinkId(linkId);
		homeworkView.setLinkType(linkType);
		homeworkView = courseVRService.selectHomework(homeworkView);
		if(null!=homeworkView) {
			//作业不为空
			String homeworkId = homeworkView.getId();
			String userId=WebContext.getSessionUserId();
			//查询作业答题内容
			HomeworkAnswerEntity entity = new HomeworkAnswerEntity();
			entity.setHomeworkId(homeworkId);
			entity.setCreateUser(userId);
			entity = courseVRService.selectOneAnswer(entity);
			if(null!=entity) {
				chapterView.setStudentComment(entity.getStudentComment());
			}
		}
		ChapterEntity entity = new ChapterEntity();
		entity.setCourseId(courseId);
		List<ChapterEntity> chapterCourseList = courseService.selectByCourseId(entity);
		mv.addObject("chapterCourseList", chapterCourseList);
		mv.addObject("chapterView", chapterView);
		//检索章节列表
		List<ChapterView> cvs = (List<ChapterView>) chapterService.chapterList(courseId);
		mv.addObject("chapterList", cvs);
		//相关课程
		result = courseService.recommendedCourses("5", courseId);
		List<CourseEntity> otherCourseList = (List<CourseEntity>) result.getData();
		mv.addObject("otherCourseList", otherCourseList);
		//判断是否需要检索测试信息
		if(CHAPTER_TYPE_MEASUERMENT.equals(chapterView.getMeasureType().toString())){
			result = measuermentService.searchMeasuerment(chapterId, LINK_TYPE_CHAPTER);
			List<QuestionEntity> questionList = (List<QuestionEntity>) result.getData();
			mv.addObject("questionList", questionList);
		}
		//判断是否考试章节
		if(CHAPTER_TYPE_EXAM.equals(chapterView.getMeasureType())){
			//考试章节
			//取用户对于章节的考试信息
			MeasurementEntity me = new MeasurementEntity();
			me.setLinkId(chapterId);
			me.setLinkType(LINK_TYPE_CHAPTER);
			me = measuermentService.findMeasurement(me);
			if(me!=null){
				UserMeasurementResultEntity um = new UserMeasurementResultEntity();
				um.setMeasurementId(me.getId());
				um.setUserId(WebContext.getSessionUserId());
				um = measuermentService.findUserExamResult(um);
				if(um != null && um.getExamStatus()!=null && um.getExamStatus().equals(EXAM_STATUS_END)){
					//已参加过考试提交了
					mv.addObject("goal", um.getGoal()+"");
					mv.addObject("totalGoal", um.getTotalGoal()+"");
					mv.addObject("examTime", DateUtil.formatAny(um.getCreateDate(), ConfigUtil.getDateTimeFormatDefualt()));
				}
			}
		}
		//是否教师
		mv.addObject("isTeacher", courseService.isTeacher(courseView.getTeacherId()));
		mv.addObject("useingChapterId", chapterId);
		String content = chapterView.getContent();
		if(content != null && !"".equals(content)) {
			String ipAndPort = getIpAndPort(content);
			if(!"".equals(ipAndPort)) {
				String serverName = WebContext.getRequest().getServerName();
				int serverPort = WebContext.getRequest().getServerPort();
				content = content.replace(ipAndPort, serverName+":"+serverPort);
				chapterView.setContent(content);
			}
		}
		//获取用户角色
		mv = getRoleAndNum(mv);
		//返回结果
		return mv;
	}


/////////////////前端请求控制END/////////////////	
	
	/**
	 * 
	 * 接口——章节大纲：大章节
	 * 
	 * @param courseId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/chapterOutlineList.action")
	@ResponseBody
	public ResultBean chapterOutlineList(String courseId) throws Exception {
		ResultBean result = new ResultBean();
		result = chapterService.searchChapterOutlineList(courseId);
		//返回结果
		return result;
	}

	/**
	 * 
	 * 接口——章节列表
	 * 
	 * @param courseId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/chapterList.action")
	@ResponseBody
	public ResultBean chapterList(String courseId) throws Exception {
		ResultBean result = ResultBean.success();
		result.setData(chapterService.chapterList(courseId));
		//返回结果
		return result;
	}
	/**
	 * 
	 * 接口——章节列表
	 * 
	 * @param courseId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/chapterListTree.action")
	@ResponseBody
	public ResultBean chapterListTree(String courseId) throws Exception {
		ResultBean result = ResultBean.success();
		result.setData(chapterService.chapterList2Tree(courseId));
		//返回结果
		return result;
	}
	/**
	 * 
	 * 接口——章节列表
	 * 
	 * @param courseId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/chapterList2Tree.action")
	@ResponseBody
	public ResultBean chapterList2Tree(String courseId) throws Exception {
		ResultBean result = ResultBean.success();
		//返回结果
		result.setData(chapterService.chapterList2Tree(courseId));
		return result;
	}

	/**
	 * 
	 * 接口——章节详情
	 * 
	 * @param courseId
	 *            id
	 * @return ResultBean
	 * @throws Exception
	 */
	@RequestMapping("/chapterDetail.action")
	@ResponseBody
	public ResultBean chapterDetail(String courseId, String id) {
		//返回结果
		return chapterService.chapterDetail(courseId, id);
	}
	
	/***
	 * 更新用户章节学习进度
	 * @param chapterId 章节ID
	 * @param progress 学习进度
	 * @return
	 */
	@RequestMapping("/updateChapterStudy.action")
	@ResponseBody
	public ResultBean updateChapterStudy(String chapterId,String progress){
		//返回结果
		return chapterService.updateChapterStudy(chapterId, progress);
	}
	
	/**
	 * 更新用户上一次学习章节的时间点
	 * @param chapterId 章节ID
	 * @param time 时间点
	 * @return
	 */
	@RequestMapping("/updateChapterLastVideoTime.action")
	@ResponseBody
	public ResultBean updateChapterLastVideoTime(String chapterId,String time){
		//返回结果
		return chapterService.updateChapterLastVideoTime(chapterId, time);
	}
	
	/**
	 * 更新用户学习章节耗费的时间（秒）
	 * @param chapterId 章节ID
	 * @param time 耗费时间
	 * @return
	 */
	@RequestMapping("/updateChapterTimeCost.action")
	@ResponseBody
	public ResultBean updateChapterTimeCost(String chapterId,String time){
		//返回结果
		return chapterService.updateChapterCostTime(chapterId, time);
	}
	

	/**
	 * 教师上传视频文件
	 * @param inputName html file标签name属性值
	 * @param linkId 链接ID
	 * @param linkType 链接类型
	 * @return
	 */
	@RequestMapping("/uploadVideo.action")
	@ResponseBody
	public ResultBean uploadVideo(String inputName,String linkId,String linkType){
		//返回结果
		return chapterService.uploadVideo(inputName, linkId, linkType);
	}
	
	/**
	 * 保利威视视频上传成功后更新数据库本章节视频信息
	 * @param linkId 链接ID
	 * @param linkType 链接类型
	 * @param vid 保利威视视频VID
	 * @param duration 视频时长
	 * @return
	 */
	@RequestMapping("/updatePolyvInfo.action")
	@ResponseBody
	public ResultBean updateVideoInfo(String linkId,String linkType,String vid,String duration){
		//返回结果
		return chapterService.updateVideoInfo(linkId, linkType, vid, duration);
	}
	
	/**
	 * 判断str中是否有IP和port，若有则取出返回
	 * @param str
	 * @return String
	 */
	private static String getIpAndPort(String str) {
		String returnStr = "";
		Pattern p = Pattern.compile("(\\d+\\.\\d+\\.\\d+\\.\\d+)\\:(\\d+)");
		Matcher m = p.matcher(str);
		if(m.find()) {
			returnStr += m.group(1)+":"+m.group(2);
		}
		//返回结果
		return returnStr;
	}
	/**
	 * 查询我的考试
	 * @return
	 */
	@RequestMapping("/myExam.action")
	@ResponseBody
	public ResultBean myExam(){
		//返回结果
		return chapterService.myxam();
	}
	/**
	 * 查询全部考试
	 * @param finishType
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 */
	@RequestMapping("/myAllExam.action")
	public ModelAndView myAllExam(ChapterEntity chapter, Integer pageSize, Integer pageNumber) {
		ModelAndView mv = new ModelAndView("baseinstitution/manager/my_exam_list");
		ResultBean result = chapterService.myAllxam(chapter, pageSize, pageNumber);
		mv.addObject("chapter", result.getData());
		//返回结果
		return mv;
	}
	/**
	 *检索考试
	 * @param chapter
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 */
	@RequestMapping("/myAllExamList.action")
	@ResponseBody
	public ResultBean myAllExamList(ChapterEntity chapter, Integer pageSize, Integer pageNumber) {
		ResultBean result = chapterService.myAllxam(chapter, pageSize, pageNumber);
		return result;
	}
	private ModelAndView getRoleAndNum(ModelAndView result) {
    	int num = courseService.getStuStatus();
    	result.addObject("num", num);
    	UserEntity user = WebContext.getSessionUser();
    	//判断是否管理员
    	ManagerUserEntity entity = managerUserService.searchByUserId(user.getUserId());
    	if(!user.getUserName().equals("guest")) {
    		List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
    		//老师和教育局角色
    		if(roleList.size() > 0 && "A".equals(user.getUserKbn()) && null==entity && !"2542668".equals(user.getUserId())) {
    			for(UserRoleEntity role : roleList) {
    				if(role.getRoleId().equals("01")) {
    					result.addObject("role", "t");
    				}
    			}
    		//教育局角色	
    		}else if(roleList.size() > 0  && (null!=entity || "2542668".equals(user.getUserId()))){
    			result.addObject("role", "m");
    		}else if("B".equals(user.getUserKbn())){
    			result.addObject("role", "e");
    	    //家长和教研员角色
    		}else if("D".equals(user.getUserKbn()) || "E".equals(user.getUserKbn())){
    			result.addObject("role", "q");
    		//学生角色
    		}else {
    			result.addObject("role", "s");
    		}
    	}

    	if(null!=entity || "2542668".equals(user.getUserId())) {
    		result.addObject("manager", "1");
    	}else {
    		result.addObject("manager", "0");
    	}
    	return result;
    }
}
