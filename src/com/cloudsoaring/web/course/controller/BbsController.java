package com.cloudsoaring.web.course.controller;


import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.UserEntity;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.BbsEntity;
import com.cloudsoaring.web.course.entity.BbsReplyEntity;
import com.cloudsoaring.web.course.entity.ManagerUserEntity;
import com.cloudsoaring.web.course.entity.NoteDataEntity;
import com.cloudsoaring.web.course.service.BbsService;
import com.cloudsoaring.web.course.service.CourseService;
import com.cloudsoaring.web.course.service.ManagerUserService;
import com.cloudsoaring.web.course.view.TagView;
import com.cloudsoaring.web.trainingplatform.constants.TpConstants;
import com.cloudsoaring.web.trainingplatform.entity.ActivityCenterEntity;
import com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * BBS论坛Controller
 */
@Controller
@RequestMapping("/pub/bbs")
@ControllerAdvice
public class BbsController extends BaseController implements CourseConstants,TpConstants {
	
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Autowired
	BbsService bbsService;
	@Autowired
	private CourseService courseService;
	@Autowired
	private ManagerUserService managerUserService;
	
	/**
	 *新增页面
	 * @param view
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping("/add.action")
	public ModelAndView add(){
		ModelAndView mv = new ModelAndView("baseinstitution/manager/bbs_add");
		mv = getRoleAndNum(mv);
		return mv;
	}
	
	/**
	 * 新增BBS
	 * @param entity
	 * @return 返回结果
	 */
	@RequestMapping("/insert.action")
	@ResponseBody
	public ResultBean insert(BbsEntity entity) throws Exception {
		entity.setId(IDGenerator.genUID());
		if(StringUtil.isEmpty(entity.getContent())){
			return ResultBean.error(CONTENT_CANNOT_EMPTY);
		}
		bbsService.saveBbs(entity);
		return ResultBean.success(MSG_ADD_NOTICE_SUCCESS);
	}
	
	/**
	 * 检索列表
	 * @return
	 */
	@RequestMapping("/bbsList.action")
	@ResponseBody
	public ResultBean bbsList(BbsEntity bbs){
		
		return bbsService.bbsList(bbs);
	}
	
	@RequestMapping("/bbsReplyList.action")
	@ResponseBody
	public ResultBean bbsReplyList(BbsReplyEntity bbsReply){
		
		return bbsService.bbsReplyList(bbsReply);
	}
	
	
	/**
	 * 新增BBS_REPLY
	 * @param entity
	 * @return 返回结果
	 */
	@RequestMapping("/insertBbsReply.action")
	@ResponseBody
	public ResultBean insertBbsReply(BbsReplyEntity entity) throws Exception {
		entity.setId(IDGenerator.genUID());
		if(StringUtil.isEmpty(entity.getContent())){
			return ResultBean.error(CONTENT_CANNOT_EMPTY);
		}
		bbsService.saveBbsReply(entity);
		return ResultBean.success(MSG_ADD_NOTICE_SUCCESS);
	}
	
	/**
	 *详情页面
	 * @param view
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping("/bbsDetail.action")
	public ModelAndView bbsDetail(String id){
		ModelAndView mv = new ModelAndView("baseinstitution/manager/bbs_view");
		BbsEntity bbs = bbsService.getBbsByPk(id);
		if(bbs != null) {
			bbs.setCreateTimeStr(sdf.format(bbs.getCreateTime()));
		}
		String userId = WebContext.getSessionUserId();
		if(userId.equals(bbs.getCreateUser())) {
			mv.addObject("isDelete", true);
		}
		mv.addObject("bbs", bbs);
		mv = getRoleAndNum(mv);
		return mv;
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/bbsDelete.action")
	@ResponseBody
	public ResultBean bbsDelete(String id) throws Exception {
		return bbsService.deleteById(id);
	}
	
	/**
	 * 删除Reply
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/bbsReplyDelete.action")
	@ResponseBody
	public ResultBean bbsReplyDelete(String id) throws Exception {
		return bbsService.deleteReplyById(id);
	}
	
	/**
	 *编辑页面
	 * @param view
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping("/edit.action")
	public ModelAndView edit(String id){
		ModelAndView mv = new ModelAndView("baseinstitution/manager/bbs_edit");
		BbsEntity bbs = bbsService.getBbsByPk(id);
		if(bbs != null) {
			bbs.setCreateTimeStr(sdf.format(bbs.getCreateTime()));
		}
		mv.addObject("bbs", bbs);
		mv = getRoleAndNum(mv);
		return mv;
	}
	
	/**
	 * update
	 * @param entity
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/update.action")
	@ResponseBody
	public ResultBean update(BbsEntity entity) throws Exception {
		if(StringUtil.isEmpty(entity.getContent())){
			return ResultBean.error(CONTENT_CANNOT_EMPTY);
		}
		bbsService.updateBbs(entity);
		return ResultBean.success(MSG_ADD_NOTICE_SUCCESS);
	}
	
	/**
	 * 
	 * @param result
	 * @return
	 */
	private ModelAndView getRoleAndNum(ModelAndView result) {
		int num = courseService.getStuStatus();
    	result.addObject("num", num);
    	UserEntity user = WebContext.getSessionUser();
    	//判断是否管理员
    	ManagerUserEntity entity = managerUserService.searchByUserId(user.getUserId());
    	if(!user.getUserName().equals("guest")) {
    		List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
    		//老师和教育局角色
    		if(roleList.size() > 0 && "A".equals(user.getUserKbn()) && null==entity && !"2527943".equals(user.getUserId())) {
    			for(UserRoleEntity role : roleList) {
    				if(role.getRoleId().equals("01")) {
    					result.addObject("role", "t");
    				}
    			}
    		//教育局角色	
    		}else if(roleList.size() > 0  && (null!=entity || "2527943".equals(user.getUserId()))){
    			result.addObject("role", "m");
    		}else if("B".equals(user.getUserKbn())){
    			result.addObject("role", "e");
    	    //家长和教研员角色
    		}else if("D".equals(user.getUserKbn()) || "E".equals(user.getUserKbn())){
    			result.addObject("role", "q");
    		//学生角色
    		}else {
    			result.addObject("role", "s");
    		}
    	}

    	if(null!=entity || "2527943".equals(user.getUserId())) {
    		result.addObject("manager", "1");
    	}else {
    		result.addObject("manager", "0");
    	}
    	return result;
    }
		

}
