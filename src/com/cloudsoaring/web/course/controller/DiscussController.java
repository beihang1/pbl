package com.cloudsoaring.web.course.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.service.DiscussService;
import com.cloudsoaring.web.course.view.DiscussView;

/**
 * 课程/章节评论列表
 * 
 * @author JGJ qq
 */
@Controller
@RequestMapping("/pub/discuss")
public class DiscussController {

	@Autowired
	private DiscussService discussService;

	/**
	 * 检索课程/章节评论列表
	 * 
	 * @param discuss
	 *            评论实体类：linkId链接ID, linkType链接类型(COURSE课程CHAPTER章节),
	 *            pageSize每页显示行数, pageNumber当前页
	 * @return 课程/章节评论列表
	 * @author WUXIAOXIANG
	 */
	@RequestMapping("/discussList.action")
	@ResponseBody
	public ResultBean discussList(DiscussView discuss) {
		return discussService.searchDiscuss4Page(discuss);
	}

	/**
	 * 发表课程/章节评论
	 * 
	 * @param
	 * @return ResultBean
	 */
	@RequestMapping("/publishDiscuss.action")
	@ResponseBody
	public ResultBean publishDiscuss(DiscussView discuss) {

		return discussService.publishDiscuss(discuss);
	}

	/**
	 * 检索我的评论
	 * 
	 * @param discuss 评论entity
	 *            
	 * @return 我的评论列表
	 * @author WUXIAOXIANG
	 */
	@RequestMapping("/userDiscussList.action")
	@ResponseBody
	public ResultBean userDiscussList(DiscussView discuss) {
		// 检索我的评论
		return discussService.userDiscussList(discuss);
	}

	/**
	 * 删除评论
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping("/deleteDiscuss.action")
	@ResponseBody
	public ResultBean deleteDiscuss(String id) {
		return discussService.deleteDiscuss(id);
	}
	
	/***
	 * 评论点赞
	 * @param id
	 * @return
	 */
	@RequestMapping("/likeDiscuss.action")
	@ResponseBody
	public ResultBean likeDiscuss(String id){
		return discussService.likeDiscuss(id);
	}
}
