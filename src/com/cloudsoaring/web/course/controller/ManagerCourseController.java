package com.cloudsoaring.web.course.controller;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.common.excel.ExcelDataBind;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.exception.BusinessException;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.UserEntity;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.utils.ConfigUtil;
import com.cloudsoaring.web.common.utils.JSONUtil;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.CourseAssistTeacherEntity;
import com.cloudsoaring.web.course.entity.CourseEntity;
import com.cloudsoaring.web.course.entity.CourseTeamEntity;
import com.cloudsoaring.web.course.entity.CourseTeamUserEntity;
import com.cloudsoaring.web.course.entity.CourseUserEvalutionEntity;
import com.cloudsoaring.web.course.entity.CourseUserEvalutionSaveEntity;
import com.cloudsoaring.web.course.entity.ManagerUserEntity;
import com.cloudsoaring.web.course.entity.MeasurementEntity;
import com.cloudsoaring.web.course.entity.QuestionEntity;
import com.cloudsoaring.web.course.entity.UserCourseEntity;
import com.cloudsoaring.web.course.entity.UserMeasurementResultEntity;
import com.cloudsoaring.web.course.service.CourseService;
import com.cloudsoaring.web.course.service.CourseUserEvalutionService;
import com.cloudsoaring.web.course.service.ManagerUserService;
import com.cloudsoaring.web.course.service.MeasuermentService;
import com.cloudsoaring.web.course.view.CourseManagerView;
import com.cloudsoaring.web.course.view.HomeworkView;
import com.cloudsoaring.web.course.view.TagView;
import com.cloudsoaring.web.course.view.TeacherView;
import com.cloudsoaring.web.course.view.UserView;
import com.cloudsoaring.web.myinfo.entity.StudentClassInfoEntity;
import com.cloudsoaring.web.myinfo.service.StudentClassInfoService;
import com.cloudsoaring.web.trainingplatform.entity.TpUserEntity;
import com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity;
/**
 * 后台——课程管理
 * @author lijuan
 */
@Controller
@RequestMapping("/manager/course")
public class ManagerCourseController extends BaseController implements CourseConstants{
    /** 注入CourseService */
    @Autowired
    private CourseService courseService;
    @Autowired
    private  MeasuermentService measuermentService;
    @Autowired
    private  StudentClassInfoService studentClassInfoService;
    @Autowired
	private ManagerUserService managerUserService;
    @Autowired
	private CourseUserEvalutionService courseUserEvalutionService;
    /**
     * 
                 * 检索课程列表
     * @param queryInfo 课程对象
     * @return 返回课程列表
     */
    @RequestMapping("/listSearch.action")
    @ResponseBody
    public ResultBean listSearch(CourseManagerView queryInfo) {
        return courseService.listSearch(queryInfo);
    }
    /**
     * 
     * 初始化课程列表
     * @param condition 课程对象
     * @return 返回课程列表初始化页面
     */
    @RequestMapping("/listInit.action")
    public ModelAndView listInit(CourseManagerView condition){
        ModelAndView result = new ModelAndView("course/manager/course_list");
        condition.setPageSize(10);
        result.addObject("condition", condition);
        TagView tagQuery = new TagView();
        List<TagView> tags = courseService.searchTagList2(tagQuery);
        List<TagView> tagsSecond = courseService.searchTagSecondList2(tagQuery);
        result.addObject("catagoryTags", tags);
        result.addObject("course", new CourseManagerView());
        result.addObject("tagsSecond", tagsSecond);
        List<StudentClassInfoEntity> gradeList = new ArrayList<StudentClassInfoEntity>();
        result.addObject("gradeList", gradeList);
        result = getRoleAndNum(result);
        return result;
    }
    /**
     * 
     * 初始化课程列表  - 老师首页
     * @param condition
     * @return
     */
    @RequestMapping("/tlistInit.action")
    public ModelAndView teachListInit(CourseManagerView condition){
    	ModelAndView result = listInit(condition);
    	 TagView tagQuery = new TagView();
         List<TagView> tags = courseService.searchTagList(tagQuery);
         List<TagView> tagsSecond = courseService.searchTagSecondList(tagQuery);
         result.addObject("catagoryTags", tags);
         result.addObject("course", new CourseManagerView());
         result.addObject("tagsSecond", tagsSecond);
    	result.setViewName("trainingplatform/client/teacher/teacher_course_list");
    	int num = courseService.getStuStatus();
    	result.addObject("num", num);
    	UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						result.addObject("role", "t");
					}
				}
			}else {
				result.addObject("role", "s");
			}
		}
    	return result;
    }
    /**
     * 推荐/取消精品课程课程信息
     * @param id 课程ID
     * @param flagBoutique 0 推荐， 1 取消推荐
     * @throws Exception 
     */
    @RequestMapping("/pushHome.action")
    @ResponseBody
    public ResultBean pushHome(String id, String flagBoutique) throws Exception {
        courseService.pushHome(id, flagBoutique);
        return ResultBean.success(MSG_S_SUBMIT);
    } 
    /**
     * 推荐/取消首页轮播
     * @param id 课程ID
     * @param flagHomeCarousel 0 推荐， 1 取消推荐
     * @throws Exception 
     */
    @RequestMapping("/pushHomeBanner.action")
    @ResponseBody
    public ResultBean pushHomeBanner(String id, String flagHomeCarousel) throws Exception {
        courseService.pushHomeBanner(id, flagHomeCarousel);
        return ResultBean.success(MSG_S_SUBMIT);
    }
    /**
     * 删除课程
     * @param id 课程ID
     * @return 
     * @throws Exception
     */
    @RequestMapping("/delCourse.action")
    @ResponseBody
    public ResultBean delCourse(String id) throws Exception {
        courseService.delCourseById(id);
        return ResultBean.success(MSG_S_SUBMIT);
    }
    /**
     * 
     * 课程发布/取消发布
     * @param id 课程id
     * @param status 课程状态：0 未发布 1 已发布
     * @return 
     * @throws Exception
     */
    @RequestMapping("/publicCourse.action")
    @ResponseBody
    public ResultBean publicCourse(String id,String status) throws Exception {
        courseService.publicCourse(id, status);
        return ResultBean.success(MSG_S_SUBMIT);
    }
   
    /**
     * 编辑页面初始化
     * @param id 课程ID
     * @return
     */
    @RequestMapping("/editInit.action")
    public ModelAndView editInit(String id) {
        ModelAndView result = new ModelAndView("course/manager/course_add");
        String type="edit";
        CourseManagerView course = courseService.searchCourse4EditById(id,type);
        List<CourseAssistTeacherEntity> teacherList = courseService.getCourseAssistTeacherList(id);
        List<String> userIdList = new ArrayList<String>();
        if(teacherList.size() > 0) {
        	for(CourseAssistTeacherEntity assist : teacherList) {
        		userIdList.add(assist.getTeacherId());
        	}
        	course.setUserIdList(userIdList);
        }
        TagView tagQuery = new TagView();
        List<TagView> tags = courseService.searchTagList2(tagQuery);
        List<TagView> tagsSecond = courseService.searchTagSecondList2(tagQuery);
        result.addObject("catagoryTags", tags);
        result.addObject("tagsSecond", tagsSecond);
        result.addObject("course", course);
        result.addObject("pageType",type);
        result = getRoleAndNum(result);
        return result;
    }
    /**
     * 编辑页面初始化
     * @param id 课程ID
     * @return
     */
    @RequestMapping("/teditInit.action")
    public ModelAndView teditInit(String id) {
    	ModelAndView result = editInit(id);
    	result.setViewName("trainingplatform/client/teacher/teacher_course_add");
    	return result;
    }
   /** 
    * 课程新增页面初始化
    * @return
    */
    @RequestMapping("/addInit.action")
    public ModelAndView addInit() {
        ModelAndView result = new ModelAndView("course/manager/course_add");
        TagView tagQuery = new TagView();
        List<TagView> tags = courseService.searchTagList2(tagQuery);
        List<TagView> tagsSecond = courseService.searchTagSecondList2(tagQuery);
        result.addObject("catagoryTags", tags);
        result.addObject("pageType", "add");
        result.addObject("course", new CourseManagerView());
        result.addObject("tagsSecond", tagsSecond);
        result = getRoleAndNum(result);
        return result;
    }
    /** 
     * 课程新增页面初始化 - 老师首页
     * @return
     */
    @RequestMapping("/taddInit.action")
    public ModelAndView taddInit() {
    	ModelAndView result = addInit();
    	result.setViewName("trainingplatform/client/teacher/teacher_course_add");
    	return result;
    }
    /**
     * 查询所有教师
     * @return 教师列表
     */
    @RequestMapping("/searchTeacherAll.action")
    @ResponseBody
    public ResultBean getTeacherList(TeacherView query){
      return  courseService.getTeacherAll(query);
    }
    /**
     * 二级标签查询
     * @param id 课程ID
     * @return 课程下的二级标签列表
     */
    @RequestMapping("/searchSecondTag.action")
    @ResponseBody
    public  List<TagView> searchSecondTag(String id){
        return courseService.searchSecondTagList(id);
    }
    
    /**
     * 二级标签查询
     * @param id 课程ID
     * @return 课程下的二级标签列表
     */
    @RequestMapping("/searchSecondTag2.action")
    @ResponseBody
    public  List<TagView> searchSecondTag2(String id){
        return courseService.searchSecondTagList2(id);
    }
    /**
     * 
     * 根据教师ID，检索教师信息
     * @return
     */
    @RequestMapping("/searchTeacher.action")
    @ResponseBody
    public ResultBean searchTeacher(String teacherId) {
        ResultBean result=ResultBean.success();
        result.setStatus(true);
        return courseService.searchTeacher(teacherId);
    }
    /**
     * 保存课程信息
     * @param courseJson 课程信息
     * @return 返回结果
     * @throws Exception 
     */
    @RequestMapping("/saveInfo.action")
    @ResponseBody
    public ResultBean saveInfo(String courseJson) throws Exception {
        CourseManagerView course = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, CourseManagerView.class);
        ResultBean result = ResultBean.success();
        if(StringUtil.isEmpty(course.getId())) {
            courseService.insertCourseCatagory(course);
        } else {
            courseService.updateCourseAll(course);
        }
        return result;
    }
    /**
     *删除课程信息
     * @param id
     * @return
     */
    @RequestMapping("/deleteCourse.action")
    @ResponseBody
    public ResultBean deleteCourse(String id){
        ResultBean result = new ResultBean();
        result =  courseService.deleteCourse(id);
        //result.setMessages(getMessage(MSG_S_DELETE));
        return result;
    }
    /**
     * 课程查看
     * @param id
     * @return
     */
    @RequestMapping("/viewCourse.action")
    @ResponseBody
    public ModelAndView viewCourse(String id){
        ModelAndView result = new ModelAndView("course/manager/course_view");
        String type="view";
        CourseManagerView course = courseService.searchCourse4EditById(id,type);
        result.addObject("course", course);
        result = getRoleAndNum(result);
        return result;
    }
    /**
     * 课程查看 - 老师首页
     * @param id
     * @return
     */
    @RequestMapping("/tviewCourse.action")
    @ResponseBody
    public ModelAndView tviewCourse(String id){
    	ModelAndView result = viewCourse(id);
    	result.setViewName("trainingplatform/client/teacher/teacher_course_view");
    	return result;
    }

    /**
     * 
     * 初始化添加作业
     * @param linkId 链接ID linkType链接类型
     * @return
     */
    @RequestMapping("/taskInit.action")
    public ModelAndView taskInit(HomeworkView home){
        ModelAndView result = new ModelAndView("course/manager/home_work");
        HomeworkView homework=courseService.searchCoChHomeWork(home);
        result.addObject("homework", homework);
        result = getRoleAndNum(result);
        return result;
    }
    /**
     * 
     * 初始化添加作业
     * @param linkId 链接ID linkType链接类型
     * @return
     */
    @RequestMapping("/ttaskInit.action")
    public ModelAndView ttaskInit(HomeworkView home){
    	ModelAndView result = taskInit(home);
    	result.setViewName("trainingplatform/client/teacher/teacher_home_work");
    	int num = courseService.getStuStatus();
    	result.addObject("num", num);
    	UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						result.addObject("role", "t");
					}
				}
			}else {
				result.addObject("role", "s");
			}
		}
    	return result;
    }
    
    /**
     * 保存作业信息
     * @param 
     * @return 返回结果
     * @throws Exception 
     */
    @RequestMapping("/saveTask.action")
    @ResponseBody
    public ResultBean saveTask(HomeworkView home)  {
        return  courseService.saveTask(home);
       
    }
    
    @RequestMapping("/saveShare.action")
    @ResponseBody
    public ResultBean saveShare(CourseManagerView courseShare)  {
        return  courseService.saveShare(courseShare);
       
    }
    
    @RequestMapping("/searchTeam.action")
    @ResponseBody
    public ResultBean searchTeam(CourseTeamEntity query)  {
        return courseService.searchTeam(query);
    }
    
    @RequestMapping("/searchTeamInit.action")
    @ResponseBody
    public ModelAndView searchTeamInit(CourseTeamEntity condition,String id){
        ModelAndView result = new ModelAndView("course/manager/course_team");
        condition.setPageSize(10);
        result.addObject("condition", condition);
        result.addObject("id", id);
        result = getRoleAndNum(result);
        return result;
    }
    
    @RequestMapping("/addTeam.action")
    @ResponseBody
    public ModelAndView addTeam(String id){
        ModelAndView result = new ModelAndView("course/manager/course_team_add");
        List<UserView> courseUserList = courseService.getCourseUserList(id);
        result.addObject("id", id);
        result.addObject("courseUserList", courseUserList);
        result.addObject("team", new CourseTeamEntity());
        result = getRoleAndNum(result);
        return result;
    }
   
    @RequestMapping("/teamEdit.action")
   	public ModelAndView teamEdit(@RequestParam("id")String id){
   		ModelAndView result = new ModelAndView("course/manager/course_team_edit");
   		CourseTeamEntity team = courseService.getCourseTeamById(id);
	   	 List<UserView> courseUserList = courseService.getCourseUserList(team.getCourseId());
	     result.addObject("id", team.getCourseId());
	     result.addObject("courseUserList", courseUserList);
	     result.addObject("team", team);
	     result = getRoleAndNum(result);
   		return result;
   	}
    
    
    @RequestMapping("/teamUserMgr.action")
   	public ModelAndView teamUserMgr(@RequestParam("id")String id){
   		ModelAndView result = new ModelAndView("course/manager/course_team_user_list");
   		CourseTeamEntity team = courseService.getCourseTeamById(id);
	   	 List<UserView> courseUserList = courseService.getCourseUserList(team.getCourseId());
	     result.addObject("courseUserList", courseUserList);
	     result.addObject("team", team);
	     result.addObject("id", team.getCourseId());
	     result = getRoleAndNum(result);
   		return result;
   	}
    
    @RequestMapping("/searchTeamUser.action")
    @ResponseBody
    public ResultBean searchTeamUser(CourseTeamUserEntity entity)  {
        return courseService.searchUser(entity);
    }
    
    @RequestMapping("/deleteTeam.action")
    @ResponseBody
   	public ResultBean deleteTeam(String id){
    	ResultBean result = new ResultBean();
    	courseService.deleteCourseIeamById(id);
		result.setMessages(getMessage(MSG_S_DELETE));
		return result;
    }
    
    @RequestMapping("/saveTeamUser.action")
    @ResponseBody
    public ResultBean saveTeamUser(String teamId, String userIds)  {
        ResultBean result = new ResultBean();
        courseService.saveTeamUser(teamId,StringUtil.split(StringUtil.getOrElse(userIds), ","));
        result.setMessages(getMessage(MSG_S_SAVE));
        return result;
    }
    
    @RequestMapping("/saveTeam.action")
    @ResponseBody
    public ResultBean saveTeam(CourseTeamEntity team)  {
        ResultBean result = new ResultBean();
        courseService.saveTeam(team);
        result.setMessages(getMessage(MSG_S_SAVE));
        return result;
    }
    
    @RequestMapping("/deleteTeamStudent.action")
    @ResponseBody
    public ResultBean deleteTeamStudent(String userId,String teamId){
        ResultBean result = new ResultBean();
        courseService.deleteTeamStudent(userId,teamId);
        result.setMessages(getMessage(MSG_S_DELETE));
        return result;
    }
    /**
     * 学员管理页面初始化
     * @param id 课程ID
     * @return 当前课程下的学员列表
     */
    @RequestMapping("/searchUserInit.action")
    @ResponseBody
    public ModelAndView searchUserInit(UserView condition,String id){
        ModelAndView result = new ModelAndView("course/manager/course_user_join");
        condition.setPageSize(10);
        result.addObject("condition", condition);
        result.addObject("id", id);
        CourseEntity entity = new CourseEntity();
        entity.setId(id);
        entity = courseService.searchCourse(entity);
        //获取老师所属学校
        String schoolId = entity.getSchoolId();
        StudentClassInfoEntity studentClassInfoEntity = new StudentClassInfoEntity();
        studentClassInfoEntity.setSchoolId(schoolId);
        List<StudentClassInfoEntity> gradeList = new ArrayList<StudentClassInfoEntity>();
        gradeList = studentClassInfoService.searchStudentGradeList(studentClassInfoEntity);
        result.addObject("gradeList", gradeList);
        result.addObject("gradeId", null);
        result = getRoleAndNum(result);
        return result;
    }
    /**
     * 学员管理页面初始化
     * @param id 课程ID
     * @return 当前课程下的学员列表
     */
    @RequestMapping("/tsearchUserInit.action")
    @ResponseBody
    public ModelAndView tsearchUserInit(UserView condition,String id){
    	ModelAndView result = searchUserInit(condition, id);
    	result.setViewName("trainingplatform/client/center/center_course_user_join");
    	return result;
    }
   /**
    * 参与课程的人员
    * @param id
    * @return
    */
    @RequestMapping("/searchUser.action")
    @ResponseBody
    public ResultBean searchUser(UserView query)  {
        return courseService.searchUser(query);
    }
    /**
     *执行添加学员
     * @param courseId
     * @param userIds
     * @return
     */
    @RequestMapping("/saveUser.action")
    @ResponseBody
    public ResultBean saveUser(String courseId, String userIds)  {
        ResultBean result = new ResultBean();
        courseService.saveUser(courseId,StringUtil.split(StringUtil.getOrElse(userIds), ","));
        result.setMessages(getMessage(MSG_S_SAVE));
        return result;
    }
    
    @RequestMapping("/saveMessage.action")
    @ResponseBody
    public ResultBean saveMessage(String courseId, String content)  {
        ResultBean result = new ResultBean();
        courseService.saveMessage(courseId,content);
        result.setMessages(getMessage(MSG_S_SAVE));
        return result;
    }
    
    @RequestMapping("/searchMessageAll.action")
    @ResponseBody
    public ResultBean searchMessageAll(String courseId)  {
        return courseService.getMessageListByCourseId(courseId);
    }
    
    /**
     * 查询所有用户
     * //TODO 添加方法功能描述
     * @return
     */
    @RequestMapping("/searchUserAll.action")
    @ResponseBody
    public ResultBean searchUserAll(UserView query)  {
        return courseService.searchUserAll(query);
    }
    /**
     * 课程复制
     * @param id课程ID
     * @return
     * @throws Exception 
     */
    @RequestMapping("/copyInfo.action")
    @ResponseBody
    public ResultBean copyInfo(String id) throws Exception  {
        ResultBean result = new ResultBean();
        courseService.copyCourse(id);
        result.setMessages(getMessage(MSG_S_SAVE));
        return result;
    }
    /**
     *问卷添加
     * @param courseId 课程ID
     * @return
     */
    @RequestMapping("/measureInit.action")
    @ResponseBody
    public ModelAndView measureInit(String courseId){
        ModelAndView result = new ModelAndView("course/manager/measure_add");
        MeasurementEntity measure=new MeasurementEntity();
        result.addObject("measure", measure);
        result.addObject("courseId", courseId);
        result.addObject("pageType","add");
        result = getRoleAndNum(result);
        return result;
    }
    
    @RequestMapping("/share.action")
    @ResponseBody
    public ModelAndView share(String id){
        ModelAndView result = new ModelAndView("course/manager/course_share");
        String type="edit";
        CourseManagerView course = courseService.searchCourse4EditById(id,type);
        result.addObject("course", course);
        result.addObject("pageType",type);
        return result;
    }
    
    /**
     *问卷添加
     * @param courseId 课程ID
     * @return
     */
    @RequestMapping("/tmeasureInit.action")
    @ResponseBody
    public ModelAndView tmeasureInit(String courseId){
    	ModelAndView result = measureInit(courseId);
    	result.setViewName("/trainingplatform/client/center/center_measure_add");
    	return result;
    }
   /**
    * 
    *问卷编辑页面初始化
    * @param courseId
    * @return
    */
    @RequestMapping("/measureEditInit.action")
    @ResponseBody
    public ModelAndView measureEditInit(String courseId){
        ModelAndView result = new ModelAndView("course/manager/measure_add");
        MeasurementEntity measure=courseService.seachMeasureByCourseId(courseId);
        result.addObject("measure", measure);
        result.addObject("courseId", courseId);
        result.addObject("isOnlyView", true);
        result.addObject("pageType","edit");
        return result;
    }
    /**
     * 
     *问卷编辑页面初始化
     * @param courseId
     * @return
     */
    @RequestMapping("/tmeasureEditInit.action")
    @ResponseBody
    public ModelAndView tmeasureEditInit(String courseId){
    	ModelAndView result = measureEditInit(courseId);
    	result.setViewName("/trainingplatform/client/center/center_measure_add");
    	return result;
    }
    /**
    * 
    * 问卷添加/编辑
    * @param measureJson
    * @return
    * @throws Exception
    */
    @RequestMapping("/saveMeasure.action")
    @ResponseBody
    public ResultBean saveMeasure(String measureJson) throws Exception {
        MeasurementEntity measure = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), measureJson, MeasurementEntity.class);
        ResultBean result = ResultBean.success();
        if(StringUtil.isEmpty(measure.getId())) {
            courseService.saveMeasure(measure);
        } else {
            courseService.updateMeasure(measure);
        }
        return result;
    }
    /**
     * 删除课程下的学员
     * @param userId 学员ID
     * @param courseId 课程ID
     * @return
     */
    @RequestMapping("/deleteStudent.action")
    @ResponseBody
    public ResultBean deleteStudent(String userId,String courseId){
        ResultBean result = new ResultBean();
        courseService.deleteStudent(userId,courseId);
        result.setMessages(getMessage(MSG_S_DELETE));
        return result;
    }
    /**
     * 
     * 查看考试结果
     * @param condition
     * @return
     */
    @SuppressWarnings({ "unchecked"})
    @RequestMapping("/userExamInfoInit.action")
    public ModelAndView userExamInfoInit(MeasurementEntity condition) {
        ModelAndView result = new ModelAndView("course/manager/user_exam_info");
        if(StringUtil.isEmpty(condition.getLinkId())){
            throw new BusinessException("参数错误!");
        }
        List<MeasurementEntity> list = courseService.search(condition);
        if(list != null && list.size() != 0) {
            result.addObject("condition", list.get(0));
        } else {
            throw new BusinessException("参数错误!");
        }
        return result;
    }
    /**
     * 
     * 查看考试结果
     * @param condition
     * @return
     */
    @RequestMapping("/tuserExamInfoInit.action")
    public ModelAndView tuserExamInfoInit(MeasurementEntity condition) {
        ModelAndView result = userExamInfoInit(condition);
        result.setViewName("/trainingplatform/client/teacher/teacher_user_exam_info");
        return result;
    }
    /**
     * 考试结果
     * @param condition
     * @return
     * @throws Exception
     */
    @RequestMapping("/viewExamData.action")
    @ResponseBody
    public ResultBean viewExamData(MeasurementEntity condition) throws Exception {
        ResultBean userResult =  courseService.searchUserMeasurementResult(condition);
        return userResult;
    }
    /**
     * 
     * 重考
     * @param query
     * @return
     * @throws Exception
     */
    @RequestMapping("/reExam.action")
    @ResponseBody
    public ResultBean reExam(UserMeasurementResultEntity query) throws Exception {
        ResultBean result = ResultBean.success();
        measuermentService.deleteUserMeasuerment(query.getUserId(), query.getMeasurementId());
        result.setMessages("操作成功！");
        return result;
    } 
    /**
     * 
                   * 考试结果查看
     * @param condition
     * @return
     */
    @RequestMapping("/userExamDetail.action")
    public ModelAndView userExamDetail(UserMeasurementResultEntity condition) {
        ModelAndView result = new ModelAndView("course/manager/user_exam_detail");
        if(StringUtil.isEmpty(condition.getMeasurementId())){
            throw new BusinessException("参数错误!");
        }
        List<UserMeasurementResultEntity> list = measuermentService.searchUserMeasurementResultByEntiy(condition);
        if(list != null && list.size() != 0) {
            result.addObject("condition", list.get(0));
        } else {
            result.addObject("condition", new UserMeasurementResultEntity());
        }
        
        return result;
    }
    /**
     * 
     *老师首页— 考试结果查看
     * @param condition
     * @return
     */
    @RequestMapping("/tuserExamDetail.action")
    public ModelAndView tuserExamDetail(UserMeasurementResultEntity condition) {
        ModelAndView result = userExamDetail(condition);
        result.setViewName("/trainingplatform/client/teacher/teacher_user_exam_detail");
        return result;
    }
    /**
     * 检索测试信息
     * @param linkId
     * @return 返回结果
     */
    @RequestMapping("/searchMeasuerment.action")
    @ResponseBody
    public ResultBean searchMeasuerment(String measurementId,String userId) {
        if(StringUtil.isEmpty(measurementId)) {
            return ResultBean.error(getMessage("参数错误!"));
        }
        ResultBean result = ResultBean.success();
        List<QuestionEntity> questionList = measuermentService.searchMeasuermentNormalAndExam(userId, measurementId);
        result.setData(questionList);
        return result;
    }
    /**
     * 导入页面初始化
     * @return 返回结果
     */
    @RequestMapping("/importInit.action")
    public String importInit(){
        return "course/manager/course_import";
    } 
    /**
     * 导入课程
     * @param linkId
     * @return 返回结果
     * @throws Exception 
     */
    @RequestMapping("/importCourse.action")
    @ResponseBody
    public ResultBean importCourse(@RequestParam("course") CommonsMultipartFile course) throws Exception {
    	InputStream input = null;
		try{
			input = course.getInputStream();
			return courseService.importCourse(input);
		}finally{
			IOUtils.closeQuietly(input);
		}
   }
    
    @RequestMapping("/exportReward.action")
    @ResponseBody
    public UserView exportReward(String userId){
        UserView user = courseService.exportUserInfo(userId);
       
        ExcelDataBind edbUser = new ExcelDataBind(user);
        InputStream is = null;
        OutputStream os = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            String date = sdf.format(new Date());
            String fileName = URLEncoder.encode("学习情况" + date +".xlsx", "UTF-8");
            WebContext.getResponse().reset();
            is = CourseService.class.getResourceAsStream("exportUserStudyInfo.xlsx");
            WebContext.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            WebContext.getResponse().setContentType(" application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8");
            edbUser.bind(is,WebContext.getResponse().getOutputStream());
        } catch(IOException ex){
            
        } finally{
            IOUtils.closeQuietly(is);
            IOUtils.closeQuietly(os);
        }
        return null;
    }
    /**
                  *公共方法 获取角色信息
     * @param result
     * @return
     */
    private ModelAndView getRoleAndNum(ModelAndView result) {
    	int num = courseService.getStuStatus();
    	result.addObject("num", num);
    	UserEntity user = WebContext.getSessionUser();
    	//判断是否管理员
    	ManagerUserEntity entity = managerUserService.searchByUserId(user.getUserId());
    	if(!user.getUserName().equals("guest")) {
    		List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
    		//老师和教育局角色
    		if(roleList.size() > 0 && "A".equals(user.getUserKbn()) && null==entity && !"2542668".equals(user.getUserId())) {
    			for(UserRoleEntity role : roleList) {
    				if(role.getRoleId().equals("01")) {
    					result.addObject("role", "t");
    				}
    			}
    		//教育局角色	
    		}else if(roleList.size() > 0  && (null!=entity || "2542668".equals(user.getUserId()))){
    			result.addObject("role", "m");
    		}else if("B".equals(user.getUserKbn())){
    			result.addObject("role", "e");
    	    //家长和教研员角色
    		}else if("D".equals(user.getUserKbn()) || "E".equals(user.getUserKbn())){
    			result.addObject("role", "q");
    		//学生角色
    		}else {
    			result.addObject("role", "s");
    		}
    	}
        //管理员信息
    	if(null!=entity || "2542668".equals(user.getUserId())) {
    		result.addObject("manager", "1");
    	}else {
    		//非管理员
    		result.addObject("manager", "0");
    	}
    	return result;
    }
    /**
                  * 查询评价信息
     * @param courseId
     * @return
     */
    @RequestMapping("/evalutionlistInit.action")
    public ModelAndView evalutionlistInit(String courseId){
        ModelAndView result = new ModelAndView("course/manager/course_user_list");
        result.addObject("courseId",courseId);
        result.addObject("evalution",new CourseUserEvalutionEntity());
        UserCourseEntity uc = new UserCourseEntity();
		uc.setCourseId(courseId);
		List<UserCourseEntity> uu = new ArrayList<UserCourseEntity>();
		uu=	courseService.searchCourseUserList(uc);
		 CourseEntity entity = new CourseEntity();
		 entity.setId(courseId);
		 entity = courseService.searchByCourseId(entity);
		 List<String> wdList = new ArrayList<String>();
		 if(null!=entity && StringUtil.isNotBlank(entity.getWeidu())) {
			 String wds[] = entity.getWeidu().split(",");
			 for(int k=0;k<wds.length;k++) {
				 wdList.add(wds[k]);
			 }
			 result.addObject("wdjihe",wds.length);
		 }else {
			 result.addObject("wdjihe",0);
		 }
		 if(null!=uu && uu.size()>0) {
		 for(int i=0;i<uu.size();i++) {
			 String uid = uu.get(i).getUserId();
			 List<CourseUserEvalutionEntity> groupList = new ArrayList<CourseUserEvalutionEntity>();
			 for(int p=0;p<wdList.size();p++) {
				 String wd=wdList.get(p);
				 CourseUserEvalutionEntity evalution = new CourseUserEvalutionEntity();
				 CourseUserEvalutionEntity entity1 = new CourseUserEvalutionEntity();
				 entity1.setUserId(uid);
				 entity1.setCourseId(courseId);
				 entity1.setWeidu(wd);
				 evalution = courseUserEvalutionService.searchWdInfo(entity1);
				 if(null!=evalution) {
					 CourseUserEvalutionEntity evalutionEntity = new CourseUserEvalutionEntity();
					 evalutionEntity.setScore(evalution.getScore());
					 evalutionEntity.setWeidu(evalution.getWeidu());
					 evalutionEntity.setDegree(evalution.getDegree());
					 groupList.add(evalutionEntity);
				 }else {
					 CourseUserEvalutionEntity evalutionEntity = new CourseUserEvalutionEntity();
					 evalutionEntity.setScore(null);
					 evalutionEntity.setWeidu(wd);
					 evalutionEntity.setDegree(null);
					 groupList.add(evalutionEntity); 
				 }
			 }
			 uu.get(i).setGroupList(groupList);
			/* List<CourseUserEvalutionEntity> evlist = new ArrayList<CourseUserEvalutionEntity>();
			 CourseUserEvalutionEntity entity1 = new CourseUserEvalutionEntity();
			 entity1.setUserId(uid);
			 entity1.setCourseId(courseId);
			 evlist = courseUserEvalutionService.searchWdList(entity1);
			 List<CourseUserEvalutionEntity> groupList = new ArrayList<CourseUserEvalutionEntity>();
			 List<Double> scorseList= new ArrayList<Double>();
			 if(null!=evlist && evlist.size()>0) {
				 for(int k=0;k<evlist.size();k++) {
					 CourseUserEvalutionEntity evalution = new CourseUserEvalutionEntity();
					 evalution.setScore(evlist.get(k).getScore());
					 evalution.setWeidu(evlist.get(k).getWeidu());
					 evalution.setDegree(evlist.get(k).getDegree());
					 groupList.add(evalution);
					 //scorseList.add(evlist.get(k).getScore().doubleValue());
				 }
				 //uu.get(i).setScorseList(scorseList);
				 uu.get(i).setGroupList(groupList);
			 }else {
				 if(null!=entity && StringUtil.isNotBlank(entity.getWeidu())) {
					 String wds[] = entity.getWeidu().split(",");
					 for(int k=0;k<wds.length;k++) {
						 CourseUserEvalutionEntity evalution = new CourseUserEvalutionEntity();
						 evalution.setScore(null);
						 evalution.setWeidu(wds[k]);
						 evalution.setDegree("");
						 groupList.add(evalution);
						 //scorseList.add(0.0);
					 }
				 }
				 //uu.get(i).setScorseList(scorseList); 
				 uu.get(i).setGroupList(groupList);
			 }*/
		 }
	}
		 result.addObject("courseUserList",uu);
		result.addObject("weiduList",wdList);
        result = getRoleAndNum(result);
        return result;
    }
    /**
               * 保存课程评价信息
     * @param courseJson 课程信息
     * @return 返回结果
     * @throws Exception 
     */
    @RequestMapping("/saveEvalutionInfo.action")
    @ResponseBody
    public ResultBean saveEvalutionInfo(String params) throws Exception {
    	CourseUserEvalutionSaveEntity course = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), params, CourseUserEvalutionSaveEntity.class);
        ResultBean result = ResultBean.success();
        String courseId =course.getCourseId();
        String userId = course.getUserId();
        String degree = course.getDegree();
        

    	if(StringUtil.isNotBlank(course.getWeidu1())) {
    		//删除开始
    		CourseUserEvalutionEntity evalution = new CourseUserEvalutionEntity();
        	evalution.setUserId(userId);
        	evalution.setCourseId(courseId);
        	evalution.setWeidu("1");
        	CourseUserEvalutionEntity evalution1 = new CourseUserEvalutionEntity();
        	evalution1 = courseUserEvalutionService.searchOneInfo(evalution);
        	if(null!=evalution1) {
        		courseUserEvalutionService.deleteByEntity(evalution);
        	}
        	//删除结束
        	CourseUserEvalutionEntity entity = new CourseUserEvalutionEntity();
        	entity.setCourseId(courseId);
        	//根据课程id获取教师id
        	CourseEntity courseEntity = new CourseEntity();
        	courseEntity.setId(courseId);
        	courseEntity = courseService.searchByCourseId(courseEntity);
        	entity.setTeacherId(courseEntity.getTeacherId());
        	entity.setUserId(userId);
        	TpUserEntity entity1=new TpUserEntity();
    		entity1 = courseService.getUserByUserId(userId);
        	entity.setUserName(entity1.getPersonName());
        	entity.setWeidu("1");
        	entity.setDegree(degree);
        	entity.setCreateUser(WebContext.getSessionUserId());
        	entity.setScore(BigDecimal.valueOf(Double.valueOf(course.getWeidu1())));
        	courseUserEvalutionService.insert(entity);
        }
    	if(StringUtil.isNotBlank(course.getWeidu2())) {
    		//删除开始
    		CourseUserEvalutionEntity evalution = new CourseUserEvalutionEntity();
        	evalution.setUserId(userId);
        	evalution.setCourseId(courseId);
        	evalution.setWeidu("2");
        	CourseUserEvalutionEntity evalution1 = new CourseUserEvalutionEntity();
        	evalution1 = courseUserEvalutionService.searchOneInfo(evalution);
        	if(null!=evalution1) {
        		courseUserEvalutionService.deleteByEntity(evalution);
        	}
        	
        	//删除结束
        	CourseUserEvalutionEntity entity = new CourseUserEvalutionEntity();
        	entity.setCourseId(courseId);
        	//根据课程id获取教师id
        	CourseEntity courseEntity = new CourseEntity();
        	courseEntity.setId(courseId);
        	courseEntity = courseService.searchByCourseId(courseEntity);
        	entity.setTeacherId(courseEntity.getTeacherId());
        	entity.setUserId(userId);
        	TpUserEntity entity1=new TpUserEntity();
    		entity1 = courseService.getUserByUserId(userId);
        	entity.setUserName(entity1.getPersonName());
        	entity.setWeidu("2");
        	entity.setDegree(degree);
        	entity.setCreateUser(WebContext.getSessionUserId());
        	entity.setScore(BigDecimal.valueOf(Double.valueOf(course.getWeidu2())));
        	courseUserEvalutionService.insert(entity);
        }
    	if(StringUtil.isNotBlank(course.getWeidu3())) {
    		//删除开始
    		CourseUserEvalutionEntity evalution = new CourseUserEvalutionEntity();
        	evalution.setUserId(userId);
        	evalution.setCourseId(courseId);
        	evalution.setWeidu("3");
        	CourseUserEvalutionEntity evalution1 = new CourseUserEvalutionEntity();
        	evalution1 = courseUserEvalutionService.searchOneInfo(evalution);
        	if(null!=evalution1) {
        		courseUserEvalutionService.deleteByEntity(evalution);
        	}
        	//删除结束
        	CourseUserEvalutionEntity entity = new CourseUserEvalutionEntity();
        	entity.setCourseId(courseId);
        	//根据课程id获取教师id
        	CourseEntity courseEntity = new CourseEntity();
        	courseEntity.setId(courseId);
        	courseEntity = courseService.searchByCourseId(courseEntity);
        	entity.setTeacherId(courseEntity.getTeacherId());
        	entity.setUserId(userId);
        	TpUserEntity entity1=new TpUserEntity();
    		entity1 = courseService.getUserByUserId(userId);
        	entity.setUserName(entity1.getPersonName());
        	entity.setWeidu("3");
        	entity.setDegree(degree);
        	entity.setCreateUser(WebContext.getSessionUserId());
        	entity.setScore(BigDecimal.valueOf(Double.valueOf(course.getWeidu3())));
        	courseUserEvalutionService.insert(entity);
        }
    	if(StringUtil.isNotBlank(course.getWeidu4())) {
    		//删除开始
    		CourseUserEvalutionEntity evalution = new CourseUserEvalutionEntity();
        	evalution.setUserId(userId);
        	evalution.setCourseId(courseId);
        	evalution.setWeidu("4");
        	CourseUserEvalutionEntity evalution1 = new CourseUserEvalutionEntity();
        	evalution1 = courseUserEvalutionService.searchOneInfo(evalution);
        	if(null!=evalution1) {
        		courseUserEvalutionService.deleteByEntity(evalution);
        	}
        	//删除结束
        	CourseUserEvalutionEntity entity = new CourseUserEvalutionEntity();
        	entity.setCourseId(courseId);
        	//根据课程id获取教师id
        	CourseEntity courseEntity = new CourseEntity();
        	courseEntity.setId(courseId);
        	courseEntity = courseService.searchByCourseId(courseEntity);
        	entity.setTeacherId(courseEntity.getTeacherId());
        	entity.setUserId(userId);
        	TpUserEntity entity1=new TpUserEntity();
    		entity1 = courseService.getUserByUserId(userId);
        	entity.setUserName(entity1.getPersonName());
        	entity.setWeidu("4");
        	entity.setDegree(degree);
        	entity.setCreateUser(WebContext.getSessionUserId());
        	entity.setScore(BigDecimal.valueOf(Double.valueOf(course.getWeidu4())));
        	courseUserEvalutionService.insert(entity);
        }
    	if(StringUtil.isNotBlank(course.getWeidu5())) {
    		//删除开始
    		CourseUserEvalutionEntity evalution = new CourseUserEvalutionEntity();
        	evalution.setUserId(userId);
        	evalution.setCourseId(courseId);
        	evalution.setWeidu("5");
        	CourseUserEvalutionEntity evalution1 = new CourseUserEvalutionEntity();
        	evalution1 = courseUserEvalutionService.searchOneInfo(evalution);
        	if(null!=evalution1) {
        		courseUserEvalutionService.deleteByEntity(evalution);
        	}
        	//删除结束
        	CourseUserEvalutionEntity entity = new CourseUserEvalutionEntity();
        	entity.setCourseId(courseId);
        	//根据课程id获取教师id
        	CourseEntity courseEntity = new CourseEntity();
        	courseEntity.setId(courseId);
        	courseEntity = courseService.searchByCourseId(courseEntity);
        	entity.setTeacherId(courseEntity.getTeacherId());
        	entity.setUserId(userId);
        	TpUserEntity entity1=new TpUserEntity();
    		entity1 = courseService.getUserByUserId(userId);
        	entity.setUserName(entity1.getPersonName());
        	entity.setWeidu("5");
        	entity.setDegree(degree);
        	entity.setCreateUser(WebContext.getSessionUserId());
        	entity.setScore(BigDecimal.valueOf(Double.valueOf(course.getWeidu5())));
        	courseUserEvalutionService.insert(entity);
        }
    	if(StringUtil.isNotBlank(course.getWeidu6())) {
    		//删除开始
    		CourseUserEvalutionEntity evalution = new CourseUserEvalutionEntity();
        	evalution.setUserId(userId);
        	evalution.setCourseId(courseId);
        	evalution.setWeidu("6");
        	CourseUserEvalutionEntity evalution1 = new CourseUserEvalutionEntity();
        	evalution1 = courseUserEvalutionService.searchOneInfo(evalution);
        	if(null!=evalution1) {
        		courseUserEvalutionService.deleteByEntity(evalution);
        	}
        	//删除结束
        	CourseUserEvalutionEntity entity = new CourseUserEvalutionEntity();
        	entity.setCourseId(courseId);
        	//根据课程id获取教师id
        	CourseEntity courseEntity = new CourseEntity();
        	courseEntity.setId(courseId);
        	courseEntity = courseService.searchByCourseId(courseEntity);
        	entity.setTeacherId(courseEntity.getTeacherId());
        	entity.setUserId(userId);
        	TpUserEntity entity1=new TpUserEntity();
    		entity1 = courseService.getUserByUserId(userId);
        	entity.setUserName(entity1.getPersonName());
        	entity.setWeidu("6");
        	entity.setDegree(degree);
        	entity.setCreateUser(WebContext.getSessionUserId());
        	entity.setScore(BigDecimal.valueOf(Double.valueOf(course.getWeidu6())));
        	courseUserEvalutionService.insert(entity);
        }
        return result;
    }

}

