package com.cloudsoaring.web.course.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import sun.misc.BASE64Encoder;

public class MeasurementEntity {
	 private Configuration configuration = null;  
     
	    public MeasurementEntity(){  
	        configuration = new Configuration();  
	        configuration.setDefaultEncoding("UTF-8");  
	    }  
	      
	    public static void main(String[] args) {  
	    	MeasurementEntity test = new MeasurementEntity();  
	        test.createWord();  
	    }  
	    public void createWord(){  
	        Map<String,Object> dataMap=new HashMap<String,Object>();  
	        getData(dataMap);  
	        configuration.setClassForTemplateLoading(this.getClass(), "");//模板文件所在路径
	        Template t=null;  
	        try {  
	            t = configuration.getTemplate("test.ftl"); //获取模板文件
	        } catch (IOException e) {  
	            e.printStackTrace();  
	        }  
	        File outFile = new File("D:/outFile"+Math.random()*10000+".doc"); //导出文件
	        Writer out = null;  
	        try {  
	            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile)));  
	        } catch (FileNotFoundException e1) {  
	            e1.printStackTrace();  
	        }  
	           
	        try {  
	            t.process(dataMap, out); //将填充数据填入模板文件并输出到目标文件 
	        } catch (TemplateException e) {  
	            e.printStackTrace();  
	        } catch (IOException e) {  
	            e.printStackTrace();  
	        }  
	    }
	    private void getData(Map<String, Object> dataMap) {  
	        dataMap.put("title", "标题");  
	        dataMap.put("nian", "2016");  
	        dataMap.put("yue", "3");  
	        dataMap.put("ri", "6");   
	        dataMap.put("shenheren", "lc");  
	        dataMap.put("image", GetImageStrFromPath("C:\\Users\\Admin\\Desktop\\111.jpg"));
	          
	        List<Map<String,Object>> list = new ArrayList<Map<String,Object>>();  
	        for (int i = 0; i < 10; i++) {  
	            Map<String,Object> map = new HashMap<String,Object>();  
	            map.put("xuehao", i);  
	            map.put("neirong", "内容"+i);  
	            map.put("image", GetImageStrFromPath("C:\\Users\\Admin\\Desktop\\测试.jpg"));
	            list.add(map);  
	        }  
	          
	          
	        dataMap.put("list", list);  
	    }  
	    public static String GetImageStrFromPath(String src) {
	             /*  InputStream in = null;
					byte[] data = null;
					// 读取图片字节数组  
					try {
					in = new FileInputStream(imgPath);
					data = new byte[in.available()];
					in.read(data);
					in.close();
					} catch (IOException e) {
					e.printStackTrace();
					}
					// 对字节数组Base64编码  
					BASE64Encoder encoder = new BASE64Encoder();
					// 返回Base64编码过的字节数组字符串  
					return encoder.encode(data);*/
	    	          if(src==null||src==""){
	    		              return "";
	    		          }
	    		          File file = new File(src);
	    		          if(!file.exists()) {
	    		              return "";
	    		         }
	    		         InputStream in = null;
	    		         byte[] data = null;  
	    		         try {
	    		             in = new FileInputStream(file);
	    		         } catch (FileNotFoundException e1) {
	    		            e1.printStackTrace();
	    		        }
	    		         try {  
	    		             data = new byte[in.available()];  
	    		            in.read(data);  
	    		            in.close();  
	    		         } catch (IOException e) {  
	    	           e.printStackTrace();  
	    		        } 
	    		         BASE64Encoder encoder = new BASE64Encoder();
	    		        return encoder.encode(data);
	    }
	
}
