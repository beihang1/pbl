package com.cloudsoaring.web.course.controller;


import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudsoaring.web.bus.entity.UserEntity;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.service.HsUserService;

/***
 * 用户登录，注册
 * 
 * @author LILIANG
 *
 */
@Controller
@RequestMapping("/pub/hsUser")
public class HsUserController extends BaseController implements CourseConstants {
	
    @Autowired
	private HsUserService hsUserService;
			
	/**
	 * 用户注册
	 * @author LILIANG
	 * @return UserEntity
	 */
	@RequestMapping("/doRegist.action")
	@ResponseBody
	public ResultBean doRegist(UserEntity user,String codeNumber) {
		ResultBean result = new ResultBean();
        hsUserService.doRegist(user,codeNumber);	        
        return result;	
	}
	
	/**
	 * 生成验证码
	 * @author LILIANG
	 * @return 
	 */
	@RequestMapping("/getCodeNumber.action")
	@ResponseBody
    public void getCodeNumber(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		hsUserService.getCodeNumber(req,resp);
    }
	
}
