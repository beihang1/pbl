package com.cloudsoaring.web.course.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.CourseExperimentalResultEntity;
import com.cloudsoaring.web.course.entity.ForceEntity;
import com.cloudsoaring.web.course.entity.SeriesForm;
import com.cloudsoaring.web.course.service.ForceService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 物体受力分析Controller
 * @author fanglili
 * @date 2019-09-26
 */
@Controller
@RequestMapping("/pub/effectsOfForce")
@ControllerAdvice
public class EffectsOfForceController extends BaseController implements CourseConstants {
	
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Autowired
	ForceService forceService;
	
	/**
	 * 获取当前登录用户最近5次的记录
	 * @return JSONArray
	 */
	@RequestMapping("/getLastFiveForceInfo.action")
	@ResponseBody
	public JSONArray getLastFiveForceInfo() {
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		List<ForceEntity> forceList = forceService.getLastTenForceList(WebContext.getSessionUserId());
		List<String> categoryList = new ArrayList<String>();
		// series
		List<SeriesForm> seriesList = new ArrayList<SeriesForm>();
		for(int i=0;i<3;i++) {
			SeriesForm series = new SeriesForm();
			series.setId(i);
			seriesList.add(series);
		}
		Map<String,Map> userMap = new HashMap<String, Map>();
		if(forceList.size() > 0 ) {
			for(ForceEntity entity : forceList) {
				categoryList.add(sdf.format(entity.getCreateTime()));
				List<String> seriesData = new ArrayList<String>();
				Map map = new HashMap();
				map.put("拉力", entity.getPullingForce());
				map.put("重力", entity.getWeight());
				map.put("摩擦力", entity.getFrictionalForce());
				userMap.put(sdf.format(entity.getCreateTime()), map);
			}
			for(int j=0;j<seriesList.size();j++)  {
				List<String> seriesData = new ArrayList<String>();
				for(String s :categoryList) {
					if(j == 0) {
						seriesData.add(userMap.get(s).get("拉力").toString());
					}else if(j == 1) {
						seriesData.add(userMap.get(s).get("重力").toString());
					}else if(j == 2) {
						seriesData.add(userMap.get(s).get("摩擦力").toString());
					}
				}
				seriesList.get(j).setData(seriesData);
			}
		}
		jo.put("category", categoryList);
		jo.put("series", seriesList);
		json.add(jo);
		return json;
	}

}
