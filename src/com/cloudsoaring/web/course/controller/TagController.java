/**
 * 
 */
package com.cloudsoaring.web.course.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseStateConstants;
import com.cloudsoaring.web.course.service.TagService;
import com.cloudsoaring.web.taxonomy.entity.TagEntityView;

/**
 * 课程分类/标签
 * @author JGJ
 *
 */
@Controller
@RequestMapping("/pub/tag")
public class TagController {

	@Autowired
	private TagService tagService;

	/**
	 * 标签一览
	 * @param flag 是否显示标签 0 不显示 1 显示
	 * @param businessType 业务类型
	 * @return
	 */
	@RequestMapping("/searchCategory.action")
	@ResponseBody
	public ResultBean searchCategory(String flag,String businessType){
		ResultBean  result=tagService.searchTag(flag,businessType);
		return result;
	}
	
	/**
	 * 标签一览
	 * @param flag 是否显示标签 0 不显示 1 显示
	 * @param businessType 业务类型
	 * @return
	 */
	@RequestMapping("/searchCategory2.action")
	@ResponseBody
	public ResultBean searchCategory2(String flag,String businessType,String schoolName,String shareFlag){
		ResultBean  result=tagService.searchTag2(flag,businessType,schoolName,shareFlag);
		return result;
	}
	
	/**
	 * 子标签一览
	 * @param tagTypeId 查询的标签类ID
	 * @param parentTagId 标签ID
	 * @return
	 */
	@RequestMapping("/searchSubCategory.action")
	@ResponseBody
	public ResultBean searchSubCategory(String tagTypeId,String parentTagId ){
		ResultBean  result=tagService.searchSubCategory(tagTypeId,parentTagId);
		return result;
	}
	
	/**
	 * 检索计划标签
	 * @return ResultBean
	 */
	@RequestMapping("/searchPlanTags.action")
	@ResponseBody
	public ResultBean searchPlanTags(){
		ResultBean result = tagService.searchTag(CourseStateConstants.TAG_BUSSINESS_TYPE_PLAN);
		return result;
	}
	/**
	 * 标签一览手机端
	 * @param 
	 * @return ResultBean
	 */
	@RequestMapping("/searchCateTagList.action")
	@ResponseBody
	public ResultBean searchCateTagList(TagEntityView tagEntity){
		return  tagService.searchCateTagList(tagEntity);
		
	}
	/**
	 * 标签一览
	 * @param flag 是否显示标签 0 不显示 1 显示
	 * @param businessType 业务类型
	 * @return
	 */
	@RequestMapping("/searchSchhoolCategory2.action")
	@ResponseBody
	public ResultBean searchSchhoolCategory2(String flag,String businessType){
		ResultBean  result=tagService.searchSchhoolCategory2(flag,businessType);
		return result;
	}
	/**
	 * 标签一览
	 * @param flag 是否显示标签 0 不显示 1 显示
	 * @param businessType 业务类型
	 * @return
	 */
	@RequestMapping("/searchSchhoolCategory3.action")
	@ResponseBody
	public ResultBean searchSchhoolCategory3(String flag,String businessType){
		ResultBean  result=tagService.searchSchhoolCategory3(flag,businessType);
		return result;
	}
}
