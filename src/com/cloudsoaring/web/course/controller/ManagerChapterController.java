package com.cloudsoaring.web.course.controller;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFShape;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.poi.xslf.usermodel.XSLFTextParagraph;
import org.apache.poi.xslf.usermodel.XSLFTextRun;
import org.apache.poi.xslf.usermodel.XSLFTextShape;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.FileEntity;
import com.cloudsoaring.web.bus.entity.UserEntity;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.utils.ConfigUtil;
import com.cloudsoaring.web.common.utils.JSONUtil;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.ChapterEntity;
import com.cloudsoaring.web.course.entity.ManagerUserEntity;
import com.cloudsoaring.web.course.service.ChapterService;
import com.cloudsoaring.web.course.service.CourseService;
import com.cloudsoaring.web.course.service.ManagerUserService;
import com.cloudsoaring.web.course.view.ChapterView;
import com.cloudsoaring.web.course.view.CourseManagerView;
import com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity;
import com.mysql.fabric.xmlrpc.base.Array;

/**
 * 
 * 章节管理
 * 
 * @author lijuan
 */
@Controller
@RequestMapping("/manager/chapter")
public class ManagerChapterController extends BaseController implements CourseConstants {
	@Autowired
	private ChapterService chapterService;
	@Autowired
    private CourseService courseService;
	@Autowired
	private ManagerUserService managerUserService;
	@Autowired
	private FileService fileService;
	/**
	 * 
	 * 初始化章节列表
	 * 
	 * @param courseId
	 * @return
	 */
	@RequestMapping("/chapterListInit.action")
	public ModelAndView addInit(String courseId, String courseNo, String title, ChapterEntity condition) {
		ModelAndView result = new ModelAndView("course/manager/chapter_list");
		CourseManagerView course = new CourseManagerView();
		course.setId(courseId);
		course.setCourseNo(courseNo);
		course.setTitle(title);
		condition.setPageSize(20);
		result.addObject("condition", condition);
		result.addObject("course", course);
		result = getRoleAndNum(result);
		return result;
	}

	/**
	 * 
	 * 初始化章节列表 - 老师首页
	 * 
	 * @param courseId
	 * @return
	 */
	@RequestMapping("/tchapterListInit.action")
	public ModelAndView taddInit(String courseId, String courseNo, String title, ChapterEntity condition) {
		ModelAndView result = addInit(courseId, courseNo, title, condition);
		result.setViewName("trainingplatform/client/teacher/teacher_chapter_list");
		return result;
	}

	/**
	 * 
	 * 检索章节列表
	 * 
	 * @param queryInfo
	 * @return
	 */
	@RequestMapping("/listSearch.action")
	@ResponseBody
	public ResultBean listSearch(ChapterEntity entity) {
		return chapterService.searchList(entity);
	}

	/**
	 * 
	 * 添加页面初始化：章
	 * 
	 * @param courseId 课程ID
	 * @return
	 */
	@RequestMapping("/addInit.action")
	public ModelAndView addInit(String courseId) {
		ModelAndView result = new ModelAndView("course/manager/chapter_three_edit");
		ChapterView chapter = new ChapterView();
		chapter.setMeasureType("0");
		result.addObject("chapter", chapter);
		result.addObject("courseId", courseId);
		result.addObject("chapterFlag", BIG_CHAPTER);
		result = getRoleAndNum(result);
		return result;
	}

	/**
	 * 添加页面初始化：章 - 老师首页
	 * 
	 * @param courseId
	 * @return
	 */
	@RequestMapping("/taddInit.action")
	public ModelAndView taddInit(String courseId) {
		ModelAndView result = addInit(courseId);
		result.setViewName("trainingplatform/client/teacher/teacher_chapter_three_edit");
		return result;
	}

	/**
	 * 
	 * 编辑页面初始化:章
	 * 
	 * @param initInfo 章节对象
	 * @return
	 */
	@RequestMapping("/editInit.action")
	public ModelAndView editInit(ChapterView initInfo) {
		ModelAndView result = new ModelAndView("course/manager/chapter_three_edit");
		ChapterView chapter = chapterService.searchCourse4EditById(initInfo.getId(), "edit", initInfo.getMeasureType());
		result.addObject("chapter", chapter);
		result.addObject("measureType", chapter.getMeasureType());
		result.addObject("chapterFlag", BIG_CHAPTER);
		result.addObject("pageType", "edit");
		if (chapter.getMeasureType().equals("5")) {
			result.addObject("chapterFileId", chapter.getSummary());
		}
		result = getRoleAndNum(result);
		return result;
	}
	/**
	 * 
	 * 编辑页面初始化:章
	 * 
	 * @param initInfo 章节对象
	 * @return
	 */
	@RequestMapping("/resourceeditInit.action")
	public ModelAndView resourceeditInit(ChapterView initInfo) {
		ModelAndView result = new ModelAndView("course/manager/chapter_three_edit2");
		ChapterView chapter = chapterService.searchCourse4EditById(initInfo.getId(), "edit", initInfo.getMeasureType());
		result.addObject("chapter", chapter);
		result.addObject("measureType", chapter.getMeasureType());
		result.addObject("chapterFlag", BIG_CHAPTER);
		result.addObject("pageType", "edit");
		if (chapter.getMeasureType().equals("5")) {
			result.addObject("chapterFileId", chapter.getSummary());
		}
		return result;
	}
	/**
	 * 编辑页面初始化:章 - 老师首页
	 * 
	 * @param initInfo 章节对象
	 * @return
	 */
	@RequestMapping("/teditInit.action")
	public ModelAndView teditInit(ChapterView initInfo) {
		ModelAndView result = editInit(initInfo);
		result.setViewName("trainingplatform/client/teacher/teacher_chapter_three_edit");
		return result;
	}

	/**
	 * 添加页面初始化：节
	 * 
	 * @param courseId 课程ID
	 * @return
	 */
	@RequestMapping("/chapterSmallInit.action")
	public ModelAndView chapterSmallInit(String courseId, String parentId) {
		ModelAndView result = new ModelAndView("course/manager/chapter_three_edit");
		ChapterView chapter = new ChapterView();
		chapter.setMeasureType("4");
		result.addObject("courseId", courseId);
		result.addObject("parentId", parentId);
		result.addObject("chapterFlag", SMALL_CHAPTER);
		result.addObject("chapter", chapter);
		return result;
	}

	/**
	 * 
	 * 编辑页面初始化:章 - 老师首页
	 * 
	 * @param initInfo 章节对象
	 * @return
	 */
	@RequestMapping("/tchapterSmallInit.action")
	public ModelAndView tchapterSmallInit(String courseId, String parentId) {
		ModelAndView result = chapterSmallInit(courseId, parentId);
		result.setViewName("trainingplatform/client/teacher/teacher_chapter_three_edit");
		return result;
	}

	/**
	 * 
	 * 编辑页面初始化:节
	 * 
	 * @param initInfo 章节对象
	 * @return
	 */
	@RequestMapping("/editSmallInit.action")
	public ModelAndView editSmallInit(ChapterView initInfo) {
		ModelAndView result = new ModelAndView("course/manager/chapter_three_edit");
		ChapterView chapter = chapterService.searchCourse4EditById(initInfo.getId(), "edit", initInfo.getMeasureType());
		result.addObject("chapter", chapter);
		result.addObject("measureType", chapter.getMeasureType());
		result.addObject("courseId", chapter.getCourseId());
		result.addObject("chapterFlag", SMALL_CHAPTER);
		result.addObject("pageType", "edit");
		return result;
	}

	/**
	 * 
	 * 编辑页面初始化:节 - 老师首页
	 * 
	 * @param initInfo 章节对象
	 * @return
	 */
	@RequestMapping("/teditSmallInit.action")
	public ModelAndView teditSmallInit(ChapterView initInfo) {
		ModelAndView result = editSmallInit(initInfo);
		result.setViewName("trainingplatform/client/teacher/teacher_chapter_three_edit");
		return result;
	}

	/**
	 * 
	 * 编辑页面初始化:小节
	 * 
	 * @param initInfo 章节对象
	 * @return
	 */
	@RequestMapping("/editThreeInit.action")
	public ModelAndView editThreeInit(ChapterView initInfo) {
		ModelAndView result = new ModelAndView("course/manager/chapter_three_edit");
		ChapterView chapter = chapterService.searchCourse4EditById(initInfo.getId(), "edit", initInfo.getMeasureType());
		result.addObject("chapter", chapter);
		result.addObject("courseId", chapter.getCourseId());
		result.addObject("measureType", chapter.getMeasureType());
		result.addObject("pageType", "edit");
		result.addObject("chapterFlag", THREE_CHAPTER);
		return result;
	}

	/**
	 * 
	 * 编辑页面初始化:小节 - 老师首页
	 * 
	 * @param initInfo 章节对象
	 * @return
	 */
	@RequestMapping("/teditThreeInit.action")
	public ModelAndView teditThreeInit(ChapterView initInfo) {
		ModelAndView result = editThreeInit(initInfo);
		result.setViewName("trainingplatform/client/teacher/teacher_chapter_three_edit");
		return result;
	}

	/**
	 * 添加页面初始化：小节
	 * 
	 * @param courseId
	 * @return
	 */
	@RequestMapping("/chapterThreeInit.action")
	public ModelAndView chapterThreeInit(String courseId, String parentId) {
		ModelAndView result = new ModelAndView("course/manager/chapter_three_edit");
		ChapterView chapter = new ChapterView();
		chapter.setMeasureType("4");
		result.addObject("chapter", chapter);
		result.addObject("courseId", courseId);
		result.addObject("parentId", parentId);
		result.addObject("chapterFlag", THREE_CHAPTER);
		return result;
	}

	/**
	 * 添加页面初始化：小节 - 老师首页
	 * 
	 * @param courseId
	 * @return
	 */
	@RequestMapping("/tchapterThreeInit.action")
	public ModelAndView tchapterThreeInit(String courseId, String parentId) {
		ModelAndView result = chapterThreeInit(courseId, parentId);
		result.setViewName("trainingplatform/client/teacher/teacher_chapter_three_edit");
		return result;
	}

	/**
	 * 保存章信息
	 * 
	 * @param chapterJson
	 * @return 返回结果
	 * @throws Exception
	 */
	@RequestMapping("/saveInfo.action")
	@ResponseBody
	public ResultBean saveInfo(String chapterJson, String videoLength,Boolean isTransForm) throws Exception {
		ChapterView chapter = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateMinuteFormatDefualt()),
				chapterJson, ChapterView.class);
		String content = chapter.getContent();
		//章节类型为图文时，修改上传视频的content
		if(chapter.getMeasureType().equals(CHAPTER_TYPE_NORMAL) && content != null && !"".equals(content)) {
			if(content.indexOf(".mp4") > 0) {
				if(content.indexOf("fileId") > 0) {
					//String fileId = content.substring(content.indexOf("fileId=") + 7, content.indexOf("&fileName"));
					/*content = "<p><video controls=\"controls\" style=\"position: absolute;top: 0;left: 0;width: 100%;height:100%;\">"
							+ "<source src=http://" + WebContext.getRequest().getServerName() + ":81"
							+ fileService.getFile(fileId, "video").getPath() + " type=\"video/mp4\">" + "</video></p>";*/
					/*content = "<p><video controls=\"controls\" style=\"position: absolute;top: 0;left: 0;width: 100%;height:100%;\">"
							+ "<source src=" + PropertyUtil.getProperty("res.context.root")
							+ fileService.getFile(fileId, "video").getPath() + " type=\"video/mp4\">" + "</video></p>";*/
					//chapter.setContent(content);
				}
			}
		}else if(chapter.getMeasureType().equals(CHAPTER_TYPE_VIDEO) && content != null && !"".equals(content)) {
			//章节类型为视频时，修改上传视频的content	
			/*String fileId = content.substring(content.indexOf("fileId=") + 7, content.indexOf("&fileName"));
			content = "<p><video controls=\"controls\" style=\"position: absolute;top: 0;left: 0;width: 100%;height:100%;\">"
					+ "<source src=http://" + WebContext.getRequest().getServerName() + ":81"
					+ fileService.getFile(fileId, "video").getPath() + " type=\"video/mp4\">" + "</video></p>";*/
			//chapter.setContent(content);
		}
		if(content.indexOf(".pptx") > 0) {
			if(isTransForm) {
				String s1 = content.substring(content.indexOf(".pptx")-100,content.indexOf(".pptx"));
				String fileId = s1.substring(s1.indexOf("fileId=") + 7, s1.indexOf("&fileName"));
				FileEntity file = new FileEntity();
				file = fileService.getFileInfo(fileId);
				if(file != null) {
					List<FileEntity> fileList = doPPTtoImage(PropertyUtil.getProperty("res.storage.root")+file.getPath());
					if(fileList.size() >0) {
						String contentInfo = "<p style='text-align: center;'>";
						for(FileEntity entity : fileList) {
							contentInfo += "<img style='height:750px;' "
									+ "src='"+PropertyUtil.getProperty("unity.context.root")+"/pblMgr/file/download.action?fileId="+entity.getFileId()+"&fileName"
									+ "="+entity.getFileName()+"'/>";
						}
						chapter.setContent(content+contentInfo+"</p>");
					}
				}
			}
		}
		ResultBean result = ResultBean.success();
		if (StringUtil.isEmpty(chapter.getId())) {
			chapterService.insertChapterInfoAll(chapter, videoLength);
		} else {
			chapterService.updateChapterAll(chapter, videoLength);
		}
		return result;
	}

	/**
	 * 保存节信息
	 * 
	 * @param chapterJson
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saveSmallChapter.action")
	@ResponseBody
	public ResultBean saveSmallChapter(String chapterJson, String videoLength) throws Exception {
		ChapterView chapter = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateMinuteFormatDefualt()),
				chapterJson, ChapterView.class);
		ResultBean result = ResultBean.success();
		if (StringUtil.isEmpty(chapter.getId())) {
			chapterService.insertChapterInfoAll(chapter, videoLength);
		} else {
			chapterService.updateChapterAll(chapter, videoLength);
		}
		return result;
	}

	/**
	 * 
	 * 保存小节信息
	 * 
	 * @param chapterJson
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saveThreeChapter.action")
	@ResponseBody
	public ResultBean saveThreeChapter(String chapterJson, String videoLength) throws Exception {
		ChapterView chapter = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateMinuteFormatDefualt()),
				chapterJson, ChapterView.class);
		ResultBean result = ResultBean.success();
		if (StringUtil.isEmpty(chapter.getId())) {
			chapterService.insertChapterInfoAll(chapter, videoLength);
		} else {
			chapterService.updateChapterAll(chapter, videoLength);
		}
		return result;
	}
    /**
     * 
     * @param id
     * @param status
     * @return
     * @throws Exception
     */
	@RequestMapping("/publicChapter.action")
	@ResponseBody
	public ResultBean publicChapter(String id, String status) throws Exception {
		chapterService.publicChapter(id, status);
		String msg = "";
		if (status.equals(CHAPTER_STATUS_PUBLIC)) {
			msg = "取消发布成功！";
		} else {
			msg = "发布成功！";
		}
		return ResultBean.success(msg);
	}

	/**
	 * 删除章节信息
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping("/deleteChapter.action")
	@ResponseBody
	public ResultBean deleteChapter(String id) {
		ResultBean result = new ResultBean();
		chapterService.deleteChapter(id);
		result.setMessages(getMessage(MSG_S_DELETE));
		return result;
	}
	/**
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/preOrderChapter.action")
	@ResponseBody
	public ResultBean preOrderChapter(String id) throws Exception {
		return chapterService.preOrderChapter(id);
	}
	/**
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/nextOrderChapter.action")
	@ResponseBody
	public ResultBean nextOrderChapter(String id) throws Exception {
		return chapterService.nextOrderChapter(id);
	}
	/**
	 * 公共方法获取角色信息
	 * @param result
	 * @return
	 */
	private ModelAndView getRoleAndNum(ModelAndView result) {
		int num = courseService.getStuStatus();
    	result.addObject("num", num);
    	UserEntity user = WebContext.getSessionUser();
    	//判断是否管理员
    	ManagerUserEntity entity = managerUserService.searchByUserId(user.getUserId());
    	if(!user.getUserName().equals("guest")) {
    		List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
    		//老师和教育局角色
    		if(roleList.size() > 0 && "A".equals(user.getUserKbn()) && null==entity && !"2542668".equals(user.getUserId())) {
    			for(UserRoleEntity role : roleList) {
    				if(role.getRoleId().equals("01")) {
    					result.addObject("role", "t");
    				}
    			}
    		//教育局角色	
    		}else if(roleList.size() > 0  && (null!=entity || "2542668".equals(user.getUserId()))){
    			result.addObject("role", "m");
    		}else if("B".equals(user.getUserKbn())){
    			result.addObject("role", "e");
    	    //家长和教研员角色
    		}else if("D".equals(user.getUserKbn()) || "E".equals(user.getUserKbn())){
    			result.addObject("role", "q");
    		//学生角色
    		}else {
    			result.addObject("role", "s");
    		}
    	}

    	if(null!=entity || "2542668".equals(user.getUserId())) {
    		result.addObject("manager", "1");
    	}else {
    		result.addObject("manager", "0");
    	}
    	return result;
	}
	
	private List<FileEntity> doPPTtoImage(String filePath) throws Exception {
		List<FileEntity> fileList = new ArrayList<FileEntity>();
    	File file = new File(filePath);
        boolean isppt = checkFile(file);
        if (!isppt) {
            System.out.println("The image you specify don't exit!");
            return null;
        }
        try {
            FileInputStream is = new FileInputStream(file);
            XMLSlideShow ppt = new XMLSlideShow(is);
            //ppt.setPageSize(new Dimension(3000, 2000));
            is.close();
            Dimension pgsize = ppt.getPageSize();
            XSLFSlide [] slide = ppt.getSlides();
            for (int i = 0; i < slide.length; i++) {
            	for(XSLFShape shape : slide[i].getShapes()) {
            		if(shape instanceof XSLFTextShape) {
            			XSLFTextShape tsh = (XSLFTextShape)shape;
            			for(XSLFTextParagraph p : tsh) {
            				for(XSLFTextRun r : p) {
            					r.setFontFamily("微软雅黑");
            				}
            			}
            		}
            	}
            	FileEntity returnFile = new FileEntity();
                BufferedImage img = new BufferedImage(pgsize.width*2, pgsize.height*2,BufferedImage.TYPE_INT_RGB);
                Graphics2D graphics = img.createGraphics();
                graphics.setPaint(Color.BLUE);
                graphics.scale(2, 2);
                graphics.fill(new Rectangle2D.Float(0, 0, pgsize.width*2, pgsize.height*2));
                slide[i].draw(graphics);
                File path = new File(PropertyUtil.getProperty("res.storage.ppt"));
                if (!path.exists()) {
                    path.mkdir();
                }
                double nameNo = Math.random();
                FileOutputStream out = new FileOutputStream(path + "/" +nameNo+ ".jpg");
                javax.imageio.ImageIO.write(img, "png", out);
                FileEntity fileEntity = new FileEntity();
				fileEntity.setContentType("jpg");
				fileEntity.setCreateUser(WebContext.getSessionUserId());
				fileEntity.setCreateDate(new Date());
				fileEntity.setFileId(IDGenerator.genUID());
				fileEntity.setFileName((i + 1) + ".jpg");
				fileEntity.setFileSize(file.length());
				fileEntity.setPath("/image/ppt/"+nameNo+ ".jpg");
				fileEntity.setType("ATTACHMENT");
				returnFile.setFileId(fileEntity.getFileId());
				returnFile.setFileName(fileEntity.getFileName());
				fileService.save(fileEntity);
				fileList.add(returnFile);
                out.close();
            }
            return fileList;
        } catch (FileNotFoundException e) {
            System.out.println(e);
        } catch (IOException e) {

        }
        return fileList;
    } 
    
    private  boolean checkFile(File file) {
        boolean isppt = false;
        String filename = file.getName();
        String suffixname = null;
        if (filename != null && filename.indexOf(".") != -1) {
            suffixname = filename.substring(filename.lastIndexOf("."));
            if (suffixname.equals(".pptx")) {
                isppt = true;
            }
            return isppt;
        } else {
            return isppt;
        }
    }
}
