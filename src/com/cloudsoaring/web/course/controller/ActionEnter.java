package com.cloudsoaring.web.course.controller;

import com.baidu.ueditor.ConfigManager;
import com.baidu.ueditor.define.ActionMap;
import com.baidu.ueditor.define.BaseState;
import com.baidu.ueditor.define.State;
import com.baidu.ueditor.hunter.FileManager;
import com.baidu.ueditor.hunter.ImageHunter;
import com.baidu.ueditor.upload.Uploader;
import com.cloudsoaring.common.utils.FileUtil;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

public class ActionEnter {
  private HttpServletRequest request = null;
  
  private String rootPath = null;
  
  private String uploadRootPath = null;
  
  private String contextPath = null;
  
  private String actionType = null;
  
  private Boolean needCompress = Boolean.valueOf(false);
  
  private ConfigManager configManager = null;
  
  public ActionEnter(HttpServletRequest request, String rootPath, String uploadRootPath) {
    this.request = request;
    this.rootPath = rootPath;
    this.actionType = request.getParameter("action");
    this.needCompress = StringUtil.toBoolean(request.getParameter("needCompress"));
    this.uploadRootPath = uploadRootPath;
    this.contextPath = request.getContextPath();
    this.configManager = ConfigManager.getInstance(this.rootPath, this.uploadRootPath, this.contextPath, request.getRequestURI());
    System.out.println("9999999999");
    System.out.println(this.uploadRootPath);
  }
  
  public String exec() {
    try {
      WebContext.reset(this.request);
      FileUtil.beginTransaction();
      String callbackName = this.request.getParameter("callback");
      String result = "";
      if (callbackName != null) {
        if (!validCallbackName(callbackName)) {
          result = (new BaseState(false, 401)).toJSONString();
        } else {
          result = String.valueOf(callbackName) + "(" + invoke() + ");";
        } 
      } else {
        result = invoke();
      } 
      return result;
    } finally {
      WebContext.clear();
    } 
  }
  
  public String invoke() {
    int nac;
    String[] list;
    int start;
    if (this.actionType == null || !ActionMap.mapping.containsKey(this.actionType))
      return (new BaseState(false, 101)).toJSONString(); 
    if (this.configManager == null || !this.configManager.valid())
      return (new BaseState(false, 102)).toJSONString(); 
    State state = null;
    int actionCode = ActionMap.getType(this.actionType);
    Map<String, Object> conf = null;
    switch (actionCode) {
      case 0:
        return this.configManager.getAllConfig().toString();
      case 1:
      case 2:
      case 4:
        conf = this.configManager.getConfig(actionCode);
        nac = actionCode;
        if (this.needCompress.booleanValue())
          nac = 9; 
        state = (new Uploader(this.request, conf)).doExec(nac);
        break;
      case 3:
        conf = this.configManager.getConfig(actionCode);
        state = (new Uploader(this.request, conf)).doExec(actionCode);
        break;
      case 5:
        conf = this.configManager.getConfig(actionCode);
        list = this.request.getParameterValues((String)conf.get("fieldName"));
        state = (new ImageHunter(this.request, conf)).capture(list, actionCode);
        break;
      case 6:
      case 7:
        conf = this.configManager.getConfig(actionCode);
        start = getStartIndex();
        state = (new FileManager(conf)).listFile(start);
        break;
    } 
    return state.toJSONString();
  }
  
  public int getStartIndex() {
    String start = this.request.getParameter("start");
    try {
      return Integer.parseInt(start);
    } catch (Exception e) {
      return 0;
    } 
  }
  
  public boolean validCallbackName(String name) {
    if (name.matches("^[a-zA-Z_]+[\\w0-9_]*$"))
      return true; 
    return false;
  }
}

