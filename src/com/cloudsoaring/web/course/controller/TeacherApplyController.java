package com.cloudsoaring.web.course.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.web.bus.entity.TagEntity;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.TeacherApplyEntity;
import com.cloudsoaring.web.course.service.TagService;
import com.cloudsoaring.web.course.service.TeacherApplyService;

/***
 * 聘请教师审批清单
 * 
 * @author LILIANG
 *
 */
@Controller
@RequestMapping("/client/teacherApply")
public class TeacherApplyController extends BaseController implements CourseConstants{

	@Autowired
	private TeacherApplyService teacherApplyService;
	@Autowired
	private TagService tagService;
	
	/**
	 * 应聘讲师一览界面
	 * @author LILIANG
	 */
	@RequestMapping("/courseApplyTeacher.action")
	public  ModelAndView courseApplyTeacher(){
		ModelAndView mv = new ModelAndView("/course/client/course_applyTeacher");
		List<TagEntity> tags=tagService.searchTagAll();		
		mv.addObject("tags",tags);
		return mv;
	}
	
	/**
	 * 保存应聘讲师提交信息
	 * @author LILIANG
	 * @param TeacherApplyEntity
	 * @return 
	 */
	@RequestMapping("/saveApplyTeacher.action")
	@ResponseBody
	public  ResultBean saveApplyTeacher(TeacherApplyEntity teacherApplyEntity){
		return teacherApplyService.saveApplyTeacher(teacherApplyEntity);
	}
	
	/**
	 * 检索该教师审批状态
	 * @author LILIANG
	 * @param TeacherApplyEntity 
	 * @return 判断用户名是否存在 true存在/false不存在
	 */
	@RequestMapping("/searchTeacherStatus.action")
	@ResponseBody
	public  ResultBean searchTeacherStatus(TeacherApplyEntity teacherApplyEntity){
		ResultBean result = new ResultBean();
		result.setStatus(true);
		//判断用户名是否存在
		boolean teacherApplyStatus = teacherApplyService.searchTeacherApplyStatus(teacherApplyEntity);
		result.setStatus(!teacherApplyStatus);
		if(teacherApplyStatus){
			//teacherApplyEntity.setUserId(WebContext.getSessionUserId());
			//result.setMessages(getMessage("该用户教师审批在审批中/审批已通过", teacherApplyEntity.getUserId()));
			return result;
		}
		return result;
	}
	
	/**
	 * 后台-聘请讲师审批管理-初始化讲师审批列表
	 * @author LILIANG
	 * @param 无
	 * @return ModelAndView
	 */
	@RequestMapping("/init.action")
	@ResponseBody
	public ModelAndView init(){
		ModelAndView mv = new ModelAndView("course/manager/teacherApply_list");
		return mv;
	}
	
	/**
	 * 后台-聘请讲师审批管理-列表分页检索
	 * @author LILIANG
	 * @param TeacherApplyEntity 类型：TeacherApplyEntity
	 * @return 检索所有的教师信息
	 */
	@RequestMapping("/search.action")
	@ResponseBody
	public ResultBean search(TeacherApplyEntity teacherApplyEntity){
		return teacherApplyService.search(teacherApplyEntity);
	}
	
	/**
	 * 后台-聘请讲师审批管理-查看页面
	 * @author LILIANG
	 * @param id 类型：TeacherApplyEntity
	 * @return ModelAndView
	 */
	@RequestMapping("/view.action")
	@ResponseBody
	public ModelAndView view(@RequestParam("id")String id,@RequestParam("userId")String userId){
		ModelAndView mv = new ModelAndView("course/manager/teacherApply_view");
		TeacherApplyEntity teacherApplyEntity = teacherApplyService.searchDetail(id,userId);
		mv.addObject("teacherApplyEntity", teacherApplyEntity);
		return mv;
	}
	
	/**
	 * 后台-聘请讲师审批管理-审批页面初始化
	 * @author LILIANG
	 * @param id 类型：TeacherApplyEntity
	 * @return ModelAndView 
	 */
	@RequestMapping("/approveInit.action")
	@ResponseBody
	public ModelAndView approveInit(@RequestParam("id")String id,@RequestParam("userId")String userId){
		ModelAndView mv = new ModelAndView("course/manager/teacherApply_approve");
		TeacherApplyEntity teacherApplyEntity = teacherApplyService.searchDetail(id,userId);
		mv.addObject("teacherApplyEntity", teacherApplyEntity);
		return mv;
	}
	
	/**
	 * 后台-聘请讲师审批管理-审批同意
	 * @author LILIANG
	 * @param TeacherApplyEntity
	 * @return 
	 */
	@RequestMapping("/saveAgreeApprove.action")
	@ResponseBody
	public ResultBean saveAgreeApprove(TeacherApplyEntity teacherApplyEntity){
		ResultBean result = new ResultBean();	
		teacherApplyService.saveAgreeApprove(teacherApplyEntity);
		return result;
	}
	
	/**
	 * 后台-聘请讲师审批管理-审批不同意
	 * @author LILIANG
	 * @param TeacherApplyEntity
	 * @return 
	 */
	@RequestMapping("/saveDisAgreeApprove.action")
	@ResponseBody
	public ResultBean saveDisAgreeApprove(TeacherApplyEntity teacherApplyEntity){
		ResultBean result = new ResultBean();	
		teacherApplyService.saveDisAgreeApprove(teacherApplyEntity);
		return result;
	}
}
