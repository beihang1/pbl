package com.cloudsoaring.web.course.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.NoteDataEntity;
import com.cloudsoaring.web.course.service.NoteService;
/**
 * 笔记模块操作
 * @author fz
 *
 */
@Controller
@RequestMapping("/pub/note")
public class NoteController extends BaseController implements CourseConstants{
	@Autowired
	private NoteService noteService;
	
	/**
	 * 检索笔记列表
	 * @return
	 */
	@RequestMapping("/noteList.action")
	@ResponseBody
	public ResultBean noteList(NoteDataEntity nd){
		
		return noteService.noteList(nd);
	}
	
	/**
	 * 课程笔记：课程/章节笔记点赞
	 * @param id
	 * @return
	 */
	@RequestMapping("/noteClickLike.action")
	@ResponseBody
	public ResultBean noteClickLike(String id){
		
		return noteService.noteClickLike(id);
	}
	
	/**
	 * 课程/章节笔记采集
	 * @return
	 */
	@RequestMapping("/noteCollect.action")
	@ResponseBody
	public ResultBean noteCollect(String id){
		
		return noteService.noteCollect(id);
	}
	
	
	/**
	 * 课程/章节笔记取消采集
	 * @return
	 */
	@RequestMapping("/deleteNoteCollect.action")
	@ResponseBody
	public ResultBean deleteNoteCollect(String id){
		
		return noteService.deleteNoteCollect(id);
	}
	
	/**
	 * 我的笔记
	 * @param type
	 * 		笔记分类（我的笔记0，收藏的笔记1）
	 * @return 返回结果
	 */
	
	@RequestMapping("/userNoteList.action")
	@ResponseBody
	public ResultBean  userNoteList(NoteDataEntity nd,String type,Integer pageSize,Integer pageNumber){
		
		return noteService.userNoteList(nd, type,pageSize,pageNumber);
	}
	
	/**
	 * 删除我的笔记
	 * 
	 * @return
	 */
	
	@RequestMapping("/deleteUserNoteList.action")
	@ResponseBody
	public ResultBean deleteUserNoteList(String id){
		
		return noteService.deleteUserNoteList(id);
		
	}

	
	
	/**
	 * 删除笔记数据
	 * @param id 笔记数据ID
	 * @return
	 */
	@RequestMapping("/deleteNoteDataById.action")
	@ResponseBody
	public ResultBean deleteNoteData(String id){
		return noteService.deleteNoteById(id);
	}
	
	/***
	 * 新增笔记数据
	 * @param noteData 笔记数据
	 * @param linkId 链接ID
	 * @param linkType 链接类型
	 * @param title 笔记标题
	 * @return 
	 * @throws Exception 
	 */
	@RequestMapping("/saveNoteData.action")
	@ResponseBody
	/*public ResultBean saveNote(NoteDataEntity noteData,String linkId,String linkType,String title){
		return noteService.saveNote(noteData, linkId, linkType, title);
	}*/
	public ResultBean saveNote(NoteDataEntity noteData) throws Exception{
		return noteService.saveNote(noteData, noteData.getLinkId(), noteData.getLinkType(), noteData.getTitle());
	}

}
