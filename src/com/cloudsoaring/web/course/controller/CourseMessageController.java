package com.cloudsoaring.web.course.controller;

import java.io.FileNotFoundException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.UserEntity;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.CourseApproveEntity;
import com.cloudsoaring.web.course.entity.CourseMessageEntity;
import com.cloudsoaring.web.course.entity.ManagerUserEntity;
import com.cloudsoaring.web.course.entity.UserOperEntity;
import com.cloudsoaring.web.course.service.CourseService;
import com.cloudsoaring.web.course.service.ManagerUserService;
import com.cloudsoaring.web.course.view.CourseManagerView;
import com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity;

@Controller
@RequestMapping("/pub/courseMsg")
public class CourseMessageController extends BaseController implements CourseConstants {

	@Autowired
	private CourseService courseService;
	@Autowired
	private ManagerUserService managerUserService;
	/**
	 * 
	 * @return
	 */
	@RequestMapping("/userMsg.action")
	public ModelAndView userMsg() {
		ModelAndView result = new ModelAndView("/course/client/userMsg");
		result = getRoleAndNum(result);
		return result;
	}
	/**
	 * 
	 * @param entity
	 * @return
	 */
	@RequestMapping("/search.action")
	@ResponseBody
	public ResultBean search(CourseMessageEntity entity){
		return courseService.searchMsg4Page(entity);
	}
	/**
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping("/updateMsg.action")
	@ResponseBody
	public ResultBean updateMsg(String id) {
		ResultBean result = new ResultBean();
		courseService.updateMsg(id);
		result.setMessages(getMessage("更新成功"));
		return result;
	}
	/**
	 * 首页站内信
	 * @return
	 */
	@RequestMapping("/frontSearch.action")
	@ResponseBody
	public ResultBean frontSearch(){
		return courseService.searchMsgFrontPage();
	}
	/**
	 * 首页站全部内信
	 * @return
	 */
	@RequestMapping("/frontAllSearch.action")
	@ResponseBody
	public ResultBean frontAllSearch(){
		return courseService.searchMsgFrontAllPage();
	}
	/**
	 * 
	 * @return
	 */
	@RequestMapping("/userFrontMsg.action")
	public ModelAndView userFrontMsg() {
		ModelAndView result = new ModelAndView("/course/client/userFrontMsg");
		result = getRoleAndNum(result);
		return result;
	}
	/**
	 * 
	 * @param id
	 * @param checkType
	 * @param content
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/saveShare.action")
	@ResponseBody
    public ResultBean saveShare(String id,String checkType,String content) throws Exception  {
		CourseApproveEntity entity= courseService.selectApproveById(id);
		if(checkType.equals("2")) {
			courseService.updateApproveForCourse(entity,content);
			return new ResultBean();
		}else {
			CourseManagerView courseShare = new CourseManagerView();
			//CourseMessageEntity entity = courseService.selectMessageById(id);
			//CourseApproveEntity entity= courseService.selectApproveById(id);
			courseShare.setId(entity.getCourseId());
			courseShare.setShareFlag(COURSE_SHARE_FLAG_ONE);
			return  courseService.saveShare(courseShare,entity);
		}
    }
	/**
	 * 
	 * @param id
	 * @param checkType
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/saveLib.action")
	@ResponseBody
    public ResultBean saveLib(String id,String checkType) throws Exception  {
		if(checkType.equals("2")) {
			courseService.updateMsgForCourse(id);
			return new ResultBean();
		}else {
			CourseManagerView courseShare = new CourseManagerView();
			 CourseMessageEntity entity = courseService.selectMessageById(id);
			 courseShare.setId(entity.getCourseId());
			 courseShare.setLibraryFlag(COURSE_END_STATUS_YES);
			 return courseService.saveLib(courseShare,id);
		}
    }
	/**
	 * 
	 * @param id
	 * @param checkType
	 * @param content
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/saveFactory.action")
	@ResponseBody
    public ResultBean saveFactory(String id,String checkType,String content) throws Exception  {
		CourseApproveEntity entity= courseService.selectApproveById(id);
		if(checkType.equals("2")) {
			courseService.updateApproveForCourse(entity,content);
			return new ResultBean();
		}else {
			CourseManagerView courseShare = new CourseManagerView();
			//CourseMessageEntity entity = courseService.selectMessageById(id);
			//CourseApproveEntity entity= courseService.selectApproveById(id);
			courseShare.setId(entity.getCourseId());
			//courseShare.setShareFlag(COURSE_SHARE_FLAG_TWO);
			//courseShare.setStatus("1");
			return courseService.saveFactory(courseShare,entity);
		}
    }
	/**
	 * 
	 * @param id
	 * @param checkType
	 * @param content
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/savePublic.action")
	@ResponseBody
    public ResultBean savePublic(String id,String checkType,String content) throws Exception  {
		CourseApproveEntity entity= courseService.selectApproveById(id);
		if(checkType.equals("2")) {
			courseService.updateApproveForCourse(entity,content);
			return new ResultBean();
		}else {
			CourseManagerView courseShare = new CourseManagerView();
			//CourseMessageEntity entity = courseService.selectMessageById(id);
			//CourseApproveEntity entity= courseService.selectApproveById(id);
			courseShare.setId(entity.getCourseId());
			courseShare.setStatus("1");
			return  courseService.saveCoursePublic(courseShare,entity);
		}
    }
	/**
	 * 
	 * @param id
	 * @param checkType
	 * @return
	 */
	@RequestMapping("/saveApplyJoin.action")
	@ResponseBody
    public ResultBean saveApplyJoin(String id,String checkType)  {
		if(checkType.equals("2")) {
			courseService.updateMsgForCourse(id);
			courseService.saveCheckNo(id);
			return new ResultBean();
		}else {
			 CourseMessageEntity entity = courseService.selectMessageById(id);
			 return  courseService.joinCourse(entity.getCourseId(),id);
		}
    }
	/**
	 * 公共方法获取用户角色
	 * @param result
	 * @return
	 */
	private ModelAndView getRoleAndNum(ModelAndView result) {
    	int num = courseService.getStuStatus();
    	result.addObject("num", num);
    	UserEntity user = WebContext.getSessionUser();
    	//判断是否管理员
    	ManagerUserEntity entity = managerUserService.searchByUserId(user.getUserId());
    	if(!user.getUserName().equals("guest")) {
    		List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
    		//老师和教育局角色
    		if(roleList.size() > 0 && "A".equals(user.getUserKbn()) && null==entity && !"2542668".equals(user.getUserId())) {
    			for(UserRoleEntity role : roleList) {
    				if(role.getRoleId().equals("01")) {
    					result.addObject("role", "t");
    				}
    			}
    		//教育局角色	
    		}else if(roleList.size() > 0  && (null!=entity || "2542668".equals(user.getUserId()))){
    			result.addObject("role", "m");
    		}else if("B".equals(user.getUserKbn())){
    			result.addObject("role", "e");
    	    //家长和教研员角色
    		}else if("D".equals(user.getUserKbn()) || "E".equals(user.getUserKbn())){
    			result.addObject("role", "q");
    		//学生角色
    		}else {
    			result.addObject("role", "s");
    		}
    	}

    	if(null!=entity || "2542668".equals(user.getUserId())) {
    		result.addObject("manager", "1");
    	}else {
    		result.addObject("manager", "0");
    	}
    	return result;
    }
}
