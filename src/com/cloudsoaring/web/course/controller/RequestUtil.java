package com.cloudsoaring.web.course.controller;

import javax.servlet.http.HttpServletRequest;

import com.cloudsoaring.common.utils.StringUtil;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RequestUtil {
    /**
     * 接收同名参数
     * @param name
     * @param request
     * @return
     */
    public static String[] requestStringArr(String name, HttpServletRequest request){
        String[] list=  request.getParameterValues(name);
        if (list==null){
            return new String[]{};
        }
        return list;
    }
    public static String requestString(String name, HttpServletRequest request){
        String parameter = request.getParameter(name);
        return parameter;
    }
    public static Date requestDate(String dateStr,String format, HttpServletRequest request){
        String parameter = request.getParameter(dateStr);
        if (StringUtil.isEmpty(parameter)){
            return null;
        }

        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            Date  date = sdf.parse(parameter);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static int requestInt(String name, HttpServletRequest request){
        String parameter = request.getParameter(name);
        if (StringUtil.isEmpty(parameter)){
            return 0;
        }
        return Integer.parseInt(parameter);
    }
    public static boolean requestBoolean(String name,HttpServletRequest request){
        String parameter = request.getParameter(name);
        if (StringUtil.isEmpty(parameter)){
            return false;
        }
        return Boolean.parseBoolean(parameter);
    }
    public static BigDecimal requestDecimal(String name,HttpServletRequest request){
        String parameter = request.getParameter(name);
        if (StringUtil.isEmpty(parameter)){
            return new BigDecimal(0);
        }
          return new BigDecimal(parameter);
    }


    /**
     * android : 所有android设备 mac os : iphone ipad windows
     * phone:Nokia等windows系统的手机
     * @param request
     * @return
     */
    public static boolean isMobileRequest(HttpServletRequest request) {
        String requestHeader = request.getHeader("user-agent").toLowerCase();
        String[] deviceArray = new String[]{"android", "iphone", "ios", "windows phone"};
        if (requestHeader == null) {
            return false;
        }
        requestHeader = requestHeader.toLowerCase();
        for (int i = 0; i < deviceArray.length; i++) {
            if (requestHeader.indexOf(deviceArray[i]) > 0) {
                return true;
            }
        }
        return false;
    }


}
