package com.cloudsoaring.web.course.controller;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.CourseExperimentalResultEntity;
import com.cloudsoaring.web.course.entity.ModelInfo;
import com.cloudsoaring.web.course.entity.SeriesForm;
import com.cloudsoaring.web.course.service.ChapterService;
import com.cloudsoaring.web.course.service.CourseService;
import com.cloudsoaring.web.course.service.ModelInfoService;
import com.cloudsoaring.web.course.view.ChapterView;
import com.cloudsoaring.web.course.view.CourseView;
import com.cloudsoaring.web.course.view.HomeworkView;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 
 * @author fanglili
 * @date 2019-03-13
 */
@Controller
@RequestMapping("/pub/courseER")
@ControllerAdvice
public class CourseERController extends BaseController implements CourseConstants {
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Autowired
	 private ModelInfoService modelInfoService;
	@Autowired
	private CourseService courseService;
	@Autowired
	private ChapterService chapterService;
	
	

	
	/**
	 * 获取成绩详情页面
	 * 
	 * @return ModelAndView
	 */
	@RequestMapping("/getUserScoreDetail.action")
	public ModelAndView getUserScoreDetail(@RequestParam("userId") String userId,@RequestParam("enterTime") String enterTime,@RequestParam("courseId") String courseId,@RequestParam("chapterId") String chapterId) {
		ModelAndView mv = new ModelAndView("/course/user/user_detail");
		ResultBean result = courseService.searchCourseDetail(courseId);
		CourseView courseView = (CourseView) result.getData();
		mv.addObject("courseView", courseView);
		result = chapterService.chapterDetail(courseId, chapterId);
		ChapterView chapterView = (ChapterView) result.getData();
		mv.addObject("chapterView", chapterView);
		return mv;
	}
	
	

	/**
	 * 获取当前登录用户Id
	 * @return JSONArray
	 */
	@RequestMapping("/getLoginUserId.action")
	@ResponseBody
	public JSONArray getLoginUserId() {
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		String userId = WebContext.getSessionUserId();
		jo.put("userId", userId);
		json.add(jo);
		return json;
	}
	
	/**
	 * 保存unity动画信息-webGL
	 * @return JSONArray
	 */
	@RequestMapping("/saveWorkspaceModel.action")
	@ResponseBody
	public JSONArray saveWorkspaceModel(String modelData,String modelId,String modelName) {
		JSONArray jsonReturn = new JSONArray();
		JSONObject jo = new JSONObject();
		if(modelData != null && modelData.length() > 0) {
			JSONObject json = JSONObject.fromObject(modelData);
			//String modelInfoDetail = json.get("modelInfo").toString();
			//String  modelId = json.get("modelId").toString();
			//String modelName = json.get("modelName").toString();
			ModelInfo modelInfo = new ModelInfo();
			modelInfo.setModelInfo(modelData);
			modelInfo.setModelName(modelName);
			/**
			 *  根据modelId判断是修改还是新增
			 * 新增返回modelId
			 */
			if(modelId != null && !"".equals(modelId)) {
				modelInfo.setModelId(modelId);
				modelInfoService.updateModelInfo(modelInfo);
				jo.put("modelId",modelId);
				jsonReturn.add(jo);
				return jsonReturn;
			}else {
				jo.put("modelId",modelInfoService.insertModelInfo(modelInfo));
				jsonReturn.add(jo);
				return jsonReturn;
			}
		}else {
			jo.put("error", "error");
			jsonReturn.add(jo);
			return jsonReturn;
		}
	}
	
	/**
	 * 保存unity动画信息
	 * @return JSONArray
	 */
	@RequestMapping("/saveWorkspaceModelPC.action")
	@ResponseBody
	public JSONArray saveWorkspaceModelPC(String modelData,String modelId,String modelName,String userId) {
		JSONArray jsonReturn = new JSONArray();
		JSONObject jo = new JSONObject();
		if(modelData != null && modelData.length() > 0) {
			JSONObject json = JSONObject.fromObject(modelData);
			//String modelInfoDetail = json.get("modelInfo").toString();
			//String  modelId = json.get("modelId").toString();
			//String modelName = json.get("modelName").toString();
			ModelInfo modelInfo = new ModelInfo();
			modelInfo.setModelInfo(modelData);
			modelInfo.setModelName(modelName);
			/**
			 *  根据modelId判断是修改还是新增
			 * 新增返回modelId
			 */
			if(modelId != null && !"".equals(modelId)) {
				modelInfo.setModelId(modelId);
				modelInfoService.updateModelInfo(modelInfo);
				jo.put("modelId",modelId);
				jsonReturn.add(jo);
				return jsonReturn;
			}else {
				modelInfo.setCreateUser(userId);
				jo.put("modelId",modelInfoService.insertModelInfoPC(modelInfo));
				jsonReturn.add(jo);
				return jsonReturn;
			}
		}else {
			jo.put("error", "error");
			jsonReturn.add(jo);
			return jsonReturn;
		}
	}
	
	/**
	 * 获取unity动画信息
	 * @return String
	 */
	@RequestMapping("/getWorkspaceModel.action")
	@ResponseBody
	public JSONObject getWorkspaceModel(String modelId) {
		JSONObject jo = new JSONObject();
		ModelInfo modelInfo =  modelInfoService.getModelInfo(modelId);
		if(modelInfo != null) {
			jo.put("modelInfo", modelInfo.getModelInfo());
			jo.put("modelId", modelInfo.getModelId());
			return jo;
		}else {
			jo.put("error", "error");
			return jo;
		}
	}
}
