package com.cloudsoaring.web.course.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.ModelInfo;
import com.cloudsoaring.web.course.entity.SceneInfo;
import com.cloudsoaring.web.course.entity.SceneInfoBean;
import com.cloudsoaring.web.course.service.ModelInfoService;
import com.cloudsoaring.web.course.service.SceneInfoService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/***
 * 创客编程模块相关请求、响应控制的Controller

 */
@Controller
@RequestMapping("/pub/sceneInfo")
public class SceneInfoController extends BaseController implements CourseConstants {
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	@Autowired
	private ModelInfoService modelInfoService;
	
	 /**
     * 	检索unity列表-web端创客编程
     * @return JSONArray
     */
    @RequestMapping("/listSearch.action")
    @ResponseBody
    public JSONArray listSearch() {
    	JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		List<SceneInfoBean> returnList = new ArrayList<SceneInfoBean>();
		List<ModelInfo> modelInfoList = modelInfoService.getModelInfoByUserId();
		if(modelInfoList.size() > 0) {
			for(ModelInfo info : modelInfoList) {
				SceneInfoBean entity = new SceneInfoBean();
				entity.setId(info.getModelId());
				entity.setSceneId(info.getModelName());
				entity.setCreateUser(info.getCreateUser());
				entity.setCreateDate(sdf.format(info.getCreateDate()));
				returnList.add(entity);
			}
		}
		jo.put("returnList", returnList);
		json.add(jo);
		return json;
    }
    
    /**
     * 	检索unity列表-单机版创客编程
     * @return JSONArray
     */
    @RequestMapping("/listSearchToUnity.action")
    @ResponseBody
    public JSONArray listSearchToUnity(String userId) {
    	JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		List<SceneInfoBean> returnList = new ArrayList<SceneInfoBean>();
		List<ModelInfo> modelInfoList = modelInfoService.getModelInfoByUserIdToUnity(userId);
		if(modelInfoList.size() > 0) {
			for(ModelInfo info : modelInfoList) {
				SceneInfoBean entity = new SceneInfoBean();
				entity.setId(info.getModelId());
				entity.setSceneId(info.getModelName());
				entity.setCreateUser(info.getCreateUser());
				entity.setCreateDate(sdf.format(info.getCreateDate()));
				returnList.add(entity);
			}
		}
		jo.put("returnList", returnList);
		json.add(jo);
		return json;
    }
    
    /**
     * 	检查场景名称的唯一性
     * @return String
     */
    @RequestMapping("/checkModelNameUnique.action")
    @ResponseBody
    public JSONArray checkModelNameUnique(String modelName) {
    	JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		jo.put("checkInfo", modelInfoService.checkModelNameUnique(modelName));
		json.add(jo);
		return json;
    }
    
    /**
     * 	删除场景信息
     * @return String
     */
    @RequestMapping("/delete.action")
    @ResponseBody
    public JSONArray delete(String modelId) {
    	JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		jo.put("flag", modelInfoService.delete(modelId));
		json.add(jo);
		return json;
    }
}
