package com.cloudsoaring.web.course.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.service.VoteService;
/**
 * 投票相关
 * @author liuyanshuang
 *
 */
@Controller
@RequestMapping("/pub/vote")
public class VoteController extends BaseController{

	@Autowired
	private VoteService voteService;
	
	/**
	 * 提交投票信息
	 * @param linkId 链接ID
	 * @param voteId 投票选项ID 选项：好评4、中评2、差评1
	 * @param linkType 链接类型，（课程、章节）
	 * @return ResultBean
	 */
	@RequestMapping("/submitVote.action")
	@ResponseBody
	public ResultBean submitVote(String linkId,String voteId,String linkType){
		return voteService.submitVote(linkId,voteId,linkType);
	}
	
	/**
	 * 投票详情
	 * @author heyaqin
	 * @param linkId 链接ID
	 * @param linkType  链接类型
	 * @return
	 */
	@RequestMapping("/voteDetail.action")
	@ResponseBody
	public ResultBean voteDetail(String linkId,String linkType){
		return voteService.voteDetail(linkId,linkType);
	}
}
