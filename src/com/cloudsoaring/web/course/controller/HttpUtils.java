package com.cloudsoaring.web.course.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.jasig.cas.client.util.URIBuilder;
import org.jasig.cas.client.util.URIBuilder.BasicNameValuePair;
import org.springframework.http.HttpEntity;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import sun.misc.BASE64Encoder;
/**
 * 公共工具类
 * http的get和post请求
 */
public class HttpUtils {
	/**
	 * get请求
	 */
	public static String sendGet(String url, String param) {
		String result = "";
		BufferedReader in = null;
		try {
			String urlNameString = url + "?" + param;
			URL realUrl = new URL(urlNameString);
			// 打开和URL之间的连接
			URLConnection connection = realUrl.openConnection();
			// 设置通用的请求属性
			connection.setRequestProperty("accept", "*/*");
			connection.setRequestProperty("Accept-Charset", "UTF-8");
			connection.setRequestProperty("connection", "Keep-Alive");
			// connection.setRequestProperty("user-agent","Mozilla/4.0 (compatible; MSIE
			// 6.0; Windows NT 5.1;SV1)");
			// 建立实际的连接
			connection.connect();
			// 获取所有响应头字段
			Map<String, List<String>> map = connection.getHeaderFields();
			// 遍历所有的响应头字段
			for (String key : map.keySet()) {
				System.out.println(key + "--->" + map.get(key));
			}
			// 定义 BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				result += line;
			}
		} catch (Exception e) {
			System.out.println("发送GET请求出现异常！" + e);
			e.printStackTrace();
		}
		// 使用finally块来关闭输入流
		finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (Exception e2) {
				e2.printStackTrace();
			}
		}
		return result;
	}
    /**
     * post请求
     * 
     * */
	public static String sendPost(String url,String param) throws UnsupportedEncodingException {
		PrintWriter out = null;
		BufferedReader in = null;
		String result = "";
		param = URLEncoder.encode(param,"utf-8");
		try {
			URL realUrl = new URL(url);
			// 打开和URL之间的连接
			URLConnection conn = realUrl.openConnection();
			// 设置通用的请求属性
			conn.setRequestProperty("accept", "*/*");
			conn.setRequestProperty("Accept-Charset", "UTF-8");
			conn.setRequestProperty("connection", "Keep-Alive");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("user-agent","Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			// 发送POST请求必须设置如下两行
			conn.setDoOutput(true);
			conn.setDoInput(true);
			// 获取URLConnection对象对应的输出流
			out = new PrintWriter(conn.getOutputStream());
			// 发送请求参数
			out.print(param);
			// flush输出流的缓冲
			out.flush();
			// 定义BufferedReader输入流来读取URL的响应
			in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				result += new String(line.getBytes(), "utf-8");
			}
		} catch (Exception e) {
			System.out.println("发送 POST 请求出现异常！" + e);
			e.printStackTrace();
		}
		// 使用finally块来关闭输出流、输入流
		finally {
			try {
				if (out != null) {
					out.close();
				}
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return result;
	}
	 /**
     * PostMethod的post请求
     * 
     * */
	public static String sendFormDataPost(String url,String param,String clientId) {
		PostMethod method = null;
        HttpClient client = new HttpClient();
        int statusCode=0;
        client.getParams().setParameter("Accept-Charset", "UTF-8");// 指定传送字符集为gb2312格式
        client.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "utf-8");// 指定传送字符集为gb2312格式
        client.getHttpConnectionManager().getParams().setConnectionTimeout(30000);// 设置连接超时时间为30秒（连接初始化时间）
        String str ="";
        try{
            method = new PostMethod(
             url+"?access_token="+param+"&tenantId="+clientId);
            method.setRequestHeader("ContentType", "application/x-www-form-urlencoded;charset=UTF-8");
            statusCode = client.executeMethod(method);
            if(200==statusCode) {
            	// 读取内容
                byte[] responseBody = method.getResponseBody();

                // 处理内
                str = new String(responseBody,"utf-8");
            }
            
        }catch(Exception e){
         System.out.println(e.getMessage());
        }
		return str;
	}
	/**
	 * 获取用户详细角色信息
	 * @param url
	 * @param param
	 * @param clientId
	 * @return
	 */
	public static String sendFormDataDetailPost(String url,String param,String tenantId,String role) {
		PostMethod method = null;
        HttpClient client = new HttpClient();
        int statusCode=0;
        client.getParams().setParameter("Accept-Charset", "UTF-8");// 指定传送字符集为gb2312格式
        client.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "utf-8");// 指定传送字符集为gb2312格式
        client.getHttpConnectionManager().getParams().setConnectionTimeout(30000);// 设置连接超时时间为30秒（连接初始化时间）
        String str ="";
        try{
            method = new PostMethod(
             url+"?access_token="+param+"&tenantId="+tenantId+"&roleId="+role);
            method.setRequestHeader("ContentType", "application/x-www-form-urlencoded;charset=UTF-8");
            statusCode = client.executeMethod(method);
            if(200==statusCode) {
            	// 读取内容
                byte[] responseBody = method.getResponseBody();

                // 处理内
                str = new String(responseBody,"utf-8");
            }
            
        }catch(Exception e){
         System.out.println(e.getMessage());
        }
		return str;
	}
	/**
	 * 有权限的post请求
	 * */
	public static String sendAuthorDataPost(String url,String clientId,String client_secret) {
		PostMethod method = null;
        HttpClient client = new HttpClient();
        int statusCode=0;
   	    BASE64Encoder enc = new BASE64Encoder();
	    String userpassword = clientId + ":" + client_secret;
	    String encodedAuthorization ="Basic "+ enc.encode(userpassword.getBytes());
        client.getParams().setParameter("Accept-Charset", "UTF-8");// 指定传送字符集为gb2312格式
        client.getHttpConnectionManager().getParams().setConnectionTimeout(30000);// 设置连接超时时间为30秒（连接初始化时间）
        String str ="";
        try{
            method = new PostMethod(
             url+"?scope=read&grant_type=client_credentials");
            method.setRequestHeader("ContentType", "application/x-www-form-urlencoded;charset=UTF-8");
            method.setRequestHeader("Authorization",encodedAuthorization);
            statusCode = client.executeMethod(method);
            if(200==statusCode) {
            	// 读取内容
                byte[] responseBody = method.getResponseBody();

                // 处理内
                str = new String(responseBody,"utf-8");
            }
            
        }catch(Exception e){
         System.out.println(e.getMessage());
        }
		return str;
	}
	public static String sendFormEvalutionPost(String url,String param) {
		PostMethod method = null;
        HttpClient client = new HttpClient();
        int statusCode=0;
        client.getParams().setParameter("Accept-Charset", "UTF-8");// 指定传送字符集为gb2312格式
        client.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "utf-8");// 指定传送字符集为gb2312格式
        client.getHttpConnectionManager().getParams().setConnectionTimeout(30000);// 设置连接超时时间为30秒（连接初始化时间）
        String str ="";
        try{
            method = new PostMethod(
             url+"?"+param);
            method.setRequestHeader("ContentType", "application/x-www-form-urlencoded;charset=UTF-8");
            statusCode = client.executeMethod(method);
            if(200==statusCode) {
            	// 读取内容
                byte[] responseBody = method.getResponseBody();

                // 处理内
                str = new String(responseBody,"utf-8");
            }
            
        }catch(Exception e){
         System.out.println(e.getMessage());
        }
		return str;
	}
	/**
	 * 
	 * @param url
	 * @param param
	 * @param clientId
	 * @return
	 */
	public static String sendFormGradeDataPost(String url,String param,String schoolId,String clientId) {
		PostMethod method = null;
        HttpClient client = new HttpClient();
        int statusCode=0;
        client.getParams().setParameter("Accept-Charset", "UTF-8");// 指定传送字符集为gb2312格式
        client.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "utf-8");// 指定传送字符集为gb2312格式
        client.getHttpConnectionManager().getParams().setConnectionTimeout(30000);// 设置连接超时时间为30秒（连接初始化时间）
        String str ="";
        try{
            method = new PostMethod(
             url+"?access_token="+param+"&schoolId="+schoolId+"&tenantId="+clientId);
            method.setRequestHeader("ContentType", "application/x-www-form-urlencoded;charset=UTF-8");
            statusCode = client.executeMethod(method);
            if(200==statusCode) {
            	// 读取内容
                byte[] responseBody = method.getResponseBody();

                // 处理内
                str = new String(responseBody,"utf-8");
            }
            
        }catch(Exception e){
         System.out.println(e.getMessage());
        }
		return str;
	}
	public static String sendFormClassDataPost(String url, String param, String gradeId, String tenantId) {
		PostMethod method = null;
        HttpClient client = new HttpClient();
        int statusCode=0;
        client.getParams().setParameter("Accept-Charset", "UTF-8");// 指定传送字符集为gb2312格式
        client.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "utf-8");// 指定传送字符集为gb2312格式
        client.getHttpConnectionManager().getParams().setConnectionTimeout(30000);// 设置连接超时时间为30秒（连接初始化时间）
        String str ="";
        try{
            method = new PostMethod(
             url+"?access_token="+param+"&gradeId="+gradeId+"&tenantId="+tenantId);
            method.setRequestHeader("ContentType", "application/x-www-form-urlencoded;charset=UTF-8");
            statusCode = client.executeMethod(method);
            if(200==statusCode) {
            	// 读取内容
                byte[] responseBody = method.getResponseBody();

                // 处理内
                str = new String(responseBody,"utf-8");
            }
            
        }catch(Exception e){
         System.out.println(e.getMessage());
        }
		return str;
	}
	public static String sendFormClassInfoDataPost(String url, String param, String classId, String tenantId) {
		PostMethod method = null;
        HttpClient client = new HttpClient();
        int statusCode=0;
        client.getParams().setParameter("Accept-Charset", "UTF-8");// 指定传送字符集为gb2312格式
        client.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "utf-8");// 指定传送字符集为gb2312格式
        client.getHttpConnectionManager().getParams().setConnectionTimeout(30000);// 设置连接超时时间为30秒（连接初始化时间）
        String str ="";
        try{
            method = new PostMethod(
             url+"?access_token="+param+"&classId="+classId+"&tenantId="+tenantId);
            method.setRequestHeader("ContentType", "application/x-www-form-urlencoded;charset=UTF-8");
            statusCode = client.executeMethod(method);
            if(200==statusCode) {
            	// 读取内容
                byte[] responseBody = method.getResponseBody();

                // 处理内
                str = new String(responseBody,"utf-8");
            }
            
        }catch(Exception e){
         System.out.println(e.getMessage());
        }
		return str;
	}
	 /**
     * PostMethod的post请求
     * 
     * */
	public static String sendFormDataGet(String url,String param,String clientId,String userIds,int page,int pageSize) {
		GetMethod method = null;
        HttpClient client = new HttpClient();
        int statusCode=0;
        client.getParams().setParameter("Accept-Charset", "UTF-8");// 指定传送字符集为gb2312格式
        client.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET, "utf-8");// 指定传送字符集为gb2312格式
        client.getHttpConnectionManager().getParams().setConnectionTimeout(30000);// 设置连接超时时间为30秒（连接初始化时间）
        String str ="";
        try{
            method = new GetMethod(
             url+"?access_token="+param+"&tenantId="+clientId+"&userIds="+userIds+"&page="+page+"&pageSize="+pageSize);
            method.setRequestHeader("ContentType", "application/x-www-form-urlencoded;charset=UTF-8");
            statusCode = client.executeMethod(method);
            if(200==statusCode) {
            	// 读取内容
                byte[] responseBody = method.getResponseBody();

                // 处理内
                str = new String(responseBody,"utf-8");
            }
            
        }catch(Exception e){
         System.out.println(e.getMessage());
        }
		return str;
	}

}
