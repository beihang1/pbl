package com.cloudsoaring.web.course.controller;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.CourseExperimentalResultEntity;
import com.cloudsoaring.web.course.entity.ModelInfo;
import com.cloudsoaring.web.course.entity.SeriesForm;
import com.cloudsoaring.web.course.entity.SysUserEntity;
import com.cloudsoaring.web.course.entity.UserLogEntity;
import com.cloudsoaring.web.course.service.ArtCraftsCourseService;
import com.cloudsoaring.web.course.service.ChapterService;
import com.cloudsoaring.web.course.service.CourseService;
import com.cloudsoaring.web.course.service.ModelInfoService;
import com.cloudsoaring.web.course.view.ChapterView;
import com.cloudsoaring.web.course.view.CourseView;
import com.cloudsoaring.web.course.view.HomeworkView;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 
 * @author fanglili
 */
@Controller
@RequestMapping("/pub/courseAC")
@ControllerAdvice
public class CourseACController extends BaseController implements CourseConstants {
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Autowired
	private ArtCraftsCourseService acCourseService;
	
	/**
	 * 绢人一
	 */
	@RequestMapping("/getUserOperation.action")
	@ResponseBody
	public JSONArray getUserOperation() {
		//声明json集合对象和数组
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		List<CourseExperimentalResultEntity> courseERList = acCourseService.getUserAllStepScore();
		if (courseERList.size() > 0) {
			for(CourseExperimentalResultEntity courseER : courseERList) {
				Map<String, Object> courseERMap = new  HashMap<String,Object>();
				Date entetTime = courseER.getEnterTime();
				courseERMap.put("enterTime", sdf.format(entetTime));
				courseERMap.put("userName", courseER.getUserName());
				courseERMap.put("userId", courseER.getUserId());
				courseERMap.put("operationItem", courseER.getFirstStepName());
				courseERMap.put("standardOperation", courseER.getStandardOperation()== null?"--":courseER.getStandardOperation());
				courseERMap.put("actualOperation", courseER.getActualOperation()== null?"--":courseER.getActualOperation());
				BigDecimal score = new BigDecimal(courseER.getScore().toString()).setScale(2, BigDecimal.ROUND_HALF_UP);
				courseERMap.put("score",score);
				//分数在60以下判断
				if(score.doubleValue() < 60){
					courseERMap.put("appraise", "综合评价：差。整体知识掌握水平差；操作不流畅，需多加练习，继续学习基础知识。");
				//分数在60-70之间判断
				}else if(score.doubleValue() >= 60 && score.doubleValue() <70){
					courseERMap.put("appraise", "综合评价：良。基础知识掌握不牢固；操作步骤需要加强练习；注意提高思考能力。");
				//分数在70-90之间判断
				}else if(score.doubleValue() >= 70 && score.doubleValue() <90){
					courseERMap.put("appraise", "综合评价：优秀。基础知识需要进一步巩固加强；操作总体顺畅，需要加强熟练程度。 优秀！");
				//	分数在90-100之间判断
				}else if(score.doubleValue() >= 90 && score.doubleValue() <= 100){
					courseERMap.put("appraise", "综合评价：非常优秀。知识掌握明确，操作能力强。表现非常优秀！");
				}
				resultList.add(courseERMap);
				}
				
		}
		jo.put("resultList", resultList);
		json.add(jo);
		return json;
	}
	/**
	 * 
	 * @return
	 */
	@RequestMapping("/getSysUser.action")
	@ResponseBody
	public JSONArray getSysUser() {
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		List<SysUserEntity> courseERList = acCourseService.getSysUser();
		if (courseERList.size() > 0) {
			for(SysUserEntity courseER : courseERList) {
				Map<String, Object> courseERMap = new  HashMap<String,Object>();
				Date entetTime = courseER.getSystemTime();
				courseERMap.put("systemTime", sdf.format(entetTime));
				courseERMap.put("id", courseER.getId() == null?"--":courseER.getId());
				courseERMap.put("userName", courseER.getUserName() == null?"--":courseER.getUserName());
				courseERMap.put("proName", courseER.getProName() == null?"--":courseER.getProName());
				courseERMap.put("operIp", courseER.getOperIp() == null?"--":courseER.getOperIp());
				resultList.add(courseERMap);
				}
				
		}
		jo.put("resultList", resultList);
		json.add(jo);
		return json;
	}
	/**
	 * 
	 * @return
	 */
	@RequestMapping("/getUserLog.action")
	@ResponseBody
	public JSONArray getUserLog() {
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		List<UserLogEntity> courseERList = acCourseService.getUserLog();
		if (courseERList.size() > 0) {
			for(UserLogEntity courseER : courseERList) {
				Map<String, Object> courseERMap = new  HashMap<String,Object>();
				Date entetTime = courseER.getOperTime();
				courseERMap.put("systemTime", sdf.format(entetTime));
				courseERMap.put("id", courseER.getId() == null?"--":courseER.getId());
				courseERMap.put("userName", courseER.getUserName() == null?"--":courseER.getUserName());
				courseERMap.put("operIp", courseER.getOperIP() == null?"--":courseER.getOperIP());
				courseERMap.put("logType", courseER.getLogType() == null?"--":courseER.getLogType());
				resultList.add(courseERMap);
				}
				
		}
		jo.put("resultList", resultList);
		json.add(jo);
		return json;
	}
}
