package com.cloudsoaring.web.course.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * 日期转化格式
 * @author Admin
 *
 */
public class DateUtil {
    public static String toStringFormat(Date dt,String format){

        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(dt);
    }
}
