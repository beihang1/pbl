package com.cloudsoaring.web.course.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.service.QuestionService;

/**
 * 测试，问卷
 * 
 * @author zhdeng
 *
 */
@Controller
@RequestMapping("/pub/question")
public class QuestionController {
	
	@Autowired
	QuestionService questionService;
	/**
	 * 问卷题目选项
	 */
	@RequestMapping("/questionnaireDetial.action")
	@ResponseBody
	public ResultBean questionnaireDetial(String linkId,String linkType) {
		return questionService.questionnaireDetial(  linkId,  linkType);
	}
	
	/**
	 * 问卷提交
	 */
	@RequestMapping("/submitQuestionnaire.action")
	@ResponseBody
	public ResultBean submitQuestionnaire(String questionIds, String optionIds,	String measureId) {
		return questionService.submitQuestionnaire( questionIds,optionIds,measureId);
	}
}
