package com.cloudsoaring.web.course.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.service.MeasuermentService;


/**
 * 前台测试信息
 * @author JGJ
 *
 */
@Controller
@RequestMapping("/pub/measuerment")
public class MeasuermentController {
	@Autowired
	private MeasuermentService measuermentService;
	
	/**
	 * 检索测试信息（题目及选项列表、正确答案）
	 * @param linkId
	 * @param linkType
	 * @return
	 */
	@RequestMapping("/searchMeasuerment.action")
	@ResponseBody
	public ResultBean searchMeasuerment(String linkId,String linkType){
		
		return measuermentService.searchMeasuerment(linkId, linkType);
	}
	
	/**
	 *  测试/考试提交
	 * @param measureId
	 * @param ids
	 * @param userAnswerIds
	 * @param submitType
	 * @return
	 */
	@RequestMapping("/submitMeasuerment.action")
	@ResponseBody
	public ResultBean submitMeasuerment(String linkType,String measureId, String ids, String userAnswerIds, @RequestParam(required=false)String submitType){
		
		return  measuermentService.submitMeasuerment(linkType,measureId, ids,userAnswerIds,submitType);
	}
	
	/**
	 * 考试开始
	 * @param linkId 链接ID，章节/课程ID
	 * @param linkType 章节/课程 CHAPTER/COURSE
	 * @return 返回结果
	 */
	@RequestMapping("/examstart.action")
	@ResponseBody
	public ResultBean examStart(String linkId,String linkType) {
		return measuermentService.examStart(linkId,linkType);
	}
	
	/**
	 * 考试题列表
	 * @param linkId 链接ID，章节/课程ID
	 * @param linkType 章节/课程 CHAPTER/COURSE
	 * @return 返回结果
	 */
	@RequestMapping("/examgetlist.action")
	@ResponseBody
	public ResultBean examGetList(String linkId,String linkType) {
		return measuermentService.getExamList(linkId,linkType);
	}
}
