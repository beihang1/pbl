package com.cloudsoaring.web.course.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.common.excel.ExcelDataBind;
import com.cloudsoaring.common.utils.DateUtil;
import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.MD5Util;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.exception.BusinessException;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.FileEntity;
import com.cloudsoaring.web.bus.entity.UserEntity;
import com.cloudsoaring.web.bus.service.UserService;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.entity.BaseEntity;
import com.cloudsoaring.web.common.utils.ConfigUtil;
import com.cloudsoaring.web.common.utils.FileUtil;
import com.cloudsoaring.web.common.utils.JSONUtil;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.common.view.ResultBean.ErrorCode;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.ArchivesDiscussEntity;
import com.cloudsoaring.web.course.entity.ArchivesEntity;
import com.cloudsoaring.web.course.entity.ChapterActiveEntity;
import com.cloudsoaring.web.course.entity.ChapterEntity;
import com.cloudsoaring.web.course.entity.CourseApproveEntity;
import com.cloudsoaring.web.course.entity.CourseEntity;
import com.cloudsoaring.web.course.entity.CourseMessageEntity;
import com.cloudsoaring.web.course.entity.HomeworkAnswerEntity;
import com.cloudsoaring.web.course.entity.HomeworkAnswerQuestionEntity;
import com.cloudsoaring.web.course.entity.HomeworkEntity;
import com.cloudsoaring.web.course.entity.ManagerUserEntity;
import com.cloudsoaring.web.course.entity.MeasurementEntity;
import com.cloudsoaring.web.course.entity.NoteDataEntity;
import com.cloudsoaring.web.course.entity.QuestionEntity;
import com.cloudsoaring.web.course.entity.Questionnaire;
import com.cloudsoaring.web.course.entity.ScoreDataEntity;
import com.cloudsoaring.web.course.entity.ScoreEntity;
import com.cloudsoaring.web.course.entity.UserCourseEntity;
import com.cloudsoaring.web.course.entity.UserLogEntity;
import com.cloudsoaring.web.course.entity.UserMeasurementResultEntity;
import com.cloudsoaring.web.course.entity.UserOperEntity;
import com.cloudsoaring.web.course.service.ChapterActiveService;
import com.cloudsoaring.web.course.service.ChapterService;
import com.cloudsoaring.web.course.service.CourseService;
import com.cloudsoaring.web.course.service.CourseVRService;
import com.cloudsoaring.web.course.service.ManagerUserService;
import com.cloudsoaring.web.course.service.MeasuermentService;
import com.cloudsoaring.web.course.service.TagService;
import com.cloudsoaring.web.course.view.ChapterView;
import com.cloudsoaring.web.course.view.CourseManagerView;
import com.cloudsoaring.web.course.view.CourseView;
import com.cloudsoaring.web.course.view.HomeworkView;
import com.cloudsoaring.web.course.view.InstitutionManagerView;
import com.cloudsoaring.web.course.view.TagView;
import com.cloudsoaring.web.course.view.UserView;
import com.cloudsoaring.web.institution.entity.InstitutionEntity;
import com.cloudsoaring.web.institution.entity.InstitutionMessageEntity;
import com.cloudsoaring.web.institution.entity.InstitutionTeacherDutyEntity;
import com.cloudsoaring.web.institution.entity.MeasurementInstitutionEntity;
import com.cloudsoaring.web.institution.entity.QuestionInstitutionEntity;
import com.cloudsoaring.web.institution.entity.QuestionOptionInstitutionEntity;
import com.cloudsoaring.web.institution.entity.TagInstitutionEntity;
import com.cloudsoaring.web.institution.entity.TagLinkInstitutionEntity;
import com.cloudsoaring.web.institution.service.InstitutionService;
import com.cloudsoaring.web.institution.service.MeasuermentInstitutionService;
import com.cloudsoaring.web.institution.service.QuestionInstitutionService;
import com.cloudsoaring.web.institution.service.TagInstitutionEvalutionService;
import com.cloudsoaring.web.institution.service.TagInstitutionService;
import com.cloudsoaring.web.institution.service.TaxonomyInstitutionService;
import com.cloudsoaring.web.myinfo.entity.StudentClassInfoEntity;
import com.cloudsoaring.web.myinfo.entity.TeacherClassInfoEntity;
import com.cloudsoaring.web.myinfo.service.StudentClassInfoService;
import com.cloudsoaring.web.myinfo.service.TeacherClassInfoService;
import com.cloudsoaring.web.points.entity.UserPoint;
import com.cloudsoaring.web.taxonomy.entity.TagInstitutionEntityView;
import com.cloudsoaring.web.taxonomy.entity.TagInstitutionView;
import com.cloudsoaring.web.taxonomy.entity.TagTypeInstitutionView;
import com.cloudsoaring.web.taxonomy.entity.TbInstitutionEvalutionManagerView;
import com.cloudsoaring.web.trainingplatform.constants.TpStateConstants;
import com.cloudsoaring.web.trainingplatform.entity.TpUserEntity;
import com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity;
import com.cloudsoaring.web.trainingplatform.service.UserManagementService;
import com.eggtwo.euq.service.ExamToWebserviceProxy;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/***
 * 课程模块相关请求、响应控制的Controller
 * 
 * @author liuyanshuang
 *
 */
@Controller
//@RequestMapping("/pub/course")
public class ManagerCodeController extends BaseController implements CourseConstants {

 	/*private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	@Autowired
	private CourseService courseService;
	@Autowired
	private ChapterService chapterService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserManagementService userManagementService;
	@Autowired
	private TaxonomyInstitutionService taxonomyInstitutionService;
	@Autowired
	private TagInstitutionEvalutionService tagInstitutionEvalutionService;
	@Autowired
	private InstitutionService institutionService;
	@Autowired
	private TagInstitutionService tagInstitutionService;
	@Autowired
	private MeasuermentInstitutionService measurementInstitutionService;
	@Autowired
	private QuestionInstitutionService questionInstitutionService;
	@Autowired
	private MeasuermentService measuermentService;
	@Autowired
	private StudentClassInfoService studentClassInfoService;
	@Autowired
	private TeacherClassInfoService teacherClassInfoService;
	@Autowired
	private ChapterActiveService chapterActiveService;
	@Autowired
	private TagService tagService;
	@Autowired
	private ManagerUserService managerUserService;
	@Autowired
	private CourseVRService courseVRService;
	*//**
	 * 用户收藏课程
	 * 
	 * @author liuzhen
	 * @param courseId
	 * @return 返回结果
	 *//*
	@RequestMapping("/collectCourse.action")
	@ResponseBody
	public ResultBean collectCourse(String courseId) {
		return courseService.collectCourse(courseId);
	}

	*//**
	 * 用户取消收藏
	 * 
	 * @param courseId 课程Id
	 * @return
	 *//*
	@RequestMapping("/cancelCollectCourse.action")
	@ResponseBody
	public ResultBean cancelCollectCourse(String courseId) {
		return courseService.cancelCollectCourse(courseId);
	}

	*//**
	 * 二级标签查询
	 * 
	 * @param id 课程ID
	 * @return 课程下的二级标签列表
	 *//*
	@RequestMapping("/searchSecondTag2.action")
	@ResponseBody
	public List<TagView> searchSecondTag2(String id) {
		return courseService.searchSecondTagList2(id);
	}
	*//**
	 * 学校评价二级标签查询
	 * 
	 * @param id 课程ID
	 * @return 课程下的二级标签列表
	 *//*
	@RequestMapping("/searchInsitutionSecondTag2.action")
	@ResponseBody
	public List<TagInstitutionView> searchInsitutionSecondTag2(String id) {
		return courseService.searchInstitutionSecondTagList2(id);
	}
	@RequestMapping("/searchSecondTag.action")
	@ResponseBody
	public List<TagView> searchSecondTag(String id) {
		return courseService.searchSecondTagList(id);
	}

	*//**
	 * 体验课程/加入课程操作
	 * 
	 * @param courseId 课程ID
	 * @return 是否加入课程成功
	 * @author WUXIAOXIANG
	 *//*
	@RequestMapping("/joinCourse.action")
	@ResponseBody
	public ResultBean joinCourse(String courseId) {
		return courseService.joinCourse(courseId);
	}

	@RequestMapping("/applyJoinCourse.action")
	@ResponseBody
	public ResultBean applyJoinCourse(String courseId) {
		return courseService.applyJoinCourse(courseId);
	}
	
	*//**
	 * 课程推荐
	 * 
	 * @author FZ
	 * @return 返回结果
	 *//*
	@RequestMapping("/recommendedCourses.action")
	@ResponseBody
	public ResultBean recommendedCourses(String courseId, String displayNum) {

		return courseService.recommendedCourses(displayNum, courseId);
	}

	*//**
	 * 课程评价
	 * 
	 * @param linkId linkType
	 * @return 返回结果
	 * @author dengzhihao
	 *//*
	@RequestMapping("/courseEvaluationList.action")
	@ResponseBody
	public ResultBean courseEvaluationList(ScoreEntity scoreEntity) {

		return courseService.courseEvaluationList(scoreEntity);
	}

	*//**
	 * 对课程评价
	 * 
	 * @param scoreEntity     评价主题entity
	 * @param scoreDataEntity 评价数据entity
	 * @return 是否评价成功
	 * @author WUXIAOXIANG
	 *//*
	@RequestMapping("/evaluateCourse.action")
	@ResponseBody
	public ResultBean evaluateCourse(ScoreEntity scoreEntity, ScoreDataEntity scoreDataEntity) {
		// 插入评价信息
		return courseService.insertEvaluateCourse(scoreEntity, scoreDataEntity);
	}

	*//**
	 * 检索课程 包括分类
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 *//*
	@RequestMapping("/courseList.action")
	@ResponseBody
	public ResultBean courseList(CourseManagerView course, String directionTagId, String categoryTagId,
			String courseStage, String sortType, Integer pageSize, Integer pageNumber,String schoolName,String schoolNatrul,String tagsId) {
		if(StringUtil.isNotBlank(tagsId)) {
			course.setTagsId(tagsId);
		}
		if(StringUtil.isNotBlank(schoolNatrul)) {
			course.setSchoolNatrul(schoolNatrul);
		}
		ResultBean result = courseService.courseList(course, directionTagId, categoryTagId, courseStage, sortType,
				pageSize, pageNumber,schoolName);
		return result;
	}

	@RequestMapping("/coursePublicList.action")
	@ResponseBody
	public ResultBean coursePublicList(CourseManagerView course, Integer pageSize, Integer pageNumber) {
		course.setShareFlag(COURSE_SHARE_FLAG_THREE);
		course.setEndFlag(COURSE_END_STATUS_YES);
		UserEntity user = WebContext.getSessionUser();
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		//获取学校名称信息
		String school="";
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
			//获取学校
			school = userEntity.getSchoolName();
		}
		//获取班级
		if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
			List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
			StudentClassInfoEntity entity = new StudentClassInfoEntity();
			entity.setStudentId(uid);
			entityList = studentClassInfoService.searchStudentClassInfoList(entity);
			if(null!=entityList && entityList.size()>0) {
				school =  entityList.get(0).getSchoolName();
			}
		}
		course.setSchoolName(school);
//		if(StringUtil.isNotBlank(course.getDirectionTagId()) && StringUtil.isBlank(course.getCategoryTagId())) {
//			course.setTagId(course.getDirectionTagId());
//		}else {
//			course.setTagId(course.getCategoryTagId());
//			course.setParentTagId(course.getDirectionTagId());
//		}
		
		ResultBean result = courseService.coursePublicList(course, pageSize, pageNumber);
		return result;
	}

	@RequestMapping("/courseFactoryList.action")
	@ResponseBody
	public ResultBean courseFactoryList(CourseManagerView course, Integer pageSize, Integer pageNumber) {
		course.setShareFlag(COURSE_SHARE_FLAG_TWO);
		//获取学校名字
		//获取工号
		UserEntity user = WebContext.getSessionUser();
				String uid =user.getUserId();
				TpUserEntity userEntity = courseService.searchInfoMessage(uid);
				//获取学校名称信息
				String school="";
				if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
					//获取学校
					school = userEntity.getSchoolName();
				}
				//获取班级
				if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
					List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
					StudentClassInfoEntity entity = new StudentClassInfoEntity();
					entity.setStudentId(uid);
					entityList = studentClassInfoService.searchStudentClassInfoList(entity);
					if(null!=entityList && entityList.size()>0) {
						school =  entityList.get(0).getSchoolName();
					}
				}
//				course.setSchoolName(school);
//		if(StringUtil.isNotBlank(course.getDirectionTagId()) && StringUtil.isBlank(course.getCategoryTagId())) {
//			course.setTagId(course.getDirectionTagId());
//		}else {
//			course.setTagId(course.getCategoryTagId());
//			course.setParentTagId(course.getDirectionTagId());
//		}
		
		
		ResultBean result = courseService.coursePublicList(course, pageSize, pageNumber);
		return result;
	}
	
	@RequestMapping("/courseQualityList.action")
	@ResponseBody
	public ResultBean courseQualityList(CourseManagerView course, Integer pageSize, Integer pageNumber) {
		course.setShareFlag(COURSE_SHARE_FLAG_ONE);
		//获取工号
		UserEntity user = WebContext.getSessionUser();
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		//获取学校名称信息
		String school="";
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
			//获取学校
			school = userEntity.getSchoolName();
		}
		//获取班级
		if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
			List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
			StudentClassInfoEntity entity = new StudentClassInfoEntity();
			entity.setStudentId(uid);
			entityList = studentClassInfoService.searchStudentClassInfoList(entity);
			if(null!=entityList && entityList.size()>0) {
				school =  entityList.get(0).getSchoolName();
			}
		}
		course.setSchoolName(school);
//		if(StringUtil.isNotBlank(course.getDirectionTagId()) && StringUtil.isBlank(course.getCategoryTagId())) {
//			course.setTagId(course.getDirectionTagId());
//		}else {
//			course.setTagId(course.getCategoryTagId());
//			course.setParentTagId(course.getDirectionTagId());
//		}
		ResultBean result = courseService.coursePublicList(course, pageSize, pageNumber);
		return result;
	}
	
	*//**
	 * 检索教师信息
	 * 
	 * @author LILIANG
	 * @return UserEntity
	 *//*
	@RequestMapping("/teacherInfo.action")
	@ResponseBody
	public ResultBean teacherInfo(UserEntity user) {
		ResultBean result = courseService.teacherInfo(user);
		return result;
	}

	*//**
	 * 检索与教师相关的课程信息
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 *//*
	@RequestMapping("/teacherCourseList.action")
	@ResponseBody
	public ResultBean teacherCourseList(CourseEntity c) {
		ResultBean result = courseService.teacherCourseList(c);
		return result;
	}

	*//**
	 * 课程浏览数+1
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 *//*
	@RequestMapping("/addCourseViewNum.action")
	@ResponseBody
	public ResultBean addCourseViewNum(CourseEntity c) {
		ResultBean result = courseService.addCourseViewNum(c);
		return result;
	}

	*//**
	 * 根据课程ID检索课程详情
	 * 
	 * @param courseId 课程ID
	 * @return ResultBean data:CourseView 课程基本信息、视频数、练习数、标签、用户课程等信息
	 *//*
	@RequestMapping("/courseDetail.action")
	@ResponseBody
	public ResultBean courseDetail(String courseId) {
		return courseService.searchCourseDetail(courseId);
	}

	*//**
	 * 课程详情-立即支付详情
	 * 
	 * @param courseId 课程Id
	 * @return
	 *//*
	@RequestMapping("/immediatePayDetail.action")
	@ResponseBody
	public ResultBean immediatePayDetail(String courseId) {
		return courseService.immediatePayDetail(courseId);
	}

	*//**
	 * 课程列表页面
	 *//*
	@RequestMapping("/courseListInit.action")
	public ModelAndView courseListInit(String courseId,String schoolName) {
		ModelAndView result = new ModelAndView("/course/client/course_list");
		result.addObject("schoolName", schoolName);
		result.addObject("courseId", courseId);
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/pubCourseListInit.action")
	public ModelAndView pubCourseListInit() {
		ModelAndView result = new ModelAndView("/course/client/course_list_public");
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/qualityCourseListInit.action")
	public ModelAndView qualityCourseListInit() {
		ModelAndView result = new ModelAndView("/course/client/course_list_quality");
		result = getRoleAndNum(result);
		return result;
	}
	
	@RequestMapping("/courseFactoryListInit.action")
	public ModelAndView courseFactoryListInit() {
		ModelAndView result = new ModelAndView("/course/client/course_list_factory");
		result = getRoleAndNum(result);
		return result;
	}
	
	@RequestMapping("/studentHomePage.action")
	public ModelAndView studentHomePage(String userId) {
		ModelAndView result = new ModelAndView("/course/client/studentHomePage");
		UserEntity loginUser = userManagementService.seachUserInfo(userId);
		// List<CourseEntity> courseList = courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result = getRoleAndNum(result);
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/studentHomePageFromList.action")
	public ModelAndView studentHomePageFromList(String userId) {
		ModelAndView result = new ModelAndView("/course/client/studentHomePageFromList");
		UserEntity loginUser = userManagementService.seachUserInfo(userId);
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result = getRoleAndNum(result);
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/teacherHomePage.action")
	public ModelAndView teacherHomePage(String userId) {
		ModelAndView result = new ModelAndView("/course/client/teacherHomePage");
		result = getRoleAndNum(result);
		UserEntity loginUser = userManagementService.seachUserInfo(userId);
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/teacherHomePageFromList.action")
	public ModelAndView teacherHomePageFromList(String userId) {
		ModelAndView result = new ModelAndView("/course/client/teacherHomePageFromList");
		result = getRoleAndNum(result);
		UserEntity loginUser = userManagementService.seachUserInfo(userId);
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/saveInfo.action")
	@ResponseBody
	public ResultBean saveInfo(String studentJson) throws Exception {
		ArchivesEntity archives = JSONUtil.parse(
				JSONUtil.createObMapperInstance(ConfigUtil.getDateMinuteFormatDefualt()), studentJson,
				ArchivesEntity.class);
		courseService.saveArchives(archives);
		ResultBean result = ResultBean.success();
		return result;
	}

	@RequestMapping("/studentList.action")
	public ModelAndView studentList() {
		ModelAndView result = new ModelAndView("/course/client/student_list");
		result = getRoleAndNum(result);
		UserEntity loginUser = userManagementService.seachUserInfo(WebContext.getSessionUserId());
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/teacherList.action")
	public ModelAndView teacherList() {
		ModelAndView result = new ModelAndView("/course/client/teacher_list");
		result = getRoleAndNum(result);
		UserEntity loginUser = userManagementService.seachUserInfo(WebContext.getSessionUserId());
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/archivesList.action")
	@ResponseBody
	public ResultBean archivesList(String userId) {
		ResultBean result = new ResultBean();
		TpUserEntity tpUser = courseService.getUserByUserId(userId);
		List<ArchivesEntity> archivesList = courseService.getArchivesList(userId);
		List<CourseEntity> courseList = courseService.getCourseListByUserId(userId);
		if (courseList.size() > 0) {
			for (CourseEntity entity : courseList) {
				ArchivesEntity archives = new ArchivesEntity();
				archives.setUserId(userId);
				archives.setUserName(tpUser.getPersonName());
				archives.setCreateTime(sdf.format(entity.getCreateDate()));
				archives.setCreateDate(entity.getCreateDate());
				archives.setInfo(sdf.format(entity.getCreateDate()) + ",创建课程，" + entity.getTitle());
				if (entity.getPictureId() != null) {
					archives.setExitImage("1");
					archives.setImageId(entity.getPictureId());
				}
				// archivesList.add(archives);
			}
			listSort(archivesList);
		}
		List<UserCourseEntity> ucList = courseService.getUCListByUserId(userId);
		if (ucList.size() > 0) {
			for (UserCourseEntity uc : ucList) {
				ArchivesEntity archives = new ArchivesEntity();
				archives.setUserId(userId);
				archives.setUserName(tpUser.getPersonName());
				archives.setCreateTime(sdf.format(uc.getCreateDate()));
				archives.setCreateDate(uc.getCreateDate());
				archives.setInfo(sdf.format(uc.getCreateDate()) + ",加入课程，" + uc.getCourseName());
				if (uc.getPictureId() != null) {
					archives.setExitImage("1");
					archives.setImageId(uc.getPictureId());
				}
				// archivesList.add(archives);
			}
			listSort(archivesList);
		}
		result.setData(archivesList);
		return result;
	}

	@RequestMapping("/saveDiscussData.action")
	@ResponseBody
	public ResultBean saveDiscussData(ArchivesDiscussEntity data) throws Exception {
		return courseService.saveDiscuss(data);
	}

	@RequestMapping("/searchUsersList.action")
	@ResponseBody
	public ResultBean searchUsersList(TpUserEntity user) {
		String userKbn = WebContext.getSessionUser().getUserKbn();
		if (userKbn != null && userKbn.contentEquals(TpStateConstants.USER_KBN_C)) {
			user.setUserKbn(TpStateConstants.USER_KBN_C);
		}
		user.setSortName("createDate");
		user.setSortOrder("DESC");
		return userManagementService.search4Page(user);
	}

	@SuppressWarnings("unused")
	private void listSort(List<ArchivesEntity> list) {
		Collections.sort(list, new Comparator<ArchivesEntity>() {
			@Override
			public int compare(ArchivesEntity o1, ArchivesEntity o2) {
				try {
					Date dt1 = o1.getCreateDate();
					Date dt2 = o2.getCreateDate();
					if (dt1.getTime() > dt2.getTime()) {
						return -1;
					} else if (dt1.getTime() < dt2.getTime()) {
						return 1;
					} else {
						return 0;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return 0;
			}
		});
	}

	*//**
	 * 系统首页
	 *//*
	@RequestMapping("/homePage.action")
	public ModelAndView homePage() {
		ModelAndView result = new ModelAndView("/course/client/homePage");
		UserEntity user = WebContext.getSessionUser();
		//获取工号
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getIsFlag())) {
			result.addObject("isFlag","1");
		}else {
			//不是管理员
			result.addObject("isFlag","0");
		}
		
		//获取人员信息
		String image = "";
		String school="";
		String grade="";
		String className="";
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
			//获取头像
			image = userEntity.getHeadurl();
			//获取学校
			school = userEntity.getSchoolName();
		}else {
			//设置默认头像
			image ="http://wisdomsaas-img.oss-cn-beijing.aliyuncs.com/BFAA0FECDD194B7A974C60B7E1B27C18?authorization=bce-auth-v1%2F7642b4b48046452697e90662eed41ace%2F2017-12-29T08%3A33%3A39Z%2F-1%2F%2F0f06024af14dad05694f25b8c85572567591c72bbd0129d3175b4cdbd6e62208";
		}
		//获取班级
		if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
			List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
			StudentClassInfoEntity entity = new StudentClassInfoEntity();
			entity.setStudentId(uid);
			entityList = studentClassInfoService.searchStudentClassInfoList(entity);
			if(null!=entityList && entityList.size()>0) {
				grade = entityList.get(0).getGradeName();
				className = entityList.get(0).getClassName();
				school =  entityList.get(0).getSchoolName();
			}
		}
		result.addObject("className", className);
		result.addObject("grade", grade);
		result.addObject("school", school);
		result.addObject("image", image);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 问卷考试系统首页
	 *//*
	@RequestMapping("/homeJavaPage.action")
	public ModelAndView homeJavaPage(String courseId) {
		ModelAndView result = new ModelAndView("/course/client/homeJavaPage");
		UserEntity user = WebContext.getSessionUser();
		//获取工号
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getIsFlag())) {
			result.addObject("isFlag","1");
		}else {
			//不是管理员
			result.addObject("isFlag","0");
		}
		
		//获取人员信息
		String image = "";
		String school="";
		String grade="";
		String className="";
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
			//获取头像
			image = userEntity.getHeadurl();
			//获取学校
			school = userEntity.getSchoolName();
		}else {
			//设置默认头像
			image ="http://wisdomsaas-img.oss-cn-beijing.aliyuncs.com/BFAA0FECDD194B7A974C60B7E1B27C18?authorization=bce-auth-v1%2F7642b4b48046452697e90662eed41ace%2F2017-12-29T08%3A33%3A39Z%2F-1%2F%2F0f06024af14dad05694f25b8c85572567591c72bbd0129d3175b4cdbd6e62208";
		}
		//获取班级
		if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
			List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
			StudentClassInfoEntity entity = new StudentClassInfoEntity();
			entity.setStudentId(uid);
			entityList = studentClassInfoService.searchStudentClassInfoList(entity);
			if(null!=entityList && entityList.size()>0) {
				grade = entityList.get(0).getGradeName();
				className = entityList.get(0).getClassName();
				school =  entityList.get(0).getSchoolName();
			}
		}
		result.addObject("className", className);
		result.addObject("grade", grade);
		result.addObject("school", school);
		result.addObject("courseId", courseId);
		result.addObject("image", image);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 题库管理
	 *//*
	@RequestMapping("/homeExamPage.action")
	public ModelAndView homeExamPage() {
		ModelAndView result = new ModelAndView("/course/manager/homeExamPage");
		UserEntity user = WebContext.getSessionUser();
		//获取工号
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getIsFlag())) {
			result.addObject("isFlag","1");
		}else {
			//不是管理员
			result.addObject("isFlag","0");
		}
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 题库管理
	 *//*
	@RequestMapping("/homeJsdPage.action")
	public ModelAndView homeJsdPage() {
		ModelAndView result = new ModelAndView("/course/client/homeJsdPage");
		UserEntity user = WebContext.getSessionUser();
		//获取工号
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getIsFlag())) {
			result.addObject("isFlag","1");
		}else {
			//不是管理员
			result.addObject("isFlag","0");
		}
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 获取登录信息
	 * @param code
	 * @param type
	 * @return
	 * @throws UnsupportedEncodingException
	 *//*
	@SuppressWarnings("null")
	@RequestMapping("/getUserInfo.action")
	@ResponseBody
	public JSONArray getUserInfo(String code, String type) throws UnsupportedEncodingException {
		//设置json集合对象和数组
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		if (code != null && !"".equals(code)) {
			//UserEntity user = WebContext.getSessionUser();
			String codeSession = (String) WebContext.getSession().getAttribute("code");
			if (codeSession == null || !code.equals(codeSession)) {
				Long str12 = System.currentTimeMillis();
				String access_tokenStr = "";
				//定义获取应用token的变量
				//String application_tokenStr="";
				if (type.equals("1")) {
					//获取access_tokenStr字符串对象
					access_tokenStr = HttpUtils.sendFormEvalutionPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token",
							"code=" + code
									+ "&grant_type=authorization_code&client_id=1EF8798194B36D72&client_secret=440a4fad895036188ea200a0da1d6156&redirect_uri="
									+ PropertyUtil.getProperty("remote.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "1EF8798194B36D72", "440a4fad895036188ea200a0da1d6156");
				} else if (type.equals("2")) {
					//获取access_tokenStr字符串对象
					access_tokenStr = HttpUtils.sendFormEvalutionPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token",
							"code=" + code
									+ "&grant_type=authorization_code&client_id=BB2650288E091B1C&client_secret=26807915a5269ba07a40d97236b5ec64&redirect_uri="
									+ PropertyUtil.getProperty("teacher.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "BB2650288E091B1C", "26807915a5269ba07a40d97236b5ec64");
				} else if (type.equals("3")) {
					//获取access_tokenStr字符串对象
					access_tokenStr = HttpUtils.sendPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token",
							"code=" + code
									+ "&grant_type=authorization_code&client_id=42F9292D78B1386F&client_secret=e1e034ac44a63ba61e8e9595689efd4c&redirect_uri="
									+ PropertyUtil.getProperty("student.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "42F9292D78B1386F", "e1e034ac44a63ba61e8e9595689efd4c");
				} else if (type.equals("4")) {
					//获取access_tokenStr字符串对象
					   access_tokenStr = HttpUtils.sendFormEvalutionPost(
					            PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", 
					            "code=" + code + 
					            "&grant_type=authorization_code&client_id=6FF0E39AD917FD04&client_secret=ea201b6aff470f9ec25c1fc9be17197a&redirect_uri=" + 
					            PropertyUtil.getProperty("institution.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "6FF0E39AD917FD04", "ea201b6aff470f9ec25c1fc9be17197a");
				} else if (type.equals("5")) {
					//获取access_tokenStr字符串对象
					access_tokenStr = HttpUtils.sendFormEvalutionPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token",
							"code=" + code
									+ "&grant_type=authorization_code&client_id=8275669B807A7B7A&client_secret=229f39d615a8c2cbbef56e56197a22c7&redirect_uri="
									+ PropertyUtil.getProperty("teacherevalution.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "8275669B807A7B7A", "229f39d615a8c2cbbef56e56197a22c7");
				}
				//设置执行时间
				Long str333 = System.currentTimeMillis();
				int tokentime =(int) (str333-str12);
				UserLogEntity entity12=new UserLogEntity();
				entity12.setId(IDGenerator.genUID());
				entity12.setUserId(WebContext.getSessionUserId());
				entity12.setUserName(WebContext.getSessionUser().getPersonName());
				entity12.setLogType("获取token信息");
				entity12.setTimes(tokentime);
				entity12.setOperTime(new Date());
				//保存方法执行时间日志信息
				tagInstitutionEvalutionService.insertUserLog(entity12);
				//获取access_token
				if (access_tokenStr != null && !"".equals(access_tokenStr)) {
					//字符转化成json对象
					JSONObject accessjson = JSONObject.fromObject(access_tokenStr);
					//获取字段值
					String access_token = accessjson.getString("access_token");
					Long str3 = System.currentTimeMillis();
					//原有逻辑获取用户信息开始
					String userStr = HttpUtils.sendFormEvalutionPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/rest/get/userinfo",
							"access_token=" + access_token);
					Long str33 = System.currentTimeMillis();
					int userinfotime =(int) (str33-str3);
					UserLogEntity entity13=new UserLogEntity();
					entity13.setId(IDGenerator.genUID());
					entity13.setUserId(WebContext.getSessionUserId());
					entity13.setUserName(WebContext.getSessionUser().getPersonName());
					entity13.setLogType("获取/passport/rest/get/userinfo信息");
					entity13.setTimes(userinfotime);
					entity13.setOperTime(new Date());
					//保存方法执行需要的时间信息日志
					tagInstitutionEvalutionService.insertUserLog(entity13);
					System.out.println("userStr=:" + userStr);
					
					
					JSONObject userjson = JSONObject.fromObject(userStr);
					//原有逻辑获取用户信息结束
					//用户id
					String userid = userjson.getString("userid");
					//用户账号
					String account = userjson.getString("account");
					//用户类型
					String usertype = userjson.getString("usertype");
					//用户名称
					String name = userjson.getString("name");
					//用户性别
					String sex = userjson.getString("sex");
					//用户角色
					String role = userjson.getString("role");
					//获取tenantId
					String tenantId = userjson.getString("tenantId");
					//获取头像url
					String headUrl = userjson.getString("head_url");
					String teacher = "";
					*//**
					 * usertype=1
					 * 1:平台总系统管理员;2:平台省级系统管理员;3:平台市级系统管理员;4:学校总系统管理员;5:学校分校系统管理员;6:学校年级系统管理员;7:教育部分系统管理员
					 * usertype=0 
					 * 1:教师;2:家长;3:学生;4:管理者;5:教研员
					 *//*
					*//**
					 * 系统角色 01:管理员;04:教师
					 *//*
					UserRoleEntity userRole = new UserRoleEntity();
					UserRoleEntity userTeacherRole = new UserRoleEntity();
					UserEntity loginUser = new UserEntity();
					TpUserEntity saveUser = new TpUserEntity();
					*//**
					 * 获取用户是老师的信息
					 * 包括老师所属的学校信息
					 *//*
					if("1".equals(role)) {
						//获取教师信息
						Long str5 = System.currentTimeMillis();
						//调用外部接口获取教师信息
						String teacherinfo = HttpUtils.sendFormDataPost("https://gwss.yceduyun.com/apis/user/V1.0/teacherInfo",
								access_token,"002");
						Long str6 = System.currentTimeMillis();
						int teachertime =(int) (str6-str5);
						UserLogEntity entity2=new UserLogEntity();
						entity2.setId(IDGenerator.genUID());
						entity2.setUserId(WebContext.getSessionUserId());
						entity2.setUserName(WebContext.getSessionUser().getPersonName());
						entity2.setLogType("获取https://gwss.yceduyun.com/apis/user/V1.0/teacherInfo信息");
						entity2.setTimes(userinfotime);
						entity2.setOperTime(new Date());
						//保存获取教师信息所需要的时间日志信息
						tagInstitutionEvalutionService.insertUserLog(entity2);
						System.out.println(teacherinfo); 
                        //解析教师信息
						JSONObject teacherinfojson = JSONObject.fromObject(teacherinfo);
						JSONObject teacherinfoDatajson = JSONObject.fromObject(teacherinfojson.get("data"));
						//获取基本信息
						if(null!=teacherinfoDatajson) {
							//省份
							String proName=teacherinfoDatajson.getString("proName");
							//市区
							String cityName=teacherinfoDatajson.getString("cityName");
							//县级
							String townName=teacherinfoDatajson.getString("townName");
							//学校id
							String schoolId=teacherinfoDatajson.getString("schoolId");
							//学校名称
							String schoolName=teacherinfoDatajson.getString("schoolName");
							//学校类型
							String schoolType=teacherinfoDatajson.getString("schoolType");
							//
							String subjectId=teacherinfoDatajson.getString("subjectId");
							//
							String subjectName=teacherinfoDatajson.getString("subjectName");
							saveUser.setProName(proName);
							saveUser.setCityName(cityName);
							saveUser.setTownName(townName);
							saveUser.setSchoolId(schoolId);
							saveUser.setSchoolName(schoolName);
							saveUser.setSchoolType(schoolType);
							saveUser.setSubjectId(subjectId);
							saveUser.setSubjectName(subjectName);
						}
						//获取字段信息
						JSONArray teacherClassArray =JSONArray.fromObject(teacherinfoDatajson.get("classList"));
						//获取老师的班级年级信息
						@SuppressWarnings("unchecked")
						List<TeacherClassInfoEntity> list = teacherClassArray.toList(teacherClassArray, TeacherClassInfoEntity.class);
						//判断对象是否为空
						if(null!=list && list.size()>0) {
							List<TeacherClassInfoEntity> entityList = new ArrayList<TeacherClassInfoEntity>();
							//查询该老师下的信息是否存在
							TeacherClassInfoEntity entity = new TeacherClassInfoEntity();
							entity.setTeacherId(userid);
							entityList = teacherClassInfoService.searchTeacherClassInfoList(entity);
							if(null!=entityList && entityList.size()>0) {
								//循环遍历教师班级信息
								for(int j=0;j<entityList.size();j++) {
									TeacherClassInfoEntity entity1 = new TeacherClassInfoEntity();
									entity1.setId(entityList.get(j).getId());
									//删除已存在信息
									teacherClassInfoService.deleteTeacherClassInfoById(entity1);
								}
							}
							//循环保存班级信息
							for(int i=0;i<list.size();i++) {
								TeacherClassInfoEntity infoEntity = new TeacherClassInfoEntity();
								infoEntity.setTeacherId(userid);//老师id
								infoEntity.setGradeId(list.get(i).getGradeId());//年级id
								infoEntity.setGradeName(list.get(i).getGradeName());//年级名称
								infoEntity.setGradeLever(list.get(i).getGradeLever());//年级等级
								infoEntity.setClassId(list.get(i).getClassId());//班级id
								infoEntity.setClassName(list.get(i).getClassName());//班级名称
								infoEntity.setSubjectId(list.get(i).getSubjectId());//
								infoEntity.setSubjectName(list.get(i).getSubjectName());//
								infoEntity.setCreateDate(new Date());//创建日期
								infoEntity.setUpdateDate(new Date());//更新日期
								teacherClassInfoService.insertTeacherClassInfo(infoEntity);
							}
						}
						//获取用户具体信息
						String param=access_token;
						String tenantId1 = "002";
						Long str7 = System.currentTimeMillis();
						//获取教师角色
						String teacherDetailinfo = HttpUtils.sendFormDataDetailPost("https://gwss.yceduyun.com/apis/usercenter/v1.0/getOneUserInfoByUseIdAndRoleId",
								param,tenantId1,role);
						Long str8 = System.currentTimeMillis();
						int studenttime =(int) (str8-str7);
						UserLogEntity entity3=new UserLogEntity();
						entity3.setId(IDGenerator.genUID());
						entity3.setUserId(WebContext.getSessionUserId());
						entity3.setUserName(WebContext.getSessionUser().getPersonName());
						entity3.setLogType("获取https://gwss.yceduyun.com/apis/usercenter/v1.0/getOneUserInfoByUseIdAndRoleId信息");
						entity3.setTimes(userinfotime);
						entity3.setOperTime(new Date());
						//保存调用教师角色花费时间信息
						tagInstitutionEvalutionService.insertUserLog(entity3);
						//字符串转json
						JSONObject teacherDetailinfojson = JSONObject.fromObject(teacherDetailinfo);
						JSONArray teacherinfoDataDetailjson = JSONArray.fromObject(teacherDetailinfojson.get("data"));
						JSONArray dutyJson = JSONObject.fromObject(teacherinfoDataDetailjson.get(0)).getJSONArray("teacherDuties");
						List<InstitutionTeacherDutyEntity> dutylist = JSONArray.toList(dutyJson, InstitutionTeacherDutyEntity.class);
                        if(null!=dutylist && dutylist.size()>0) {
                        	for(int p=0;p<dutylist.size();p++) {
                        		//001代表校长
                        		if("校长".equals(dutylist.get(p).getDutyName())) {
                        			teacher="001";
                        			break;
                        		}else {
                        			//002代表老师
                        			teacher="002";
                        		}
                        	}
                        }
					}else if("3".equals(role)) {
						//获取学生信息
						Long str9 = System.currentTimeMillis();
						//调用学生接口获取信息
						String studentinfo = HttpUtils.sendFormDataPost("https://gwss.yceduyun.com/apis/user/V1.0/studentInfo",
								access_token,"002");
						Long str10 = System.currentTimeMillis();
						int studenttime =(int) (str10-str9);
						UserLogEntity entity3=new UserLogEntity();
						entity3.setId(IDGenerator.genUID());
						entity3.setUserId(WebContext.getSessionUserId());
						entity3.setUserName(WebContext.getSessionUser().getPersonName());
						entity3.setLogType("获取https://gwss.yceduyun.com/apis/user/V1.0/studentInfo信息");
						entity3.setTimes(userinfotime);
						entity3.setOperTime(new Date());
						//保存调用学生接口获取花费的时间日志
						tagInstitutionEvalutionService.insertUserLog(entity3);
						JSONObject studentinfojson = JSONObject.fromObject(studentinfo);
						JSONObject studentinfoDatajson = JSONObject.fromObject(studentinfojson.get("data"));
						if(null!= studentinfoDatajson) {
							String studentId=userid;
							//姓名
							String studentName=studentinfoDatajson.getString("username");
							String schoolId=studentinfoDatajson.getString("schoolId");//学校id
							String schoolName=studentinfoDatajson.getString("schoolName");//学校名称
							String schoolType=studentinfoDatajson.getString("schoolTye");//学校类型
							String gradeId=studentinfoDatajson.getString("gradeId");//年级id
							String gradeName=studentinfoDatajson.getString("gradeName");//年级名称
							String classId=studentinfoDatajson.getString("classId");//班级id
							String className=studentinfoDatajson.getString("className");//班级名称
							String gradeLever=studentinfoDatajson.getString("gradeLever");//年级等级
							String proName=studentinfoDatajson.getString("proName");//省份
							String cityName=studentinfoDatajson.getString("cityName");//市区
							String townName=studentinfoDatajson.getString("townName");//县级
							StudentClassInfoEntity studentClassInfoEntity = new StudentClassInfoEntity();
							studentClassInfoEntity.setStudentId(studentId);
							List<StudentClassInfoEntity> studentlist = new ArrayList<StudentClassInfoEntity>();
							studentlist = studentClassInfoService.searchStudentClassInfoList(studentClassInfoEntity);
							if(null!=studentlist && studentlist.size()>0) {
								//循环遍历学生信息
								for(int k=0;k<studentlist.size();k++) {
									StudentClassInfoEntity student = new StudentClassInfoEntity();
									student.setId(studentlist.get(k).getId());
									//删除学生信息
									studentClassInfoService.deleteStudentClassInfoById(student);
								}
							}
							//保存学生班级信息
							StudentClassInfoEntity stunfoEntity = new StudentClassInfoEntity();
							stunfoEntity.setStudentId(studentId);
							stunfoEntity.setStudentName(studentName);
							stunfoEntity.setSchoolId(schoolId);
							stunfoEntity.setSchoolName(schoolName);
							stunfoEntity.setSchoolType(schoolType);
							stunfoEntity.setGradeId(gradeId);
							stunfoEntity.setGradeName(gradeName);
							stunfoEntity.setGradeLever(gradeLever);
							stunfoEntity.setClassId(classId);
							stunfoEntity.setClassName(className);
							stunfoEntity.setProName(proName);
							stunfoEntity.setCityName(cityName);
							stunfoEntity.setTownName(townName);
							stunfoEntity.setCreateDate(new Date());
							studentClassInfoService.insertStudentClassInfo(stunfoEntity);
							Long str122 = System.currentTimeMillis();
							int studentinerttime =(int) (str122-str10);
							UserLogEntity entity32=new UserLogEntity();
							entity32.setId(IDGenerator.genUID());
							entity32.setUserId(WebContext.getSessionUserId());
							entity32.setUserName(WebContext.getSessionUser().getPersonName());
							entity32.setLogType("保存学生角色信息");
							entity32.setTimes(studentinerttime);
							entity32.setOperTime(new Date());
							tagInstitutionEvalutionService.insertUserLog(entity32);
						}
						
					}
					if (usertype != null && !"".equals(usertype) && usertype.equals("0")) {
						//老师
						if (!"".equals(role) && role.equals("1")) {
							userRole.setRoleId("01");
							userTeacherRole.setRoleId("04");
							loginUser.setUserKbn("A");
							saveUser.setUserKbn("A");
							if (userid != null && !"".equals(userid)) {
								userRole.setUserId(userid);
								userTeacherRole.setUserId(userid);
							}
							//管理者
						} else if (!"".equals(role) && role.equals("4")) {
							userRole.setRoleId("01");
							loginUser.setUserKbn("B");
							saveUser.setUserKbn("B");
							if (userid != null && !"".equals(userid)) {
								userRole.setUserId(userid);
							}
							//学生
						} else if(!"".equals(role) && role.equals("3")){
							loginUser.setUserKbn("C");
							saveUser.setUserKbn("C");
							//家长
						}else if(!"".equals(role) && role.equals("2")){
							loginUser.setUserKbn("D");
							saveUser.setUserKbn("D");
							//教研员
						}else if(!"".equals(role) && role.equals("5")){
							loginUser.setUserKbn("E");
							saveUser.setUserKbn("E");
						}
					}
					
					loginUser.setUserId(userid);
					loginUser.setUserName(account == null ? "" : account);
					loginUser.setSex(sex == null ? "" : sex);
					loginUser.setPersonName(name == null ? "" : name);
					loginUser.setPassword(MD5Util.getPassword4MD5("123456"));
					userRole.setCreateDate(new Date());
					userRole.setCreateUser(userid);
					userTeacherRole.setCreateDate(new Date());
					userTeacherRole.setCreateUser(userid);
					loginUser.setCreateDate(new Date());
					loginUser.setCreateUser(userid);
					loginUser.setLockFlag("0");

					saveUser.setUserId(userid);
					saveUser.setUserName(account == null ? "" : account);
					saveUser.setSex(sex == null ? "" : sex);
					saveUser.setPersonName(name == null ? "" : name);
					saveUser.setPassword(MD5Util.getPassword4MD5("123456"));
					saveUser.setCreateDate(new Date());
					saveUser.setCreateUser(userid);
					saveUser.setCreateDate(new Date());
					saveUser.setCreateUser(userid);
					saveUser.setLockFlag("0");
					saveUser.setTenantId(tenantId);
					saveUser.setHeadurl(headUrl);
					saveUser.setDuty(teacher);
					Long usertime1 = System.currentTimeMillis();
					courseService.saveUserAndRole(userRole, userTeacherRole, saveUser);
					Long usertime2 = System.currentTimeMillis();
					WebContext.loginUser(loginUser, true);
					WebContext.getSession().setAttribute("code", code);
					jo.put("result", "success");
					Long str123 = System.currentTimeMillis();
					System.out.println("计算花费时间---------------------------");
					int times = (int) (usertime2-usertime1);
					UserLogEntity entity=new UserLogEntity();
					entity.setId(IDGenerator.genUID());
					entity.setUserId(WebContext.getSessionUserId());
					entity.setUserName(WebContext.getSessionUser().getPersonName());
					entity.setLogType("获取保存用户信息");
					entity.setTimes(times);
					entity.setOperTime(new Date());
					tagInstitutionEvalutionService.insertUserLog(entity);
					System.out.println(str123-str12);
				}
			}

		}
		json.add(jo);
		return json;
	}
    *//**
     * 手机端首页
     * @return
     *//*
	@RequestMapping("/homePagePhone.action")
	public ModelAndView homePagePhone() {
		ModelAndView result = new ModelAndView("/course/client/homePagePhone");
		return result;
	}
    *//**
                * 教师页面
     * @return
     *//*
	@RequestMapping("/teacherHome.action")
	public ModelAndView teacherHome() {
		ModelAndView result = new ModelAndView("/course/client/teacherHomeBack");
		return result;
	}
    *//**
     * 学生页面
     * @return
     *//*
	@RequestMapping("/studentHome.action")
	public ModelAndView studentHome() {
		ModelAndView result = new ModelAndView("/course/client/studentHomeBack");
		return result;
	}
    *//**
     * 人员信息
     * @return
     *//*
	@RequestMapping("/sysUser.action")
	public ModelAndView sysUser() {
		ModelAndView result = new ModelAndView("/course/client/sysUser");
		result = getRoleAndNum(result);
		return result;
	}
    *//**
     * 系统日志
     * @return
     *//*
	@RequestMapping("/sysLog.action")
	public ModelAndView sysLog() {
		ModelAndView result = new ModelAndView("/course/client/sysLog");
		result = getRoleAndNum(result);
		return result;
	}
   *//**
    * 保存系统日志
    * @param proName
    * @param IpInfo
    * @return
    *//*
	@RequestMapping("/insertSysUser.action")
	@ResponseBody
	public JSONArray insertSysUser(String proName, String IpInfo) {
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		courseService.insertSysUser(proName, IpInfo);
		jo.put("result", "success");
		json.add(jo);
		return json;
	}
    *//**
     * 保存操作日志
     * @param browserInfo
     * @param sysInfo
     * @param IpInfo
     * @param operDetail
     * @param operSite
     * @return
     *//*
	@RequestMapping("/insertUserOper.action")
	@ResponseBody
	public JSONArray insertUserOper(String browserInfo, String sysInfo, String IpInfo, String operDetail,
			String operSite) {
		UserOperEntity oper = new UserOperEntity();
		oper.setSysInfo(browserInfo + "(" + sysInfo + ")");
		oper.setOperDetail(operDetail);
		oper.setOperSite(operSite);
		oper.setOperTime(new Date());
		oper.setOperIP(IpInfo);
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		courseService.inserUserOper(oper);
		jo.put("result", "success");
		json.add(jo);
		return json;
	}
	*//**
	 * 
	 * @param IpInfo
	 * @param logType
	 * @return
	 *//*
	@RequestMapping("/insertUserLog.action")
	@ResponseBody
	public JSONArray insertUserLog(String IpInfo, String logType) {
		UserLogEntity log = new UserLogEntity();
		log.setOperIP(IpInfo);
		log.setLogType(logType);
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		String userId = WebContext.getSessionUserId();
		String userName = WebContext.getSessionUser().getPersonName();
		log.setUserId(userId);
		log.setUserName(userName);
		courseService.insertUserLog(log);
		jo.put("result", "success");
		json.add(jo);
		WebContext.logout();
		return json;
	}
    *//**
                  * 根据用户名查询用户信息
     * @param username
     * @param IpInfo
     * @return
     *//*
	@RequestMapping("/getUserByUsername.action")
	@ResponseBody
	public JSONArray getUserByUserName(String username, String IpInfo) {
		TpUserEntity result = courseService.getUserByUsername(username);
		if (result != null) {
			UserLogEntity log = new UserLogEntity();
			log.setOperIP(IpInfo);
			log.setLogType("登录");
			log.setUserId(result.getUserId());
			log.setUserName(result.getPersonName());
			courseService.insertUserLog(log);
		}
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		if (result != null) {
			jo.put("result", "success");
		} else {
			jo.put("result", "error");
		}
		json.add(jo);
		return json;
	}

	*//**
	 * 系统首页
	 *//*
	@RequestMapping("/webEditor.action")
	public ModelAndView webEditor() {
		ModelAndView result = new ModelAndView("/course/client/webEditor");
		String serverName = WebContext.getRequest().getServerName();
		int serverPort = WebContext.getRequest().getServerPort();
		result.addObject("serverUrl", serverName + ":" + serverPort);
		return result;
	}
    *//**
     * 课程详情页面
     * @param courseId
     * @param schoolName
     * @return
     *//*
	@RequestMapping("/courseDetailInit.action")
	public ModelAndView courseDetailInit(String courseId,String schoolName) {
		ModelAndView result = new ModelAndView("/course/client/course_detail");
		ResultBean data = courseService.searchCourseDetail(courseId);
		if (!data.isStatus()) {
			throw new BusinessException(data.getMessages());
		}
		ChapterEntity entity = new ChapterEntity();
		entity.setCourseId(courseId);
		List<ChapterEntity> chapterCourseList = courseService.selectByCourseId(entity);
		result.addObject("chapterCourseList", chapterCourseList);
		result.addObject("courseView", data.getData());
		result.addObject("schoolName", schoolName);
		result.addObject("courseId", courseId);
		// 检索章节列表
		List<ChapterView> chapterList = chapterService.chapterList(courseId);
		result.addObject("chapterList", chapterList);
		String measureType="2";
		List<ChapterView> chapterList1 = chapterService.chapterList1(courseId,measureType);
		result.addObject("chapterExamList", chapterList1);
		List<CourseMessageEntity> userMsgList = courseService.getUserMsgList(courseId);
		if(userMsgList.size() > 0) {
			CourseMessageEntity msg = userMsgList.get(0);
			if(msg.getCheckStatus().equals("-1")) {
				result.addObject("msgInfo", "-1");
			}else if(msg.getCheckStatus().equals("1")) {
				result.addObject("msgInfo", "1");
			}else {
				result.addObject("msgInfo", "none");
			}
		}
		result = getRoleAndNum(result);
		return result;
	}
    *//**
     * 公开课详情页面
     * @param courseId
     * @return
     *//*
	@RequestMapping("/coursePublicDetail.action")
	public ModelAndView coursePublicDetail(String courseId) {
		ModelAndView result = new ModelAndView("/course/client/course_public_detail");
		ResultBean data = courseService.searchCourseDetail(courseId);
		if (!data.isStatus()) {
			throw new BusinessException(data.getMessages());
		}
		result.addObject("courseView", data.getData());
		result = getRoleAndNum(result);
		return result;
	}

	*//**
	 * 导师课程页面初始化
	 * 
	 * @param
	 * @return
	 *//*
	@RequestMapping("/teacherCourseInit.action")
	public ModelAndView teacherCourseInit(UserEntity user) {
		ModelAndView mv = new ModelAndView("/course/client/teacher_course_list");
		ResultBean result = courseService.teacherInfo(user);
		UserEntity use = new UserEntity();
		if (result.getData() != null) {
			use = (UserEntity) result.getData();
			if ("0".equals(use.getSex())) {
				use.setSex("男");
			} else {
				use.setSex("女");
			}
		}
		mv.addObject("user", use);
		return mv;
	}

	*//**
	 * 投票页面初始化
	 * 
	 * @param
	 * @return
	 *//*
	@RequestMapping("/voteItemInit.action")
	public ModelAndView voteItemInit(String courseId, String linkType) {
		ModelAndView mv = new ModelAndView("/course/client/vote_item");
		if (StringUtils.isNotEmpty(linkType)) {
			mv.addObject("linkType", linkType);
		}
		mv.addObject("courseId", courseId);
		return mv;
	}

	*//**
	 * 根据用户ID，课程ID检索用户课程
	 * 
	 * @param courseId
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchUserCourse.action")
	@ResponseBody
	public ResultBean searchUserCourse(String courseId) {
		ResultBean result = new ResultBean();
		if (courseId == null || courseId.equals("")) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_NULL_ID));

			return result;
		}

		UserCourseEntity uc = new UserCourseEntity();
		uc.setCourseId(courseId);
		// 从session中取出登录用户的ID
		String userId = WebContext.getSessionUserId();
		if (userId == null) {
			result.setStatus(false);
			result.setErrorCode(ErrorCode.NOT_LOGIN);
			result.setMessages(getMessage(MSG_E_SESSION_TIMEOUT));
			return result;
		}
		uc.setUserId(userId);
		uc = courseService.searchUserCourse(uc);
		result.setData(uc);
		result.setStatus(true);
		return result;
	}

	*//**
	 * 课程作业页面初始化
	 * 
	 * @param linkId   链接ID
	 * @param linkType 链接类型
	 * @return
	 *//*
	@RequestMapping("/homeWorkInit.action")
	public ModelAndView homeWorkInit(String linkId, String linkType) {
		ModelAndView mv = new ModelAndView("/course/client/home_work");
		if (StringUtils.isNotEmpty(linkType)) {
			mv.addObject("linkType", linkType);
		}
		mv.addObject("linkId", linkId);
		return mv;
	}

	*//**
	 * 检索我的作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/honeworkDetail.action")
	@ResponseBody
	public ResultBean honeworkDetail(HomeworkView homeworkView) {
		ResultBean result = new ResultBean();
		String userId = WebContext.getSessionUserId();
		List<HomeworkView> viewlist = new ArrayList<HomeworkView>();
		viewlist = (List<HomeworkView>) (courseService.honeworkList(homeworkView)).getData();
		for (int i = 0; i < viewlist.size(); i++) {
			CourseView courseView = new CourseView();
			courseView.setTeacherId(userId);
			if (courseService.isTeacher(courseView.getTeacherId())) {
				viewlist.get(i).setTeacher(1);
			} else {
				viewlist.get(i).setTeacher(0);
			}

		}

		result.setData(viewlist);
		return result;
	}

	@RequestMapping("/homeworkTitle.action")
	@ResponseBody
	public ResultBean homeworkTitle(HomeworkView homeworkView) {
		ResultBean result = new ResultBean();
		HomeworkEntity homeWork = courseService.getHomeWorkByLinkId(homeworkView);
		if (homeWork != null) {
			result.setStatus(true);
			result.setData(homeWork);
		}
		return result;
	}

	@RequestMapping("/homeWorkEdit.action")
	public ModelAndView homeWorkEdit(String id) {
		ModelAndView result = new ModelAndView("/course/client/home_work_edit");
		HomeworkView view = courseService.getHomeWorkById(id);
		result.addObject("homeWork", view);
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/homeWorkView.action")
	public ModelAndView homeWorkView(String id) {
		ModelAndView result = new ModelAndView("/course/client/home_work_view");
		HomeworkView view = courseService.getHomeWorkById(id);
		view.setCreateTime(sdf.format(view.getCreateDate()));
		result.addObject("homeWork", view);
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/homeWorkAddQuestion.action")
	public ModelAndView homeWorkAddQuestion(String id) {
		ModelAndView result = new ModelAndView("/course/client/home_work_question");
		HomeworkView view = courseService.getHomeWorkById(id);
		view.setCreateTime(sdf.format(view.getCreateDate()));
		result.addObject("homeWork", view);
		result.addObject("aq", new HomeworkAnswerQuestionEntity());
		result = getRoleAndNum(result);
		return result;
	}

	*//**
	 * 上传文件
	 *//*
	@RequestMapping("/upload.action")
	@ResponseBody
	public ResultBean upload(HomeworkView homeworkView) throws Exception {
		WebContext.init4CrossDomainAjax();
		return courseService.uploadDocument(homeworkView);
	}

	@RequestMapping("/download.action")
	public void download(@RequestParam(required = true) String fileId, String fileType, HttpServletResponse response)
			throws Exception {

		OutputStream output = new BufferedOutputStream(response.getOutputStream());
		InputStream input = null;
		try {
			FileEntity file = new FileEntity();
			if (StringUtil.isNotEmpty(fileType)) {
				file = fileService.getFile(fileId, fileType);
			} else {
				file = fileService.getFile(fileId, FILE_TYPE_HOMEWORK);
			}

			byte[] binary = file.getBinary();
			String fileName = file.getFileName();
			fileName = URLEncoder.encode(fileName, "UTF-8");
			response.reset();
			response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			response.addHeader("Content-Length", "" + file.getFileSize());
			response.setContentType("application/octet-stream;charset=UTF-8");
			if (StringUtil.isNotEmpty(file.getPath()) && StringUtil.isNotEmpty(ConfigUtil.getFileStorageRoot())) {
				// 文件有路径
				input = FileUtil.getInputStream(ConfigUtil.getFileStorageRoot(), file.getPath());
				IOUtils.copyLarge(input, output);
			} else {
				output.write(binary);
			}
			output.flush();
		} finally {
			IOUtils.closeQuietly(output);
			IOUtils.closeQuietly(input);
		}
	}

	*//**
	 * 课程作业管理页面初始化
	 * 
	 * @param
	 * @return
	 *//*
	@RequestMapping("/homeWorkReviewInit.action")
	public ModelAndView homeWorkReviewInit(String linkId, String linkType) {
		ModelAndView mv = new ModelAndView("/course/client/home_work_review");
		HomeworkView home = null;
		if (StringUtil.isNotEmpty(linkId) && StringUtil.isNotEmpty(linkType)) {
			home = courseService.searchworkDetail(linkId, linkType);
		}
		if (home == null) {
			home = new HomeworkView();
		}
		BaseEntity condition = new BaseEntity();
		condition.setPageSize(5);
		mv.addObject("linkId", linkId);
		mv.addObject("linkType", linkType);
		mv.addObject("title", home.getTitle());
		mv.addObject("courseTitle", home.getCourseTitle());
		mv.addObject("condition", condition);
		return mv;
	}

	*//**
	 * 导师品鉴作业初始化
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/homeworkReviewList.action")
	@ResponseBody
	public ResultBean homeworkReviewList(HomeworkView home) {

		return courseService.homeworkReviewList(home, PropertyUtil.getProperty("res.context.root"));
	}

	@RequestMapping("/gethomeworkReviewList.action")
	@ResponseBody
	public ResultBean gethomeworkReviewList(HomeworkView home) {
		return courseService.gethomeworkReviewList(home);
	}

	@RequestMapping("/gethomeworkReviewListByJson.action")
	@ResponseBody
	public JSONArray gethomeworkReviewListByJson(String courseId, String linkId, String linkType) throws IOException {

		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		List<HomeworkView> homeworkList = courseService.gethomeworkReviewListJson(courseId, linkId, linkType,
				PropertyUtil.getProperty("res.context.root"));
		if (homeworkList.size() > 0) {
			for (HomeworkView view : homeworkList) {
				Map<String, Object> courseERMap = new HashMap<String, Object>();
				courseERMap.put("userName", view.getCreateUser());
				courseERMap.put("audioContent", view.getContent() == null ? "" : view.getContent());
				// courseERMap.put("answerFileName", view.getAnswerFileName());
				courseERMap.put("score", view.getScore() == null ? "" : view.getScore());
				courseERMap.put("teacherComment", view.getTeacherComment() == null ? "" : view.getTeacherComment());
				// courseERMap.put("teacherScore", view.getTeacherScore() ==
				// null?"":view.getTeacherScore());
				// courseERMap.put("teacherComments", view.getTeacherComments() ==
				// null?"":view.getTeacherComments());
				resultList.add(courseERMap);
			}
		}
		jo.put("resultList", resultList);
		json.add(jo);
		return json;
	}

	*//**
	 * 导师评价
	 * 
	 * @author Id
	 * @param SCORE teacherComment
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/saveHomeworkReview.action")
	@ResponseBody
	public ResultBean saveHomeworkReview(HomeworkView home) {

		return courseService.saveHomeworkReview(home);
	}

	*//**
	 * 检索我的课程列表（检索用户课程，包括已关注、已学、已学完等检索条件以及检索列表中每一项课程的问卷）
	 * 
	 * @param finishType 完成类型（已收藏 1、已学2、3 已学完）
	 * @return 我的课程列表
	 * @author WUXIAOXIANG
	 *//*
	@RequestMapping("/userCourseList.action")
	@ResponseBody
	public ResultBean userCourseList(String finishType, Integer pageSize, Integer pageNumber) {
		// 检索我的课程
		ResultBean result = courseService.userCourseList(finishType, pageSize, pageNumber);
		return result;
	}

	@RequestMapping("/userCompulsoryList.action")
	@ResponseBody
	public ResultBean userCompulsoryList(String courseType, Integer pageSize, Integer pageNumber) {
		// 检索我的课程
		ResultBean result = courseService.userCompulsoryList(courseType, pageSize, pageNumber);
		return result;
	}

	*//**
	 * 检索我的作业列表
	 * 
	 * @author liuzhen
	 * @param
	 * @return 返回结果
	 *//*
	@RequestMapping("/userHomeworkList.action")
	@ResponseBody
	public ResultBean userHomeworkList(HomeworkView homeworkView, Integer pageSize, Integer pageNumber) {
		// 检索我的作业
		ResultBean result = courseService.userHomeworkList(homeworkView, pageSize, pageNumber);
		return result;
	}

	*//**
	 * 检索当前课程的所有作业
	 * 
	 * @author liuzhen
	 * @param
	 * @return 返回结果
	 *//*
	@RequestMapping("/alluserHomeworkList.action")
	@ResponseBody
	public ResultBean alluserHomeworkList(HomeworkView homeworkView, Integer pageSize, Integer pageNumber) {
		ResultBean result = courseService.getAlluserHomeworkList(homeworkView, pageSize, pageNumber);
		return result;
	}

	*//**
	 * 检索手机端导师个人中心的课程列表
	 * 
	 * @param pageSize   每页显示行数
	 * @param pageNumber 当前页
	 * @return 课程列表
	 * @author WUXIAOXIANG
	 *//*
	@RequestMapping("/teacherCourseAppList.action")
	@ResponseBody
	public ResultBean teacherCourseAppList(Integer pageSize, Integer pageNumber) {
		ResultBean result = courseService.teacherCourseAppList(pageSize, pageNumber);
		return result;
	}

	*//**
	 * 加入课程-支付详情初始化
	 * 
	 * @param courseId
	 * @return
	 *//*
	@SuppressWarnings({ "rawtypes" })
	@RequestMapping("/paydetail.action")
	public ModelAndView homeWorkReviewInit(String courseId) {
		ModelAndView mv = new ModelAndView("/course/client/course_pay_detail");
		UserEntity user = userService.searchById(WebContext.getSessionUserId());
		Map map = (Map) courseService.immediatePayDetail(courseId).getData();
		CourseView view = (CourseView) map.get("courseView");
		UserPoint userPoint = (UserPoint) map.get("userPoint");
		BigDecimal remainPoints = (BigDecimal) map.get("remainPoints");
		mv.addObject("user", user);
		mv.addObject("view", view);
		mv.addObject("userPoint", userPoint);
		mv.addObject("remainPoints", remainPoints);
		return mv;
	}

	*//**
	 * 用户管理页面 检索列表
	 * 
	 * @param condition
	 * @return 返回结果
	 *//*
	@RequestMapping("/search.action")
	@ResponseBody
	public ResultBean search(UserOperEntity condition) {
		return courseService.search4Page(condition);
	}

	@RequestMapping("/userOperInit.action")
	public ModelAndView userOperInit(String courseId) {
		ModelAndView result = new ModelAndView("/course/client/userOper");
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/mgrApprove.action")
	public ModelAndView mgrApprove(String courseId) {
		ModelAndView result = new ModelAndView("/course/client/mgr_approve");
		result = getRoleAndNum(result);
		return result;
	}
	
	@RequestMapping("/approveSearch.action")
	@ResponseBody
	public ResultBean approveSearch(CourseApproveEntity entity){
		return courseService.approveSearch(entity);
	}
	
	@RequestMapping("/saveHomeWork.action")
	@ResponseBody
	public ResultBean saveHomeWork(HomeworkView homework) throws Exception {
		return courseService.saveHomeWorkAnswer(homework);
	}

	@RequestMapping("/saveHomeWorkById.action")
	@ResponseBody
	public ResultBean saveHomeWorkById(HomeworkView homework) throws Exception {
		return courseService.saveHomeWorkById(homework);
	}
	
	@RequestMapping("/saveHomeWorkReviewById.action")
	@ResponseBody
	public ResultBean saveHomeWorkReviewById(String courseJson) throws Exception {
		HomeworkView homework = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, HomeworkView.class);
		return courseService.saveHomeWorkReviewById(homework);
	}

	@RequestMapping("/saveHomeWorkAnswerQuestion.action")
	@ResponseBody
	public ResultBean saveHomeWorkAnswerQuestion(HomeworkAnswerQuestionEntity aq) throws Exception {
		return courseService.saveHomeWorkAnswerQuestion(aq);
	}

	@RequestMapping("/answerQuestionList.action")
	@ResponseBody
	public ResultBean answerQuestionList(HomeworkAnswerQuestionEntity aq) {
		return courseService.searchAnswerQuestionList(aq);
	}

	@RequestMapping("/likeHomeWorkById.action")
	@ResponseBody
	public ResultBean likeHomeWorkById(String id) throws Exception {
		return courseService.likeHomeWorkById(id);
	}

	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/institutionlist.action")
	public ModelAndView institutionlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/institution/manager/institution_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		return result;
	}

	*//**
	 * 学校评价总列表
	 * 
	 * @return
	 *//*
	@RequestMapping("/institutionManger.action")
	public ModelAndView institutionManger() {
		ModelAndView result = new ModelAndView();
		UserEntity user = WebContext.getSessionUser();
		if (!"游客".equals(user.getPersonName())) {
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			TpUserEntity entity =new TpUserEntity();
			entity = userManagementService.searchById(userId);
			if ("A".equals(role) && "001".equals(entity.getDuty())) {
				result = new ModelAndView("/institution/manager/institutionManagerBack");
			} else if ("C".equals(role) || ("A".equals(role) && !"001".equals(entity.getDuty()))) {
				result = new ModelAndView("/institution/manager/institutionEvlutionStudentBack");
			}else if("B".equals(role)) {
				result = new ModelAndView("/institution/manager/institutionBack");
			}
		} else {
			result = new ModelAndView("/institution/manager/institutionEvlutionStudentBack");
		}
		
		return result;
	}
	

	*//**
	 * 标签一览
	 *//*
	@RequestMapping("/searchTagList2.action")
	@ResponseBody
	public ResultBean searchTagList2(TagInstitutionEntityView tagEntity) {
		return taxonomyInstitutionService.searchTagList2(tagEntity);

	}
	*//**
	 * 标签一览
	 *//*
	@RequestMapping("/searchInstitutionTagList2.action")
	@ResponseBody
	public ResultBean searchInstitutionTagList2(TagInstitutionEntityView tagEntity) {
		tagEntity.setCreateUser(WebContext.getSessionUserId());
		return taxonomyInstitutionService.searchInstitutionTagList2(tagEntity);

	}
	*//**
	 * 标签一览
	 *//*
	@RequestMapping("/searchTagSchoolList2.action")
	@ResponseBody
	public ResultBean searchTagSchoolList2(TagInstitutionEntityView tagEntity) {
		return taxonomyInstitutionService.searchTagList2(tagEntity);

	}
	*//**
	 * 分类基本列表
	 * 
	 * @return
	 *//*
	@RequestMapping("/institutioninit.action")
	public ModelAndView institutioninit() {
		ModelAndView mv = new ModelAndView("/institutiontaxonomy/manager/taxonomy_course_list");
		BaseEntity condition = new BaseEntity();
		condition.setPageSize(5);
		mv.addObject("condition", condition);
		return mv;
	}

	*//**
	 * 分类查询列表
	 * 
	 * @param businessType
	 * @return
	 *//*
	@RequestMapping("/searchTagTypeList2.action")
	@ResponseBody
	public List<TagTypeInstitutionView> searchTagTypeList2(String businessType) {

		return taxonomyInstitutionService.searchTagTypeList2(businessType);

	}

	*//**
	 * 
	 * 删除标签
	 * 
	 * @param tagIds
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/delTag.action")
	@ResponseBody
	public ResultBean delTag(String tagId, String ico) throws Exception {

		return taxonomyInstitutionService.delTag(tagId, ico);

	}

	*//**
	 * 保存信息
	 * 
	 * @param browserInfo
	 * @param sysInfo
	 * @param IpInfo
	 * @param operDetail
	 * @param operSite
	 * @return
	 *//*
	@RequestMapping("/insertUserInstitution.action")
	@ResponseBody
	public JSONArray insertUserInstitution(String browserInfo, String sysInfo, String IpInfo, String operDetail,
			String operSite) {
		UserOperEntity oper = new UserOperEntity();
		oper.setSysInfo(browserInfo + "(" + sysInfo + ")");
		oper.setOperDetail(operDetail);
		oper.setOperSite(operSite);
		oper.setOperTime(new Date());
		oper.setOperIP(IpInfo);
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		tagInstitutionEvalutionService.inserUserOper(oper);
		jo.put("result", "success");
		json.add(jo);
		return json;
	}

	*//**
	 * 编辑标签
	 * 
	 * @param TagEntity
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/updateTag.action")
	@ResponseBody
	public ResultBean updateTag(TagInstitutionEntity tagEntity) throws Exception {

		return taxonomyInstitutionService.updateTag(tagEntity);

	}

	*//**
	 * 保存新增标签
	 * 
	 * @param parentId 父标签ID,tagTypeId:标签类ID,tagName:标签名称
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveTag2.action")
	@ResponseBody
	public ResultBean saveTag2(TagInstitutionEntityView tagEntity) throws Exception {
		return taxonomyInstitutionService.saveTag2(tagEntity);

	}

	*//**
	 * 
	 * 初始化课程列表
	 * 
	 * @param condition 课程对象
	 * @return 返回课程列表初始化页面
	 *//*
	@RequestMapping("/institutionListInit.action")
	public ModelAndView institutionListInit(InstitutionMessageEntity condition) {
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		result.addObject("course", new InstitutionMessageEntity());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}

	*//**
	 * 
	 * 检索评价列表
	 * 
	 * @param queryInfo 机构对象
	 * @return 返回评价列表
	 *//*
	@RequestMapping("/listSearch.action")
	@ResponseBody
	public ResultBean listSearch(TbInstitutionEvalutionManagerView queryInfo) {
		queryInfo.setCreateUser(WebContext.getSessionUserId());
		return taxonomyInstitutionService.listSearch(queryInfo);
	}

	*//**
	 * 
	 * 检索评价问卷列表
	 * 
	 * @param queryInfo 机构对象
	 * @return 返回评价列表
	 *//*
	@RequestMapping("/listQestionSearch.action")
	@ResponseBody
	public ResultBean listQestionSearch(MeasurementInstitutionEntity queryInfo) {
		UserEntity user = WebContext.getSessionUser();	
		String role = user.getUserKbn();
		//判断权限
		String userId = user.getCreateUser();
		queryInfo.setCreateUser(userId);
		return taxonomyInstitutionService.listSchoolMeasurementSearch(queryInfo);
	}

	*//**
	 * 评价新增页面初始化
	 * 
	 * @return
	 *//*
	@RequestMapping("/addInit.action")
	public ModelAndView addInit() {
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_add");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		result.addObject("pageType", "add");
		result.addObject("course", new TbInstitutionEvalutionManagerView());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}

	*//**
	 * 搜素全部学校
	 * 
	 * @param query
	 * @return
	 *//*
	@RequestMapping("/searchSchoolAll.action")
	@ResponseBody
	public ResultBean searchSchoolAll(InstitutionEntity query) {
		return institutionService.getTeacherAll(query);
	}
	*//**
	 * 搜素评价问卷全部学校
	 * 
	 * @param query
	 * @return
	 *//*
	@RequestMapping("/searchEvalutionSchoolAll.action")
	@ResponseBody
	public ResultBean searchEvalutionSchoolAll(InstitutionEntity query) {
		if(null!=query && StringUtil.isNotBlank(query.getInstitutionNameseacher())) {
			query.setInstitutionName(query.getInstitutionNameseacher());
		}
		return institutionService.getEvalutionTeacherAll(query);
	}

	*//**
	 * 
	 * 根据学校ID，检索学校信息
	 * 
	 * @return
	 *//*
	@RequestMapping("/searchSchool.action")
	@ResponseBody
	public ResultBean searchSchool(String id) {
		ResultBean result = ResultBean.success();
		result.setStatus(true);
		return institutionService.searchSchool(id);
	}

	*//**
	 * 保存评价任务信息
	 * 
	 * @param courseJson 任务信息
	 * @return 返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/saveEvalutionInfo.action")
	@ResponseBody
	public ResultBean saveEvalutionInfo(String courseJson) throws Exception {
		TbInstitutionEvalutionManagerView course = JSONUtil.parse(
				JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson,
				TbInstitutionEvalutionManagerView.class);
		ResultBean result = ResultBean.success();
		course.setStatus("0");
		if (StringUtil.isEmpty(course.getId())) {
			institutionService.insertCourseCatagory(course);
		} else {
			institutionService.updateCourseAll(course);
		}
		return result;
	}

	*//**
	 * 暂存
	 * 
	 * @param courseJson
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/zancunEvalutionInfo.action")
	@ResponseBody
	public ResultBean zancunEvalutionInfo(String courseJson) throws Exception {
		TbInstitutionEvalutionManagerView course = JSONUtil.parse(
				JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson,
				TbInstitutionEvalutionManagerView.class);
		ResultBean result = ResultBean.success();
		course.setStatus("0");
		if (StringUtil.isEmpty(course.getId())) {
			institutionService.insertCourseCatagory(course);
		} else {
			institutionService.updateCourseAll(course);
		}
		return result;
	}

	*//**
	 * 删除评价任务信息
	 * 
	 * @param id
	 * @return
	 *//*
	@RequestMapping("/deleteEvalution.action")
	@ResponseBody
	public ResultBean deleteEvalution(String id) {
		ResultBean result = new ResultBean();
		institutionService.deleteEvalution(id);
		result.setMessages(getMessage(MSG_S_DELETE));
		return result;
	}

	@RequestMapping("/editInit.action")
	public ModelAndView editInit(String id) {
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_add");
		String status = "0";
		String type = "edit";
		TbInstitutionEvalutionManagerView course = tagInstitutionEvalutionService.searchEvalutionById(id, type);
		String evalutionId = course.getId();
		String institutionId = course.getTeacherId();
		List<TagLinkInstitutionEntity> entityList = new ArrayList<TagLinkInstitutionEntity>();
		TagLinkInstitutionEntity entity = new TagLinkInstitutionEntity();
		entity.setLinkId(evalutionId);
		entityList = tagInstitutionEvalutionService.searchList(entity);
		if (null != entityList) {
			for (int i = 0; i < entityList.size(); i++) {
				if (StringUtils.isBlank(entityList.get(i).getParentTagId())) {
					TagInstitutionEntity isentity = new TagInstitutionEntity();
					isentity.setTagId(entityList.get(i).getTagId());
					isentity = tagInstitutionService.searchFirstData(isentity);
					course.setFirstTagName(isentity.getTagName());
					course.setFirstTag(isentity.getTagName());
				} else {
					TagInstitutionEntity isentity = new TagInstitutionEntity();
					isentity.setTagId(entityList.get(i).getTagId());
					isentity = tagInstitutionService.searchFirstData(isentity);
					course.setSecondTag(isentity.getTagName());
					course.setSecondTagName(isentity.getTagName());
				}
			}
		}
		InstitutionEntity institution = new InstitutionEntity();
		institution = institutionService.selectInstitutionById(institutionId);
		course.setInstitutionName(institution.getInstitutionName());
		course.setSchoolType(institution.getSchoolType());
		course.setInstitutionAbbreviation(institution.getInstitutionAbbreviation());
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		result.addObject("tagsSecond", tagsSecond);
		result.addObject("course", course);
		result.addObject("pageType", type);
		return result;
	}

	*//**
	 * 问卷添加
	 * 
	 * @param courseId 评价任务ID
	 * @return
	 *//*
	@RequestMapping("/measureInit.action")
	@ResponseBody
	public ModelAndView measureInit(String courseId) {
		ModelAndView result = new ModelAndView("institution/manager/insitution_measure_add");
		MeasurementInstitutionEntity measure = new MeasurementInstitutionEntity();
		result.addObject("measure", measure);
		result.addObject("courseId", courseId);
		result.addObject("pageType", "add");
		return result;
	}
	*//**
	 * 问卷添加
	 * 
	 * @param courseId 评价任务ID
	 * @return
	 *//*
	@RequestMapping("/measureInit1.action")
	@ResponseBody
	public ModelAndView measureInit1(String courseId) {
		ModelAndView result = new ModelAndView("institution/manager/insitution_measure_editor");
		MeasurementInstitutionEntity measure = new MeasurementInstitutionEntity();
		measure.setLinkId(courseId);
		measure = measurementInstitutionService.findInstitutionMeasurement(measure);
		List<QuestionInstitutionEntity> questionList = measurementInstitutionService.searchQuestionList(measure.getId());
		measure.setQuestionList(questionList);
		result.addObject("measure", measure);
		result.addObject("courseId", courseId);
		result.addObject("pageType", "add");
		return result;
	}
	*//**
	 * 
	 * 问卷编辑页面初始化
	 * 
	 * @param courseId
	 * @return
	 *//*
	@RequestMapping("/measureInsitutionEditInit.action")
	@ResponseBody
	public ModelAndView measureEditInit(String courseId) {
		ModelAndView result = new ModelAndView("institution/manager/insitution_measure_add");
		MeasurementInstitutionEntity measure = measurementInstitutionService.seachMeasureById(courseId);
		result.addObject("measure", measure);
		result.addObject("courseId", courseId);
		result.addObject("isOnlyView", true);
		result.addObject("pageType", "edit");
		return result;
	}

	*//**
	 * 
	 * 问卷添加/编辑
	 * 
	 * @param measureJson
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveMeasure.action")
	@ResponseBody
	public ResultBean saveMeasure(String measureJson) throws Exception {
		MeasurementInstitutionEntity measure = JSONUtil.parse(
				JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), measureJson,
				MeasurementInstitutionEntity.class);
		ResultBean result = ResultBean.success();
		if (StringUtil.isEmpty(measure.getId())) {
			measurementInstitutionService.saveMeasure(measure);
		} else {
			measurementInstitutionService.updateInstitutionMeasure(measure);
		}
		return result;
	}

	*//**
	 * 
	 * 初始化评价问卷列表
	 * 
	 * @param condition 评价对象
	 * @return 返回课程列表初始化页面
	 *//*
	@RequestMapping("/institutionQuestListInit.action")
	public ModelAndView institutionQuestListInit(InstitutionMessageEntity condition) {
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_question_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		result.addObject("course", new InstitutionMessageEntity());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}

	*//**
	 * 
	 * 评价的发布/取消发布
	 * 
	 * @param id     课程id
	 * @param status 评价状态：0 未发布 1 已发布
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/publicEvalution.action")
	@ResponseBody
	public ResultBean publicEvalution(String id, String status) throws Exception {
		tagInstitutionEvalutionService.publicEvalution(id, status);
		return ResultBean.success(MSG_S_SUBMIT);
	}

	*//**
	 * 问卷显示页面
	 *//*
	@RequestMapping("/question.action")
	public ModelAndView question(String linkId, String linkType) {
		ModelAndView result = new ModelAndView("/institution/manager/institution_evalution_questionner_item");
		result.addObject("linkId", linkId);
		result.addObject("linkType", linkType);
		return result;
	}

	*//**
	 * 问卷题目选项
	 *//*
	@RequestMapping("/questionnaireDetial.action")
	@ResponseBody
	public ResultBean questionnaireDetial(String linkId, String linkType) {
		return questionInstitutionService.questionnaireDetial(linkId, linkType);
	}

	*//**
	 * 问卷提交
	 *//*
	@RequestMapping("/submitQuestionnaire.action")
	@ResponseBody
	public ResultBean submitQuestionnaire(String questionIds, String optionIds, String measureId) {
		return questionInstitutionService.submitQuestionnaire(questionIds, optionIds, measureId);
	}

	*//**
	 * 问卷调查结果列表
	 * 
	 * @author heyaqin
	 * @return 返回结果
	 *//*
	@RequestMapping("/questionResultList.action")
	public ModelAndView questionResultList(MeasurementInstitutionEntity condition) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_result_list");
		mv.addObject("condition", condition);
		UserEntity user = WebContext.getSessionUser();
		if (!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if (roleList.size() > 0) {
				for (UserRoleEntity role : roleList) {
					if (role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			} else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}

	*//**
	 * 问卷调查列表检索
	 * 
	 * @author
	 * @return 返回结果
	 *//*
	@RequestMapping("/questionResultListSearch.action")
	@ResponseBody
	public ResultBean questionResultListSearch(MeasurementInstitutionEntity condition) {
			UserEntity user = WebContext.getSessionUser();	
			String userId = user.getCreateUser();
			condition.setCreateUser(userId);
		return userManagementService.selectQuestionResultList(condition);
	}

	*//**
	 * 问卷统计列表初始化
	 * 
	 * @author
	 * @param id 问卷ID
	 * @return 返回结果
	 *//*
	@RequestMapping("/viewMeasurementListInit.action")
	public ModelAndView viewMeasurementListInit(String id) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_statistics");
		int optionNum = userManagementService.searchInstitutionTotalOptions(id) + 1;
		QuestionOptionInstitutionEntity questionOptionEntity = new QuestionOptionInstitutionEntity();
		questionOptionEntity.setMeasureId(id);
		mv.addObject("questionOptionEntity", questionOptionEntity);
		mv.addObject("optionNum", optionNum);
		// 问卷统计结果列表
		ArrayList<Map<String, String>> optionMapList = userManagementService.selectInstitutionOptionsList(id);
		mv.addObject("optionMapList", optionMapList);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 问卷统计列表检索
	 * 
	 * @author HEYAQIN
	 * @param id 问卷ID
	 * @return 返回结果
	 *//*
	@RequestMapping("/viewMeasurementList.action")
	@ResponseBody
	public ArrayList<Map<String, String>> viewMeasurementList(String id) {
		if (StringUtil.isEmpty(id)) {
			return null;
		} else {
			ArrayList<Map<String, String>> optionMapList = userManagementService.selectInstitutionOptionsList(id);
			return optionMapList;
		}
	}

	*//**
	 * 问卷参与人员 检索
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/viewUserSearch.action")
	@ResponseBody
	public ResultBean viewUserSeacher(String measurementId) {
		return userManagementService.selectEvalutionQuestionUserList(measurementId);
	}

	*//**
	 * 问卷参与人员回答详情
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/viewDetailInit.action")
	public ModelAndView viewDetailInit(UserMeasurementResultEntity condition) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_user_detail");
		if (StringUtil.isEmpty(condition.getMeasurementId())) {
			throw new BusinessException("参数错误!");
		}

		List<UserMeasurementResultEntity> list = measuermentService
				.searchInstitutionUserMeasurementResultByEntiy(condition);
		if (list != null && list.size() != 0) {
			mv.addObject("condition", list.get(0));
		} else {
			mv.addObject("condition", new UserMeasurementResultEntity());
		}
		return mv;
	}

	*//**
	 * 问卷回答详情
	 * 
	 * @param measurementId
	 * @param userId
	 * @return
	 *//*
	@RequestMapping("/viewUserAnswerSearch.action")
	@ResponseBody
	public ResultBean viewUserAnswerSearch(String measurementId, String userId) {
		return userManagementService.selectEvalutionAnswerUserList(measurementId, userId);
	}

	*//**
	 * 检索测试信息
	 * 
	 * @param linkId
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchMeasuerment.action")
	@ResponseBody
	public ResultBean searchMeasuerment(String measurementId, String userId) {
		if (StringUtil.isEmpty(measurementId)) {
			return ResultBean.error(getMessage("参数错误!"));
		}
		ResultBean result = ResultBean.success();
		List<QuestionInstitutionEntity> questionList = measurementInstitutionService
				.searchMeasuermentInstitutionNormalAndExam(userId, measurementId);
		result.setData(questionList);
		return result;
	}

	*//**
	 * 问卷参与人员 画面初始化
	 * 
	 * @param condition
	 * @return
	 *//*
	@SuppressWarnings("unchecked")
	@RequestMapping("/viewUserInit.action")
	public ModelAndView viewUserInit(MeasurementInstitutionEntity condition) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_user_list");
		mv.addObject("measurementId", condition.getId());
		List<MeasurementInstitutionEntity> list = measurementInstitutionService.search(condition);
		if (list != null && list.size() != 0) {
			mv.addObject("condition", list.get(0));
		} else {
			throw new BusinessException("参数错误!");
		}
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 删除问卷调查列表
	 * 
	 * @author heyaqin
	 * @return 返回结果
	 *//*
	@RequestMapping("/deleteQuestionList.action")
	@ResponseBody
	public ResultBean deleteQuestionList(String id) {
		userManagementService.deleteEvalutionQuestionList(id);
		return ResultBean.success();
	}

	*//**
	 * 导入页面初始化
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/importInit.action")
	public String importInit() {
		return "institution/manager/institution_import";
	}

	*//**
	 * 导入机构
	 * 
	 * @param linkId
	 * @return 返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/importUpload.action")
	@ResponseBody
	public ResultBean importCourse(@RequestParam("institution") CommonsMultipartFile institution,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		InputStream input = null;
		try {
			input = institution.getInputStream();
			File targetFile = FileUtil.writeToTmpFile(input);
			return institutionService.importInstitution(targetFile);
		} finally {
			IOUtils.closeQuietly(input);
		}
	}

	*//**
	 * 批量导出机构
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/exportInstitution.action")
	@ResponseBody
	public InstitutionManagerView exportInstitution(InstitutionEntity entity) throws Exception {
		List<InstitutionManagerView> viewList = new ArrayList<InstitutionManagerView>();
		UserEntity userEntity = WebContext.getSessionUser();
		if (!"游客".equals(userEntity.getPersonName())) {
			String role = userEntity.getUserKbn();
			String userId=userEntity.getUserId();
			if("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				entity.setCreateUser(userId);
				viewList = institutionService.exportInstitution(entity);
			}else if("B".equals(role)) {
				entity.setCreateUser(userId);
				viewList = institutionService.exportManagerInstitution(entity);
			}
		}
		
		InstitutionManagerView user = new InstitutionManagerView();
		user.setInstitutionList(viewList);
		ExcelDataBind edbUser = new ExcelDataBind(user);
		InputStream is = null;
		OutputStream os = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String date = sdf.format(new Date());
			String fileName = URLEncoder.encode("组织机构" + date + ".xlsx", "UTF-8");
			WebContext.getResponse().reset();
			is = InstitutionService.class.getResourceAsStream("institutionExportTemplate.xlsx");
			WebContext.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			WebContext.getResponse()
					.setContentType(" application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8");
			edbUser.bind(is, WebContext.getResponse().getOutputStream());
		} catch (IOException ex) {

		} finally {

			IOUtils.closeQuietly(is);
			IOUtils.closeQuietly(os);
		}
		return null;
	}

	*//**
	 * 增加机构方法
	 * 
	 * @param entity
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/addJyInstitution.action")
	public ModelAndView addJyInstitution(InstitutionEntity entity) throws Exception {
		ModelAndView mv = new ModelAndView("institution/manager/institution_add");
		InstitutionEntity institutionEntity = new InstitutionEntity();
		UserEntity user = WebContext.getSessionUser();
		String role = user.getUserKbn();
		//判断权限
		List<InstitutionEntity> schoollist=new ArrayList<InstitutionEntity>();
		String userId = user.getCreateUser();
		if ("A".equals(role)) {
			TpUserEntity tpUserEntity =new TpUserEntity();
			tpUserEntity = userManagementService.searchById(userId);
			institutionEntity.setInstitutionName(tpUserEntity.getSchoolName());
			institutionEntity=institutionService.seacherPersonEdution(institutionEntity);
			if(null!=institutionEntity) {
				entity.setInstitutionName(institutionEntity.getInstitutionName());
			}
			schoollist.add(institutionEntity);
		}else if("B".equals(role)) {
			institutionEntity.setCreateUser(userId);
			institutionEntity= institutionService.searchEdution(institutionEntity);
            if(null!=institutionEntity) {
            	entity.setInstitutionName(institutionEntity.getInstitutionName());
			}
            schoollist.add(institutionEntity);
		}
		mv.addObject("schoollist", schoollist);
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 添加学校信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/addInstitution.action")
	public ModelAndView addInstitution(String institutionParentId) throws Exception {
		InstitutionEntity entity = new InstitutionEntity();
		entity.setInstitutionParentId(institutionParentId);
		ModelAndView mv = new ModelAndView("institution/manager/institution_addSchool");
		List<InstitutionEntity> schoollist=new ArrayList<InstitutionEntity>();
		UserEntity user = WebContext.getSessionUser();
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				schoollist = institutionService.seacherSchoolLocationList(entity);
			} else if("B".equals(role)) {
				entity.setCreateUser(userId);
				schoollist= institutionService.searchEdutionSchoolList(entity);
			}
		
		mv.addObject("schoollist", schoollist);
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 删除班级
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/deleteInstitutionById.action")
	@ResponseBody
	public ResultBean deleteGradeById(String id) throws Exception {
		InstitutionEntity entity = new InstitutionEntity();
		entity.setId(id);
		institutionService.deleteInstitutionById(entity);
		return ResultBean.success(DELETE_GRADE_SUCCESS);
	}

	*//**
	 * 跳转到班级编辑页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/institutionEdit.action")
	public ModelAndView institutionEdit(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		ModelAndView mv = new ModelAndView("institution/manager/institution_edit");
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 检索班级信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchInstitution.action")
	@ResponseBody
	public ResultBean searchNotice(InstitutionEntity entity) {
		return institutionService.searchInstitutionEvalutionList(entity);
	}
	*//**
	 * 学校检索班级信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/schoolsearchInstitution.action")
	@ResponseBody
	public ResultBean schoolsearchInstitution(InstitutionEntity entity) {
		return institutionService.searchInstitutionEvalutionList1(entity);
	}
	*//**
	 * 检索班级信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchSchoolInstitution.action")
	@ResponseBody
	public ResultBean searchSchoolInstitution(InstitutionEntity entity) {
		ResultBean result = new ResultBean();
		UserEntity user = WebContext.getSessionUser();
		if (!"游客".equals(user.getPersonName())) {
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				entity.setCreateUser(userId);
				result=institutionService.searchInstitutionTeacherEvalutionList(entity);
			} else if ("C".equals(role)) {
				entity.setCreateUser(userId);
				StudentClassInfoEntity classInfo = new StudentClassInfoEntity();
				classInfo.setStudentId(userId);
				classInfo = studentClassInfoService.searchStudentClassInfo(classInfo);
			    if(null!=classInfo) {
			    	entity.setInstitutionName(classInfo.getSchoolName());
					result= institutionService.searchInstitutionEvalutionList(entity);
			    }
			}else if("B".equals(role)) {
				entity.setCreateUser(userId);
				result= institutionService.searchInstitutionManagerEvalutionList(entity);
			}
		}
		return result;
	}

	*//**
	 * 新增信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/insertInstitution.action")
	@ResponseBody
	public ResultBean insertInstitution(InstitutionEntity entity) throws Exception {
		ResultBean result = new ResultBean();
		InstitutionEntity institutionEntity = new InstitutionEntity();
		institutionEntity.setInstitutionName(entity.getInstitutionName());
		List<InstitutionEntity> datalist = (List<InstitutionEntity>) institutionService.searchInstitutionEvalutionList1(institutionEntity).getData();
		if(null!=datalist && datalist.size()>0) {
			result.setStatus(false);
			result = ResultBean.error("学校名称已存在");
		}else {
//			String institutionType=entity.getInstitutionType();
//			TagInstitutionEntity tagInstitutionEntity = new TagInstitutionEntity();
//			tagInstitutionEntity.setTagId(institutionType);
//			tagInstitutionEntity = tagInstitutionService.searchFirstData(tagInstitutionEntity);
//			entity.setInstitutionType(tagInstitutionEntity.getTagName());
			if(StringUtil.isNotBlank(entity.getSchoolNatrue())) {
				if("1".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("小学");
				}else if("2".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("初中");
				}else if("3".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("高中");
				}else if("4".equals(entity.getSchoolNatrue())) {
				   entity.setInstitutionType("九年一贯制");
			    }
			}
			institutionService.insertInstitution(entity, getCompressConfigByName("imageP"));
			result.setStatus(true);
			result= ResultBean.success(MSG_ADD_GRADE_SUCCESS);
		}
		return result;
	}
	*//**
	 * 新增信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/insertSchoolEvalutionInstitution.action")
	@ResponseBody
	public ResultBean insertSchoolEvalutionInstitution(InstitutionEntity entity) throws Exception {
		institutionService.insertInstitution(entity, getCompressConfigByName("imageP"));
		return ResultBean.success(MSG_ADD_GRADE_SUCCESS);
	}
	*//**
	 * 编辑保存
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/updateInstitution.action")
	@ResponseBody
	public ResultBean updateInstitution(InstitutionEntity entity) throws Exception {
		ResultBean result = new ResultBean();
		InstitutionEntity institutionEntity = new InstitutionEntity();
		institutionEntity.setInstitutionName(entity.getInstitutionName());
		List<InstitutionEntity> datalist = (List<InstitutionEntity>) institutionService.searchInstitutionEvalutionList1(institutionEntity).getData();
		if(null!=datalist && datalist.size()>0 && !entity.getId().equals(datalist.get(0).getId())) {
			result.setStatus(false);
			result = ResultBean.error("学校名称已存在");
		}else {
//			String institutionType=entity.getInstitutionType();
//			TagInstitutionEntity tagInstitutionEntity = new TagInstitutionEntity();
//			tagInstitutionEntity.setTagId(institutionType);
//			tagInstitutionEntity = tagInstitutionService.searchFirstData(tagInstitutionEntity);
//			entity.setInstitutionType(tagInstitutionEntity.getTagName());
			if(StringUtil.isNotBlank(entity.getSchoolNatrue())) {
				if("1".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("小学");
				}else if("2".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("初中");
				}else if("3".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("高中");
				}else if("4".equals(entity.getSchoolNatrue())) {
					   entity.setInstitutionType("九年一贯制");
				}
			}
			institutionService.updateInstitution(entity, getCompressConfigByName("imageP"));
			result.setStatus(true);
			result = ResultBean.success("创建学校成功");
		}
		return result;
	}
	*//**
	 * 编辑保存
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/updateSchoolEvalutionInstitution.action")
	@ResponseBody
	public ResultBean updateSchoolEvalutionInstitution(InstitutionEntity entity) throws Exception {
		entity.setCreateUser(WebContext.getSessionUserId());
		institutionService.updateInstitution(entity, getCompressConfigByName("imageP"));
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/institutionStudentlist.action")
	public ModelAndView institutionStudentlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/institution/manager/institution_student_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		return result;
	}

	*//**
	 * 跳转到班级查看页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/institutionLook.action")
	public ModelAndView institutionLook(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		ModelAndView mv = new ModelAndView("institution/manager/institution_look");
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 老师分享作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/shareWork.action")
	@ResponseBody
	public ResultBean shareWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.shareHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 老师取消分享作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/cancleShareWork.action")
	@ResponseBody
	public ResultBean cancleShareWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.cancleShareHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);

	}

	*//**
	 * 老师置顶作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/topWork.action")
	@ResponseBody
	public ResultBean topWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.topHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 老师取消置顶作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/cancleTopWork.action")
	@ResponseBody
	public ResultBean cancleTopWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.cancleTopHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 老师删除作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/deleteWork.action")
	@ResponseBody
	public ResultBean deleteWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.deleteWork(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/baseinstitutionlist.action")
	public ModelAndView baseinstitutionlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/baseinstitution/manager/base_institution_list");
		result.addObject("condition", condition);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/schoolbaseinstitutionlist.action")
	public ModelAndView schoolbaseinstitutionlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/baseinstitution/manager/base_school_institution_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 增加机构方法
	 * 
	 * @param entity
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/addBaseInstitution.action")
	public ModelAndView addBaseInstitution(InstitutionEntity entity) throws Exception {
		ModelAndView mv = new ModelAndView("baseinstitution/manager/base_institution_add");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList1(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		List<TagInstitutionEntity> tagList = tagInstitutionService.searchTagInstitutionAll();
		mv.addObject("tagList", tagList);
		mv.addObject("catagoryTags", tags);
		mv.addObject("tagsSecond", tagsSecond);
		mv.addObject("institution", entity);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 跳转到班级编辑页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/baseinstitutionEdit.action")
	public ModelAndView baseinstitutionEdit(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		ModelAndView mv = new ModelAndView("baseinstitution/manager/base_institution_edit");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList1(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		List<TagInstitutionEntity> tagList = tagInstitutionService.searchTagInstitutionAll();
        String secondId = entity.getFirstTag();
        TagInstitutionEntity taginfo = tagInstitutionService.searchTagInstitution(secondId);
        entity.setSecondTag(taginfo.getParentId());
		mv.addObject("tagList", tagList);
		mv.addObject("catagoryTags", tags);
		mv.addObject("tagsSecond", tagsSecond);
		mv.addObject("institution", entity);
		mv = getRoleAndNum(mv);
		return mv;
	}
	
	*//**
	 * 跳转到班级编辑页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/baseSchoolinstitutionEdit.action")
	public ModelAndView baseSchoolinstitutionEdit(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		InstitutionEntity institutionEntity = new InstitutionEntity();
		ModelAndView mv = new ModelAndView("baseinstitution/manager/base_school_institution_edit");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList1(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		List<TagInstitutionEntity> tagList = tagInstitutionService.searchTagInstitutionAll();
		List<InstitutionEntity> schoollist=new ArrayList<InstitutionEntity>();
		UserEntity user = WebContext.getSessionUser();
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			if(StringUtil.isNotBlank(entity.getInstitutionParentId())) {
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				schoollist = institutionService.seacherSchoolLocationList(entity);
			} else if("B".equals(role)) {
				entity.setCreateUser(userId);
				schoollist= institutionService.searchEdutionSchoolList(entity);
			}
			
		}else {
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				institutionEntity.setInstitutionName(tpUserEntity.getSchoolName());
				institutionEntity=institutionService.seacherPersonEdution(institutionEntity);
				entity.setInstitutionName(institutionEntity.getInstitutionName());
				schoollist.add(institutionEntity);
			}else if("B".equals(role)) {
				institutionEntity.setCreateUser(userId);
				institutionEntity= institutionService.searchEdution(institutionEntity);
				entity.setInstitutionName(institutionEntity.getInstitutionName());
				schoollist.add(institutionEntity);
			}
		}
		
		mv.addObject("schoollist", schoollist);
		mv.addObject("tagList", tagList);
		mv.addObject("catagoryTags", tags);
		mv.addObject("tagsSecond", tagsSecond);
		mv.addObject("institution", entity);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 分类基本列表
	 * 
	 * @return
	 *//*
	@RequestMapping("/baseinstitutioninit.action")
	public ModelAndView baseinstitutioninit() {
		ModelAndView mv = new ModelAndView("baseinstitutiontaxonomy/manager/base_taxonomy_course_list");
		BaseEntity condition = new BaseEntity();
		condition.setPageSize(5);
		mv.addObject("condition", condition);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 学校列表页面
	 *//*
	@RequestMapping("/schoolListInit.action")
	public ModelAndView schoolListInit(String schoolName) {
		ModelAndView result = new ModelAndView("baseinstitution/manager/school_list");
		String userId= WebContext.getSessionUserId();
		ManagerUserEntity entity= managerUserService.searchByUserId(userId);
		String mangerRole="0";
		if(null!=entity || "2542668".equals(userId)) {
			mangerRole="1";
		}else {
			mangerRole="0";
		}
		result.addObject("schoolName", schoolName);
		result.addObject("mangerRole", mangerRole);
		result = getRoleAndNum(result);
		return result;
	}

	*//**
	 * 检索课程 包括分类
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 * directionTagId 学段
	 * categoryTagId 分类
	 *//*
	@RequestMapping("/schoolList.action")
	@ResponseBody
	public ResultBean schoolList(InstitutionEntity course, String directionTagId, String categoryTagId,
			String courseStage, String sortType, Integer pageSize, Integer pageNumber,String schoolStage) {
		ResultBean result = new ResultBean();
		if(StringUtil.isNotBlank(course.getSchoolName())) {
			course.setInstitutionName(course.getSchoolName());
		}
		//区域
		if(StringUtil.isNotBlank(directionTagId)) {
			course.setFirstTag(directionTagId);
		}
		if(StringUtil.isNotBlank(directionTagId)) {
			TagInstitutionEntity entity = new TagInstitutionEntity();
			entity.setTagId(directionTagId);
			entity = tagInstitutionService.searchFirstData(entity);
			String xdName = entity.getTagName();
			course.setInstitutionType(xdName);
		}
		if(StringUtil.isNotBlank(categoryTagId)) {
			TagInstitutionEntity entity = new TagInstitutionEntity();
			entity.setTagId(categoryTagId);
			entity = tagInstitutionService.searchFirstData(entity);
			String xdName = entity.getTagName();
			course.setFirstTag(categoryTagId);
		}
		//学校阶段
		if(StringUtil.isNotBlank(schoolStage)) {
			course.setSchoolNatrue(schoolStage);
		}
		//权限查看
		String userId = WebContext.getSessionUserId();
		ManagerUserEntity entity= managerUserService.searchByUserId(userId);
		//教育局的人员暂时看到全部
		if(null!=entity || "2542668".equals(userId) || "B".equals(WebContext.getSessionUser().getUserKbn())) {
			result = courseService.schoolList(course, directionTagId, categoryTagId, courseStage, sortType,
					pageSize, pageNumber);
		}else {
			String role = WebContext.getSessionUser().getUserKbn();
			course.setCreateUser(userId);
			if("A".equals(role)) {
				result = courseService.schooTeacherlList(course, directionTagId, categoryTagId, courseStage, sortType,
						pageSize, pageNumber);
			}else if("C".equals(role)) {
				result = courseService.schooStudentlList(course, directionTagId, categoryTagId, courseStage, sortType,
						pageSize, pageNumber);
			}
		}
		
		return result;
	}
	*//**
	 * 日志操作页面 检索列表
	 * 
	 * @param condition
	 * @return 返回结果
	 *//*
	@RequestMapping("/logsearch.action")
	@ResponseBody
	public ResultBean logsearch(UserOperEntity condition) {
		return courseService.search4Page(condition);
	}
	*//**
	 * 已完成
	 * @param finishType
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 *//*
	@RequestMapping("/userCourseFinishList.action")
	@ResponseBody
	public ResultBean userCourseFinishList(String finishType, Integer pageSize, Integer pageNumber) {
		finishType ="3";
		pageSize =5;
		pageNumber=1;
		// 检索我的课程
		ResultBean result = courseService.userCourseList(finishType, pageSize, pageNumber);
		return result;
	}
	*//**
	 * 已完成
	 * @param finishType
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 *//*
	@RequestMapping("/userCourseUnFinishList.action")
	@ResponseBody
	public ResultBean userCourseUnFinishList(String finishType, Integer pageSize, Integer pageNumber) {
		pageSize =5;
		pageNumber=1;
		// 检索我的课程
		ResultBean result = courseService.userCourseUnFinishList(finishType, pageSize, pageNumber);
		return result;
	}
    *//**
                  * 作业列表
     * @return
     *//*
	@RequestMapping("/zuoyeList.action")
	@ResponseBody
	public ResultBean zuoyeList() {
		// 检索我的课程
		ResultBean result = courseService.userZyList();
		return result;
	}
    *//**
     * 
                  * 考试结果查看
     * @param UserMeasurementResultEntity condition 入参
     * @return
     *//*
    @RequestMapping("/userExamDetail.action")
    public ModelAndView userExamDetail(UserMeasurementResultEntity condition) {
    	//考试结果页面
        ModelAndView result = new ModelAndView("course/manager/user_exam_detail");
        //考试结果id是否存在
        if(StringUtil.isEmpty(condition.getMeasurementId())){
            throw new BusinessException("参数错误!");
        }
        condition.setCreateUser(WebContext.getSessionUserId());
        //查询考试结果
        List<UserMeasurementResultEntity> list = measuermentService.searchUserMeasurementResultByEntiy(condition);
        if(list != null && list.size() != 0) {
            result.addObject("condition", list.get(0));
        } else {
            result.addObject("condition", new UserMeasurementResultEntity());
        }
        //返回考试结果
        return result;
    }
	*//**
	 * 查询全部考试
	 * @param ChapterEntity chapter章节参数
	 * @param pageSize  每页展示多少条数据
	 * @param pageNumber 页码
	 * @return
	 *//*
	@RequestMapping("/myAllExam.action")
	public ModelAndView myAllExam(ChapterEntity chapter, Integer pageSize, Integer pageNumber) {
		//考试页面内容展示路径
		ModelAndView mv = new ModelAndView("baseinstitution/manager/my_exam_list");
		//查询数据表
		ResultBean result = chapterService.myAllxam(chapter, pageSize, pageNumber);
		mv.addObject("chapter", result.getData());
		return mv;
	}
	*//**
	 *检索考试
	 * @param chapter
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 *//*
	@RequestMapping("/myAllExamList.action")
	@ResponseBody
	public ResultBean myAllExamList(ChapterEntity chapter, Integer pageSize, Integer pageNumber) {
		ResultBean result = chapterService.myAllxam(chapter, pageSize, pageNumber);
		return result;
	}
	*//**
	 * 查询课工厂方法
	 * @param courseId 课程id
	 * @param content 课程内容
	 * @param type 课程类型
	 * @return
	 *//*
	@RequestMapping("/factory.action")
    @ResponseBody
    public ResultBean factory(String courseId,String content,String type)  {
        ResultBean result = new ResultBean();
        result = courseService.factory(courseId,content,type);
        return result;
    }
	*//**
	 * 用户作业列表
	 * @return
	 *//*
	@RequestMapping("/userHomeWork.action")
	public ModelAndView userHomeWork() {
		//作业列表路径
		ModelAndView result = new ModelAndView("/course/client/user_home_work");
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 查询作业列表
	 * @return
	 *//*
	@RequestMapping("/userHomeWorkSearch.action")
	@ResponseBody
	public ResultBean search(){
		//返回结果
		return courseService.searchUserHomeWork4Page();
	}
	*//**
	 * 查询学校信息
	 * @return
	 *//*
	@RequestMapping("/selectSchoolNameInfo.action")
	@ResponseBody
	public ResultBean selectSchoolNameInfo() {
		ResultBean result = new ResultBean();
		//获取登录信息
		String userId = WebContext.getSessionUserId();
		//获取角色信息
		String role = WebContext.getSessionUser().getUserKbn();
		//判断角色
		if("C".equals(role)) {
			//学生角色
			StudentClassInfoEntity entity =new StudentClassInfoEntity();
			entity.setStudentId(userId);
			List<StudentClassInfoEntity> stuList = studentClassInfoService.searchStudentClassInfoList(entity);
			stuList.get(0).setUserId(userId);
			//设置返回值数据
			result.setData(stuList.get(0));
		}else {
			//教师信息
			TpUserEntity tpUserEntity = userManagementService.searchById(userId);
			result.setData(tpUserEntity);
		}
		 
		return result;
	}
	*//**
	 * 结束课程方法
	 * @param id 课程id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/endCourse.action")
	@ResponseBody
	public ResultBean endCourse(String id) throws Exception {
		courseService.endCourse(id);
		return ResultBean.success(MSG_S_SUBMIT);
	}
	*//**
	 * 图书馆课程复制
	 * @param id 课程id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/toLibCourse.action")
	@ResponseBody
	public ResultBean toLibCourse(String id) throws Exception {
		courseService.toLibCourse(id);
		return ResultBean.success(MSG_S_SUBMIT);
	}
	*//**
	 * 课程复制方法
	 * @param id 课程主键
	 * @return
	 *//*
	@RequestMapping("/copyInfo.action")
    @ResponseBody
    public ResultBean copyInfo(String id)  {
        ResultBean result = new ResultBean();
        //课程复制实现方法
        courseService.copyCourseByHtml(id);
        result.setMessages(getMessage(MSG_S_SAVE));
        return result;
    }
    *//**
                 * 新增课程活动记录方法
     * @param courseId 课程id
     * @return
     *//*
	@RequestMapping("/addActiveInit.action")
	public ModelAndView addActiveInit(String courseId) {
	//课程活动新增页面路径
	ModelAndView result = new ModelAndView("course/manager/course_active_add");
	TagView tagQuery = new TagView();
	//一级分类对象
	List<TagView> tags = courseService.searchTagList2(tagQuery);
	//二级分类对象
	List<TagView> tagsSecond = courseService.searchTagSecondList2(tagQuery);
	//查询章节信息
	ChapterEntity entity = new ChapterEntity();
	entity.setCourseId(courseId);
	List<ChapterEntity> chapterList = courseService.selectByCourseId(entity);
	//返回前台对象
	result.addObject("catagoryTags", chapterList);
	//result.addObject("catagoryTags", tags);
	result.addObject("pageType", "add");
	result.addObject("course", new CourseManagerView());
	result.addObject("tagsSecond", tagsSecond);
	result = getRoleAndNum(result);
	return result;
	}
    *//**
                  * 保存课程章节活动记录信息
     * @param courseJson 课程信息
     * @return 返回结果
     * @throws Exception 
     *//*
    @RequestMapping("/saveActiveChapterInfo.action")
    @ResponseBody
    public ResultBean saveActiveChapterInfo(String courseJson) throws Exception {
    	//实例化对象
        CourseManagerView courseview = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, CourseManagerView.class);
    	ChapterActiveEntity course = new ChapterActiveEntity();
    	course.setChapterId(courseview.getChapterId());
    	course.setActiveFlag(courseview.getActiveFlag());
    	//根据章节id查询章节信息
    	ChapterEntity view= new ChapterEntity();
    	String chapterId = courseview.getChapterId();
    	view = chapterService.selectChapteById(chapterId);
    	//设置章节字段进行保存
    	course.setChapterTitle(view.getTitle());
    	course.setContent(courseview.getContent());
        ResultBean result = ResultBean.success();
        //保存章节活动内容
        chapterActiveService.insertInfo(course);
        return result;
    }
   *//**
             * 查询课程章节阶段活动记录信息
    * @param activeFlag
    * @param chapterId
    * @return
    *//*
	@RequestMapping("/searchActiveChapterInfo.action")
	@ResponseBody
	public ResultBean searchActiveChapterInfo(String activeFlag,String chapterId){
	ChapterActiveEntity course = new ChapterActiveEntity();
	//判断活动记录标识是否为空
	if(StringUtil.isNotBlank(activeFlag)) {
		course.setActiveFlag(activeFlag);
	}
	course.setChapterId(chapterId);
	//查询该章节的全部活动记录
	List<ChapterActiveEntity> view = chapterActiveService.searchList(course);
	ResultBean result = ResultBean.success();
	result.setData(view);
	return result;
	}
	*//**
	 * 查询课程阶段活动记录信息
	 * @param activeFlag 活动记录阶段标识
	 * @param courseId  课程主键
	 * @param chapterId  章节主键
	 * @return
	 *//*
	@RequestMapping("/searchActiveAllChapterInfo.action")
	@ResponseBody
	public ResultBean searchActiveAllChapterInfo(String activeFlag,String courseId,String chapterId){
	ChapterActiveEntity course = new ChapterActiveEntity();
	//判断活动记录标识是否为空
	if(StringUtil.isNotBlank(activeFlag)) {
		course.setActiveFlag(activeFlag);
	}
	course.setCourseId(courseId);
	if(StringUtil.isNotBlank(chapterId)) {
		course.setChapterId(chapterId);
	}
	//返回查询结果
	return chapterActiveService.searchAllCourseChapterPage(course);
	}
	*//**
	 * 查询角色的公共方法
	 * @param result
	 * @return
	 *//*
	private ModelAndView getRoleAndNum(ModelAndView result) {
    	int num = courseService.getStuStatus();
    	result.addObject("num", num);
    	UserEntity user = WebContext.getSessionUser();
    	//判断是否管理员
    	ManagerUserEntity entity = managerUserService.searchByUserId(user.getUserId());
    	if(!user.getUserName().equals("guest")) {
    		List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
    		//老师和教育局角色
    		if(roleList.size() > 0 && "A".equals(user.getUserKbn()) && null==entity && !"2542668".equals(user.getUserId())) {
    			for(UserRoleEntity role : roleList) {
    				if(role.getRoleId().equals("01")) {
    					result.addObject("role", "t");
    				}
    			}
    		//教育局角色	
    		}else if(roleList.size() > 0  && (null!=entity || "2542668".equals(user.getUserId()))){
    			result.addObject("role", "m");
    		}else if("B".equals(user.getUserKbn())){
    			result.addObject("role", "e");
    	    //家长和教研员角色
    		}else if("D".equals(user.getUserKbn()) || "E".equals(user.getUserKbn())){
    			result.addObject("role", "q");
    		//学生角色
    		}else {
    			result.addObject("role", "s");
    		}
    	}
        //管理员角色
    	if(null!=entity || "2542668".equals(user.getUserId())) {
    		result.addObject("manager", "1");
    	}else {
    		result.addObject("manager", "0");
    	}
    	return result;
    }
    *//**
     * 活动记录编辑页面方法
     * @param courseId  课程id
     * @param activeId  活动id
     * @return
     *//*
	@RequestMapping("/editActiveInit.action")
	public ModelAndView editActiveInit(String courseId,String activeId) {
	//活动编辑页面路径
	ModelAndView result = new ModelAndView("course/manager/course_active_editor");
	TagView tagQuery = new TagView();
	//查询一级分类方法
	List<TagView> tags = courseService.searchTagList2(tagQuery);
	//查询e二级分类方法
	List<TagView> tagsSecond = courseService.searchTagSecondList2(tagQuery);
	//根据课程id查询所有对应的章节信息
	ChapterEntity entity = new ChapterEntity();
	entity.setCourseId(courseId);
	List<ChapterEntity> chapterList = courseService.selectByCourseId(entity);
	//根据活动记录id查询活动章节信息
	ChapterActiveEntity active = new ChapterActiveEntity();
	active.setId(activeId);
	List<ChapterActiveEntity> activeList = chapterActiveService.searchList(active);
	//设置返回前台的字段信息
	CourseManagerView courseManagerView = new  CourseManagerView();
	courseManagerView.setChapterId(activeList.get(0).getChapterId());
	courseManagerView.setChapterTitle(activeList.get(0).getChapterTitle());
	courseManagerView.setActiveFlag(activeList.get(0).getActiveFlag());
	courseManagerView.setContent(activeList.get(0).getContent());
	//章节对象
	result.addObject("catagoryTags", chapterList);
	//result.addObject("catagoryTags", tags);
	result.addObject("pageType", "add");
	result.addObject("activeId", activeId);
	//课程对象
	result.addObject("course",courseManagerView);
	result.addObject("tagsSecond", tagsSecond);
	result = getRoleAndNum(result);
	return result;
	}
    *//**
                * 保存活动记录课程章节信息
	* @param courseJson 课程信息
	* @return 返回结果
	* @throws Exception 
	*//*
	@RequestMapping("/updateActiveChapterInfo.action")
	@ResponseBody
	public ResultBean updateActiveChapterInfo(String courseJson) throws Exception {
    //转化实例对象方法
	CourseManagerView courseview = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, CourseManagerView.class);
	ChapterActiveEntity course = new ChapterActiveEntity();
	//删除当前活动记录
	ChapterActiveEntity entity = new ChapterActiveEntity();
	String id = courseview.getActiveId();
	entity.setId(id);
	chapterActiveService.deleteInfo(entity);
	//设置保存课程的信息字段
	course.setChapterId(courseview.getChapterId());
	course.setActiveFlag(courseview.getActiveFlag());
	//查询章节信息
	ChapterEntity view= new ChapterEntity();
	String chapterId = courseview.getChapterId();
	view = chapterService.selectChapteById(chapterId);
	course.setChapterTitle(view.getTitle());
	course.setContent(courseview.getContent());
	ResultBean result = ResultBean.success();
	//保存活动记录的课程章节信息
	chapterActiveService.insertInfo(course);
	return result;
	}
    *//**
                 * 删除活动
	* @param courseJson 课程信息
	* @return 返回结果
	* @throws Exception 
	*//*
	@RequestMapping("/activeDelete.action")
	@ResponseBody
	public ResultBean activeDelete(String id) throws Exception {
	ChapterActiveEntity entity = new ChapterActiveEntity();
	//设置活动记录主键
	entity.setId(id);
	chapterActiveService.deleteInfo(entity);
	ResultBean result = ResultBean.success();
	return result;
	}
	*//**
	 * 添加页面初始化：节
	 * 
	 * @param courseId 课程ID
	 * @return
	 *//*
	@RequestMapping("/courseSmallInit.action")
	public ModelAndView courseSmallInit(String courseId, String parentId) {
		ModelAndView result = new ModelAndView("course/manager/course_three_edit");
		ChapterView chapter = new ChapterView();
		chapter.setMeasureType("4");
		result.addObject("courseId", courseId);
		result.addObject("parentId", parentId);
		result.addObject("chapterFlag", SMALL_CHAPTER);
		result.addObject("chapter", chapter);
		return result;
	}
	*//**
	 * 保存章信息
	 * 
	 * @param chapterJson
	 * @return 返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/saveExamInfo.action")
	@ResponseBody
	public ResultBean saveExamInfo(String chapterJson, String videoLength) throws Exception {
		//将字符串转化成章节对象
		ChapterView chapter = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateMinuteFormatDefualt()),
				chapterJson, ChapterView.class);
		ResultBean result = ResultBean.success();
		//设置章节状态0未发布，1已发布
		chapter.setStatus("1");
		//设置加入课程1是已加入
		chapter.setIsCourse("1");
		if (StringUtil.isEmpty(chapter.getId())) {
			//未存在的章节考试直接保存
			chapterService.insertChapterInfoAll(chapter, videoLength);
		} else {
			//已存在的章节考试更新信息
			chapterService.updateChapterAll(chapter, videoLength);
		}
		//返回信息
		return result;
	}
    *//**
     * 
                  * 查看考试结果
     * @param condition
     * @return
     *//*
    @SuppressWarnings({ "unchecked"})
    @RequestMapping("/userExamInfoInit.action")
    public ModelAndView userExamInfoInit(MeasurementEntity condition) {
        ModelAndView result = new ModelAndView("course/manager/user_exam_info");
        //参数为空抛出异常
        if(StringUtil.isEmpty(condition.getLinkId())){
            throw new BusinessException("参数错误!");
        }
        //根据参数查询考试结果
        List<MeasurementEntity> list = courseService.search(condition);
        if(list != null && list.size() != 0) {
            result.addObject("condition", list.get(0));
        } else {
            throw new BusinessException("参数错误!");
        }
        //返回考试信息
        return result;
    }
    *//**
                 * 根据课程id获取章节考试状态信息
     * @param courseId
     * @return
     * @throws Exception
     *//*
	@RequestMapping("/searchExamStatus.action")
	@ResponseBody
	public ChapterView searchExamStatus(String courseId) throws Exception {
		//查询章节信息
		ChapterView view = chapterService.seacrhExamStatus(courseId);
		return view;
	}
	@SuppressWarnings("unchecked")
	*//**
	* 前端请求-章节详情
	* @param chapterId 章节ID
	* @return 
	*//*
	@RequestMapping("/toChapterDetail.action")
	public ModelAndView toChapterDetail(String courseId,String chapterId){
		ModelAndView mv = new ModelAndView("course/client/course_chapter_detail");
		ResultBean result = courseService.searchCourseDetail(courseId);
		CourseView courseView = (CourseView) result.getData();
		mv.addObject("courseView", courseView);
		mv.addObject("note", new NoteDataEntity());
		mv.addObject("homeWork", new HomeworkView());
		result = chapterService.chapterDetail(courseId, chapterId);
		ChapterView chapterView = (ChapterView) result.getData();
		ChapterEntity entity = new ChapterEntity();
		entity.setCourseId(courseId);
		List<ChapterEntity> chapterCourseList = courseService.selectByCourseId(entity);
		mv.addObject("chapterCourseList", chapterCourseList);
		mv.addObject("chapterView", chapterView);
		//检索章节列表
		List<ChapterView> cvs = (List<ChapterView>) chapterService.chapterList(courseId);
		mv.addObject("chapterList", cvs);
		//相关课程
		result = courseService.recommendedCourses("5", courseId);
		List<CourseEntity> otherCourseList = (List<CourseEntity>) result.getData();
		mv.addObject("otherCourseList", otherCourseList);
		//判断是否需要检索测试信息
		if(CHAPTER_TYPE_MEASUERMENT.equals(chapterView.getMeasureType().toString())){
			result = measuermentService.searchMeasuerment(chapterId, LINK_TYPE_CHAPTER);
			List<QuestionEntity> questionList = (List<QuestionEntity>) result.getData();
			mv.addObject("questionList", questionList);
		}
		//判断是否考试章节
		if(CHAPTER_TYPE_EXAM.equals(chapterView.getMeasureType())){
			//考试章节
			//取用户对于章节的考试信息
			MeasurementEntity me = new MeasurementEntity();
			me.setLinkId(chapterId);
			me.setLinkType(LINK_TYPE_CHAPTER);
			me = measuermentService.findMeasurement(me);
			if(me!=null){
				UserMeasurementResultEntity um = new UserMeasurementResultEntity();
				um.setMeasurementId(me.getId());
				um.setUserId(WebContext.getSessionUserId());
				um = measuermentService.findUserExamResult(um);
				if(um != null && um.getExamStatus()!=null && um.getExamStatus().equals(EXAM_STATUS_END)){
					//已参加过考试提交了
					mv.addObject("goal", um.getGoal()+"");
					mv.addObject("totalGoal", um.getTotalGoal()+"");
					mv.addObject("examTime", DateUtil.formatAny(um.getCreateDate(), ConfigUtil.getDateTimeFormatDefualt()));
				}
			}
		}
		//是否教师
		mv.addObject("isTeacher", courseService.isTeacher(courseView.getTeacherId()));
		mv.addObject("useingChapterId", chapterId);
		String content = chapterView.getContent();
		if(content != null && !"".equals(content)) {
			String ipAndPort = getIpAndPort(content);
			if(!"".equals(ipAndPort)) {
				String serverName = WebContext.getRequest().getServerName();
				int serverPort = WebContext.getRequest().getServerPort();
				content = content.replace(ipAndPort, serverName+":"+serverPort);
				chapterView.setContent(content);
			}
		}
		
		return mv;
	}
	*//**
	 * 判断str中是否有IP和port，若有则取出返回
	 * @param str
	 * @return String
	 *//*
	private static String getIpAndPort(String str) {
		String returnStr = "";
		Pattern p = Pattern.compile("(\\d+\\.\\d+\\.\\d+\\.\\d+)\\:(\\d+)");
		Matcher m = p.matcher(str);
		if(m.find()) {
			returnStr += m.group(1)+":"+m.group(2);
		}
		
		return returnStr;
	}
	*//**
	 * 标签一览
	 * @param flag 是否显示标签 0 不显示 1 显示
	 * @param businessType 业务类型
	 * @return
	 *//*
	@RequestMapping("/searchSchhoolCategory.action")
	@ResponseBody
	public ResultBean searchSchhoolCategory(String flag,String businessType){
		ResultBean  result=tagService.searchSchhoolCategory(flag,businessType);
		return result;
	}
	*//**
	 * 
	 * 初始化待办课程列表
	 * 
	 * @param condition 课程对象
	 * @return 返回课程列表初始化页面
	 *//*
	@RequestMapping("/institutionDbListInit.action")
	public ModelAndView institutionDbListInit(InstitutionMessageEntity condition) {
		//设置代办页面
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_db_list");
		//设置分页参数
		condition.setPageSize(10);
		result.addObject("condition", condition);
		TagInstitutionView tagQuery = new TagInstitutionView();
		//一级标签信息查询
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		//二级标签信息查询
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		//返回课程实例对象
		result.addObject("course", new InstitutionMessageEntity());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}
	*//**
	 * 
	 * 初始化已办课程列表
	 * 
	 * @param condition 课程对象
	 * @return 返回课程列表初始化页面
	 *//*
	@RequestMapping("/institutionYbListInit.action")
	public ModelAndView institutionYbListInit(InstitutionMessageEntity condition) {
		//设置已办页面
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_yb_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		TagInstitutionView tagQuery = new TagInstitutionView();
		//一级标签信息查询
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		//二级标签信息查询
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		//返回课程实例对象
		result.addObject("course", new InstitutionMessageEntity());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}
	*//**
	 * 
	 * 检索评价列表
	 * 
	 * @param queryInfo 机构对象
	 * @return 返回评价列表
	 *//*
	@RequestMapping("/listdbSearch.action")
	@ResponseBody
	public ResultBean listdbSearch(MeasurementInstitutionEntity queryInfo) {
		UserEntity user = WebContext.getSessionUser();	
		//获取人员角色
		String role = user.getUserKbn();
		//获取登录人账号
		String userId = user.getCreateUser();
		//教师角色
		if("A".equals(role)) {
			TpUserEntity tpUserEntity =new TpUserEntity();
			//查询教师信息
			tpUserEntity = userManagementService.searchById(userId);
		    if(null!=tpUserEntity) {
		    	queryInfo.setInstitutionName(tpUserEntity.getSchoolName());
		    }
		    //学生角色
		}else if("C".equals(role)) {
			//查询学生信息
			StudentClassInfoEntity classInfo = new StudentClassInfoEntity();
			classInfo.setStudentId(userId);
			classInfo = studentClassInfoService.searchStudentClassInfo(classInfo);
			if(null!=classInfo && StringUtil.isNotBlank(classInfo.getSchoolName())) {
				queryInfo.setInstitutionName(classInfo.getSchoolName());
			}
		}
		
		return taxonomyInstitutionService.listdbSearch(queryInfo);
	}
	*//**
	 * 
	 * 检索已办评价列表
	 * 
	 * @param queryInfo 机构对象
	 * @return 返回评价列表
	 *//*
	@RequestMapping("/listybSearch.action")
	@ResponseBody
	public ResultBean listybSearch(MeasurementInstitutionEntity queryInfo) {
		UserEntity user = WebContext.getSessionUser();	
		//获取人员角色
		String role = user.getUserKbn();
		//登录账号
		String userId = user.getCreateUser();
		//教师角色
	    if("A".equals(role)) {
	    	//查询用户信息
	    	TpUserEntity tpUserEntity =new TpUserEntity();
			tpUserEntity = userManagementService.searchById(userId);
			if(null!=tpUserEntity) {
				//设置学校名称
				queryInfo.setInstitutionName(tpUserEntity.getSchoolName());
			}
	    }else if("C".equals(role)) {
	    	//查询学生信息
			StudentClassInfoEntity classInfo = new StudentClassInfoEntity();
			classInfo.setStudentId(userId);
			classInfo = studentClassInfoService.searchStudentClassInfo(classInfo);
			if(null!=classInfo && StringUtil.isNotBlank(classInfo.getSchoolName())) {
				//设置学生学校名称
				queryInfo.setInstitutionName(classInfo.getSchoolName());
			}
		}
	    //返回查询结果列表
		return taxonomyInstitutionService.listybSearch(queryInfo);
	}
	*//**
	 * 管理人员基本信息页面
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/managerUserList.action")
	public ModelAndView managerUserList(ManagerUserEntity condition) {
		//管理员信息页面展示路径
		ModelAndView result = new ModelAndView("/baseinstitution/manager/base_manager_user_list");
		//设置每页展示10条数据
		condition.setPageSize(10);
        result.addObject("course", new CourseManagerView());
		result.addObject("condition", condition);
		result = getRoleAndNum(result);
		//返回页面所需信息结果
		return result;
	}
	*//**
	 * 管理者信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchManagerUserList.action")
	@ResponseBody
	public ResultBean searchManagerUserList(ManagerUserEntity entity) {
		//查询管理员数据表全部信息
		return managerUserService.searchPage(entity);
	}
	*//**
	 * 搜素全部人员
	 * 
	 * @param query
	 * @return
	 *//*
	@RequestMapping("/searchUserAll.action")
	@ResponseBody
	public ResultBean searchUserAll(TpUserEntity query) {
		if(null!=query && StringUtil.isNotBlank(query.getUsrName())) {
			query.setUsrName(query.getUsrName().trim());
		}
		//根据用户名查询人员信息表数据
		return userManagementService.searchManagerUserAll(query);
	}
	*//**
	 * 保存超级管理员信息
	 * @param userId 添加人员账号id
	 * @param userName  人员名称
	 * @param personName 名称
	 * @param role 角色
	 * @return
	 *//*
	@RequestMapping("/saveManagerUser.action")
	@ResponseBody
	public ResultBean saveManagerUser(String userId,String userName,String personName,String role) {
		ResultBean result = new ResultBean();
		//查询管理员表中是否已存在该成员
		ManagerUserEntity entity = managerUserService.searchByUserId(userId);
		//存在则返回
		if(null!=entity) {
			result = ResultBean.error();
		}else {
			//不存在则保存到数据表里
			ManagerUserEntity managerUserEntity = new ManagerUserEntity();
			managerUserEntity.setUserId(userId);
			managerUserEntity.setUserName(userName);
			managerUserEntity.setPersonName(personName);
			managerUserEntity.setRole(role);
			managerUserEntity.setCreateDate(new Date());
			//设置该条信息的创建人
			managerUserEntity.setCreateUser(WebContext.getSessionUserId());
			managerUserService.insert(managerUserEntity);
			//保存成功返回信息
			result = ResultBean.success();
		}
		
		return result;
	}
	*//**
	 * 删除管理员
	 * @param id 员工账号
	 * @return  返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/deleteManagerById.action")
	@ResponseBody
	public ResultBean deleteManagerById(String id) throws Exception {
		//根据员工账号在tb_manager_user表里删除其信息
		ManagerUserEntity entity = new ManagerUserEntity();
		entity.setUserId(id);
		managerUserService.delete(entity);
		return ResultBean.success(DELETE_GRADE_SUCCESS);
	}
	*//**
	 * 查询管理信息
	 * 
	 * @param 员工账号
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchByUserId.action")
	@ResponseBody
	public ManagerUserEntity searchByUserId(String id) throws Exception {
		//返回查询结果
		return managerUserService.searchByUserId(id);
	}
	*//**
	 *  作业列表页面
	 * @param homeworkView 入参
	 * @return 返回结果
	 *//*
	@RequestMapping("/zuoyeManager.action")
	public ModelAndView zuoyeManager(HomeworkView homeworkView) {
		//列表页面路径
		ModelAndView result = new ModelAndView("/course/manager/homework_list");
		//返回作业信息对象给前台
		result.addObject("homeworkView", homeworkView);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 *  查询作业列表结果方法
	 * @param homeworkView 前台传入后台参数
	 * @return  返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/zuoyelistSearch.action")
	@ResponseBody
	public ResultBean zuoyelistSearch(HomeworkView homeworkView) throws Exception {
		ResultBean result =new ResultBean();
		//获取登录信息对象
		UserEntity user = WebContext.getSessionUser();
    	//判断是否管理员
    	ManagerUserEntity entity = managerUserService.searchByUserId(user.getUserId());
    	if(null!=entity) {
    		//管理员返回的结果
    		result= courseVRService.seacherHomeworkPage(homeworkView);
    	}else {
    		//普通教师查询返回的结果
    		homeworkView.setTeacherId(user.getUserId());
    		result= courseVRService.seacherHomeworkPage(homeworkView);
    	}
		return result;
	}
	*//**
	 *  根据作业id查询作业列表
	 * @param id 作业主键
	 * @param title  作业标题
	 * @return  返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/zuoyelistById.action")
	public ModelAndView zuoyelistById(String id,String title) throws Exception {
		//作业列表路径
		ModelAndView result = new ModelAndView("/course/manager/homework_user_list");
		HomeworkView homeworkView = new HomeworkView();
		homeworkView.setId(id);
		homeworkView.setTitle(title);
		//作业实例化对象
		result.addObject("homeworkView", homeworkView);
		//作业主键
		result.addObject("id", id);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 预览作业页面方法
	 * @param id  作业主键
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/homeWorkById.action")
	public ModelAndView homeWorkById(String id) throws Exception {
		//预览页面路径
		ModelAndView result = new ModelAndView("/course/client/home_work_teacher_review");
		HomeworkView view = courseService.getHomeWorkById(id);
		//设置创建时间返回前台
		if(null!=view.getCreateDate()) {
			view.setCreateTime(sdf.format(view.getCreateDate()));
		}
        //返回作业实体类对象
		result.addObject("homeWork", view);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 查询作业列表
	 * @param id 作业id集合字符串
	 * @param homeworkView  作业实体类
	 * @param linkId 课程主键
	 * @param linkType  课程/章节类型
	 * @return
	 *//*
	@RequestMapping("/zuoyelistByIdSearch.action")
	@ResponseBody
	public ResultBean zuoyelistByIdSearch(String id,HomeworkView homeworkView,String linkId,String linkType) {
		// 将前台汇总的作业id拆分
		if(StringUtil.isNotBlank(id)) {
			String ids[]=id.split(",");
			homeworkView.setId(ids[0]);
		}else {
			homeworkView.setLinkId(linkId);
			homeworkView.setLinkType(linkType);
			HomeworkEntity homeWork = courseService.getHomeWorkByLinkId(homeworkView);
			if(null!=homeWork) {
				//获取作业主键
				homeworkView.setId(homeWork.getId());
				//获取作业标题
				homeworkView.setTitle(homeWork.getTitle());
			}
		}
		//查询作业信息
		return courseVRService.seacherHomeworkUserPage(homeworkView);
	}
    *//**
                 * 保存作业信息
     * @param courseJson 作业信息
     * @return 返回结果
     * @throws Exception 
     *//*
    @RequestMapping("/saveZyInfo.action")
    @ResponseBody
    public ResultBean saveZyInfo(String courseJson) throws Exception {
    	//将字符串转化成实例化对象
    	HomeworkView course = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, HomeworkView.class);
        ResultBean result = ResultBean.success();
        String homeworkId=course.getId();
        //获取账号id
        String userId = WebContext.getSessionUserId();
        //查询作业数据表
        HomeworkAnswerEntity entity = new  HomeworkAnswerEntity();
        entity.setHomeworkId(homeworkId);
        entity.setCreateUser(userId);
        entity = courseVRService.selectOneAnswer(entity);
        if(null==entity) {
        	//作业作答数据表信息为空，插入数据信息
        	course.setId(IDGenerator.genUID());
        	course.setCreateUser(userId);
        	course.setCreateDate(new Date());
        	//设置学生提交的信息
        	course.setStudentComment(course.getContent());
        	course.setHomeworkId(homeworkId);
        	course.setAnswerUser(userId);
        	//设置作业状态
        	course.setStatus(HOMEWORK_ANSWER_STATUS_ONE);
        	courseVRService.insertAnswer(course);
        }else {
        	//作业作答数据表信息不为空，更新数据信息
        	entity.setStudentComment(course.getContent());
        	entity.setUpdateDate(new Date());
        	entity.setUpdateUser(userId);
        	courseVRService.update(entity);
        }
        return result;
    }
	*//**
	 * 学生作业列表页面
	 * @param HomeworkView homeworkView 
	 * @return
	 *//*
	@RequestMapping("/studentZuoyePage.action")
	public ModelAndView studentZuoyePage(HomeworkView homeworkView) {
		//学生作业列表页面路径
		ModelAndView result = new ModelAndView("/course/manager/homework_student_list");
		//作业实例对象实体类
		result.addObject("homeworkView", homeworkView);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 查询作业列表
	 * 
	 * @param HomeworkView homeworkView 入参
	 * @return 返回结果
	 *//*
	@RequestMapping("/studentZuoyelistSearch.action")
	@ResponseBody
	public ResultBean studentZuoyelistSearch(HomeworkView homeworkView) {
		homeworkView.setCreateUser(WebContext.getSessionUserId());
		//根据账号id获取作业对象信息
		return courseVRService.seacherStudentHomeworkUserPage(homeworkView);
	}
    *//**
                   * 编辑作业
     * @param HomeworkView home 作业实例对象入参 
     * @return
     *//*
    @RequestMapping("/studentTaskInit.action")
    public ModelAndView studentTaskInit(HomeworkView home){
    	//作业编辑页面
        ModelAndView result = new ModelAndView("course/manager/home_student_work");
        //获取当前登录者账号
        UserEntity user = WebContext.getSessionUser();
        home.setCreateUser(user.getUserId());
        HomeworkView homework=courseVRService.searchStudentHomeWork(home);
        //前台页面信息回显对象
        result.addObject("homework", homework);
        result = getRoleAndNum(result);
        return result;
    }
    *//**
                  * 保存作业信息
     * @param id  作业id
     * @param studentComment 学生提交内容
     * @param homeanwserId 作业作答id
     * @param status 提交作业状态
     * @return
     * @throws Exception
     *//*
    @RequestMapping("/saveMystudy.action")
    @ResponseBody
    public ResultBean saveMystudy(String id,String studentComment,String homeanwserId,String status) throws Exception {
        ResultBean result = ResultBean.success();
        String userId = WebContext.getSessionUserId();
        //根据homeanwserId判断当前登陆者是否已经作答过改作业
        if(StringUtil.isBlank(homeanwserId)) {
        	//未作答，保存作答内容
        	HomeworkView course = new HomeworkView();
        	//随机生成作业作答id
        	course.setId(IDGenerator.genUID());
        	course.setCreateUser(userId);
        	course.setCreateDate(new Date());
        	//作业作答内容
        	course.setStudentComment(studentComment);
        	course.setHomeworkId(id);
        	course.setAnswerUser(userId);
        	//设置作业状态
        	course.setStatus(HOMEWORK_ANSWER_STATUS_ONE);
        	courseVRService.insertAnswer(course);
        }else {
        	//更新做大内容
        	HomeworkAnswerEntity entity = new HomeworkAnswerEntity();
        	entity.setId(homeanwserId);
        	entity = courseVRService.selectOneAnswer(entity);
        	entity.setStudentComment(studentComment);
        	entity.setUpdateDate(new Date());
        	entity.setUpdateUser(userId);
        	courseVRService.update(entity);
        }
        return result;
    }
    *//**
                 * 学生查看作业列表方法
     * @param home
     * @return
     *//*
    @RequestMapping("/studentSeacherTaskInit.action")
    public ModelAndView studentSeacherTaskInit(HomeworkView home){
    	//学生作业列表页面路径
        ModelAndView result = new ModelAndView("course/manager/home_student_look_work");
        //根据登录id查询作业信息
        UserEntity user = WebContext.getSessionUser();
        home.setCreateUser(user.getUserId());
        HomeworkView homework=courseVRService.searchStudentHomeWork(home);
        //作业信息返回给前台
        result.addObject("homework", homework);
        result = getRoleAndNum(result);
        return result;
    }
    *//**
                  * 查询年级 课程
     * @param id 课程id
     * @return
     *//*
    @RequestMapping("/searchGrade.action")
    @ResponseBody
    public List<StudentClassInfoEntity> searchGrade(String id){
    	//根据课程id查询课程信息
        CourseEntity entity = new CourseEntity();
        entity.setId(id);
        entity = courseService.searchCourse(entity);
        //获取老师所属学校
        String schoolId = entity.getSchoolId();
        StudentClassInfoEntity studentClassInfoEntity = new StudentClassInfoEntity();
        studentClassInfoEntity.setSchoolId(schoolId);
        //根据学校查询年级
        List<StudentClassInfoEntity> gradeList = new ArrayList<StudentClassInfoEntity>();
        gradeList = studentClassInfoService.searchStudentGradeList(studentClassInfoEntity);
        //返回查询的年级集合给前台
        return gradeList;
    }
     *//**
                     * 根据课程主键和年级主键查询班级信息
      * @param courseId  课程主键
      * @param gradeId 年级主键
      * @return
      *//*
    @RequestMapping("/searchClass.action")
    @ResponseBody
    public List<StudentClassInfoEntity> searchClass(String courseId,String gradeId){
    	//根据课程主键查询课程信息
    	 CourseEntity entity = new CourseEntity();
         entity.setId(courseId);
         entity = courseService.searchCourse(entity);
         //获取老师所属学校
         String schoolId = entity.getSchoolId();
        List<StudentClassInfoEntity> gradeList = new ArrayList<StudentClassInfoEntity>();
        //根据学校主键和年级查询班级信息
        StudentClassInfoEntity studentClassInfoEntity = new StudentClassInfoEntity();
        studentClassInfoEntity.setSchoolId(schoolId);//学校id
        studentClassInfoEntity.setGradeId(gradeId);//年级id
        gradeList = studentClassInfoService.searchStudentClassList(studentClassInfoEntity);
        return gradeList;
    }
    *//**
                  * 添加学生时查询学生列表
     * @param UserView query 用户参数 
     * @param courseId 所属课程信息id
     * @return
     *//*
    @RequestMapping("/searchStudentUserAll.action")
    @ResponseBody
    public ResultBean searchStudentUserAll(UserView query,String courseId)  {
    	//根据课程id查询课程所属老师对应的学校信息
    	CourseEntity entity = new CourseEntity();
        entity.setId(courseId);
        entity = courseService.searchCourse(entity);
        //获取老师所属学校
        String schoolId = entity.getSchoolId();
        //根据学校id获取这个学校下面所有的学生信息
        query.setSchoolId(schoolId);
        return courseService.searchStudentUserAll(query);
    }
	*//**
	 * 用户个人中心信息页面方法
	 * @return 返回结果
	 *//*
	@RequestMapping("/userPblInfo.action")
	public ModelAndView userPblInfo(){
		//查询代办信息表里审核通过的代办信息数量
		int num = courseService.getStuStatus();
		//个人中心页面路径参数
		ModelAndView mv = new ModelAndView("trainingplatform/client/center/center_pbl_user");
		//根据用户id查询用户信息
		UserEntity user = userManagementService.seachUserInfo(WebContext.getSessionUserId());
		//返回用户对象
		mv.addObject("user", user);
		mv.addObject("num", num);
		//获取登录人员的角色信息
		UserEntity userRole = WebContext.getSessionUser();
		//判断用户是否登录状态guest为未登录状态
		if(!userRole.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(userRole.getUserId());
			if(roleList.size() > 0) {
				//循环遍历人员角色
				for(UserRoleEntity role : roleList) {
					//角色包含01的设置role字段为t,即老师角色
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				//s为学生角色
				mv.addObject("role", "s");
			}
		}
		UserEntity user1 = WebContext.getSessionUser();
		//获取工号
		String uid =user1.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		    //获取人员信息
		        //设置头像字段
				String image = "";
				//设置头像字段
				String school="";
				//设置年级字段
				String grade="";
				//设置班级字段
				String className="";
				if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
					//获取头像
					image = userEntity.getHeadurl();
					//获取学校
					school = userEntity.getSchoolName();
				}else {
					//设置默认头像
					image ="http://wisdomsaas-img.oss-cn-beijing.aliyuncs.com/BFAA0FECDD194B7A974C60B7E1B27C18?authorization=bce-auth-v1%2F7642b4b48046452697e90662eed41ace%2F2017-12-29T08%3A33%3A39Z%2F-1%2F%2F0f06024af14dad05694f25b8c85572567591c72bbd0129d3175b4cdbd6e62208";
				}
				//获取班级
				if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
					//实例化学生对象实体类并查询学生信息
					List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
					StudentClassInfoEntity entity = new StudentClassInfoEntity();
					entity.setStudentId(uid);
					entityList = studentClassInfoService.searchStudentClassInfoList(entity);
					//如果学生对象不为空则执行如下赋值
					if(null!=entityList && entityList.size()>0) {
						grade = entityList.get(0).getGradeName();
						className = entityList.get(0).getClassName();
						school =  entityList.get(0).getSchoolName();
					}
				}
				//返回班级信息
				mv.addObject("className", className);
				//向页面返回年级信息
				mv.addObject("grade", grade);
				//返回学生所属学校名称信息
				mv.addObject("school", school);
		       return mv;
	}
	*//**
	 * 批量点评初始化页面方法
	 * @param userIds  学生id集合字符串
	 * @param personNames  学生姓名字符集合
	 * @param id      作业id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/allEvalutionMsg.action")
	public ModelAndView allEvalutionMsg(String userIds,String personNames,String id) throws Exception {
		//批量点评方法
		ModelAndView result = new ModelAndView("/course/client/home_work_teacher_all_review");
		String ids[]=id.split(",");
		HomeworkView view = courseService.getHomeWorkById(ids[0]);
		//判断作业对象返回的时间是否为空，不为空则赋值给新的字段用于前端展示
		if(null!=view.getCreateDate()) {
			if(StringUtil.isNotBlank(sdf.format(view.getCreateDate()))) {
				view.setCreateTime(sdf.format(view.getCreateDate()));
			}
		}
		//收集作业对象
		result.addObject("homeWork", view);
		//收集作业id
		result.addObject("id", id);
		//收集学生账号集合id
		result.addObject("userIds", userIds);
		//收集学生姓名集合
		result.addObject("personNames", personNames);
		//收集作业答案id
		result.addObject("answerIds", id);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 批量点评保存
	 * @param courseJson 前台返回的json字符串
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveAllHomeWorkReviewById.action")
	@ResponseBody
	public ResultBean saveAllHomeWorkReviewById(String courseJson) throws Exception {
		//将字符串转化成实例化对象
		HomeworkView homework = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, HomeworkView.class);
		return courseService.saveAllHomeWorkReviewById(homework);
	}
	*//**
	 * 一键催交
	 * @param userIds  学生id集合字符串参数
	 * @param courseId 课程id
	 * @param homeworkId  作业id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveCjById.action")
	@ResponseBody
	public ResultBean saveCjById(String userIds,String courseId,String homeworkId) throws Exception {
		return courseService.saveACjById(userIds,courseId,homeworkId);
	}
	*//**
	 * 课程考试调用java调查网接口
	 * @param courseId  课程id
	 * @param userId  用户id
	 * @return
	 * @throws ParseException
	 *//*
	@RequestMapping("/searchExamByCourseIdList.action")
	@ResponseBody
	public ResultBean searchExamByCourseIdList(String courseId,String userId) throws ParseException {
		ResultBean result = new ResultBean();
		String param = courseId;
		//声明调用java调查网的webservice接口
		ExamToWebserviceProxy proxy = new ExamToWebserviceProxy();
		String out;
		try {
			//调用外部接口方法
			out = proxy.examList(param,1,20,userId);
			JSONArray jsonArray = new JSONArray(); 
			//将接口返回值转化成数组格式
			jsonArray = jsonArray.fromObject(out);
			UserCourseEntity uc = new UserCourseEntity();
			uc.setCourseId(courseId);
			uc.setUserId(WebContext.getSessionUserId());
			// 检索用户是否存在
			UserCourseEntity uu = courseService.searchOneCourseIdData(uc);
			//根据课程id获取该课程的老师id
			CourseEntity courseEntity = courseService.getCourseId(courseId);
			//转化对应的实体类
			@SuppressWarnings("unchecked")
			List<Questionnaire> list = jsonArray.toList(jsonArray, Questionnaire.class);
			if(null!=list && list.size()>0) {
				//循环遍历集合将字段赋值
				for(int i=0;i<list.size();i++) {
					//已加入的学员或者这个课程的老师创建者都可以看到这个
					if(null!=uu ||(null!=courseEntity && WebContext.getSessionUserId().equals(courseEntity.getTeacherId()))) {
						//已加入课程
						list.get(i).setStudentFlag("0");
						if (list.get(i).getEndDate() != null ) {
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Long end = sdf.parse(list.get(i).getEndDateStr()).getTime();
							Long nowtime = new Date().getTime();
							//判断问卷/考试是否已结束
								if(end-nowtime<0) {
									list.get(i).setExamStatusText("问卷/考试已结束");
									list.get(i).setExamStatus("1");	
								}					
							}
						if (list.get(i).getStartDate() != null ) {
							//将字符串转化成时间再获取秒数
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Long start = sdf.parse(list.get(i).getStartDateStr()).getTime();
							Long nowtime = new Date().getTime();
							//判断问卷/考试是否开始
								if(start-nowtime>0) {
									list.get(i).setExamStatusText("问卷/考试未开始");
									list.get(i).setExamStatus("1");
								}
							}
					}else {
						list.get(i).setStudentFlag("1");//未加入课程
						list.get(i).setExamStatusText("未加入课程");
					}
					
				}
			}
			 result.setData(list);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
		@Autowired
	private CourseService courseService;
	@Autowired
	private ChapterService chapterService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserManagementService userManagementService;
	@Autowired
	private TaxonomyInstitutionService taxonomyInstitutionService;
	@Autowired
	private TagInstitutionEvalutionService tagInstitutionEvalutionService;
	@Autowired
	private InstitutionService institutionService;
	@Autowired
	private TagInstitutionService tagInstitutionService;
	@Autowired
	private MeasuermentInstitutionService measurementInstitutionService;
	@Autowired
	private QuestionInstitutionService questionInstitutionService;
	@Autowired
	private MeasuermentService measuermentService;
	@Autowired
	private StudentClassInfoService studentClassInfoService;
	@Autowired
	private TeacherClassInfoService teacherClassInfoService;
	@Autowired
	private ChapterActiveService chapterActiveService;
	@Autowired
	private TagService tagService;
	@Autowired
	private ManagerUserService managerUserService;
	@Autowired
	private CourseVRService courseVRService;
	*//**
	 * 用户收藏课程
	 * 
	 * @author liuzhen
	 * @param courseId
	 * @return 返回结果
	 *//*
	@RequestMapping("/collectCourse.action")
	@ResponseBody
	public ResultBean collectCourse(String courseId) {
		return courseService.collectCourse(courseId);
	}

	*//**
	 * 用户取消收藏
	 * 
	 * @param courseId 课程Id
	 * @return
	 *//*
	@RequestMapping("/cancelCollectCourse.action")
	@ResponseBody
	public ResultBean cancelCollectCourse(String courseId) {
		return courseService.cancelCollectCourse(courseId);
	}

	*//**
	 * 二级标签查询
	 * 
	 * @param id 课程ID
	 * @return 课程下的二级标签列表
	 *//*
	@RequestMapping("/searchSecondTag2.action")
	@ResponseBody
	public List<TagView> searchSecondTag2(String id) {
		return courseService.searchSecondTagList2(id);
	}
	*//**
	 * 学校评价二级标签查询
	 * 
	 * @param id 课程ID
	 * @return 课程下的二级标签列表
	 *//*
	@RequestMapping("/searchInsitutionSecondTag2.action")
	@ResponseBody
	public List<TagInstitutionView> searchInsitutionSecondTag2(String id) {
		return courseService.searchInstitutionSecondTagList2(id);
	}
	@RequestMapping("/searchSecondTag.action")
	@ResponseBody
	public List<TagView> searchSecondTag(String id) {
		return courseService.searchSecondTagList(id);
	}

	*//**
	 * 体验课程/加入课程操作
	 * 
	 * @param courseId 课程ID
	 * @return 是否加入课程成功
	 * @author WUXIAOXIANG
	 *//*
	@RequestMapping("/joinCourse.action")
	@ResponseBody
	public ResultBean joinCourse(String courseId) {
		return courseService.joinCourse(courseId);
	}

	@RequestMapping("/applyJoinCourse.action")
	@ResponseBody
	public ResultBean applyJoinCourse(String courseId) {
		return courseService.applyJoinCourse(courseId);
	}
	
	*//**
	 * 课程推荐
	 * 
	 * @author FZ
	 * @return 返回结果
	 *//*
	@RequestMapping("/recommendedCourses.action")
	@ResponseBody
	public ResultBean recommendedCourses(String courseId, String displayNum) {

		return courseService.recommendedCourses(displayNum, courseId);
	}

	*//**
	 * 课程评价
	 * 
	 * @param linkId linkType
	 * @return 返回结果
	 * @author dengzhihao
	 *//*
	@RequestMapping("/courseEvaluationList.action")
	@ResponseBody
	public ResultBean courseEvaluationList(ScoreEntity scoreEntity) {

		return courseService.courseEvaluationList(scoreEntity);
	}

	*//**
	 * 对课程评价
	 * 
	 * @param scoreEntity     评价主题entity
	 * @param scoreDataEntity 评价数据entity
	 * @return 是否评价成功
	 * @author WUXIAOXIANG
	 *//*
	@RequestMapping("/evaluateCourse.action")
	@ResponseBody
	public ResultBean evaluateCourse(ScoreEntity scoreEntity, ScoreDataEntity scoreDataEntity) {
		// 插入评价信息
		return courseService.insertEvaluateCourse(scoreEntity, scoreDataEntity);
	}

	*//**
	 * 检索课程 包括分类
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 *//*
	@RequestMapping("/courseList.action")
	@ResponseBody
	public ResultBean courseList(CourseManagerView course, String directionTagId, String categoryTagId,
			String courseStage, String sortType, Integer pageSize, Integer pageNumber,String schoolName,String schoolNatrul,String tagsId) {
		if(StringUtil.isNotBlank(tagsId)) {
			course.setTagsId(tagsId);
		}
		if(StringUtil.isNotBlank(schoolNatrul)) {
			course.setSchoolNatrul(schoolNatrul);
		}
		ResultBean result = courseService.courseList(course, directionTagId, categoryTagId, courseStage, sortType,
				pageSize, pageNumber,schoolName);
		return result;
	}

	@RequestMapping("/coursePublicList.action")
	@ResponseBody
	public ResultBean coursePublicList(CourseManagerView course, Integer pageSize, Integer pageNumber) {
		course.setShareFlag(COURSE_SHARE_FLAG_THREE);
		course.setEndFlag(COURSE_END_STATUS_YES);
		UserEntity user = WebContext.getSessionUser();
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		//获取学校名称信息
		String school="";
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
			//获取学校
			school = userEntity.getSchoolName();
		}
		//获取班级
		if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
			List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
			StudentClassInfoEntity entity = new StudentClassInfoEntity();
			entity.setStudentId(uid);
			entityList = studentClassInfoService.searchStudentClassInfoList(entity);
			if(null!=entityList && entityList.size()>0) {
				school =  entityList.get(0).getSchoolName();
			}
		}
		course.setSchoolName(school);
//		if(StringUtil.isNotBlank(course.getDirectionTagId()) && StringUtil.isBlank(course.getCategoryTagId())) {
//			course.setTagId(course.getDirectionTagId());
//		}else {
//			course.setTagId(course.getCategoryTagId());
//			course.setParentTagId(course.getDirectionTagId());
//		}
		
		ResultBean result = courseService.coursePublicList(course, pageSize, pageNumber);
		return result;
	}

	@RequestMapping("/courseFactoryList.action")
	@ResponseBody
	public ResultBean courseFactoryList(CourseManagerView course, Integer pageSize, Integer pageNumber) {
		course.setShareFlag(COURSE_SHARE_FLAG_TWO);
		//获取学校名字
		//获取工号
		UserEntity user = WebContext.getSessionUser();
				String uid =user.getUserId();
				TpUserEntity userEntity = courseService.searchInfoMessage(uid);
				//获取学校名称信息
				String school="";
				if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
					//获取学校
					school = userEntity.getSchoolName();
				}
				//获取班级
				if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
					List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
					StudentClassInfoEntity entity = new StudentClassInfoEntity();
					entity.setStudentId(uid);
					entityList = studentClassInfoService.searchStudentClassInfoList(entity);
					if(null!=entityList && entityList.size()>0) {
						school =  entityList.get(0).getSchoolName();
					}
				}
//				course.setSchoolName(school);
//		if(StringUtil.isNotBlank(course.getDirectionTagId()) && StringUtil.isBlank(course.getCategoryTagId())) {
//			course.setTagId(course.getDirectionTagId());
//		}else {
//			course.setTagId(course.getCategoryTagId());
//			course.setParentTagId(course.getDirectionTagId());
//		}
		
		
		ResultBean result = courseService.coursePublicList(course, pageSize, pageNumber);
		return result;
	}
	
	@RequestMapping("/courseQualityList.action")
	@ResponseBody
	public ResultBean courseQualityList(CourseManagerView course, Integer pageSize, Integer pageNumber) {
		course.setShareFlag(COURSE_SHARE_FLAG_ONE);
		//获取工号
		UserEntity user = WebContext.getSessionUser();
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		//获取学校名称信息
		String school="";
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
			//获取学校
			school = userEntity.getSchoolName();
		}
		//获取班级
		if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
			List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
			StudentClassInfoEntity entity = new StudentClassInfoEntity();
			entity.setStudentId(uid);
			entityList = studentClassInfoService.searchStudentClassInfoList(entity);
			if(null!=entityList && entityList.size()>0) {
				school =  entityList.get(0).getSchoolName();
			}
		}
		course.setSchoolName(school);
//		if(StringUtil.isNotBlank(course.getDirectionTagId()) && StringUtil.isBlank(course.getCategoryTagId())) {
//			course.setTagId(course.getDirectionTagId());
//		}else {
//			course.setTagId(course.getCategoryTagId());
//			course.setParentTagId(course.getDirectionTagId());
//		}
		ResultBean result = courseService.coursePublicList(course, pageSize, pageNumber);
		return result;
	}
	
	*//**
	 * 检索教师信息
	 * 
	 * @author LILIANG
	 * @return UserEntity
	 *//*
	@RequestMapping("/teacherInfo.action")
	@ResponseBody
	public ResultBean teacherInfo(UserEntity user) {
		ResultBean result = courseService.teacherInfo(user);
		return result;
	}

	*//**
	 * 检索与教师相关的课程信息
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 *//*
	@RequestMapping("/teacherCourseList.action")
	@ResponseBody
	public ResultBean teacherCourseList(CourseEntity c) {
		ResultBean result = courseService.teacherCourseList(c);
		return result;
	}

	*//**
	 * 课程浏览数+1
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 *//*
	@RequestMapping("/addCourseViewNum.action")
	@ResponseBody
	public ResultBean addCourseViewNum(CourseEntity c) {
		ResultBean result = courseService.addCourseViewNum(c);
		return result;
	}

	*//**
	 * 根据课程ID检索课程详情
	 * 
	 * @param courseId 课程ID
	 * @return ResultBean data:CourseView 课程基本信息、视频数、练习数、标签、用户课程等信息
	 *//*
	@RequestMapping("/courseDetail.action")
	@ResponseBody
	public ResultBean courseDetail(String courseId) {
		return courseService.searchCourseDetail(courseId);
	}

	*//**
	 * 课程详情-立即支付详情
	 * 
	 * @param courseId 课程Id
	 * @return
	 *//*
	@RequestMapping("/immediatePayDetail.action")
	@ResponseBody
	public ResultBean immediatePayDetail(String courseId) {
		return courseService.immediatePayDetail(courseId);
	}

	*//**
	 * 课程列表页面
	 *//*
	@RequestMapping("/courseListInit.action")
	public ModelAndView courseListInit(String courseId,String schoolName) {
		ModelAndView result = new ModelAndView("/course/client/course_list");
		result.addObject("schoolName", schoolName);
		result.addObject("courseId", courseId);
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/pubCourseListInit.action")
	public ModelAndView pubCourseListInit() {
		ModelAndView result = new ModelAndView("/course/client/course_list_public");
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/qualityCourseListInit.action")
	public ModelAndView qualityCourseListInit() {
		ModelAndView result = new ModelAndView("/course/client/course_list_quality");
		result = getRoleAndNum(result);
		return result;
	}
	
	@RequestMapping("/courseFactoryListInit.action")
	public ModelAndView courseFactoryListInit() {
		ModelAndView result = new ModelAndView("/course/client/course_list_factory");
		result = getRoleAndNum(result);
		return result;
	}
	
	@RequestMapping("/studentHomePage.action")
	public ModelAndView studentHomePage(String userId) {
		ModelAndView result = new ModelAndView("/course/client/studentHomePage");
		UserEntity loginUser = userManagementService.seachUserInfo(userId);
		// List<CourseEntity> courseList = courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result = getRoleAndNum(result);
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/studentHomePageFromList.action")
	public ModelAndView studentHomePageFromList(String userId) {
		ModelAndView result = new ModelAndView("/course/client/studentHomePageFromList");
		UserEntity loginUser = userManagementService.seachUserInfo(userId);
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result = getRoleAndNum(result);
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/teacherHomePage.action")
	public ModelAndView teacherHomePage(String userId) {
		ModelAndView result = new ModelAndView("/course/client/teacherHomePage");
		result = getRoleAndNum(result);
		UserEntity loginUser = userManagementService.seachUserInfo(userId);
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/teacherHomePageFromList.action")
	public ModelAndView teacherHomePageFromList(String userId) {
		ModelAndView result = new ModelAndView("/course/client/teacherHomePageFromList");
		result = getRoleAndNum(result);
		UserEntity loginUser = userManagementService.seachUserInfo(userId);
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/saveInfo.action")
	@ResponseBody
	public ResultBean saveInfo(String studentJson) throws Exception {
		ArchivesEntity archives = JSONUtil.parse(
				JSONUtil.createObMapperInstance(ConfigUtil.getDateMinuteFormatDefualt()), studentJson,
				ArchivesEntity.class);
		courseService.saveArchives(archives);
		ResultBean result = ResultBean.success();
		return result;
	}

	@RequestMapping("/studentList.action")
	public ModelAndView studentList() {
		ModelAndView result = new ModelAndView("/course/client/student_list");
		result = getRoleAndNum(result);
		UserEntity loginUser = userManagementService.seachUserInfo(WebContext.getSessionUserId());
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/teacherList.action")
	public ModelAndView teacherList() {
		ModelAndView result = new ModelAndView("/course/client/teacher_list");
		result = getRoleAndNum(result);
		UserEntity loginUser = userManagementService.seachUserInfo(WebContext.getSessionUserId());
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/archivesList.action")
	@ResponseBody
	public ResultBean archivesList(String userId) {
		ResultBean result = new ResultBean();
		TpUserEntity tpUser = courseService.getUserByUserId(userId);
		List<ArchivesEntity> archivesList = courseService.getArchivesList(userId);
		List<CourseEntity> courseList = courseService.getCourseListByUserId(userId);
		if (courseList.size() > 0) {
			for (CourseEntity entity : courseList) {
				ArchivesEntity archives = new ArchivesEntity();
				archives.setUserId(userId);
				archives.setUserName(tpUser.getPersonName());
				archives.setCreateTime(sdf.format(entity.getCreateDate()));
				archives.setCreateDate(entity.getCreateDate());
				archives.setInfo(sdf.format(entity.getCreateDate()) + ",创建课程，" + entity.getTitle());
				if (entity.getPictureId() != null) {
					archives.setExitImage("1");
					archives.setImageId(entity.getPictureId());
				}
				// archivesList.add(archives);
			}
			listSort(archivesList);
		}
		List<UserCourseEntity> ucList = courseService.getUCListByUserId(userId);
		if (ucList.size() > 0) {
			for (UserCourseEntity uc : ucList) {
				ArchivesEntity archives = new ArchivesEntity();
				archives.setUserId(userId);
				archives.setUserName(tpUser.getPersonName());
				archives.setCreateTime(sdf.format(uc.getCreateDate()));
				archives.setCreateDate(uc.getCreateDate());
				archives.setInfo(sdf.format(uc.getCreateDate()) + ",加入课程，" + uc.getCourseName());
				if (uc.getPictureId() != null) {
					archives.setExitImage("1");
					archives.setImageId(uc.getPictureId());
				}
				// archivesList.add(archives);
			}
			listSort(archivesList);
		}
		result.setData(archivesList);
		return result;
	}

	@RequestMapping("/saveDiscussData.action")
	@ResponseBody
	public ResultBean saveDiscussData(ArchivesDiscussEntity data) throws Exception {
		return courseService.saveDiscuss(data);
	}

	@RequestMapping("/searchUsersList.action")
	@ResponseBody
	public ResultBean searchUsersList(TpUserEntity user) {
		String userKbn = WebContext.getSessionUser().getUserKbn();
		if (userKbn != null && userKbn.contentEquals(TpStateConstants.USER_KBN_C)) {
			user.setUserKbn(TpStateConstants.USER_KBN_C);
		}
		user.setSortName("createDate");
		user.setSortOrder("DESC");
		return userManagementService.search4Page(user);
	}

	@SuppressWarnings("unused")
	private void listSort(List<ArchivesEntity> list) {
		Collections.sort(list, new Comparator<ArchivesEntity>() {
			@Override
			public int compare(ArchivesEntity o1, ArchivesEntity o2) {
				try {
					Date dt1 = o1.getCreateDate();
					Date dt2 = o2.getCreateDate();
					if (dt1.getTime() > dt2.getTime()) {
						return -1;
					} else if (dt1.getTime() < dt2.getTime()) {
						return 1;
					} else {
						return 0;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return 0;
			}
		});
	}

	*//**
	 * 系统首页
	 *//*
	@RequestMapping("/homePage.action")
	public ModelAndView homePage() {
		ModelAndView result = new ModelAndView("/course/client/homePage");
		UserEntity user = WebContext.getSessionUser();
		//获取工号
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getIsFlag())) {
			result.addObject("isFlag","1");
		}else {
			//不是管理员
			result.addObject("isFlag","0");
		}
		
		//获取人员信息
		String image = "";
		String school="";
		String grade="";
		String className="";
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
			//获取头像
			image = userEntity.getHeadurl();
			//获取学校
			school = userEntity.getSchoolName();
		}else {
			//设置默认头像
			image ="http://wisdomsaas-img.oss-cn-beijing.aliyuncs.com/BFAA0FECDD194B7A974C60B7E1B27C18?authorization=bce-auth-v1%2F7642b4b48046452697e90662eed41ace%2F2017-12-29T08%3A33%3A39Z%2F-1%2F%2F0f06024af14dad05694f25b8c85572567591c72bbd0129d3175b4cdbd6e62208";
		}
		//获取班级
		if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
			List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
			StudentClassInfoEntity entity = new StudentClassInfoEntity();
			entity.setStudentId(uid);
			entityList = studentClassInfoService.searchStudentClassInfoList(entity);
			if(null!=entityList && entityList.size()>0) {
				grade = entityList.get(0).getGradeName();
				className = entityList.get(0).getClassName();
				school =  entityList.get(0).getSchoolName();
			}
		}
		result.addObject("className", className);
		result.addObject("grade", grade);
		result.addObject("school", school);
		result.addObject("image", image);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 问卷考试系统首页
	 *//*
	@RequestMapping("/homeJavaPage.action")
	public ModelAndView homeJavaPage(String courseId) {
		ModelAndView result = new ModelAndView("/course/client/homeJavaPage");
		UserEntity user = WebContext.getSessionUser();
		//获取工号
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getIsFlag())) {
			result.addObject("isFlag","1");
		}else {
			//不是管理员
			result.addObject("isFlag","0");
		}
		
		//获取人员信息
		String image = "";
		String school="";
		String grade="";
		String className="";
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
			//获取头像
			image = userEntity.getHeadurl();
			//获取学校
			school = userEntity.getSchoolName();
		}else {
			//设置默认头像
			image ="http://wisdomsaas-img.oss-cn-beijing.aliyuncs.com/BFAA0FECDD194B7A974C60B7E1B27C18?authorization=bce-auth-v1%2F7642b4b48046452697e90662eed41ace%2F2017-12-29T08%3A33%3A39Z%2F-1%2F%2F0f06024af14dad05694f25b8c85572567591c72bbd0129d3175b4cdbd6e62208";
		}
		//获取班级
		if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
			List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
			StudentClassInfoEntity entity = new StudentClassInfoEntity();
			entity.setStudentId(uid);
			entityList = studentClassInfoService.searchStudentClassInfoList(entity);
			if(null!=entityList && entityList.size()>0) {
				grade = entityList.get(0).getGradeName();
				className = entityList.get(0).getClassName();
				school =  entityList.get(0).getSchoolName();
			}
		}
		result.addObject("className", className);
		result.addObject("grade", grade);
		result.addObject("school", school);
		result.addObject("courseId", courseId);
		result.addObject("image", image);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 题库管理
	 *//*
	@RequestMapping("/homeExamPage.action")
	public ModelAndView homeExamPage() {
		ModelAndView result = new ModelAndView("/course/manager/homeExamPage");
		UserEntity user = WebContext.getSessionUser();
		//获取工号
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getIsFlag())) {
			result.addObject("isFlag","1");
		}else {
			//不是管理员
			result.addObject("isFlag","0");
		}
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 题库管理
	 *//*
	@RequestMapping("/homeJsdPage.action")
	public ModelAndView homeJsdPage() {
		ModelAndView result = new ModelAndView("/course/client/homeJsdPage");
		UserEntity user = WebContext.getSessionUser();
		//获取工号
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getIsFlag())) {
			result.addObject("isFlag","1");
		}else {
			//不是管理员
			result.addObject("isFlag","0");
		}
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 获取登录信息
	 * @param code
	 * @param type
	 * @return
	 * @throws UnsupportedEncodingException
	 *//*
	@SuppressWarnings("null")
	@RequestMapping("/getUserInfo.action")
	@ResponseBody
	public JSONArray getUserInfo(String code, String type) throws UnsupportedEncodingException {
		//设置json集合对象和数组
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		if (code != null && !"".equals(code)) {
			//UserEntity user = WebContext.getSessionUser();
			String codeSession = (String) WebContext.getSession().getAttribute("code");
			if (codeSession == null || !code.equals(codeSession)) {
				Long str12 = System.currentTimeMillis();
				String access_tokenStr = "";
				//定义获取应用token的变量
				//String application_tokenStr="";
				if (type.equals("1")) {
					//获取access_tokenStr字符串对象
					access_tokenStr = HttpUtils.sendFormEvalutionPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token",
							"code=" + code
									+ "&grant_type=authorization_code&client_id=1EF8798194B36D72&client_secret=440a4fad895036188ea200a0da1d6156&redirect_uri="
									+ PropertyUtil.getProperty("remote.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "1EF8798194B36D72", "440a4fad895036188ea200a0da1d6156");
				} else if (type.equals("2")) {
					//获取access_tokenStr字符串对象
					access_tokenStr = HttpUtils.sendFormEvalutionPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token",
							"code=" + code
									+ "&grant_type=authorization_code&client_id=BB2650288E091B1C&client_secret=26807915a5269ba07a40d97236b5ec64&redirect_uri="
									+ PropertyUtil.getProperty("teacher.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "BB2650288E091B1C", "26807915a5269ba07a40d97236b5ec64");
				} else if (type.equals("3")) {
					//获取access_tokenStr字符串对象
					access_tokenStr = HttpUtils.sendPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token",
							"code=" + code
									+ "&grant_type=authorization_code&client_id=42F9292D78B1386F&client_secret=e1e034ac44a63ba61e8e9595689efd4c&redirect_uri="
									+ PropertyUtil.getProperty("student.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "42F9292D78B1386F", "e1e034ac44a63ba61e8e9595689efd4c");
				} else if (type.equals("4")) {
					//获取access_tokenStr字符串对象
					   access_tokenStr = HttpUtils.sendFormEvalutionPost(
					            PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", 
					            "code=" + code + 
					            "&grant_type=authorization_code&client_id=6FF0E39AD917FD04&client_secret=ea201b6aff470f9ec25c1fc9be17197a&redirect_uri=" + 
					            PropertyUtil.getProperty("institution.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "6FF0E39AD917FD04", "ea201b6aff470f9ec25c1fc9be17197a");
				} else if (type.equals("5")) {
					//获取access_tokenStr字符串对象
					access_tokenStr = HttpUtils.sendFormEvalutionPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token",
							"code=" + code
									+ "&grant_type=authorization_code&client_id=8275669B807A7B7A&client_secret=229f39d615a8c2cbbef56e56197a22c7&redirect_uri="
									+ PropertyUtil.getProperty("teacherevalution.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "8275669B807A7B7A", "229f39d615a8c2cbbef56e56197a22c7");
				}
				//设置执行时间
				Long str333 = System.currentTimeMillis();
				int tokentime =(int) (str333-str12);
				UserLogEntity entity12=new UserLogEntity();
				entity12.setId(IDGenerator.genUID());
				entity12.setUserId(WebContext.getSessionUserId());
				entity12.setUserName(WebContext.getSessionUser().getPersonName());
				entity12.setLogType("获取token信息");
				entity12.setTimes(tokentime);
				entity12.setOperTime(new Date());
				//保存方法执行时间日志信息
				tagInstitutionEvalutionService.insertUserLog(entity12);
				//获取access_token
				if (access_tokenStr != null && !"".equals(access_tokenStr)) {
					//字符转化成json对象
					JSONObject accessjson = JSONObject.fromObject(access_tokenStr);
					//获取字段值
					String access_token = accessjson.getString("access_token");
					Long str3 = System.currentTimeMillis();
					//原有逻辑获取用户信息开始
					String userStr = HttpUtils.sendFormEvalutionPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/rest/get/userinfo",
							"access_token=" + access_token);
					Long str33 = System.currentTimeMillis();
					int userinfotime =(int) (str33-str3);
					UserLogEntity entity13=new UserLogEntity();
					entity13.setId(IDGenerator.genUID());
					entity13.setUserId(WebContext.getSessionUserId());
					entity13.setUserName(WebContext.getSessionUser().getPersonName());
					entity13.setLogType("获取/passport/rest/get/userinfo信息");
					entity13.setTimes(userinfotime);
					entity13.setOperTime(new Date());
					//保存方法执行需要的时间信息日志
					tagInstitutionEvalutionService.insertUserLog(entity13);
					System.out.println("userStr=:" + userStr);
					
					
					JSONObject userjson = JSONObject.fromObject(userStr);
					//原有逻辑获取用户信息结束
					//用户id
					String userid = userjson.getString("userid");
					//用户账号
					String account = userjson.getString("account");
					//用户类型
					String usertype = userjson.getString("usertype");
					//用户名称
					String name = userjson.getString("name");
					//用户性别
					String sex = userjson.getString("sex");
					//用户角色
					String role = userjson.getString("role");
					//获取tenantId
					String tenantId = userjson.getString("tenantId");
					//获取头像url
					String headUrl = userjson.getString("head_url");
					String teacher = "";
					*//**
					 * usertype=1
					 * 1:平台总系统管理员;2:平台省级系统管理员;3:平台市级系统管理员;4:学校总系统管理员;5:学校分校系统管理员;6:学校年级系统管理员;7:教育部分系统管理员
					 * usertype=0 
					 * 1:教师;2:家长;3:学生;4:管理者;5:教研员
					 *//*
					*//**
					 * 系统角色 01:管理员;04:教师
					 *//*
					UserRoleEntity userRole = new UserRoleEntity();
					UserRoleEntity userTeacherRole = new UserRoleEntity();
					UserEntity loginUser = new UserEntity();
					TpUserEntity saveUser = new TpUserEntity();
					*//**
					 * 获取用户是老师的信息
					 * 包括老师所属的学校信息
					 *//*
					if("1".equals(role)) {
						//获取教师信息
						Long str5 = System.currentTimeMillis();
						//调用外部接口获取教师信息
						String teacherinfo = HttpUtils.sendFormDataPost("https://gwss.yceduyun.com/apis/user/V1.0/teacherInfo",
								access_token,"002");
						Long str6 = System.currentTimeMillis();
						int teachertime =(int) (str6-str5);
						UserLogEntity entity2=new UserLogEntity();
						entity2.setId(IDGenerator.genUID());
						entity2.setUserId(WebContext.getSessionUserId());
						entity2.setUserName(WebContext.getSessionUser().getPersonName());
						entity2.setLogType("获取https://gwss.yceduyun.com/apis/user/V1.0/teacherInfo信息");
						entity2.setTimes(userinfotime);
						entity2.setOperTime(new Date());
						//保存获取教师信息所需要的时间日志信息
						tagInstitutionEvalutionService.insertUserLog(entity2);
						System.out.println(teacherinfo); 
                        //解析教师信息
						JSONObject teacherinfojson = JSONObject.fromObject(teacherinfo);
						JSONObject teacherinfoDatajson = JSONObject.fromObject(teacherinfojson.get("data"));
						//获取基本信息
						if(null!=teacherinfoDatajson) {
							//省份
							String proName=teacherinfoDatajson.getString("proName");
							//市区
							String cityName=teacherinfoDatajson.getString("cityName");
							//县级
							String townName=teacherinfoDatajson.getString("townName");
							//学校id
							String schoolId=teacherinfoDatajson.getString("schoolId");
							//学校名称
							String schoolName=teacherinfoDatajson.getString("schoolName");
							//学校类型
							String schoolType=teacherinfoDatajson.getString("schoolType");
							//
							String subjectId=teacherinfoDatajson.getString("subjectId");
							//
							String subjectName=teacherinfoDatajson.getString("subjectName");
							saveUser.setProName(proName);
							saveUser.setCityName(cityName);
							saveUser.setTownName(townName);
							saveUser.setSchoolId(schoolId);
							saveUser.setSchoolName(schoolName);
							saveUser.setSchoolType(schoolType);
							saveUser.setSubjectId(subjectId);
							saveUser.setSubjectName(subjectName);
						}
						//获取字段信息
						JSONArray teacherClassArray =JSONArray.fromObject(teacherinfoDatajson.get("classList"));
						//获取老师的班级年级信息
						@SuppressWarnings("unchecked")
						List<TeacherClassInfoEntity> list = teacherClassArray.toList(teacherClassArray, TeacherClassInfoEntity.class);
						//判断对象是否为空
						if(null!=list && list.size()>0) {
							List<TeacherClassInfoEntity> entityList = new ArrayList<TeacherClassInfoEntity>();
							//查询该老师下的信息是否存在
							TeacherClassInfoEntity entity = new TeacherClassInfoEntity();
							entity.setTeacherId(userid);
							entityList = teacherClassInfoService.searchTeacherClassInfoList(entity);
							if(null!=entityList && entityList.size()>0) {
								//循环遍历教师班级信息
								for(int j=0;j<entityList.size();j++) {
									TeacherClassInfoEntity entity1 = new TeacherClassInfoEntity();
									entity1.setId(entityList.get(j).getId());
									//删除已存在信息
									teacherClassInfoService.deleteTeacherClassInfoById(entity1);
								}
							}
							//循环保存班级信息
							for(int i=0;i<list.size();i++) {
								TeacherClassInfoEntity infoEntity = new TeacherClassInfoEntity();
								infoEntity.setTeacherId(userid);//老师id
								infoEntity.setGradeId(list.get(i).getGradeId());//年级id
								infoEntity.setGradeName(list.get(i).getGradeName());//年级名称
								infoEntity.setGradeLever(list.get(i).getGradeLever());//年级等级
								infoEntity.setClassId(list.get(i).getClassId());//班级id
								infoEntity.setClassName(list.get(i).getClassName());//班级名称
								infoEntity.setSubjectId(list.get(i).getSubjectId());//
								infoEntity.setSubjectName(list.get(i).getSubjectName());//
								infoEntity.setCreateDate(new Date());//创建日期
								infoEntity.setUpdateDate(new Date());//更新日期
								teacherClassInfoService.insertTeacherClassInfo(infoEntity);
							}
						}
						//获取用户具体信息
						String param=access_token;
						String tenantId1 = "002";
						Long str7 = System.currentTimeMillis();
						//获取教师角色
						String teacherDetailinfo = HttpUtils.sendFormDataDetailPost("https://gwss.yceduyun.com/apis/usercenter/v1.0/getOneUserInfoByUseIdAndRoleId",
								param,tenantId1,role);
						Long str8 = System.currentTimeMillis();
						int studenttime =(int) (str8-str7);
						UserLogEntity entity3=new UserLogEntity();
						entity3.setId(IDGenerator.genUID());
						entity3.setUserId(WebContext.getSessionUserId());
						entity3.setUserName(WebContext.getSessionUser().getPersonName());
						entity3.setLogType("获取https://gwss.yceduyun.com/apis/usercenter/v1.0/getOneUserInfoByUseIdAndRoleId信息");
						entity3.setTimes(userinfotime);
						entity3.setOperTime(new Date());
						//保存调用教师角色花费时间信息
						tagInstitutionEvalutionService.insertUserLog(entity3);
						//字符串转json
						JSONObject teacherDetailinfojson = JSONObject.fromObject(teacherDetailinfo);
						JSONArray teacherinfoDataDetailjson = JSONArray.fromObject(teacherDetailinfojson.get("data"));
						JSONArray dutyJson = JSONObject.fromObject(teacherinfoDataDetailjson.get(0)).getJSONArray("teacherDuties");
						List<InstitutionTeacherDutyEntity> dutylist = JSONArray.toList(dutyJson, InstitutionTeacherDutyEntity.class);
                        if(null!=dutylist && dutylist.size()>0) {
                        	for(int p=0;p<dutylist.size();p++) {
                        		//001代表校长
                        		if("校长".equals(dutylist.get(p).getDutyName())) {
                        			teacher="001";
                        			break;
                        		}else {
                        			//002代表老师
                        			teacher="002";
                        		}
                        	}
                        }
					}else if("3".equals(role)) {
						//获取学生信息
						Long str9 = System.currentTimeMillis();
						//调用学生接口获取信息
						String studentinfo = HttpUtils.sendFormDataPost("https://gwss.yceduyun.com/apis/user/V1.0/studentInfo",
								access_token,"002");
						Long str10 = System.currentTimeMillis();
						int studenttime =(int) (str10-str9);
						UserLogEntity entity3=new UserLogEntity();
						entity3.setId(IDGenerator.genUID());
						entity3.setUserId(WebContext.getSessionUserId());
						entity3.setUserName(WebContext.getSessionUser().getPersonName());
						entity3.setLogType("获取https://gwss.yceduyun.com/apis/user/V1.0/studentInfo信息");
						entity3.setTimes(userinfotime);
						entity3.setOperTime(new Date());
						//保存调用学生接口获取花费的时间日志
						tagInstitutionEvalutionService.insertUserLog(entity3);
						JSONObject studentinfojson = JSONObject.fromObject(studentinfo);
						JSONObject studentinfoDatajson = JSONObject.fromObject(studentinfojson.get("data"));
						if(null!= studentinfoDatajson) {
							String studentId=userid;
							//姓名
							String studentName=studentinfoDatajson.getString("username");
							String schoolId=studentinfoDatajson.getString("schoolId");//学校id
							String schoolName=studentinfoDatajson.getString("schoolName");//学校名称
							String schoolType=studentinfoDatajson.getString("schoolTye");//学校类型
							String gradeId=studentinfoDatajson.getString("gradeId");//年级id
							String gradeName=studentinfoDatajson.getString("gradeName");//年级名称
							String classId=studentinfoDatajson.getString("classId");//班级id
							String className=studentinfoDatajson.getString("className");//班级名称
							String gradeLever=studentinfoDatajson.getString("gradeLever");//年级等级
							String proName=studentinfoDatajson.getString("proName");//省份
							String cityName=studentinfoDatajson.getString("cityName");//市区
							String townName=studentinfoDatajson.getString("townName");//县级
							StudentClassInfoEntity studentClassInfoEntity = new StudentClassInfoEntity();
							studentClassInfoEntity.setStudentId(studentId);
							List<StudentClassInfoEntity> studentlist = new ArrayList<StudentClassInfoEntity>();
							studentlist = studentClassInfoService.searchStudentClassInfoList(studentClassInfoEntity);
							if(null!=studentlist && studentlist.size()>0) {
								//循环遍历学生信息
								for(int k=0;k<studentlist.size();k++) {
									StudentClassInfoEntity student = new StudentClassInfoEntity();
									student.setId(studentlist.get(k).getId());
									//删除学生信息
									studentClassInfoService.deleteStudentClassInfoById(student);
								}
							}
							//保存学生班级信息
							StudentClassInfoEntity stunfoEntity = new StudentClassInfoEntity();
							stunfoEntity.setStudentId(studentId);
							stunfoEntity.setStudentName(studentName);
							stunfoEntity.setSchoolId(schoolId);
							stunfoEntity.setSchoolName(schoolName);
							stunfoEntity.setSchoolType(schoolType);
							stunfoEntity.setGradeId(gradeId);
							stunfoEntity.setGradeName(gradeName);
							stunfoEntity.setGradeLever(gradeLever);
							stunfoEntity.setClassId(classId);
							stunfoEntity.setClassName(className);
							stunfoEntity.setProName(proName);
							stunfoEntity.setCityName(cityName);
							stunfoEntity.setTownName(townName);
							stunfoEntity.setCreateDate(new Date());
							studentClassInfoService.insertStudentClassInfo(stunfoEntity);
							Long str122 = System.currentTimeMillis();
							int studentinerttime =(int) (str122-str10);
							UserLogEntity entity32=new UserLogEntity();
							entity32.setId(IDGenerator.genUID());
							entity32.setUserId(WebContext.getSessionUserId());
							entity32.setUserName(WebContext.getSessionUser().getPersonName());
							entity32.setLogType("保存学生角色信息");
							entity32.setTimes(studentinerttime);
							entity32.setOperTime(new Date());
							tagInstitutionEvalutionService.insertUserLog(entity32);
						}
						
					}
					if (usertype != null && !"".equals(usertype) && usertype.equals("0")) {
						//老师
						if (!"".equals(role) && role.equals("1")) {
							userRole.setRoleId("01");
							userTeacherRole.setRoleId("04");
							loginUser.setUserKbn("A");
							saveUser.setUserKbn("A");
							if (userid != null && !"".equals(userid)) {
								userRole.setUserId(userid);
								userTeacherRole.setUserId(userid);
							}
							//管理者
						} else if (!"".equals(role) && role.equals("4")) {
							userRole.setRoleId("01");
							loginUser.setUserKbn("B");
							saveUser.setUserKbn("B");
							if (userid != null && !"".equals(userid)) {
								userRole.setUserId(userid);
							}
							//学生
						} else if(!"".equals(role) && role.equals("3")){
							loginUser.setUserKbn("C");
							saveUser.setUserKbn("C");
							//家长
						}else if(!"".equals(role) && role.equals("2")){
							loginUser.setUserKbn("D");
							saveUser.setUserKbn("D");
							//教研员
						}else if(!"".equals(role) && role.equals("5")){
							loginUser.setUserKbn("E");
							saveUser.setUserKbn("E");
						}
					}
					
					loginUser.setUserId(userid);
					loginUser.setUserName(account == null ? "" : account);
					loginUser.setSex(sex == null ? "" : sex);
					loginUser.setPersonName(name == null ? "" : name);
					loginUser.setPassword(MD5Util.getPassword4MD5("123456"));
					userRole.setCreateDate(new Date());
					userRole.setCreateUser(userid);
					userTeacherRole.setCreateDate(new Date());
					userTeacherRole.setCreateUser(userid);
					loginUser.setCreateDate(new Date());
					loginUser.setCreateUser(userid);
					loginUser.setLockFlag("0");

					saveUser.setUserId(userid);
					saveUser.setUserName(account == null ? "" : account);
					saveUser.setSex(sex == null ? "" : sex);
					saveUser.setPersonName(name == null ? "" : name);
					saveUser.setPassword(MD5Util.getPassword4MD5("123456"));
					saveUser.setCreateDate(new Date());
					saveUser.setCreateUser(userid);
					saveUser.setCreateDate(new Date());
					saveUser.setCreateUser(userid);
					saveUser.setLockFlag("0");
					saveUser.setTenantId(tenantId);
					saveUser.setHeadurl(headUrl);
					saveUser.setDuty(teacher);
					Long usertime1 = System.currentTimeMillis();
					courseService.saveUserAndRole(userRole, userTeacherRole, saveUser);
					Long usertime2 = System.currentTimeMillis();
					WebContext.loginUser(loginUser, true);
					WebContext.getSession().setAttribute("code", code);
					jo.put("result", "success");
					Long str123 = System.currentTimeMillis();
					System.out.println("计算花费时间---------------------------");
					int times = (int) (usertime2-usertime1);
					UserLogEntity entity=new UserLogEntity();
					entity.setId(IDGenerator.genUID());
					entity.setUserId(WebContext.getSessionUserId());
					entity.setUserName(WebContext.getSessionUser().getPersonName());
					entity.setLogType("获取保存用户信息");
					entity.setTimes(times);
					entity.setOperTime(new Date());
					tagInstitutionEvalutionService.insertUserLog(entity);
					System.out.println(str123-str12);
				}
			}

		}
		json.add(jo);
		return json;
	}
    *//**
     * 手机端首页
     * @return
     *//*
	@RequestMapping("/homePagePhone.action")
	public ModelAndView homePagePhone() {
		ModelAndView result = new ModelAndView("/course/client/homePagePhone");
		return result;
	}
    *//**
                * 教师页面
     * @return
     *//*
	@RequestMapping("/teacherHome.action")
	public ModelAndView teacherHome() {
		ModelAndView result = new ModelAndView("/course/client/teacherHomeBack");
		return result;
	}
    *//**
     * 学生页面
     * @return
     *//*
	@RequestMapping("/studentHome.action")
	public ModelAndView studentHome() {
		ModelAndView result = new ModelAndView("/course/client/studentHomeBack");
		return result;
	}
    *//**
     * 人员信息
     * @return
     *//*
	@RequestMapping("/sysUser.action")
	public ModelAndView sysUser() {
		ModelAndView result = new ModelAndView("/course/client/sysUser");
		result = getRoleAndNum(result);
		return result;
	}
    *//**
     * 系统日志
     * @return
     *//*
	@RequestMapping("/sysLog.action")
	public ModelAndView sysLog() {
		ModelAndView result = new ModelAndView("/course/client/sysLog");
		result = getRoleAndNum(result);
		return result;
	}
   *//**
    * 保存系统日志
    * @param proName
    * @param IpInfo
    * @return
    *//*
	@RequestMapping("/insertSysUser.action")
	@ResponseBody
	public JSONArray insertSysUser(String proName, String IpInfo) {
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		courseService.insertSysUser(proName, IpInfo);
		jo.put("result", "success");
		json.add(jo);
		return json;
	}
    *//**
     * 保存操作日志
     * @param browserInfo
     * @param sysInfo
     * @param IpInfo
     * @param operDetail
     * @param operSite
     * @return
     *//*
	@RequestMapping("/insertUserOper.action")
	@ResponseBody
	public JSONArray insertUserOper(String browserInfo, String sysInfo, String IpInfo, String operDetail,
			String operSite) {
		UserOperEntity oper = new UserOperEntity();
		oper.setSysInfo(browserInfo + "(" + sysInfo + ")");
		oper.setOperDetail(operDetail);
		oper.setOperSite(operSite);
		oper.setOperTime(new Date());
		oper.setOperIP(IpInfo);
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		courseService.inserUserOper(oper);
		jo.put("result", "success");
		json.add(jo);
		return json;
	}
	*//**
	 * 
	 * @param IpInfo
	 * @param logType
	 * @return
	 *//*
	@RequestMapping("/insertUserLog.action")
	@ResponseBody
	public JSONArray insertUserLog(String IpInfo, String logType) {
		UserLogEntity log = new UserLogEntity();
		log.setOperIP(IpInfo);
		log.setLogType(logType);
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		String userId = WebContext.getSessionUserId();
		String userName = WebContext.getSessionUser().getPersonName();
		log.setUserId(userId);
		log.setUserName(userName);
		courseService.insertUserLog(log);
		jo.put("result", "success");
		json.add(jo);
		WebContext.logout();
		return json;
	}
    *//**
                  * 根据用户名查询用户信息
     * @param username
     * @param IpInfo
     * @return
     *//*
	@RequestMapping("/getUserByUsername.action")
	@ResponseBody
	public JSONArray getUserByUserName(String username, String IpInfo) {
		TpUserEntity result = courseService.getUserByUsername(username);
		if (result != null) {
			UserLogEntity log = new UserLogEntity();
			log.setOperIP(IpInfo);
			log.setLogType("登录");
			log.setUserId(result.getUserId());
			log.setUserName(result.getPersonName());
			courseService.insertUserLog(log);
		}
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		if (result != null) {
			jo.put("result", "success");
		} else {
			jo.put("result", "error");
		}
		json.add(jo);
		return json;
	}

	*//**
	 * 系统首页
	 *//*
	@RequestMapping("/webEditor.action")
	public ModelAndView webEditor() {
		ModelAndView result = new ModelAndView("/course/client/webEditor");
		String serverName = WebContext.getRequest().getServerName();
		int serverPort = WebContext.getRequest().getServerPort();
		result.addObject("serverUrl", serverName + ":" + serverPort);
		return result;
	}
    *//**
     * 课程详情页面
     * @param courseId
     * @param schoolName
     * @return
     *//*
	@RequestMapping("/courseDetailInit.action")
	public ModelAndView courseDetailInit(String courseId,String schoolName) {
		ModelAndView result = new ModelAndView("/course/client/course_detail");
		ResultBean data = courseService.searchCourseDetail(courseId);
		if (!data.isStatus()) {
			throw new BusinessException(data.getMessages());
		}
		ChapterEntity entity = new ChapterEntity();
		entity.setCourseId(courseId);
		List<ChapterEntity> chapterCourseList = courseService.selectByCourseId(entity);
		result.addObject("chapterCourseList", chapterCourseList);
		result.addObject("courseView", data.getData());
		result.addObject("schoolName", schoolName);
		result.addObject("courseId", courseId);
		// 检索章节列表
		List<ChapterView> chapterList = chapterService.chapterList(courseId);
		result.addObject("chapterList", chapterList);
		String measureType="2";
		List<ChapterView> chapterList1 = chapterService.chapterList1(courseId,measureType);
		result.addObject("chapterExamList", chapterList1);
		List<CourseMessageEntity> userMsgList = courseService.getUserMsgList(courseId);
		if(userMsgList.size() > 0) {
			CourseMessageEntity msg = userMsgList.get(0);
			if(msg.getCheckStatus().equals("-1")) {
				result.addObject("msgInfo", "-1");
			}else if(msg.getCheckStatus().equals("1")) {
				result.addObject("msgInfo", "1");
			}else {
				result.addObject("msgInfo", "none");
			}
		}
		result = getRoleAndNum(result);
		return result;
	}
    *//**
     * 公开课详情页面
     * @param courseId
     * @return
     *//*
	@RequestMapping("/coursePublicDetail.action")
	public ModelAndView coursePublicDetail(String courseId) {
		ModelAndView result = new ModelAndView("/course/client/course_public_detail");
		ResultBean data = courseService.searchCourseDetail(courseId);
		if (!data.isStatus()) {
			throw new BusinessException(data.getMessages());
		}
		result.addObject("courseView", data.getData());
		result = getRoleAndNum(result);
		return result;
	}

	*//**
	 * 导师课程页面初始化
	 * 
	 * @param
	 * @return
	 *//*
	@RequestMapping("/teacherCourseInit.action")
	public ModelAndView teacherCourseInit(UserEntity user) {
		ModelAndView mv = new ModelAndView("/course/client/teacher_course_list");
		ResultBean result = courseService.teacherInfo(user);
		UserEntity use = new UserEntity();
		if (result.getData() != null) {
			use = (UserEntity) result.getData();
			if ("0".equals(use.getSex())) {
				use.setSex("男");
			} else {
				use.setSex("女");
			}
		}
		mv.addObject("user", use);
		return mv;
	}

	*//**
	 * 投票页面初始化
	 * 
	 * @param
	 * @return
	 *//*
	@RequestMapping("/voteItemInit.action")
	public ModelAndView voteItemInit(String courseId, String linkType) {
		ModelAndView mv = new ModelAndView("/course/client/vote_item");
		if (StringUtils.isNotEmpty(linkType)) {
			mv.addObject("linkType", linkType);
		}
		mv.addObject("courseId", courseId);
		return mv;
	}

	*//**
	 * 根据用户ID，课程ID检索用户课程
	 * 
	 * @param courseId
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchUserCourse.action")
	@ResponseBody
	public ResultBean searchUserCourse(String courseId) {
		ResultBean result = new ResultBean();
		if (courseId == null || courseId.equals("")) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_NULL_ID));

			return result;
		}

		UserCourseEntity uc = new UserCourseEntity();
		uc.setCourseId(courseId);
		// 从session中取出登录用户的ID
		String userId = WebContext.getSessionUserId();
		if (userId == null) {
			result.setStatus(false);
			result.setErrorCode(ErrorCode.NOT_LOGIN);
			result.setMessages(getMessage(MSG_E_SESSION_TIMEOUT));
			return result;
		}
		uc.setUserId(userId);
		uc = courseService.searchUserCourse(uc);
		result.setData(uc);
		result.setStatus(true);
		return result;
	}

	*//**
	 * 课程作业页面初始化
	 * 
	 * @param linkId   链接ID
	 * @param linkType 链接类型
	 * @return
	 *//*
	@RequestMapping("/homeWorkInit.action")
	public ModelAndView homeWorkInit(String linkId, String linkType) {
		ModelAndView mv = new ModelAndView("/course/client/home_work");
		if (StringUtils.isNotEmpty(linkType)) {
			mv.addObject("linkType", linkType);
		}
		mv.addObject("linkId", linkId);
		return mv;
	}

	*//**
	 * 检索我的作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/honeworkDetail.action")
	@ResponseBody
	public ResultBean honeworkDetail(HomeworkView homeworkView) {
		ResultBean result = new ResultBean();
		String userId = WebContext.getSessionUserId();
		List<HomeworkView> viewlist = new ArrayList<HomeworkView>();
		viewlist = (List<HomeworkView>) (courseService.honeworkList(homeworkView)).getData();
		for (int i = 0; i < viewlist.size(); i++) {
			CourseView courseView = new CourseView();
			courseView.setTeacherId(userId);
			if (courseService.isTeacher(courseView.getTeacherId())) {
				viewlist.get(i).setTeacher(1);
			} else {
				viewlist.get(i).setTeacher(0);
			}

		}

		result.setData(viewlist);
		return result;
	}

	@RequestMapping("/homeworkTitle.action")
	@ResponseBody
	public ResultBean homeworkTitle(HomeworkView homeworkView) {
		ResultBean result = new ResultBean();
		HomeworkEntity homeWork = courseService.getHomeWorkByLinkId(homeworkView);
		if (homeWork != null) {
			result.setStatus(true);
			result.setData(homeWork);
		}
		return result;
	}

	@RequestMapping("/homeWorkEdit.action")
	public ModelAndView homeWorkEdit(String id) {
		ModelAndView result = new ModelAndView("/course/client/home_work_edit");
		HomeworkView view = courseService.getHomeWorkById(id);
		result.addObject("homeWork", view);
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/homeWorkView.action")
	public ModelAndView homeWorkView(String id) {
		ModelAndView result = new ModelAndView("/course/client/home_work_view");
		HomeworkView view = courseService.getHomeWorkById(id);
		view.setCreateTime(sdf.format(view.getCreateDate()));
		result.addObject("homeWork", view);
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/homeWorkAddQuestion.action")
	public ModelAndView homeWorkAddQuestion(String id) {
		ModelAndView result = new ModelAndView("/course/client/home_work_question");
		HomeworkView view = courseService.getHomeWorkById(id);
		view.setCreateTime(sdf.format(view.getCreateDate()));
		result.addObject("homeWork", view);
		result.addObject("aq", new HomeworkAnswerQuestionEntity());
		result = getRoleAndNum(result);
		return result;
	}

	*//**
	 * 上传文件
	 *//*
	@RequestMapping("/upload.action")
	@ResponseBody
	public ResultBean upload(HomeworkView homeworkView) throws Exception {
		WebContext.init4CrossDomainAjax();
		return courseService.uploadDocument(homeworkView);
	}

	@RequestMapping("/download.action")
	public void download(@RequestParam(required = true) String fileId, String fileType, HttpServletResponse response)
			throws Exception {

		OutputStream output = new BufferedOutputStream(response.getOutputStream());
		InputStream input = null;
		try {
			FileEntity file = new FileEntity();
			if (StringUtil.isNotEmpty(fileType)) {
				file = fileService.getFile(fileId, fileType);
			} else {
				file = fileService.getFile(fileId, FILE_TYPE_HOMEWORK);
			}

			byte[] binary = file.getBinary();
			String fileName = file.getFileName();
			fileName = URLEncoder.encode(fileName, "UTF-8");
			response.reset();
			response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			response.addHeader("Content-Length", "" + file.getFileSize());
			response.setContentType("application/octet-stream;charset=UTF-8");
			if (StringUtil.isNotEmpty(file.getPath()) && StringUtil.isNotEmpty(ConfigUtil.getFileStorageRoot())) {
				// 文件有路径
				input = FileUtil.getInputStream(ConfigUtil.getFileStorageRoot(), file.getPath());
				IOUtils.copyLarge(input, output);
			} else {
				output.write(binary);
			}
			output.flush();
		} finally {
			IOUtils.closeQuietly(output);
			IOUtils.closeQuietly(input);
		}
	}

	*//**
	 * 课程作业管理页面初始化
	 * 
	 * @param
	 * @return
	 *//*
	@RequestMapping("/homeWorkReviewInit.action")
	public ModelAndView homeWorkReviewInit(String linkId, String linkType) {
		ModelAndView mv = new ModelAndView("/course/client/home_work_review");
		HomeworkView home = null;
		if (StringUtil.isNotEmpty(linkId) && StringUtil.isNotEmpty(linkType)) {
			home = courseService.searchworkDetail(linkId, linkType);
		}
		if (home == null) {
			home = new HomeworkView();
		}
		BaseEntity condition = new BaseEntity();
		condition.setPageSize(5);
		mv.addObject("linkId", linkId);
		mv.addObject("linkType", linkType);
		mv.addObject("title", home.getTitle());
		mv.addObject("courseTitle", home.getCourseTitle());
		mv.addObject("condition", condition);
		return mv;
	}

	*//**
	 * 导师品鉴作业初始化
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/homeworkReviewList.action")
	@ResponseBody
	public ResultBean homeworkReviewList(HomeworkView home) {

		return courseService.homeworkReviewList(home, PropertyUtil.getProperty("res.context.root"));
	}

	@RequestMapping("/gethomeworkReviewList.action")
	@ResponseBody
	public ResultBean gethomeworkReviewList(HomeworkView home) {
		return courseService.gethomeworkReviewList(home);
	}

	@RequestMapping("/gethomeworkReviewListByJson.action")
	@ResponseBody
	public JSONArray gethomeworkReviewListByJson(String courseId, String linkId, String linkType) throws IOException {

		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		List<HomeworkView> homeworkList = courseService.gethomeworkReviewListJson(courseId, linkId, linkType,
				PropertyUtil.getProperty("res.context.root"));
		if (homeworkList.size() > 0) {
			for (HomeworkView view : homeworkList) {
				Map<String, Object> courseERMap = new HashMap<String, Object>();
				courseERMap.put("userName", view.getCreateUser());
				courseERMap.put("audioContent", view.getContent() == null ? "" : view.getContent());
				// courseERMap.put("answerFileName", view.getAnswerFileName());
				courseERMap.put("score", view.getScore() == null ? "" : view.getScore());
				courseERMap.put("teacherComment", view.getTeacherComment() == null ? "" : view.getTeacherComment());
				// courseERMap.put("teacherScore", view.getTeacherScore() ==
				// null?"":view.getTeacherScore());
				// courseERMap.put("teacherComments", view.getTeacherComments() ==
				// null?"":view.getTeacherComments());
				resultList.add(courseERMap);
			}
		}
		jo.put("resultList", resultList);
		json.add(jo);
		return json;
	}

	*//**
	 * 导师评价
	 * 
	 * @author Id
	 * @param SCORE teacherComment
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/saveHomeworkReview.action")
	@ResponseBody
	public ResultBean saveHomeworkReview(HomeworkView home) {

		return courseService.saveHomeworkReview(home);
	}

	*//**
	 * 检索我的课程列表（检索用户课程，包括已关注、已学、已学完等检索条件以及检索列表中每一项课程的问卷）
	 * 
	 * @param finishType 完成类型（已收藏 1、已学2、3 已学完）
	 * @return 我的课程列表
	 * @author WUXIAOXIANG
	 *//*
	@RequestMapping("/userCourseList.action")
	@ResponseBody
	public ResultBean userCourseList(String finishType, Integer pageSize, Integer pageNumber) {
		// 检索我的课程
		ResultBean result = courseService.userCourseList(finishType, pageSize, pageNumber);
		return result;
	}

	@RequestMapping("/userCompulsoryList.action")
	@ResponseBody
	public ResultBean userCompulsoryList(String courseType, Integer pageSize, Integer pageNumber) {
		// 检索我的课程
		ResultBean result = courseService.userCompulsoryList(courseType, pageSize, pageNumber);
		return result;
	}

	*//**
	 * 检索我的作业列表
	 * 
	 * @author liuzhen
	 * @param
	 * @return 返回结果
	 *//*
	@RequestMapping("/userHomeworkList.action")
	@ResponseBody
	public ResultBean userHomeworkList(HomeworkView homeworkView, Integer pageSize, Integer pageNumber) {
		// 检索我的作业
		ResultBean result = courseService.userHomeworkList(homeworkView, pageSize, pageNumber);
		return result;
	}

	*//**
	 * 检索当前课程的所有作业
	 * 
	 * @author liuzhen
	 * @param
	 * @return 返回结果
	 *//*
	@RequestMapping("/alluserHomeworkList.action")
	@ResponseBody
	public ResultBean alluserHomeworkList(HomeworkView homeworkView, Integer pageSize, Integer pageNumber) {
		ResultBean result = courseService.getAlluserHomeworkList(homeworkView, pageSize, pageNumber);
		return result;
	}

	*//**
	 * 检索手机端导师个人中心的课程列表
	 * 
	 * @param pageSize   每页显示行数
	 * @param pageNumber 当前页
	 * @return 课程列表
	 * @author WUXIAOXIANG
	 *//*
	@RequestMapping("/teacherCourseAppList.action")
	@ResponseBody
	public ResultBean teacherCourseAppList(Integer pageSize, Integer pageNumber) {
		ResultBean result = courseService.teacherCourseAppList(pageSize, pageNumber);
		return result;
	}

	*//**
	 * 加入课程-支付详情初始化
	 * 
	 * @param courseId
	 * @return
	 *//*
	@SuppressWarnings({ "rawtypes" })
	@RequestMapping("/paydetail.action")
	public ModelAndView homeWorkReviewInit(String courseId) {
		ModelAndView mv = new ModelAndView("/course/client/course_pay_detail");
		UserEntity user = userService.searchById(WebContext.getSessionUserId());
		Map map = (Map) courseService.immediatePayDetail(courseId).getData();
		CourseView view = (CourseView) map.get("courseView");
		UserPoint userPoint = (UserPoint) map.get("userPoint");
		BigDecimal remainPoints = (BigDecimal) map.get("remainPoints");
		mv.addObject("user", user);
		mv.addObject("view", view);
		mv.addObject("userPoint", userPoint);
		mv.addObject("remainPoints", remainPoints);
		return mv;
	}

	*//**
	 * 用户管理页面 检索列表
	 * 
	 * @param condition
	 * @return 返回结果
	 *//*
	@RequestMapping("/search.action")
	@ResponseBody
	public ResultBean search(UserOperEntity condition) {
		return courseService.search4Page(condition);
	}

	@RequestMapping("/userOperInit.action")
	public ModelAndView userOperInit(String courseId) {
		ModelAndView result = new ModelAndView("/course/client/userOper");
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/mgrApprove.action")
	public ModelAndView mgrApprove(String courseId) {
		ModelAndView result = new ModelAndView("/course/client/mgr_approve");
		result = getRoleAndNum(result);
		return result;
	}
	
	@RequestMapping("/approveSearch.action")
	@ResponseBody
	public ResultBean approveSearch(CourseApproveEntity entity){
		return courseService.approveSearch(entity);
	}
	
	@RequestMapping("/saveHomeWork.action")
	@ResponseBody
	public ResultBean saveHomeWork(HomeworkView homework) throws Exception {
		return courseService.saveHomeWorkAnswer(homework);
	}

	@RequestMapping("/saveHomeWorkById.action")
	@ResponseBody
	public ResultBean saveHomeWorkById(HomeworkView homework) throws Exception {
		return courseService.saveHomeWorkById(homework);
	}
	
	@RequestMapping("/saveHomeWorkReviewById.action")
	@ResponseBody
	public ResultBean saveHomeWorkReviewById(String courseJson) throws Exception {
		HomeworkView homework = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, HomeworkView.class);
		return courseService.saveHomeWorkReviewById(homework);
	}

	@RequestMapping("/saveHomeWorkAnswerQuestion.action")
	@ResponseBody
	public ResultBean saveHomeWorkAnswerQuestion(HomeworkAnswerQuestionEntity aq) throws Exception {
		return courseService.saveHomeWorkAnswerQuestion(aq);
	}

	@RequestMapping("/answerQuestionList.action")
	@ResponseBody
	public ResultBean answerQuestionList(HomeworkAnswerQuestionEntity aq) {
		return courseService.searchAnswerQuestionList(aq);
	}

	@RequestMapping("/likeHomeWorkById.action")
	@ResponseBody
	public ResultBean likeHomeWorkById(String id) throws Exception {
		return courseService.likeHomeWorkById(id);
	}

	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/institutionlist.action")
	public ModelAndView institutionlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/institution/manager/institution_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		return result;
	}

	*//**
	 * 学校评价总列表
	 * 
	 * @return
	 *//*
	@RequestMapping("/institutionManger.action")
	public ModelAndView institutionManger() {
		ModelAndView result = new ModelAndView();
		UserEntity user = WebContext.getSessionUser();
		if (!"游客".equals(user.getPersonName())) {
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			TpUserEntity entity =new TpUserEntity();
			entity = userManagementService.searchById(userId);
			if ("A".equals(role) && "001".equals(entity.getDuty())) {
				result = new ModelAndView("/institution/manager/institutionManagerBack");
			} else if ("C".equals(role) || ("A".equals(role) && !"001".equals(entity.getDuty()))) {
				result = new ModelAndView("/institution/manager/institutionEvlutionStudentBack");
			}else if("B".equals(role)) {
				result = new ModelAndView("/institution/manager/institutionBack");
			}
		} else {
			result = new ModelAndView("/institution/manager/institutionEvlutionStudentBack");
		}
		
		return result;
	}
	

	*//**
	 * 标签一览
	 *//*
	@RequestMapping("/searchTagList2.action")
	@ResponseBody
	public ResultBean searchTagList2(TagInstitutionEntityView tagEntity) {
		return taxonomyInstitutionService.searchTagList2(tagEntity);

	}
	*//**
	 * 标签一览
	 *//*
	@RequestMapping("/searchInstitutionTagList2.action")
	@ResponseBody
	public ResultBean searchInstitutionTagList2(TagInstitutionEntityView tagEntity) {
		tagEntity.setCreateUser(WebContext.getSessionUserId());
		return taxonomyInstitutionService.searchInstitutionTagList2(tagEntity);

	}
	*//**
	 * 标签一览
	 *//*
	@RequestMapping("/searchTagSchoolList2.action")
	@ResponseBody
	public ResultBean searchTagSchoolList2(TagInstitutionEntityView tagEntity) {
		return taxonomyInstitutionService.searchTagList2(tagEntity);

	}
	*//**
	 * 分类基本列表
	 * 
	 * @return
	 *//*
	@RequestMapping("/institutioninit.action")
	public ModelAndView institutioninit() {
		ModelAndView mv = new ModelAndView("/institutiontaxonomy/manager/taxonomy_course_list");
		BaseEntity condition = new BaseEntity();
		condition.setPageSize(5);
		mv.addObject("condition", condition);
		return mv;
	}

	*//**
	 * 分类查询列表
	 * 
	 * @param businessType
	 * @return
	 *//*
	@RequestMapping("/searchTagTypeList2.action")
	@ResponseBody
	public List<TagTypeInstitutionView> searchTagTypeList2(String businessType) {

		return taxonomyInstitutionService.searchTagTypeList2(businessType);

	}

	*//**
	 * 
	 * 删除标签
	 * 
	 * @param tagIds
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/delTag.action")
	@ResponseBody
	public ResultBean delTag(String tagId, String ico) throws Exception {

		return taxonomyInstitutionService.delTag(tagId, ico);

	}

	*//**
	 * 保存信息
	 * 
	 * @param browserInfo
	 * @param sysInfo
	 * @param IpInfo
	 * @param operDetail
	 * @param operSite
	 * @return
	 *//*
	@RequestMapping("/insertUserInstitution.action")
	@ResponseBody
	public JSONArray insertUserInstitution(String browserInfo, String sysInfo, String IpInfo, String operDetail,
			String operSite) {
		UserOperEntity oper = new UserOperEntity();
		oper.setSysInfo(browserInfo + "(" + sysInfo + ")");
		oper.setOperDetail(operDetail);
		oper.setOperSite(operSite);
		oper.setOperTime(new Date());
		oper.setOperIP(IpInfo);
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		tagInstitutionEvalutionService.inserUserOper(oper);
		jo.put("result", "success");
		json.add(jo);
		return json;
	}

	*//**
	 * 编辑标签
	 * 
	 * @param TagEntity
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/updateTag.action")
	@ResponseBody
	public ResultBean updateTag(TagInstitutionEntity tagEntity) throws Exception {

		return taxonomyInstitutionService.updateTag(tagEntity);

	}

	*//**
	 * 保存新增标签
	 * 
	 * @param parentId 父标签ID,tagTypeId:标签类ID,tagName:标签名称
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveTag2.action")
	@ResponseBody
	public ResultBean saveTag2(TagInstitutionEntityView tagEntity) throws Exception {
		return taxonomyInstitutionService.saveTag2(tagEntity);

	}

	*//**
	 * 
	 * 初始化课程列表
	 * 
	 * @param condition 课程对象
	 * @return 返回课程列表初始化页面
	 *//*
	@RequestMapping("/institutionListInit.action")
	public ModelAndView institutionListInit(InstitutionMessageEntity condition) {
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		result.addObject("course", new InstitutionMessageEntity());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}

	*//**
	 * 
	 * 检索评价列表
	 * 
	 * @param queryInfo 机构对象
	 * @return 返回评价列表
	 *//*
	@RequestMapping("/listSearch.action")
	@ResponseBody
	public ResultBean listSearch(TbInstitutionEvalutionManagerView queryInfo) {
		queryInfo.setCreateUser(WebContext.getSessionUserId());
		return taxonomyInstitutionService.listSearch(queryInfo);
	}

	*//**
	 * 
	 * 检索评价问卷列表
	 * 
	 * @param queryInfo 机构对象
	 * @return 返回评价列表
	 *//*
	@RequestMapping("/listQestionSearch.action")
	@ResponseBody
	public ResultBean listQestionSearch(MeasurementInstitutionEntity queryInfo) {
		UserEntity user = WebContext.getSessionUser();	
		String role = user.getUserKbn();
		//判断权限
		String userId = user.getCreateUser();
		queryInfo.setCreateUser(userId);
		return taxonomyInstitutionService.listSchoolMeasurementSearch(queryInfo);
	}

	*//**
	 * 评价新增页面初始化
	 * 
	 * @return
	 *//*
	@RequestMapping("/addInit.action")
	public ModelAndView addInit() {
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_add");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		result.addObject("pageType", "add");
		result.addObject("course", new TbInstitutionEvalutionManagerView());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}

	*//**
	 * 搜素全部学校
	 * 
	 * @param query
	 * @return
	 *//*
	@RequestMapping("/searchSchoolAll.action")
	@ResponseBody
	public ResultBean searchSchoolAll(InstitutionEntity query) {
		return institutionService.getTeacherAll(query);
	}
	*//**
	 * 搜素评价问卷全部学校
	 * 
	 * @param query
	 * @return
	 *//*
	@RequestMapping("/searchEvalutionSchoolAll.action")
	@ResponseBody
	public ResultBean searchEvalutionSchoolAll(InstitutionEntity query) {
		if(null!=query && StringUtil.isNotBlank(query.getInstitutionNameseacher())) {
			query.setInstitutionName(query.getInstitutionNameseacher());
		}
		return institutionService.getEvalutionTeacherAll(query);
	}

	*//**
	 * 
	 * 根据学校ID，检索学校信息
	 * 
	 * @return
	 *//*
	@RequestMapping("/searchSchool.action")
	@ResponseBody
	public ResultBean searchSchool(String id) {
		ResultBean result = ResultBean.success();
		result.setStatus(true);
		return institutionService.searchSchool(id);
	}

	*//**
	 * 保存评价任务信息
	 * 
	 * @param courseJson 任务信息
	 * @return 返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/saveEvalutionInfo.action")
	@ResponseBody
	public ResultBean saveEvalutionInfo(String courseJson) throws Exception {
		TbInstitutionEvalutionManagerView course = JSONUtil.parse(
				JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson,
				TbInstitutionEvalutionManagerView.class);
		ResultBean result = ResultBean.success();
		course.setStatus("0");
		if (StringUtil.isEmpty(course.getId())) {
			institutionService.insertCourseCatagory(course);
		} else {
			institutionService.updateCourseAll(course);
		}
		return result;
	}

	*//**
	 * 暂存
	 * 
	 * @param courseJson
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/zancunEvalutionInfo.action")
	@ResponseBody
	public ResultBean zancunEvalutionInfo(String courseJson) throws Exception {
		TbInstitutionEvalutionManagerView course = JSONUtil.parse(
				JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson,
				TbInstitutionEvalutionManagerView.class);
		ResultBean result = ResultBean.success();
		course.setStatus("0");
		if (StringUtil.isEmpty(course.getId())) {
			institutionService.insertCourseCatagory(course);
		} else {
			institutionService.updateCourseAll(course);
		}
		return result;
	}

	*//**
	 * 删除评价任务信息
	 * 
	 * @param id
	 * @return
	 *//*
	@RequestMapping("/deleteEvalution.action")
	@ResponseBody
	public ResultBean deleteEvalution(String id) {
		ResultBean result = new ResultBean();
		institutionService.deleteEvalution(id);
		result.setMessages(getMessage(MSG_S_DELETE));
		return result;
	}

	@RequestMapping("/editInit.action")
	public ModelAndView editInit(String id) {
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_add");
		String status = "0";
		String type = "edit";
		TbInstitutionEvalutionManagerView course = tagInstitutionEvalutionService.searchEvalutionById(id, type);
		String evalutionId = course.getId();
		String institutionId = course.getTeacherId();
		List<TagLinkInstitutionEntity> entityList = new ArrayList<TagLinkInstitutionEntity>();
		TagLinkInstitutionEntity entity = new TagLinkInstitutionEntity();
		entity.setLinkId(evalutionId);
		entityList = tagInstitutionEvalutionService.searchList(entity);
		if (null != entityList) {
			for (int i = 0; i < entityList.size(); i++) {
				if (StringUtils.isBlank(entityList.get(i).getParentTagId())) {
					TagInstitutionEntity isentity = new TagInstitutionEntity();
					isentity.setTagId(entityList.get(i).getTagId());
					isentity = tagInstitutionService.searchFirstData(isentity);
					course.setFirstTagName(isentity.getTagName());
					course.setFirstTag(isentity.getTagName());
				} else {
					TagInstitutionEntity isentity = new TagInstitutionEntity();
					isentity.setTagId(entityList.get(i).getTagId());
					isentity = tagInstitutionService.searchFirstData(isentity);
					course.setSecondTag(isentity.getTagName());
					course.setSecondTagName(isentity.getTagName());
				}
			}
		}
		InstitutionEntity institution = new InstitutionEntity();
		institution = institutionService.selectInstitutionById(institutionId);
		course.setInstitutionName(institution.getInstitutionName());
		course.setSchoolType(institution.getSchoolType());
		course.setInstitutionAbbreviation(institution.getInstitutionAbbreviation());
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		result.addObject("tagsSecond", tagsSecond);
		result.addObject("course", course);
		result.addObject("pageType", type);
		return result;
	}

	*//**
	 * 问卷添加
	 * 
	 * @param courseId 评价任务ID
	 * @return
	 *//*
	@RequestMapping("/measureInit.action")
	@ResponseBody
	public ModelAndView measureInit(String courseId) {
		ModelAndView result = new ModelAndView("institution/manager/insitution_measure_add");
		MeasurementInstitutionEntity measure = new MeasurementInstitutionEntity();
		result.addObject("measure", measure);
		result.addObject("courseId", courseId);
		result.addObject("pageType", "add");
		return result;
	}
	*//**
	 * 问卷添加
	 * 
	 * @param courseId 评价任务ID
	 * @return
	 *//*
	@RequestMapping("/measureInit1.action")
	@ResponseBody
	public ModelAndView measureInit1(String courseId) {
		ModelAndView result = new ModelAndView("institution/manager/insitution_measure_editor");
		MeasurementInstitutionEntity measure = new MeasurementInstitutionEntity();
		measure.setLinkId(courseId);
		measure = measurementInstitutionService.findInstitutionMeasurement(measure);
		List<QuestionInstitutionEntity> questionList = measurementInstitutionService.searchQuestionList(measure.getId());
		measure.setQuestionList(questionList);
		result.addObject("measure", measure);
		result.addObject("courseId", courseId);
		result.addObject("pageType", "add");
		return result;
	}
	*//**
	 * 
	 * 问卷编辑页面初始化
	 * 
	 * @param courseId
	 * @return
	 *//*
	@RequestMapping("/measureInsitutionEditInit.action")
	@ResponseBody
	public ModelAndView measureEditInit(String courseId) {
		ModelAndView result = new ModelAndView("institution/manager/insitution_measure_add");
		MeasurementInstitutionEntity measure = measurementInstitutionService.seachMeasureById(courseId);
		result.addObject("measure", measure);
		result.addObject("courseId", courseId);
		result.addObject("isOnlyView", true);
		result.addObject("pageType", "edit");
		return result;
	}

	*//**
	 * 
	 * 问卷添加/编辑
	 * 
	 * @param measureJson
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveMeasure.action")
	@ResponseBody
	public ResultBean saveMeasure(String measureJson) throws Exception {
		MeasurementInstitutionEntity measure = JSONUtil.parse(
				JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), measureJson,
				MeasurementInstitutionEntity.class);
		ResultBean result = ResultBean.success();
		if (StringUtil.isEmpty(measure.getId())) {
			measurementInstitutionService.saveMeasure(measure);
		} else {
			measurementInstitutionService.updateInstitutionMeasure(measure);
		}
		return result;
	}

	*//**
	 * 
	 * 初始化评价问卷列表
	 * 
	 * @param condition 评价对象
	 * @return 返回课程列表初始化页面
	 *//*
	@RequestMapping("/institutionQuestListInit.action")
	public ModelAndView institutionQuestListInit(InstitutionMessageEntity condition) {
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_question_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		result.addObject("course", new InstitutionMessageEntity());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}

	*//**
	 * 
	 * 评价的发布/取消发布
	 * 
	 * @param id     课程id
	 * @param status 评价状态：0 未发布 1 已发布
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/publicEvalution.action")
	@ResponseBody
	public ResultBean publicEvalution(String id, String status) throws Exception {
		tagInstitutionEvalutionService.publicEvalution(id, status);
		return ResultBean.success(MSG_S_SUBMIT);
	}

	*//**
	 * 问卷显示页面
	 *//*
	@RequestMapping("/question.action")
	public ModelAndView question(String linkId, String linkType) {
		ModelAndView result = new ModelAndView("/institution/manager/institution_evalution_questionner_item");
		result.addObject("linkId", linkId);
		result.addObject("linkType", linkType);
		return result;
	}

	*//**
	 * 问卷题目选项
	 *//*
	@RequestMapping("/questionnaireDetial.action")
	@ResponseBody
	public ResultBean questionnaireDetial(String linkId, String linkType) {
		return questionInstitutionService.questionnaireDetial(linkId, linkType);
	}

	*//**
	 * 问卷提交
	 *//*
	@RequestMapping("/submitQuestionnaire.action")
	@ResponseBody
	public ResultBean submitQuestionnaire(String questionIds, String optionIds, String measureId) {
		return questionInstitutionService.submitQuestionnaire(questionIds, optionIds, measureId);
	}

	*//**
	 * 问卷调查结果列表
	 * 
	 * @author heyaqin
	 * @return 返回结果
	 *//*
	@RequestMapping("/questionResultList.action")
	public ModelAndView questionResultList(MeasurementInstitutionEntity condition) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_result_list");
		mv.addObject("condition", condition);
		UserEntity user = WebContext.getSessionUser();
		if (!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if (roleList.size() > 0) {
				for (UserRoleEntity role : roleList) {
					if (role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			} else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}

	*//**
	 * 问卷调查列表检索
	 * 
	 * @author
	 * @return 返回结果
	 *//*
	@RequestMapping("/questionResultListSearch.action")
	@ResponseBody
	public ResultBean questionResultListSearch(MeasurementInstitutionEntity condition) {
			UserEntity user = WebContext.getSessionUser();	
			String userId = user.getCreateUser();
			condition.setCreateUser(userId);
		return userManagementService.selectQuestionResultList(condition);
	}

	*//**
	 * 问卷统计列表初始化
	 * 
	 * @author
	 * @param id 问卷ID
	 * @return 返回结果
	 *//*
	@RequestMapping("/viewMeasurementListInit.action")
	public ModelAndView viewMeasurementListInit(String id) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_statistics");
		int optionNum = userManagementService.searchInstitutionTotalOptions(id) + 1;
		QuestionOptionInstitutionEntity questionOptionEntity = new QuestionOptionInstitutionEntity();
		questionOptionEntity.setMeasureId(id);
		mv.addObject("questionOptionEntity", questionOptionEntity);
		mv.addObject("optionNum", optionNum);
		// 问卷统计结果列表
		ArrayList<Map<String, String>> optionMapList = userManagementService.selectInstitutionOptionsList(id);
		mv.addObject("optionMapList", optionMapList);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 问卷统计列表检索
	 * 
	 * @author HEYAQIN
	 * @param id 问卷ID
	 * @return 返回结果
	 *//*
	@RequestMapping("/viewMeasurementList.action")
	@ResponseBody
	public ArrayList<Map<String, String>> viewMeasurementList(String id) {
		if (StringUtil.isEmpty(id)) {
			return null;
		} else {
			ArrayList<Map<String, String>> optionMapList = userManagementService.selectInstitutionOptionsList(id);
			return optionMapList;
		}
	}

	*//**
	 * 问卷参与人员 检索
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/viewUserSearch.action")
	@ResponseBody
	public ResultBean viewUserSeacher(String measurementId) {
		return userManagementService.selectEvalutionQuestionUserList(measurementId);
	}

	*//**
	 * 问卷参与人员回答详情
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/viewDetailInit.action")
	public ModelAndView viewDetailInit(UserMeasurementResultEntity condition) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_user_detail");
		if (StringUtil.isEmpty(condition.getMeasurementId())) {
			throw new BusinessException("参数错误!");
		}

		List<UserMeasurementResultEntity> list = measuermentService
				.searchInstitutionUserMeasurementResultByEntiy(condition);
		if (list != null && list.size() != 0) {
			mv.addObject("condition", list.get(0));
		} else {
			mv.addObject("condition", new UserMeasurementResultEntity());
		}
		return mv;
	}

	*//**
	 * 问卷回答详情
	 * 
	 * @param measurementId
	 * @param userId
	 * @return
	 *//*
	@RequestMapping("/viewUserAnswerSearch.action")
	@ResponseBody
	public ResultBean viewUserAnswerSearch(String measurementId, String userId) {
		return userManagementService.selectEvalutionAnswerUserList(measurementId, userId);
	}

	*//**
	 * 检索测试信息
	 * 
	 * @param linkId
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchMeasuerment.action")
	@ResponseBody
	public ResultBean searchMeasuerment(String measurementId, String userId) {
		if (StringUtil.isEmpty(measurementId)) {
			return ResultBean.error(getMessage("参数错误!"));
		}
		ResultBean result = ResultBean.success();
		List<QuestionInstitutionEntity> questionList = measurementInstitutionService
				.searchMeasuermentInstitutionNormalAndExam(userId, measurementId);
		result.setData(questionList);
		return result;
	}

	*//**
	 * 问卷参与人员 画面初始化
	 * 
	 * @param condition
	 * @return
	 *//*
	@SuppressWarnings("unchecked")
	@RequestMapping("/viewUserInit.action")
	public ModelAndView viewUserInit(MeasurementInstitutionEntity condition) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_user_list");
		mv.addObject("measurementId", condition.getId());
		List<MeasurementInstitutionEntity> list = measurementInstitutionService.search(condition);
		if (list != null && list.size() != 0) {
			mv.addObject("condition", list.get(0));
		} else {
			throw new BusinessException("参数错误!");
		}
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 删除问卷调查列表
	 * 
	 * @author heyaqin
	 * @return 返回结果
	 *//*
	@RequestMapping("/deleteQuestionList.action")
	@ResponseBody
	public ResultBean deleteQuestionList(String id) {
		userManagementService.deleteEvalutionQuestionList(id);
		return ResultBean.success();
	}

	*//**
	 * 导入页面初始化
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/importInit.action")
	public String importInit() {
		return "institution/manager/institution_import";
	}

	*//**
	 * 导入机构
	 * 
	 * @param linkId
	 * @return 返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/importUpload.action")
	@ResponseBody
	public ResultBean importCourse(@RequestParam("institution") CommonsMultipartFile institution,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		InputStream input = null;
		try {
			input = institution.getInputStream();
			File targetFile = FileUtil.writeToTmpFile(input);
			return institutionService.importInstitution(targetFile);
		} finally {
			IOUtils.closeQuietly(input);
		}
	}

	*//**
	 * 批量导出机构
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/exportInstitution.action")
	@ResponseBody
	public InstitutionManagerView exportInstitution(InstitutionEntity entity) throws Exception {
		List<InstitutionManagerView> viewList = new ArrayList<InstitutionManagerView>();
		UserEntity userEntity = WebContext.getSessionUser();
		if (!"游客".equals(userEntity.getPersonName())) {
			String role = userEntity.getUserKbn();
			String userId=userEntity.getUserId();
			if("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				entity.setCreateUser(userId);
				viewList = institutionService.exportInstitution(entity);
			}else if("B".equals(role)) {
				entity.setCreateUser(userId);
				viewList = institutionService.exportManagerInstitution(entity);
			}
		}
		
		InstitutionManagerView user = new InstitutionManagerView();
		user.setInstitutionList(viewList);
		ExcelDataBind edbUser = new ExcelDataBind(user);
		InputStream is = null;
		OutputStream os = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String date = sdf.format(new Date());
			String fileName = URLEncoder.encode("组织机构" + date + ".xlsx", "UTF-8");
			WebContext.getResponse().reset();
			is = InstitutionService.class.getResourceAsStream("institutionExportTemplate.xlsx");
			WebContext.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			WebContext.getResponse()
					.setContentType(" application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8");
			edbUser.bind(is, WebContext.getResponse().getOutputStream());
		} catch (IOException ex) {

		} finally {

			IOUtils.closeQuietly(is);
			IOUtils.closeQuietly(os);
		}
		return null;
	}

	*//**
	 * 增加机构方法
	 * 
	 * @param entity
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/addJyInstitution.action")
	public ModelAndView addJyInstitution(InstitutionEntity entity) throws Exception {
		ModelAndView mv = new ModelAndView("institution/manager/institution_add");
		InstitutionEntity institutionEntity = new InstitutionEntity();
		UserEntity user = WebContext.getSessionUser();
		String role = user.getUserKbn();
		//判断权限
		List<InstitutionEntity> schoollist=new ArrayList<InstitutionEntity>();
		String userId = user.getCreateUser();
		if ("A".equals(role)) {
			TpUserEntity tpUserEntity =new TpUserEntity();
			tpUserEntity = userManagementService.searchById(userId);
			institutionEntity.setInstitutionName(tpUserEntity.getSchoolName());
			institutionEntity=institutionService.seacherPersonEdution(institutionEntity);
			if(null!=institutionEntity) {
				entity.setInstitutionName(institutionEntity.getInstitutionName());
			}
			schoollist.add(institutionEntity);
		}else if("B".equals(role)) {
			institutionEntity.setCreateUser(userId);
			institutionEntity= institutionService.searchEdution(institutionEntity);
            if(null!=institutionEntity) {
            	entity.setInstitutionName(institutionEntity.getInstitutionName());
			}
            schoollist.add(institutionEntity);
		}
		mv.addObject("schoollist", schoollist);
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 添加学校信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/addInstitution.action")
	public ModelAndView addInstitution(String institutionParentId) throws Exception {
		InstitutionEntity entity = new InstitutionEntity();
		entity.setInstitutionParentId(institutionParentId);
		ModelAndView mv = new ModelAndView("institution/manager/institution_addSchool");
		List<InstitutionEntity> schoollist=new ArrayList<InstitutionEntity>();
		UserEntity user = WebContext.getSessionUser();
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				schoollist = institutionService.seacherSchoolLocationList(entity);
			} else if("B".equals(role)) {
				entity.setCreateUser(userId);
				schoollist= institutionService.searchEdutionSchoolList(entity);
			}
		
		mv.addObject("schoollist", schoollist);
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 删除班级
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/deleteInstitutionById.action")
	@ResponseBody
	public ResultBean deleteGradeById(String id) throws Exception {
		InstitutionEntity entity = new InstitutionEntity();
		entity.setId(id);
		institutionService.deleteInstitutionById(entity);
		return ResultBean.success(DELETE_GRADE_SUCCESS);
	}

	*//**
	 * 跳转到班级编辑页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/institutionEdit.action")
	public ModelAndView institutionEdit(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		ModelAndView mv = new ModelAndView("institution/manager/institution_edit");
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 检索班级信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchInstitution.action")
	@ResponseBody
	public ResultBean searchNotice(InstitutionEntity entity) {
		return institutionService.searchInstitutionEvalutionList(entity);
	}
	*//**
	 * 学校检索班级信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/schoolsearchInstitution.action")
	@ResponseBody
	public ResultBean schoolsearchInstitution(InstitutionEntity entity) {
		return institutionService.searchInstitutionEvalutionList1(entity);
	}
	*//**
	 * 检索班级信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchSchoolInstitution.action")
	@ResponseBody
	public ResultBean searchSchoolInstitution(InstitutionEntity entity) {
		ResultBean result = new ResultBean();
		UserEntity user = WebContext.getSessionUser();
		if (!"游客".equals(user.getPersonName())) {
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				entity.setCreateUser(userId);
				result=institutionService.searchInstitutionTeacherEvalutionList(entity);
			} else if ("C".equals(role)) {
				entity.setCreateUser(userId);
				StudentClassInfoEntity classInfo = new StudentClassInfoEntity();
				classInfo.setStudentId(userId);
				classInfo = studentClassInfoService.searchStudentClassInfo(classInfo);
			    if(null!=classInfo) {
			    	entity.setInstitutionName(classInfo.getSchoolName());
					result= institutionService.searchInstitutionEvalutionList(entity);
			    }
			}else if("B".equals(role)) {
				entity.setCreateUser(userId);
				result= institutionService.searchInstitutionManagerEvalutionList(entity);
			}
		}
		return result;
	}

	*//**
	 * 新增信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/insertInstitution.action")
	@ResponseBody
	public ResultBean insertInstitution(InstitutionEntity entity) throws Exception {
		ResultBean result = new ResultBean();
		InstitutionEntity institutionEntity = new InstitutionEntity();
		institutionEntity.setInstitutionName(entity.getInstitutionName());
		List<InstitutionEntity> datalist = (List<InstitutionEntity>) institutionService.searchInstitutionEvalutionList1(institutionEntity).getData();
		if(null!=datalist && datalist.size()>0) {
			result.setStatus(false);
			result = ResultBean.error("学校名称已存在");
		}else {
//			String institutionType=entity.getInstitutionType();
//			TagInstitutionEntity tagInstitutionEntity = new TagInstitutionEntity();
//			tagInstitutionEntity.setTagId(institutionType);
//			tagInstitutionEntity = tagInstitutionService.searchFirstData(tagInstitutionEntity);
//			entity.setInstitutionType(tagInstitutionEntity.getTagName());
			if(StringUtil.isNotBlank(entity.getSchoolNatrue())) {
				if("1".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("小学");
				}else if("2".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("初中");
				}else if("3".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("高中");
				}else if("4".equals(entity.getSchoolNatrue())) {
				   entity.setInstitutionType("九年一贯制");
			    }
			}
			institutionService.insertInstitution(entity, getCompressConfigByName("imageP"));
			result.setStatus(true);
			result= ResultBean.success(MSG_ADD_GRADE_SUCCESS);
		}
		return result;
	}
	*//**
	 * 新增信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/insertSchoolEvalutionInstitution.action")
	@ResponseBody
	public ResultBean insertSchoolEvalutionInstitution(InstitutionEntity entity) throws Exception {
		institutionService.insertInstitution(entity, getCompressConfigByName("imageP"));
		return ResultBean.success(MSG_ADD_GRADE_SUCCESS);
	}
	*//**
	 * 编辑保存
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/updateInstitution.action")
	@ResponseBody
	public ResultBean updateInstitution(InstitutionEntity entity) throws Exception {
		ResultBean result = new ResultBean();
		InstitutionEntity institutionEntity = new InstitutionEntity();
		institutionEntity.setInstitutionName(entity.getInstitutionName());
		List<InstitutionEntity> datalist = (List<InstitutionEntity>) institutionService.searchInstitutionEvalutionList1(institutionEntity).getData();
		if(null!=datalist && datalist.size()>0 && !entity.getId().equals(datalist.get(0).getId())) {
			result.setStatus(false);
			result = ResultBean.error("学校名称已存在");
		}else {
//			String institutionType=entity.getInstitutionType();
//			TagInstitutionEntity tagInstitutionEntity = new TagInstitutionEntity();
//			tagInstitutionEntity.setTagId(institutionType);
//			tagInstitutionEntity = tagInstitutionService.searchFirstData(tagInstitutionEntity);
//			entity.setInstitutionType(tagInstitutionEntity.getTagName());
			if(StringUtil.isNotBlank(entity.getSchoolNatrue())) {
				if("1".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("小学");
				}else if("2".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("初中");
				}else if("3".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("高中");
				}else if("4".equals(entity.getSchoolNatrue())) {
					   entity.setInstitutionType("九年一贯制");
				}
			}
			institutionService.updateInstitution(entity, getCompressConfigByName("imageP"));
			result.setStatus(true);
			result = ResultBean.success("创建学校成功");
		}
		return result;
	}
	*//**
	 * 编辑保存
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/updateSchoolEvalutionInstitution.action")
	@ResponseBody
	public ResultBean updateSchoolEvalutionInstitution(InstitutionEntity entity) throws Exception {
		entity.setCreateUser(WebContext.getSessionUserId());
		institutionService.updateInstitution(entity, getCompressConfigByName("imageP"));
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/institutionStudentlist.action")
	public ModelAndView institutionStudentlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/institution/manager/institution_student_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		return result;
	}

	*//**
	 * 跳转到班级查看页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/institutionLook.action")
	public ModelAndView institutionLook(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		ModelAndView mv = new ModelAndView("institution/manager/institution_look");
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 老师分享作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/shareWork.action")
	@ResponseBody
	public ResultBean shareWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.shareHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 老师取消分享作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/cancleShareWork.action")
	@ResponseBody
	public ResultBean cancleShareWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.cancleShareHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);

	}

	*//**
	 * 老师置顶作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/topWork.action")
	@ResponseBody
	public ResultBean topWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.topHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 老师取消置顶作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/cancleTopWork.action")
	@ResponseBody
	public ResultBean cancleTopWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.cancleTopHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 老师删除作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/deleteWork.action")
	@ResponseBody
	public ResultBean deleteWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.deleteWork(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/baseinstitutionlist.action")
	public ModelAndView baseinstitutionlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/baseinstitution/manager/base_institution_list");
		result.addObject("condition", condition);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/schoolbaseinstitutionlist.action")
	public ModelAndView schoolbaseinstitutionlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/baseinstitution/manager/base_school_institution_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 增加机构方法
	 * 
	 * @param entity
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/addBaseInstitution.action")
	public ModelAndView addBaseInstitution(InstitutionEntity entity) throws Exception {
		ModelAndView mv = new ModelAndView("baseinstitution/manager/base_institution_add");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList1(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		List<TagInstitutionEntity> tagList = tagInstitutionService.searchTagInstitutionAll();
		mv.addObject("tagList", tagList);
		mv.addObject("catagoryTags", tags);
		mv.addObject("tagsSecond", tagsSecond);
		mv.addObject("institution", entity);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 跳转到班级编辑页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/baseinstitutionEdit.action")
	public ModelAndView baseinstitutionEdit(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		ModelAndView mv = new ModelAndView("baseinstitution/manager/base_institution_edit");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList1(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		List<TagInstitutionEntity> tagList = tagInstitutionService.searchTagInstitutionAll();
        String secondId = entity.getFirstTag();
        TagInstitutionEntity taginfo = tagInstitutionService.searchTagInstitution(secondId);
        entity.setSecondTag(taginfo.getParentId());
		mv.addObject("tagList", tagList);
		mv.addObject("catagoryTags", tags);
		mv.addObject("tagsSecond", tagsSecond);
		mv.addObject("institution", entity);
		mv = getRoleAndNum(mv);
		return mv;
	}
	
	*//**
	 * 跳转到班级编辑页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/baseSchoolinstitutionEdit.action")
	public ModelAndView baseSchoolinstitutionEdit(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		InstitutionEntity institutionEntity = new InstitutionEntity();
		ModelAndView mv = new ModelAndView("baseinstitution/manager/base_school_institution_edit");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList1(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		List<TagInstitutionEntity> tagList = tagInstitutionService.searchTagInstitutionAll();
		List<InstitutionEntity> schoollist=new ArrayList<InstitutionEntity>();
		UserEntity user = WebContext.getSessionUser();
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			if(StringUtil.isNotBlank(entity.getInstitutionParentId())) {
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				schoollist = institutionService.seacherSchoolLocationList(entity);
			} else if("B".equals(role)) {
				entity.setCreateUser(userId);
				schoollist= institutionService.searchEdutionSchoolList(entity);
			}
			
		}else {
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				institutionEntity.setInstitutionName(tpUserEntity.getSchoolName());
				institutionEntity=institutionService.seacherPersonEdution(institutionEntity);
				entity.setInstitutionName(institutionEntity.getInstitutionName());
				schoollist.add(institutionEntity);
			}else if("B".equals(role)) {
				institutionEntity.setCreateUser(userId);
				institutionEntity= institutionService.searchEdution(institutionEntity);
				entity.setInstitutionName(institutionEntity.getInstitutionName());
				schoollist.add(institutionEntity);
			}
		}
		
		mv.addObject("schoollist", schoollist);
		mv.addObject("tagList", tagList);
		mv.addObject("catagoryTags", tags);
		mv.addObject("tagsSecond", tagsSecond);
		mv.addObject("institution", entity);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 分类基本列表
	 * 
	 * @return
	 *//*
	@RequestMapping("/baseinstitutioninit.action")
	public ModelAndView baseinstitutioninit() {
		ModelAndView mv = new ModelAndView("baseinstitutiontaxonomy/manager/base_taxonomy_course_list");
		BaseEntity condition = new BaseEntity();
		condition.setPageSize(5);
		mv.addObject("condition", condition);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 学校列表页面
	 *//*
	@RequestMapping("/schoolListInit.action")
	public ModelAndView schoolListInit(String schoolName) {
		ModelAndView result = new ModelAndView("baseinstitution/manager/school_list");
		String userId= WebContext.getSessionUserId();
		ManagerUserEntity entity= managerUserService.searchByUserId(userId);
		String mangerRole="0";
		if(null!=entity || "2542668".equals(userId)) {
			mangerRole="1";
		}else {
			mangerRole="0";
		}
		result.addObject("schoolName", schoolName);
		result.addObject("mangerRole", mangerRole);
		result = getRoleAndNum(result);
		return result;
	}

	*//**
	 * 检索课程 包括分类
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 * directionTagId 学段
	 * categoryTagId 分类
	 *//*
	@RequestMapping("/schoolList.action")
	@ResponseBody
	public ResultBean schoolList(InstitutionEntity course, String directionTagId, String categoryTagId,
			String courseStage, String sortType, Integer pageSize, Integer pageNumber,String schoolStage) {
		ResultBean result = new ResultBean();
		if(StringUtil.isNotBlank(course.getSchoolName())) {
			course.setInstitutionName(course.getSchoolName());
		}
		//区域
		if(StringUtil.isNotBlank(directionTagId)) {
			course.setFirstTag(directionTagId);
		}
		if(StringUtil.isNotBlank(directionTagId)) {
			TagInstitutionEntity entity = new TagInstitutionEntity();
			entity.setTagId(directionTagId);
			entity = tagInstitutionService.searchFirstData(entity);
			String xdName = entity.getTagName();
			course.setInstitutionType(xdName);
		}
		if(StringUtil.isNotBlank(categoryTagId)) {
			TagInstitutionEntity entity = new TagInstitutionEntity();
			entity.setTagId(categoryTagId);
			entity = tagInstitutionService.searchFirstData(entity);
			String xdName = entity.getTagName();
			course.setFirstTag(categoryTagId);
		}
		//学校阶段
		if(StringUtil.isNotBlank(schoolStage)) {
			course.setSchoolNatrue(schoolStage);
		}
		//权限查看
		String userId = WebContext.getSessionUserId();
		ManagerUserEntity entity= managerUserService.searchByUserId(userId);
		//教育局的人员暂时看到全部
		if(null!=entity || "2542668".equals(userId) || "B".equals(WebContext.getSessionUser().getUserKbn())) {
			result = courseService.schoolList(course, directionTagId, categoryTagId, courseStage, sortType,
					pageSize, pageNumber);
		}else {
			String role = WebContext.getSessionUser().getUserKbn();
			course.setCreateUser(userId);
			if("A".equals(role)) {
				result = courseService.schooTeacherlList(course, directionTagId, categoryTagId, courseStage, sortType,
						pageSize, pageNumber);
			}else if("C".equals(role)) {
				result = courseService.schooStudentlList(course, directionTagId, categoryTagId, courseStage, sortType,
						pageSize, pageNumber);
			}
		}
		
		return result;
	}
	*//**
	 * 日志操作页面 检索列表
	 * 
	 * @param condition
	 * @return 返回结果
	 *//*
	@RequestMapping("/logsearch.action")
	@ResponseBody
	public ResultBean logsearch(UserOperEntity condition) {
		return courseService.search4Page(condition);
	}
	*//**
	 * 已完成
	 * @param finishType
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 *//*
	@RequestMapping("/userCourseFinishList.action")
	@ResponseBody
	public ResultBean userCourseFinishList(String finishType, Integer pageSize, Integer pageNumber) {
		finishType ="3";
		pageSize =5;
		pageNumber=1;
		// 检索我的课程
		ResultBean result = courseService.userCourseList(finishType, pageSize, pageNumber);
		return result;
	}
	*//**
	 * 已完成
	 * @param finishType
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 *//*
	@RequestMapping("/userCourseUnFinishList.action")
	@ResponseBody
	public ResultBean userCourseUnFinishList(String finishType, Integer pageSize, Integer pageNumber) {
		pageSize =5;
		pageNumber=1;
		// 检索我的课程
		ResultBean result = courseService.userCourseUnFinishList(finishType, pageSize, pageNumber);
		return result;
	}
    *//**
                  * 作业列表
     * @return
     *//*
	@RequestMapping("/zuoyeList.action")
	@ResponseBody
	public ResultBean zuoyeList() {
		// 检索我的课程
		ResultBean result = courseService.userZyList();
		return result;
	}
    *//**
     * 
                  * 考试结果查看
     * @param UserMeasurementResultEntity condition 入参
     * @return
     *//*
    @RequestMapping("/userExamDetail.action")
    public ModelAndView userExamDetail(UserMeasurementResultEntity condition) {
    	//考试结果页面
        ModelAndView result = new ModelAndView("course/manager/user_exam_detail");
        //考试结果id是否存在
        if(StringUtil.isEmpty(condition.getMeasurementId())){
            throw new BusinessException("参数错误!");
        }
        condition.setCreateUser(WebContext.getSessionUserId());
        //查询考试结果
        List<UserMeasurementResultEntity> list = measuermentService.searchUserMeasurementResultByEntiy(condition);
        if(list != null && list.size() != 0) {
            result.addObject("condition", list.get(0));
        } else {
            result.addObject("condition", new UserMeasurementResultEntity());
        }
        //返回考试结果
        return result;
    }
	*//**
	 * 查询全部考试
	 * @param ChapterEntity chapter章节参数
	 * @param pageSize  每页展示多少条数据
	 * @param pageNumber 页码
	 * @return
	 *//*
	@RequestMapping("/myAllExam.action")
	public ModelAndView myAllExam(ChapterEntity chapter, Integer pageSize, Integer pageNumber) {
		//考试页面内容展示路径
		ModelAndView mv = new ModelAndView("baseinstitution/manager/my_exam_list");
		//查询数据表
		ResultBean result = chapterService.myAllxam(chapter, pageSize, pageNumber);
		mv.addObject("chapter", result.getData());
		return mv;
	}
	*//**
	 *检索考试
	 * @param chapter
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 *//*
	@RequestMapping("/myAllExamList.action")
	@ResponseBody
	public ResultBean myAllExamList(ChapterEntity chapter, Integer pageSize, Integer pageNumber) {
		ResultBean result = chapterService.myAllxam(chapter, pageSize, pageNumber);
		return result;
	}
	*//**
	 * 查询课工厂方法
	 * @param courseId 课程id
	 * @param content 课程内容
	 * @param type 课程类型
	 * @return
	 *//*
	@RequestMapping("/factory.action")
    @ResponseBody
    public ResultBean factory(String courseId,String content,String type)  {
        ResultBean result = new ResultBean();
        result = courseService.factory(courseId,content,type);
        return result;
    }
	*//**
	 * 用户作业列表
	 * @return
	 *//*
	@RequestMapping("/userHomeWork.action")
	public ModelAndView userHomeWork() {
		//作业列表路径
		ModelAndView result = new ModelAndView("/course/client/user_home_work");
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 查询作业列表
	 * @return
	 *//*
	@RequestMapping("/userHomeWorkSearch.action")
	@ResponseBody
	public ResultBean search(){
		//返回结果
		return courseService.searchUserHomeWork4Page();
	}
	*//**
	 * 查询学校信息
	 * @return
	 *//*
	@RequestMapping("/selectSchoolNameInfo.action")
	@ResponseBody
	public ResultBean selectSchoolNameInfo() {
		ResultBean result = new ResultBean();
		//获取登录信息
		String userId = WebContext.getSessionUserId();
		//获取角色信息
		String role = WebContext.getSessionUser().getUserKbn();
		//判断角色
		if("C".equals(role)) {
			//学生角色
			StudentClassInfoEntity entity =new StudentClassInfoEntity();
			entity.setStudentId(userId);
			List<StudentClassInfoEntity> stuList = studentClassInfoService.searchStudentClassInfoList(entity);
			stuList.get(0).setUserId(userId);
			//设置返回值数据
			result.setData(stuList.get(0));
		}else {
			//教师信息
			TpUserEntity tpUserEntity = userManagementService.searchById(userId);
			result.setData(tpUserEntity);
		}
		 
		return result;
	}
	*//**
	 * 结束课程方法
	 * @param id 课程id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/endCourse.action")
	@ResponseBody
	public ResultBean endCourse(String id) throws Exception {
		courseService.endCourse(id);
		return ResultBean.success(MSG_S_SUBMIT);
	}
	*//**
	 * 图书馆课程复制
	 * @param id 课程id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/toLibCourse.action")
	@ResponseBody
	public ResultBean toLibCourse(String id) throws Exception {
		courseService.toLibCourse(id);
		return ResultBean.success(MSG_S_SUBMIT);
	}
	*//**
	 * 课程复制方法
	 * @param id 课程主键
	 * @return
	 *//*
	@RequestMapping("/copyInfo.action")
    @ResponseBody
    public ResultBean copyInfo(String id)  {
        ResultBean result = new ResultBean();
        //课程复制实现方法
        courseService.copyCourseByHtml(id);
        result.setMessages(getMessage(MSG_S_SAVE));
        return result;
    }
    *//**
                 * 新增课程活动记录方法
     * @param courseId 课程id
     * @return
     *//*
	@RequestMapping("/addActiveInit.action")
	public ModelAndView addActiveInit(String courseId) {
	//课程活动新增页面路径
	ModelAndView result = new ModelAndView("course/manager/course_active_add");
	TagView tagQuery = new TagView();
	//一级分类对象
	List<TagView> tags = courseService.searchTagList2(tagQuery);
	//二级分类对象
	List<TagView> tagsSecond = courseService.searchTagSecondList2(tagQuery);
	//查询章节信息
	ChapterEntity entity = new ChapterEntity();
	entity.setCourseId(courseId);
	List<ChapterEntity> chapterList = courseService.selectByCourseId(entity);
	//返回前台对象
	result.addObject("catagoryTags", chapterList);
	//result.addObject("catagoryTags", tags);
	result.addObject("pageType", "add");
	result.addObject("course", new CourseManagerView());
	result.addObject("tagsSecond", tagsSecond);
	result = getRoleAndNum(result);
	return result;
	}
    *//**
                  * 保存课程章节活动记录信息
     * @param courseJson 课程信息
     * @return 返回结果
     * @throws Exception 
     *//*
    @RequestMapping("/saveActiveChapterInfo.action")
    @ResponseBody
    public ResultBean saveActiveChapterInfo(String courseJson) throws Exception {
    	//实例化对象
        CourseManagerView courseview = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, CourseManagerView.class);
    	ChapterActiveEntity course = new ChapterActiveEntity();
    	course.setChapterId(courseview.getChapterId());
    	course.setActiveFlag(courseview.getActiveFlag());
    	//根据章节id查询章节信息
    	ChapterEntity view= new ChapterEntity();
    	String chapterId = courseview.getChapterId();
    	view = chapterService.selectChapteById(chapterId);
    	//设置章节字段进行保存
    	course.setChapterTitle(view.getTitle());
    	course.setContent(courseview.getContent());
        ResultBean result = ResultBean.success();
        //保存章节活动内容
        chapterActiveService.insertInfo(course);
        return result;
    }
   *//**
             * 查询课程章节阶段活动记录信息
    * @param activeFlag
    * @param chapterId
    * @return
    *//*
	@RequestMapping("/searchActiveChapterInfo.action")
	@ResponseBody
	public ResultBean searchActiveChapterInfo(String activeFlag,String chapterId){
	ChapterActiveEntity course = new ChapterActiveEntity();
	//判断活动记录标识是否为空
	if(StringUtil.isNotBlank(activeFlag)) {
		course.setActiveFlag(activeFlag);
	}
	course.setChapterId(chapterId);
	//查询该章节的全部活动记录
	List<ChapterActiveEntity> view = chapterActiveService.searchList(course);
	ResultBean result = ResultBean.success();
	result.setData(view);
	return result;
	}
	*//**
	 * 查询课程阶段活动记录信息
	 * @param activeFlag 活动记录阶段标识
	 * @param courseId  课程主键
	 * @param chapterId  章节主键
	 * @return
	 *//*
	@RequestMapping("/searchActiveAllChapterInfo.action")
	@ResponseBody
	public ResultBean searchActiveAllChapterInfo(String activeFlag,String courseId,String chapterId){
	ChapterActiveEntity course = new ChapterActiveEntity();
	//判断活动记录标识是否为空
	if(StringUtil.isNotBlank(activeFlag)) {
		course.setActiveFlag(activeFlag);
	}
	course.setCourseId(courseId);
	if(StringUtil.isNotBlank(chapterId)) {
		course.setChapterId(chapterId);
	}
	//返回查询结果
	return chapterActiveService.searchAllCourseChapterPage(course);
	}
	*//**
	 * 查询角色的公共方法
	 * @param result
	 * @return
	 *//*
	private ModelAndView getRoleAndNum(ModelAndView result) {
    	int num = courseService.getStuStatus();
    	result.addObject("num", num);
    	UserEntity user = WebContext.getSessionUser();
    	//判断是否管理员
    	ManagerUserEntity entity = managerUserService.searchByUserId(user.getUserId());
    	if(!user.getUserName().equals("guest")) {
    		List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
    		//老师和教育局角色
    		if(roleList.size() > 0 && "A".equals(user.getUserKbn()) && null==entity && !"2542668".equals(user.getUserId())) {
    			for(UserRoleEntity role : roleList) {
    				if(role.getRoleId().equals("01")) {
    					result.addObject("role", "t");
    				}
    			}
    		//教育局角色	
    		}else if(roleList.size() > 0  && (null!=entity || "2542668".equals(user.getUserId()))){
    			result.addObject("role", "m");
    		}else if("B".equals(user.getUserKbn())){
    			result.addObject("role", "e");
    	    //家长和教研员角色
    		}else if("D".equals(user.getUserKbn()) || "E".equals(user.getUserKbn())){
    			result.addObject("role", "q");
    		//学生角色
    		}else {
    			result.addObject("role", "s");
    		}
    	}
        //管理员角色
    	if(null!=entity || "2542668".equals(user.getUserId())) {
    		result.addObject("manager", "1");
    	}else {
    		result.addObject("manager", "0");
    	}
    	return result;
    }
    *//**
     * 活动记录编辑页面方法
     * @param courseId  课程id
     * @param activeId  活动id
     * @return
     *//*
	@RequestMapping("/editActiveInit.action")
	public ModelAndView editActiveInit(String courseId,String activeId) {
	//活动编辑页面路径
	ModelAndView result = new ModelAndView("course/manager/course_active_editor");
	TagView tagQuery = new TagView();
	//查询一级分类方法
	List<TagView> tags = courseService.searchTagList2(tagQuery);
	//查询e二级分类方法
	List<TagView> tagsSecond = courseService.searchTagSecondList2(tagQuery);
	//根据课程id查询所有对应的章节信息
	ChapterEntity entity = new ChapterEntity();
	entity.setCourseId(courseId);
	List<ChapterEntity> chapterList = courseService.selectByCourseId(entity);
	//根据活动记录id查询活动章节信息
	ChapterActiveEntity active = new ChapterActiveEntity();
	active.setId(activeId);
	List<ChapterActiveEntity> activeList = chapterActiveService.searchList(active);
	//设置返回前台的字段信息
	CourseManagerView courseManagerView = new  CourseManagerView();
	courseManagerView.setChapterId(activeList.get(0).getChapterId());
	courseManagerView.setChapterTitle(activeList.get(0).getChapterTitle());
	courseManagerView.setActiveFlag(activeList.get(0).getActiveFlag());
	courseManagerView.setContent(activeList.get(0).getContent());
	//章节对象
	result.addObject("catagoryTags", chapterList);
	//result.addObject("catagoryTags", tags);
	result.addObject("pageType", "add");
	result.addObject("activeId", activeId);
	//课程对象
	result.addObject("course",courseManagerView);
	result.addObject("tagsSecond", tagsSecond);
	result = getRoleAndNum(result);
	return result;
	}
    *//**
                * 保存活动记录课程章节信息
	* @param courseJson 课程信息
	* @return 返回结果
	* @throws Exception 
	*//*
	@RequestMapping("/updateActiveChapterInfo.action")
	@ResponseBody
	public ResultBean updateActiveChapterInfo(String courseJson) throws Exception {
    //转化实例对象方法
	CourseManagerView courseview = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, CourseManagerView.class);
	ChapterActiveEntity course = new ChapterActiveEntity();
	//删除当前活动记录
	ChapterActiveEntity entity = new ChapterActiveEntity();
	String id = courseview.getActiveId();
	entity.setId(id);
	chapterActiveService.deleteInfo(entity);
	//设置保存课程的信息字段
	course.setChapterId(courseview.getChapterId());
	course.setActiveFlag(courseview.getActiveFlag());
	//查询章节信息
	ChapterEntity view= new ChapterEntity();
	String chapterId = courseview.getChapterId();
	view = chapterService.selectChapteById(chapterId);
	course.setChapterTitle(view.getTitle());
	course.setContent(courseview.getContent());
	ResultBean result = ResultBean.success();
	//保存活动记录的课程章节信息
	chapterActiveService.insertInfo(course);
	return result;
	}
    *//**
                 * 删除活动
	* @param courseJson 课程信息
	* @return 返回结果
	* @throws Exception 
	*//*
	@RequestMapping("/activeDelete.action")
	@ResponseBody
	public ResultBean activeDelete(String id) throws Exception {
	ChapterActiveEntity entity = new ChapterActiveEntity();
	//设置活动记录主键
	entity.setId(id);
	chapterActiveService.deleteInfo(entity);
	ResultBean result = ResultBean.success();
	return result;
	}
	*//**
	 * 添加页面初始化：节
	 * 
	 * @param courseId 课程ID
	 * @return
	 *//*
	@RequestMapping("/courseSmallInit.action")
	public ModelAndView courseSmallInit(String courseId, String parentId) {
		ModelAndView result = new ModelAndView("course/manager/course_three_edit");
		ChapterView chapter = new ChapterView();
		chapter.setMeasureType("4");
		result.addObject("courseId", courseId);
		result.addObject("parentId", parentId);
		result.addObject("chapterFlag", SMALL_CHAPTER);
		result.addObject("chapter", chapter);
		return result;
	}
	*//**
	 * 保存章信息
	 * 
	 * @param chapterJson
	 * @return 返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/saveExamInfo.action")
	@ResponseBody
	public ResultBean saveExamInfo(String chapterJson, String videoLength) throws Exception {
		//将字符串转化成章节对象
		ChapterView chapter = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateMinuteFormatDefualt()),
				chapterJson, ChapterView.class);
		ResultBean result = ResultBean.success();
		//设置章节状态0未发布，1已发布
		chapter.setStatus("1");
		//设置加入课程1是已加入
		chapter.setIsCourse("1");
		if (StringUtil.isEmpty(chapter.getId())) {
			//未存在的章节考试直接保存
			chapterService.insertChapterInfoAll(chapter, videoLength);
		} else {
			//已存在的章节考试更新信息
			chapterService.updateChapterAll(chapter, videoLength);
		}
		//返回信息
		return result;
	}
    *//**
     * 
                  * 查看考试结果
     * @param condition
     * @return
     *//*
    @SuppressWarnings({ "unchecked"})
    @RequestMapping("/userExamInfoInit.action")
    public ModelAndView userExamInfoInit(MeasurementEntity condition) {
        ModelAndView result = new ModelAndView("course/manager/user_exam_info");
        //参数为空抛出异常
        if(StringUtil.isEmpty(condition.getLinkId())){
            throw new BusinessException("参数错误!");
        }
        //根据参数查询考试结果
        List<MeasurementEntity> list = courseService.search(condition);
        if(list != null && list.size() != 0) {
            result.addObject("condition", list.get(0));
        } else {
            throw new BusinessException("参数错误!");
        }
        //返回考试信息
        return result;
    }
    *//**
                 * 根据课程id获取章节考试状态信息
     * @param courseId
     * @return
     * @throws Exception
     *//*
	@RequestMapping("/searchExamStatus.action")
	@ResponseBody
	public ChapterView searchExamStatus(String courseId) throws Exception {
		//查询章节信息
		ChapterView view = chapterService.seacrhExamStatus(courseId);
		return view;
	}
	@SuppressWarnings("unchecked")
	*//**
	* 前端请求-章节详情
	* @param chapterId 章节ID
	* @return 
	*//*
	@RequestMapping("/toChapterDetail.action")
	public ModelAndView toChapterDetail(String courseId,String chapterId){
		ModelAndView mv = new ModelAndView("course/client/course_chapter_detail");
		ResultBean result = courseService.searchCourseDetail(courseId);
		CourseView courseView = (CourseView) result.getData();
		mv.addObject("courseView", courseView);
		mv.addObject("note", new NoteDataEntity());
		mv.addObject("homeWork", new HomeworkView());
		result = chapterService.chapterDetail(courseId, chapterId);
		ChapterView chapterView = (ChapterView) result.getData();
		ChapterEntity entity = new ChapterEntity();
		entity.setCourseId(courseId);
		List<ChapterEntity> chapterCourseList = courseService.selectByCourseId(entity);
		mv.addObject("chapterCourseList", chapterCourseList);
		mv.addObject("chapterView", chapterView);
		//检索章节列表
		List<ChapterView> cvs = (List<ChapterView>) chapterService.chapterList(courseId);
		mv.addObject("chapterList", cvs);
		//相关课程
		result = courseService.recommendedCourses("5", courseId);
		List<CourseEntity> otherCourseList = (List<CourseEntity>) result.getData();
		mv.addObject("otherCourseList", otherCourseList);
		//判断是否需要检索测试信息
		if(CHAPTER_TYPE_MEASUERMENT.equals(chapterView.getMeasureType().toString())){
			result = measuermentService.searchMeasuerment(chapterId, LINK_TYPE_CHAPTER);
			List<QuestionEntity> questionList = (List<QuestionEntity>) result.getData();
			mv.addObject("questionList", questionList);
		}
		//判断是否考试章节
		if(CHAPTER_TYPE_EXAM.equals(chapterView.getMeasureType())){
			//考试章节
			//取用户对于章节的考试信息
			MeasurementEntity me = new MeasurementEntity();
			me.setLinkId(chapterId);
			me.setLinkType(LINK_TYPE_CHAPTER);
			me = measuermentService.findMeasurement(me);
			if(me!=null){
				UserMeasurementResultEntity um = new UserMeasurementResultEntity();
				um.setMeasurementId(me.getId());
				um.setUserId(WebContext.getSessionUserId());
				um = measuermentService.findUserExamResult(um);
				if(um != null && um.getExamStatus()!=null && um.getExamStatus().equals(EXAM_STATUS_END)){
					//已参加过考试提交了
					mv.addObject("goal", um.getGoal()+"");
					mv.addObject("totalGoal", um.getTotalGoal()+"");
					mv.addObject("examTime", DateUtil.formatAny(um.getCreateDate(), ConfigUtil.getDateTimeFormatDefualt()));
				}
			}
		}
		//是否教师
		mv.addObject("isTeacher", courseService.isTeacher(courseView.getTeacherId()));
		mv.addObject("useingChapterId", chapterId);
		String content = chapterView.getContent();
		if(content != null && !"".equals(content)) {
			String ipAndPort = getIpAndPort(content);
			if(!"".equals(ipAndPort)) {
				String serverName = WebContext.getRequest().getServerName();
				int serverPort = WebContext.getRequest().getServerPort();
				content = content.replace(ipAndPort, serverName+":"+serverPort);
				chapterView.setContent(content);
			}
		}
		
		return mv;
	}
	*//**
	 * 判断str中是否有IP和port，若有则取出返回
	 * @param str
	 * @return String
	 *//*
	private static String getIpAndPort(String str) {
		String returnStr = "";
		Pattern p = Pattern.compile("(\\d+\\.\\d+\\.\\d+\\.\\d+)\\:(\\d+)");
		Matcher m = p.matcher(str);
		if(m.find()) {
			returnStr += m.group(1)+":"+m.group(2);
		}
		
		return returnStr;
	}
	*//**
	 * 标签一览
	 * @param flag 是否显示标签 0 不显示 1 显示
	 * @param businessType 业务类型
	 * @return
	 *//*
	@RequestMapping("/searchSchhoolCategory.action")
	@ResponseBody
	public ResultBean searchSchhoolCategory(String flag,String businessType){
		ResultBean  result=tagService.searchSchhoolCategory(flag,businessType);
		return result;
	}
	*//**
	 * 
	 * 初始化待办课程列表
	 * 
	 * @param condition 课程对象
	 * @return 返回课程列表初始化页面
	 *//*
	@RequestMapping("/institutionDbListInit.action")
	public ModelAndView institutionDbListInit(InstitutionMessageEntity condition) {
		//设置代办页面
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_db_list");
		//设置分页参数
		condition.setPageSize(10);
		result.addObject("condition", condition);
		TagInstitutionView tagQuery = new TagInstitutionView();
		//一级标签信息查询
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		//二级标签信息查询
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		//返回课程实例对象
		result.addObject("course", new InstitutionMessageEntity());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}
	*//**
	 * 
	 * 初始化已办课程列表
	 * 
	 * @param condition 课程对象
	 * @return 返回课程列表初始化页面
	 *//*
	@RequestMapping("/institutionYbListInit.action")
	public ModelAndView institutionYbListInit(InstitutionMessageEntity condition) {
		//设置已办页面
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_yb_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		TagInstitutionView tagQuery = new TagInstitutionView();
		//一级标签信息查询
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		//二级标签信息查询
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		//返回课程实例对象
		result.addObject("course", new InstitutionMessageEntity());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}
	*//**
	 * 
	 * 检索评价列表
	 * 
	 * @param queryInfo 机构对象
	 * @return 返回评价列表
	 *//*
	@RequestMapping("/listdbSearch.action")
	@ResponseBody
	public ResultBean listdbSearch(MeasurementInstitutionEntity queryInfo) {
		UserEntity user = WebContext.getSessionUser();	
		//获取人员角色
		String role = user.getUserKbn();
		//获取登录人账号
		String userId = user.getCreateUser();
		//教师角色
		if("A".equals(role)) {
			TpUserEntity tpUserEntity =new TpUserEntity();
			//查询教师信息
			tpUserEntity = userManagementService.searchById(userId);
		    if(null!=tpUserEntity) {
		    	queryInfo.setInstitutionName(tpUserEntity.getSchoolName());
		    }
		    //学生角色
		}else if("C".equals(role)) {
			//查询学生信息
			StudentClassInfoEntity classInfo = new StudentClassInfoEntity();
			classInfo.setStudentId(userId);
			classInfo = studentClassInfoService.searchStudentClassInfo(classInfo);
			if(null!=classInfo && StringUtil.isNotBlank(classInfo.getSchoolName())) {
				queryInfo.setInstitutionName(classInfo.getSchoolName());
			}
		}
		
		return taxonomyInstitutionService.listdbSearch(queryInfo);
	}
	*//**
	 * 
	 * 检索已办评价列表
	 * 
	 * @param queryInfo 机构对象
	 * @return 返回评价列表
	 *//*
	@RequestMapping("/listybSearch.action")
	@ResponseBody
	public ResultBean listybSearch(MeasurementInstitutionEntity queryInfo) {
		UserEntity user = WebContext.getSessionUser();	
		//获取人员角色
		String role = user.getUserKbn();
		//登录账号
		String userId = user.getCreateUser();
		//教师角色
	    if("A".equals(role)) {
	    	//查询用户信息
	    	TpUserEntity tpUserEntity =new TpUserEntity();
			tpUserEntity = userManagementService.searchById(userId);
			if(null!=tpUserEntity) {
				//设置学校名称
				queryInfo.setInstitutionName(tpUserEntity.getSchoolName());
			}
	    }else if("C".equals(role)) {
	    	//查询学生信息
			StudentClassInfoEntity classInfo = new StudentClassInfoEntity();
			classInfo.setStudentId(userId);
			classInfo = studentClassInfoService.searchStudentClassInfo(classInfo);
			if(null!=classInfo && StringUtil.isNotBlank(classInfo.getSchoolName())) {
				//设置学生学校名称
				queryInfo.setInstitutionName(classInfo.getSchoolName());
			}
		}
	    //返回查询结果列表
		return taxonomyInstitutionService.listybSearch(queryInfo);
	}
	*//**
	 * 管理人员基本信息页面
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/managerUserList.action")
	public ModelAndView managerUserList(ManagerUserEntity condition) {
		//管理员信息页面展示路径
		ModelAndView result = new ModelAndView("/baseinstitution/manager/base_manager_user_list");
		//设置每页展示10条数据
		condition.setPageSize(10);
        result.addObject("course", new CourseManagerView());
		result.addObject("condition", condition);
		result = getRoleAndNum(result);
		//返回页面所需信息结果
		return result;
	}
	*//**
	 * 管理者信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchManagerUserList.action")
	@ResponseBody
	public ResultBean searchManagerUserList(ManagerUserEntity entity) {
		//查询管理员数据表全部信息
		return managerUserService.searchPage(entity);
	}
	*//**
	 * 搜素全部人员
	 * 
	 * @param query
	 * @return
	 *//*
	@RequestMapping("/searchUserAll.action")
	@ResponseBody
	public ResultBean searchUserAll(TpUserEntity query) {
		if(null!=query && StringUtil.isNotBlank(query.getUsrName())) {
			query.setUsrName(query.getUsrName().trim());
		}
		//根据用户名查询人员信息表数据
		return userManagementService.searchManagerUserAll(query);
	}
	*//**
	 * 保存超级管理员信息
	 * @param userId 添加人员账号id
	 * @param userName  人员名称
	 * @param personName 名称
	 * @param role 角色
	 * @return
	 *//*
	@RequestMapping("/saveManagerUser.action")
	@ResponseBody
	public ResultBean saveManagerUser(String userId,String userName,String personName,String role) {
		ResultBean result = new ResultBean();
		//查询管理员表中是否已存在该成员
		ManagerUserEntity entity = managerUserService.searchByUserId(userId);
		//存在则返回
		if(null!=entity) {
			result = ResultBean.error();
		}else {
			//不存在则保存到数据表里
			ManagerUserEntity managerUserEntity = new ManagerUserEntity();
			managerUserEntity.setUserId(userId);
			managerUserEntity.setUserName(userName);
			managerUserEntity.setPersonName(personName);
			managerUserEntity.setRole(role);
			managerUserEntity.setCreateDate(new Date());
			//设置该条信息的创建人
			managerUserEntity.setCreateUser(WebContext.getSessionUserId());
			managerUserService.insert(managerUserEntity);
			//保存成功返回信息
			result = ResultBean.success();
		}
		
		return result;
	}
	*//**
	 * 删除管理员
	 * @param id 员工账号
	 * @return  返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/deleteManagerById.action")
	@ResponseBody
	public ResultBean deleteManagerById(String id) throws Exception {
		//根据员工账号在tb_manager_user表里删除其信息
		ManagerUserEntity entity = new ManagerUserEntity();
		entity.setUserId(id);
		managerUserService.delete(entity);
		return ResultBean.success(DELETE_GRADE_SUCCESS);
	}
	*//**
	 * 查询管理信息
	 * 
	 * @param 员工账号
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchByUserId.action")
	@ResponseBody
	public ManagerUserEntity searchByUserId(String id) throws Exception {
		//返回查询结果
		return managerUserService.searchByUserId(id);
	}
	*//**
	 *  作业列表页面
	 * @param homeworkView 入参
	 * @return 返回结果
	 *//*
	@RequestMapping("/zuoyeManager.action")
	public ModelAndView zuoyeManager(HomeworkView homeworkView) {
		//列表页面路径
		ModelAndView result = new ModelAndView("/course/manager/homework_list");
		//返回作业信息对象给前台
		result.addObject("homeworkView", homeworkView);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 *  查询作业列表结果方法
	 * @param homeworkView 前台传入后台参数
	 * @return  返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/zuoyelistSearch.action")
	@ResponseBody
	public ResultBean zuoyelistSearch(HomeworkView homeworkView) throws Exception {
		ResultBean result =new ResultBean();
		//获取登录信息对象
		UserEntity user = WebContext.getSessionUser();
    	//判断是否管理员
    	ManagerUserEntity entity = managerUserService.searchByUserId(user.getUserId());
    	if(null!=entity) {
    		//管理员返回的结果
    		result= courseVRService.seacherHomeworkPage(homeworkView);
    	}else {
    		//普通教师查询返回的结果
    		homeworkView.setTeacherId(user.getUserId());
    		result= courseVRService.seacherHomeworkPage(homeworkView);
    	}
		return result;
	}
	*//**
	 *  根据作业id查询作业列表
	 * @param id 作业主键
	 * @param title  作业标题
	 * @return  返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/zuoyelistById.action")
	public ModelAndView zuoyelistById(String id,String title) throws Exception {
		//作业列表路径
		ModelAndView result = new ModelAndView("/course/manager/homework_user_list");
		HomeworkView homeworkView = new HomeworkView();
		homeworkView.setId(id);
		homeworkView.setTitle(title);
		//作业实例化对象
		result.addObject("homeworkView", homeworkView);
		//作业主键
		result.addObject("id", id);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 预览作业页面方法
	 * @param id  作业主键
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/homeWorkById.action")
	public ModelAndView homeWorkById(String id) throws Exception {
		//预览页面路径
		ModelAndView result = new ModelAndView("/course/client/home_work_teacher_review");
		HomeworkView view = courseService.getHomeWorkById(id);
		//设置创建时间返回前台
		if(null!=view.getCreateDate()) {
			view.setCreateTime(sdf.format(view.getCreateDate()));
		}
        //返回作业实体类对象
		result.addObject("homeWork", view);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 查询作业列表
	 * @param id 作业id集合字符串
	 * @param homeworkView  作业实体类
	 * @param linkId 课程主键
	 * @param linkType  课程/章节类型
	 * @return
	 *//*
	@RequestMapping("/zuoyelistByIdSearch.action")
	@ResponseBody
	public ResultBean zuoyelistByIdSearch(String id,HomeworkView homeworkView,String linkId,String linkType) {
		// 将前台汇总的作业id拆分
		if(StringUtil.isNotBlank(id)) {
			String ids[]=id.split(",");
			homeworkView.setId(ids[0]);
		}else {
			homeworkView.setLinkId(linkId);
			homeworkView.setLinkType(linkType);
			HomeworkEntity homeWork = courseService.getHomeWorkByLinkId(homeworkView);
			if(null!=homeWork) {
				//获取作业主键
				homeworkView.setId(homeWork.getId());
				//获取作业标题
				homeworkView.setTitle(homeWork.getTitle());
			}
		}
		//查询作业信息
		return courseVRService.seacherHomeworkUserPage(homeworkView);
	}
    *//**
                 * 保存作业信息
     * @param courseJson 作业信息
     * @return 返回结果
     * @throws Exception 
     *//*
    @RequestMapping("/saveZyInfo.action")
    @ResponseBody
    public ResultBean saveZyInfo(String courseJson) throws Exception {
    	//将字符串转化成实例化对象
    	HomeworkView course = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, HomeworkView.class);
        ResultBean result = ResultBean.success();
        String homeworkId=course.getId();
        //获取账号id
        String userId = WebContext.getSessionUserId();
        //查询作业数据表
        HomeworkAnswerEntity entity = new  HomeworkAnswerEntity();
        entity.setHomeworkId(homeworkId);
        entity.setCreateUser(userId);
        entity = courseVRService.selectOneAnswer(entity);
        if(null==entity) {
        	//作业作答数据表信息为空，插入数据信息
        	course.setId(IDGenerator.genUID());
        	course.setCreateUser(userId);
        	course.setCreateDate(new Date());
        	//设置学生提交的信息
        	course.setStudentComment(course.getContent());
        	course.setHomeworkId(homeworkId);
        	course.setAnswerUser(userId);
        	//设置作业状态
        	course.setStatus(HOMEWORK_ANSWER_STATUS_ONE);
        	courseVRService.insertAnswer(course);
        }else {
        	//作业作答数据表信息不为空，更新数据信息
        	entity.setStudentComment(course.getContent());
        	entity.setUpdateDate(new Date());
        	entity.setUpdateUser(userId);
        	courseVRService.update(entity);
        }
        return result;
    }
	*//**
	 * 学生作业列表页面
	 * @param HomeworkView homeworkView 
	 * @return
	 *//*
	@RequestMapping("/studentZuoyePage.action")
	public ModelAndView studentZuoyePage(HomeworkView homeworkView) {
		//学生作业列表页面路径
		ModelAndView result = new ModelAndView("/course/manager/homework_student_list");
		//作业实例对象实体类
		result.addObject("homeworkView", homeworkView);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 查询作业列表
	 * 
	 * @param HomeworkView homeworkView 入参
	 * @return 返回结果
	 *//*
	@RequestMapping("/studentZuoyelistSearch.action")
	@ResponseBody
	public ResultBean studentZuoyelistSearch(HomeworkView homeworkView) {
		homeworkView.setCreateUser(WebContext.getSessionUserId());
		//根据账号id获取作业对象信息
		return courseVRService.seacherStudentHomeworkUserPage(homeworkView);
	}
    *//**
                   * 编辑作业
     * @param HomeworkView home 作业实例对象入参 
     * @return
     *//*
    @RequestMapping("/studentTaskInit.action")
    public ModelAndView studentTaskInit(HomeworkView home){
    	//作业编辑页面
        ModelAndView result = new ModelAndView("course/manager/home_student_work");
        //获取当前登录者账号
        UserEntity user = WebContext.getSessionUser();
        home.setCreateUser(user.getUserId());
        HomeworkView homework=courseVRService.searchStudentHomeWork(home);
        //前台页面信息回显对象
        result.addObject("homework", homework);
        result = getRoleAndNum(result);
        return result;
    }
    *//**
                  * 保存作业信息
     * @param id  作业id
     * @param studentComment 学生提交内容
     * @param homeanwserId 作业作答id
     * @param status 提交作业状态
     * @return
     * @throws Exception
     *//*
    @RequestMapping("/saveMystudy.action")
    @ResponseBody
    public ResultBean saveMystudy(String id,String studentComment,String homeanwserId,String status) throws Exception {
        ResultBean result = ResultBean.success();
        String userId = WebContext.getSessionUserId();
        //根据homeanwserId判断当前登陆者是否已经作答过改作业
        if(StringUtil.isBlank(homeanwserId)) {
        	//未作答，保存作答内容
        	HomeworkView course = new HomeworkView();
        	//随机生成作业作答id
        	course.setId(IDGenerator.genUID());
        	course.setCreateUser(userId);
        	course.setCreateDate(new Date());
        	//作业作答内容
        	course.setStudentComment(studentComment);
        	course.setHomeworkId(id);
        	course.setAnswerUser(userId);
        	//设置作业状态
        	course.setStatus(HOMEWORK_ANSWER_STATUS_ONE);
        	courseVRService.insertAnswer(course);
        }else {
        	//更新做大内容
        	HomeworkAnswerEntity entity = new HomeworkAnswerEntity();
        	entity.setId(homeanwserId);
        	entity = courseVRService.selectOneAnswer(entity);
        	entity.setStudentComment(studentComment);
        	entity.setUpdateDate(new Date());
        	entity.setUpdateUser(userId);
        	courseVRService.update(entity);
        }
        return result;
    }
    *//**
                 * 学生查看作业列表方法
     * @param home
     * @return
     *//*
    @RequestMapping("/studentSeacherTaskInit.action")
    public ModelAndView studentSeacherTaskInit(HomeworkView home){
    	//学生作业列表页面路径
        ModelAndView result = new ModelAndView("course/manager/home_student_look_work");
        //根据登录id查询作业信息
        UserEntity user = WebContext.getSessionUser();
        home.setCreateUser(user.getUserId());
        HomeworkView homework=courseVRService.searchStudentHomeWork(home);
        //作业信息返回给前台
        result.addObject("homework", homework);
        result = getRoleAndNum(result);
        return result;
    }
    *//**
                  * 查询年级 课程
     * @param id 课程id
     * @return
     *//*
    @RequestMapping("/searchGrade.action")
    @ResponseBody
    public List<StudentClassInfoEntity> searchGrade(String id){
    	//根据课程id查询课程信息
        CourseEntity entity = new CourseEntity();
        entity.setId(id);
        entity = courseService.searchCourse(entity);
        //获取老师所属学校
        String schoolId = entity.getSchoolId();
        StudentClassInfoEntity studentClassInfoEntity = new StudentClassInfoEntity();
        studentClassInfoEntity.setSchoolId(schoolId);
        //根据学校查询年级
        List<StudentClassInfoEntity> gradeList = new ArrayList<StudentClassInfoEntity>();
        gradeList = studentClassInfoService.searchStudentGradeList(studentClassInfoEntity);
        //返回查询的年级集合给前台
        return gradeList;
    }
     *//**
                     * 根据课程主键和年级主键查询班级信息
      * @param courseId  课程主键
      * @param gradeId 年级主键
      * @return
      *//*
    @RequestMapping("/searchClass.action")
    @ResponseBody
    public List<StudentClassInfoEntity> searchClass(String courseId,String gradeId){
    	//根据课程主键查询课程信息
    	 CourseEntity entity = new CourseEntity();
         entity.setId(courseId);
         entity = courseService.searchCourse(entity);
         //获取老师所属学校
         String schoolId = entity.getSchoolId();
        List<StudentClassInfoEntity> gradeList = new ArrayList<StudentClassInfoEntity>();
        //根据学校主键和年级查询班级信息
        StudentClassInfoEntity studentClassInfoEntity = new StudentClassInfoEntity();
        studentClassInfoEntity.setSchoolId(schoolId);//学校id
        studentClassInfoEntity.setGradeId(gradeId);//年级id
        gradeList = studentClassInfoService.searchStudentClassList(studentClassInfoEntity);
        return gradeList;
    }
    *//**
                  * 添加学生时查询学生列表
     * @param UserView query 用户参数 
     * @param courseId 所属课程信息id
     * @return
     *//*
    @RequestMapping("/searchStudentUserAll.action")
    @ResponseBody
    public ResultBean searchStudentUserAll(UserView query,String courseId)  {
    	//根据课程id查询课程所属老师对应的学校信息
    	CourseEntity entity = new CourseEntity();
        entity.setId(courseId);
        entity = courseService.searchCourse(entity);
        //获取老师所属学校
        String schoolId = entity.getSchoolId();
        //根据学校id获取这个学校下面所有的学生信息
        query.setSchoolId(schoolId);
        return courseService.searchStudentUserAll(query);
    }
	*//**
	 * 用户个人中心信息页面方法
	 * @return 返回结果
	 *//*
	@RequestMapping("/userPblInfo.action")
	public ModelAndView userPblInfo(){
		//查询代办信息表里审核通过的代办信息数量
		int num = courseService.getStuStatus();
		//个人中心页面路径参数
		ModelAndView mv = new ModelAndView("trainingplatform/client/center/center_pbl_user");
		//根据用户id查询用户信息
		UserEntity user = userManagementService.seachUserInfo(WebContext.getSessionUserId());
		//返回用户对象
		mv.addObject("user", user);
		mv.addObject("num", num);
		//获取登录人员的角色信息
		UserEntity userRole = WebContext.getSessionUser();
		//判断用户是否登录状态guest为未登录状态
		if(!userRole.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(userRole.getUserId());
			if(roleList.size() > 0) {
				//循环遍历人员角色
				for(UserRoleEntity role : roleList) {
					//角色包含01的设置role字段为t,即老师角色
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				//s为学生角色
				mv.addObject("role", "s");
			}
		}
		UserEntity user1 = WebContext.getSessionUser();
		//获取工号
		String uid =user1.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		    //获取人员信息
		        //设置头像字段
				String image = "";
				//设置头像字段
				String school="";
				//设置年级字段
				String grade="";
				//设置班级字段
				String className="";
				if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
					//获取头像
					image = userEntity.getHeadurl();
					//获取学校
					school = userEntity.getSchoolName();
				}else {
					//设置默认头像
					image ="http://wisdomsaas-img.oss-cn-beijing.aliyuncs.com/BFAA0FECDD194B7A974C60B7E1B27C18?authorization=bce-auth-v1%2F7642b4b48046452697e90662eed41ace%2F2017-12-29T08%3A33%3A39Z%2F-1%2F%2F0f06024af14dad05694f25b8c85572567591c72bbd0129d3175b4cdbd6e62208";
				}
				//获取班级
				if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
					//实例化学生对象实体类并查询学生信息
					List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
					StudentClassInfoEntity entity = new StudentClassInfoEntity();
					entity.setStudentId(uid);
					entityList = studentClassInfoService.searchStudentClassInfoList(entity);
					//如果学生对象不为空则执行如下赋值
					if(null!=entityList && entityList.size()>0) {
						grade = entityList.get(0).getGradeName();
						className = entityList.get(0).getClassName();
						school =  entityList.get(0).getSchoolName();
					}
				}
				//返回班级信息
				mv.addObject("className", className);
				//向页面返回年级信息
				mv.addObject("grade", grade);
				//返回学生所属学校名称信息
				mv.addObject("school", school);
		       return mv;
	}
	*//**
	 * 批量点评初始化页面方法
	 * @param userIds  学生id集合字符串
	 * @param personNames  学生姓名字符集合
	 * @param id      作业id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/allEvalutionMsg.action")
	public ModelAndView allEvalutionMsg(String userIds,String personNames,String id) throws Exception {
		//批量点评方法
		ModelAndView result = new ModelAndView("/course/client/home_work_teacher_all_review");
		String ids[]=id.split(",");
		HomeworkView view = courseService.getHomeWorkById(ids[0]);
		//判断作业对象返回的时间是否为空，不为空则赋值给新的字段用于前端展示
		if(null!=view.getCreateDate()) {
			if(StringUtil.isNotBlank(sdf.format(view.getCreateDate()))) {
				view.setCreateTime(sdf.format(view.getCreateDate()));
			}
		}
		//收集作业对象
		result.addObject("homeWork", view);
		//收集作业id
		result.addObject("id", id);
		//收集学生账号集合id
		result.addObject("userIds", userIds);
		//收集学生姓名集合
		result.addObject("personNames", personNames);
		//收集作业答案id
		result.addObject("answerIds", id);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 批量点评保存
	 * @param courseJson 前台返回的json字符串
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveAllHomeWorkReviewById.action")
	@ResponseBody
	public ResultBean saveAllHomeWorkReviewById(String courseJson) throws Exception {
		//将字符串转化成实例化对象
		HomeworkView homework = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, HomeworkView.class);
		return courseService.saveAllHomeWorkReviewById(homework);
	}
	*//**
	 * 一键催交
	 * @param userIds  学生id集合字符串参数
	 * @param courseId 课程id
	 * @param homeworkId  作业id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveCjById.action")
	@ResponseBody
	public ResultBean saveCjById(String userIds,String courseId,String homeworkId) throws Exception {
		return courseService.saveACjById(userIds,courseId,homeworkId);
	}
	*//**
	 * 课程考试调用java调查网接口
	 * @param courseId  课程id
	 * @param userId  用户id
	 * @return
	 * @throws ParseException
	 *//*
	@RequestMapping("/searchExamByCourseIdList.action")
	@ResponseBody
	public ResultBean searchExamByCourseIdList(String courseId,String userId) throws ParseException {
		ResultBean result = new ResultBean();
		String param = courseId;
		//声明调用java调查网的webservice接口
		ExamToWebserviceProxy proxy = new ExamToWebserviceProxy();
		String out;
		try {
			//调用外部接口方法
			out = proxy.examList(param,1,20,userId);
			JSONArray jsonArray = new JSONArray(); 
			//将接口返回值转化成数组格式
			jsonArray = jsonArray.fromObject(out);
			UserCourseEntity uc = new UserCourseEntity();
			uc.setCourseId(courseId);
			uc.setUserId(WebContext.getSessionUserId());
			// 检索用户是否存在
			UserCourseEntity uu = courseService.searchOneCourseIdData(uc);
			//根据课程id获取该课程的老师id
			CourseEntity courseEntity = courseService.getCourseId(courseId);
			//转化对应的实体类
			@SuppressWarnings("unchecked")
			List<Questionnaire> list = jsonArray.toList(jsonArray, Questionnaire.class);
			if(null!=list && list.size()>0) {
				//循环遍历集合将字段赋值
				for(int i=0;i<list.size();i++) {
					//已加入的学员或者这个课程的老师创建者都可以看到这个
					if(null!=uu ||(null!=courseEntity && WebContext.getSessionUserId().equals(courseEntity.getTeacherId()))) {
						//已加入课程
						list.get(i).setStudentFlag("0");
						if (list.get(i).getEndDate() != null ) {
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Long end = sdf.parse(list.get(i).getEndDateStr()).getTime();
							Long nowtime = new Date().getTime();
							//判断问卷/考试是否已结束
								if(end-nowtime<0) {
									list.get(i).setExamStatusText("问卷/考试已结束");
									list.get(i).setExamStatus("1");	
								}					
							}
						if (list.get(i).getStartDate() != null ) {
							//将字符串转化成时间再获取秒数
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Long start = sdf.parse(list.get(i).getStartDateStr()).getTime();
							Long nowtime = new Date().getTime();
							//判断问卷/考试是否开始
								if(start-nowtime>0) {
									list.get(i).setExamStatusText("问卷/考试未开始");
									list.get(i).setExamStatus("1");
								}
							}
					}else {
						list.get(i).setStudentFlag("1");//未加入课程
						list.get(i).setExamStatusText("未加入课程");
					}
					
				}
			}
			 result.setData(list);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
		@Autowired
	private CourseService courseService;
	@Autowired
	private ChapterService chapterService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserManagementService userManagementService;
	@Autowired
	private TaxonomyInstitutionService taxonomyInstitutionService;
	@Autowired
	private TagInstitutionEvalutionService tagInstitutionEvalutionService;
	@Autowired
	private InstitutionService institutionService;
	@Autowired
	private TagInstitutionService tagInstitutionService;
	@Autowired
	private MeasuermentInstitutionService measurementInstitutionService;
	@Autowired
	private QuestionInstitutionService questionInstitutionService;
	@Autowired
	private MeasuermentService measuermentService;
	@Autowired
	private StudentClassInfoService studentClassInfoService;
	@Autowired
	private TeacherClassInfoService teacherClassInfoService;
	@Autowired
	private ChapterActiveService chapterActiveService;
	@Autowired
	private TagService tagService;
	@Autowired
	private ManagerUserService managerUserService;
	@Autowired
	private CourseVRService courseVRService;
	*//**
	 * 用户收藏课程
	 * 
	 * @author liuzhen
	 * @param courseId
	 * @return 返回结果
	 *//*
	@RequestMapping("/collectCourse.action")
	@ResponseBody
	public ResultBean collectCourse(String courseId) {
		return courseService.collectCourse(courseId);
	}

	*//**
	 * 用户取消收藏
	 * 
	 * @param courseId 课程Id
	 * @return
	 *//*
	@RequestMapping("/cancelCollectCourse.action")
	@ResponseBody
	public ResultBean cancelCollectCourse(String courseId) {
		return courseService.cancelCollectCourse(courseId);
	}

	*//**
	 * 二级标签查询
	 * 
	 * @param id 课程ID
	 * @return 课程下的二级标签列表
	 *//*
	@RequestMapping("/searchSecondTag2.action")
	@ResponseBody
	public List<TagView> searchSecondTag2(String id) {
		return courseService.searchSecondTagList2(id);
	}
	*//**
	 * 学校评价二级标签查询
	 * 
	 * @param id 课程ID
	 * @return 课程下的二级标签列表
	 *//*
	@RequestMapping("/searchInsitutionSecondTag2.action")
	@ResponseBody
	public List<TagInstitutionView> searchInsitutionSecondTag2(String id) {
		return courseService.searchInstitutionSecondTagList2(id);
	}
	@RequestMapping("/searchSecondTag.action")
	@ResponseBody
	public List<TagView> searchSecondTag(String id) {
		return courseService.searchSecondTagList(id);
	}

	*//**
	 * 体验课程/加入课程操作
	 * 
	 * @param courseId 课程ID
	 * @return 是否加入课程成功
	 * @author WUXIAOXIANG
	 *//*
	@RequestMapping("/joinCourse.action")
	@ResponseBody
	public ResultBean joinCourse(String courseId) {
		return courseService.joinCourse(courseId);
	}

	@RequestMapping("/applyJoinCourse.action")
	@ResponseBody
	public ResultBean applyJoinCourse(String courseId) {
		return courseService.applyJoinCourse(courseId);
	}
	
	*//**
	 * 课程推荐
	 * 
	 * @author FZ
	 * @return 返回结果
	 *//*
	@RequestMapping("/recommendedCourses.action")
	@ResponseBody
	public ResultBean recommendedCourses(String courseId, String displayNum) {

		return courseService.recommendedCourses(displayNum, courseId);
	}

	*//**
	 * 课程评价
	 * 
	 * @param linkId linkType
	 * @return 返回结果
	 * @author dengzhihao
	 *//*
	@RequestMapping("/courseEvaluationList.action")
	@ResponseBody
	public ResultBean courseEvaluationList(ScoreEntity scoreEntity) {

		return courseService.courseEvaluationList(scoreEntity);
	}

	*//**
	 * 对课程评价
	 * 
	 * @param scoreEntity     评价主题entity
	 * @param scoreDataEntity 评价数据entity
	 * @return 是否评价成功
	 * @author WUXIAOXIANG
	 *//*
	@RequestMapping("/evaluateCourse.action")
	@ResponseBody
	public ResultBean evaluateCourse(ScoreEntity scoreEntity, ScoreDataEntity scoreDataEntity) {
		// 插入评价信息
		return courseService.insertEvaluateCourse(scoreEntity, scoreDataEntity);
	}

	*//**
	 * 检索课程 包括分类
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 *//*
	@RequestMapping("/courseList.action")
	@ResponseBody
	public ResultBean courseList(CourseManagerView course, String directionTagId, String categoryTagId,
			String courseStage, String sortType, Integer pageSize, Integer pageNumber,String schoolName,String schoolNatrul,String tagsId) {
		if(StringUtil.isNotBlank(tagsId)) {
			course.setTagsId(tagsId);
		}
		if(StringUtil.isNotBlank(schoolNatrul)) {
			course.setSchoolNatrul(schoolNatrul);
		}
		ResultBean result = courseService.courseList(course, directionTagId, categoryTagId, courseStage, sortType,
				pageSize, pageNumber,schoolName);
		return result;
	}

	@RequestMapping("/coursePublicList.action")
	@ResponseBody
	public ResultBean coursePublicList(CourseManagerView course, Integer pageSize, Integer pageNumber) {
		course.setShareFlag(COURSE_SHARE_FLAG_THREE);
		course.setEndFlag(COURSE_END_STATUS_YES);
		UserEntity user = WebContext.getSessionUser();
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		//获取学校名称信息
		String school="";
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
			//获取学校
			school = userEntity.getSchoolName();
		}
		//获取班级
		if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
			List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
			StudentClassInfoEntity entity = new StudentClassInfoEntity();
			entity.setStudentId(uid);
			entityList = studentClassInfoService.searchStudentClassInfoList(entity);
			if(null!=entityList && entityList.size()>0) {
				school =  entityList.get(0).getSchoolName();
			}
		}
		course.setSchoolName(school);
//		if(StringUtil.isNotBlank(course.getDirectionTagId()) && StringUtil.isBlank(course.getCategoryTagId())) {
//			course.setTagId(course.getDirectionTagId());
//		}else {
//			course.setTagId(course.getCategoryTagId());
//			course.setParentTagId(course.getDirectionTagId());
//		}
		
		ResultBean result = courseService.coursePublicList(course, pageSize, pageNumber);
		return result;
	}

	@RequestMapping("/courseFactoryList.action")
	@ResponseBody
	public ResultBean courseFactoryList(CourseManagerView course, Integer pageSize, Integer pageNumber) {
		course.setShareFlag(COURSE_SHARE_FLAG_TWO);
		//获取学校名字
		//获取工号
		UserEntity user = WebContext.getSessionUser();
				String uid =user.getUserId();
				TpUserEntity userEntity = courseService.searchInfoMessage(uid);
				//获取学校名称信息
				String school="";
				if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
					//获取学校
					school = userEntity.getSchoolName();
				}
				//获取班级
				if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
					List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
					StudentClassInfoEntity entity = new StudentClassInfoEntity();
					entity.setStudentId(uid);
					entityList = studentClassInfoService.searchStudentClassInfoList(entity);
					if(null!=entityList && entityList.size()>0) {
						school =  entityList.get(0).getSchoolName();
					}
				}
//				course.setSchoolName(school);
//		if(StringUtil.isNotBlank(course.getDirectionTagId()) && StringUtil.isBlank(course.getCategoryTagId())) {
//			course.setTagId(course.getDirectionTagId());
//		}else {
//			course.setTagId(course.getCategoryTagId());
//			course.setParentTagId(course.getDirectionTagId());
//		}
		
		
		ResultBean result = courseService.coursePublicList(course, pageSize, pageNumber);
		return result;
	}
	
	@RequestMapping("/courseQualityList.action")
	@ResponseBody
	public ResultBean courseQualityList(CourseManagerView course, Integer pageSize, Integer pageNumber) {
		course.setShareFlag(COURSE_SHARE_FLAG_ONE);
		//获取工号
		UserEntity user = WebContext.getSessionUser();
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		//获取学校名称信息
		String school="";
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
			//获取学校
			school = userEntity.getSchoolName();
		}
		//获取班级
		if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
			List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
			StudentClassInfoEntity entity = new StudentClassInfoEntity();
			entity.setStudentId(uid);
			entityList = studentClassInfoService.searchStudentClassInfoList(entity);
			if(null!=entityList && entityList.size()>0) {
				school =  entityList.get(0).getSchoolName();
			}
		}
		course.setSchoolName(school);
//		if(StringUtil.isNotBlank(course.getDirectionTagId()) && StringUtil.isBlank(course.getCategoryTagId())) {
//			course.setTagId(course.getDirectionTagId());
//		}else {
//			course.setTagId(course.getCategoryTagId());
//			course.setParentTagId(course.getDirectionTagId());
//		}
		ResultBean result = courseService.coursePublicList(course, pageSize, pageNumber);
		return result;
	}
	
	*//**
	 * 检索教师信息
	 * 
	 * @author LILIANG
	 * @return UserEntity
	 *//*
	@RequestMapping("/teacherInfo.action")
	@ResponseBody
	public ResultBean teacherInfo(UserEntity user) {
		ResultBean result = courseService.teacherInfo(user);
		return result;
	}

	*//**
	 * 检索与教师相关的课程信息
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 *//*
	@RequestMapping("/teacherCourseList.action")
	@ResponseBody
	public ResultBean teacherCourseList(CourseEntity c) {
		ResultBean result = courseService.teacherCourseList(c);
		return result;
	}

	*//**
	 * 课程浏览数+1
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 *//*
	@RequestMapping("/addCourseViewNum.action")
	@ResponseBody
	public ResultBean addCourseViewNum(CourseEntity c) {
		ResultBean result = courseService.addCourseViewNum(c);
		return result;
	}

	*//**
	 * 根据课程ID检索课程详情
	 * 
	 * @param courseId 课程ID
	 * @return ResultBean data:CourseView 课程基本信息、视频数、练习数、标签、用户课程等信息
	 *//*
	@RequestMapping("/courseDetail.action")
	@ResponseBody
	public ResultBean courseDetail(String courseId) {
		return courseService.searchCourseDetail(courseId);
	}

	*//**
	 * 课程详情-立即支付详情
	 * 
	 * @param courseId 课程Id
	 * @return
	 *//*
	@RequestMapping("/immediatePayDetail.action")
	@ResponseBody
	public ResultBean immediatePayDetail(String courseId) {
		return courseService.immediatePayDetail(courseId);
	}

	*//**
	 * 课程列表页面
	 *//*
	@RequestMapping("/courseListInit.action")
	public ModelAndView courseListInit(String courseId,String schoolName) {
		ModelAndView result = new ModelAndView("/course/client/course_list");
		result.addObject("schoolName", schoolName);
		result.addObject("courseId", courseId);
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/pubCourseListInit.action")
	public ModelAndView pubCourseListInit() {
		ModelAndView result = new ModelAndView("/course/client/course_list_public");
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/qualityCourseListInit.action")
	public ModelAndView qualityCourseListInit() {
		ModelAndView result = new ModelAndView("/course/client/course_list_quality");
		result = getRoleAndNum(result);
		return result;
	}
	
	@RequestMapping("/courseFactoryListInit.action")
	public ModelAndView courseFactoryListInit() {
		ModelAndView result = new ModelAndView("/course/client/course_list_factory");
		result = getRoleAndNum(result);
		return result;
	}
	
	@RequestMapping("/studentHomePage.action")
	public ModelAndView studentHomePage(String userId) {
		ModelAndView result = new ModelAndView("/course/client/studentHomePage");
		UserEntity loginUser = userManagementService.seachUserInfo(userId);
		// List<CourseEntity> courseList = courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result = getRoleAndNum(result);
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/studentHomePageFromList.action")
	public ModelAndView studentHomePageFromList(String userId) {
		ModelAndView result = new ModelAndView("/course/client/studentHomePageFromList");
		UserEntity loginUser = userManagementService.seachUserInfo(userId);
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result = getRoleAndNum(result);
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/teacherHomePage.action")
	public ModelAndView teacherHomePage(String userId) {
		ModelAndView result = new ModelAndView("/course/client/teacherHomePage");
		result = getRoleAndNum(result);
		UserEntity loginUser = userManagementService.seachUserInfo(userId);
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/teacherHomePageFromList.action")
	public ModelAndView teacherHomePageFromList(String userId) {
		ModelAndView result = new ModelAndView("/course/client/teacherHomePageFromList");
		result = getRoleAndNum(result);
		UserEntity loginUser = userManagementService.seachUserInfo(userId);
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/saveInfo.action")
	@ResponseBody
	public ResultBean saveInfo(String studentJson) throws Exception {
		ArchivesEntity archives = JSONUtil.parse(
				JSONUtil.createObMapperInstance(ConfigUtil.getDateMinuteFormatDefualt()), studentJson,
				ArchivesEntity.class);
		courseService.saveArchives(archives);
		ResultBean result = ResultBean.success();
		return result;
	}

	@RequestMapping("/studentList.action")
	public ModelAndView studentList() {
		ModelAndView result = new ModelAndView("/course/client/student_list");
		result = getRoleAndNum(result);
		UserEntity loginUser = userManagementService.seachUserInfo(WebContext.getSessionUserId());
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/teacherList.action")
	public ModelAndView teacherList() {
		ModelAndView result = new ModelAndView("/course/client/teacher_list");
		result = getRoleAndNum(result);
		UserEntity loginUser = userManagementService.seachUserInfo(WebContext.getSessionUserId());
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/archivesList.action")
	@ResponseBody
	public ResultBean archivesList(String userId) {
		ResultBean result = new ResultBean();
		TpUserEntity tpUser = courseService.getUserByUserId(userId);
		List<ArchivesEntity> archivesList = courseService.getArchivesList(userId);
		List<CourseEntity> courseList = courseService.getCourseListByUserId(userId);
		if (courseList.size() > 0) {
			for (CourseEntity entity : courseList) {
				ArchivesEntity archives = new ArchivesEntity();
				archives.setUserId(userId);
				archives.setUserName(tpUser.getPersonName());
				archives.setCreateTime(sdf.format(entity.getCreateDate()));
				archives.setCreateDate(entity.getCreateDate());
				archives.setInfo(sdf.format(entity.getCreateDate()) + ",创建课程，" + entity.getTitle());
				if (entity.getPictureId() != null) {
					archives.setExitImage("1");
					archives.setImageId(entity.getPictureId());
				}
				// archivesList.add(archives);
			}
			listSort(archivesList);
		}
		List<UserCourseEntity> ucList = courseService.getUCListByUserId(userId);
		if (ucList.size() > 0) {
			for (UserCourseEntity uc : ucList) {
				ArchivesEntity archives = new ArchivesEntity();
				archives.setUserId(userId);
				archives.setUserName(tpUser.getPersonName());
				archives.setCreateTime(sdf.format(uc.getCreateDate()));
				archives.setCreateDate(uc.getCreateDate());
				archives.setInfo(sdf.format(uc.getCreateDate()) + ",加入课程，" + uc.getCourseName());
				if (uc.getPictureId() != null) {
					archives.setExitImage("1");
					archives.setImageId(uc.getPictureId());
				}
				// archivesList.add(archives);
			}
			listSort(archivesList);
		}
		result.setData(archivesList);
		return result;
	}

	@RequestMapping("/saveDiscussData.action")
	@ResponseBody
	public ResultBean saveDiscussData(ArchivesDiscussEntity data) throws Exception {
		return courseService.saveDiscuss(data);
	}

	@RequestMapping("/searchUsersList.action")
	@ResponseBody
	public ResultBean searchUsersList(TpUserEntity user) {
		String userKbn = WebContext.getSessionUser().getUserKbn();
		if (userKbn != null && userKbn.contentEquals(TpStateConstants.USER_KBN_C)) {
			user.setUserKbn(TpStateConstants.USER_KBN_C);
		}
		user.setSortName("createDate");
		user.setSortOrder("DESC");
		return userManagementService.search4Page(user);
	}

	@SuppressWarnings("unused")
	private void listSort(List<ArchivesEntity> list) {
		Collections.sort(list, new Comparator<ArchivesEntity>() {
			@Override
			public int compare(ArchivesEntity o1, ArchivesEntity o2) {
				try {
					Date dt1 = o1.getCreateDate();
					Date dt2 = o2.getCreateDate();
					if (dt1.getTime() > dt2.getTime()) {
						return -1;
					} else if (dt1.getTime() < dt2.getTime()) {
						return 1;
					} else {
						return 0;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return 0;
			}
		});
	}

	*//**
	 * 系统首页
	 *//*
	@RequestMapping("/homePage.action")
	public ModelAndView homePage() {
		ModelAndView result = new ModelAndView("/course/client/homePage");
		UserEntity user = WebContext.getSessionUser();
		//获取工号
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getIsFlag())) {
			result.addObject("isFlag","1");
		}else {
			//不是管理员
			result.addObject("isFlag","0");
		}
		
		//获取人员信息
		String image = "";
		String school="";
		String grade="";
		String className="";
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
			//获取头像
			image = userEntity.getHeadurl();
			//获取学校
			school = userEntity.getSchoolName();
		}else {
			//设置默认头像
			image ="http://wisdomsaas-img.oss-cn-beijing.aliyuncs.com/BFAA0FECDD194B7A974C60B7E1B27C18?authorization=bce-auth-v1%2F7642b4b48046452697e90662eed41ace%2F2017-12-29T08%3A33%3A39Z%2F-1%2F%2F0f06024af14dad05694f25b8c85572567591c72bbd0129d3175b4cdbd6e62208";
		}
		//获取班级
		if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
			List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
			StudentClassInfoEntity entity = new StudentClassInfoEntity();
			entity.setStudentId(uid);
			entityList = studentClassInfoService.searchStudentClassInfoList(entity);
			if(null!=entityList && entityList.size()>0) {
				grade = entityList.get(0).getGradeName();
				className = entityList.get(0).getClassName();
				school =  entityList.get(0).getSchoolName();
			}
		}
		result.addObject("className", className);
		result.addObject("grade", grade);
		result.addObject("school", school);
		result.addObject("image", image);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 问卷考试系统首页
	 *//*
	@RequestMapping("/homeJavaPage.action")
	public ModelAndView homeJavaPage(String courseId) {
		ModelAndView result = new ModelAndView("/course/client/homeJavaPage");
		UserEntity user = WebContext.getSessionUser();
		//获取工号
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getIsFlag())) {
			result.addObject("isFlag","1");
		}else {
			//不是管理员
			result.addObject("isFlag","0");
		}
		
		//获取人员信息
		String image = "";
		String school="";
		String grade="";
		String className="";
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
			//获取头像
			image = userEntity.getHeadurl();
			//获取学校
			school = userEntity.getSchoolName();
		}else {
			//设置默认头像
			image ="http://wisdomsaas-img.oss-cn-beijing.aliyuncs.com/BFAA0FECDD194B7A974C60B7E1B27C18?authorization=bce-auth-v1%2F7642b4b48046452697e90662eed41ace%2F2017-12-29T08%3A33%3A39Z%2F-1%2F%2F0f06024af14dad05694f25b8c85572567591c72bbd0129d3175b4cdbd6e62208";
		}
		//获取班级
		if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
			List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
			StudentClassInfoEntity entity = new StudentClassInfoEntity();
			entity.setStudentId(uid);
			entityList = studentClassInfoService.searchStudentClassInfoList(entity);
			if(null!=entityList && entityList.size()>0) {
				grade = entityList.get(0).getGradeName();
				className = entityList.get(0).getClassName();
				school =  entityList.get(0).getSchoolName();
			}
		}
		result.addObject("className", className);
		result.addObject("grade", grade);
		result.addObject("school", school);
		result.addObject("courseId", courseId);
		result.addObject("image", image);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 题库管理
	 *//*
	@RequestMapping("/homeExamPage.action")
	public ModelAndView homeExamPage() {
		ModelAndView result = new ModelAndView("/course/manager/homeExamPage");
		UserEntity user = WebContext.getSessionUser();
		//获取工号
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getIsFlag())) {
			result.addObject("isFlag","1");
		}else {
			//不是管理员
			result.addObject("isFlag","0");
		}
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 题库管理
	 *//*
	@RequestMapping("/homeJsdPage.action")
	public ModelAndView homeJsdPage() {
		ModelAndView result = new ModelAndView("/course/client/homeJsdPage");
		UserEntity user = WebContext.getSessionUser();
		//获取工号
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getIsFlag())) {
			result.addObject("isFlag","1");
		}else {
			//不是管理员
			result.addObject("isFlag","0");
		}
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 获取登录信息
	 * @param code
	 * @param type
	 * @return
	 * @throws UnsupportedEncodingException
	 *//*
	@SuppressWarnings("null")
	@RequestMapping("/getUserInfo.action")
	@ResponseBody
	public JSONArray getUserInfo(String code, String type) throws UnsupportedEncodingException {
		//设置json集合对象和数组
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		if (code != null && !"".equals(code)) {
			//UserEntity user = WebContext.getSessionUser();
			String codeSession = (String) WebContext.getSession().getAttribute("code");
			if (codeSession == null || !code.equals(codeSession)) {
				Long str12 = System.currentTimeMillis();
				String access_tokenStr = "";
				//定义获取应用token的变量
				//String application_tokenStr="";
				if (type.equals("1")) {
					//获取access_tokenStr字符串对象
					access_tokenStr = HttpUtils.sendFormEvalutionPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token",
							"code=" + code
									+ "&grant_type=authorization_code&client_id=1EF8798194B36D72&client_secret=440a4fad895036188ea200a0da1d6156&redirect_uri="
									+ PropertyUtil.getProperty("remote.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "1EF8798194B36D72", "440a4fad895036188ea200a0da1d6156");
				} else if (type.equals("2")) {
					//获取access_tokenStr字符串对象
					access_tokenStr = HttpUtils.sendFormEvalutionPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token",
							"code=" + code
									+ "&grant_type=authorization_code&client_id=BB2650288E091B1C&client_secret=26807915a5269ba07a40d97236b5ec64&redirect_uri="
									+ PropertyUtil.getProperty("teacher.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "BB2650288E091B1C", "26807915a5269ba07a40d97236b5ec64");
				} else if (type.equals("3")) {
					//获取access_tokenStr字符串对象
					access_tokenStr = HttpUtils.sendPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token",
							"code=" + code
									+ "&grant_type=authorization_code&client_id=42F9292D78B1386F&client_secret=e1e034ac44a63ba61e8e9595689efd4c&redirect_uri="
									+ PropertyUtil.getProperty("student.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "42F9292D78B1386F", "e1e034ac44a63ba61e8e9595689efd4c");
				} else if (type.equals("4")) {
					//获取access_tokenStr字符串对象
					   access_tokenStr = HttpUtils.sendFormEvalutionPost(
					            PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", 
					            "code=" + code + 
					            "&grant_type=authorization_code&client_id=6FF0E39AD917FD04&client_secret=ea201b6aff470f9ec25c1fc9be17197a&redirect_uri=" + 
					            PropertyUtil.getProperty("institution.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "6FF0E39AD917FD04", "ea201b6aff470f9ec25c1fc9be17197a");
				} else if (type.equals("5")) {
					//获取access_tokenStr字符串对象
					access_tokenStr = HttpUtils.sendFormEvalutionPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token",
							"code=" + code
									+ "&grant_type=authorization_code&client_id=8275669B807A7B7A&client_secret=229f39d615a8c2cbbef56e56197a22c7&redirect_uri="
									+ PropertyUtil.getProperty("teacherevalution.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "8275669B807A7B7A", "229f39d615a8c2cbbef56e56197a22c7");
				}
				//设置执行时间
				Long str333 = System.currentTimeMillis();
				int tokentime =(int) (str333-str12);
				UserLogEntity entity12=new UserLogEntity();
				entity12.setId(IDGenerator.genUID());
				entity12.setUserId(WebContext.getSessionUserId());
				entity12.setUserName(WebContext.getSessionUser().getPersonName());
				entity12.setLogType("获取token信息");
				entity12.setTimes(tokentime);
				entity12.setOperTime(new Date());
				//保存方法执行时间日志信息
				tagInstitutionEvalutionService.insertUserLog(entity12);
				//获取access_token
				if (access_tokenStr != null && !"".equals(access_tokenStr)) {
					//字符转化成json对象
					JSONObject accessjson = JSONObject.fromObject(access_tokenStr);
					//获取字段值
					String access_token = accessjson.getString("access_token");
					Long str3 = System.currentTimeMillis();
					//原有逻辑获取用户信息开始
					String userStr = HttpUtils.sendFormEvalutionPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/rest/get/userinfo",
							"access_token=" + access_token);
					Long str33 = System.currentTimeMillis();
					int userinfotime =(int) (str33-str3);
					UserLogEntity entity13=new UserLogEntity();
					entity13.setId(IDGenerator.genUID());
					entity13.setUserId(WebContext.getSessionUserId());
					entity13.setUserName(WebContext.getSessionUser().getPersonName());
					entity13.setLogType("获取/passport/rest/get/userinfo信息");
					entity13.setTimes(userinfotime);
					entity13.setOperTime(new Date());
					//保存方法执行需要的时间信息日志
					tagInstitutionEvalutionService.insertUserLog(entity13);
					System.out.println("userStr=:" + userStr);
					
					
					JSONObject userjson = JSONObject.fromObject(userStr);
					//原有逻辑获取用户信息结束
					//用户id
					String userid = userjson.getString("userid");
					//用户账号
					String account = userjson.getString("account");
					//用户类型
					String usertype = userjson.getString("usertype");
					//用户名称
					String name = userjson.getString("name");
					//用户性别
					String sex = userjson.getString("sex");
					//用户角色
					String role = userjson.getString("role");
					//获取tenantId
					String tenantId = userjson.getString("tenantId");
					//获取头像url
					String headUrl = userjson.getString("head_url");
					String teacher = "";
					*//**
					 * usertype=1
					 * 1:平台总系统管理员;2:平台省级系统管理员;3:平台市级系统管理员;4:学校总系统管理员;5:学校分校系统管理员;6:学校年级系统管理员;7:教育部分系统管理员
					 * usertype=0 
					 * 1:教师;2:家长;3:学生;4:管理者;5:教研员
					 *//*
					*//**
					 * 系统角色 01:管理员;04:教师
					 *//*
					UserRoleEntity userRole = new UserRoleEntity();
					UserRoleEntity userTeacherRole = new UserRoleEntity();
					UserEntity loginUser = new UserEntity();
					TpUserEntity saveUser = new TpUserEntity();
					*//**
					 * 获取用户是老师的信息
					 * 包括老师所属的学校信息
					 *//*
					if("1".equals(role)) {
						//获取教师信息
						Long str5 = System.currentTimeMillis();
						//调用外部接口获取教师信息
						String teacherinfo = HttpUtils.sendFormDataPost("https://gwss.yceduyun.com/apis/user/V1.0/teacherInfo",
								access_token,"002");
						Long str6 = System.currentTimeMillis();
						int teachertime =(int) (str6-str5);
						UserLogEntity entity2=new UserLogEntity();
						entity2.setId(IDGenerator.genUID());
						entity2.setUserId(WebContext.getSessionUserId());
						entity2.setUserName(WebContext.getSessionUser().getPersonName());
						entity2.setLogType("获取https://gwss.yceduyun.com/apis/user/V1.0/teacherInfo信息");
						entity2.setTimes(userinfotime);
						entity2.setOperTime(new Date());
						//保存获取教师信息所需要的时间日志信息
						tagInstitutionEvalutionService.insertUserLog(entity2);
						System.out.println(teacherinfo); 
                        //解析教师信息
						JSONObject teacherinfojson = JSONObject.fromObject(teacherinfo);
						JSONObject teacherinfoDatajson = JSONObject.fromObject(teacherinfojson.get("data"));
						//获取基本信息
						if(null!=teacherinfoDatajson) {
							//省份
							String proName=teacherinfoDatajson.getString("proName");
							//市区
							String cityName=teacherinfoDatajson.getString("cityName");
							//县级
							String townName=teacherinfoDatajson.getString("townName");
							//学校id
							String schoolId=teacherinfoDatajson.getString("schoolId");
							//学校名称
							String schoolName=teacherinfoDatajson.getString("schoolName");
							//学校类型
							String schoolType=teacherinfoDatajson.getString("schoolType");
							//
							String subjectId=teacherinfoDatajson.getString("subjectId");
							//
							String subjectName=teacherinfoDatajson.getString("subjectName");
							saveUser.setProName(proName);
							saveUser.setCityName(cityName);
							saveUser.setTownName(townName);
							saveUser.setSchoolId(schoolId);
							saveUser.setSchoolName(schoolName);
							saveUser.setSchoolType(schoolType);
							saveUser.setSubjectId(subjectId);
							saveUser.setSubjectName(subjectName);
						}
						//获取字段信息
						JSONArray teacherClassArray =JSONArray.fromObject(teacherinfoDatajson.get("classList"));
						//获取老师的班级年级信息
						@SuppressWarnings("unchecked")
						List<TeacherClassInfoEntity> list = teacherClassArray.toList(teacherClassArray, TeacherClassInfoEntity.class);
						//判断对象是否为空
						if(null!=list && list.size()>0) {
							List<TeacherClassInfoEntity> entityList = new ArrayList<TeacherClassInfoEntity>();
							//查询该老师下的信息是否存在
							TeacherClassInfoEntity entity = new TeacherClassInfoEntity();
							entity.setTeacherId(userid);
							entityList = teacherClassInfoService.searchTeacherClassInfoList(entity);
							if(null!=entityList && entityList.size()>0) {
								//循环遍历教师班级信息
								for(int j=0;j<entityList.size();j++) {
									TeacherClassInfoEntity entity1 = new TeacherClassInfoEntity();
									entity1.setId(entityList.get(j).getId());
									//删除已存在信息
									teacherClassInfoService.deleteTeacherClassInfoById(entity1);
								}
							}
							//循环保存班级信息
							for(int i=0;i<list.size();i++) {
								TeacherClassInfoEntity infoEntity = new TeacherClassInfoEntity();
								infoEntity.setTeacherId(userid);//老师id
								infoEntity.setGradeId(list.get(i).getGradeId());//年级id
								infoEntity.setGradeName(list.get(i).getGradeName());//年级名称
								infoEntity.setGradeLever(list.get(i).getGradeLever());//年级等级
								infoEntity.setClassId(list.get(i).getClassId());//班级id
								infoEntity.setClassName(list.get(i).getClassName());//班级名称
								infoEntity.setSubjectId(list.get(i).getSubjectId());//
								infoEntity.setSubjectName(list.get(i).getSubjectName());//
								infoEntity.setCreateDate(new Date());//创建日期
								infoEntity.setUpdateDate(new Date());//更新日期
								teacherClassInfoService.insertTeacherClassInfo(infoEntity);
							}
						}
						//获取用户具体信息
						String param=access_token;
						String tenantId1 = "002";
						Long str7 = System.currentTimeMillis();
						//获取教师角色
						String teacherDetailinfo = HttpUtils.sendFormDataDetailPost("https://gwss.yceduyun.com/apis/usercenter/v1.0/getOneUserInfoByUseIdAndRoleId",
								param,tenantId1,role);
						Long str8 = System.currentTimeMillis();
						int studenttime =(int) (str8-str7);
						UserLogEntity entity3=new UserLogEntity();
						entity3.setId(IDGenerator.genUID());
						entity3.setUserId(WebContext.getSessionUserId());
						entity3.setUserName(WebContext.getSessionUser().getPersonName());
						entity3.setLogType("获取https://gwss.yceduyun.com/apis/usercenter/v1.0/getOneUserInfoByUseIdAndRoleId信息");
						entity3.setTimes(userinfotime);
						entity3.setOperTime(new Date());
						//保存调用教师角色花费时间信息
						tagInstitutionEvalutionService.insertUserLog(entity3);
						//字符串转json
						JSONObject teacherDetailinfojson = JSONObject.fromObject(teacherDetailinfo);
						JSONArray teacherinfoDataDetailjson = JSONArray.fromObject(teacherDetailinfojson.get("data"));
						JSONArray dutyJson = JSONObject.fromObject(teacherinfoDataDetailjson.get(0)).getJSONArray("teacherDuties");
						List<InstitutionTeacherDutyEntity> dutylist = JSONArray.toList(dutyJson, InstitutionTeacherDutyEntity.class);
                        if(null!=dutylist && dutylist.size()>0) {
                        	for(int p=0;p<dutylist.size();p++) {
                        		//001代表校长
                        		if("校长".equals(dutylist.get(p).getDutyName())) {
                        			teacher="001";
                        			break;
                        		}else {
                        			//002代表老师
                        			teacher="002";
                        		}
                        	}
                        }
					}else if("3".equals(role)) {
						//获取学生信息
						Long str9 = System.currentTimeMillis();
						//调用学生接口获取信息
						String studentinfo = HttpUtils.sendFormDataPost("https://gwss.yceduyun.com/apis/user/V1.0/studentInfo",
								access_token,"002");
						Long str10 = System.currentTimeMillis();
						int studenttime =(int) (str10-str9);
						UserLogEntity entity3=new UserLogEntity();
						entity3.setId(IDGenerator.genUID());
						entity3.setUserId(WebContext.getSessionUserId());
						entity3.setUserName(WebContext.getSessionUser().getPersonName());
						entity3.setLogType("获取https://gwss.yceduyun.com/apis/user/V1.0/studentInfo信息");
						entity3.setTimes(userinfotime);
						entity3.setOperTime(new Date());
						//保存调用学生接口获取花费的时间日志
						tagInstitutionEvalutionService.insertUserLog(entity3);
						JSONObject studentinfojson = JSONObject.fromObject(studentinfo);
						JSONObject studentinfoDatajson = JSONObject.fromObject(studentinfojson.get("data"));
						if(null!= studentinfoDatajson) {
							String studentId=userid;
							//姓名
							String studentName=studentinfoDatajson.getString("username");
							String schoolId=studentinfoDatajson.getString("schoolId");//学校id
							String schoolName=studentinfoDatajson.getString("schoolName");//学校名称
							String schoolType=studentinfoDatajson.getString("schoolTye");//学校类型
							String gradeId=studentinfoDatajson.getString("gradeId");//年级id
							String gradeName=studentinfoDatajson.getString("gradeName");//年级名称
							String classId=studentinfoDatajson.getString("classId");//班级id
							String className=studentinfoDatajson.getString("className");//班级名称
							String gradeLever=studentinfoDatajson.getString("gradeLever");//年级等级
							String proName=studentinfoDatajson.getString("proName");//省份
							String cityName=studentinfoDatajson.getString("cityName");//市区
							String townName=studentinfoDatajson.getString("townName");//县级
							StudentClassInfoEntity studentClassInfoEntity = new StudentClassInfoEntity();
							studentClassInfoEntity.setStudentId(studentId);
							List<StudentClassInfoEntity> studentlist = new ArrayList<StudentClassInfoEntity>();
							studentlist = studentClassInfoService.searchStudentClassInfoList(studentClassInfoEntity);
							if(null!=studentlist && studentlist.size()>0) {
								//循环遍历学生信息
								for(int k=0;k<studentlist.size();k++) {
									StudentClassInfoEntity student = new StudentClassInfoEntity();
									student.setId(studentlist.get(k).getId());
									//删除学生信息
									studentClassInfoService.deleteStudentClassInfoById(student);
								}
							}
							//保存学生班级信息
							StudentClassInfoEntity stunfoEntity = new StudentClassInfoEntity();
							stunfoEntity.setStudentId(studentId);
							stunfoEntity.setStudentName(studentName);
							stunfoEntity.setSchoolId(schoolId);
							stunfoEntity.setSchoolName(schoolName);
							stunfoEntity.setSchoolType(schoolType);
							stunfoEntity.setGradeId(gradeId);
							stunfoEntity.setGradeName(gradeName);
							stunfoEntity.setGradeLever(gradeLever);
							stunfoEntity.setClassId(classId);
							stunfoEntity.setClassName(className);
							stunfoEntity.setProName(proName);
							stunfoEntity.setCityName(cityName);
							stunfoEntity.setTownName(townName);
							stunfoEntity.setCreateDate(new Date());
							studentClassInfoService.insertStudentClassInfo(stunfoEntity);
							Long str122 = System.currentTimeMillis();
							int studentinerttime =(int) (str122-str10);
							UserLogEntity entity32=new UserLogEntity();
							entity32.setId(IDGenerator.genUID());
							entity32.setUserId(WebContext.getSessionUserId());
							entity32.setUserName(WebContext.getSessionUser().getPersonName());
							entity32.setLogType("保存学生角色信息");
							entity32.setTimes(studentinerttime);
							entity32.setOperTime(new Date());
							tagInstitutionEvalutionService.insertUserLog(entity32);
						}
						
					}
					if (usertype != null && !"".equals(usertype) && usertype.equals("0")) {
						//老师
						if (!"".equals(role) && role.equals("1")) {
							userRole.setRoleId("01");
							userTeacherRole.setRoleId("04");
							loginUser.setUserKbn("A");
							saveUser.setUserKbn("A");
							if (userid != null && !"".equals(userid)) {
								userRole.setUserId(userid);
								userTeacherRole.setUserId(userid);
							}
							//管理者
						} else if (!"".equals(role) && role.equals("4")) {
							userRole.setRoleId("01");
							loginUser.setUserKbn("B");
							saveUser.setUserKbn("B");
							if (userid != null && !"".equals(userid)) {
								userRole.setUserId(userid);
							}
							//学生
						} else if(!"".equals(role) && role.equals("3")){
							loginUser.setUserKbn("C");
							saveUser.setUserKbn("C");
							//家长
						}else if(!"".equals(role) && role.equals("2")){
							loginUser.setUserKbn("D");
							saveUser.setUserKbn("D");
							//教研员
						}else if(!"".equals(role) && role.equals("5")){
							loginUser.setUserKbn("E");
							saveUser.setUserKbn("E");
						}
					}
					
					loginUser.setUserId(userid);
					loginUser.setUserName(account == null ? "" : account);
					loginUser.setSex(sex == null ? "" : sex);
					loginUser.setPersonName(name == null ? "" : name);
					loginUser.setPassword(MD5Util.getPassword4MD5("123456"));
					userRole.setCreateDate(new Date());
					userRole.setCreateUser(userid);
					userTeacherRole.setCreateDate(new Date());
					userTeacherRole.setCreateUser(userid);
					loginUser.setCreateDate(new Date());
					loginUser.setCreateUser(userid);
					loginUser.setLockFlag("0");

					saveUser.setUserId(userid);
					saveUser.setUserName(account == null ? "" : account);
					saveUser.setSex(sex == null ? "" : sex);
					saveUser.setPersonName(name == null ? "" : name);
					saveUser.setPassword(MD5Util.getPassword4MD5("123456"));
					saveUser.setCreateDate(new Date());
					saveUser.setCreateUser(userid);
					saveUser.setCreateDate(new Date());
					saveUser.setCreateUser(userid);
					saveUser.setLockFlag("0");
					saveUser.setTenantId(tenantId);
					saveUser.setHeadurl(headUrl);
					saveUser.setDuty(teacher);
					Long usertime1 = System.currentTimeMillis();
					courseService.saveUserAndRole(userRole, userTeacherRole, saveUser);
					Long usertime2 = System.currentTimeMillis();
					WebContext.loginUser(loginUser, true);
					WebContext.getSession().setAttribute("code", code);
					jo.put("result", "success");
					Long str123 = System.currentTimeMillis();
					System.out.println("计算花费时间---------------------------");
					int times = (int) (usertime2-usertime1);
					UserLogEntity entity=new UserLogEntity();
					entity.setId(IDGenerator.genUID());
					entity.setUserId(WebContext.getSessionUserId());
					entity.setUserName(WebContext.getSessionUser().getPersonName());
					entity.setLogType("获取保存用户信息");
					entity.setTimes(times);
					entity.setOperTime(new Date());
					tagInstitutionEvalutionService.insertUserLog(entity);
					System.out.println(str123-str12);
				}
			}

		}
		json.add(jo);
		return json;
	}
    *//**
     * 手机端首页
     * @return
     *//*
	@RequestMapping("/homePagePhone.action")
	public ModelAndView homePagePhone() {
		ModelAndView result = new ModelAndView("/course/client/homePagePhone");
		return result;
	}
    *//**
                * 教师页面
     * @return
     *//*
	@RequestMapping("/teacherHome.action")
	public ModelAndView teacherHome() {
		ModelAndView result = new ModelAndView("/course/client/teacherHomeBack");
		return result;
	}
    *//**
     * 学生页面
     * @return
     *//*
	@RequestMapping("/studentHome.action")
	public ModelAndView studentHome() {
		ModelAndView result = new ModelAndView("/course/client/studentHomeBack");
		return result;
	}
    *//**
     * 人员信息
     * @return
     *//*
	@RequestMapping("/sysUser.action")
	public ModelAndView sysUser() {
		ModelAndView result = new ModelAndView("/course/client/sysUser");
		result = getRoleAndNum(result);
		return result;
	}
    *//**
     * 系统日志
     * @return
     *//*
	@RequestMapping("/sysLog.action")
	public ModelAndView sysLog() {
		ModelAndView result = new ModelAndView("/course/client/sysLog");
		result = getRoleAndNum(result);
		return result;
	}
   *//**
    * 保存系统日志
    * @param proName
    * @param IpInfo
    * @return
    *//*
	@RequestMapping("/insertSysUser.action")
	@ResponseBody
	public JSONArray insertSysUser(String proName, String IpInfo) {
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		courseService.insertSysUser(proName, IpInfo);
		jo.put("result", "success");
		json.add(jo);
		return json;
	}
    *//**
     * 保存操作日志
     * @param browserInfo
     * @param sysInfo
     * @param IpInfo
     * @param operDetail
     * @param operSite
     * @return
     *//*
	@RequestMapping("/insertUserOper.action")
	@ResponseBody
	public JSONArray insertUserOper(String browserInfo, String sysInfo, String IpInfo, String operDetail,
			String operSite) {
		UserOperEntity oper = new UserOperEntity();
		oper.setSysInfo(browserInfo + "(" + sysInfo + ")");
		oper.setOperDetail(operDetail);
		oper.setOperSite(operSite);
		oper.setOperTime(new Date());
		oper.setOperIP(IpInfo);
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		courseService.inserUserOper(oper);
		jo.put("result", "success");
		json.add(jo);
		return json;
	}
	*//**
	 * 
	 * @param IpInfo
	 * @param logType
	 * @return
	 *//*
	@RequestMapping("/insertUserLog.action")
	@ResponseBody
	public JSONArray insertUserLog(String IpInfo, String logType) {
		UserLogEntity log = new UserLogEntity();
		log.setOperIP(IpInfo);
		log.setLogType(logType);
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		String userId = WebContext.getSessionUserId();
		String userName = WebContext.getSessionUser().getPersonName();
		log.setUserId(userId);
		log.setUserName(userName);
		courseService.insertUserLog(log);
		jo.put("result", "success");
		json.add(jo);
		WebContext.logout();
		return json;
	}
    *//**
                  * 根据用户名查询用户信息
     * @param username
     * @param IpInfo
     * @return
     *//*
	@RequestMapping("/getUserByUsername.action")
	@ResponseBody
	public JSONArray getUserByUserName(String username, String IpInfo) {
		TpUserEntity result = courseService.getUserByUsername(username);
		if (result != null) {
			UserLogEntity log = new UserLogEntity();
			log.setOperIP(IpInfo);
			log.setLogType("登录");
			log.setUserId(result.getUserId());
			log.setUserName(result.getPersonName());
			courseService.insertUserLog(log);
		}
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		if (result != null) {
			jo.put("result", "success");
		} else {
			jo.put("result", "error");
		}
		json.add(jo);
		return json;
	}

	*//**
	 * 系统首页
	 *//*
	@RequestMapping("/webEditor.action")
	public ModelAndView webEditor() {
		ModelAndView result = new ModelAndView("/course/client/webEditor");
		String serverName = WebContext.getRequest().getServerName();
		int serverPort = WebContext.getRequest().getServerPort();
		result.addObject("serverUrl", serverName + ":" + serverPort);
		return result;
	}
    *//**
     * 课程详情页面
     * @param courseId
     * @param schoolName
     * @return
     *//*
	@RequestMapping("/courseDetailInit.action")
	public ModelAndView courseDetailInit(String courseId,String schoolName) {
		ModelAndView result = new ModelAndView("/course/client/course_detail");
		ResultBean data = courseService.searchCourseDetail(courseId);
		if (!data.isStatus()) {
			throw new BusinessException(data.getMessages());
		}
		ChapterEntity entity = new ChapterEntity();
		entity.setCourseId(courseId);
		List<ChapterEntity> chapterCourseList = courseService.selectByCourseId(entity);
		result.addObject("chapterCourseList", chapterCourseList);
		result.addObject("courseView", data.getData());
		result.addObject("schoolName", schoolName);
		result.addObject("courseId", courseId);
		// 检索章节列表
		List<ChapterView> chapterList = chapterService.chapterList(courseId);
		result.addObject("chapterList", chapterList);
		String measureType="2";
		List<ChapterView> chapterList1 = chapterService.chapterList1(courseId,measureType);
		result.addObject("chapterExamList", chapterList1);
		List<CourseMessageEntity> userMsgList = courseService.getUserMsgList(courseId);
		if(userMsgList.size() > 0) {
			CourseMessageEntity msg = userMsgList.get(0);
			if(msg.getCheckStatus().equals("-1")) {
				result.addObject("msgInfo", "-1");
			}else if(msg.getCheckStatus().equals("1")) {
				result.addObject("msgInfo", "1");
			}else {
				result.addObject("msgInfo", "none");
			}
		}
		result = getRoleAndNum(result);
		return result;
	}
    *//**
     * 公开课详情页面
     * @param courseId
     * @return
     *//*
	@RequestMapping("/coursePublicDetail.action")
	public ModelAndView coursePublicDetail(String courseId) {
		ModelAndView result = new ModelAndView("/course/client/course_public_detail");
		ResultBean data = courseService.searchCourseDetail(courseId);
		if (!data.isStatus()) {
			throw new BusinessException(data.getMessages());
		}
		result.addObject("courseView", data.getData());
		result = getRoleAndNum(result);
		return result;
	}

	*//**
	 * 导师课程页面初始化
	 * 
	 * @param
	 * @return
	 *//*
	@RequestMapping("/teacherCourseInit.action")
	public ModelAndView teacherCourseInit(UserEntity user) {
		ModelAndView mv = new ModelAndView("/course/client/teacher_course_list");
		ResultBean result = courseService.teacherInfo(user);
		UserEntity use = new UserEntity();
		if (result.getData() != null) {
			use = (UserEntity) result.getData();
			if ("0".equals(use.getSex())) {
				use.setSex("男");
			} else {
				use.setSex("女");
			}
		}
		mv.addObject("user", use);
		return mv;
	}

	*//**
	 * 投票页面初始化
	 * 
	 * @param
	 * @return
	 *//*
	@RequestMapping("/voteItemInit.action")
	public ModelAndView voteItemInit(String courseId, String linkType) {
		ModelAndView mv = new ModelAndView("/course/client/vote_item");
		if (StringUtils.isNotEmpty(linkType)) {
			mv.addObject("linkType", linkType);
		}
		mv.addObject("courseId", courseId);
		return mv;
	}

	*//**
	 * 根据用户ID，课程ID检索用户课程
	 * 
	 * @param courseId
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchUserCourse.action")
	@ResponseBody
	public ResultBean searchUserCourse(String courseId) {
		ResultBean result = new ResultBean();
		if (courseId == null || courseId.equals("")) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_NULL_ID));

			return result;
		}

		UserCourseEntity uc = new UserCourseEntity();
		uc.setCourseId(courseId);
		// 从session中取出登录用户的ID
		String userId = WebContext.getSessionUserId();
		if (userId == null) {
			result.setStatus(false);
			result.setErrorCode(ErrorCode.NOT_LOGIN);
			result.setMessages(getMessage(MSG_E_SESSION_TIMEOUT));
			return result;
		}
		uc.setUserId(userId);
		uc = courseService.searchUserCourse(uc);
		result.setData(uc);
		result.setStatus(true);
		return result;
	}

	*//**
	 * 课程作业页面初始化
	 * 
	 * @param linkId   链接ID
	 * @param linkType 链接类型
	 * @return
	 *//*
	@RequestMapping("/homeWorkInit.action")
	public ModelAndView homeWorkInit(String linkId, String linkType) {
		ModelAndView mv = new ModelAndView("/course/client/home_work");
		if (StringUtils.isNotEmpty(linkType)) {
			mv.addObject("linkType", linkType);
		}
		mv.addObject("linkId", linkId);
		return mv;
	}

	*//**
	 * 检索我的作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/honeworkDetail.action")
	@ResponseBody
	public ResultBean honeworkDetail(HomeworkView homeworkView) {
		ResultBean result = new ResultBean();
		String userId = WebContext.getSessionUserId();
		List<HomeworkView> viewlist = new ArrayList<HomeworkView>();
		viewlist = (List<HomeworkView>) (courseService.honeworkList(homeworkView)).getData();
		for (int i = 0; i < viewlist.size(); i++) {
			CourseView courseView = new CourseView();
			courseView.setTeacherId(userId);
			if (courseService.isTeacher(courseView.getTeacherId())) {
				viewlist.get(i).setTeacher(1);
			} else {
				viewlist.get(i).setTeacher(0);
			}

		}

		result.setData(viewlist);
		return result;
	}

	@RequestMapping("/homeworkTitle.action")
	@ResponseBody
	public ResultBean homeworkTitle(HomeworkView homeworkView) {
		ResultBean result = new ResultBean();
		HomeworkEntity homeWork = courseService.getHomeWorkByLinkId(homeworkView);
		if (homeWork != null) {
			result.setStatus(true);
			result.setData(homeWork);
		}
		return result;
	}

	@RequestMapping("/homeWorkEdit.action")
	public ModelAndView homeWorkEdit(String id) {
		ModelAndView result = new ModelAndView("/course/client/home_work_edit");
		HomeworkView view = courseService.getHomeWorkById(id);
		result.addObject("homeWork", view);
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/homeWorkView.action")
	public ModelAndView homeWorkView(String id) {
		ModelAndView result = new ModelAndView("/course/client/home_work_view");
		HomeworkView view = courseService.getHomeWorkById(id);
		view.setCreateTime(sdf.format(view.getCreateDate()));
		result.addObject("homeWork", view);
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/homeWorkAddQuestion.action")
	public ModelAndView homeWorkAddQuestion(String id) {
		ModelAndView result = new ModelAndView("/course/client/home_work_question");
		HomeworkView view = courseService.getHomeWorkById(id);
		view.setCreateTime(sdf.format(view.getCreateDate()));
		result.addObject("homeWork", view);
		result.addObject("aq", new HomeworkAnswerQuestionEntity());
		result = getRoleAndNum(result);
		return result;
	}

	*//**
	 * 上传文件
	 *//*
	@RequestMapping("/upload.action")
	@ResponseBody
	public ResultBean upload(HomeworkView homeworkView) throws Exception {
		WebContext.init4CrossDomainAjax();
		return courseService.uploadDocument(homeworkView);
	}

	@RequestMapping("/download.action")
	public void download(@RequestParam(required = true) String fileId, String fileType, HttpServletResponse response)
			throws Exception {

		OutputStream output = new BufferedOutputStream(response.getOutputStream());
		InputStream input = null;
		try {
			FileEntity file = new FileEntity();
			if (StringUtil.isNotEmpty(fileType)) {
				file = fileService.getFile(fileId, fileType);
			} else {
				file = fileService.getFile(fileId, FILE_TYPE_HOMEWORK);
			}

			byte[] binary = file.getBinary();
			String fileName = file.getFileName();
			fileName = URLEncoder.encode(fileName, "UTF-8");
			response.reset();
			response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			response.addHeader("Content-Length", "" + file.getFileSize());
			response.setContentType("application/octet-stream;charset=UTF-8");
			if (StringUtil.isNotEmpty(file.getPath()) && StringUtil.isNotEmpty(ConfigUtil.getFileStorageRoot())) {
				// 文件有路径
				input = FileUtil.getInputStream(ConfigUtil.getFileStorageRoot(), file.getPath());
				IOUtils.copyLarge(input, output);
			} else {
				output.write(binary);
			}
			output.flush();
		} finally {
			IOUtils.closeQuietly(output);
			IOUtils.closeQuietly(input);
		}
	}

	*//**
	 * 课程作业管理页面初始化
	 * 
	 * @param
	 * @return
	 *//*
	@RequestMapping("/homeWorkReviewInit.action")
	public ModelAndView homeWorkReviewInit(String linkId, String linkType) {
		ModelAndView mv = new ModelAndView("/course/client/home_work_review");
		HomeworkView home = null;
		if (StringUtil.isNotEmpty(linkId) && StringUtil.isNotEmpty(linkType)) {
			home = courseService.searchworkDetail(linkId, linkType);
		}
		if (home == null) {
			home = new HomeworkView();
		}
		BaseEntity condition = new BaseEntity();
		condition.setPageSize(5);
		mv.addObject("linkId", linkId);
		mv.addObject("linkType", linkType);
		mv.addObject("title", home.getTitle());
		mv.addObject("courseTitle", home.getCourseTitle());
		mv.addObject("condition", condition);
		return mv;
	}

	*//**
	 * 导师品鉴作业初始化
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/homeworkReviewList.action")
	@ResponseBody
	public ResultBean homeworkReviewList(HomeworkView home) {

		return courseService.homeworkReviewList(home, PropertyUtil.getProperty("res.context.root"));
	}

	@RequestMapping("/gethomeworkReviewList.action")
	@ResponseBody
	public ResultBean gethomeworkReviewList(HomeworkView home) {
		return courseService.gethomeworkReviewList(home);
	}

	@RequestMapping("/gethomeworkReviewListByJson.action")
	@ResponseBody
	public JSONArray gethomeworkReviewListByJson(String courseId, String linkId, String linkType) throws IOException {

		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		List<HomeworkView> homeworkList = courseService.gethomeworkReviewListJson(courseId, linkId, linkType,
				PropertyUtil.getProperty("res.context.root"));
		if (homeworkList.size() > 0) {
			for (HomeworkView view : homeworkList) {
				Map<String, Object> courseERMap = new HashMap<String, Object>();
				courseERMap.put("userName", view.getCreateUser());
				courseERMap.put("audioContent", view.getContent() == null ? "" : view.getContent());
				// courseERMap.put("answerFileName", view.getAnswerFileName());
				courseERMap.put("score", view.getScore() == null ? "" : view.getScore());
				courseERMap.put("teacherComment", view.getTeacherComment() == null ? "" : view.getTeacherComment());
				// courseERMap.put("teacherScore", view.getTeacherScore() ==
				// null?"":view.getTeacherScore());
				// courseERMap.put("teacherComments", view.getTeacherComments() ==
				// null?"":view.getTeacherComments());
				resultList.add(courseERMap);
			}
		}
		jo.put("resultList", resultList);
		json.add(jo);
		return json;
	}

	*//**
	 * 导师评价
	 * 
	 * @author Id
	 * @param SCORE teacherComment
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/saveHomeworkReview.action")
	@ResponseBody
	public ResultBean saveHomeworkReview(HomeworkView home) {

		return courseService.saveHomeworkReview(home);
	}

	*//**
	 * 检索我的课程列表（检索用户课程，包括已关注、已学、已学完等检索条件以及检索列表中每一项课程的问卷）
	 * 
	 * @param finishType 完成类型（已收藏 1、已学2、3 已学完）
	 * @return 我的课程列表
	 * @author WUXIAOXIANG
	 *//*
	@RequestMapping("/userCourseList.action")
	@ResponseBody
	public ResultBean userCourseList(String finishType, Integer pageSize, Integer pageNumber) {
		// 检索我的课程
		ResultBean result = courseService.userCourseList(finishType, pageSize, pageNumber);
		return result;
	}

	@RequestMapping("/userCompulsoryList.action")
	@ResponseBody
	public ResultBean userCompulsoryList(String courseType, Integer pageSize, Integer pageNumber) {
		// 检索我的课程
		ResultBean result = courseService.userCompulsoryList(courseType, pageSize, pageNumber);
		return result;
	}

	*//**
	 * 检索我的作业列表
	 * 
	 * @author liuzhen
	 * @param
	 * @return 返回结果
	 *//*
	@RequestMapping("/userHomeworkList.action")
	@ResponseBody
	public ResultBean userHomeworkList(HomeworkView homeworkView, Integer pageSize, Integer pageNumber) {
		// 检索我的作业
		ResultBean result = courseService.userHomeworkList(homeworkView, pageSize, pageNumber);
		return result;
	}

	*//**
	 * 检索当前课程的所有作业
	 * 
	 * @author liuzhen
	 * @param
	 * @return 返回结果
	 *//*
	@RequestMapping("/alluserHomeworkList.action")
	@ResponseBody
	public ResultBean alluserHomeworkList(HomeworkView homeworkView, Integer pageSize, Integer pageNumber) {
		ResultBean result = courseService.getAlluserHomeworkList(homeworkView, pageSize, pageNumber);
		return result;
	}

	*//**
	 * 检索手机端导师个人中心的课程列表
	 * 
	 * @param pageSize   每页显示行数
	 * @param pageNumber 当前页
	 * @return 课程列表
	 * @author WUXIAOXIANG
	 *//*
	@RequestMapping("/teacherCourseAppList.action")
	@ResponseBody
	public ResultBean teacherCourseAppList(Integer pageSize, Integer pageNumber) {
		ResultBean result = courseService.teacherCourseAppList(pageSize, pageNumber);
		return result;
	}

	*//**
	 * 加入课程-支付详情初始化
	 * 
	 * @param courseId
	 * @return
	 *//*
	@SuppressWarnings({ "rawtypes" })
	@RequestMapping("/paydetail.action")
	public ModelAndView homeWorkReviewInit(String courseId) {
		ModelAndView mv = new ModelAndView("/course/client/course_pay_detail");
		UserEntity user = userService.searchById(WebContext.getSessionUserId());
		Map map = (Map) courseService.immediatePayDetail(courseId).getData();
		CourseView view = (CourseView) map.get("courseView");
		UserPoint userPoint = (UserPoint) map.get("userPoint");
		BigDecimal remainPoints = (BigDecimal) map.get("remainPoints");
		mv.addObject("user", user);
		mv.addObject("view", view);
		mv.addObject("userPoint", userPoint);
		mv.addObject("remainPoints", remainPoints);
		return mv;
	}

	*//**
	 * 用户管理页面 检索列表
	 * 
	 * @param condition
	 * @return 返回结果
	 *//*
	@RequestMapping("/search.action")
	@ResponseBody
	public ResultBean search(UserOperEntity condition) {
		return courseService.search4Page(condition);
	}

	@RequestMapping("/userOperInit.action")
	public ModelAndView userOperInit(String courseId) {
		ModelAndView result = new ModelAndView("/course/client/userOper");
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/mgrApprove.action")
	public ModelAndView mgrApprove(String courseId) {
		ModelAndView result = new ModelAndView("/course/client/mgr_approve");
		result = getRoleAndNum(result);
		return result;
	}
	
	@RequestMapping("/approveSearch.action")
	@ResponseBody
	public ResultBean approveSearch(CourseApproveEntity entity){
		return courseService.approveSearch(entity);
	}
	
	@RequestMapping("/saveHomeWork.action")
	@ResponseBody
	public ResultBean saveHomeWork(HomeworkView homework) throws Exception {
		return courseService.saveHomeWorkAnswer(homework);
	}

	@RequestMapping("/saveHomeWorkById.action")
	@ResponseBody
	public ResultBean saveHomeWorkById(HomeworkView homework) throws Exception {
		return courseService.saveHomeWorkById(homework);
	}
	
	@RequestMapping("/saveHomeWorkReviewById.action")
	@ResponseBody
	public ResultBean saveHomeWorkReviewById(String courseJson) throws Exception {
		HomeworkView homework = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, HomeworkView.class);
		return courseService.saveHomeWorkReviewById(homework);
	}

	@RequestMapping("/saveHomeWorkAnswerQuestion.action")
	@ResponseBody
	public ResultBean saveHomeWorkAnswerQuestion(HomeworkAnswerQuestionEntity aq) throws Exception {
		return courseService.saveHomeWorkAnswerQuestion(aq);
	}

	@RequestMapping("/answerQuestionList.action")
	@ResponseBody
	public ResultBean answerQuestionList(HomeworkAnswerQuestionEntity aq) {
		return courseService.searchAnswerQuestionList(aq);
	}

	@RequestMapping("/likeHomeWorkById.action")
	@ResponseBody
	public ResultBean likeHomeWorkById(String id) throws Exception {
		return courseService.likeHomeWorkById(id);
	}

	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/institutionlist.action")
	public ModelAndView institutionlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/institution/manager/institution_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		return result;
	}

	*//**
	 * 学校评价总列表
	 * 
	 * @return
	 *//*
	@RequestMapping("/institutionManger.action")
	public ModelAndView institutionManger() {
		ModelAndView result = new ModelAndView();
		UserEntity user = WebContext.getSessionUser();
		if (!"游客".equals(user.getPersonName())) {
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			TpUserEntity entity =new TpUserEntity();
			entity = userManagementService.searchById(userId);
			if ("A".equals(role) && "001".equals(entity.getDuty())) {
				result = new ModelAndView("/institution/manager/institutionManagerBack");
			} else if ("C".equals(role) || ("A".equals(role) && !"001".equals(entity.getDuty()))) {
				result = new ModelAndView("/institution/manager/institutionEvlutionStudentBack");
			}else if("B".equals(role)) {
				result = new ModelAndView("/institution/manager/institutionBack");
			}
		} else {
			result = new ModelAndView("/institution/manager/institutionEvlutionStudentBack");
		}
		
		return result;
	}
	

	*//**
	 * 标签一览
	 *//*
	@RequestMapping("/searchTagList2.action")
	@ResponseBody
	public ResultBean searchTagList2(TagInstitutionEntityView tagEntity) {
		return taxonomyInstitutionService.searchTagList2(tagEntity);

	}
	*//**
	 * 标签一览
	 *//*
	@RequestMapping("/searchInstitutionTagList2.action")
	@ResponseBody
	public ResultBean searchInstitutionTagList2(TagInstitutionEntityView tagEntity) {
		tagEntity.setCreateUser(WebContext.getSessionUserId());
		return taxonomyInstitutionService.searchInstitutionTagList2(tagEntity);

	}
	*//**
	 * 标签一览
	 *//*
	@RequestMapping("/searchTagSchoolList2.action")
	@ResponseBody
	public ResultBean searchTagSchoolList2(TagInstitutionEntityView tagEntity) {
		return taxonomyInstitutionService.searchTagList2(tagEntity);

	}
	*//**
	 * 分类基本列表
	 * 
	 * @return
	 *//*
	@RequestMapping("/institutioninit.action")
	public ModelAndView institutioninit() {
		ModelAndView mv = new ModelAndView("/institutiontaxonomy/manager/taxonomy_course_list");
		BaseEntity condition = new BaseEntity();
		condition.setPageSize(5);
		mv.addObject("condition", condition);
		return mv;
	}

	*//**
	 * 分类查询列表
	 * 
	 * @param businessType
	 * @return
	 *//*
	@RequestMapping("/searchTagTypeList2.action")
	@ResponseBody
	public List<TagTypeInstitutionView> searchTagTypeList2(String businessType) {

		return taxonomyInstitutionService.searchTagTypeList2(businessType);

	}

	*//**
	 * 
	 * 删除标签
	 * 
	 * @param tagIds
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/delTag.action")
	@ResponseBody
	public ResultBean delTag(String tagId, String ico) throws Exception {

		return taxonomyInstitutionService.delTag(tagId, ico);

	}

	*//**
	 * 保存信息
	 * 
	 * @param browserInfo
	 * @param sysInfo
	 * @param IpInfo
	 * @param operDetail
	 * @param operSite
	 * @return
	 *//*
	@RequestMapping("/insertUserInstitution.action")
	@ResponseBody
	public JSONArray insertUserInstitution(String browserInfo, String sysInfo, String IpInfo, String operDetail,
			String operSite) {
		UserOperEntity oper = new UserOperEntity();
		oper.setSysInfo(browserInfo + "(" + sysInfo + ")");
		oper.setOperDetail(operDetail);
		oper.setOperSite(operSite);
		oper.setOperTime(new Date());
		oper.setOperIP(IpInfo);
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		tagInstitutionEvalutionService.inserUserOper(oper);
		jo.put("result", "success");
		json.add(jo);
		return json;
	}

	*//**
	 * 编辑标签
	 * 
	 * @param TagEntity
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/updateTag.action")
	@ResponseBody
	public ResultBean updateTag(TagInstitutionEntity tagEntity) throws Exception {

		return taxonomyInstitutionService.updateTag(tagEntity);

	}

	*//**
	 * 保存新增标签
	 * 
	 * @param parentId 父标签ID,tagTypeId:标签类ID,tagName:标签名称
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveTag2.action")
	@ResponseBody
	public ResultBean saveTag2(TagInstitutionEntityView tagEntity) throws Exception {
		return taxonomyInstitutionService.saveTag2(tagEntity);

	}

	*//**
	 * 
	 * 初始化课程列表
	 * 
	 * @param condition 课程对象
	 * @return 返回课程列表初始化页面
	 *//*
	@RequestMapping("/institutionListInit.action")
	public ModelAndView institutionListInit(InstitutionMessageEntity condition) {
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		result.addObject("course", new InstitutionMessageEntity());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}

	*//**
	 * 
	 * 检索评价列表
	 * 
	 * @param queryInfo 机构对象
	 * @return 返回评价列表
	 *//*
	@RequestMapping("/listSearch.action")
	@ResponseBody
	public ResultBean listSearch(TbInstitutionEvalutionManagerView queryInfo) {
		queryInfo.setCreateUser(WebContext.getSessionUserId());
		return taxonomyInstitutionService.listSearch(queryInfo);
	}

	*//**
	 * 
	 * 检索评价问卷列表
	 * 
	 * @param queryInfo 机构对象
	 * @return 返回评价列表
	 *//*
	@RequestMapping("/listQestionSearch.action")
	@ResponseBody
	public ResultBean listQestionSearch(MeasurementInstitutionEntity queryInfo) {
		UserEntity user = WebContext.getSessionUser();	
		String role = user.getUserKbn();
		//判断权限
		String userId = user.getCreateUser();
		queryInfo.setCreateUser(userId);
		return taxonomyInstitutionService.listSchoolMeasurementSearch(queryInfo);
	}

	*//**
	 * 评价新增页面初始化
	 * 
	 * @return
	 *//*
	@RequestMapping("/addInit.action")
	public ModelAndView addInit() {
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_add");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		result.addObject("pageType", "add");
		result.addObject("course", new TbInstitutionEvalutionManagerView());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}

	*//**
	 * 搜素全部学校
	 * 
	 * @param query
	 * @return
	 *//*
	@RequestMapping("/searchSchoolAll.action")
	@ResponseBody
	public ResultBean searchSchoolAll(InstitutionEntity query) {
		return institutionService.getTeacherAll(query);
	}
	*//**
	 * 搜素评价问卷全部学校
	 * 
	 * @param query
	 * @return
	 *//*
	@RequestMapping("/searchEvalutionSchoolAll.action")
	@ResponseBody
	public ResultBean searchEvalutionSchoolAll(InstitutionEntity query) {
		if(null!=query && StringUtil.isNotBlank(query.getInstitutionNameseacher())) {
			query.setInstitutionName(query.getInstitutionNameseacher());
		}
		return institutionService.getEvalutionTeacherAll(query);
	}

	*//**
	 * 
	 * 根据学校ID，检索学校信息
	 * 
	 * @return
	 *//*
	@RequestMapping("/searchSchool.action")
	@ResponseBody
	public ResultBean searchSchool(String id) {
		ResultBean result = ResultBean.success();
		result.setStatus(true);
		return institutionService.searchSchool(id);
	}

	*//**
	 * 保存评价任务信息
	 * 
	 * @param courseJson 任务信息
	 * @return 返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/saveEvalutionInfo.action")
	@ResponseBody
	public ResultBean saveEvalutionInfo(String courseJson) throws Exception {
		TbInstitutionEvalutionManagerView course = JSONUtil.parse(
				JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson,
				TbInstitutionEvalutionManagerView.class);
		ResultBean result = ResultBean.success();
		course.setStatus("0");
		if (StringUtil.isEmpty(course.getId())) {
			institutionService.insertCourseCatagory(course);
		} else {
			institutionService.updateCourseAll(course);
		}
		return result;
	}

	*//**
	 * 暂存
	 * 
	 * @param courseJson
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/zancunEvalutionInfo.action")
	@ResponseBody
	public ResultBean zancunEvalutionInfo(String courseJson) throws Exception {
		TbInstitutionEvalutionManagerView course = JSONUtil.parse(
				JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson,
				TbInstitutionEvalutionManagerView.class);
		ResultBean result = ResultBean.success();
		course.setStatus("0");
		if (StringUtil.isEmpty(course.getId())) {
			institutionService.insertCourseCatagory(course);
		} else {
			institutionService.updateCourseAll(course);
		}
		return result;
	}

	*//**
	 * 删除评价任务信息
	 * 
	 * @param id
	 * @return
	 *//*
	@RequestMapping("/deleteEvalution.action")
	@ResponseBody
	public ResultBean deleteEvalution(String id) {
		ResultBean result = new ResultBean();
		institutionService.deleteEvalution(id);
		result.setMessages(getMessage(MSG_S_DELETE));
		return result;
	}

	@RequestMapping("/editInit.action")
	public ModelAndView editInit(String id) {
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_add");
		String status = "0";
		String type = "edit";
		TbInstitutionEvalutionManagerView course = tagInstitutionEvalutionService.searchEvalutionById(id, type);
		String evalutionId = course.getId();
		String institutionId = course.getTeacherId();
		List<TagLinkInstitutionEntity> entityList = new ArrayList<TagLinkInstitutionEntity>();
		TagLinkInstitutionEntity entity = new TagLinkInstitutionEntity();
		entity.setLinkId(evalutionId);
		entityList = tagInstitutionEvalutionService.searchList(entity);
		if (null != entityList) {
			for (int i = 0; i < entityList.size(); i++) {
				if (StringUtils.isBlank(entityList.get(i).getParentTagId())) {
					TagInstitutionEntity isentity = new TagInstitutionEntity();
					isentity.setTagId(entityList.get(i).getTagId());
					isentity = tagInstitutionService.searchFirstData(isentity);
					course.setFirstTagName(isentity.getTagName());
					course.setFirstTag(isentity.getTagName());
				} else {
					TagInstitutionEntity isentity = new TagInstitutionEntity();
					isentity.setTagId(entityList.get(i).getTagId());
					isentity = tagInstitutionService.searchFirstData(isentity);
					course.setSecondTag(isentity.getTagName());
					course.setSecondTagName(isentity.getTagName());
				}
			}
		}
		InstitutionEntity institution = new InstitutionEntity();
		institution = institutionService.selectInstitutionById(institutionId);
		course.setInstitutionName(institution.getInstitutionName());
		course.setSchoolType(institution.getSchoolType());
		course.setInstitutionAbbreviation(institution.getInstitutionAbbreviation());
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		result.addObject("tagsSecond", tagsSecond);
		result.addObject("course", course);
		result.addObject("pageType", type);
		return result;
	}

	*//**
	 * 问卷添加
	 * 
	 * @param courseId 评价任务ID
	 * @return
	 *//*
	@RequestMapping("/measureInit.action")
	@ResponseBody
	public ModelAndView measureInit(String courseId) {
		ModelAndView result = new ModelAndView("institution/manager/insitution_measure_add");
		MeasurementInstitutionEntity measure = new MeasurementInstitutionEntity();
		result.addObject("measure", measure);
		result.addObject("courseId", courseId);
		result.addObject("pageType", "add");
		return result;
	}
	*//**
	 * 问卷添加
	 * 
	 * @param courseId 评价任务ID
	 * @return
	 *//*
	@RequestMapping("/measureInit1.action")
	@ResponseBody
	public ModelAndView measureInit1(String courseId) {
		ModelAndView result = new ModelAndView("institution/manager/insitution_measure_editor");
		MeasurementInstitutionEntity measure = new MeasurementInstitutionEntity();
		measure.setLinkId(courseId);
		measure = measurementInstitutionService.findInstitutionMeasurement(measure);
		List<QuestionInstitutionEntity> questionList = measurementInstitutionService.searchQuestionList(measure.getId());
		measure.setQuestionList(questionList);
		result.addObject("measure", measure);
		result.addObject("courseId", courseId);
		result.addObject("pageType", "add");
		return result;
	}
	*//**
	 * 
	 * 问卷编辑页面初始化
	 * 
	 * @param courseId
	 * @return
	 *//*
	@RequestMapping("/measureInsitutionEditInit.action")
	@ResponseBody
	public ModelAndView measureEditInit(String courseId) {
		ModelAndView result = new ModelAndView("institution/manager/insitution_measure_add");
		MeasurementInstitutionEntity measure = measurementInstitutionService.seachMeasureById(courseId);
		result.addObject("measure", measure);
		result.addObject("courseId", courseId);
		result.addObject("isOnlyView", true);
		result.addObject("pageType", "edit");
		return result;
	}

	*//**
	 * 
	 * 问卷添加/编辑
	 * 
	 * @param measureJson
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveMeasure.action")
	@ResponseBody
	public ResultBean saveMeasure(String measureJson) throws Exception {
		MeasurementInstitutionEntity measure = JSONUtil.parse(
				JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), measureJson,
				MeasurementInstitutionEntity.class);
		ResultBean result = ResultBean.success();
		if (StringUtil.isEmpty(measure.getId())) {
			measurementInstitutionService.saveMeasure(measure);
		} else {
			measurementInstitutionService.updateInstitutionMeasure(measure);
		}
		return result;
	}

	*//**
	 * 
	 * 初始化评价问卷列表
	 * 
	 * @param condition 评价对象
	 * @return 返回课程列表初始化页面
	 *//*
	@RequestMapping("/institutionQuestListInit.action")
	public ModelAndView institutionQuestListInit(InstitutionMessageEntity condition) {
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_question_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		result.addObject("course", new InstitutionMessageEntity());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}

	*//**
	 * 
	 * 评价的发布/取消发布
	 * 
	 * @param id     课程id
	 * @param status 评价状态：0 未发布 1 已发布
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/publicEvalution.action")
	@ResponseBody
	public ResultBean publicEvalution(String id, String status) throws Exception {
		tagInstitutionEvalutionService.publicEvalution(id, status);
		return ResultBean.success(MSG_S_SUBMIT);
	}

	*//**
	 * 问卷显示页面
	 *//*
	@RequestMapping("/question.action")
	public ModelAndView question(String linkId, String linkType) {
		ModelAndView result = new ModelAndView("/institution/manager/institution_evalution_questionner_item");
		result.addObject("linkId", linkId);
		result.addObject("linkType", linkType);
		return result;
	}

	*//**
	 * 问卷题目选项
	 *//*
	@RequestMapping("/questionnaireDetial.action")
	@ResponseBody
	public ResultBean questionnaireDetial(String linkId, String linkType) {
		return questionInstitutionService.questionnaireDetial(linkId, linkType);
	}

	*//**
	 * 问卷提交
	 *//*
	@RequestMapping("/submitQuestionnaire.action")
	@ResponseBody
	public ResultBean submitQuestionnaire(String questionIds, String optionIds, String measureId) {
		return questionInstitutionService.submitQuestionnaire(questionIds, optionIds, measureId);
	}

	*//**
	 * 问卷调查结果列表
	 * 
	 * @author heyaqin
	 * @return 返回结果
	 *//*
	@RequestMapping("/questionResultList.action")
	public ModelAndView questionResultList(MeasurementInstitutionEntity condition) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_result_list");
		mv.addObject("condition", condition);
		UserEntity user = WebContext.getSessionUser();
		if (!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if (roleList.size() > 0) {
				for (UserRoleEntity role : roleList) {
					if (role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			} else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}

	*//**
	 * 问卷调查列表检索
	 * 
	 * @author
	 * @return 返回结果
	 *//*
	@RequestMapping("/questionResultListSearch.action")
	@ResponseBody
	public ResultBean questionResultListSearch(MeasurementInstitutionEntity condition) {
			UserEntity user = WebContext.getSessionUser();	
			String userId = user.getCreateUser();
			condition.setCreateUser(userId);
		return userManagementService.selectQuestionResultList(condition);
	}

	*//**
	 * 问卷统计列表初始化
	 * 
	 * @author
	 * @param id 问卷ID
	 * @return 返回结果
	 *//*
	@RequestMapping("/viewMeasurementListInit.action")
	public ModelAndView viewMeasurementListInit(String id) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_statistics");
		int optionNum = userManagementService.searchInstitutionTotalOptions(id) + 1;
		QuestionOptionInstitutionEntity questionOptionEntity = new QuestionOptionInstitutionEntity();
		questionOptionEntity.setMeasureId(id);
		mv.addObject("questionOptionEntity", questionOptionEntity);
		mv.addObject("optionNum", optionNum);
		// 问卷统计结果列表
		ArrayList<Map<String, String>> optionMapList = userManagementService.selectInstitutionOptionsList(id);
		mv.addObject("optionMapList", optionMapList);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 问卷统计列表检索
	 * 
	 * @author HEYAQIN
	 * @param id 问卷ID
	 * @return 返回结果
	 *//*
	@RequestMapping("/viewMeasurementList.action")
	@ResponseBody
	public ArrayList<Map<String, String>> viewMeasurementList(String id) {
		if (StringUtil.isEmpty(id)) {
			return null;
		} else {
			ArrayList<Map<String, String>> optionMapList = userManagementService.selectInstitutionOptionsList(id);
			return optionMapList;
		}
	}

	*//**
	 * 问卷参与人员 检索
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/viewUserSearch.action")
	@ResponseBody
	public ResultBean viewUserSeacher(String measurementId) {
		return userManagementService.selectEvalutionQuestionUserList(measurementId);
	}

	*//**
	 * 问卷参与人员回答详情
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/viewDetailInit.action")
	public ModelAndView viewDetailInit(UserMeasurementResultEntity condition) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_user_detail");
		if (StringUtil.isEmpty(condition.getMeasurementId())) {
			throw new BusinessException("参数错误!");
		}

		List<UserMeasurementResultEntity> list = measuermentService
				.searchInstitutionUserMeasurementResultByEntiy(condition);
		if (list != null && list.size() != 0) {
			mv.addObject("condition", list.get(0));
		} else {
			mv.addObject("condition", new UserMeasurementResultEntity());
		}
		return mv;
	}

	*//**
	 * 问卷回答详情
	 * 
	 * @param measurementId
	 * @param userId
	 * @return
	 *//*
	@RequestMapping("/viewUserAnswerSearch.action")
	@ResponseBody
	public ResultBean viewUserAnswerSearch(String measurementId, String userId) {
		return userManagementService.selectEvalutionAnswerUserList(measurementId, userId);
	}

	*//**
	 * 检索测试信息
	 * 
	 * @param linkId
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchMeasuerment.action")
	@ResponseBody
	public ResultBean searchMeasuerment(String measurementId, String userId) {
		if (StringUtil.isEmpty(measurementId)) {
			return ResultBean.error(getMessage("参数错误!"));
		}
		ResultBean result = ResultBean.success();
		List<QuestionInstitutionEntity> questionList = measurementInstitutionService
				.searchMeasuermentInstitutionNormalAndExam(userId, measurementId);
		result.setData(questionList);
		return result;
	}

	*//**
	 * 问卷参与人员 画面初始化
	 * 
	 * @param condition
	 * @return
	 *//*
	@SuppressWarnings("unchecked")
	@RequestMapping("/viewUserInit.action")
	public ModelAndView viewUserInit(MeasurementInstitutionEntity condition) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_user_list");
		mv.addObject("measurementId", condition.getId());
		List<MeasurementInstitutionEntity> list = measurementInstitutionService.search(condition);
		if (list != null && list.size() != 0) {
			mv.addObject("condition", list.get(0));
		} else {
			throw new BusinessException("参数错误!");
		}
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 删除问卷调查列表
	 * 
	 * @author heyaqin
	 * @return 返回结果
	 *//*
	@RequestMapping("/deleteQuestionList.action")
	@ResponseBody
	public ResultBean deleteQuestionList(String id) {
		userManagementService.deleteEvalutionQuestionList(id);
		return ResultBean.success();
	}

	*//**
	 * 导入页面初始化
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/importInit.action")
	public String importInit() {
		return "institution/manager/institution_import";
	}

	*//**
	 * 导入机构
	 * 
	 * @param linkId
	 * @return 返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/importUpload.action")
	@ResponseBody
	public ResultBean importCourse(@RequestParam("institution") CommonsMultipartFile institution,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		InputStream input = null;
		try {
			input = institution.getInputStream();
			File targetFile = FileUtil.writeToTmpFile(input);
			return institutionService.importInstitution(targetFile);
		} finally {
			IOUtils.closeQuietly(input);
		}
	}

	*//**
	 * 批量导出机构
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/exportInstitution.action")
	@ResponseBody
	public InstitutionManagerView exportInstitution(InstitutionEntity entity) throws Exception {
		List<InstitutionManagerView> viewList = new ArrayList<InstitutionManagerView>();
		UserEntity userEntity = WebContext.getSessionUser();
		if (!"游客".equals(userEntity.getPersonName())) {
			String role = userEntity.getUserKbn();
			String userId=userEntity.getUserId();
			if("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				entity.setCreateUser(userId);
				viewList = institutionService.exportInstitution(entity);
			}else if("B".equals(role)) {
				entity.setCreateUser(userId);
				viewList = institutionService.exportManagerInstitution(entity);
			}
		}
		
		InstitutionManagerView user = new InstitutionManagerView();
		user.setInstitutionList(viewList);
		ExcelDataBind edbUser = new ExcelDataBind(user);
		InputStream is = null;
		OutputStream os = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String date = sdf.format(new Date());
			String fileName = URLEncoder.encode("组织机构" + date + ".xlsx", "UTF-8");
			WebContext.getResponse().reset();
			is = InstitutionService.class.getResourceAsStream("institutionExportTemplate.xlsx");
			WebContext.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			WebContext.getResponse()
					.setContentType(" application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8");
			edbUser.bind(is, WebContext.getResponse().getOutputStream());
		} catch (IOException ex) {

		} finally {

			IOUtils.closeQuietly(is);
			IOUtils.closeQuietly(os);
		}
		return null;
	}

	*//**
	 * 增加机构方法
	 * 
	 * @param entity
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/addJyInstitution.action")
	public ModelAndView addJyInstitution(InstitutionEntity entity) throws Exception {
		ModelAndView mv = new ModelAndView("institution/manager/institution_add");
		InstitutionEntity institutionEntity = new InstitutionEntity();
		UserEntity user = WebContext.getSessionUser();
		String role = user.getUserKbn();
		//判断权限
		List<InstitutionEntity> schoollist=new ArrayList<InstitutionEntity>();
		String userId = user.getCreateUser();
		if ("A".equals(role)) {
			TpUserEntity tpUserEntity =new TpUserEntity();
			tpUserEntity = userManagementService.searchById(userId);
			institutionEntity.setInstitutionName(tpUserEntity.getSchoolName());
			institutionEntity=institutionService.seacherPersonEdution(institutionEntity);
			if(null!=institutionEntity) {
				entity.setInstitutionName(institutionEntity.getInstitutionName());
			}
			schoollist.add(institutionEntity);
		}else if("B".equals(role)) {
			institutionEntity.setCreateUser(userId);
			institutionEntity= institutionService.searchEdution(institutionEntity);
            if(null!=institutionEntity) {
            	entity.setInstitutionName(institutionEntity.getInstitutionName());
			}
            schoollist.add(institutionEntity);
		}
		mv.addObject("schoollist", schoollist);
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 添加学校信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/addInstitution.action")
	public ModelAndView addInstitution(String institutionParentId) throws Exception {
		InstitutionEntity entity = new InstitutionEntity();
		entity.setInstitutionParentId(institutionParentId);
		ModelAndView mv = new ModelAndView("institution/manager/institution_addSchool");
		List<InstitutionEntity> schoollist=new ArrayList<InstitutionEntity>();
		UserEntity user = WebContext.getSessionUser();
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				schoollist = institutionService.seacherSchoolLocationList(entity);
			} else if("B".equals(role)) {
				entity.setCreateUser(userId);
				schoollist= institutionService.searchEdutionSchoolList(entity);
			}
		
		mv.addObject("schoollist", schoollist);
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 删除班级
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/deleteInstitutionById.action")
	@ResponseBody
	public ResultBean deleteGradeById(String id) throws Exception {
		InstitutionEntity entity = new InstitutionEntity();
		entity.setId(id);
		institutionService.deleteInstitutionById(entity);
		return ResultBean.success(DELETE_GRADE_SUCCESS);
	}

	*//**
	 * 跳转到班级编辑页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/institutionEdit.action")
	public ModelAndView institutionEdit(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		ModelAndView mv = new ModelAndView("institution/manager/institution_edit");
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 检索班级信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchInstitution.action")
	@ResponseBody
	public ResultBean searchNotice(InstitutionEntity entity) {
		return institutionService.searchInstitutionEvalutionList(entity);
	}
	*//**
	 * 学校检索班级信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/schoolsearchInstitution.action")
	@ResponseBody
	public ResultBean schoolsearchInstitution(InstitutionEntity entity) {
		return institutionService.searchInstitutionEvalutionList1(entity);
	}
	*//**
	 * 检索班级信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchSchoolInstitution.action")
	@ResponseBody
	public ResultBean searchSchoolInstitution(InstitutionEntity entity) {
		ResultBean result = new ResultBean();
		UserEntity user = WebContext.getSessionUser();
		if (!"游客".equals(user.getPersonName())) {
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				entity.setCreateUser(userId);
				result=institutionService.searchInstitutionTeacherEvalutionList(entity);
			} else if ("C".equals(role)) {
				entity.setCreateUser(userId);
				StudentClassInfoEntity classInfo = new StudentClassInfoEntity();
				classInfo.setStudentId(userId);
				classInfo = studentClassInfoService.searchStudentClassInfo(classInfo);
			    if(null!=classInfo) {
			    	entity.setInstitutionName(classInfo.getSchoolName());
					result= institutionService.searchInstitutionEvalutionList(entity);
			    }
			}else if("B".equals(role)) {
				entity.setCreateUser(userId);
				result= institutionService.searchInstitutionManagerEvalutionList(entity);
			}
		}
		return result;
	}

	*//**
	 * 新增信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/insertInstitution.action")
	@ResponseBody
	public ResultBean insertInstitution(InstitutionEntity entity) throws Exception {
		ResultBean result = new ResultBean();
		InstitutionEntity institutionEntity = new InstitutionEntity();
		institutionEntity.setInstitutionName(entity.getInstitutionName());
		List<InstitutionEntity> datalist = (List<InstitutionEntity>) institutionService.searchInstitutionEvalutionList1(institutionEntity).getData();
		if(null!=datalist && datalist.size()>0) {
			result.setStatus(false);
			result = ResultBean.error("学校名称已存在");
		}else {
//			String institutionType=entity.getInstitutionType();
//			TagInstitutionEntity tagInstitutionEntity = new TagInstitutionEntity();
//			tagInstitutionEntity.setTagId(institutionType);
//			tagInstitutionEntity = tagInstitutionService.searchFirstData(tagInstitutionEntity);
//			entity.setInstitutionType(tagInstitutionEntity.getTagName());
			if(StringUtil.isNotBlank(entity.getSchoolNatrue())) {
				if("1".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("小学");
				}else if("2".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("初中");
				}else if("3".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("高中");
				}else if("4".equals(entity.getSchoolNatrue())) {
				   entity.setInstitutionType("九年一贯制");
			    }
			}
			institutionService.insertInstitution(entity, getCompressConfigByName("imageP"));
			result.setStatus(true);
			result= ResultBean.success(MSG_ADD_GRADE_SUCCESS);
		}
		return result;
	}
	*//**
	 * 新增信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/insertSchoolEvalutionInstitution.action")
	@ResponseBody
	public ResultBean insertSchoolEvalutionInstitution(InstitutionEntity entity) throws Exception {
		institutionService.insertInstitution(entity, getCompressConfigByName("imageP"));
		return ResultBean.success(MSG_ADD_GRADE_SUCCESS);
	}
	*//**
	 * 编辑保存
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/updateInstitution.action")
	@ResponseBody
	public ResultBean updateInstitution(InstitutionEntity entity) throws Exception {
		ResultBean result = new ResultBean();
		InstitutionEntity institutionEntity = new InstitutionEntity();
		institutionEntity.setInstitutionName(entity.getInstitutionName());
		List<InstitutionEntity> datalist = (List<InstitutionEntity>) institutionService.searchInstitutionEvalutionList1(institutionEntity).getData();
		if(null!=datalist && datalist.size()>0 && !entity.getId().equals(datalist.get(0).getId())) {
			result.setStatus(false);
			result = ResultBean.error("学校名称已存在");
		}else {
//			String institutionType=entity.getInstitutionType();
//			TagInstitutionEntity tagInstitutionEntity = new TagInstitutionEntity();
//			tagInstitutionEntity.setTagId(institutionType);
//			tagInstitutionEntity = tagInstitutionService.searchFirstData(tagInstitutionEntity);
//			entity.setInstitutionType(tagInstitutionEntity.getTagName());
			if(StringUtil.isNotBlank(entity.getSchoolNatrue())) {
				if("1".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("小学");
				}else if("2".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("初中");
				}else if("3".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("高中");
				}else if("4".equals(entity.getSchoolNatrue())) {
					   entity.setInstitutionType("九年一贯制");
				}
			}
			institutionService.updateInstitution(entity, getCompressConfigByName("imageP"));
			result.setStatus(true);
			result = ResultBean.success("创建学校成功");
		}
		return result;
	}
	*//**
	 * 编辑保存
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/updateSchoolEvalutionInstitution.action")
	@ResponseBody
	public ResultBean updateSchoolEvalutionInstitution(InstitutionEntity entity) throws Exception {
		entity.setCreateUser(WebContext.getSessionUserId());
		institutionService.updateInstitution(entity, getCompressConfigByName("imageP"));
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/institutionStudentlist.action")
	public ModelAndView institutionStudentlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/institution/manager/institution_student_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		return result;
	}

	*//**
	 * 跳转到班级查看页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/institutionLook.action")
	public ModelAndView institutionLook(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		ModelAndView mv = new ModelAndView("institution/manager/institution_look");
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 老师分享作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/shareWork.action")
	@ResponseBody
	public ResultBean shareWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.shareHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 老师取消分享作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/cancleShareWork.action")
	@ResponseBody
	public ResultBean cancleShareWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.cancleShareHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);

	}

	*//**
	 * 老师置顶作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/topWork.action")
	@ResponseBody
	public ResultBean topWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.topHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 老师取消置顶作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/cancleTopWork.action")
	@ResponseBody
	public ResultBean cancleTopWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.cancleTopHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 老师删除作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/deleteWork.action")
	@ResponseBody
	public ResultBean deleteWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.deleteWork(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/baseinstitutionlist.action")
	public ModelAndView baseinstitutionlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/baseinstitution/manager/base_institution_list");
		result.addObject("condition", condition);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/schoolbaseinstitutionlist.action")
	public ModelAndView schoolbaseinstitutionlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/baseinstitution/manager/base_school_institution_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 增加机构方法
	 * 
	 * @param entity
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/addBaseInstitution.action")
	public ModelAndView addBaseInstitution(InstitutionEntity entity) throws Exception {
		ModelAndView mv = new ModelAndView("baseinstitution/manager/base_institution_add");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList1(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		List<TagInstitutionEntity> tagList = tagInstitutionService.searchTagInstitutionAll();
		mv.addObject("tagList", tagList);
		mv.addObject("catagoryTags", tags);
		mv.addObject("tagsSecond", tagsSecond);
		mv.addObject("institution", entity);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 跳转到班级编辑页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/baseinstitutionEdit.action")
	public ModelAndView baseinstitutionEdit(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		ModelAndView mv = new ModelAndView("baseinstitution/manager/base_institution_edit");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList1(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		List<TagInstitutionEntity> tagList = tagInstitutionService.searchTagInstitutionAll();
        String secondId = entity.getFirstTag();
        TagInstitutionEntity taginfo = tagInstitutionService.searchTagInstitution(secondId);
        entity.setSecondTag(taginfo.getParentId());
		mv.addObject("tagList", tagList);
		mv.addObject("catagoryTags", tags);
		mv.addObject("tagsSecond", tagsSecond);
		mv.addObject("institution", entity);
		mv = getRoleAndNum(mv);
		return mv;
	}
	
	*//**
	 * 跳转到班级编辑页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/baseSchoolinstitutionEdit.action")
	public ModelAndView baseSchoolinstitutionEdit(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		InstitutionEntity institutionEntity = new InstitutionEntity();
		ModelAndView mv = new ModelAndView("baseinstitution/manager/base_school_institution_edit");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList1(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		List<TagInstitutionEntity> tagList = tagInstitutionService.searchTagInstitutionAll();
		List<InstitutionEntity> schoollist=new ArrayList<InstitutionEntity>();
		UserEntity user = WebContext.getSessionUser();
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			if(StringUtil.isNotBlank(entity.getInstitutionParentId())) {
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				schoollist = institutionService.seacherSchoolLocationList(entity);
			} else if("B".equals(role)) {
				entity.setCreateUser(userId);
				schoollist= institutionService.searchEdutionSchoolList(entity);
			}
			
		}else {
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				institutionEntity.setInstitutionName(tpUserEntity.getSchoolName());
				institutionEntity=institutionService.seacherPersonEdution(institutionEntity);
				entity.setInstitutionName(institutionEntity.getInstitutionName());
				schoollist.add(institutionEntity);
			}else if("B".equals(role)) {
				institutionEntity.setCreateUser(userId);
				institutionEntity= institutionService.searchEdution(institutionEntity);
				entity.setInstitutionName(institutionEntity.getInstitutionName());
				schoollist.add(institutionEntity);
			}
		}
		
		mv.addObject("schoollist", schoollist);
		mv.addObject("tagList", tagList);
		mv.addObject("catagoryTags", tags);
		mv.addObject("tagsSecond", tagsSecond);
		mv.addObject("institution", entity);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 分类基本列表
	 * 
	 * @return
	 *//*
	@RequestMapping("/baseinstitutioninit.action")
	public ModelAndView baseinstitutioninit() {
		ModelAndView mv = new ModelAndView("baseinstitutiontaxonomy/manager/base_taxonomy_course_list");
		BaseEntity condition = new BaseEntity();
		condition.setPageSize(5);
		mv.addObject("condition", condition);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 学校列表页面
	 *//*
	@RequestMapping("/schoolListInit.action")
	public ModelAndView schoolListInit(String schoolName) {
		ModelAndView result = new ModelAndView("baseinstitution/manager/school_list");
		String userId= WebContext.getSessionUserId();
		ManagerUserEntity entity= managerUserService.searchByUserId(userId);
		String mangerRole="0";
		if(null!=entity || "2542668".equals(userId)) {
			mangerRole="1";
		}else {
			mangerRole="0";
		}
		result.addObject("schoolName", schoolName);
		result.addObject("mangerRole", mangerRole);
		result = getRoleAndNum(result);
		return result;
	}

	*//**
	 * 检索课程 包括分类
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 * directionTagId 学段
	 * categoryTagId 分类
	 *//*
	@RequestMapping("/schoolList.action")
	@ResponseBody
	public ResultBean schoolList(InstitutionEntity course, String directionTagId, String categoryTagId,
			String courseStage, String sortType, Integer pageSize, Integer pageNumber,String schoolStage) {
		ResultBean result = new ResultBean();
		if(StringUtil.isNotBlank(course.getSchoolName())) {
			course.setInstitutionName(course.getSchoolName());
		}
		//区域
		if(StringUtil.isNotBlank(directionTagId)) {
			course.setFirstTag(directionTagId);
		}
		if(StringUtil.isNotBlank(directionTagId)) {
			TagInstitutionEntity entity = new TagInstitutionEntity();
			entity.setTagId(directionTagId);
			entity = tagInstitutionService.searchFirstData(entity);
			String xdName = entity.getTagName();
			course.setInstitutionType(xdName);
		}
		if(StringUtil.isNotBlank(categoryTagId)) {
			TagInstitutionEntity entity = new TagInstitutionEntity();
			entity.setTagId(categoryTagId);
			entity = tagInstitutionService.searchFirstData(entity);
			String xdName = entity.getTagName();
			course.setFirstTag(categoryTagId);
		}
		//学校阶段
		if(StringUtil.isNotBlank(schoolStage)) {
			course.setSchoolNatrue(schoolStage);
		}
		//权限查看
		String userId = WebContext.getSessionUserId();
		ManagerUserEntity entity= managerUserService.searchByUserId(userId);
		//教育局的人员暂时看到全部
		if(null!=entity || "2542668".equals(userId) || "B".equals(WebContext.getSessionUser().getUserKbn())) {
			result = courseService.schoolList(course, directionTagId, categoryTagId, courseStage, sortType,
					pageSize, pageNumber);
		}else {
			String role = WebContext.getSessionUser().getUserKbn();
			course.setCreateUser(userId);
			if("A".equals(role)) {
				result = courseService.schooTeacherlList(course, directionTagId, categoryTagId, courseStage, sortType,
						pageSize, pageNumber);
			}else if("C".equals(role)) {
				result = courseService.schooStudentlList(course, directionTagId, categoryTagId, courseStage, sortType,
						pageSize, pageNumber);
			}
		}
		
		return result;
	}
	*//**
	 * 日志操作页面 检索列表
	 * 
	 * @param condition
	 * @return 返回结果
	 *//*
	@RequestMapping("/logsearch.action")
	@ResponseBody
	public ResultBean logsearch(UserOperEntity condition) {
		return courseService.search4Page(condition);
	}
	*//**
	 * 已完成
	 * @param finishType
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 *//*
	@RequestMapping("/userCourseFinishList.action")
	@ResponseBody
	public ResultBean userCourseFinishList(String finishType, Integer pageSize, Integer pageNumber) {
		finishType ="3";
		pageSize =5;
		pageNumber=1;
		// 检索我的课程
		ResultBean result = courseService.userCourseList(finishType, pageSize, pageNumber);
		return result;
	}
	*//**
	 * 已完成
	 * @param finishType
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 *//*
	@RequestMapping("/userCourseUnFinishList.action")
	@ResponseBody
	public ResultBean userCourseUnFinishList(String finishType, Integer pageSize, Integer pageNumber) {
		pageSize =5;
		pageNumber=1;
		// 检索我的课程
		ResultBean result = courseService.userCourseUnFinishList(finishType, pageSize, pageNumber);
		return result;
	}
    *//**
                  * 作业列表
     * @return
     *//*
	@RequestMapping("/zuoyeList.action")
	@ResponseBody
	public ResultBean zuoyeList() {
		// 检索我的课程
		ResultBean result = courseService.userZyList();
		return result;
	}
    *//**
     * 
                  * 考试结果查看
     * @param UserMeasurementResultEntity condition 入参
     * @return
     *//*
    @RequestMapping("/userExamDetail.action")
    public ModelAndView userExamDetail(UserMeasurementResultEntity condition) {
    	//考试结果页面
        ModelAndView result = new ModelAndView("course/manager/user_exam_detail");
        //考试结果id是否存在
        if(StringUtil.isEmpty(condition.getMeasurementId())){
            throw new BusinessException("参数错误!");
        }
        condition.setCreateUser(WebContext.getSessionUserId());
        //查询考试结果
        List<UserMeasurementResultEntity> list = measuermentService.searchUserMeasurementResultByEntiy(condition);
        if(list != null && list.size() != 0) {
            result.addObject("condition", list.get(0));
        } else {
            result.addObject("condition", new UserMeasurementResultEntity());
        }
        //返回考试结果
        return result;
    }
	*//**
	 * 查询全部考试
	 * @param ChapterEntity chapter章节参数
	 * @param pageSize  每页展示多少条数据
	 * @param pageNumber 页码
	 * @return
	 *//*
	@RequestMapping("/myAllExam.action")
	public ModelAndView myAllExam(ChapterEntity chapter, Integer pageSize, Integer pageNumber) {
		//考试页面内容展示路径
		ModelAndView mv = new ModelAndView("baseinstitution/manager/my_exam_list");
		//查询数据表
		ResultBean result = chapterService.myAllxam(chapter, pageSize, pageNumber);
		mv.addObject("chapter", result.getData());
		return mv;
	}
	*//**
	 *检索考试
	 * @param chapter
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 *//*
	@RequestMapping("/myAllExamList.action")
	@ResponseBody
	public ResultBean myAllExamList(ChapterEntity chapter, Integer pageSize, Integer pageNumber) {
		ResultBean result = chapterService.myAllxam(chapter, pageSize, pageNumber);
		return result;
	}
	*//**
	 * 查询课工厂方法
	 * @param courseId 课程id
	 * @param content 课程内容
	 * @param type 课程类型
	 * @return
	 *//*
	@RequestMapping("/factory.action")
    @ResponseBody
    public ResultBean factory(String courseId,String content,String type)  {
        ResultBean result = new ResultBean();
        result = courseService.factory(courseId,content,type);
        return result;
    }
	*//**
	 * 用户作业列表
	 * @return
	 *//*
	@RequestMapping("/userHomeWork.action")
	public ModelAndView userHomeWork() {
		//作业列表路径
		ModelAndView result = new ModelAndView("/course/client/user_home_work");
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 查询作业列表
	 * @return
	 *//*
	@RequestMapping("/userHomeWorkSearch.action")
	@ResponseBody
	public ResultBean search(){
		//返回结果
		return courseService.searchUserHomeWork4Page();
	}
	*//**
	 * 查询学校信息
	 * @return
	 *//*
	@RequestMapping("/selectSchoolNameInfo.action")
	@ResponseBody
	public ResultBean selectSchoolNameInfo() {
		ResultBean result = new ResultBean();
		//获取登录信息
		String userId = WebContext.getSessionUserId();
		//获取角色信息
		String role = WebContext.getSessionUser().getUserKbn();
		//判断角色
		if("C".equals(role)) {
			//学生角色
			StudentClassInfoEntity entity =new StudentClassInfoEntity();
			entity.setStudentId(userId);
			List<StudentClassInfoEntity> stuList = studentClassInfoService.searchStudentClassInfoList(entity);
			stuList.get(0).setUserId(userId);
			//设置返回值数据
			result.setData(stuList.get(0));
		}else {
			//教师信息
			TpUserEntity tpUserEntity = userManagementService.searchById(userId);
			result.setData(tpUserEntity);
		}
		 
		return result;
	}
	*//**
	 * 结束课程方法
	 * @param id 课程id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/endCourse.action")
	@ResponseBody
	public ResultBean endCourse(String id) throws Exception {
		courseService.endCourse(id);
		return ResultBean.success(MSG_S_SUBMIT);
	}
	*//**
	 * 图书馆课程复制
	 * @param id 课程id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/toLibCourse.action")
	@ResponseBody
	public ResultBean toLibCourse(String id) throws Exception {
		courseService.toLibCourse(id);
		return ResultBean.success(MSG_S_SUBMIT);
	}
	*//**
	 * 课程复制方法
	 * @param id 课程主键
	 * @return
	 *//*
	@RequestMapping("/copyInfo.action")
    @ResponseBody
    public ResultBean copyInfo(String id)  {
        ResultBean result = new ResultBean();
        //课程复制实现方法
        courseService.copyCourseByHtml(id);
        result.setMessages(getMessage(MSG_S_SAVE));
        return result;
    }
    *//**
                 * 新增课程活动记录方法
     * @param courseId 课程id
     * @return
     *//*
	@RequestMapping("/addActiveInit.action")
	public ModelAndView addActiveInit(String courseId) {
	//课程活动新增页面路径
	ModelAndView result = new ModelAndView("course/manager/course_active_add");
	TagView tagQuery = new TagView();
	//一级分类对象
	List<TagView> tags = courseService.searchTagList2(tagQuery);
	//二级分类对象
	List<TagView> tagsSecond = courseService.searchTagSecondList2(tagQuery);
	//查询章节信息
	ChapterEntity entity = new ChapterEntity();
	entity.setCourseId(courseId);
	List<ChapterEntity> chapterList = courseService.selectByCourseId(entity);
	//返回前台对象
	result.addObject("catagoryTags", chapterList);
	//result.addObject("catagoryTags", tags);
	result.addObject("pageType", "add");
	result.addObject("course", new CourseManagerView());
	result.addObject("tagsSecond", tagsSecond);
	result = getRoleAndNum(result);
	return result;
	}
    *//**
                  * 保存课程章节活动记录信息
     * @param courseJson 课程信息
     * @return 返回结果
     * @throws Exception 
     *//*
    @RequestMapping("/saveActiveChapterInfo.action")
    @ResponseBody
    public ResultBean saveActiveChapterInfo(String courseJson) throws Exception {
    	//实例化对象
        CourseManagerView courseview = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, CourseManagerView.class);
    	ChapterActiveEntity course = new ChapterActiveEntity();
    	course.setChapterId(courseview.getChapterId());
    	course.setActiveFlag(courseview.getActiveFlag());
    	//根据章节id查询章节信息
    	ChapterEntity view= new ChapterEntity();
    	String chapterId = courseview.getChapterId();
    	view = chapterService.selectChapteById(chapterId);
    	//设置章节字段进行保存
    	course.setChapterTitle(view.getTitle());
    	course.setContent(courseview.getContent());
        ResultBean result = ResultBean.success();
        //保存章节活动内容
        chapterActiveService.insertInfo(course);
        return result;
    }
   *//**
             * 查询课程章节阶段活动记录信息
    * @param activeFlag
    * @param chapterId
    * @return
    *//*
	@RequestMapping("/searchActiveChapterInfo.action")
	@ResponseBody
	public ResultBean searchActiveChapterInfo(String activeFlag,String chapterId){
	ChapterActiveEntity course = new ChapterActiveEntity();
	//判断活动记录标识是否为空
	if(StringUtil.isNotBlank(activeFlag)) {
		course.setActiveFlag(activeFlag);
	}
	course.setChapterId(chapterId);
	//查询该章节的全部活动记录
	List<ChapterActiveEntity> view = chapterActiveService.searchList(course);
	ResultBean result = ResultBean.success();
	result.setData(view);
	return result;
	}
	*//**
	 * 查询课程阶段活动记录信息
	 * @param activeFlag 活动记录阶段标识
	 * @param courseId  课程主键
	 * @param chapterId  章节主键
	 * @return
	 *//*
	@RequestMapping("/searchActiveAllChapterInfo.action")
	@ResponseBody
	public ResultBean searchActiveAllChapterInfo(String activeFlag,String courseId,String chapterId){
	ChapterActiveEntity course = new ChapterActiveEntity();
	//判断活动记录标识是否为空
	if(StringUtil.isNotBlank(activeFlag)) {
		course.setActiveFlag(activeFlag);
	}
	course.setCourseId(courseId);
	if(StringUtil.isNotBlank(chapterId)) {
		course.setChapterId(chapterId);
	}
	//返回查询结果
	return chapterActiveService.searchAllCourseChapterPage(course);
	}
	*//**
	 * 查询角色的公共方法
	 * @param result
	 * @return
	 *//*
	private ModelAndView getRoleAndNum(ModelAndView result) {
    	int num = courseService.getStuStatus();
    	result.addObject("num", num);
    	UserEntity user = WebContext.getSessionUser();
    	//判断是否管理员
    	ManagerUserEntity entity = managerUserService.searchByUserId(user.getUserId());
    	if(!user.getUserName().equals("guest")) {
    		List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
    		//老师和教育局角色
    		if(roleList.size() > 0 && "A".equals(user.getUserKbn()) && null==entity && !"2542668".equals(user.getUserId())) {
    			for(UserRoleEntity role : roleList) {
    				if(role.getRoleId().equals("01")) {
    					result.addObject("role", "t");
    				}
    			}
    		//教育局角色	
    		}else if(roleList.size() > 0  && (null!=entity || "2542668".equals(user.getUserId()))){
    			result.addObject("role", "m");
    		}else if("B".equals(user.getUserKbn())){
    			result.addObject("role", "e");
    	    //家长和教研员角色
    		}else if("D".equals(user.getUserKbn()) || "E".equals(user.getUserKbn())){
    			result.addObject("role", "q");
    		//学生角色
    		}else {
    			result.addObject("role", "s");
    		}
    	}
        //管理员角色
    	if(null!=entity || "2542668".equals(user.getUserId())) {
    		result.addObject("manager", "1");
    	}else {
    		result.addObject("manager", "0");
    	}
    	return result;
    }
    *//**
     * 活动记录编辑页面方法
     * @param courseId  课程id
     * @param activeId  活动id
     * @return
     *//*
	@RequestMapping("/editActiveInit.action")
	public ModelAndView editActiveInit(String courseId,String activeId) {
	//活动编辑页面路径
	ModelAndView result = new ModelAndView("course/manager/course_active_editor");
	TagView tagQuery = new TagView();
	//查询一级分类方法
	List<TagView> tags = courseService.searchTagList2(tagQuery);
	//查询e二级分类方法
	List<TagView> tagsSecond = courseService.searchTagSecondList2(tagQuery);
	//根据课程id查询所有对应的章节信息
	ChapterEntity entity = new ChapterEntity();
	entity.setCourseId(courseId);
	List<ChapterEntity> chapterList = courseService.selectByCourseId(entity);
	//根据活动记录id查询活动章节信息
	ChapterActiveEntity active = new ChapterActiveEntity();
	active.setId(activeId);
	List<ChapterActiveEntity> activeList = chapterActiveService.searchList(active);
	//设置返回前台的字段信息
	CourseManagerView courseManagerView = new  CourseManagerView();
	courseManagerView.setChapterId(activeList.get(0).getChapterId());
	courseManagerView.setChapterTitle(activeList.get(0).getChapterTitle());
	courseManagerView.setActiveFlag(activeList.get(0).getActiveFlag());
	courseManagerView.setContent(activeList.get(0).getContent());
	//章节对象
	result.addObject("catagoryTags", chapterList);
	//result.addObject("catagoryTags", tags);
	result.addObject("pageType", "add");
	result.addObject("activeId", activeId);
	//课程对象
	result.addObject("course",courseManagerView);
	result.addObject("tagsSecond", tagsSecond);
	result = getRoleAndNum(result);
	return result;
	}
    *//**
                * 保存活动记录课程章节信息
	* @param courseJson 课程信息
	* @return 返回结果
	* @throws Exception 
	*//*
	@RequestMapping("/updateActiveChapterInfo.action")
	@ResponseBody
	public ResultBean updateActiveChapterInfo(String courseJson) throws Exception {
    //转化实例对象方法
	CourseManagerView courseview = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, CourseManagerView.class);
	ChapterActiveEntity course = new ChapterActiveEntity();
	//删除当前活动记录
	ChapterActiveEntity entity = new ChapterActiveEntity();
	String id = courseview.getActiveId();
	entity.setId(id);
	chapterActiveService.deleteInfo(entity);
	//设置保存课程的信息字段
	course.setChapterId(courseview.getChapterId());
	course.setActiveFlag(courseview.getActiveFlag());
	//查询章节信息
	ChapterEntity view= new ChapterEntity();
	String chapterId = courseview.getChapterId();
	view = chapterService.selectChapteById(chapterId);
	course.setChapterTitle(view.getTitle());
	course.setContent(courseview.getContent());
	ResultBean result = ResultBean.success();
	//保存活动记录的课程章节信息
	chapterActiveService.insertInfo(course);
	return result;
	}
    *//**
                 * 删除活动
	* @param courseJson 课程信息
	* @return 返回结果
	* @throws Exception 
	*//*
	@RequestMapping("/activeDelete.action")
	@ResponseBody
	public ResultBean activeDelete(String id) throws Exception {
	ChapterActiveEntity entity = new ChapterActiveEntity();
	//设置活动记录主键
	entity.setId(id);
	chapterActiveService.deleteInfo(entity);
	ResultBean result = ResultBean.success();
	return result;
	}
	*//**
	 * 添加页面初始化：节
	 * 
	 * @param courseId 课程ID
	 * @return
	 *//*
	@RequestMapping("/courseSmallInit.action")
	public ModelAndView courseSmallInit(String courseId, String parentId) {
		ModelAndView result = new ModelAndView("course/manager/course_three_edit");
		ChapterView chapter = new ChapterView();
		chapter.setMeasureType("4");
		result.addObject("courseId", courseId);
		result.addObject("parentId", parentId);
		result.addObject("chapterFlag", SMALL_CHAPTER);
		result.addObject("chapter", chapter);
		return result;
	}
	*//**
	 * 保存章信息
	 * 
	 * @param chapterJson
	 * @return 返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/saveExamInfo.action")
	@ResponseBody
	public ResultBean saveExamInfo(String chapterJson, String videoLength) throws Exception {
		//将字符串转化成章节对象
		ChapterView chapter = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateMinuteFormatDefualt()),
				chapterJson, ChapterView.class);
		ResultBean result = ResultBean.success();
		//设置章节状态0未发布，1已发布
		chapter.setStatus("1");
		//设置加入课程1是已加入
		chapter.setIsCourse("1");
		if (StringUtil.isEmpty(chapter.getId())) {
			//未存在的章节考试直接保存
			chapterService.insertChapterInfoAll(chapter, videoLength);
		} else {
			//已存在的章节考试更新信息
			chapterService.updateChapterAll(chapter, videoLength);
		}
		//返回信息
		return result;
	}
    *//**
     * 
                  * 查看考试结果
     * @param condition
     * @return
     *//*
    @SuppressWarnings({ "unchecked"})
    @RequestMapping("/userExamInfoInit.action")
    public ModelAndView userExamInfoInit(MeasurementEntity condition) {
        ModelAndView result = new ModelAndView("course/manager/user_exam_info");
        //参数为空抛出异常
        if(StringUtil.isEmpty(condition.getLinkId())){
            throw new BusinessException("参数错误!");
        }
        //根据参数查询考试结果
        List<MeasurementEntity> list = courseService.search(condition);
        if(list != null && list.size() != 0) {
            result.addObject("condition", list.get(0));
        } else {
            throw new BusinessException("参数错误!");
        }
        //返回考试信息
        return result;
    }
    *//**
                 * 根据课程id获取章节考试状态信息
     * @param courseId
     * @return
     * @throws Exception
     *//*
	@RequestMapping("/searchExamStatus.action")
	@ResponseBody
	public ChapterView searchExamStatus(String courseId) throws Exception {
		//查询章节信息
		ChapterView view = chapterService.seacrhExamStatus(courseId);
		return view;
	}
	@SuppressWarnings("unchecked")
	*//**
	* 前端请求-章节详情
	* @param chapterId 章节ID
	* @return 
	*//*
	@RequestMapping("/toChapterDetail.action")
	public ModelAndView toChapterDetail(String courseId,String chapterId){
		ModelAndView mv = new ModelAndView("course/client/course_chapter_detail");
		ResultBean result = courseService.searchCourseDetail(courseId);
		CourseView courseView = (CourseView) result.getData();
		mv.addObject("courseView", courseView);
		mv.addObject("note", new NoteDataEntity());
		mv.addObject("homeWork", new HomeworkView());
		result = chapterService.chapterDetail(courseId, chapterId);
		ChapterView chapterView = (ChapterView) result.getData();
		ChapterEntity entity = new ChapterEntity();
		entity.setCourseId(courseId);
		List<ChapterEntity> chapterCourseList = courseService.selectByCourseId(entity);
		mv.addObject("chapterCourseList", chapterCourseList);
		mv.addObject("chapterView", chapterView);
		//检索章节列表
		List<ChapterView> cvs = (List<ChapterView>) chapterService.chapterList(courseId);
		mv.addObject("chapterList", cvs);
		//相关课程
		result = courseService.recommendedCourses("5", courseId);
		List<CourseEntity> otherCourseList = (List<CourseEntity>) result.getData();
		mv.addObject("otherCourseList", otherCourseList);
		//判断是否需要检索测试信息
		if(CHAPTER_TYPE_MEASUERMENT.equals(chapterView.getMeasureType().toString())){
			result = measuermentService.searchMeasuerment(chapterId, LINK_TYPE_CHAPTER);
			List<QuestionEntity> questionList = (List<QuestionEntity>) result.getData();
			mv.addObject("questionList", questionList);
		}
		//判断是否考试章节
		if(CHAPTER_TYPE_EXAM.equals(chapterView.getMeasureType())){
			//考试章节
			//取用户对于章节的考试信息
			MeasurementEntity me = new MeasurementEntity();
			me.setLinkId(chapterId);
			me.setLinkType(LINK_TYPE_CHAPTER);
			me = measuermentService.findMeasurement(me);
			if(me!=null){
				UserMeasurementResultEntity um = new UserMeasurementResultEntity();
				um.setMeasurementId(me.getId());
				um.setUserId(WebContext.getSessionUserId());
				um = measuermentService.findUserExamResult(um);
				if(um != null && um.getExamStatus()!=null && um.getExamStatus().equals(EXAM_STATUS_END)){
					//已参加过考试提交了
					mv.addObject("goal", um.getGoal()+"");
					mv.addObject("totalGoal", um.getTotalGoal()+"");
					mv.addObject("examTime", DateUtil.formatAny(um.getCreateDate(), ConfigUtil.getDateTimeFormatDefualt()));
				}
			}
		}
		//是否教师
		mv.addObject("isTeacher", courseService.isTeacher(courseView.getTeacherId()));
		mv.addObject("useingChapterId", chapterId);
		String content = chapterView.getContent();
		if(content != null && !"".equals(content)) {
			String ipAndPort = getIpAndPort(content);
			if(!"".equals(ipAndPort)) {
				String serverName = WebContext.getRequest().getServerName();
				int serverPort = WebContext.getRequest().getServerPort();
				content = content.replace(ipAndPort, serverName+":"+serverPort);
				chapterView.setContent(content);
			}
		}
		
		return mv;
	}
	*//**
	 * 判断str中是否有IP和port，若有则取出返回
	 * @param str
	 * @return String
	 *//*
	private static String getIpAndPort(String str) {
		String returnStr = "";
		Pattern p = Pattern.compile("(\\d+\\.\\d+\\.\\d+\\.\\d+)\\:(\\d+)");
		Matcher m = p.matcher(str);
		if(m.find()) {
			returnStr += m.group(1)+":"+m.group(2);
		}
		
		return returnStr;
	}
	*//**
	 * 标签一览
	 * @param flag 是否显示标签 0 不显示 1 显示
	 * @param businessType 业务类型
	 * @return
	 *//*
	@RequestMapping("/searchSchhoolCategory.action")
	@ResponseBody
	public ResultBean searchSchhoolCategory(String flag,String businessType){
		ResultBean  result=tagService.searchSchhoolCategory(flag,businessType);
		return result;
	}
	*//**
	 * 
	 * 初始化待办课程列表
	 * 
	 * @param condition 课程对象
	 * @return 返回课程列表初始化页面
	 *//*
	@RequestMapping("/institutionDbListInit.action")
	public ModelAndView institutionDbListInit(InstitutionMessageEntity condition) {
		//设置代办页面
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_db_list");
		//设置分页参数
		condition.setPageSize(10);
		result.addObject("condition", condition);
		TagInstitutionView tagQuery = new TagInstitutionView();
		//一级标签信息查询
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		//二级标签信息查询
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		//返回课程实例对象
		result.addObject("course", new InstitutionMessageEntity());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}
	*//**
	 * 
	 * 初始化已办课程列表
	 * 
	 * @param condition 课程对象
	 * @return 返回课程列表初始化页面
	 *//*
	@RequestMapping("/institutionYbListInit.action")
	public ModelAndView institutionYbListInit(InstitutionMessageEntity condition) {
		//设置已办页面
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_yb_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		TagInstitutionView tagQuery = new TagInstitutionView();
		//一级标签信息查询
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		//二级标签信息查询
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		//返回课程实例对象
		result.addObject("course", new InstitutionMessageEntity());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}
	*//**
	 * 
	 * 检索评价列表
	 * 
	 * @param queryInfo 机构对象
	 * @return 返回评价列表
	 *//*
	@RequestMapping("/listdbSearch.action")
	@ResponseBody
	public ResultBean listdbSearch(MeasurementInstitutionEntity queryInfo) {
		UserEntity user = WebContext.getSessionUser();	
		//获取人员角色
		String role = user.getUserKbn();
		//获取登录人账号
		String userId = user.getCreateUser();
		//教师角色
		if("A".equals(role)) {
			TpUserEntity tpUserEntity =new TpUserEntity();
			//查询教师信息
			tpUserEntity = userManagementService.searchById(userId);
		    if(null!=tpUserEntity) {
		    	queryInfo.setInstitutionName(tpUserEntity.getSchoolName());
		    }
		    //学生角色
		}else if("C".equals(role)) {
			//查询学生信息
			StudentClassInfoEntity classInfo = new StudentClassInfoEntity();
			classInfo.setStudentId(userId);
			classInfo = studentClassInfoService.searchStudentClassInfo(classInfo);
			if(null!=classInfo && StringUtil.isNotBlank(classInfo.getSchoolName())) {
				queryInfo.setInstitutionName(classInfo.getSchoolName());
			}
		}
		
		return taxonomyInstitutionService.listdbSearch(queryInfo);
	}
	*//**
	 * 
	 * 检索已办评价列表
	 * 
	 * @param queryInfo 机构对象
	 * @return 返回评价列表
	 *//*
	@RequestMapping("/listybSearch.action")
	@ResponseBody
	public ResultBean listybSearch(MeasurementInstitutionEntity queryInfo) {
		UserEntity user = WebContext.getSessionUser();	
		//获取人员角色
		String role = user.getUserKbn();
		//登录账号
		String userId = user.getCreateUser();
		//教师角色
	    if("A".equals(role)) {
	    	//查询用户信息
	    	TpUserEntity tpUserEntity =new TpUserEntity();
			tpUserEntity = userManagementService.searchById(userId);
			if(null!=tpUserEntity) {
				//设置学校名称
				queryInfo.setInstitutionName(tpUserEntity.getSchoolName());
			}
	    }else if("C".equals(role)) {
	    	//查询学生信息
			StudentClassInfoEntity classInfo = new StudentClassInfoEntity();
			classInfo.setStudentId(userId);
			classInfo = studentClassInfoService.searchStudentClassInfo(classInfo);
			if(null!=classInfo && StringUtil.isNotBlank(classInfo.getSchoolName())) {
				//设置学生学校名称
				queryInfo.setInstitutionName(classInfo.getSchoolName());
			}
		}
	    //返回查询结果列表
		return taxonomyInstitutionService.listybSearch(queryInfo);
	}
	*//**
	 * 管理人员基本信息页面
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/managerUserList.action")
	public ModelAndView managerUserList(ManagerUserEntity condition) {
		//管理员信息页面展示路径
		ModelAndView result = new ModelAndView("/baseinstitution/manager/base_manager_user_list");
		//设置每页展示10条数据
		condition.setPageSize(10);
        result.addObject("course", new CourseManagerView());
		result.addObject("condition", condition);
		result = getRoleAndNum(result);
		//返回页面所需信息结果
		return result;
	}
	*//**
	 * 管理者信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchManagerUserList.action")
	@ResponseBody
	public ResultBean searchManagerUserList(ManagerUserEntity entity) {
		//查询管理员数据表全部信息
		return managerUserService.searchPage(entity);
	}
	*//**
	 * 搜素全部人员
	 * 
	 * @param query
	 * @return
	 *//*
	@RequestMapping("/searchUserAll.action")
	@ResponseBody
	public ResultBean searchUserAll(TpUserEntity query) {
		if(null!=query && StringUtil.isNotBlank(query.getUsrName())) {
			query.setUsrName(query.getUsrName().trim());
		}
		//根据用户名查询人员信息表数据
		return userManagementService.searchManagerUserAll(query);
	}
	*//**
	 * 保存超级管理员信息
	 * @param userId 添加人员账号id
	 * @param userName  人员名称
	 * @param personName 名称
	 * @param role 角色
	 * @return
	 *//*
	@RequestMapping("/saveManagerUser.action")
	@ResponseBody
	public ResultBean saveManagerUser(String userId,String userName,String personName,String role) {
		ResultBean result = new ResultBean();
		//查询管理员表中是否已存在该成员
		ManagerUserEntity entity = managerUserService.searchByUserId(userId);
		//存在则返回
		if(null!=entity) {
			result = ResultBean.error();
		}else {
			//不存在则保存到数据表里
			ManagerUserEntity managerUserEntity = new ManagerUserEntity();
			managerUserEntity.setUserId(userId);
			managerUserEntity.setUserName(userName);
			managerUserEntity.setPersonName(personName);
			managerUserEntity.setRole(role);
			managerUserEntity.setCreateDate(new Date());
			//设置该条信息的创建人
			managerUserEntity.setCreateUser(WebContext.getSessionUserId());
			managerUserService.insert(managerUserEntity);
			//保存成功返回信息
			result = ResultBean.success();
		}
		
		return result;
	}
	*//**
	 * 删除管理员
	 * @param id 员工账号
	 * @return  返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/deleteManagerById.action")
	@ResponseBody
	public ResultBean deleteManagerById(String id) throws Exception {
		//根据员工账号在tb_manager_user表里删除其信息
		ManagerUserEntity entity = new ManagerUserEntity();
		entity.setUserId(id);
		managerUserService.delete(entity);
		return ResultBean.success(DELETE_GRADE_SUCCESS);
	}
	*//**
	 * 查询管理信息
	 * 
	 * @param 员工账号
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchByUserId.action")
	@ResponseBody
	public ManagerUserEntity searchByUserId(String id) throws Exception {
		//返回查询结果
		return managerUserService.searchByUserId(id);
	}
	*//**
	 *  作业列表页面
	 * @param homeworkView 入参
	 * @return 返回结果
	 *//*
	@RequestMapping("/zuoyeManager.action")
	public ModelAndView zuoyeManager(HomeworkView homeworkView) {
		//列表页面路径
		ModelAndView result = new ModelAndView("/course/manager/homework_list");
		//返回作业信息对象给前台
		result.addObject("homeworkView", homeworkView);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 *  查询作业列表结果方法
	 * @param homeworkView 前台传入后台参数
	 * @return  返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/zuoyelistSearch.action")
	@ResponseBody
	public ResultBean zuoyelistSearch(HomeworkView homeworkView) throws Exception {
		ResultBean result =new ResultBean();
		//获取登录信息对象
		UserEntity user = WebContext.getSessionUser();
    	//判断是否管理员
    	ManagerUserEntity entity = managerUserService.searchByUserId(user.getUserId());
    	if(null!=entity) {
    		//管理员返回的结果
    		result= courseVRService.seacherHomeworkPage(homeworkView);
    	}else {
    		//普通教师查询返回的结果
    		homeworkView.setTeacherId(user.getUserId());
    		result= courseVRService.seacherHomeworkPage(homeworkView);
    	}
		return result;
	}
	*//**
	 *  根据作业id查询作业列表
	 * @param id 作业主键
	 * @param title  作业标题
	 * @return  返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/zuoyelistById.action")
	public ModelAndView zuoyelistById(String id,String title) throws Exception {
		//作业列表路径
		ModelAndView result = new ModelAndView("/course/manager/homework_user_list");
		HomeworkView homeworkView = new HomeworkView();
		homeworkView.setId(id);
		homeworkView.setTitle(title);
		//作业实例化对象
		result.addObject("homeworkView", homeworkView);
		//作业主键
		result.addObject("id", id);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 预览作业页面方法
	 * @param id  作业主键
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/homeWorkById.action")
	public ModelAndView homeWorkById(String id) throws Exception {
		//预览页面路径
		ModelAndView result = new ModelAndView("/course/client/home_work_teacher_review");
		HomeworkView view = courseService.getHomeWorkById(id);
		//设置创建时间返回前台
		if(null!=view.getCreateDate()) {
			view.setCreateTime(sdf.format(view.getCreateDate()));
		}
        //返回作业实体类对象
		result.addObject("homeWork", view);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 查询作业列表
	 * @param id 作业id集合字符串
	 * @param homeworkView  作业实体类
	 * @param linkId 课程主键
	 * @param linkType  课程/章节类型
	 * @return
	 *//*
	@RequestMapping("/zuoyelistByIdSearch.action")
	@ResponseBody
	public ResultBean zuoyelistByIdSearch(String id,HomeworkView homeworkView,String linkId,String linkType) {
		// 将前台汇总的作业id拆分
		if(StringUtil.isNotBlank(id)) {
			String ids[]=id.split(",");
			homeworkView.setId(ids[0]);
		}else {
			homeworkView.setLinkId(linkId);
			homeworkView.setLinkType(linkType);
			HomeworkEntity homeWork = courseService.getHomeWorkByLinkId(homeworkView);
			if(null!=homeWork) {
				//获取作业主键
				homeworkView.setId(homeWork.getId());
				//获取作业标题
				homeworkView.setTitle(homeWork.getTitle());
			}
		}
		//查询作业信息
		return courseVRService.seacherHomeworkUserPage(homeworkView);
	}
    *//**
                 * 保存作业信息
     * @param courseJson 作业信息
     * @return 返回结果
     * @throws Exception 
     *//*
    @RequestMapping("/saveZyInfo.action")
    @ResponseBody
    public ResultBean saveZyInfo(String courseJson) throws Exception {
    	//将字符串转化成实例化对象
    	HomeworkView course = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, HomeworkView.class);
        ResultBean result = ResultBean.success();
        String homeworkId=course.getId();
        //获取账号id
        String userId = WebContext.getSessionUserId();
        //查询作业数据表
        HomeworkAnswerEntity entity = new  HomeworkAnswerEntity();
        entity.setHomeworkId(homeworkId);
        entity.setCreateUser(userId);
        entity = courseVRService.selectOneAnswer(entity);
        if(null==entity) {
        	//作业作答数据表信息为空，插入数据信息
        	course.setId(IDGenerator.genUID());
        	course.setCreateUser(userId);
        	course.setCreateDate(new Date());
        	//设置学生提交的信息
        	course.setStudentComment(course.getContent());
        	course.setHomeworkId(homeworkId);
        	course.setAnswerUser(userId);
        	//设置作业状态
        	course.setStatus(HOMEWORK_ANSWER_STATUS_ONE);
        	courseVRService.insertAnswer(course);
        }else {
        	//作业作答数据表信息不为空，更新数据信息
        	entity.setStudentComment(course.getContent());
        	entity.setUpdateDate(new Date());
        	entity.setUpdateUser(userId);
        	courseVRService.update(entity);
        }
        return result;
    }
	*//**
	 * 学生作业列表页面
	 * @param HomeworkView homeworkView 
	 * @return
	 *//*
	@RequestMapping("/studentZuoyePage.action")
	public ModelAndView studentZuoyePage(HomeworkView homeworkView) {
		//学生作业列表页面路径
		ModelAndView result = new ModelAndView("/course/manager/homework_student_list");
		//作业实例对象实体类
		result.addObject("homeworkView", homeworkView);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 查询作业列表
	 * 
	 * @param HomeworkView homeworkView 入参
	 * @return 返回结果
	 *//*
	@RequestMapping("/studentZuoyelistSearch.action")
	@ResponseBody
	public ResultBean studentZuoyelistSearch(HomeworkView homeworkView) {
		homeworkView.setCreateUser(WebContext.getSessionUserId());
		//根据账号id获取作业对象信息
		return courseVRService.seacherStudentHomeworkUserPage(homeworkView);
	}
    *//**
                   * 编辑作业
     * @param HomeworkView home 作业实例对象入参 
     * @return
     *//*
    @RequestMapping("/studentTaskInit.action")
    public ModelAndView studentTaskInit(HomeworkView home){
    	//作业编辑页面
        ModelAndView result = new ModelAndView("course/manager/home_student_work");
        //获取当前登录者账号
        UserEntity user = WebContext.getSessionUser();
        home.setCreateUser(user.getUserId());
        HomeworkView homework=courseVRService.searchStudentHomeWork(home);
        //前台页面信息回显对象
        result.addObject("homework", homework);
        result = getRoleAndNum(result);
        return result;
    }
    *//**
                  * 保存作业信息
     * @param id  作业id
     * @param studentComment 学生提交内容
     * @param homeanwserId 作业作答id
     * @param status 提交作业状态
     * @return
     * @throws Exception
     *//*
    @RequestMapping("/saveMystudy.action")
    @ResponseBody
    public ResultBean saveMystudy(String id,String studentComment,String homeanwserId,String status) throws Exception {
        ResultBean result = ResultBean.success();
        String userId = WebContext.getSessionUserId();
        //根据homeanwserId判断当前登陆者是否已经作答过改作业
        if(StringUtil.isBlank(homeanwserId)) {
        	//未作答，保存作答内容
        	HomeworkView course = new HomeworkView();
        	//随机生成作业作答id
        	course.setId(IDGenerator.genUID());
        	course.setCreateUser(userId);
        	course.setCreateDate(new Date());
        	//作业作答内容
        	course.setStudentComment(studentComment);
        	course.setHomeworkId(id);
        	course.setAnswerUser(userId);
        	//设置作业状态
        	course.setStatus(HOMEWORK_ANSWER_STATUS_ONE);
        	courseVRService.insertAnswer(course);
        }else {
        	//更新做大内容
        	HomeworkAnswerEntity entity = new HomeworkAnswerEntity();
        	entity.setId(homeanwserId);
        	entity = courseVRService.selectOneAnswer(entity);
        	entity.setStudentComment(studentComment);
        	entity.setUpdateDate(new Date());
        	entity.setUpdateUser(userId);
        	courseVRService.update(entity);
        }
        return result;
    }
    *//**
                 * 学生查看作业列表方法
     * @param home
     * @return
     *//*
    @RequestMapping("/studentSeacherTaskInit.action")
    public ModelAndView studentSeacherTaskInit(HomeworkView home){
    	//学生作业列表页面路径
        ModelAndView result = new ModelAndView("course/manager/home_student_look_work");
        //根据登录id查询作业信息
        UserEntity user = WebContext.getSessionUser();
        home.setCreateUser(user.getUserId());
        HomeworkView homework=courseVRService.searchStudentHomeWork(home);
        //作业信息返回给前台
        result.addObject("homework", homework);
        result = getRoleAndNum(result);
        return result;
    }
    *//**
                  * 查询年级 课程
     * @param id 课程id
     * @return
     *//*
    @RequestMapping("/searchGrade.action")
    @ResponseBody
    public List<StudentClassInfoEntity> searchGrade(String id){
    	//根据课程id查询课程信息
        CourseEntity entity = new CourseEntity();
        entity.setId(id);
        entity = courseService.searchCourse(entity);
        //获取老师所属学校
        String schoolId = entity.getSchoolId();
        StudentClassInfoEntity studentClassInfoEntity = new StudentClassInfoEntity();
        studentClassInfoEntity.setSchoolId(schoolId);
        //根据学校查询年级
        List<StudentClassInfoEntity> gradeList = new ArrayList<StudentClassInfoEntity>();
        gradeList = studentClassInfoService.searchStudentGradeList(studentClassInfoEntity);
        //返回查询的年级集合给前台
        return gradeList;
    }
     *//**
                     * 根据课程主键和年级主键查询班级信息
      * @param courseId  课程主键
      * @param gradeId 年级主键
      * @return
      *//*
    @RequestMapping("/searchClass.action")
    @ResponseBody
    public List<StudentClassInfoEntity> searchClass(String courseId,String gradeId){
    	//根据课程主键查询课程信息
    	 CourseEntity entity = new CourseEntity();
         entity.setId(courseId);
         entity = courseService.searchCourse(entity);
         //获取老师所属学校
         String schoolId = entity.getSchoolId();
        List<StudentClassInfoEntity> gradeList = new ArrayList<StudentClassInfoEntity>();
        //根据学校主键和年级查询班级信息
        StudentClassInfoEntity studentClassInfoEntity = new StudentClassInfoEntity();
        studentClassInfoEntity.setSchoolId(schoolId);//学校id
        studentClassInfoEntity.setGradeId(gradeId);//年级id
        gradeList = studentClassInfoService.searchStudentClassList(studentClassInfoEntity);
        return gradeList;
    }
    *//**
                  * 添加学生时查询学生列表
     * @param UserView query 用户参数 
     * @param courseId 所属课程信息id
     * @return
     *//*
    @RequestMapping("/searchStudentUserAll.action")
    @ResponseBody
    public ResultBean searchStudentUserAll(UserView query,String courseId)  {
    	//根据课程id查询课程所属老师对应的学校信息
    	CourseEntity entity = new CourseEntity();
        entity.setId(courseId);
        entity = courseService.searchCourse(entity);
        //获取老师所属学校
        String schoolId = entity.getSchoolId();
        //根据学校id获取这个学校下面所有的学生信息
        query.setSchoolId(schoolId);
        return courseService.searchStudentUserAll(query);
    }
	*//**
	 * 用户个人中心信息页面方法
	 * @return 返回结果
	 *//*
	@RequestMapping("/userPblInfo.action")
	public ModelAndView userPblInfo(){
		//查询代办信息表里审核通过的代办信息数量
		int num = courseService.getStuStatus();
		//个人中心页面路径参数
		ModelAndView mv = new ModelAndView("trainingplatform/client/center/center_pbl_user");
		//根据用户id查询用户信息
		UserEntity user = userManagementService.seachUserInfo(WebContext.getSessionUserId());
		//返回用户对象
		mv.addObject("user", user);
		mv.addObject("num", num);
		//获取登录人员的角色信息
		UserEntity userRole = WebContext.getSessionUser();
		//判断用户是否登录状态guest为未登录状态
		if(!userRole.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(userRole.getUserId());
			if(roleList.size() > 0) {
				//循环遍历人员角色
				for(UserRoleEntity role : roleList) {
					//角色包含01的设置role字段为t,即老师角色
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				//s为学生角色
				mv.addObject("role", "s");
			}
		}
		UserEntity user1 = WebContext.getSessionUser();
		//获取工号
		String uid =user1.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		    //获取人员信息
		        //设置头像字段
				String image = "";
				//设置头像字段
				String school="";
				//设置年级字段
				String grade="";
				//设置班级字段
				String className="";
				if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
					//获取头像
					image = userEntity.getHeadurl();
					//获取学校
					school = userEntity.getSchoolName();
				}else {
					//设置默认头像
					image ="http://wisdomsaas-img.oss-cn-beijing.aliyuncs.com/BFAA0FECDD194B7A974C60B7E1B27C18?authorization=bce-auth-v1%2F7642b4b48046452697e90662eed41ace%2F2017-12-29T08%3A33%3A39Z%2F-1%2F%2F0f06024af14dad05694f25b8c85572567591c72bbd0129d3175b4cdbd6e62208";
				}
				//获取班级
				if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
					//实例化学生对象实体类并查询学生信息
					List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
					StudentClassInfoEntity entity = new StudentClassInfoEntity();
					entity.setStudentId(uid);
					entityList = studentClassInfoService.searchStudentClassInfoList(entity);
					//如果学生对象不为空则执行如下赋值
					if(null!=entityList && entityList.size()>0) {
						grade = entityList.get(0).getGradeName();
						className = entityList.get(0).getClassName();
						school =  entityList.get(0).getSchoolName();
					}
				}
				//返回班级信息
				mv.addObject("className", className);
				//向页面返回年级信息
				mv.addObject("grade", grade);
				//返回学生所属学校名称信息
				mv.addObject("school", school);
		       return mv;
	}
	*//**
	 * 批量点评初始化页面方法
	 * @param userIds  学生id集合字符串
	 * @param personNames  学生姓名字符集合
	 * @param id      作业id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/allEvalutionMsg.action")
	public ModelAndView allEvalutionMsg(String userIds,String personNames,String id) throws Exception {
		//批量点评方法
		ModelAndView result = new ModelAndView("/course/client/home_work_teacher_all_review");
		String ids[]=id.split(",");
		HomeworkView view = courseService.getHomeWorkById(ids[0]);
		//判断作业对象返回的时间是否为空，不为空则赋值给新的字段用于前端展示
		if(null!=view.getCreateDate()) {
			if(StringUtil.isNotBlank(sdf.format(view.getCreateDate()))) {
				view.setCreateTime(sdf.format(view.getCreateDate()));
			}
		}
		//收集作业对象
		result.addObject("homeWork", view);
		//收集作业id
		result.addObject("id", id);
		//收集学生账号集合id
		result.addObject("userIds", userIds);
		//收集学生姓名集合
		result.addObject("personNames", personNames);
		//收集作业答案id
		result.addObject("answerIds", id);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 批量点评保存
	 * @param courseJson 前台返回的json字符串
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveAllHomeWorkReviewById.action")
	@ResponseBody
	public ResultBean saveAllHomeWorkReviewById(String courseJson) throws Exception {
		//将字符串转化成实例化对象
		HomeworkView homework = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, HomeworkView.class);
		return courseService.saveAllHomeWorkReviewById(homework);
	}
	*//**
	 * 一键催交
	 * @param userIds  学生id集合字符串参数
	 * @param courseId 课程id
	 * @param homeworkId  作业id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveCjById.action")
	@ResponseBody
	public ResultBean saveCjById(String userIds,String courseId,String homeworkId) throws Exception {
		return courseService.saveACjById(userIds,courseId,homeworkId);
	}
	*//**
	 * 课程考试调用java调查网接口
	 * @param courseId  课程id
	 * @param userId  用户id
	 * @return
	 * @throws ParseException
	 *//*
	@RequestMapping("/searchExamByCourseIdList.action")
	@ResponseBody
	public ResultBean searchExamByCourseIdList(String courseId,String userId) throws ParseException {
		ResultBean result = new ResultBean();
		String param = courseId;
		//声明调用java调查网的webservice接口
		ExamToWebserviceProxy proxy = new ExamToWebserviceProxy();
		String out;
		try {
			//调用外部接口方法
			out = proxy.examList(param,1,20,userId);
			JSONArray jsonArray = new JSONArray(); 
			//将接口返回值转化成数组格式
			jsonArray = jsonArray.fromObject(out);
			UserCourseEntity uc = new UserCourseEntity();
			uc.setCourseId(courseId);
			uc.setUserId(WebContext.getSessionUserId());
			// 检索用户是否存在
			UserCourseEntity uu = courseService.searchOneCourseIdData(uc);
			//根据课程id获取该课程的老师id
			CourseEntity courseEntity = courseService.getCourseId(courseId);
			//转化对应的实体类
			@SuppressWarnings("unchecked")
			List<Questionnaire> list = jsonArray.toList(jsonArray, Questionnaire.class);
			if(null!=list && list.size()>0) {
				//循环遍历集合将字段赋值
				for(int i=0;i<list.size();i++) {
					//已加入的学员或者这个课程的老师创建者都可以看到这个
					if(null!=uu ||(null!=courseEntity && WebContext.getSessionUserId().equals(courseEntity.getTeacherId()))) {
						//已加入课程
						list.get(i).setStudentFlag("0");
						if (list.get(i).getEndDate() != null ) {
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Long end = sdf.parse(list.get(i).getEndDateStr()).getTime();
							Long nowtime = new Date().getTime();
							//判断问卷/考试是否已结束
								if(end-nowtime<0) {
									list.get(i).setExamStatusText("问卷/考试已结束");
									list.get(i).setExamStatus("1");	
								}					
							}
						if (list.get(i).getStartDate() != null ) {
							//将字符串转化成时间再获取秒数
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Long start = sdf.parse(list.get(i).getStartDateStr()).getTime();
							Long nowtime = new Date().getTime();
							//判断问卷/考试是否开始
								if(start-nowtime>0) {
									list.get(i).setExamStatusText("问卷/考试未开始");
									list.get(i).setExamStatus("1");
								}
							}
					}else {
						list.get(i).setStudentFlag("1");//未加入课程
						list.get(i).setExamStatusText("未加入课程");
					}
					
				}
			}
			 result.setData(list);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
		@Autowired
	private CourseService courseService;
	@Autowired
	private ChapterService chapterService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserManagementService userManagementService;
	@Autowired
	private TaxonomyInstitutionService taxonomyInstitutionService;
	@Autowired
	private TagInstitutionEvalutionService tagInstitutionEvalutionService;
	@Autowired
	private InstitutionService institutionService;
	@Autowired
	private TagInstitutionService tagInstitutionService;
	@Autowired
	private MeasuermentInstitutionService measurementInstitutionService;
	@Autowired
	private QuestionInstitutionService questionInstitutionService;
	@Autowired
	private MeasuermentService measuermentService;
	@Autowired
	private StudentClassInfoService studentClassInfoService;
	@Autowired
	private TeacherClassInfoService teacherClassInfoService;
	@Autowired
	private ChapterActiveService chapterActiveService;
	@Autowired
	private TagService tagService;
	@Autowired
	private ManagerUserService managerUserService;
	@Autowired
	private CourseVRService courseVRService;
	*//**
	 * 用户收藏课程
	 * 
	 * @author liuzhen
	 * @param courseId
	 * @return 返回结果
	 *//*
	@RequestMapping("/collectCourse.action")
	@ResponseBody
	public ResultBean collectCourse(String courseId) {
		return courseService.collectCourse(courseId);
	}

	*//**
	 * 用户取消收藏
	 * 
	 * @param courseId 课程Id
	 * @return
	 *//*
	@RequestMapping("/cancelCollectCourse.action")
	@ResponseBody
	public ResultBean cancelCollectCourse(String courseId) {
		return courseService.cancelCollectCourse(courseId);
	}

	*//**
	 * 二级标签查询
	 * 
	 * @param id 课程ID
	 * @return 课程下的二级标签列表
	 *//*
	@RequestMapping("/searchSecondTag2.action")
	@ResponseBody
	public List<TagView> searchSecondTag2(String id) {
		return courseService.searchSecondTagList2(id);
	}
	*//**
	 * 学校评价二级标签查询
	 * 
	 * @param id 课程ID
	 * @return 课程下的二级标签列表
	 *//*
	@RequestMapping("/searchInsitutionSecondTag2.action")
	@ResponseBody
	public List<TagInstitutionView> searchInsitutionSecondTag2(String id) {
		return courseService.searchInstitutionSecondTagList2(id);
	}
	@RequestMapping("/searchSecondTag.action")
	@ResponseBody
	public List<TagView> searchSecondTag(String id) {
		return courseService.searchSecondTagList(id);
	}

	*//**
	 * 体验课程/加入课程操作
	 * 
	 * @param courseId 课程ID
	 * @return 是否加入课程成功
	 * @author WUXIAOXIANG
	 *//*
	@RequestMapping("/joinCourse.action")
	@ResponseBody
	public ResultBean joinCourse(String courseId) {
		return courseService.joinCourse(courseId);
	}

	@RequestMapping("/applyJoinCourse.action")
	@ResponseBody
	public ResultBean applyJoinCourse(String courseId) {
		return courseService.applyJoinCourse(courseId);
	}
	
	*//**
	 * 课程推荐
	 * 
	 * @author FZ
	 * @return 返回结果
	 *//*
	@RequestMapping("/recommendedCourses.action")
	@ResponseBody
	public ResultBean recommendedCourses(String courseId, String displayNum) {

		return courseService.recommendedCourses(displayNum, courseId);
	}

	*//**
	 * 课程评价
	 * 
	 * @param linkId linkType
	 * @return 返回结果
	 * @author dengzhihao
	 *//*
	@RequestMapping("/courseEvaluationList.action")
	@ResponseBody
	public ResultBean courseEvaluationList(ScoreEntity scoreEntity) {

		return courseService.courseEvaluationList(scoreEntity);
	}

	*//**
	 * 对课程评价
	 * 
	 * @param scoreEntity     评价主题entity
	 * @param scoreDataEntity 评价数据entity
	 * @return 是否评价成功
	 * @author WUXIAOXIANG
	 *//*
	@RequestMapping("/evaluateCourse.action")
	@ResponseBody
	public ResultBean evaluateCourse(ScoreEntity scoreEntity, ScoreDataEntity scoreDataEntity) {
		// 插入评价信息
		return courseService.insertEvaluateCourse(scoreEntity, scoreDataEntity);
	}

	*//**
	 * 检索课程 包括分类
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 *//*
	@RequestMapping("/courseList.action")
	@ResponseBody
	public ResultBean courseList(CourseManagerView course, String directionTagId, String categoryTagId,
			String courseStage, String sortType, Integer pageSize, Integer pageNumber,String schoolName,String schoolNatrul,String tagsId) {
		if(StringUtil.isNotBlank(tagsId)) {
			course.setTagsId(tagsId);
		}
		if(StringUtil.isNotBlank(schoolNatrul)) {
			course.setSchoolNatrul(schoolNatrul);
		}
		ResultBean result = courseService.courseList(course, directionTagId, categoryTagId, courseStage, sortType,
				pageSize, pageNumber,schoolName);
		return result;
	}

	@RequestMapping("/coursePublicList.action")
	@ResponseBody
	public ResultBean coursePublicList(CourseManagerView course, Integer pageSize, Integer pageNumber) {
		course.setShareFlag(COURSE_SHARE_FLAG_THREE);
		course.setEndFlag(COURSE_END_STATUS_YES);
		UserEntity user = WebContext.getSessionUser();
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		//获取学校名称信息
		String school="";
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
			//获取学校
			school = userEntity.getSchoolName();
		}
		//获取班级
		if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
			List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
			StudentClassInfoEntity entity = new StudentClassInfoEntity();
			entity.setStudentId(uid);
			entityList = studentClassInfoService.searchStudentClassInfoList(entity);
			if(null!=entityList && entityList.size()>0) {
				school =  entityList.get(0).getSchoolName();
			}
		}
		course.setSchoolName(school);
//		if(StringUtil.isNotBlank(course.getDirectionTagId()) && StringUtil.isBlank(course.getCategoryTagId())) {
//			course.setTagId(course.getDirectionTagId());
//		}else {
//			course.setTagId(course.getCategoryTagId());
//			course.setParentTagId(course.getDirectionTagId());
//		}
		
		ResultBean result = courseService.coursePublicList(course, pageSize, pageNumber);
		return result;
	}

	@RequestMapping("/courseFactoryList.action")
	@ResponseBody
	public ResultBean courseFactoryList(CourseManagerView course, Integer pageSize, Integer pageNumber) {
		course.setShareFlag(COURSE_SHARE_FLAG_TWO);
		//获取学校名字
		//获取工号
		UserEntity user = WebContext.getSessionUser();
				String uid =user.getUserId();
				TpUserEntity userEntity = courseService.searchInfoMessage(uid);
				//获取学校名称信息
				String school="";
				if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
					//获取学校
					school = userEntity.getSchoolName();
				}
				//获取班级
				if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
					List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
					StudentClassInfoEntity entity = new StudentClassInfoEntity();
					entity.setStudentId(uid);
					entityList = studentClassInfoService.searchStudentClassInfoList(entity);
					if(null!=entityList && entityList.size()>0) {
						school =  entityList.get(0).getSchoolName();
					}
				}
//				course.setSchoolName(school);
//		if(StringUtil.isNotBlank(course.getDirectionTagId()) && StringUtil.isBlank(course.getCategoryTagId())) {
//			course.setTagId(course.getDirectionTagId());
//		}else {
//			course.setTagId(course.getCategoryTagId());
//			course.setParentTagId(course.getDirectionTagId());
//		}
		
		
		ResultBean result = courseService.coursePublicList(course, pageSize, pageNumber);
		return result;
	}
	
	@RequestMapping("/courseQualityList.action")
	@ResponseBody
	public ResultBean courseQualityList(CourseManagerView course, Integer pageSize, Integer pageNumber) {
		course.setShareFlag(COURSE_SHARE_FLAG_ONE);
		//获取工号
		UserEntity user = WebContext.getSessionUser();
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		//获取学校名称信息
		String school="";
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
			//获取学校
			school = userEntity.getSchoolName();
		}
		//获取班级
		if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
			List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
			StudentClassInfoEntity entity = new StudentClassInfoEntity();
			entity.setStudentId(uid);
			entityList = studentClassInfoService.searchStudentClassInfoList(entity);
			if(null!=entityList && entityList.size()>0) {
				school =  entityList.get(0).getSchoolName();
			}
		}
		course.setSchoolName(school);
//		if(StringUtil.isNotBlank(course.getDirectionTagId()) && StringUtil.isBlank(course.getCategoryTagId())) {
//			course.setTagId(course.getDirectionTagId());
//		}else {
//			course.setTagId(course.getCategoryTagId());
//			course.setParentTagId(course.getDirectionTagId());
//		}
		ResultBean result = courseService.coursePublicList(course, pageSize, pageNumber);
		return result;
	}
	
	*//**
	 * 检索教师信息
	 * 
	 * @author LILIANG
	 * @return UserEntity
	 *//*
	@RequestMapping("/teacherInfo.action")
	@ResponseBody
	public ResultBean teacherInfo(UserEntity user) {
		ResultBean result = courseService.teacherInfo(user);
		return result;
	}

	*//**
	 * 检索与教师相关的课程信息
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 *//*
	@RequestMapping("/teacherCourseList.action")
	@ResponseBody
	public ResultBean teacherCourseList(CourseEntity c) {
		ResultBean result = courseService.teacherCourseList(c);
		return result;
	}

	*//**
	 * 课程浏览数+1
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 *//*
	@RequestMapping("/addCourseViewNum.action")
	@ResponseBody
	public ResultBean addCourseViewNum(CourseEntity c) {
		ResultBean result = courseService.addCourseViewNum(c);
		return result;
	}

	*//**
	 * 根据课程ID检索课程详情
	 * 
	 * @param courseId 课程ID
	 * @return ResultBean data:CourseView 课程基本信息、视频数、练习数、标签、用户课程等信息
	 *//*
	@RequestMapping("/courseDetail.action")
	@ResponseBody
	public ResultBean courseDetail(String courseId) {
		return courseService.searchCourseDetail(courseId);
	}

	*//**
	 * 课程详情-立即支付详情
	 * 
	 * @param courseId 课程Id
	 * @return
	 *//*
	@RequestMapping("/immediatePayDetail.action")
	@ResponseBody
	public ResultBean immediatePayDetail(String courseId) {
		return courseService.immediatePayDetail(courseId);
	}

	*//**
	 * 课程列表页面
	 *//*
	@RequestMapping("/courseListInit.action")
	public ModelAndView courseListInit(String courseId,String schoolName) {
		ModelAndView result = new ModelAndView("/course/client/course_list");
		result.addObject("schoolName", schoolName);
		result.addObject("courseId", courseId);
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/pubCourseListInit.action")
	public ModelAndView pubCourseListInit() {
		ModelAndView result = new ModelAndView("/course/client/course_list_public");
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/qualityCourseListInit.action")
	public ModelAndView qualityCourseListInit() {
		ModelAndView result = new ModelAndView("/course/client/course_list_quality");
		result = getRoleAndNum(result);
		return result;
	}
	
	@RequestMapping("/courseFactoryListInit.action")
	public ModelAndView courseFactoryListInit() {
		ModelAndView result = new ModelAndView("/course/client/course_list_factory");
		result = getRoleAndNum(result);
		return result;
	}
	
	@RequestMapping("/studentHomePage.action")
	public ModelAndView studentHomePage(String userId) {
		ModelAndView result = new ModelAndView("/course/client/studentHomePage");
		UserEntity loginUser = userManagementService.seachUserInfo(userId);
		// List<CourseEntity> courseList = courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result = getRoleAndNum(result);
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/studentHomePageFromList.action")
	public ModelAndView studentHomePageFromList(String userId) {
		ModelAndView result = new ModelAndView("/course/client/studentHomePageFromList");
		UserEntity loginUser = userManagementService.seachUserInfo(userId);
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result = getRoleAndNum(result);
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/teacherHomePage.action")
	public ModelAndView teacherHomePage(String userId) {
		ModelAndView result = new ModelAndView("/course/client/teacherHomePage");
		result = getRoleAndNum(result);
		UserEntity loginUser = userManagementService.seachUserInfo(userId);
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/teacherHomePageFromList.action")
	public ModelAndView teacherHomePageFromList(String userId) {
		ModelAndView result = new ModelAndView("/course/client/teacherHomePageFromList");
		result = getRoleAndNum(result);
		UserEntity loginUser = userManagementService.seachUserInfo(userId);
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/saveInfo.action")
	@ResponseBody
	public ResultBean saveInfo(String studentJson) throws Exception {
		ArchivesEntity archives = JSONUtil.parse(
				JSONUtil.createObMapperInstance(ConfigUtil.getDateMinuteFormatDefualt()), studentJson,
				ArchivesEntity.class);
		courseService.saveArchives(archives);
		ResultBean result = ResultBean.success();
		return result;
	}

	@RequestMapping("/studentList.action")
	public ModelAndView studentList() {
		ModelAndView result = new ModelAndView("/course/client/student_list");
		result = getRoleAndNum(result);
		UserEntity loginUser = userManagementService.seachUserInfo(WebContext.getSessionUserId());
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/teacherList.action")
	public ModelAndView teacherList() {
		ModelAndView result = new ModelAndView("/course/client/teacher_list");
		result = getRoleAndNum(result);
		UserEntity loginUser = userManagementService.seachUserInfo(WebContext.getSessionUserId());
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/archivesList.action")
	@ResponseBody
	public ResultBean archivesList(String userId) {
		ResultBean result = new ResultBean();
		TpUserEntity tpUser = courseService.getUserByUserId(userId);
		List<ArchivesEntity> archivesList = courseService.getArchivesList(userId);
		List<CourseEntity> courseList = courseService.getCourseListByUserId(userId);
		if (courseList.size() > 0) {
			for (CourseEntity entity : courseList) {
				ArchivesEntity archives = new ArchivesEntity();
				archives.setUserId(userId);
				archives.setUserName(tpUser.getPersonName());
				archives.setCreateTime(sdf.format(entity.getCreateDate()));
				archives.setCreateDate(entity.getCreateDate());
				archives.setInfo(sdf.format(entity.getCreateDate()) + ",创建课程，" + entity.getTitle());
				if (entity.getPictureId() != null) {
					archives.setExitImage("1");
					archives.setImageId(entity.getPictureId());
				}
				// archivesList.add(archives);
			}
			listSort(archivesList);
		}
		List<UserCourseEntity> ucList = courseService.getUCListByUserId(userId);
		if (ucList.size() > 0) {
			for (UserCourseEntity uc : ucList) {
				ArchivesEntity archives = new ArchivesEntity();
				archives.setUserId(userId);
				archives.setUserName(tpUser.getPersonName());
				archives.setCreateTime(sdf.format(uc.getCreateDate()));
				archives.setCreateDate(uc.getCreateDate());
				archives.setInfo(sdf.format(uc.getCreateDate()) + ",加入课程，" + uc.getCourseName());
				if (uc.getPictureId() != null) {
					archives.setExitImage("1");
					archives.setImageId(uc.getPictureId());
				}
				// archivesList.add(archives);
			}
			listSort(archivesList);
		}
		result.setData(archivesList);
		return result;
	}

	@RequestMapping("/saveDiscussData.action")
	@ResponseBody
	public ResultBean saveDiscussData(ArchivesDiscussEntity data) throws Exception {
		return courseService.saveDiscuss(data);
	}

	@RequestMapping("/searchUsersList.action")
	@ResponseBody
	public ResultBean searchUsersList(TpUserEntity user) {
		String userKbn = WebContext.getSessionUser().getUserKbn();
		if (userKbn != null && userKbn.contentEquals(TpStateConstants.USER_KBN_C)) {
			user.setUserKbn(TpStateConstants.USER_KBN_C);
		}
		user.setSortName("createDate");
		user.setSortOrder("DESC");
		return userManagementService.search4Page(user);
	}

	@SuppressWarnings("unused")
	private void listSort(List<ArchivesEntity> list) {
		Collections.sort(list, new Comparator<ArchivesEntity>() {
			@Override
			public int compare(ArchivesEntity o1, ArchivesEntity o2) {
				try {
					Date dt1 = o1.getCreateDate();
					Date dt2 = o2.getCreateDate();
					if (dt1.getTime() > dt2.getTime()) {
						return -1;
					} else if (dt1.getTime() < dt2.getTime()) {
						return 1;
					} else {
						return 0;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return 0;
			}
		});
	}

	*//**
	 * 系统首页
	 *//*
	@RequestMapping("/homePage.action")
	public ModelAndView homePage() {
		ModelAndView result = new ModelAndView("/course/client/homePage");
		UserEntity user = WebContext.getSessionUser();
		//获取工号
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getIsFlag())) {
			result.addObject("isFlag","1");
		}else {
			//不是管理员
			result.addObject("isFlag","0");
		}
		
		//获取人员信息
		String image = "";
		String school="";
		String grade="";
		String className="";
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
			//获取头像
			image = userEntity.getHeadurl();
			//获取学校
			school = userEntity.getSchoolName();
		}else {
			//设置默认头像
			image ="http://wisdomsaas-img.oss-cn-beijing.aliyuncs.com/BFAA0FECDD194B7A974C60B7E1B27C18?authorization=bce-auth-v1%2F7642b4b48046452697e90662eed41ace%2F2017-12-29T08%3A33%3A39Z%2F-1%2F%2F0f06024af14dad05694f25b8c85572567591c72bbd0129d3175b4cdbd6e62208";
		}
		//获取班级
		if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
			List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
			StudentClassInfoEntity entity = new StudentClassInfoEntity();
			entity.setStudentId(uid);
			entityList = studentClassInfoService.searchStudentClassInfoList(entity);
			if(null!=entityList && entityList.size()>0) {
				grade = entityList.get(0).getGradeName();
				className = entityList.get(0).getClassName();
				school =  entityList.get(0).getSchoolName();
			}
		}
		result.addObject("className", className);
		result.addObject("grade", grade);
		result.addObject("school", school);
		result.addObject("image", image);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 问卷考试系统首页
	 *//*
	@RequestMapping("/homeJavaPage.action")
	public ModelAndView homeJavaPage(String courseId) {
		ModelAndView result = new ModelAndView("/course/client/homeJavaPage");
		UserEntity user = WebContext.getSessionUser();
		//获取工号
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getIsFlag())) {
			result.addObject("isFlag","1");
		}else {
			//不是管理员
			result.addObject("isFlag","0");
		}
		
		//获取人员信息
		String image = "";
		String school="";
		String grade="";
		String className="";
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
			//获取头像
			image = userEntity.getHeadurl();
			//获取学校
			school = userEntity.getSchoolName();
		}else {
			//设置默认头像
			image ="http://wisdomsaas-img.oss-cn-beijing.aliyuncs.com/BFAA0FECDD194B7A974C60B7E1B27C18?authorization=bce-auth-v1%2F7642b4b48046452697e90662eed41ace%2F2017-12-29T08%3A33%3A39Z%2F-1%2F%2F0f06024af14dad05694f25b8c85572567591c72bbd0129d3175b4cdbd6e62208";
		}
		//获取班级
		if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
			List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
			StudentClassInfoEntity entity = new StudentClassInfoEntity();
			entity.setStudentId(uid);
			entityList = studentClassInfoService.searchStudentClassInfoList(entity);
			if(null!=entityList && entityList.size()>0) {
				grade = entityList.get(0).getGradeName();
				className = entityList.get(0).getClassName();
				school =  entityList.get(0).getSchoolName();
			}
		}
		result.addObject("className", className);
		result.addObject("grade", grade);
		result.addObject("school", school);
		result.addObject("courseId", courseId);
		result.addObject("image", image);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 题库管理
	 *//*
	@RequestMapping("/homeExamPage.action")
	public ModelAndView homeExamPage() {
		ModelAndView result = new ModelAndView("/course/manager/homeExamPage");
		UserEntity user = WebContext.getSessionUser();
		//获取工号
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getIsFlag())) {
			result.addObject("isFlag","1");
		}else {
			//不是管理员
			result.addObject("isFlag","0");
		}
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 题库管理
	 *//*
	@RequestMapping("/homeJsdPage.action")
	public ModelAndView homeJsdPage() {
		ModelAndView result = new ModelAndView("/course/client/homeJsdPage");
		UserEntity user = WebContext.getSessionUser();
		//获取工号
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getIsFlag())) {
			result.addObject("isFlag","1");
		}else {
			//不是管理员
			result.addObject("isFlag","0");
		}
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 获取登录信息
	 * @param code
	 * @param type
	 * @return
	 * @throws UnsupportedEncodingException
	 *//*
	@SuppressWarnings("null")
	@RequestMapping("/getUserInfo.action")
	@ResponseBody
	public JSONArray getUserInfo(String code, String type) throws UnsupportedEncodingException {
		//设置json集合对象和数组
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		if (code != null && !"".equals(code)) {
			//UserEntity user = WebContext.getSessionUser();
			String codeSession = (String) WebContext.getSession().getAttribute("code");
			if (codeSession == null || !code.equals(codeSession)) {
				Long str12 = System.currentTimeMillis();
				String access_tokenStr = "";
				//定义获取应用token的变量
				//String application_tokenStr="";
				if (type.equals("1")) {
					//获取access_tokenStr字符串对象
					access_tokenStr = HttpUtils.sendFormEvalutionPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token",
							"code=" + code
									+ "&grant_type=authorization_code&client_id=1EF8798194B36D72&client_secret=440a4fad895036188ea200a0da1d6156&redirect_uri="
									+ PropertyUtil.getProperty("remote.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "1EF8798194B36D72", "440a4fad895036188ea200a0da1d6156");
				} else if (type.equals("2")) {
					//获取access_tokenStr字符串对象
					access_tokenStr = HttpUtils.sendFormEvalutionPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token",
							"code=" + code
									+ "&grant_type=authorization_code&client_id=BB2650288E091B1C&client_secret=26807915a5269ba07a40d97236b5ec64&redirect_uri="
									+ PropertyUtil.getProperty("teacher.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "BB2650288E091B1C", "26807915a5269ba07a40d97236b5ec64");
				} else if (type.equals("3")) {
					//获取access_tokenStr字符串对象
					access_tokenStr = HttpUtils.sendPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token",
							"code=" + code
									+ "&grant_type=authorization_code&client_id=42F9292D78B1386F&client_secret=e1e034ac44a63ba61e8e9595689efd4c&redirect_uri="
									+ PropertyUtil.getProperty("student.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "42F9292D78B1386F", "e1e034ac44a63ba61e8e9595689efd4c");
				} else if (type.equals("4")) {
					//获取access_tokenStr字符串对象
					   access_tokenStr = HttpUtils.sendFormEvalutionPost(
					            PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", 
					            "code=" + code + 
					            "&grant_type=authorization_code&client_id=6FF0E39AD917FD04&client_secret=ea201b6aff470f9ec25c1fc9be17197a&redirect_uri=" + 
					            PropertyUtil.getProperty("institution.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "6FF0E39AD917FD04", "ea201b6aff470f9ec25c1fc9be17197a");
				} else if (type.equals("5")) {
					//获取access_tokenStr字符串对象
					access_tokenStr = HttpUtils.sendFormEvalutionPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token",
							"code=" + code
									+ "&grant_type=authorization_code&client_id=8275669B807A7B7A&client_secret=229f39d615a8c2cbbef56e56197a22c7&redirect_uri="
									+ PropertyUtil.getProperty("teacherevalution.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "8275669B807A7B7A", "229f39d615a8c2cbbef56e56197a22c7");
				}
				//设置执行时间
				Long str333 = System.currentTimeMillis();
				int tokentime =(int) (str333-str12);
				UserLogEntity entity12=new UserLogEntity();
				entity12.setId(IDGenerator.genUID());
				entity12.setUserId(WebContext.getSessionUserId());
				entity12.setUserName(WebContext.getSessionUser().getPersonName());
				entity12.setLogType("获取token信息");
				entity12.setTimes(tokentime);
				entity12.setOperTime(new Date());
				//保存方法执行时间日志信息
				tagInstitutionEvalutionService.insertUserLog(entity12);
				//获取access_token
				if (access_tokenStr != null && !"".equals(access_tokenStr)) {
					//字符转化成json对象
					JSONObject accessjson = JSONObject.fromObject(access_tokenStr);
					//获取字段值
					String access_token = accessjson.getString("access_token");
					Long str3 = System.currentTimeMillis();
					//原有逻辑获取用户信息开始
					String userStr = HttpUtils.sendFormEvalutionPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/rest/get/userinfo",
							"access_token=" + access_token);
					Long str33 = System.currentTimeMillis();
					int userinfotime =(int) (str33-str3);
					UserLogEntity entity13=new UserLogEntity();
					entity13.setId(IDGenerator.genUID());
					entity13.setUserId(WebContext.getSessionUserId());
					entity13.setUserName(WebContext.getSessionUser().getPersonName());
					entity13.setLogType("获取/passport/rest/get/userinfo信息");
					entity13.setTimes(userinfotime);
					entity13.setOperTime(new Date());
					//保存方法执行需要的时间信息日志
					tagInstitutionEvalutionService.insertUserLog(entity13);
					System.out.println("userStr=:" + userStr);
					
					
					JSONObject userjson = JSONObject.fromObject(userStr);
					//原有逻辑获取用户信息结束
					//用户id
					String userid = userjson.getString("userid");
					//用户账号
					String account = userjson.getString("account");
					//用户类型
					String usertype = userjson.getString("usertype");
					//用户名称
					String name = userjson.getString("name");
					//用户性别
					String sex = userjson.getString("sex");
					//用户角色
					String role = userjson.getString("role");
					//获取tenantId
					String tenantId = userjson.getString("tenantId");
					//获取头像url
					String headUrl = userjson.getString("head_url");
					String teacher = "";
					*//**
					 * usertype=1
					 * 1:平台总系统管理员;2:平台省级系统管理员;3:平台市级系统管理员;4:学校总系统管理员;5:学校分校系统管理员;6:学校年级系统管理员;7:教育部分系统管理员
					 * usertype=0 
					 * 1:教师;2:家长;3:学生;4:管理者;5:教研员
					 *//*
					*//**
					 * 系统角色 01:管理员;04:教师
					 *//*
					UserRoleEntity userRole = new UserRoleEntity();
					UserRoleEntity userTeacherRole = new UserRoleEntity();
					UserEntity loginUser = new UserEntity();
					TpUserEntity saveUser = new TpUserEntity();
					*//**
					 * 获取用户是老师的信息
					 * 包括老师所属的学校信息
					 *//*
					if("1".equals(role)) {
						//获取教师信息
						Long str5 = System.currentTimeMillis();
						//调用外部接口获取教师信息
						String teacherinfo = HttpUtils.sendFormDataPost("https://gwss.yceduyun.com/apis/user/V1.0/teacherInfo",
								access_token,"002");
						Long str6 = System.currentTimeMillis();
						int teachertime =(int) (str6-str5);
						UserLogEntity entity2=new UserLogEntity();
						entity2.setId(IDGenerator.genUID());
						entity2.setUserId(WebContext.getSessionUserId());
						entity2.setUserName(WebContext.getSessionUser().getPersonName());
						entity2.setLogType("获取https://gwss.yceduyun.com/apis/user/V1.0/teacherInfo信息");
						entity2.setTimes(userinfotime);
						entity2.setOperTime(new Date());
						//保存获取教师信息所需要的时间日志信息
						tagInstitutionEvalutionService.insertUserLog(entity2);
						System.out.println(teacherinfo); 
                        //解析教师信息
						JSONObject teacherinfojson = JSONObject.fromObject(teacherinfo);
						JSONObject teacherinfoDatajson = JSONObject.fromObject(teacherinfojson.get("data"));
						//获取基本信息
						if(null!=teacherinfoDatajson) {
							//省份
							String proName=teacherinfoDatajson.getString("proName");
							//市区
							String cityName=teacherinfoDatajson.getString("cityName");
							//县级
							String townName=teacherinfoDatajson.getString("townName");
							//学校id
							String schoolId=teacherinfoDatajson.getString("schoolId");
							//学校名称
							String schoolName=teacherinfoDatajson.getString("schoolName");
							//学校类型
							String schoolType=teacherinfoDatajson.getString("schoolType");
							//
							String subjectId=teacherinfoDatajson.getString("subjectId");
							//
							String subjectName=teacherinfoDatajson.getString("subjectName");
							saveUser.setProName(proName);
							saveUser.setCityName(cityName);
							saveUser.setTownName(townName);
							saveUser.setSchoolId(schoolId);
							saveUser.setSchoolName(schoolName);
							saveUser.setSchoolType(schoolType);
							saveUser.setSubjectId(subjectId);
							saveUser.setSubjectName(subjectName);
						}
						//获取字段信息
						JSONArray teacherClassArray =JSONArray.fromObject(teacherinfoDatajson.get("classList"));
						//获取老师的班级年级信息
						@SuppressWarnings("unchecked")
						List<TeacherClassInfoEntity> list = teacherClassArray.toList(teacherClassArray, TeacherClassInfoEntity.class);
						//判断对象是否为空
						if(null!=list && list.size()>0) {
							List<TeacherClassInfoEntity> entityList = new ArrayList<TeacherClassInfoEntity>();
							//查询该老师下的信息是否存在
							TeacherClassInfoEntity entity = new TeacherClassInfoEntity();
							entity.setTeacherId(userid);
							entityList = teacherClassInfoService.searchTeacherClassInfoList(entity);
							if(null!=entityList && entityList.size()>0) {
								//循环遍历教师班级信息
								for(int j=0;j<entityList.size();j++) {
									TeacherClassInfoEntity entity1 = new TeacherClassInfoEntity();
									entity1.setId(entityList.get(j).getId());
									//删除已存在信息
									teacherClassInfoService.deleteTeacherClassInfoById(entity1);
								}
							}
							//循环保存班级信息
							for(int i=0;i<list.size();i++) {
								TeacherClassInfoEntity infoEntity = new TeacherClassInfoEntity();
								infoEntity.setTeacherId(userid);//老师id
								infoEntity.setGradeId(list.get(i).getGradeId());//年级id
								infoEntity.setGradeName(list.get(i).getGradeName());//年级名称
								infoEntity.setGradeLever(list.get(i).getGradeLever());//年级等级
								infoEntity.setClassId(list.get(i).getClassId());//班级id
								infoEntity.setClassName(list.get(i).getClassName());//班级名称
								infoEntity.setSubjectId(list.get(i).getSubjectId());//
								infoEntity.setSubjectName(list.get(i).getSubjectName());//
								infoEntity.setCreateDate(new Date());//创建日期
								infoEntity.setUpdateDate(new Date());//更新日期
								teacherClassInfoService.insertTeacherClassInfo(infoEntity);
							}
						}
						//获取用户具体信息
						String param=access_token;
						String tenantId1 = "002";
						Long str7 = System.currentTimeMillis();
						//获取教师角色
						String teacherDetailinfo = HttpUtils.sendFormDataDetailPost("https://gwss.yceduyun.com/apis/usercenter/v1.0/getOneUserInfoByUseIdAndRoleId",
								param,tenantId1,role);
						Long str8 = System.currentTimeMillis();
						int studenttime =(int) (str8-str7);
						UserLogEntity entity3=new UserLogEntity();
						entity3.setId(IDGenerator.genUID());
						entity3.setUserId(WebContext.getSessionUserId());
						entity3.setUserName(WebContext.getSessionUser().getPersonName());
						entity3.setLogType("获取https://gwss.yceduyun.com/apis/usercenter/v1.0/getOneUserInfoByUseIdAndRoleId信息");
						entity3.setTimes(userinfotime);
						entity3.setOperTime(new Date());
						//保存调用教师角色花费时间信息
						tagInstitutionEvalutionService.insertUserLog(entity3);
						//字符串转json
						JSONObject teacherDetailinfojson = JSONObject.fromObject(teacherDetailinfo);
						JSONArray teacherinfoDataDetailjson = JSONArray.fromObject(teacherDetailinfojson.get("data"));
						JSONArray dutyJson = JSONObject.fromObject(teacherinfoDataDetailjson.get(0)).getJSONArray("teacherDuties");
						List<InstitutionTeacherDutyEntity> dutylist = JSONArray.toList(dutyJson, InstitutionTeacherDutyEntity.class);
                        if(null!=dutylist && dutylist.size()>0) {
                        	for(int p=0;p<dutylist.size();p++) {
                        		//001代表校长
                        		if("校长".equals(dutylist.get(p).getDutyName())) {
                        			teacher="001";
                        			break;
                        		}else {
                        			//002代表老师
                        			teacher="002";
                        		}
                        	}
                        }
					}else if("3".equals(role)) {
						//获取学生信息
						Long str9 = System.currentTimeMillis();
						//调用学生接口获取信息
						String studentinfo = HttpUtils.sendFormDataPost("https://gwss.yceduyun.com/apis/user/V1.0/studentInfo",
								access_token,"002");
						Long str10 = System.currentTimeMillis();
						int studenttime =(int) (str10-str9);
						UserLogEntity entity3=new UserLogEntity();
						entity3.setId(IDGenerator.genUID());
						entity3.setUserId(WebContext.getSessionUserId());
						entity3.setUserName(WebContext.getSessionUser().getPersonName());
						entity3.setLogType("获取https://gwss.yceduyun.com/apis/user/V1.0/studentInfo信息");
						entity3.setTimes(userinfotime);
						entity3.setOperTime(new Date());
						//保存调用学生接口获取花费的时间日志
						tagInstitutionEvalutionService.insertUserLog(entity3);
						JSONObject studentinfojson = JSONObject.fromObject(studentinfo);
						JSONObject studentinfoDatajson = JSONObject.fromObject(studentinfojson.get("data"));
						if(null!= studentinfoDatajson) {
							String studentId=userid;
							//姓名
							String studentName=studentinfoDatajson.getString("username");
							String schoolId=studentinfoDatajson.getString("schoolId");//学校id
							String schoolName=studentinfoDatajson.getString("schoolName");//学校名称
							String schoolType=studentinfoDatajson.getString("schoolTye");//学校类型
							String gradeId=studentinfoDatajson.getString("gradeId");//年级id
							String gradeName=studentinfoDatajson.getString("gradeName");//年级名称
							String classId=studentinfoDatajson.getString("classId");//班级id
							String className=studentinfoDatajson.getString("className");//班级名称
							String gradeLever=studentinfoDatajson.getString("gradeLever");//年级等级
							String proName=studentinfoDatajson.getString("proName");//省份
							String cityName=studentinfoDatajson.getString("cityName");//市区
							String townName=studentinfoDatajson.getString("townName");//县级
							StudentClassInfoEntity studentClassInfoEntity = new StudentClassInfoEntity();
							studentClassInfoEntity.setStudentId(studentId);
							List<StudentClassInfoEntity> studentlist = new ArrayList<StudentClassInfoEntity>();
							studentlist = studentClassInfoService.searchStudentClassInfoList(studentClassInfoEntity);
							if(null!=studentlist && studentlist.size()>0) {
								//循环遍历学生信息
								for(int k=0;k<studentlist.size();k++) {
									StudentClassInfoEntity student = new StudentClassInfoEntity();
									student.setId(studentlist.get(k).getId());
									//删除学生信息
									studentClassInfoService.deleteStudentClassInfoById(student);
								}
							}
							//保存学生班级信息
							StudentClassInfoEntity stunfoEntity = new StudentClassInfoEntity();
							stunfoEntity.setStudentId(studentId);
							stunfoEntity.setStudentName(studentName);
							stunfoEntity.setSchoolId(schoolId);
							stunfoEntity.setSchoolName(schoolName);
							stunfoEntity.setSchoolType(schoolType);
							stunfoEntity.setGradeId(gradeId);
							stunfoEntity.setGradeName(gradeName);
							stunfoEntity.setGradeLever(gradeLever);
							stunfoEntity.setClassId(classId);
							stunfoEntity.setClassName(className);
							stunfoEntity.setProName(proName);
							stunfoEntity.setCityName(cityName);
							stunfoEntity.setTownName(townName);
							stunfoEntity.setCreateDate(new Date());
							studentClassInfoService.insertStudentClassInfo(stunfoEntity);
							Long str122 = System.currentTimeMillis();
							int studentinerttime =(int) (str122-str10);
							UserLogEntity entity32=new UserLogEntity();
							entity32.setId(IDGenerator.genUID());
							entity32.setUserId(WebContext.getSessionUserId());
							entity32.setUserName(WebContext.getSessionUser().getPersonName());
							entity32.setLogType("保存学生角色信息");
							entity32.setTimes(studentinerttime);
							entity32.setOperTime(new Date());
							tagInstitutionEvalutionService.insertUserLog(entity32);
						}
						
					}
					if (usertype != null && !"".equals(usertype) && usertype.equals("0")) {
						//老师
						if (!"".equals(role) && role.equals("1")) {
							userRole.setRoleId("01");
							userTeacherRole.setRoleId("04");
							loginUser.setUserKbn("A");
							saveUser.setUserKbn("A");
							if (userid != null && !"".equals(userid)) {
								userRole.setUserId(userid);
								userTeacherRole.setUserId(userid);
							}
							//管理者
						} else if (!"".equals(role) && role.equals("4")) {
							userRole.setRoleId("01");
							loginUser.setUserKbn("B");
							saveUser.setUserKbn("B");
							if (userid != null && !"".equals(userid)) {
								userRole.setUserId(userid);
							}
							//学生
						} else if(!"".equals(role) && role.equals("3")){
							loginUser.setUserKbn("C");
							saveUser.setUserKbn("C");
							//家长
						}else if(!"".equals(role) && role.equals("2")){
							loginUser.setUserKbn("D");
							saveUser.setUserKbn("D");
							//教研员
						}else if(!"".equals(role) && role.equals("5")){
							loginUser.setUserKbn("E");
							saveUser.setUserKbn("E");
						}
					}
					
					loginUser.setUserId(userid);
					loginUser.setUserName(account == null ? "" : account);
					loginUser.setSex(sex == null ? "" : sex);
					loginUser.setPersonName(name == null ? "" : name);
					loginUser.setPassword(MD5Util.getPassword4MD5("123456"));
					userRole.setCreateDate(new Date());
					userRole.setCreateUser(userid);
					userTeacherRole.setCreateDate(new Date());
					userTeacherRole.setCreateUser(userid);
					loginUser.setCreateDate(new Date());
					loginUser.setCreateUser(userid);
					loginUser.setLockFlag("0");

					saveUser.setUserId(userid);
					saveUser.setUserName(account == null ? "" : account);
					saveUser.setSex(sex == null ? "" : sex);
					saveUser.setPersonName(name == null ? "" : name);
					saveUser.setPassword(MD5Util.getPassword4MD5("123456"));
					saveUser.setCreateDate(new Date());
					saveUser.setCreateUser(userid);
					saveUser.setCreateDate(new Date());
					saveUser.setCreateUser(userid);
					saveUser.setLockFlag("0");
					saveUser.setTenantId(tenantId);
					saveUser.setHeadurl(headUrl);
					saveUser.setDuty(teacher);
					Long usertime1 = System.currentTimeMillis();
					courseService.saveUserAndRole(userRole, userTeacherRole, saveUser);
					Long usertime2 = System.currentTimeMillis();
					WebContext.loginUser(loginUser, true);
					WebContext.getSession().setAttribute("code", code);
					jo.put("result", "success");
					Long str123 = System.currentTimeMillis();
					System.out.println("计算花费时间---------------------------");
					int times = (int) (usertime2-usertime1);
					UserLogEntity entity=new UserLogEntity();
					entity.setId(IDGenerator.genUID());
					entity.setUserId(WebContext.getSessionUserId());
					entity.setUserName(WebContext.getSessionUser().getPersonName());
					entity.setLogType("获取保存用户信息");
					entity.setTimes(times);
					entity.setOperTime(new Date());
					tagInstitutionEvalutionService.insertUserLog(entity);
					System.out.println(str123-str12);
				}
			}

		}
		json.add(jo);
		return json;
	}
    *//**
     * 手机端首页
     * @return
     *//*
	@RequestMapping("/homePagePhone.action")
	public ModelAndView homePagePhone() {
		ModelAndView result = new ModelAndView("/course/client/homePagePhone");
		return result;
	}
    *//**
                * 教师页面
     * @return
     *//*
	@RequestMapping("/teacherHome.action")
	public ModelAndView teacherHome() {
		ModelAndView result = new ModelAndView("/course/client/teacherHomeBack");
		return result;
	}
    *//**
     * 学生页面
     * @return
     *//*
	@RequestMapping("/studentHome.action")
	public ModelAndView studentHome() {
		ModelAndView result = new ModelAndView("/course/client/studentHomeBack");
		return result;
	}
    *//**
     * 人员信息
     * @return
     *//*
	@RequestMapping("/sysUser.action")
	public ModelAndView sysUser() {
		ModelAndView result = new ModelAndView("/course/client/sysUser");
		result = getRoleAndNum(result);
		return result;
	}
    *//**
     * 系统日志
     * @return
     *//*
	@RequestMapping("/sysLog.action")
	public ModelAndView sysLog() {
		ModelAndView result = new ModelAndView("/course/client/sysLog");
		result = getRoleAndNum(result);
		return result;
	}
   *//**
    * 保存系统日志
    * @param proName
    * @param IpInfo
    * @return
    *//*
	@RequestMapping("/insertSysUser.action")
	@ResponseBody
	public JSONArray insertSysUser(String proName, String IpInfo) {
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		courseService.insertSysUser(proName, IpInfo);
		jo.put("result", "success");
		json.add(jo);
		return json;
	}
    *//**
     * 保存操作日志
     * @param browserInfo
     * @param sysInfo
     * @param IpInfo
     * @param operDetail
     * @param operSite
     * @return
     *//*
	@RequestMapping("/insertUserOper.action")
	@ResponseBody
	public JSONArray insertUserOper(String browserInfo, String sysInfo, String IpInfo, String operDetail,
			String operSite) {
		UserOperEntity oper = new UserOperEntity();
		oper.setSysInfo(browserInfo + "(" + sysInfo + ")");
		oper.setOperDetail(operDetail);
		oper.setOperSite(operSite);
		oper.setOperTime(new Date());
		oper.setOperIP(IpInfo);
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		courseService.inserUserOper(oper);
		jo.put("result", "success");
		json.add(jo);
		return json;
	}
	*//**
	 * 
	 * @param IpInfo
	 * @param logType
	 * @return
	 *//*
	@RequestMapping("/insertUserLog.action")
	@ResponseBody
	public JSONArray insertUserLog(String IpInfo, String logType) {
		UserLogEntity log = new UserLogEntity();
		log.setOperIP(IpInfo);
		log.setLogType(logType);
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		String userId = WebContext.getSessionUserId();
		String userName = WebContext.getSessionUser().getPersonName();
		log.setUserId(userId);
		log.setUserName(userName);
		courseService.insertUserLog(log);
		jo.put("result", "success");
		json.add(jo);
		WebContext.logout();
		return json;
	}
    *//**
                  * 根据用户名查询用户信息
     * @param username
     * @param IpInfo
     * @return
     *//*
	@RequestMapping("/getUserByUsername.action")
	@ResponseBody
	public JSONArray getUserByUserName(String username, String IpInfo) {
		TpUserEntity result = courseService.getUserByUsername(username);
		if (result != null) {
			UserLogEntity log = new UserLogEntity();
			log.setOperIP(IpInfo);
			log.setLogType("登录");
			log.setUserId(result.getUserId());
			log.setUserName(result.getPersonName());
			courseService.insertUserLog(log);
		}
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		if (result != null) {
			jo.put("result", "success");
		} else {
			jo.put("result", "error");
		}
		json.add(jo);
		return json;
	}

	*//**
	 * 系统首页
	 *//*
	@RequestMapping("/webEditor.action")
	public ModelAndView webEditor() {
		ModelAndView result = new ModelAndView("/course/client/webEditor");
		String serverName = WebContext.getRequest().getServerName();
		int serverPort = WebContext.getRequest().getServerPort();
		result.addObject("serverUrl", serverName + ":" + serverPort);
		return result;
	}
    *//**
     * 课程详情页面
     * @param courseId
     * @param schoolName
     * @return
     *//*
	@RequestMapping("/courseDetailInit.action")
	public ModelAndView courseDetailInit(String courseId,String schoolName) {
		ModelAndView result = new ModelAndView("/course/client/course_detail");
		ResultBean data = courseService.searchCourseDetail(courseId);
		if (!data.isStatus()) {
			throw new BusinessException(data.getMessages());
		}
		ChapterEntity entity = new ChapterEntity();
		entity.setCourseId(courseId);
		List<ChapterEntity> chapterCourseList = courseService.selectByCourseId(entity);
		result.addObject("chapterCourseList", chapterCourseList);
		result.addObject("courseView", data.getData());
		result.addObject("schoolName", schoolName);
		result.addObject("courseId", courseId);
		// 检索章节列表
		List<ChapterView> chapterList = chapterService.chapterList(courseId);
		result.addObject("chapterList", chapterList);
		String measureType="2";
		List<ChapterView> chapterList1 = chapterService.chapterList1(courseId,measureType);
		result.addObject("chapterExamList", chapterList1);
		List<CourseMessageEntity> userMsgList = courseService.getUserMsgList(courseId);
		if(userMsgList.size() > 0) {
			CourseMessageEntity msg = userMsgList.get(0);
			if(msg.getCheckStatus().equals("-1")) {
				result.addObject("msgInfo", "-1");
			}else if(msg.getCheckStatus().equals("1")) {
				result.addObject("msgInfo", "1");
			}else {
				result.addObject("msgInfo", "none");
			}
		}
		result = getRoleAndNum(result);
		return result;
	}
    *//**
     * 公开课详情页面
     * @param courseId
     * @return
     *//*
	@RequestMapping("/coursePublicDetail.action")
	public ModelAndView coursePublicDetail(String courseId) {
		ModelAndView result = new ModelAndView("/course/client/course_public_detail");
		ResultBean data = courseService.searchCourseDetail(courseId);
		if (!data.isStatus()) {
			throw new BusinessException(data.getMessages());
		}
		result.addObject("courseView", data.getData());
		result = getRoleAndNum(result);
		return result;
	}

	*//**
	 * 导师课程页面初始化
	 * 
	 * @param
	 * @return
	 *//*
	@RequestMapping("/teacherCourseInit.action")
	public ModelAndView teacherCourseInit(UserEntity user) {
		ModelAndView mv = new ModelAndView("/course/client/teacher_course_list");
		ResultBean result = courseService.teacherInfo(user);
		UserEntity use = new UserEntity();
		if (result.getData() != null) {
			use = (UserEntity) result.getData();
			if ("0".equals(use.getSex())) {
				use.setSex("男");
			} else {
				use.setSex("女");
			}
		}
		mv.addObject("user", use);
		return mv;
	}

	*//**
	 * 投票页面初始化
	 * 
	 * @param
	 * @return
	 *//*
	@RequestMapping("/voteItemInit.action")
	public ModelAndView voteItemInit(String courseId, String linkType) {
		ModelAndView mv = new ModelAndView("/course/client/vote_item");
		if (StringUtils.isNotEmpty(linkType)) {
			mv.addObject("linkType", linkType);
		}
		mv.addObject("courseId", courseId);
		return mv;
	}

	*//**
	 * 根据用户ID，课程ID检索用户课程
	 * 
	 * @param courseId
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchUserCourse.action")
	@ResponseBody
	public ResultBean searchUserCourse(String courseId) {
		ResultBean result = new ResultBean();
		if (courseId == null || courseId.equals("")) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_NULL_ID));

			return result;
		}

		UserCourseEntity uc = new UserCourseEntity();
		uc.setCourseId(courseId);
		// 从session中取出登录用户的ID
		String userId = WebContext.getSessionUserId();
		if (userId == null) {
			result.setStatus(false);
			result.setErrorCode(ErrorCode.NOT_LOGIN);
			result.setMessages(getMessage(MSG_E_SESSION_TIMEOUT));
			return result;
		}
		uc.setUserId(userId);
		uc = courseService.searchUserCourse(uc);
		result.setData(uc);
		result.setStatus(true);
		return result;
	}

	*//**
	 * 课程作业页面初始化
	 * 
	 * @param linkId   链接ID
	 * @param linkType 链接类型
	 * @return
	 *//*
	@RequestMapping("/homeWorkInit.action")
	public ModelAndView homeWorkInit(String linkId, String linkType) {
		ModelAndView mv = new ModelAndView("/course/client/home_work");
		if (StringUtils.isNotEmpty(linkType)) {
			mv.addObject("linkType", linkType);
		}
		mv.addObject("linkId", linkId);
		return mv;
	}

	*//**
	 * 检索我的作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/honeworkDetail.action")
	@ResponseBody
	public ResultBean honeworkDetail(HomeworkView homeworkView) {
		ResultBean result = new ResultBean();
		String userId = WebContext.getSessionUserId();
		List<HomeworkView> viewlist = new ArrayList<HomeworkView>();
		viewlist = (List<HomeworkView>) (courseService.honeworkList(homeworkView)).getData();
		for (int i = 0; i < viewlist.size(); i++) {
			CourseView courseView = new CourseView();
			courseView.setTeacherId(userId);
			if (courseService.isTeacher(courseView.getTeacherId())) {
				viewlist.get(i).setTeacher(1);
			} else {
				viewlist.get(i).setTeacher(0);
			}

		}

		result.setData(viewlist);
		return result;
	}

	@RequestMapping("/homeworkTitle.action")
	@ResponseBody
	public ResultBean homeworkTitle(HomeworkView homeworkView) {
		ResultBean result = new ResultBean();
		HomeworkEntity homeWork = courseService.getHomeWorkByLinkId(homeworkView);
		if (homeWork != null) {
			result.setStatus(true);
			result.setData(homeWork);
		}
		return result;
	}

	@RequestMapping("/homeWorkEdit.action")
	public ModelAndView homeWorkEdit(String id) {
		ModelAndView result = new ModelAndView("/course/client/home_work_edit");
		HomeworkView view = courseService.getHomeWorkById(id);
		result.addObject("homeWork", view);
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/homeWorkView.action")
	public ModelAndView homeWorkView(String id) {
		ModelAndView result = new ModelAndView("/course/client/home_work_view");
		HomeworkView view = courseService.getHomeWorkById(id);
		view.setCreateTime(sdf.format(view.getCreateDate()));
		result.addObject("homeWork", view);
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/homeWorkAddQuestion.action")
	public ModelAndView homeWorkAddQuestion(String id) {
		ModelAndView result = new ModelAndView("/course/client/home_work_question");
		HomeworkView view = courseService.getHomeWorkById(id);
		view.setCreateTime(sdf.format(view.getCreateDate()));
		result.addObject("homeWork", view);
		result.addObject("aq", new HomeworkAnswerQuestionEntity());
		result = getRoleAndNum(result);
		return result;
	}

	*//**
	 * 上传文件
	 *//*
	@RequestMapping("/upload.action")
	@ResponseBody
	public ResultBean upload(HomeworkView homeworkView) throws Exception {
		WebContext.init4CrossDomainAjax();
		return courseService.uploadDocument(homeworkView);
	}

	@RequestMapping("/download.action")
	public void download(@RequestParam(required = true) String fileId, String fileType, HttpServletResponse response)
			throws Exception {

		OutputStream output = new BufferedOutputStream(response.getOutputStream());
		InputStream input = null;
		try {
			FileEntity file = new FileEntity();
			if (StringUtil.isNotEmpty(fileType)) {
				file = fileService.getFile(fileId, fileType);
			} else {
				file = fileService.getFile(fileId, FILE_TYPE_HOMEWORK);
			}

			byte[] binary = file.getBinary();
			String fileName = file.getFileName();
			fileName = URLEncoder.encode(fileName, "UTF-8");
			response.reset();
			response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			response.addHeader("Content-Length", "" + file.getFileSize());
			response.setContentType("application/octet-stream;charset=UTF-8");
			if (StringUtil.isNotEmpty(file.getPath()) && StringUtil.isNotEmpty(ConfigUtil.getFileStorageRoot())) {
				// 文件有路径
				input = FileUtil.getInputStream(ConfigUtil.getFileStorageRoot(), file.getPath());
				IOUtils.copyLarge(input, output);
			} else {
				output.write(binary);
			}
			output.flush();
		} finally {
			IOUtils.closeQuietly(output);
			IOUtils.closeQuietly(input);
		}
	}

	*//**
	 * 课程作业管理页面初始化
	 * 
	 * @param
	 * @return
	 *//*
	@RequestMapping("/homeWorkReviewInit.action")
	public ModelAndView homeWorkReviewInit(String linkId, String linkType) {
		ModelAndView mv = new ModelAndView("/course/client/home_work_review");
		HomeworkView home = null;
		if (StringUtil.isNotEmpty(linkId) && StringUtil.isNotEmpty(linkType)) {
			home = courseService.searchworkDetail(linkId, linkType);
		}
		if (home == null) {
			home = new HomeworkView();
		}
		BaseEntity condition = new BaseEntity();
		condition.setPageSize(5);
		mv.addObject("linkId", linkId);
		mv.addObject("linkType", linkType);
		mv.addObject("title", home.getTitle());
		mv.addObject("courseTitle", home.getCourseTitle());
		mv.addObject("condition", condition);
		return mv;
	}

	*//**
	 * 导师品鉴作业初始化
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/homeworkReviewList.action")
	@ResponseBody
	public ResultBean homeworkReviewList(HomeworkView home) {

		return courseService.homeworkReviewList(home, PropertyUtil.getProperty("res.context.root"));
	}

	@RequestMapping("/gethomeworkReviewList.action")
	@ResponseBody
	public ResultBean gethomeworkReviewList(HomeworkView home) {
		return courseService.gethomeworkReviewList(home);
	}

	@RequestMapping("/gethomeworkReviewListByJson.action")
	@ResponseBody
	public JSONArray gethomeworkReviewListByJson(String courseId, String linkId, String linkType) throws IOException {

		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		List<HomeworkView> homeworkList = courseService.gethomeworkReviewListJson(courseId, linkId, linkType,
				PropertyUtil.getProperty("res.context.root"));
		if (homeworkList.size() > 0) {
			for (HomeworkView view : homeworkList) {
				Map<String, Object> courseERMap = new HashMap<String, Object>();
				courseERMap.put("userName", view.getCreateUser());
				courseERMap.put("audioContent", view.getContent() == null ? "" : view.getContent());
				// courseERMap.put("answerFileName", view.getAnswerFileName());
				courseERMap.put("score", view.getScore() == null ? "" : view.getScore());
				courseERMap.put("teacherComment", view.getTeacherComment() == null ? "" : view.getTeacherComment());
				// courseERMap.put("teacherScore", view.getTeacherScore() ==
				// null?"":view.getTeacherScore());
				// courseERMap.put("teacherComments", view.getTeacherComments() ==
				// null?"":view.getTeacherComments());
				resultList.add(courseERMap);
			}
		}
		jo.put("resultList", resultList);
		json.add(jo);
		return json;
	}

	*//**
	 * 导师评价
	 * 
	 * @author Id
	 * @param SCORE teacherComment
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/saveHomeworkReview.action")
	@ResponseBody
	public ResultBean saveHomeworkReview(HomeworkView home) {

		return courseService.saveHomeworkReview(home);
	}

	*//**
	 * 检索我的课程列表（检索用户课程，包括已关注、已学、已学完等检索条件以及检索列表中每一项课程的问卷）
	 * 
	 * @param finishType 完成类型（已收藏 1、已学2、3 已学完）
	 * @return 我的课程列表
	 * @author WUXIAOXIANG
	 *//*
	@RequestMapping("/userCourseList.action")
	@ResponseBody
	public ResultBean userCourseList(String finishType, Integer pageSize, Integer pageNumber) {
		// 检索我的课程
		ResultBean result = courseService.userCourseList(finishType, pageSize, pageNumber);
		return result;
	}

	@RequestMapping("/userCompulsoryList.action")
	@ResponseBody
	public ResultBean userCompulsoryList(String courseType, Integer pageSize, Integer pageNumber) {
		// 检索我的课程
		ResultBean result = courseService.userCompulsoryList(courseType, pageSize, pageNumber);
		return result;
	}

	*//**
	 * 检索我的作业列表
	 * 
	 * @author liuzhen
	 * @param
	 * @return 返回结果
	 *//*
	@RequestMapping("/userHomeworkList.action")
	@ResponseBody
	public ResultBean userHomeworkList(HomeworkView homeworkView, Integer pageSize, Integer pageNumber) {
		// 检索我的作业
		ResultBean result = courseService.userHomeworkList(homeworkView, pageSize, pageNumber);
		return result;
	}

	*//**
	 * 检索当前课程的所有作业
	 * 
	 * @author liuzhen
	 * @param
	 * @return 返回结果
	 *//*
	@RequestMapping("/alluserHomeworkList.action")
	@ResponseBody
	public ResultBean alluserHomeworkList(HomeworkView homeworkView, Integer pageSize, Integer pageNumber) {
		ResultBean result = courseService.getAlluserHomeworkList(homeworkView, pageSize, pageNumber);
		return result;
	}

	*//**
	 * 检索手机端导师个人中心的课程列表
	 * 
	 * @param pageSize   每页显示行数
	 * @param pageNumber 当前页
	 * @return 课程列表
	 * @author WUXIAOXIANG
	 *//*
	@RequestMapping("/teacherCourseAppList.action")
	@ResponseBody
	public ResultBean teacherCourseAppList(Integer pageSize, Integer pageNumber) {
		ResultBean result = courseService.teacherCourseAppList(pageSize, pageNumber);
		return result;
	}

	*//**
	 * 加入课程-支付详情初始化
	 * 
	 * @param courseId
	 * @return
	 *//*
	@SuppressWarnings({ "rawtypes" })
	@RequestMapping("/paydetail.action")
	public ModelAndView homeWorkReviewInit(String courseId) {
		ModelAndView mv = new ModelAndView("/course/client/course_pay_detail");
		UserEntity user = userService.searchById(WebContext.getSessionUserId());
		Map map = (Map) courseService.immediatePayDetail(courseId).getData();
		CourseView view = (CourseView) map.get("courseView");
		UserPoint userPoint = (UserPoint) map.get("userPoint");
		BigDecimal remainPoints = (BigDecimal) map.get("remainPoints");
		mv.addObject("user", user);
		mv.addObject("view", view);
		mv.addObject("userPoint", userPoint);
		mv.addObject("remainPoints", remainPoints);
		return mv;
	}

	*//**
	 * 用户管理页面 检索列表
	 * 
	 * @param condition
	 * @return 返回结果
	 *//*
	@RequestMapping("/search.action")
	@ResponseBody
	public ResultBean search(UserOperEntity condition) {
		return courseService.search4Page(condition);
	}

	@RequestMapping("/userOperInit.action")
	public ModelAndView userOperInit(String courseId) {
		ModelAndView result = new ModelAndView("/course/client/userOper");
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/mgrApprove.action")
	public ModelAndView mgrApprove(String courseId) {
		ModelAndView result = new ModelAndView("/course/client/mgr_approve");
		result = getRoleAndNum(result);
		return result;
	}
	
	@RequestMapping("/approveSearch.action")
	@ResponseBody
	public ResultBean approveSearch(CourseApproveEntity entity){
		return courseService.approveSearch(entity);
	}
	
	@RequestMapping("/saveHomeWork.action")
	@ResponseBody
	public ResultBean saveHomeWork(HomeworkView homework) throws Exception {
		return courseService.saveHomeWorkAnswer(homework);
	}

	@RequestMapping("/saveHomeWorkById.action")
	@ResponseBody
	public ResultBean saveHomeWorkById(HomeworkView homework) throws Exception {
		return courseService.saveHomeWorkById(homework);
	}
	
	@RequestMapping("/saveHomeWorkReviewById.action")
	@ResponseBody
	public ResultBean saveHomeWorkReviewById(String courseJson) throws Exception {
		HomeworkView homework = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, HomeworkView.class);
		return courseService.saveHomeWorkReviewById(homework);
	}

	@RequestMapping("/saveHomeWorkAnswerQuestion.action")
	@ResponseBody
	public ResultBean saveHomeWorkAnswerQuestion(HomeworkAnswerQuestionEntity aq) throws Exception {
		return courseService.saveHomeWorkAnswerQuestion(aq);
	}

	@RequestMapping("/answerQuestionList.action")
	@ResponseBody
	public ResultBean answerQuestionList(HomeworkAnswerQuestionEntity aq) {
		return courseService.searchAnswerQuestionList(aq);
	}

	@RequestMapping("/likeHomeWorkById.action")
	@ResponseBody
	public ResultBean likeHomeWorkById(String id) throws Exception {
		return courseService.likeHomeWorkById(id);
	}

	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/institutionlist.action")
	public ModelAndView institutionlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/institution/manager/institution_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		return result;
	}

	*//**
	 * 学校评价总列表
	 * 
	 * @return
	 *//*
	@RequestMapping("/institutionManger.action")
	public ModelAndView institutionManger() {
		ModelAndView result = new ModelAndView();
		UserEntity user = WebContext.getSessionUser();
		if (!"游客".equals(user.getPersonName())) {
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			TpUserEntity entity =new TpUserEntity();
			entity = userManagementService.searchById(userId);
			if ("A".equals(role) && "001".equals(entity.getDuty())) {
				result = new ModelAndView("/institution/manager/institutionManagerBack");
			} else if ("C".equals(role) || ("A".equals(role) && !"001".equals(entity.getDuty()))) {
				result = new ModelAndView("/institution/manager/institutionEvlutionStudentBack");
			}else if("B".equals(role)) {
				result = new ModelAndView("/institution/manager/institutionBack");
			}
		} else {
			result = new ModelAndView("/institution/manager/institutionEvlutionStudentBack");
		}
		
		return result;
	}
	

	*//**
	 * 标签一览
	 *//*
	@RequestMapping("/searchTagList2.action")
	@ResponseBody
	public ResultBean searchTagList2(TagInstitutionEntityView tagEntity) {
		return taxonomyInstitutionService.searchTagList2(tagEntity);

	}
	*//**
	 * 标签一览
	 *//*
	@RequestMapping("/searchInstitutionTagList2.action")
	@ResponseBody
	public ResultBean searchInstitutionTagList2(TagInstitutionEntityView tagEntity) {
		tagEntity.setCreateUser(WebContext.getSessionUserId());
		return taxonomyInstitutionService.searchInstitutionTagList2(tagEntity);

	}
	*//**
	 * 标签一览
	 *//*
	@RequestMapping("/searchTagSchoolList2.action")
	@ResponseBody
	public ResultBean searchTagSchoolList2(TagInstitutionEntityView tagEntity) {
		return taxonomyInstitutionService.searchTagList2(tagEntity);

	}
	*//**
	 * 分类基本列表
	 * 
	 * @return
	 *//*
	@RequestMapping("/institutioninit.action")
	public ModelAndView institutioninit() {
		ModelAndView mv = new ModelAndView("/institutiontaxonomy/manager/taxonomy_course_list");
		BaseEntity condition = new BaseEntity();
		condition.setPageSize(5);
		mv.addObject("condition", condition);
		return mv;
	}

	*//**
	 * 分类查询列表
	 * 
	 * @param businessType
	 * @return
	 *//*
	@RequestMapping("/searchTagTypeList2.action")
	@ResponseBody
	public List<TagTypeInstitutionView> searchTagTypeList2(String businessType) {

		return taxonomyInstitutionService.searchTagTypeList2(businessType);

	}

	*//**
	 * 
	 * 删除标签
	 * 
	 * @param tagIds
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/delTag.action")
	@ResponseBody
	public ResultBean delTag(String tagId, String ico) throws Exception {

		return taxonomyInstitutionService.delTag(tagId, ico);

	}

	*//**
	 * 保存信息
	 * 
	 * @param browserInfo
	 * @param sysInfo
	 * @param IpInfo
	 * @param operDetail
	 * @param operSite
	 * @return
	 *//*
	@RequestMapping("/insertUserInstitution.action")
	@ResponseBody
	public JSONArray insertUserInstitution(String browserInfo, String sysInfo, String IpInfo, String operDetail,
			String operSite) {
		UserOperEntity oper = new UserOperEntity();
		oper.setSysInfo(browserInfo + "(" + sysInfo + ")");
		oper.setOperDetail(operDetail);
		oper.setOperSite(operSite);
		oper.setOperTime(new Date());
		oper.setOperIP(IpInfo);
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		tagInstitutionEvalutionService.inserUserOper(oper);
		jo.put("result", "success");
		json.add(jo);
		return json;
	}

	*//**
	 * 编辑标签
	 * 
	 * @param TagEntity
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/updateTag.action")
	@ResponseBody
	public ResultBean updateTag(TagInstitutionEntity tagEntity) throws Exception {

		return taxonomyInstitutionService.updateTag(tagEntity);

	}

	*//**
	 * 保存新增标签
	 * 
	 * @param parentId 父标签ID,tagTypeId:标签类ID,tagName:标签名称
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveTag2.action")
	@ResponseBody
	public ResultBean saveTag2(TagInstitutionEntityView tagEntity) throws Exception {
		return taxonomyInstitutionService.saveTag2(tagEntity);

	}

	*//**
	 * 
	 * 初始化课程列表
	 * 
	 * @param condition 课程对象
	 * @return 返回课程列表初始化页面
	 *//*
	@RequestMapping("/institutionListInit.action")
	public ModelAndView institutionListInit(InstitutionMessageEntity condition) {
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		result.addObject("course", new InstitutionMessageEntity());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}

	*//**
	 * 
	 * 检索评价列表
	 * 
	 * @param queryInfo 机构对象
	 * @return 返回评价列表
	 *//*
	@RequestMapping("/listSearch.action")
	@ResponseBody
	public ResultBean listSearch(TbInstitutionEvalutionManagerView queryInfo) {
		queryInfo.setCreateUser(WebContext.getSessionUserId());
		return taxonomyInstitutionService.listSearch(queryInfo);
	}

	*//**
	 * 
	 * 检索评价问卷列表
	 * 
	 * @param queryInfo 机构对象
	 * @return 返回评价列表
	 *//*
	@RequestMapping("/listQestionSearch.action")
	@ResponseBody
	public ResultBean listQestionSearch(MeasurementInstitutionEntity queryInfo) {
		UserEntity user = WebContext.getSessionUser();	
		String role = user.getUserKbn();
		//判断权限
		String userId = user.getCreateUser();
		queryInfo.setCreateUser(userId);
		return taxonomyInstitutionService.listSchoolMeasurementSearch(queryInfo);
	}

	*//**
	 * 评价新增页面初始化
	 * 
	 * @return
	 *//*
	@RequestMapping("/addInit.action")
	public ModelAndView addInit() {
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_add");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		result.addObject("pageType", "add");
		result.addObject("course", new TbInstitutionEvalutionManagerView());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}

	*//**
	 * 搜素全部学校
	 * 
	 * @param query
	 * @return
	 *//*
	@RequestMapping("/searchSchoolAll.action")
	@ResponseBody
	public ResultBean searchSchoolAll(InstitutionEntity query) {
		return institutionService.getTeacherAll(query);
	}
	*//**
	 * 搜素评价问卷全部学校
	 * 
	 * @param query
	 * @return
	 *//*
	@RequestMapping("/searchEvalutionSchoolAll.action")
	@ResponseBody
	public ResultBean searchEvalutionSchoolAll(InstitutionEntity query) {
		if(null!=query && StringUtil.isNotBlank(query.getInstitutionNameseacher())) {
			query.setInstitutionName(query.getInstitutionNameseacher());
		}
		return institutionService.getEvalutionTeacherAll(query);
	}

	*//**
	 * 
	 * 根据学校ID，检索学校信息
	 * 
	 * @return
	 *//*
	@RequestMapping("/searchSchool.action")
	@ResponseBody
	public ResultBean searchSchool(String id) {
		ResultBean result = ResultBean.success();
		result.setStatus(true);
		return institutionService.searchSchool(id);
	}

	*//**
	 * 保存评价任务信息
	 * 
	 * @param courseJson 任务信息
	 * @return 返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/saveEvalutionInfo.action")
	@ResponseBody
	public ResultBean saveEvalutionInfo(String courseJson) throws Exception {
		TbInstitutionEvalutionManagerView course = JSONUtil.parse(
				JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson,
				TbInstitutionEvalutionManagerView.class);
		ResultBean result = ResultBean.success();
		course.setStatus("0");
		if (StringUtil.isEmpty(course.getId())) {
			institutionService.insertCourseCatagory(course);
		} else {
			institutionService.updateCourseAll(course);
		}
		return result;
	}

	*//**
	 * 暂存
	 * 
	 * @param courseJson
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/zancunEvalutionInfo.action")
	@ResponseBody
	public ResultBean zancunEvalutionInfo(String courseJson) throws Exception {
		TbInstitutionEvalutionManagerView course = JSONUtil.parse(
				JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson,
				TbInstitutionEvalutionManagerView.class);
		ResultBean result = ResultBean.success();
		course.setStatus("0");
		if (StringUtil.isEmpty(course.getId())) {
			institutionService.insertCourseCatagory(course);
		} else {
			institutionService.updateCourseAll(course);
		}
		return result;
	}

	*//**
	 * 删除评价任务信息
	 * 
	 * @param id
	 * @return
	 *//*
	@RequestMapping("/deleteEvalution.action")
	@ResponseBody
	public ResultBean deleteEvalution(String id) {
		ResultBean result = new ResultBean();
		institutionService.deleteEvalution(id);
		result.setMessages(getMessage(MSG_S_DELETE));
		return result;
	}

	@RequestMapping("/editInit.action")
	public ModelAndView editInit(String id) {
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_add");
		String status = "0";
		String type = "edit";
		TbInstitutionEvalutionManagerView course = tagInstitutionEvalutionService.searchEvalutionById(id, type);
		String evalutionId = course.getId();
		String institutionId = course.getTeacherId();
		List<TagLinkInstitutionEntity> entityList = new ArrayList<TagLinkInstitutionEntity>();
		TagLinkInstitutionEntity entity = new TagLinkInstitutionEntity();
		entity.setLinkId(evalutionId);
		entityList = tagInstitutionEvalutionService.searchList(entity);
		if (null != entityList) {
			for (int i = 0; i < entityList.size(); i++) {
				if (StringUtils.isBlank(entityList.get(i).getParentTagId())) {
					TagInstitutionEntity isentity = new TagInstitutionEntity();
					isentity.setTagId(entityList.get(i).getTagId());
					isentity = tagInstitutionService.searchFirstData(isentity);
					course.setFirstTagName(isentity.getTagName());
					course.setFirstTag(isentity.getTagName());
				} else {
					TagInstitutionEntity isentity = new TagInstitutionEntity();
					isentity.setTagId(entityList.get(i).getTagId());
					isentity = tagInstitutionService.searchFirstData(isentity);
					course.setSecondTag(isentity.getTagName());
					course.setSecondTagName(isentity.getTagName());
				}
			}
		}
		InstitutionEntity institution = new InstitutionEntity();
		institution = institutionService.selectInstitutionById(institutionId);
		course.setInstitutionName(institution.getInstitutionName());
		course.setSchoolType(institution.getSchoolType());
		course.setInstitutionAbbreviation(institution.getInstitutionAbbreviation());
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		result.addObject("tagsSecond", tagsSecond);
		result.addObject("course", course);
		result.addObject("pageType", type);
		return result;
	}

	*//**
	 * 问卷添加
	 * 
	 * @param courseId 评价任务ID
	 * @return
	 *//*
	@RequestMapping("/measureInit.action")
	@ResponseBody
	public ModelAndView measureInit(String courseId) {
		ModelAndView result = new ModelAndView("institution/manager/insitution_measure_add");
		MeasurementInstitutionEntity measure = new MeasurementInstitutionEntity();
		result.addObject("measure", measure);
		result.addObject("courseId", courseId);
		result.addObject("pageType", "add");
		return result;
	}
	*//**
	 * 问卷添加
	 * 
	 * @param courseId 评价任务ID
	 * @return
	 *//*
	@RequestMapping("/measureInit1.action")
	@ResponseBody
	public ModelAndView measureInit1(String courseId) {
		ModelAndView result = new ModelAndView("institution/manager/insitution_measure_editor");
		MeasurementInstitutionEntity measure = new MeasurementInstitutionEntity();
		measure.setLinkId(courseId);
		measure = measurementInstitutionService.findInstitutionMeasurement(measure);
		List<QuestionInstitutionEntity> questionList = measurementInstitutionService.searchQuestionList(measure.getId());
		measure.setQuestionList(questionList);
		result.addObject("measure", measure);
		result.addObject("courseId", courseId);
		result.addObject("pageType", "add");
		return result;
	}
	*//**
	 * 
	 * 问卷编辑页面初始化
	 * 
	 * @param courseId
	 * @return
	 *//*
	@RequestMapping("/measureInsitutionEditInit.action")
	@ResponseBody
	public ModelAndView measureEditInit(String courseId) {
		ModelAndView result = new ModelAndView("institution/manager/insitution_measure_add");
		MeasurementInstitutionEntity measure = measurementInstitutionService.seachMeasureById(courseId);
		result.addObject("measure", measure);
		result.addObject("courseId", courseId);
		result.addObject("isOnlyView", true);
		result.addObject("pageType", "edit");
		return result;
	}

	*//**
	 * 
	 * 问卷添加/编辑
	 * 
	 * @param measureJson
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveMeasure.action")
	@ResponseBody
	public ResultBean saveMeasure(String measureJson) throws Exception {
		MeasurementInstitutionEntity measure = JSONUtil.parse(
				JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), measureJson,
				MeasurementInstitutionEntity.class);
		ResultBean result = ResultBean.success();
		if (StringUtil.isEmpty(measure.getId())) {
			measurementInstitutionService.saveMeasure(measure);
		} else {
			measurementInstitutionService.updateInstitutionMeasure(measure);
		}
		return result;
	}

	*//**
	 * 
	 * 初始化评价问卷列表
	 * 
	 * @param condition 评价对象
	 * @return 返回课程列表初始化页面
	 *//*
	@RequestMapping("/institutionQuestListInit.action")
	public ModelAndView institutionQuestListInit(InstitutionMessageEntity condition) {
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_question_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		result.addObject("course", new InstitutionMessageEntity());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}

	*//**
	 * 
	 * 评价的发布/取消发布
	 * 
	 * @param id     课程id
	 * @param status 评价状态：0 未发布 1 已发布
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/publicEvalution.action")
	@ResponseBody
	public ResultBean publicEvalution(String id, String status) throws Exception {
		tagInstitutionEvalutionService.publicEvalution(id, status);
		return ResultBean.success(MSG_S_SUBMIT);
	}

	*//**
	 * 问卷显示页面
	 *//*
	@RequestMapping("/question.action")
	public ModelAndView question(String linkId, String linkType) {
		ModelAndView result = new ModelAndView("/institution/manager/institution_evalution_questionner_item");
		result.addObject("linkId", linkId);
		result.addObject("linkType", linkType);
		return result;
	}

	*//**
	 * 问卷题目选项
	 *//*
	@RequestMapping("/questionnaireDetial.action")
	@ResponseBody
	public ResultBean questionnaireDetial(String linkId, String linkType) {
		return questionInstitutionService.questionnaireDetial(linkId, linkType);
	}

	*//**
	 * 问卷提交
	 *//*
	@RequestMapping("/submitQuestionnaire.action")
	@ResponseBody
	public ResultBean submitQuestionnaire(String questionIds, String optionIds, String measureId) {
		return questionInstitutionService.submitQuestionnaire(questionIds, optionIds, measureId);
	}

	*//**
	 * 问卷调查结果列表
	 * 
	 * @author heyaqin
	 * @return 返回结果
	 *//*
	@RequestMapping("/questionResultList.action")
	public ModelAndView questionResultList(MeasurementInstitutionEntity condition) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_result_list");
		mv.addObject("condition", condition);
		UserEntity user = WebContext.getSessionUser();
		if (!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if (roleList.size() > 0) {
				for (UserRoleEntity role : roleList) {
					if (role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			} else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}

	*//**
	 * 问卷调查列表检索
	 * 
	 * @author
	 * @return 返回结果
	 *//*
	@RequestMapping("/questionResultListSearch.action")
	@ResponseBody
	public ResultBean questionResultListSearch(MeasurementInstitutionEntity condition) {
			UserEntity user = WebContext.getSessionUser();	
			String userId = user.getCreateUser();
			condition.setCreateUser(userId);
		return userManagementService.selectQuestionResultList(condition);
	}

	*//**
	 * 问卷统计列表初始化
	 * 
	 * @author
	 * @param id 问卷ID
	 * @return 返回结果
	 *//*
	@RequestMapping("/viewMeasurementListInit.action")
	public ModelAndView viewMeasurementListInit(String id) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_statistics");
		int optionNum = userManagementService.searchInstitutionTotalOptions(id) + 1;
		QuestionOptionInstitutionEntity questionOptionEntity = new QuestionOptionInstitutionEntity();
		questionOptionEntity.setMeasureId(id);
		mv.addObject("questionOptionEntity", questionOptionEntity);
		mv.addObject("optionNum", optionNum);
		// 问卷统计结果列表
		ArrayList<Map<String, String>> optionMapList = userManagementService.selectInstitutionOptionsList(id);
		mv.addObject("optionMapList", optionMapList);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 问卷统计列表检索
	 * 
	 * @author HEYAQIN
	 * @param id 问卷ID
	 * @return 返回结果
	 *//*
	@RequestMapping("/viewMeasurementList.action")
	@ResponseBody
	public ArrayList<Map<String, String>> viewMeasurementList(String id) {
		if (StringUtil.isEmpty(id)) {
			return null;
		} else {
			ArrayList<Map<String, String>> optionMapList = userManagementService.selectInstitutionOptionsList(id);
			return optionMapList;
		}
	}

	*//**
	 * 问卷参与人员 检索
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/viewUserSearch.action")
	@ResponseBody
	public ResultBean viewUserSeacher(String measurementId) {
		return userManagementService.selectEvalutionQuestionUserList(measurementId);
	}

	*//**
	 * 问卷参与人员回答详情
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/viewDetailInit.action")
	public ModelAndView viewDetailInit(UserMeasurementResultEntity condition) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_user_detail");
		if (StringUtil.isEmpty(condition.getMeasurementId())) {
			throw new BusinessException("参数错误!");
		}

		List<UserMeasurementResultEntity> list = measuermentService
				.searchInstitutionUserMeasurementResultByEntiy(condition);
		if (list != null && list.size() != 0) {
			mv.addObject("condition", list.get(0));
		} else {
			mv.addObject("condition", new UserMeasurementResultEntity());
		}
		return mv;
	}

	*//**
	 * 问卷回答详情
	 * 
	 * @param measurementId
	 * @param userId
	 * @return
	 *//*
	@RequestMapping("/viewUserAnswerSearch.action")
	@ResponseBody
	public ResultBean viewUserAnswerSearch(String measurementId, String userId) {
		return userManagementService.selectEvalutionAnswerUserList(measurementId, userId);
	}

	*//**
	 * 检索测试信息
	 * 
	 * @param linkId
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchMeasuerment.action")
	@ResponseBody
	public ResultBean searchMeasuerment(String measurementId, String userId) {
		if (StringUtil.isEmpty(measurementId)) {
			return ResultBean.error(getMessage("参数错误!"));
		}
		ResultBean result = ResultBean.success();
		List<QuestionInstitutionEntity> questionList = measurementInstitutionService
				.searchMeasuermentInstitutionNormalAndExam(userId, measurementId);
		result.setData(questionList);
		return result;
	}

	*//**
	 * 问卷参与人员 画面初始化
	 * 
	 * @param condition
	 * @return
	 *//*
	@SuppressWarnings("unchecked")
	@RequestMapping("/viewUserInit.action")
	public ModelAndView viewUserInit(MeasurementInstitutionEntity condition) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_user_list");
		mv.addObject("measurementId", condition.getId());
		List<MeasurementInstitutionEntity> list = measurementInstitutionService.search(condition);
		if (list != null && list.size() != 0) {
			mv.addObject("condition", list.get(0));
		} else {
			throw new BusinessException("参数错误!");
		}
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 删除问卷调查列表
	 * 
	 * @author heyaqin
	 * @return 返回结果
	 *//*
	@RequestMapping("/deleteQuestionList.action")
	@ResponseBody
	public ResultBean deleteQuestionList(String id) {
		userManagementService.deleteEvalutionQuestionList(id);
		return ResultBean.success();
	}

	*//**
	 * 导入页面初始化
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/importInit.action")
	public String importInit() {
		return "institution/manager/institution_import";
	}

	*//**
	 * 导入机构
	 * 
	 * @param linkId
	 * @return 返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/importUpload.action")
	@ResponseBody
	public ResultBean importCourse(@RequestParam("institution") CommonsMultipartFile institution,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		InputStream input = null;
		try {
			input = institution.getInputStream();
			File targetFile = FileUtil.writeToTmpFile(input);
			return institutionService.importInstitution(targetFile);
		} finally {
			IOUtils.closeQuietly(input);
		}
	}

	*//**
	 * 批量导出机构
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/exportInstitution.action")
	@ResponseBody
	public InstitutionManagerView exportInstitution(InstitutionEntity entity) throws Exception {
		List<InstitutionManagerView> viewList = new ArrayList<InstitutionManagerView>();
		UserEntity userEntity = WebContext.getSessionUser();
		if (!"游客".equals(userEntity.getPersonName())) {
			String role = userEntity.getUserKbn();
			String userId=userEntity.getUserId();
			if("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				entity.setCreateUser(userId);
				viewList = institutionService.exportInstitution(entity);
			}else if("B".equals(role)) {
				entity.setCreateUser(userId);
				viewList = institutionService.exportManagerInstitution(entity);
			}
		}
		
		InstitutionManagerView user = new InstitutionManagerView();
		user.setInstitutionList(viewList);
		ExcelDataBind edbUser = new ExcelDataBind(user);
		InputStream is = null;
		OutputStream os = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String date = sdf.format(new Date());
			String fileName = URLEncoder.encode("组织机构" + date + ".xlsx", "UTF-8");
			WebContext.getResponse().reset();
			is = InstitutionService.class.getResourceAsStream("institutionExportTemplate.xlsx");
			WebContext.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			WebContext.getResponse()
					.setContentType(" application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8");
			edbUser.bind(is, WebContext.getResponse().getOutputStream());
		} catch (IOException ex) {

		} finally {

			IOUtils.closeQuietly(is);
			IOUtils.closeQuietly(os);
		}
		return null;
	}

	*//**
	 * 增加机构方法
	 * 
	 * @param entity
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/addJyInstitution.action")
	public ModelAndView addJyInstitution(InstitutionEntity entity) throws Exception {
		ModelAndView mv = new ModelAndView("institution/manager/institution_add");
		InstitutionEntity institutionEntity = new InstitutionEntity();
		UserEntity user = WebContext.getSessionUser();
		String role = user.getUserKbn();
		//判断权限
		List<InstitutionEntity> schoollist=new ArrayList<InstitutionEntity>();
		String userId = user.getCreateUser();
		if ("A".equals(role)) {
			TpUserEntity tpUserEntity =new TpUserEntity();
			tpUserEntity = userManagementService.searchById(userId);
			institutionEntity.setInstitutionName(tpUserEntity.getSchoolName());
			institutionEntity=institutionService.seacherPersonEdution(institutionEntity);
			if(null!=institutionEntity) {
				entity.setInstitutionName(institutionEntity.getInstitutionName());
			}
			schoollist.add(institutionEntity);
		}else if("B".equals(role)) {
			institutionEntity.setCreateUser(userId);
			institutionEntity= institutionService.searchEdution(institutionEntity);
            if(null!=institutionEntity) {
            	entity.setInstitutionName(institutionEntity.getInstitutionName());
			}
            schoollist.add(institutionEntity);
		}
		mv.addObject("schoollist", schoollist);
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 添加学校信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/addInstitution.action")
	public ModelAndView addInstitution(String institutionParentId) throws Exception {
		InstitutionEntity entity = new InstitutionEntity();
		entity.setInstitutionParentId(institutionParentId);
		ModelAndView mv = new ModelAndView("institution/manager/institution_addSchool");
		List<InstitutionEntity> schoollist=new ArrayList<InstitutionEntity>();
		UserEntity user = WebContext.getSessionUser();
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				schoollist = institutionService.seacherSchoolLocationList(entity);
			} else if("B".equals(role)) {
				entity.setCreateUser(userId);
				schoollist= institutionService.searchEdutionSchoolList(entity);
			}
		
		mv.addObject("schoollist", schoollist);
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 删除班级
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/deleteInstitutionById.action")
	@ResponseBody
	public ResultBean deleteGradeById(String id) throws Exception {
		InstitutionEntity entity = new InstitutionEntity();
		entity.setId(id);
		institutionService.deleteInstitutionById(entity);
		return ResultBean.success(DELETE_GRADE_SUCCESS);
	}

	*//**
	 * 跳转到班级编辑页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/institutionEdit.action")
	public ModelAndView institutionEdit(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		ModelAndView mv = new ModelAndView("institution/manager/institution_edit");
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 检索班级信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchInstitution.action")
	@ResponseBody
	public ResultBean searchNotice(InstitutionEntity entity) {
		return institutionService.searchInstitutionEvalutionList(entity);
	}
	*//**
	 * 学校检索班级信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/schoolsearchInstitution.action")
	@ResponseBody
	public ResultBean schoolsearchInstitution(InstitutionEntity entity) {
		return institutionService.searchInstitutionEvalutionList1(entity);
	}
	*//**
	 * 检索班级信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchSchoolInstitution.action")
	@ResponseBody
	public ResultBean searchSchoolInstitution(InstitutionEntity entity) {
		ResultBean result = new ResultBean();
		UserEntity user = WebContext.getSessionUser();
		if (!"游客".equals(user.getPersonName())) {
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				entity.setCreateUser(userId);
				result=institutionService.searchInstitutionTeacherEvalutionList(entity);
			} else if ("C".equals(role)) {
				entity.setCreateUser(userId);
				StudentClassInfoEntity classInfo = new StudentClassInfoEntity();
				classInfo.setStudentId(userId);
				classInfo = studentClassInfoService.searchStudentClassInfo(classInfo);
			    if(null!=classInfo) {
			    	entity.setInstitutionName(classInfo.getSchoolName());
					result= institutionService.searchInstitutionEvalutionList(entity);
			    }
			}else if("B".equals(role)) {
				entity.setCreateUser(userId);
				result= institutionService.searchInstitutionManagerEvalutionList(entity);
			}
		}
		return result;
	}

	*//**
	 * 新增信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/insertInstitution.action")
	@ResponseBody
	public ResultBean insertInstitution(InstitutionEntity entity) throws Exception {
		ResultBean result = new ResultBean();
		InstitutionEntity institutionEntity = new InstitutionEntity();
		institutionEntity.setInstitutionName(entity.getInstitutionName());
		List<InstitutionEntity> datalist = (List<InstitutionEntity>) institutionService.searchInstitutionEvalutionList1(institutionEntity).getData();
		if(null!=datalist && datalist.size()>0) {
			result.setStatus(false);
			result = ResultBean.error("学校名称已存在");
		}else {
//			String institutionType=entity.getInstitutionType();
//			TagInstitutionEntity tagInstitutionEntity = new TagInstitutionEntity();
//			tagInstitutionEntity.setTagId(institutionType);
//			tagInstitutionEntity = tagInstitutionService.searchFirstData(tagInstitutionEntity);
//			entity.setInstitutionType(tagInstitutionEntity.getTagName());
			if(StringUtil.isNotBlank(entity.getSchoolNatrue())) {
				if("1".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("小学");
				}else if("2".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("初中");
				}else if("3".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("高中");
				}else if("4".equals(entity.getSchoolNatrue())) {
				   entity.setInstitutionType("九年一贯制");
			    }
			}
			institutionService.insertInstitution(entity, getCompressConfigByName("imageP"));
			result.setStatus(true);
			result= ResultBean.success(MSG_ADD_GRADE_SUCCESS);
		}
		return result;
	}
	*//**
	 * 新增信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/insertSchoolEvalutionInstitution.action")
	@ResponseBody
	public ResultBean insertSchoolEvalutionInstitution(InstitutionEntity entity) throws Exception {
		institutionService.insertInstitution(entity, getCompressConfigByName("imageP"));
		return ResultBean.success(MSG_ADD_GRADE_SUCCESS);
	}
	*//**
	 * 编辑保存
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/updateInstitution.action")
	@ResponseBody
	public ResultBean updateInstitution(InstitutionEntity entity) throws Exception {
		ResultBean result = new ResultBean();
		InstitutionEntity institutionEntity = new InstitutionEntity();
		institutionEntity.setInstitutionName(entity.getInstitutionName());
		List<InstitutionEntity> datalist = (List<InstitutionEntity>) institutionService.searchInstitutionEvalutionList1(institutionEntity).getData();
		if(null!=datalist && datalist.size()>0 && !entity.getId().equals(datalist.get(0).getId())) {
			result.setStatus(false);
			result = ResultBean.error("学校名称已存在");
		}else {
//			String institutionType=entity.getInstitutionType();
//			TagInstitutionEntity tagInstitutionEntity = new TagInstitutionEntity();
//			tagInstitutionEntity.setTagId(institutionType);
//			tagInstitutionEntity = tagInstitutionService.searchFirstData(tagInstitutionEntity);
//			entity.setInstitutionType(tagInstitutionEntity.getTagName());
			if(StringUtil.isNotBlank(entity.getSchoolNatrue())) {
				if("1".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("小学");
				}else if("2".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("初中");
				}else if("3".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("高中");
				}else if("4".equals(entity.getSchoolNatrue())) {
					   entity.setInstitutionType("九年一贯制");
				}
			}
			institutionService.updateInstitution(entity, getCompressConfigByName("imageP"));
			result.setStatus(true);
			result = ResultBean.success("创建学校成功");
		}
		return result;
	}
	*//**
	 * 编辑保存
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/updateSchoolEvalutionInstitution.action")
	@ResponseBody
	public ResultBean updateSchoolEvalutionInstitution(InstitutionEntity entity) throws Exception {
		entity.setCreateUser(WebContext.getSessionUserId());
		institutionService.updateInstitution(entity, getCompressConfigByName("imageP"));
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/institutionStudentlist.action")
	public ModelAndView institutionStudentlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/institution/manager/institution_student_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		return result;
	}

	*//**
	 * 跳转到班级查看页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/institutionLook.action")
	public ModelAndView institutionLook(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		ModelAndView mv = new ModelAndView("institution/manager/institution_look");
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 老师分享作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/shareWork.action")
	@ResponseBody
	public ResultBean shareWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.shareHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 老师取消分享作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/cancleShareWork.action")
	@ResponseBody
	public ResultBean cancleShareWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.cancleShareHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);

	}

	*//**
	 * 老师置顶作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/topWork.action")
	@ResponseBody
	public ResultBean topWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.topHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 老师取消置顶作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/cancleTopWork.action")
	@ResponseBody
	public ResultBean cancleTopWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.cancleTopHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 老师删除作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/deleteWork.action")
	@ResponseBody
	public ResultBean deleteWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.deleteWork(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/baseinstitutionlist.action")
	public ModelAndView baseinstitutionlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/baseinstitution/manager/base_institution_list");
		result.addObject("condition", condition);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/schoolbaseinstitutionlist.action")
	public ModelAndView schoolbaseinstitutionlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/baseinstitution/manager/base_school_institution_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 增加机构方法
	 * 
	 * @param entity
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/addBaseInstitution.action")
	public ModelAndView addBaseInstitution(InstitutionEntity entity) throws Exception {
		ModelAndView mv = new ModelAndView("baseinstitution/manager/base_institution_add");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList1(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		List<TagInstitutionEntity> tagList = tagInstitutionService.searchTagInstitutionAll();
		mv.addObject("tagList", tagList);
		mv.addObject("catagoryTags", tags);
		mv.addObject("tagsSecond", tagsSecond);
		mv.addObject("institution", entity);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 跳转到班级编辑页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/baseinstitutionEdit.action")
	public ModelAndView baseinstitutionEdit(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		ModelAndView mv = new ModelAndView("baseinstitution/manager/base_institution_edit");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList1(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		List<TagInstitutionEntity> tagList = tagInstitutionService.searchTagInstitutionAll();
        String secondId = entity.getFirstTag();
        TagInstitutionEntity taginfo = tagInstitutionService.searchTagInstitution(secondId);
        entity.setSecondTag(taginfo.getParentId());
		mv.addObject("tagList", tagList);
		mv.addObject("catagoryTags", tags);
		mv.addObject("tagsSecond", tagsSecond);
		mv.addObject("institution", entity);
		mv = getRoleAndNum(mv);
		return mv;
	}
	
	*//**
	 * 跳转到班级编辑页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/baseSchoolinstitutionEdit.action")
	public ModelAndView baseSchoolinstitutionEdit(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		InstitutionEntity institutionEntity = new InstitutionEntity();
		ModelAndView mv = new ModelAndView("baseinstitution/manager/base_school_institution_edit");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList1(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		List<TagInstitutionEntity> tagList = tagInstitutionService.searchTagInstitutionAll();
		List<InstitutionEntity> schoollist=new ArrayList<InstitutionEntity>();
		UserEntity user = WebContext.getSessionUser();
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			if(StringUtil.isNotBlank(entity.getInstitutionParentId())) {
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				schoollist = institutionService.seacherSchoolLocationList(entity);
			} else if("B".equals(role)) {
				entity.setCreateUser(userId);
				schoollist= institutionService.searchEdutionSchoolList(entity);
			}
			
		}else {
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				institutionEntity.setInstitutionName(tpUserEntity.getSchoolName());
				institutionEntity=institutionService.seacherPersonEdution(institutionEntity);
				entity.setInstitutionName(institutionEntity.getInstitutionName());
				schoollist.add(institutionEntity);
			}else if("B".equals(role)) {
				institutionEntity.setCreateUser(userId);
				institutionEntity= institutionService.searchEdution(institutionEntity);
				entity.setInstitutionName(institutionEntity.getInstitutionName());
				schoollist.add(institutionEntity);
			}
		}
		
		mv.addObject("schoollist", schoollist);
		mv.addObject("tagList", tagList);
		mv.addObject("catagoryTags", tags);
		mv.addObject("tagsSecond", tagsSecond);
		mv.addObject("institution", entity);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 分类基本列表
	 * 
	 * @return
	 *//*
	@RequestMapping("/baseinstitutioninit.action")
	public ModelAndView baseinstitutioninit() {
		ModelAndView mv = new ModelAndView("baseinstitutiontaxonomy/manager/base_taxonomy_course_list");
		BaseEntity condition = new BaseEntity();
		condition.setPageSize(5);
		mv.addObject("condition", condition);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 学校列表页面
	 *//*
	@RequestMapping("/schoolListInit.action")
	public ModelAndView schoolListInit(String schoolName) {
		ModelAndView result = new ModelAndView("baseinstitution/manager/school_list");
		String userId= WebContext.getSessionUserId();
		ManagerUserEntity entity= managerUserService.searchByUserId(userId);
		String mangerRole="0";
		if(null!=entity || "2542668".equals(userId)) {
			mangerRole="1";
		}else {
			mangerRole="0";
		}
		result.addObject("schoolName", schoolName);
		result.addObject("mangerRole", mangerRole);
		result = getRoleAndNum(result);
		return result;
	}

	*//**
	 * 检索课程 包括分类
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 * directionTagId 学段
	 * categoryTagId 分类
	 *//*
	@RequestMapping("/schoolList.action")
	@ResponseBody
	public ResultBean schoolList(InstitutionEntity course, String directionTagId, String categoryTagId,
			String courseStage, String sortType, Integer pageSize, Integer pageNumber,String schoolStage) {
		ResultBean result = new ResultBean();
		if(StringUtil.isNotBlank(course.getSchoolName())) {
			course.setInstitutionName(course.getSchoolName());
		}
		//区域
		if(StringUtil.isNotBlank(directionTagId)) {
			course.setFirstTag(directionTagId);
		}
		if(StringUtil.isNotBlank(directionTagId)) {
			TagInstitutionEntity entity = new TagInstitutionEntity();
			entity.setTagId(directionTagId);
			entity = tagInstitutionService.searchFirstData(entity);
			String xdName = entity.getTagName();
			course.setInstitutionType(xdName);
		}
		if(StringUtil.isNotBlank(categoryTagId)) {
			TagInstitutionEntity entity = new TagInstitutionEntity();
			entity.setTagId(categoryTagId);
			entity = tagInstitutionService.searchFirstData(entity);
			String xdName = entity.getTagName();
			course.setFirstTag(categoryTagId);
		}
		//学校阶段
		if(StringUtil.isNotBlank(schoolStage)) {
			course.setSchoolNatrue(schoolStage);
		}
		//权限查看
		String userId = WebContext.getSessionUserId();
		ManagerUserEntity entity= managerUserService.searchByUserId(userId);
		//教育局的人员暂时看到全部
		if(null!=entity || "2542668".equals(userId) || "B".equals(WebContext.getSessionUser().getUserKbn())) {
			result = courseService.schoolList(course, directionTagId, categoryTagId, courseStage, sortType,
					pageSize, pageNumber);
		}else {
			String role = WebContext.getSessionUser().getUserKbn();
			course.setCreateUser(userId);
			if("A".equals(role)) {
				result = courseService.schooTeacherlList(course, directionTagId, categoryTagId, courseStage, sortType,
						pageSize, pageNumber);
			}else if("C".equals(role)) {
				result = courseService.schooStudentlList(course, directionTagId, categoryTagId, courseStage, sortType,
						pageSize, pageNumber);
			}
		}
		
		return result;
	}
	*//**
	 * 日志操作页面 检索列表
	 * 
	 * @param condition
	 * @return 返回结果
	 *//*
	@RequestMapping("/logsearch.action")
	@ResponseBody
	public ResultBean logsearch(UserOperEntity condition) {
		return courseService.search4Page(condition);
	}
	*//**
	 * 已完成
	 * @param finishType
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 *//*
	@RequestMapping("/userCourseFinishList.action")
	@ResponseBody
	public ResultBean userCourseFinishList(String finishType, Integer pageSize, Integer pageNumber) {
		finishType ="3";
		pageSize =5;
		pageNumber=1;
		// 检索我的课程
		ResultBean result = courseService.userCourseList(finishType, pageSize, pageNumber);
		return result;
	}
	*//**
	 * 已完成
	 * @param finishType
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 *//*
	@RequestMapping("/userCourseUnFinishList.action")
	@ResponseBody
	public ResultBean userCourseUnFinishList(String finishType, Integer pageSize, Integer pageNumber) {
		pageSize =5;
		pageNumber=1;
		// 检索我的课程
		ResultBean result = courseService.userCourseUnFinishList(finishType, pageSize, pageNumber);
		return result;
	}
    *//**
                  * 作业列表
     * @return
     *//*
	@RequestMapping("/zuoyeList.action")
	@ResponseBody
	public ResultBean zuoyeList() {
		// 检索我的课程
		ResultBean result = courseService.userZyList();
		return result;
	}
    *//**
     * 
                  * 考试结果查看
     * @param UserMeasurementResultEntity condition 入参
     * @return
     *//*
    @RequestMapping("/userExamDetail.action")
    public ModelAndView userExamDetail(UserMeasurementResultEntity condition) {
    	//考试结果页面
        ModelAndView result = new ModelAndView("course/manager/user_exam_detail");
        //考试结果id是否存在
        if(StringUtil.isEmpty(condition.getMeasurementId())){
            throw new BusinessException("参数错误!");
        }
        condition.setCreateUser(WebContext.getSessionUserId());
        //查询考试结果
        List<UserMeasurementResultEntity> list = measuermentService.searchUserMeasurementResultByEntiy(condition);
        if(list != null && list.size() != 0) {
            result.addObject("condition", list.get(0));
        } else {
            result.addObject("condition", new UserMeasurementResultEntity());
        }
        //返回考试结果
        return result;
    }
	*//**
	 * 查询全部考试
	 * @param ChapterEntity chapter章节参数
	 * @param pageSize  每页展示多少条数据
	 * @param pageNumber 页码
	 * @return
	 *//*
	@RequestMapping("/myAllExam.action")
	public ModelAndView myAllExam(ChapterEntity chapter, Integer pageSize, Integer pageNumber) {
		//考试页面内容展示路径
		ModelAndView mv = new ModelAndView("baseinstitution/manager/my_exam_list");
		//查询数据表
		ResultBean result = chapterService.myAllxam(chapter, pageSize, pageNumber);
		mv.addObject("chapter", result.getData());
		return mv;
	}
	*//**
	 *检索考试
	 * @param chapter
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 *//*
	@RequestMapping("/myAllExamList.action")
	@ResponseBody
	public ResultBean myAllExamList(ChapterEntity chapter, Integer pageSize, Integer pageNumber) {
		ResultBean result = chapterService.myAllxam(chapter, pageSize, pageNumber);
		return result;
	}
	*//**
	 * 查询课工厂方法
	 * @param courseId 课程id
	 * @param content 课程内容
	 * @param type 课程类型
	 * @return
	 *//*
	@RequestMapping("/factory.action")
    @ResponseBody
    public ResultBean factory(String courseId,String content,String type)  {
        ResultBean result = new ResultBean();
        result = courseService.factory(courseId,content,type);
        return result;
    }
	*//**
	 * 用户作业列表
	 * @return
	 *//*
	@RequestMapping("/userHomeWork.action")
	public ModelAndView userHomeWork() {
		//作业列表路径
		ModelAndView result = new ModelAndView("/course/client/user_home_work");
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 查询作业列表
	 * @return
	 *//*
	@RequestMapping("/userHomeWorkSearch.action")
	@ResponseBody
	public ResultBean search(){
		//返回结果
		return courseService.searchUserHomeWork4Page();
	}
	*//**
	 * 查询学校信息
	 * @return
	 *//*
	@RequestMapping("/selectSchoolNameInfo.action")
	@ResponseBody
	public ResultBean selectSchoolNameInfo() {
		ResultBean result = new ResultBean();
		//获取登录信息
		String userId = WebContext.getSessionUserId();
		//获取角色信息
		String role = WebContext.getSessionUser().getUserKbn();
		//判断角色
		if("C".equals(role)) {
			//学生角色
			StudentClassInfoEntity entity =new StudentClassInfoEntity();
			entity.setStudentId(userId);
			List<StudentClassInfoEntity> stuList = studentClassInfoService.searchStudentClassInfoList(entity);
			stuList.get(0).setUserId(userId);
			//设置返回值数据
			result.setData(stuList.get(0));
		}else {
			//教师信息
			TpUserEntity tpUserEntity = userManagementService.searchById(userId);
			result.setData(tpUserEntity);
		}
		 
		return result;
	}
	*//**
	 * 结束课程方法
	 * @param id 课程id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/endCourse.action")
	@ResponseBody
	public ResultBean endCourse(String id) throws Exception {
		courseService.endCourse(id);
		return ResultBean.success(MSG_S_SUBMIT);
	}
	*//**
	 * 图书馆课程复制
	 * @param id 课程id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/toLibCourse.action")
	@ResponseBody
	public ResultBean toLibCourse(String id) throws Exception {
		courseService.toLibCourse(id);
		return ResultBean.success(MSG_S_SUBMIT);
	}
	*//**
	 * 课程复制方法
	 * @param id 课程主键
	 * @return
	 *//*
	@RequestMapping("/copyInfo.action")
    @ResponseBody
    public ResultBean copyInfo(String id)  {
        ResultBean result = new ResultBean();
        //课程复制实现方法
        courseService.copyCourseByHtml(id);
        result.setMessages(getMessage(MSG_S_SAVE));
        return result;
    }
    *//**
                 * 新增课程活动记录方法
     * @param courseId 课程id
     * @return
     *//*
	@RequestMapping("/addActiveInit.action")
	public ModelAndView addActiveInit(String courseId) {
	//课程活动新增页面路径
	ModelAndView result = new ModelAndView("course/manager/course_active_add");
	TagView tagQuery = new TagView();
	//一级分类对象
	List<TagView> tags = courseService.searchTagList2(tagQuery);
	//二级分类对象
	List<TagView> tagsSecond = courseService.searchTagSecondList2(tagQuery);
	//查询章节信息
	ChapterEntity entity = new ChapterEntity();
	entity.setCourseId(courseId);
	List<ChapterEntity> chapterList = courseService.selectByCourseId(entity);
	//返回前台对象
	result.addObject("catagoryTags", chapterList);
	//result.addObject("catagoryTags", tags);
	result.addObject("pageType", "add");
	result.addObject("course", new CourseManagerView());
	result.addObject("tagsSecond", tagsSecond);
	result = getRoleAndNum(result);
	return result;
	}
    *//**
                  * 保存课程章节活动记录信息
     * @param courseJson 课程信息
     * @return 返回结果
     * @throws Exception 
     *//*
    @RequestMapping("/saveActiveChapterInfo.action")
    @ResponseBody
    public ResultBean saveActiveChapterInfo(String courseJson) throws Exception {
    	//实例化对象
        CourseManagerView courseview = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, CourseManagerView.class);
    	ChapterActiveEntity course = new ChapterActiveEntity();
    	course.setChapterId(courseview.getChapterId());
    	course.setActiveFlag(courseview.getActiveFlag());
    	//根据章节id查询章节信息
    	ChapterEntity view= new ChapterEntity();
    	String chapterId = courseview.getChapterId();
    	view = chapterService.selectChapteById(chapterId);
    	//设置章节字段进行保存
    	course.setChapterTitle(view.getTitle());
    	course.setContent(courseview.getContent());
        ResultBean result = ResultBean.success();
        //保存章节活动内容
        chapterActiveService.insertInfo(course);
        return result;
    }
   *//**
             * 查询课程章节阶段活动记录信息
    * @param activeFlag
    * @param chapterId
    * @return
    *//*
	@RequestMapping("/searchActiveChapterInfo.action")
	@ResponseBody
	public ResultBean searchActiveChapterInfo(String activeFlag,String chapterId){
	ChapterActiveEntity course = new ChapterActiveEntity();
	//判断活动记录标识是否为空
	if(StringUtil.isNotBlank(activeFlag)) {
		course.setActiveFlag(activeFlag);
	}
	course.setChapterId(chapterId);
	//查询该章节的全部活动记录
	List<ChapterActiveEntity> view = chapterActiveService.searchList(course);
	ResultBean result = ResultBean.success();
	result.setData(view);
	return result;
	}
	*//**
	 * 查询课程阶段活动记录信息
	 * @param activeFlag 活动记录阶段标识
	 * @param courseId  课程主键
	 * @param chapterId  章节主键
	 * @return
	 *//*
	@RequestMapping("/searchActiveAllChapterInfo.action")
	@ResponseBody
	public ResultBean searchActiveAllChapterInfo(String activeFlag,String courseId,String chapterId){
	ChapterActiveEntity course = new ChapterActiveEntity();
	//判断活动记录标识是否为空
	if(StringUtil.isNotBlank(activeFlag)) {
		course.setActiveFlag(activeFlag);
	}
	course.setCourseId(courseId);
	if(StringUtil.isNotBlank(chapterId)) {
		course.setChapterId(chapterId);
	}
	//返回查询结果
	return chapterActiveService.searchAllCourseChapterPage(course);
	}
	*//**
	 * 查询角色的公共方法
	 * @param result
	 * @return
	 *//*
	private ModelAndView getRoleAndNum(ModelAndView result) {
    	int num = courseService.getStuStatus();
    	result.addObject("num", num);
    	UserEntity user = WebContext.getSessionUser();
    	//判断是否管理员
    	ManagerUserEntity entity = managerUserService.searchByUserId(user.getUserId());
    	if(!user.getUserName().equals("guest")) {
    		List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
    		//老师和教育局角色
    		if(roleList.size() > 0 && "A".equals(user.getUserKbn()) && null==entity && !"2542668".equals(user.getUserId())) {
    			for(UserRoleEntity role : roleList) {
    				if(role.getRoleId().equals("01")) {
    					result.addObject("role", "t");
    				}
    			}
    		//教育局角色	
    		}else if(roleList.size() > 0  && (null!=entity || "2542668".equals(user.getUserId()))){
    			result.addObject("role", "m");
    		}else if("B".equals(user.getUserKbn())){
    			result.addObject("role", "e");
    	    //家长和教研员角色
    		}else if("D".equals(user.getUserKbn()) || "E".equals(user.getUserKbn())){
    			result.addObject("role", "q");
    		//学生角色
    		}else {
    			result.addObject("role", "s");
    		}
    	}
        //管理员角色
    	if(null!=entity || "2542668".equals(user.getUserId())) {
    		result.addObject("manager", "1");
    	}else {
    		result.addObject("manager", "0");
    	}
    	return result;
    }
    *//**
     * 活动记录编辑页面方法
     * @param courseId  课程id
     * @param activeId  活动id
     * @return
     *//*
	@RequestMapping("/editActiveInit.action")
	public ModelAndView editActiveInit(String courseId,String activeId) {
	//活动编辑页面路径
	ModelAndView result = new ModelAndView("course/manager/course_active_editor");
	TagView tagQuery = new TagView();
	//查询一级分类方法
	List<TagView> tags = courseService.searchTagList2(tagQuery);
	//查询e二级分类方法
	List<TagView> tagsSecond = courseService.searchTagSecondList2(tagQuery);
	//根据课程id查询所有对应的章节信息
	ChapterEntity entity = new ChapterEntity();
	entity.setCourseId(courseId);
	List<ChapterEntity> chapterList = courseService.selectByCourseId(entity);
	//根据活动记录id查询活动章节信息
	ChapterActiveEntity active = new ChapterActiveEntity();
	active.setId(activeId);
	List<ChapterActiveEntity> activeList = chapterActiveService.searchList(active);
	//设置返回前台的字段信息
	CourseManagerView courseManagerView = new  CourseManagerView();
	courseManagerView.setChapterId(activeList.get(0).getChapterId());
	courseManagerView.setChapterTitle(activeList.get(0).getChapterTitle());
	courseManagerView.setActiveFlag(activeList.get(0).getActiveFlag());
	courseManagerView.setContent(activeList.get(0).getContent());
	//章节对象
	result.addObject("catagoryTags", chapterList);
	//result.addObject("catagoryTags", tags);
	result.addObject("pageType", "add");
	result.addObject("activeId", activeId);
	//课程对象
	result.addObject("course",courseManagerView);
	result.addObject("tagsSecond", tagsSecond);
	result = getRoleAndNum(result);
	return result;
	}
    *//**
                * 保存活动记录课程章节信息
	* @param courseJson 课程信息
	* @return 返回结果
	* @throws Exception 
	*//*
	@RequestMapping("/updateActiveChapterInfo.action")
	@ResponseBody
	public ResultBean updateActiveChapterInfo(String courseJson) throws Exception {
    //转化实例对象方法
	CourseManagerView courseview = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, CourseManagerView.class);
	ChapterActiveEntity course = new ChapterActiveEntity();
	//删除当前活动记录
	ChapterActiveEntity entity = new ChapterActiveEntity();
	String id = courseview.getActiveId();
	entity.setId(id);
	chapterActiveService.deleteInfo(entity);
	//设置保存课程的信息字段
	course.setChapterId(courseview.getChapterId());
	course.setActiveFlag(courseview.getActiveFlag());
	//查询章节信息
	ChapterEntity view= new ChapterEntity();
	String chapterId = courseview.getChapterId();
	view = chapterService.selectChapteById(chapterId);
	course.setChapterTitle(view.getTitle());
	course.setContent(courseview.getContent());
	ResultBean result = ResultBean.success();
	//保存活动记录的课程章节信息
	chapterActiveService.insertInfo(course);
	return result;
	}
    *//**
                 * 删除活动
	* @param courseJson 课程信息
	* @return 返回结果
	* @throws Exception 
	*//*
	@RequestMapping("/activeDelete.action")
	@ResponseBody
	public ResultBean activeDelete(String id) throws Exception {
	ChapterActiveEntity entity = new ChapterActiveEntity();
	//设置活动记录主键
	entity.setId(id);
	chapterActiveService.deleteInfo(entity);
	ResultBean result = ResultBean.success();
	return result;
	}
	*//**
	 * 添加页面初始化：节
	 * 
	 * @param courseId 课程ID
	 * @return
	 *//*
	@RequestMapping("/courseSmallInit.action")
	public ModelAndView courseSmallInit(String courseId, String parentId) {
		ModelAndView result = new ModelAndView("course/manager/course_three_edit");
		ChapterView chapter = new ChapterView();
		chapter.setMeasureType("4");
		result.addObject("courseId", courseId);
		result.addObject("parentId", parentId);
		result.addObject("chapterFlag", SMALL_CHAPTER);
		result.addObject("chapter", chapter);
		return result;
	}
	*//**
	 * 保存章信息
	 * 
	 * @param chapterJson
	 * @return 返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/saveExamInfo.action")
	@ResponseBody
	public ResultBean saveExamInfo(String chapterJson, String videoLength) throws Exception {
		//将字符串转化成章节对象
		ChapterView chapter = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateMinuteFormatDefualt()),
				chapterJson, ChapterView.class);
		ResultBean result = ResultBean.success();
		//设置章节状态0未发布，1已发布
		chapter.setStatus("1");
		//设置加入课程1是已加入
		chapter.setIsCourse("1");
		if (StringUtil.isEmpty(chapter.getId())) {
			//未存在的章节考试直接保存
			chapterService.insertChapterInfoAll(chapter, videoLength);
		} else {
			//已存在的章节考试更新信息
			chapterService.updateChapterAll(chapter, videoLength);
		}
		//返回信息
		return result;
	}
    *//**
     * 
                  * 查看考试结果
     * @param condition
     * @return
     *//*
    @SuppressWarnings({ "unchecked"})
    @RequestMapping("/userExamInfoInit.action")
    public ModelAndView userExamInfoInit(MeasurementEntity condition) {
        ModelAndView result = new ModelAndView("course/manager/user_exam_info");
        //参数为空抛出异常
        if(StringUtil.isEmpty(condition.getLinkId())){
            throw new BusinessException("参数错误!");
        }
        //根据参数查询考试结果
        List<MeasurementEntity> list = courseService.search(condition);
        if(list != null && list.size() != 0) {
            result.addObject("condition", list.get(0));
        } else {
            throw new BusinessException("参数错误!");
        }
        //返回考试信息
        return result;
    }
    *//**
                 * 根据课程id获取章节考试状态信息
     * @param courseId
     * @return
     * @throws Exception
     *//*
	@RequestMapping("/searchExamStatus.action")
	@ResponseBody
	public ChapterView searchExamStatus(String courseId) throws Exception {
		//查询章节信息
		ChapterView view = chapterService.seacrhExamStatus(courseId);
		return view;
	}
	@SuppressWarnings("unchecked")
	*//**
	* 前端请求-章节详情
	* @param chapterId 章节ID
	* @return 
	*//*
	@RequestMapping("/toChapterDetail.action")
	public ModelAndView toChapterDetail(String courseId,String chapterId){
		ModelAndView mv = new ModelAndView("course/client/course_chapter_detail");
		ResultBean result = courseService.searchCourseDetail(courseId);
		CourseView courseView = (CourseView) result.getData();
		mv.addObject("courseView", courseView);
		mv.addObject("note", new NoteDataEntity());
		mv.addObject("homeWork", new HomeworkView());
		result = chapterService.chapterDetail(courseId, chapterId);
		ChapterView chapterView = (ChapterView) result.getData();
		ChapterEntity entity = new ChapterEntity();
		entity.setCourseId(courseId);
		List<ChapterEntity> chapterCourseList = courseService.selectByCourseId(entity);
		mv.addObject("chapterCourseList", chapterCourseList);
		mv.addObject("chapterView", chapterView);
		//检索章节列表
		List<ChapterView> cvs = (List<ChapterView>) chapterService.chapterList(courseId);
		mv.addObject("chapterList", cvs);
		//相关课程
		result = courseService.recommendedCourses("5", courseId);
		List<CourseEntity> otherCourseList = (List<CourseEntity>) result.getData();
		mv.addObject("otherCourseList", otherCourseList);
		//判断是否需要检索测试信息
		if(CHAPTER_TYPE_MEASUERMENT.equals(chapterView.getMeasureType().toString())){
			result = measuermentService.searchMeasuerment(chapterId, LINK_TYPE_CHAPTER);
			List<QuestionEntity> questionList = (List<QuestionEntity>) result.getData();
			mv.addObject("questionList", questionList);
		}
		//判断是否考试章节
		if(CHAPTER_TYPE_EXAM.equals(chapterView.getMeasureType())){
			//考试章节
			//取用户对于章节的考试信息
			MeasurementEntity me = new MeasurementEntity();
			me.setLinkId(chapterId);
			me.setLinkType(LINK_TYPE_CHAPTER);
			me = measuermentService.findMeasurement(me);
			if(me!=null){
				UserMeasurementResultEntity um = new UserMeasurementResultEntity();
				um.setMeasurementId(me.getId());
				um.setUserId(WebContext.getSessionUserId());
				um = measuermentService.findUserExamResult(um);
				if(um != null && um.getExamStatus()!=null && um.getExamStatus().equals(EXAM_STATUS_END)){
					//已参加过考试提交了
					mv.addObject("goal", um.getGoal()+"");
					mv.addObject("totalGoal", um.getTotalGoal()+"");
					mv.addObject("examTime", DateUtil.formatAny(um.getCreateDate(), ConfigUtil.getDateTimeFormatDefualt()));
				}
			}
		}
		//是否教师
		mv.addObject("isTeacher", courseService.isTeacher(courseView.getTeacherId()));
		mv.addObject("useingChapterId", chapterId);
		String content = chapterView.getContent();
		if(content != null && !"".equals(content)) {
			String ipAndPort = getIpAndPort(content);
			if(!"".equals(ipAndPort)) {
				String serverName = WebContext.getRequest().getServerName();
				int serverPort = WebContext.getRequest().getServerPort();
				content = content.replace(ipAndPort, serverName+":"+serverPort);
				chapterView.setContent(content);
			}
		}
		
		return mv;
	}
	*//**
	 * 判断str中是否有IP和port，若有则取出返回
	 * @param str
	 * @return String
	 *//*
	private static String getIpAndPort(String str) {
		String returnStr = "";
		Pattern p = Pattern.compile("(\\d+\\.\\d+\\.\\d+\\.\\d+)\\:(\\d+)");
		Matcher m = p.matcher(str);
		if(m.find()) {
			returnStr += m.group(1)+":"+m.group(2);
		}
		
		return returnStr;
	}
	*//**
	 * 标签一览
	 * @param flag 是否显示标签 0 不显示 1 显示
	 * @param businessType 业务类型
	 * @return
	 *//*
	@RequestMapping("/searchSchhoolCategory.action")
	@ResponseBody
	public ResultBean searchSchhoolCategory(String flag,String businessType){
		ResultBean  result=tagService.searchSchhoolCategory(flag,businessType);
		return result;
	}
	*//**
	 * 
	 * 初始化待办课程列表
	 * 
	 * @param condition 课程对象
	 * @return 返回课程列表初始化页面
	 *//*
	@RequestMapping("/institutionDbListInit.action")
	public ModelAndView institutionDbListInit(InstitutionMessageEntity condition) {
		//设置代办页面
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_db_list");
		//设置分页参数
		condition.setPageSize(10);
		result.addObject("condition", condition);
		TagInstitutionView tagQuery = new TagInstitutionView();
		//一级标签信息查询
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		//二级标签信息查询
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		//返回课程实例对象
		result.addObject("course", new InstitutionMessageEntity());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}
	*//**
	 * 
	 * 初始化已办课程列表
	 * 
	 * @param condition 课程对象
	 * @return 返回课程列表初始化页面
	 *//*
	@RequestMapping("/institutionYbListInit.action")
	public ModelAndView institutionYbListInit(InstitutionMessageEntity condition) {
		//设置已办页面
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_yb_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		TagInstitutionView tagQuery = new TagInstitutionView();
		//一级标签信息查询
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		//二级标签信息查询
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		//返回课程实例对象
		result.addObject("course", new InstitutionMessageEntity());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}
	*//**
	 * 
	 * 检索评价列表
	 * 
	 * @param queryInfo 机构对象
	 * @return 返回评价列表
	 *//*
	@RequestMapping("/listdbSearch.action")
	@ResponseBody
	public ResultBean listdbSearch(MeasurementInstitutionEntity queryInfo) {
		UserEntity user = WebContext.getSessionUser();	
		//获取人员角色
		String role = user.getUserKbn();
		//获取登录人账号
		String userId = user.getCreateUser();
		//教师角色
		if("A".equals(role)) {
			TpUserEntity tpUserEntity =new TpUserEntity();
			//查询教师信息
			tpUserEntity = userManagementService.searchById(userId);
		    if(null!=tpUserEntity) {
		    	queryInfo.setInstitutionName(tpUserEntity.getSchoolName());
		    }
		    //学生角色
		}else if("C".equals(role)) {
			//查询学生信息
			StudentClassInfoEntity classInfo = new StudentClassInfoEntity();
			classInfo.setStudentId(userId);
			classInfo = studentClassInfoService.searchStudentClassInfo(classInfo);
			if(null!=classInfo && StringUtil.isNotBlank(classInfo.getSchoolName())) {
				queryInfo.setInstitutionName(classInfo.getSchoolName());
			}
		}
		
		return taxonomyInstitutionService.listdbSearch(queryInfo);
	}
	*//**
	 * 
	 * 检索已办评价列表
	 * 
	 * @param queryInfo 机构对象
	 * @return 返回评价列表
	 *//*
	@RequestMapping("/listybSearch.action")
	@ResponseBody
	public ResultBean listybSearch(MeasurementInstitutionEntity queryInfo) {
		UserEntity user = WebContext.getSessionUser();	
		//获取人员角色
		String role = user.getUserKbn();
		//登录账号
		String userId = user.getCreateUser();
		//教师角色
	    if("A".equals(role)) {
	    	//查询用户信息
	    	TpUserEntity tpUserEntity =new TpUserEntity();
			tpUserEntity = userManagementService.searchById(userId);
			if(null!=tpUserEntity) {
				//设置学校名称
				queryInfo.setInstitutionName(tpUserEntity.getSchoolName());
			}
	    }else if("C".equals(role)) {
	    	//查询学生信息
			StudentClassInfoEntity classInfo = new StudentClassInfoEntity();
			classInfo.setStudentId(userId);
			classInfo = studentClassInfoService.searchStudentClassInfo(classInfo);
			if(null!=classInfo && StringUtil.isNotBlank(classInfo.getSchoolName())) {
				//设置学生学校名称
				queryInfo.setInstitutionName(classInfo.getSchoolName());
			}
		}
	    //返回查询结果列表
		return taxonomyInstitutionService.listybSearch(queryInfo);
	}
	*//**
	 * 管理人员基本信息页面
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/managerUserList.action")
	public ModelAndView managerUserList(ManagerUserEntity condition) {
		//管理员信息页面展示路径
		ModelAndView result = new ModelAndView("/baseinstitution/manager/base_manager_user_list");
		//设置每页展示10条数据
		condition.setPageSize(10);
        result.addObject("course", new CourseManagerView());
		result.addObject("condition", condition);
		result = getRoleAndNum(result);
		//返回页面所需信息结果
		return result;
	}
	*//**
	 * 管理者信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchManagerUserList.action")
	@ResponseBody
	public ResultBean searchManagerUserList(ManagerUserEntity entity) {
		//查询管理员数据表全部信息
		return managerUserService.searchPage(entity);
	}
	*//**
	 * 搜素全部人员
	 * 
	 * @param query
	 * @return
	 *//*
	@RequestMapping("/searchUserAll.action")
	@ResponseBody
	public ResultBean searchUserAll(TpUserEntity query) {
		if(null!=query && StringUtil.isNotBlank(query.getUsrName())) {
			query.setUsrName(query.getUsrName().trim());
		}
		//根据用户名查询人员信息表数据
		return userManagementService.searchManagerUserAll(query);
	}
	*//**
	 * 保存超级管理员信息
	 * @param userId 添加人员账号id
	 * @param userName  人员名称
	 * @param personName 名称
	 * @param role 角色
	 * @return
	 *//*
	@RequestMapping("/saveManagerUser.action")
	@ResponseBody
	public ResultBean saveManagerUser(String userId,String userName,String personName,String role) {
		ResultBean result = new ResultBean();
		//查询管理员表中是否已存在该成员
		ManagerUserEntity entity = managerUserService.searchByUserId(userId);
		//存在则返回
		if(null!=entity) {
			result = ResultBean.error();
		}else {
			//不存在则保存到数据表里
			ManagerUserEntity managerUserEntity = new ManagerUserEntity();
			managerUserEntity.setUserId(userId);
			managerUserEntity.setUserName(userName);
			managerUserEntity.setPersonName(personName);
			managerUserEntity.setRole(role);
			managerUserEntity.setCreateDate(new Date());
			//设置该条信息的创建人
			managerUserEntity.setCreateUser(WebContext.getSessionUserId());
			managerUserService.insert(managerUserEntity);
			//保存成功返回信息
			result = ResultBean.success();
		}
		
		return result;
	}
	*//**
	 * 删除管理员
	 * @param id 员工账号
	 * @return  返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/deleteManagerById.action")
	@ResponseBody
	public ResultBean deleteManagerById(String id) throws Exception {
		//根据员工账号在tb_manager_user表里删除其信息
		ManagerUserEntity entity = new ManagerUserEntity();
		entity.setUserId(id);
		managerUserService.delete(entity);
		return ResultBean.success(DELETE_GRADE_SUCCESS);
	}
	*//**
	 * 查询管理信息
	 * 
	 * @param 员工账号
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchByUserId.action")
	@ResponseBody
	public ManagerUserEntity searchByUserId(String id) throws Exception {
		//返回查询结果
		return managerUserService.searchByUserId(id);
	}
	*//**
	 *  作业列表页面
	 * @param homeworkView 入参
	 * @return 返回结果
	 *//*
	@RequestMapping("/zuoyeManager.action")
	public ModelAndView zuoyeManager(HomeworkView homeworkView) {
		//列表页面路径
		ModelAndView result = new ModelAndView("/course/manager/homework_list");
		//返回作业信息对象给前台
		result.addObject("homeworkView", homeworkView);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 *  查询作业列表结果方法
	 * @param homeworkView 前台传入后台参数
	 * @return  返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/zuoyelistSearch.action")
	@ResponseBody
	public ResultBean zuoyelistSearch(HomeworkView homeworkView) throws Exception {
		ResultBean result =new ResultBean();
		//获取登录信息对象
		UserEntity user = WebContext.getSessionUser();
    	//判断是否管理员
    	ManagerUserEntity entity = managerUserService.searchByUserId(user.getUserId());
    	if(null!=entity) {
    		//管理员返回的结果
    		result= courseVRService.seacherHomeworkPage(homeworkView);
    	}else {
    		//普通教师查询返回的结果
    		homeworkView.setTeacherId(user.getUserId());
    		result= courseVRService.seacherHomeworkPage(homeworkView);
    	}
		return result;
	}
	*//**
	 *  根据作业id查询作业列表
	 * @param id 作业主键
	 * @param title  作业标题
	 * @return  返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/zuoyelistById.action")
	public ModelAndView zuoyelistById(String id,String title) throws Exception {
		//作业列表路径
		ModelAndView result = new ModelAndView("/course/manager/homework_user_list");
		HomeworkView homeworkView = new HomeworkView();
		homeworkView.setId(id);
		homeworkView.setTitle(title);
		//作业实例化对象
		result.addObject("homeworkView", homeworkView);
		//作业主键
		result.addObject("id", id);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 预览作业页面方法
	 * @param id  作业主键
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/homeWorkById.action")
	public ModelAndView homeWorkById(String id) throws Exception {
		//预览页面路径
		ModelAndView result = new ModelAndView("/course/client/home_work_teacher_review");
		HomeworkView view = courseService.getHomeWorkById(id);
		//设置创建时间返回前台
		if(null!=view.getCreateDate()) {
			view.setCreateTime(sdf.format(view.getCreateDate()));
		}
        //返回作业实体类对象
		result.addObject("homeWork", view);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 查询作业列表
	 * @param id 作业id集合字符串
	 * @param homeworkView  作业实体类
	 * @param linkId 课程主键
	 * @param linkType  课程/章节类型
	 * @return
	 *//*
	@RequestMapping("/zuoyelistByIdSearch.action")
	@ResponseBody
	public ResultBean zuoyelistByIdSearch(String id,HomeworkView homeworkView,String linkId,String linkType) {
		// 将前台汇总的作业id拆分
		if(StringUtil.isNotBlank(id)) {
			String ids[]=id.split(",");
			homeworkView.setId(ids[0]);
		}else {
			homeworkView.setLinkId(linkId);
			homeworkView.setLinkType(linkType);
			HomeworkEntity homeWork = courseService.getHomeWorkByLinkId(homeworkView);
			if(null!=homeWork) {
				//获取作业主键
				homeworkView.setId(homeWork.getId());
				//获取作业标题
				homeworkView.setTitle(homeWork.getTitle());
			}
		}
		//查询作业信息
		return courseVRService.seacherHomeworkUserPage(homeworkView);
	}
    *//**
                 * 保存作业信息
     * @param courseJson 作业信息
     * @return 返回结果
     * @throws Exception 
     *//*
    @RequestMapping("/saveZyInfo.action")
    @ResponseBody
    public ResultBean saveZyInfo(String courseJson) throws Exception {
    	//将字符串转化成实例化对象
    	HomeworkView course = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, HomeworkView.class);
        ResultBean result = ResultBean.success();
        String homeworkId=course.getId();
        //获取账号id
        String userId = WebContext.getSessionUserId();
        //查询作业数据表
        HomeworkAnswerEntity entity = new  HomeworkAnswerEntity();
        entity.setHomeworkId(homeworkId);
        entity.setCreateUser(userId);
        entity = courseVRService.selectOneAnswer(entity);
        if(null==entity) {
        	//作业作答数据表信息为空，插入数据信息
        	course.setId(IDGenerator.genUID());
        	course.setCreateUser(userId);
        	course.setCreateDate(new Date());
        	//设置学生提交的信息
        	course.setStudentComment(course.getContent());
        	course.setHomeworkId(homeworkId);
        	course.setAnswerUser(userId);
        	//设置作业状态
        	course.setStatus(HOMEWORK_ANSWER_STATUS_ONE);
        	courseVRService.insertAnswer(course);
        }else {
        	//作业作答数据表信息不为空，更新数据信息
        	entity.setStudentComment(course.getContent());
        	entity.setUpdateDate(new Date());
        	entity.setUpdateUser(userId);
        	courseVRService.update(entity);
        }
        return result;
    }
	*//**
	 * 学生作业列表页面
	 * @param HomeworkView homeworkView 
	 * @return
	 *//*
	@RequestMapping("/studentZuoyePage.action")
	public ModelAndView studentZuoyePage(HomeworkView homeworkView) {
		//学生作业列表页面路径
		ModelAndView result = new ModelAndView("/course/manager/homework_student_list");
		//作业实例对象实体类
		result.addObject("homeworkView", homeworkView);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 查询作业列表
	 * 
	 * @param HomeworkView homeworkView 入参
	 * @return 返回结果
	 *//*
	@RequestMapping("/studentZuoyelistSearch.action")
	@ResponseBody
	public ResultBean studentZuoyelistSearch(HomeworkView homeworkView) {
		homeworkView.setCreateUser(WebContext.getSessionUserId());
		//根据账号id获取作业对象信息
		return courseVRService.seacherStudentHomeworkUserPage(homeworkView);
	}
    *//**
                   * 编辑作业
     * @param HomeworkView home 作业实例对象入参 
     * @return
     *//*
    @RequestMapping("/studentTaskInit.action")
    public ModelAndView studentTaskInit(HomeworkView home){
    	//作业编辑页面
        ModelAndView result = new ModelAndView("course/manager/home_student_work");
        //获取当前登录者账号
        UserEntity user = WebContext.getSessionUser();
        home.setCreateUser(user.getUserId());
        HomeworkView homework=courseVRService.searchStudentHomeWork(home);
        //前台页面信息回显对象
        result.addObject("homework", homework);
        result = getRoleAndNum(result);
        return result;
    }
    *//**
                  * 保存作业信息
     * @param id  作业id
     * @param studentComment 学生提交内容
     * @param homeanwserId 作业作答id
     * @param status 提交作业状态
     * @return
     * @throws Exception
     *//*
    @RequestMapping("/saveMystudy.action")
    @ResponseBody
    public ResultBean saveMystudy(String id,String studentComment,String homeanwserId,String status) throws Exception {
        ResultBean result = ResultBean.success();
        String userId = WebContext.getSessionUserId();
        //根据homeanwserId判断当前登陆者是否已经作答过改作业
        if(StringUtil.isBlank(homeanwserId)) {
        	//未作答，保存作答内容
        	HomeworkView course = new HomeworkView();
        	//随机生成作业作答id
        	course.setId(IDGenerator.genUID());
        	course.setCreateUser(userId);
        	course.setCreateDate(new Date());
        	//作业作答内容
        	course.setStudentComment(studentComment);
        	course.setHomeworkId(id);
        	course.setAnswerUser(userId);
        	//设置作业状态
        	course.setStatus(HOMEWORK_ANSWER_STATUS_ONE);
        	courseVRService.insertAnswer(course);
        }else {
        	//更新做大内容
        	HomeworkAnswerEntity entity = new HomeworkAnswerEntity();
        	entity.setId(homeanwserId);
        	entity = courseVRService.selectOneAnswer(entity);
        	entity.setStudentComment(studentComment);
        	entity.setUpdateDate(new Date());
        	entity.setUpdateUser(userId);
        	courseVRService.update(entity);
        }
        return result;
    }
    *//**
                 * 学生查看作业列表方法
     * @param home
     * @return
     *//*
    @RequestMapping("/studentSeacherTaskInit.action")
    public ModelAndView studentSeacherTaskInit(HomeworkView home){
    	//学生作业列表页面路径
        ModelAndView result = new ModelAndView("course/manager/home_student_look_work");
        //根据登录id查询作业信息
        UserEntity user = WebContext.getSessionUser();
        home.setCreateUser(user.getUserId());
        HomeworkView homework=courseVRService.searchStudentHomeWork(home);
        //作业信息返回给前台
        result.addObject("homework", homework);
        result = getRoleAndNum(result);
        return result;
    }
    *//**
                  * 查询年级 课程
     * @param id 课程id
     * @return
     *//*
    @RequestMapping("/searchGrade.action")
    @ResponseBody
    public List<StudentClassInfoEntity> searchGrade(String id){
    	//根据课程id查询课程信息
        CourseEntity entity = new CourseEntity();
        entity.setId(id);
        entity = courseService.searchCourse(entity);
        //获取老师所属学校
        String schoolId = entity.getSchoolId();
        StudentClassInfoEntity studentClassInfoEntity = new StudentClassInfoEntity();
        studentClassInfoEntity.setSchoolId(schoolId);
        //根据学校查询年级
        List<StudentClassInfoEntity> gradeList = new ArrayList<StudentClassInfoEntity>();
        gradeList = studentClassInfoService.searchStudentGradeList(studentClassInfoEntity);
        //返回查询的年级集合给前台
        return gradeList;
    }
     *//**
                     * 根据课程主键和年级主键查询班级信息
      * @param courseId  课程主键
      * @param gradeId 年级主键
      * @return
      *//*
    @RequestMapping("/searchClass.action")
    @ResponseBody
    public List<StudentClassInfoEntity> searchClass(String courseId,String gradeId){
    	//根据课程主键查询课程信息
    	 CourseEntity entity = new CourseEntity();
         entity.setId(courseId);
         entity = courseService.searchCourse(entity);
         //获取老师所属学校
         String schoolId = entity.getSchoolId();
        List<StudentClassInfoEntity> gradeList = new ArrayList<StudentClassInfoEntity>();
        //根据学校主键和年级查询班级信息
        StudentClassInfoEntity studentClassInfoEntity = new StudentClassInfoEntity();
        studentClassInfoEntity.setSchoolId(schoolId);//学校id
        studentClassInfoEntity.setGradeId(gradeId);//年级id
        gradeList = studentClassInfoService.searchStudentClassList(studentClassInfoEntity);
        return gradeList;
    }
    *//**
                  * 添加学生时查询学生列表
     * @param UserView query 用户参数 
     * @param courseId 所属课程信息id
     * @return
     *//*
    @RequestMapping("/searchStudentUserAll.action")
    @ResponseBody
    public ResultBean searchStudentUserAll(UserView query,String courseId)  {
    	//根据课程id查询课程所属老师对应的学校信息
    	CourseEntity entity = new CourseEntity();
        entity.setId(courseId);
        entity = courseService.searchCourse(entity);
        //获取老师所属学校
        String schoolId = entity.getSchoolId();
        //根据学校id获取这个学校下面所有的学生信息
        query.setSchoolId(schoolId);
        return courseService.searchStudentUserAll(query);
    }
	*//**
	 * 用户个人中心信息页面方法
	 * @return 返回结果
	 *//*
	@RequestMapping("/userPblInfo.action")
	public ModelAndView userPblInfo(){
		//查询代办信息表里审核通过的代办信息数量
		int num = courseService.getStuStatus();
		//个人中心页面路径参数
		ModelAndView mv = new ModelAndView("trainingplatform/client/center/center_pbl_user");
		//根据用户id查询用户信息
		UserEntity user = userManagementService.seachUserInfo(WebContext.getSessionUserId());
		//返回用户对象
		mv.addObject("user", user);
		mv.addObject("num", num);
		//获取登录人员的角色信息
		UserEntity userRole = WebContext.getSessionUser();
		//判断用户是否登录状态guest为未登录状态
		if(!userRole.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(userRole.getUserId());
			if(roleList.size() > 0) {
				//循环遍历人员角色
				for(UserRoleEntity role : roleList) {
					//角色包含01的设置role字段为t,即老师角色
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				//s为学生角色
				mv.addObject("role", "s");
			}
		}
		UserEntity user1 = WebContext.getSessionUser();
		//获取工号
		String uid =user1.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		    //获取人员信息
		        //设置头像字段
				String image = "";
				//设置头像字段
				String school="";
				//设置年级字段
				String grade="";
				//设置班级字段
				String className="";
				if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
					//获取头像
					image = userEntity.getHeadurl();
					//获取学校
					school = userEntity.getSchoolName();
				}else {
					//设置默认头像
					image ="http://wisdomsaas-img.oss-cn-beijing.aliyuncs.com/BFAA0FECDD194B7A974C60B7E1B27C18?authorization=bce-auth-v1%2F7642b4b48046452697e90662eed41ace%2F2017-12-29T08%3A33%3A39Z%2F-1%2F%2F0f06024af14dad05694f25b8c85572567591c72bbd0129d3175b4cdbd6e62208";
				}
				//获取班级
				if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
					//实例化学生对象实体类并查询学生信息
					List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
					StudentClassInfoEntity entity = new StudentClassInfoEntity();
					entity.setStudentId(uid);
					entityList = studentClassInfoService.searchStudentClassInfoList(entity);
					//如果学生对象不为空则执行如下赋值
					if(null!=entityList && entityList.size()>0) {
						grade = entityList.get(0).getGradeName();
						className = entityList.get(0).getClassName();
						school =  entityList.get(0).getSchoolName();
					}
				}
				//返回班级信息
				mv.addObject("className", className);
				//向页面返回年级信息
				mv.addObject("grade", grade);
				//返回学生所属学校名称信息
				mv.addObject("school", school);
		       return mv;
	}
	*//**
	 * 批量点评初始化页面方法
	 * @param userIds  学生id集合字符串
	 * @param personNames  学生姓名字符集合
	 * @param id      作业id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/allEvalutionMsg.action")
	public ModelAndView allEvalutionMsg(String userIds,String personNames,String id) throws Exception {
		//批量点评方法
		ModelAndView result = new ModelAndView("/course/client/home_work_teacher_all_review");
		String ids[]=id.split(",");
		HomeworkView view = courseService.getHomeWorkById(ids[0]);
		//判断作业对象返回的时间是否为空，不为空则赋值给新的字段用于前端展示
		if(null!=view.getCreateDate()) {
			if(StringUtil.isNotBlank(sdf.format(view.getCreateDate()))) {
				view.setCreateTime(sdf.format(view.getCreateDate()));
			}
		}
		//收集作业对象
		result.addObject("homeWork", view);
		//收集作业id
		result.addObject("id", id);
		//收集学生账号集合id
		result.addObject("userIds", userIds);
		//收集学生姓名集合
		result.addObject("personNames", personNames);
		//收集作业答案id
		result.addObject("answerIds", id);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 批量点评保存
	 * @param courseJson 前台返回的json字符串
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveAllHomeWorkReviewById.action")
	@ResponseBody
	public ResultBean saveAllHomeWorkReviewById(String courseJson) throws Exception {
		//将字符串转化成实例化对象
		HomeworkView homework = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, HomeworkView.class);
		return courseService.saveAllHomeWorkReviewById(homework);
	}
	*//**
	 * 一键催交
	 * @param userIds  学生id集合字符串参数
	 * @param courseId 课程id
	 * @param homeworkId  作业id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveCjById.action")
	@ResponseBody
	public ResultBean saveCjById(String userIds,String courseId,String homeworkId) throws Exception {
		return courseService.saveACjById(userIds,courseId,homeworkId);
	}
	*//**
	 * 课程考试调用java调查网接口
	 * @param courseId  课程id
	 * @param userId  用户id
	 * @return
	 * @throws ParseException
	 *//*
	@RequestMapping("/searchExamByCourseIdList.action")
	@ResponseBody
	public ResultBean searchExamByCourseIdList(String courseId,String userId) throws ParseException {
		ResultBean result = new ResultBean();
		String param = courseId;
		//声明调用java调查网的webservice接口
		ExamToWebserviceProxy proxy = new ExamToWebserviceProxy();
		String out;
		try {
			//调用外部接口方法
			out = proxy.examList(param,1,20,userId);
			JSONArray jsonArray = new JSONArray(); 
			//将接口返回值转化成数组格式
			jsonArray = jsonArray.fromObject(out);
			UserCourseEntity uc = new UserCourseEntity();
			uc.setCourseId(courseId);
			uc.setUserId(WebContext.getSessionUserId());
			// 检索用户是否存在
			UserCourseEntity uu = courseService.searchOneCourseIdData(uc);
			//根据课程id获取该课程的老师id
			CourseEntity courseEntity = courseService.getCourseId(courseId);
			//转化对应的实体类
			@SuppressWarnings("unchecked")
			List<Questionnaire> list = jsonArray.toList(jsonArray, Questionnaire.class);
			if(null!=list && list.size()>0) {
				//循环遍历集合将字段赋值
				for(int i=0;i<list.size();i++) {
					//已加入的学员或者这个课程的老师创建者都可以看到这个
					if(null!=uu ||(null!=courseEntity && WebContext.getSessionUserId().equals(courseEntity.getTeacherId()))) {
						//已加入课程
						list.get(i).setStudentFlag("0");
						if (list.get(i).getEndDate() != null ) {
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Long end = sdf.parse(list.get(i).getEndDateStr()).getTime();
							Long nowtime = new Date().getTime();
							//判断问卷/考试是否已结束
								if(end-nowtime<0) {
									list.get(i).setExamStatusText("问卷/考试已结束");
									list.get(i).setExamStatus("1");	
								}					
							}
						if (list.get(i).getStartDate() != null ) {
							//将字符串转化成时间再获取秒数
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Long start = sdf.parse(list.get(i).getStartDateStr()).getTime();
							Long nowtime = new Date().getTime();
							//判断问卷/考试是否开始
								if(start-nowtime>0) {
									list.get(i).setExamStatusText("问卷/考试未开始");
									list.get(i).setExamStatus("1");
								}
							}
					}else {
						list.get(i).setStudentFlag("1");//未加入课程
						list.get(i).setExamStatusText("未加入课程");
					}
					
				}
			}
			 result.setData(list);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
		@Autowired
	private CourseService courseService;
	@Autowired
	private ChapterService chapterService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserManagementService userManagementService;
	@Autowired
	private TaxonomyInstitutionService taxonomyInstitutionService;
	@Autowired
	private TagInstitutionEvalutionService tagInstitutionEvalutionService;
	@Autowired
	private InstitutionService institutionService;
	@Autowired
	private TagInstitutionService tagInstitutionService;
	@Autowired
	private MeasuermentInstitutionService measurementInstitutionService;
	@Autowired
	private QuestionInstitutionService questionInstitutionService;
	@Autowired
	private MeasuermentService measuermentService;
	@Autowired
	private StudentClassInfoService studentClassInfoService;
	@Autowired
	private TeacherClassInfoService teacherClassInfoService;
	@Autowired
	private ChapterActiveService chapterActiveService;
	@Autowired
	private TagService tagService;
	@Autowired
	private ManagerUserService managerUserService;
	@Autowired
	private CourseVRService courseVRService;
	*//**
	 * 用户收藏课程
	 * 
	 * @author liuzhen
	 * @param courseId
	 * @return 返回结果
	 *//*
	@RequestMapping("/collectCourse.action")
	@ResponseBody
	public ResultBean collectCourse(String courseId) {
		return courseService.collectCourse(courseId);
	}

	*//**
	 * 用户取消收藏
	 * 
	 * @param courseId 课程Id
	 * @return
	 *//*
	@RequestMapping("/cancelCollectCourse.action")
	@ResponseBody
	public ResultBean cancelCollectCourse(String courseId) {
		return courseService.cancelCollectCourse(courseId);
	}

	*//**
	 * 二级标签查询
	 * 
	 * @param id 课程ID
	 * @return 课程下的二级标签列表
	 *//*
	@RequestMapping("/searchSecondTag2.action")
	@ResponseBody
	public List<TagView> searchSecondTag2(String id) {
		return courseService.searchSecondTagList2(id);
	}
	*//**
	 * 学校评价二级标签查询
	 * 
	 * @param id 课程ID
	 * @return 课程下的二级标签列表
	 *//*
	@RequestMapping("/searchInsitutionSecondTag2.action")
	@ResponseBody
	public List<TagInstitutionView> searchInsitutionSecondTag2(String id) {
		return courseService.searchInstitutionSecondTagList2(id);
	}
	@RequestMapping("/searchSecondTag.action")
	@ResponseBody
	public List<TagView> searchSecondTag(String id) {
		return courseService.searchSecondTagList(id);
	}

	*//**
	 * 体验课程/加入课程操作
	 * 
	 * @param courseId 课程ID
	 * @return 是否加入课程成功
	 * @author WUXIAOXIANG
	 *//*
	@RequestMapping("/joinCourse.action")
	@ResponseBody
	public ResultBean joinCourse(String courseId) {
		return courseService.joinCourse(courseId);
	}

	@RequestMapping("/applyJoinCourse.action")
	@ResponseBody
	public ResultBean applyJoinCourse(String courseId) {
		return courseService.applyJoinCourse(courseId);
	}
	
	*//**
	 * 课程推荐
	 * 
	 * @author FZ
	 * @return 返回结果
	 *//*
	@RequestMapping("/recommendedCourses.action")
	@ResponseBody
	public ResultBean recommendedCourses(String courseId, String displayNum) {

		return courseService.recommendedCourses(displayNum, courseId);
	}

	*//**
	 * 课程评价
	 * 
	 * @param linkId linkType
	 * @return 返回结果
	 * @author dengzhihao
	 *//*
	@RequestMapping("/courseEvaluationList.action")
	@ResponseBody
	public ResultBean courseEvaluationList(ScoreEntity scoreEntity) {

		return courseService.courseEvaluationList(scoreEntity);
	}

	*//**
	 * 对课程评价
	 * 
	 * @param scoreEntity     评价主题entity
	 * @param scoreDataEntity 评价数据entity
	 * @return 是否评价成功
	 * @author WUXIAOXIANG
	 *//*
	@RequestMapping("/evaluateCourse.action")
	@ResponseBody
	public ResultBean evaluateCourse(ScoreEntity scoreEntity, ScoreDataEntity scoreDataEntity) {
		// 插入评价信息
		return courseService.insertEvaluateCourse(scoreEntity, scoreDataEntity);
	}

	*//**
	 * 检索课程 包括分类
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 *//*
	@RequestMapping("/courseList.action")
	@ResponseBody
	public ResultBean courseList(CourseManagerView course, String directionTagId, String categoryTagId,
			String courseStage, String sortType, Integer pageSize, Integer pageNumber,String schoolName,String schoolNatrul,String tagsId) {
		if(StringUtil.isNotBlank(tagsId)) {
			course.setTagsId(tagsId);
		}
		if(StringUtil.isNotBlank(schoolNatrul)) {
			course.setSchoolNatrul(schoolNatrul);
		}
		ResultBean result = courseService.courseList(course, directionTagId, categoryTagId, courseStage, sortType,
				pageSize, pageNumber,schoolName);
		return result;
	}

	@RequestMapping("/coursePublicList.action")
	@ResponseBody
	public ResultBean coursePublicList(CourseManagerView course, Integer pageSize, Integer pageNumber) {
		course.setShareFlag(COURSE_SHARE_FLAG_THREE);
		course.setEndFlag(COURSE_END_STATUS_YES);
		UserEntity user = WebContext.getSessionUser();
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		//获取学校名称信息
		String school="";
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
			//获取学校
			school = userEntity.getSchoolName();
		}
		//获取班级
		if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
			List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
			StudentClassInfoEntity entity = new StudentClassInfoEntity();
			entity.setStudentId(uid);
			entityList = studentClassInfoService.searchStudentClassInfoList(entity);
			if(null!=entityList && entityList.size()>0) {
				school =  entityList.get(0).getSchoolName();
			}
		}
		course.setSchoolName(school);
//		if(StringUtil.isNotBlank(course.getDirectionTagId()) && StringUtil.isBlank(course.getCategoryTagId())) {
//			course.setTagId(course.getDirectionTagId());
//		}else {
//			course.setTagId(course.getCategoryTagId());
//			course.setParentTagId(course.getDirectionTagId());
//		}
		
		ResultBean result = courseService.coursePublicList(course, pageSize, pageNumber);
		return result;
	}

	@RequestMapping("/courseFactoryList.action")
	@ResponseBody
	public ResultBean courseFactoryList(CourseManagerView course, Integer pageSize, Integer pageNumber) {
		course.setShareFlag(COURSE_SHARE_FLAG_TWO);
		//获取学校名字
		//获取工号
		UserEntity user = WebContext.getSessionUser();
				String uid =user.getUserId();
				TpUserEntity userEntity = courseService.searchInfoMessage(uid);
				//获取学校名称信息
				String school="";
				if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
					//获取学校
					school = userEntity.getSchoolName();
				}
				//获取班级
				if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
					List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
					StudentClassInfoEntity entity = new StudentClassInfoEntity();
					entity.setStudentId(uid);
					entityList = studentClassInfoService.searchStudentClassInfoList(entity);
					if(null!=entityList && entityList.size()>0) {
						school =  entityList.get(0).getSchoolName();
					}
				}
//				course.setSchoolName(school);
//		if(StringUtil.isNotBlank(course.getDirectionTagId()) && StringUtil.isBlank(course.getCategoryTagId())) {
//			course.setTagId(course.getDirectionTagId());
//		}else {
//			course.setTagId(course.getCategoryTagId());
//			course.setParentTagId(course.getDirectionTagId());
//		}
		
		
		ResultBean result = courseService.coursePublicList(course, pageSize, pageNumber);
		return result;
	}
	
	@RequestMapping("/courseQualityList.action")
	@ResponseBody
	public ResultBean courseQualityList(CourseManagerView course, Integer pageSize, Integer pageNumber) {
		course.setShareFlag(COURSE_SHARE_FLAG_ONE);
		//获取工号
		UserEntity user = WebContext.getSessionUser();
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		//获取学校名称信息
		String school="";
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
			//获取学校
			school = userEntity.getSchoolName();
		}
		//获取班级
		if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
			List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
			StudentClassInfoEntity entity = new StudentClassInfoEntity();
			entity.setStudentId(uid);
			entityList = studentClassInfoService.searchStudentClassInfoList(entity);
			if(null!=entityList && entityList.size()>0) {
				school =  entityList.get(0).getSchoolName();
			}
		}
		course.setSchoolName(school);
//		if(StringUtil.isNotBlank(course.getDirectionTagId()) && StringUtil.isBlank(course.getCategoryTagId())) {
//			course.setTagId(course.getDirectionTagId());
//		}else {
//			course.setTagId(course.getCategoryTagId());
//			course.setParentTagId(course.getDirectionTagId());
//		}
		ResultBean result = courseService.coursePublicList(course, pageSize, pageNumber);
		return result;
	}
	
	*//**
	 * 检索教师信息
	 * 
	 * @author LILIANG
	 * @return UserEntity
	 *//*
	@RequestMapping("/teacherInfo.action")
	@ResponseBody
	public ResultBean teacherInfo(UserEntity user) {
		ResultBean result = courseService.teacherInfo(user);
		return result;
	}

	*//**
	 * 检索与教师相关的课程信息
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 *//*
	@RequestMapping("/teacherCourseList.action")
	@ResponseBody
	public ResultBean teacherCourseList(CourseEntity c) {
		ResultBean result = courseService.teacherCourseList(c);
		return result;
	}

	*//**
	 * 课程浏览数+1
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 *//*
	@RequestMapping("/addCourseViewNum.action")
	@ResponseBody
	public ResultBean addCourseViewNum(CourseEntity c) {
		ResultBean result = courseService.addCourseViewNum(c);
		return result;
	}

	*//**
	 * 根据课程ID检索课程详情
	 * 
	 * @param courseId 课程ID
	 * @return ResultBean data:CourseView 课程基本信息、视频数、练习数、标签、用户课程等信息
	 *//*
	@RequestMapping("/courseDetail.action")
	@ResponseBody
	public ResultBean courseDetail(String courseId) {
		return courseService.searchCourseDetail(courseId);
	}

	*//**
	 * 课程详情-立即支付详情
	 * 
	 * @param courseId 课程Id
	 * @return
	 *//*
	@RequestMapping("/immediatePayDetail.action")
	@ResponseBody
	public ResultBean immediatePayDetail(String courseId) {
		return courseService.immediatePayDetail(courseId);
	}

	*//**
	 * 课程列表页面
	 *//*
	@RequestMapping("/courseListInit.action")
	public ModelAndView courseListInit(String courseId,String schoolName) {
		ModelAndView result = new ModelAndView("/course/client/course_list");
		result.addObject("schoolName", schoolName);
		result.addObject("courseId", courseId);
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/pubCourseListInit.action")
	public ModelAndView pubCourseListInit() {
		ModelAndView result = new ModelAndView("/course/client/course_list_public");
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/qualityCourseListInit.action")
	public ModelAndView qualityCourseListInit() {
		ModelAndView result = new ModelAndView("/course/client/course_list_quality");
		result = getRoleAndNum(result);
		return result;
	}
	
	@RequestMapping("/courseFactoryListInit.action")
	public ModelAndView courseFactoryListInit() {
		ModelAndView result = new ModelAndView("/course/client/course_list_factory");
		result = getRoleAndNum(result);
		return result;
	}
	
	@RequestMapping("/studentHomePage.action")
	public ModelAndView studentHomePage(String userId) {
		ModelAndView result = new ModelAndView("/course/client/studentHomePage");
		UserEntity loginUser = userManagementService.seachUserInfo(userId);
		// List<CourseEntity> courseList = courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result = getRoleAndNum(result);
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/studentHomePageFromList.action")
	public ModelAndView studentHomePageFromList(String userId) {
		ModelAndView result = new ModelAndView("/course/client/studentHomePageFromList");
		UserEntity loginUser = userManagementService.seachUserInfo(userId);
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result = getRoleAndNum(result);
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/teacherHomePage.action")
	public ModelAndView teacherHomePage(String userId) {
		ModelAndView result = new ModelAndView("/course/client/teacherHomePage");
		result = getRoleAndNum(result);
		UserEntity loginUser = userManagementService.seachUserInfo(userId);
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/teacherHomePageFromList.action")
	public ModelAndView teacherHomePageFromList(String userId) {
		ModelAndView result = new ModelAndView("/course/client/teacherHomePageFromList");
		result = getRoleAndNum(result);
		UserEntity loginUser = userManagementService.seachUserInfo(userId);
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/saveInfo.action")
	@ResponseBody
	public ResultBean saveInfo(String studentJson) throws Exception {
		ArchivesEntity archives = JSONUtil.parse(
				JSONUtil.createObMapperInstance(ConfigUtil.getDateMinuteFormatDefualt()), studentJson,
				ArchivesEntity.class);
		courseService.saveArchives(archives);
		ResultBean result = ResultBean.success();
		return result;
	}

	@RequestMapping("/studentList.action")
	public ModelAndView studentList() {
		ModelAndView result = new ModelAndView("/course/client/student_list");
		result = getRoleAndNum(result);
		UserEntity loginUser = userManagementService.seachUserInfo(WebContext.getSessionUserId());
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/teacherList.action")
	public ModelAndView teacherList() {
		ModelAndView result = new ModelAndView("/course/client/teacher_list");
		result = getRoleAndNum(result);
		UserEntity loginUser = userManagementService.seachUserInfo(WebContext.getSessionUserId());
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/archivesList.action")
	@ResponseBody
	public ResultBean archivesList(String userId) {
		ResultBean result = new ResultBean();
		TpUserEntity tpUser = courseService.getUserByUserId(userId);
		List<ArchivesEntity> archivesList = courseService.getArchivesList(userId);
		List<CourseEntity> courseList = courseService.getCourseListByUserId(userId);
		if (courseList.size() > 0) {
			for (CourseEntity entity : courseList) {
				ArchivesEntity archives = new ArchivesEntity();
				archives.setUserId(userId);
				archives.setUserName(tpUser.getPersonName());
				archives.setCreateTime(sdf.format(entity.getCreateDate()));
				archives.setCreateDate(entity.getCreateDate());
				archives.setInfo(sdf.format(entity.getCreateDate()) + ",创建课程，" + entity.getTitle());
				if (entity.getPictureId() != null) {
					archives.setExitImage("1");
					archives.setImageId(entity.getPictureId());
				}
				// archivesList.add(archives);
			}
			listSort(archivesList);
		}
		List<UserCourseEntity> ucList = courseService.getUCListByUserId(userId);
		if (ucList.size() > 0) {
			for (UserCourseEntity uc : ucList) {
				ArchivesEntity archives = new ArchivesEntity();
				archives.setUserId(userId);
				archives.setUserName(tpUser.getPersonName());
				archives.setCreateTime(sdf.format(uc.getCreateDate()));
				archives.setCreateDate(uc.getCreateDate());
				archives.setInfo(sdf.format(uc.getCreateDate()) + ",加入课程，" + uc.getCourseName());
				if (uc.getPictureId() != null) {
					archives.setExitImage("1");
					archives.setImageId(uc.getPictureId());
				}
				// archivesList.add(archives);
			}
			listSort(archivesList);
		}
		result.setData(archivesList);
		return result;
	}

	@RequestMapping("/saveDiscussData.action")
	@ResponseBody
	public ResultBean saveDiscussData(ArchivesDiscussEntity data) throws Exception {
		return courseService.saveDiscuss(data);
	}

	@RequestMapping("/searchUsersList.action")
	@ResponseBody
	public ResultBean searchUsersList(TpUserEntity user) {
		String userKbn = WebContext.getSessionUser().getUserKbn();
		if (userKbn != null && userKbn.contentEquals(TpStateConstants.USER_KBN_C)) {
			user.setUserKbn(TpStateConstants.USER_KBN_C);
		}
		user.setSortName("createDate");
		user.setSortOrder("DESC");
		return userManagementService.search4Page(user);
	}

	@SuppressWarnings("unused")
	private void listSort(List<ArchivesEntity> list) {
		Collections.sort(list, new Comparator<ArchivesEntity>() {
			@Override
			public int compare(ArchivesEntity o1, ArchivesEntity o2) {
				try {
					Date dt1 = o1.getCreateDate();
					Date dt2 = o2.getCreateDate();
					if (dt1.getTime() > dt2.getTime()) {
						return -1;
					} else if (dt1.getTime() < dt2.getTime()) {
						return 1;
					} else {
						return 0;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return 0;
			}
		});
	}

	*//**
	 * 系统首页
	 *//*
	@RequestMapping("/homePage.action")
	public ModelAndView homePage() {
		ModelAndView result = new ModelAndView("/course/client/homePage");
		UserEntity user = WebContext.getSessionUser();
		//获取工号
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getIsFlag())) {
			result.addObject("isFlag","1");
		}else {
			//不是管理员
			result.addObject("isFlag","0");
		}
		
		//获取人员信息
		String image = "";
		String school="";
		String grade="";
		String className="";
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
			//获取头像
			image = userEntity.getHeadurl();
			//获取学校
			school = userEntity.getSchoolName();
		}else {
			//设置默认头像
			image ="http://wisdomsaas-img.oss-cn-beijing.aliyuncs.com/BFAA0FECDD194B7A974C60B7E1B27C18?authorization=bce-auth-v1%2F7642b4b48046452697e90662eed41ace%2F2017-12-29T08%3A33%3A39Z%2F-1%2F%2F0f06024af14dad05694f25b8c85572567591c72bbd0129d3175b4cdbd6e62208";
		}
		//获取班级
		if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
			List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
			StudentClassInfoEntity entity = new StudentClassInfoEntity();
			entity.setStudentId(uid);
			entityList = studentClassInfoService.searchStudentClassInfoList(entity);
			if(null!=entityList && entityList.size()>0) {
				grade = entityList.get(0).getGradeName();
				className = entityList.get(0).getClassName();
				school =  entityList.get(0).getSchoolName();
			}
		}
		result.addObject("className", className);
		result.addObject("grade", grade);
		result.addObject("school", school);
		result.addObject("image", image);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 问卷考试系统首页
	 *//*
	@RequestMapping("/homeJavaPage.action")
	public ModelAndView homeJavaPage(String courseId) {
		ModelAndView result = new ModelAndView("/course/client/homeJavaPage");
		UserEntity user = WebContext.getSessionUser();
		//获取工号
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getIsFlag())) {
			result.addObject("isFlag","1");
		}else {
			//不是管理员
			result.addObject("isFlag","0");
		}
		
		//获取人员信息
		String image = "";
		String school="";
		String grade="";
		String className="";
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
			//获取头像
			image = userEntity.getHeadurl();
			//获取学校
			school = userEntity.getSchoolName();
		}else {
			//设置默认头像
			image ="http://wisdomsaas-img.oss-cn-beijing.aliyuncs.com/BFAA0FECDD194B7A974C60B7E1B27C18?authorization=bce-auth-v1%2F7642b4b48046452697e90662eed41ace%2F2017-12-29T08%3A33%3A39Z%2F-1%2F%2F0f06024af14dad05694f25b8c85572567591c72bbd0129d3175b4cdbd6e62208";
		}
		//获取班级
		if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
			List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
			StudentClassInfoEntity entity = new StudentClassInfoEntity();
			entity.setStudentId(uid);
			entityList = studentClassInfoService.searchStudentClassInfoList(entity);
			if(null!=entityList && entityList.size()>0) {
				grade = entityList.get(0).getGradeName();
				className = entityList.get(0).getClassName();
				school =  entityList.get(0).getSchoolName();
			}
		}
		result.addObject("className", className);
		result.addObject("grade", grade);
		result.addObject("school", school);
		result.addObject("courseId", courseId);
		result.addObject("image", image);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 题库管理
	 *//*
	@RequestMapping("/homeExamPage.action")
	public ModelAndView homeExamPage() {
		ModelAndView result = new ModelAndView("/course/manager/homeExamPage");
		UserEntity user = WebContext.getSessionUser();
		//获取工号
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getIsFlag())) {
			result.addObject("isFlag","1");
		}else {
			//不是管理员
			result.addObject("isFlag","0");
		}
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 题库管理
	 *//*
	@RequestMapping("/homeJsdPage.action")
	public ModelAndView homeJsdPage() {
		ModelAndView result = new ModelAndView("/course/client/homeJsdPage");
		UserEntity user = WebContext.getSessionUser();
		//获取工号
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getIsFlag())) {
			result.addObject("isFlag","1");
		}else {
			//不是管理员
			result.addObject("isFlag","0");
		}
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 获取登录信息
	 * @param code
	 * @param type
	 * @return
	 * @throws UnsupportedEncodingException
	 *//*
	@SuppressWarnings("null")
	@RequestMapping("/getUserInfo.action")
	@ResponseBody
	public JSONArray getUserInfo(String code, String type) throws UnsupportedEncodingException {
		//设置json集合对象和数组
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		if (code != null && !"".equals(code)) {
			//UserEntity user = WebContext.getSessionUser();
			String codeSession = (String) WebContext.getSession().getAttribute("code");
			if (codeSession == null || !code.equals(codeSession)) {
				Long str12 = System.currentTimeMillis();
				String access_tokenStr = "";
				//定义获取应用token的变量
				//String application_tokenStr="";
				if (type.equals("1")) {
					//获取access_tokenStr字符串对象
					access_tokenStr = HttpUtils.sendFormEvalutionPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token",
							"code=" + code
									+ "&grant_type=authorization_code&client_id=1EF8798194B36D72&client_secret=440a4fad895036188ea200a0da1d6156&redirect_uri="
									+ PropertyUtil.getProperty("remote.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "1EF8798194B36D72", "440a4fad895036188ea200a0da1d6156");
				} else if (type.equals("2")) {
					//获取access_tokenStr字符串对象
					access_tokenStr = HttpUtils.sendFormEvalutionPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token",
							"code=" + code
									+ "&grant_type=authorization_code&client_id=BB2650288E091B1C&client_secret=26807915a5269ba07a40d97236b5ec64&redirect_uri="
									+ PropertyUtil.getProperty("teacher.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "BB2650288E091B1C", "26807915a5269ba07a40d97236b5ec64");
				} else if (type.equals("3")) {
					//获取access_tokenStr字符串对象
					access_tokenStr = HttpUtils.sendPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token",
							"code=" + code
									+ "&grant_type=authorization_code&client_id=42F9292D78B1386F&client_secret=e1e034ac44a63ba61e8e9595689efd4c&redirect_uri="
									+ PropertyUtil.getProperty("student.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "42F9292D78B1386F", "e1e034ac44a63ba61e8e9595689efd4c");
				} else if (type.equals("4")) {
					//获取access_tokenStr字符串对象
					   access_tokenStr = HttpUtils.sendFormEvalutionPost(
					            PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", 
					            "code=" + code + 
					            "&grant_type=authorization_code&client_id=6FF0E39AD917FD04&client_secret=ea201b6aff470f9ec25c1fc9be17197a&redirect_uri=" + 
					            PropertyUtil.getProperty("institution.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "6FF0E39AD917FD04", "ea201b6aff470f9ec25c1fc9be17197a");
				} else if (type.equals("5")) {
					//获取access_tokenStr字符串对象
					access_tokenStr = HttpUtils.sendFormEvalutionPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token",
							"code=" + code
									+ "&grant_type=authorization_code&client_id=8275669B807A7B7A&client_secret=229f39d615a8c2cbbef56e56197a22c7&redirect_uri="
									+ PropertyUtil.getProperty("teacherevalution.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "8275669B807A7B7A", "229f39d615a8c2cbbef56e56197a22c7");
				}
				//设置执行时间
				Long str333 = System.currentTimeMillis();
				int tokentime =(int) (str333-str12);
				UserLogEntity entity12=new UserLogEntity();
				entity12.setId(IDGenerator.genUID());
				entity12.setUserId(WebContext.getSessionUserId());
				entity12.setUserName(WebContext.getSessionUser().getPersonName());
				entity12.setLogType("获取token信息");
				entity12.setTimes(tokentime);
				entity12.setOperTime(new Date());
				//保存方法执行时间日志信息
				tagInstitutionEvalutionService.insertUserLog(entity12);
				//获取access_token
				if (access_tokenStr != null && !"".equals(access_tokenStr)) {
					//字符转化成json对象
					JSONObject accessjson = JSONObject.fromObject(access_tokenStr);
					//获取字段值
					String access_token = accessjson.getString("access_token");
					Long str3 = System.currentTimeMillis();
					//原有逻辑获取用户信息开始
					String userStr = HttpUtils.sendFormEvalutionPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/rest/get/userinfo",
							"access_token=" + access_token);
					Long str33 = System.currentTimeMillis();
					int userinfotime =(int) (str33-str3);
					UserLogEntity entity13=new UserLogEntity();
					entity13.setId(IDGenerator.genUID());
					entity13.setUserId(WebContext.getSessionUserId());
					entity13.setUserName(WebContext.getSessionUser().getPersonName());
					entity13.setLogType("获取/passport/rest/get/userinfo信息");
					entity13.setTimes(userinfotime);
					entity13.setOperTime(new Date());
					//保存方法执行需要的时间信息日志
					tagInstitutionEvalutionService.insertUserLog(entity13);
					System.out.println("userStr=:" + userStr);
					
					
					JSONObject userjson = JSONObject.fromObject(userStr);
					//原有逻辑获取用户信息结束
					//用户id
					String userid = userjson.getString("userid");
					//用户账号
					String account = userjson.getString("account");
					//用户类型
					String usertype = userjson.getString("usertype");
					//用户名称
					String name = userjson.getString("name");
					//用户性别
					String sex = userjson.getString("sex");
					//用户角色
					String role = userjson.getString("role");
					//获取tenantId
					String tenantId = userjson.getString("tenantId");
					//获取头像url
					String headUrl = userjson.getString("head_url");
					String teacher = "";
					*//**
					 * usertype=1
					 * 1:平台总系统管理员;2:平台省级系统管理员;3:平台市级系统管理员;4:学校总系统管理员;5:学校分校系统管理员;6:学校年级系统管理员;7:教育部分系统管理员
					 * usertype=0 
					 * 1:教师;2:家长;3:学生;4:管理者;5:教研员
					 *//*
					*//**
					 * 系统角色 01:管理员;04:教师
					 *//*
					UserRoleEntity userRole = new UserRoleEntity();
					UserRoleEntity userTeacherRole = new UserRoleEntity();
					UserEntity loginUser = new UserEntity();
					TpUserEntity saveUser = new TpUserEntity();
					*//**
					 * 获取用户是老师的信息
					 * 包括老师所属的学校信息
					 *//*
					if("1".equals(role)) {
						//获取教师信息
						Long str5 = System.currentTimeMillis();
						//调用外部接口获取教师信息
						String teacherinfo = HttpUtils.sendFormDataPost("https://gwss.yceduyun.com/apis/user/V1.0/teacherInfo",
								access_token,"002");
						Long str6 = System.currentTimeMillis();
						int teachertime =(int) (str6-str5);
						UserLogEntity entity2=new UserLogEntity();
						entity2.setId(IDGenerator.genUID());
						entity2.setUserId(WebContext.getSessionUserId());
						entity2.setUserName(WebContext.getSessionUser().getPersonName());
						entity2.setLogType("获取https://gwss.yceduyun.com/apis/user/V1.0/teacherInfo信息");
						entity2.setTimes(userinfotime);
						entity2.setOperTime(new Date());
						//保存获取教师信息所需要的时间日志信息
						tagInstitutionEvalutionService.insertUserLog(entity2);
						System.out.println(teacherinfo); 
                        //解析教师信息
						JSONObject teacherinfojson = JSONObject.fromObject(teacherinfo);
						JSONObject teacherinfoDatajson = JSONObject.fromObject(teacherinfojson.get("data"));
						//获取基本信息
						if(null!=teacherinfoDatajson) {
							//省份
							String proName=teacherinfoDatajson.getString("proName");
							//市区
							String cityName=teacherinfoDatajson.getString("cityName");
							//县级
							String townName=teacherinfoDatajson.getString("townName");
							//学校id
							String schoolId=teacherinfoDatajson.getString("schoolId");
							//学校名称
							String schoolName=teacherinfoDatajson.getString("schoolName");
							//学校类型
							String schoolType=teacherinfoDatajson.getString("schoolType");
							//
							String subjectId=teacherinfoDatajson.getString("subjectId");
							//
							String subjectName=teacherinfoDatajson.getString("subjectName");
							saveUser.setProName(proName);
							saveUser.setCityName(cityName);
							saveUser.setTownName(townName);
							saveUser.setSchoolId(schoolId);
							saveUser.setSchoolName(schoolName);
							saveUser.setSchoolType(schoolType);
							saveUser.setSubjectId(subjectId);
							saveUser.setSubjectName(subjectName);
						}
						//获取字段信息
						JSONArray teacherClassArray =JSONArray.fromObject(teacherinfoDatajson.get("classList"));
						//获取老师的班级年级信息
						@SuppressWarnings("unchecked")
						List<TeacherClassInfoEntity> list = teacherClassArray.toList(teacherClassArray, TeacherClassInfoEntity.class);
						//判断对象是否为空
						if(null!=list && list.size()>0) {
							List<TeacherClassInfoEntity> entityList = new ArrayList<TeacherClassInfoEntity>();
							//查询该老师下的信息是否存在
							TeacherClassInfoEntity entity = new TeacherClassInfoEntity();
							entity.setTeacherId(userid);
							entityList = teacherClassInfoService.searchTeacherClassInfoList(entity);
							if(null!=entityList && entityList.size()>0) {
								//循环遍历教师班级信息
								for(int j=0;j<entityList.size();j++) {
									TeacherClassInfoEntity entity1 = new TeacherClassInfoEntity();
									entity1.setId(entityList.get(j).getId());
									//删除已存在信息
									teacherClassInfoService.deleteTeacherClassInfoById(entity1);
								}
							}
							//循环保存班级信息
							for(int i=0;i<list.size();i++) {
								TeacherClassInfoEntity infoEntity = new TeacherClassInfoEntity();
								infoEntity.setTeacherId(userid);//老师id
								infoEntity.setGradeId(list.get(i).getGradeId());//年级id
								infoEntity.setGradeName(list.get(i).getGradeName());//年级名称
								infoEntity.setGradeLever(list.get(i).getGradeLever());//年级等级
								infoEntity.setClassId(list.get(i).getClassId());//班级id
								infoEntity.setClassName(list.get(i).getClassName());//班级名称
								infoEntity.setSubjectId(list.get(i).getSubjectId());//
								infoEntity.setSubjectName(list.get(i).getSubjectName());//
								infoEntity.setCreateDate(new Date());//创建日期
								infoEntity.setUpdateDate(new Date());//更新日期
								teacherClassInfoService.insertTeacherClassInfo(infoEntity);
							}
						}
						//获取用户具体信息
						String param=access_token;
						String tenantId1 = "002";
						Long str7 = System.currentTimeMillis();
						//获取教师角色
						String teacherDetailinfo = HttpUtils.sendFormDataDetailPost("https://gwss.yceduyun.com/apis/usercenter/v1.0/getOneUserInfoByUseIdAndRoleId",
								param,tenantId1,role);
						Long str8 = System.currentTimeMillis();
						int studenttime =(int) (str8-str7);
						UserLogEntity entity3=new UserLogEntity();
						entity3.setId(IDGenerator.genUID());
						entity3.setUserId(WebContext.getSessionUserId());
						entity3.setUserName(WebContext.getSessionUser().getPersonName());
						entity3.setLogType("获取https://gwss.yceduyun.com/apis/usercenter/v1.0/getOneUserInfoByUseIdAndRoleId信息");
						entity3.setTimes(userinfotime);
						entity3.setOperTime(new Date());
						//保存调用教师角色花费时间信息
						tagInstitutionEvalutionService.insertUserLog(entity3);
						//字符串转json
						JSONObject teacherDetailinfojson = JSONObject.fromObject(teacherDetailinfo);
						JSONArray teacherinfoDataDetailjson = JSONArray.fromObject(teacherDetailinfojson.get("data"));
						JSONArray dutyJson = JSONObject.fromObject(teacherinfoDataDetailjson.get(0)).getJSONArray("teacherDuties");
						List<InstitutionTeacherDutyEntity> dutylist = JSONArray.toList(dutyJson, InstitutionTeacherDutyEntity.class);
                        if(null!=dutylist && dutylist.size()>0) {
                        	for(int p=0;p<dutylist.size();p++) {
                        		//001代表校长
                        		if("校长".equals(dutylist.get(p).getDutyName())) {
                        			teacher="001";
                        			break;
                        		}else {
                        			//002代表老师
                        			teacher="002";
                        		}
                        	}
                        }
					}else if("3".equals(role)) {
						//获取学生信息
						Long str9 = System.currentTimeMillis();
						//调用学生接口获取信息
						String studentinfo = HttpUtils.sendFormDataPost("https://gwss.yceduyun.com/apis/user/V1.0/studentInfo",
								access_token,"002");
						Long str10 = System.currentTimeMillis();
						int studenttime =(int) (str10-str9);
						UserLogEntity entity3=new UserLogEntity();
						entity3.setId(IDGenerator.genUID());
						entity3.setUserId(WebContext.getSessionUserId());
						entity3.setUserName(WebContext.getSessionUser().getPersonName());
						entity3.setLogType("获取https://gwss.yceduyun.com/apis/user/V1.0/studentInfo信息");
						entity3.setTimes(userinfotime);
						entity3.setOperTime(new Date());
						//保存调用学生接口获取花费的时间日志
						tagInstitutionEvalutionService.insertUserLog(entity3);
						JSONObject studentinfojson = JSONObject.fromObject(studentinfo);
						JSONObject studentinfoDatajson = JSONObject.fromObject(studentinfojson.get("data"));
						if(null!= studentinfoDatajson) {
							String studentId=userid;
							//姓名
							String studentName=studentinfoDatajson.getString("username");
							String schoolId=studentinfoDatajson.getString("schoolId");//学校id
							String schoolName=studentinfoDatajson.getString("schoolName");//学校名称
							String schoolType=studentinfoDatajson.getString("schoolTye");//学校类型
							String gradeId=studentinfoDatajson.getString("gradeId");//年级id
							String gradeName=studentinfoDatajson.getString("gradeName");//年级名称
							String classId=studentinfoDatajson.getString("classId");//班级id
							String className=studentinfoDatajson.getString("className");//班级名称
							String gradeLever=studentinfoDatajson.getString("gradeLever");//年级等级
							String proName=studentinfoDatajson.getString("proName");//省份
							String cityName=studentinfoDatajson.getString("cityName");//市区
							String townName=studentinfoDatajson.getString("townName");//县级
							StudentClassInfoEntity studentClassInfoEntity = new StudentClassInfoEntity();
							studentClassInfoEntity.setStudentId(studentId);
							List<StudentClassInfoEntity> studentlist = new ArrayList<StudentClassInfoEntity>();
							studentlist = studentClassInfoService.searchStudentClassInfoList(studentClassInfoEntity);
							if(null!=studentlist && studentlist.size()>0) {
								//循环遍历学生信息
								for(int k=0;k<studentlist.size();k++) {
									StudentClassInfoEntity student = new StudentClassInfoEntity();
									student.setId(studentlist.get(k).getId());
									//删除学生信息
									studentClassInfoService.deleteStudentClassInfoById(student);
								}
							}
							//保存学生班级信息
							StudentClassInfoEntity stunfoEntity = new StudentClassInfoEntity();
							stunfoEntity.setStudentId(studentId);
							stunfoEntity.setStudentName(studentName);
							stunfoEntity.setSchoolId(schoolId);
							stunfoEntity.setSchoolName(schoolName);
							stunfoEntity.setSchoolType(schoolType);
							stunfoEntity.setGradeId(gradeId);
							stunfoEntity.setGradeName(gradeName);
							stunfoEntity.setGradeLever(gradeLever);
							stunfoEntity.setClassId(classId);
							stunfoEntity.setClassName(className);
							stunfoEntity.setProName(proName);
							stunfoEntity.setCityName(cityName);
							stunfoEntity.setTownName(townName);
							stunfoEntity.setCreateDate(new Date());
							studentClassInfoService.insertStudentClassInfo(stunfoEntity);
							Long str122 = System.currentTimeMillis();
							int studentinerttime =(int) (str122-str10);
							UserLogEntity entity32=new UserLogEntity();
							entity32.setId(IDGenerator.genUID());
							entity32.setUserId(WebContext.getSessionUserId());
							entity32.setUserName(WebContext.getSessionUser().getPersonName());
							entity32.setLogType("保存学生角色信息");
							entity32.setTimes(studentinerttime);
							entity32.setOperTime(new Date());
							tagInstitutionEvalutionService.insertUserLog(entity32);
						}
						
					}
					if (usertype != null && !"".equals(usertype) && usertype.equals("0")) {
						//老师
						if (!"".equals(role) && role.equals("1")) {
							userRole.setRoleId("01");
							userTeacherRole.setRoleId("04");
							loginUser.setUserKbn("A");
							saveUser.setUserKbn("A");
							if (userid != null && !"".equals(userid)) {
								userRole.setUserId(userid);
								userTeacherRole.setUserId(userid);
							}
							//管理者
						} else if (!"".equals(role) && role.equals("4")) {
							userRole.setRoleId("01");
							loginUser.setUserKbn("B");
							saveUser.setUserKbn("B");
							if (userid != null && !"".equals(userid)) {
								userRole.setUserId(userid);
							}
							//学生
						} else if(!"".equals(role) && role.equals("3")){
							loginUser.setUserKbn("C");
							saveUser.setUserKbn("C");
							//家长
						}else if(!"".equals(role) && role.equals("2")){
							loginUser.setUserKbn("D");
							saveUser.setUserKbn("D");
							//教研员
						}else if(!"".equals(role) && role.equals("5")){
							loginUser.setUserKbn("E");
							saveUser.setUserKbn("E");
						}
					}
					
					loginUser.setUserId(userid);
					loginUser.setUserName(account == null ? "" : account);
					loginUser.setSex(sex == null ? "" : sex);
					loginUser.setPersonName(name == null ? "" : name);
					loginUser.setPassword(MD5Util.getPassword4MD5("123456"));
					userRole.setCreateDate(new Date());
					userRole.setCreateUser(userid);
					userTeacherRole.setCreateDate(new Date());
					userTeacherRole.setCreateUser(userid);
					loginUser.setCreateDate(new Date());
					loginUser.setCreateUser(userid);
					loginUser.setLockFlag("0");

					saveUser.setUserId(userid);
					saveUser.setUserName(account == null ? "" : account);
					saveUser.setSex(sex == null ? "" : sex);
					saveUser.setPersonName(name == null ? "" : name);
					saveUser.setPassword(MD5Util.getPassword4MD5("123456"));
					saveUser.setCreateDate(new Date());
					saveUser.setCreateUser(userid);
					saveUser.setCreateDate(new Date());
					saveUser.setCreateUser(userid);
					saveUser.setLockFlag("0");
					saveUser.setTenantId(tenantId);
					saveUser.setHeadurl(headUrl);
					saveUser.setDuty(teacher);
					Long usertime1 = System.currentTimeMillis();
					courseService.saveUserAndRole(userRole, userTeacherRole, saveUser);
					Long usertime2 = System.currentTimeMillis();
					WebContext.loginUser(loginUser, true);
					WebContext.getSession().setAttribute("code", code);
					jo.put("result", "success");
					Long str123 = System.currentTimeMillis();
					System.out.println("计算花费时间---------------------------");
					int times = (int) (usertime2-usertime1);
					UserLogEntity entity=new UserLogEntity();
					entity.setId(IDGenerator.genUID());
					entity.setUserId(WebContext.getSessionUserId());
					entity.setUserName(WebContext.getSessionUser().getPersonName());
					entity.setLogType("获取保存用户信息");
					entity.setTimes(times);
					entity.setOperTime(new Date());
					tagInstitutionEvalutionService.insertUserLog(entity);
					System.out.println(str123-str12);
				}
			}

		}
		json.add(jo);
		return json;
	}
    *//**
     * 手机端首页
     * @return
     *//*
	@RequestMapping("/homePagePhone.action")
	public ModelAndView homePagePhone() {
		ModelAndView result = new ModelAndView("/course/client/homePagePhone");
		return result;
	}
    *//**
                * 教师页面
     * @return
     *//*
	@RequestMapping("/teacherHome.action")
	public ModelAndView teacherHome() {
		ModelAndView result = new ModelAndView("/course/client/teacherHomeBack");
		return result;
	}
    *//**
     * 学生页面
     * @return
     *//*
	@RequestMapping("/studentHome.action")
	public ModelAndView studentHome() {
		ModelAndView result = new ModelAndView("/course/client/studentHomeBack");
		return result;
	}
    *//**
     * 人员信息
     * @return
     *//*
	@RequestMapping("/sysUser.action")
	public ModelAndView sysUser() {
		ModelAndView result = new ModelAndView("/course/client/sysUser");
		result = getRoleAndNum(result);
		return result;
	}
    *//**
     * 系统日志
     * @return
     *//*
	@RequestMapping("/sysLog.action")
	public ModelAndView sysLog() {
		ModelAndView result = new ModelAndView("/course/client/sysLog");
		result = getRoleAndNum(result);
		return result;
	}
   *//**
    * 保存系统日志
    * @param proName
    * @param IpInfo
    * @return
    *//*
	@RequestMapping("/insertSysUser.action")
	@ResponseBody
	public JSONArray insertSysUser(String proName, String IpInfo) {
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		courseService.insertSysUser(proName, IpInfo);
		jo.put("result", "success");
		json.add(jo);
		return json;
	}
    *//**
     * 保存操作日志
     * @param browserInfo
     * @param sysInfo
     * @param IpInfo
     * @param operDetail
     * @param operSite
     * @return
     *//*
	@RequestMapping("/insertUserOper.action")
	@ResponseBody
	public JSONArray insertUserOper(String browserInfo, String sysInfo, String IpInfo, String operDetail,
			String operSite) {
		UserOperEntity oper = new UserOperEntity();
		oper.setSysInfo(browserInfo + "(" + sysInfo + ")");
		oper.setOperDetail(operDetail);
		oper.setOperSite(operSite);
		oper.setOperTime(new Date());
		oper.setOperIP(IpInfo);
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		courseService.inserUserOper(oper);
		jo.put("result", "success");
		json.add(jo);
		return json;
	}
	*//**
	 * 
	 * @param IpInfo
	 * @param logType
	 * @return
	 *//*
	@RequestMapping("/insertUserLog.action")
	@ResponseBody
	public JSONArray insertUserLog(String IpInfo, String logType) {
		UserLogEntity log = new UserLogEntity();
		log.setOperIP(IpInfo);
		log.setLogType(logType);
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		String userId = WebContext.getSessionUserId();
		String userName = WebContext.getSessionUser().getPersonName();
		log.setUserId(userId);
		log.setUserName(userName);
		courseService.insertUserLog(log);
		jo.put("result", "success");
		json.add(jo);
		WebContext.logout();
		return json;
	}
    *//**
                  * 根据用户名查询用户信息
     * @param username
     * @param IpInfo
     * @return
     *//*
	@RequestMapping("/getUserByUsername.action")
	@ResponseBody
	public JSONArray getUserByUserName(String username, String IpInfo) {
		TpUserEntity result = courseService.getUserByUsername(username);
		if (result != null) {
			UserLogEntity log = new UserLogEntity();
			log.setOperIP(IpInfo);
			log.setLogType("登录");
			log.setUserId(result.getUserId());
			log.setUserName(result.getPersonName());
			courseService.insertUserLog(log);
		}
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		if (result != null) {
			jo.put("result", "success");
		} else {
			jo.put("result", "error");
		}
		json.add(jo);
		return json;
	}

	*//**
	 * 系统首页
	 *//*
	@RequestMapping("/webEditor.action")
	public ModelAndView webEditor() {
		ModelAndView result = new ModelAndView("/course/client/webEditor");
		String serverName = WebContext.getRequest().getServerName();
		int serverPort = WebContext.getRequest().getServerPort();
		result.addObject("serverUrl", serverName + ":" + serverPort);
		return result;
	}
    *//**
     * 课程详情页面
     * @param courseId
     * @param schoolName
     * @return
     *//*
	@RequestMapping("/courseDetailInit.action")
	public ModelAndView courseDetailInit(String courseId,String schoolName) {
		ModelAndView result = new ModelAndView("/course/client/course_detail");
		ResultBean data = courseService.searchCourseDetail(courseId);
		if (!data.isStatus()) {
			throw new BusinessException(data.getMessages());
		}
		ChapterEntity entity = new ChapterEntity();
		entity.setCourseId(courseId);
		List<ChapterEntity> chapterCourseList = courseService.selectByCourseId(entity);
		result.addObject("chapterCourseList", chapterCourseList);
		result.addObject("courseView", data.getData());
		result.addObject("schoolName", schoolName);
		result.addObject("courseId", courseId);
		// 检索章节列表
		List<ChapterView> chapterList = chapterService.chapterList(courseId);
		result.addObject("chapterList", chapterList);
		String measureType="2";
		List<ChapterView> chapterList1 = chapterService.chapterList1(courseId,measureType);
		result.addObject("chapterExamList", chapterList1);
		List<CourseMessageEntity> userMsgList = courseService.getUserMsgList(courseId);
		if(userMsgList.size() > 0) {
			CourseMessageEntity msg = userMsgList.get(0);
			if(msg.getCheckStatus().equals("-1")) {
				result.addObject("msgInfo", "-1");
			}else if(msg.getCheckStatus().equals("1")) {
				result.addObject("msgInfo", "1");
			}else {
				result.addObject("msgInfo", "none");
			}
		}
		result = getRoleAndNum(result);
		return result;
	}
    *//**
     * 公开课详情页面
     * @param courseId
     * @return
     *//*
	@RequestMapping("/coursePublicDetail.action")
	public ModelAndView coursePublicDetail(String courseId) {
		ModelAndView result = new ModelAndView("/course/client/course_public_detail");
		ResultBean data = courseService.searchCourseDetail(courseId);
		if (!data.isStatus()) {
			throw new BusinessException(data.getMessages());
		}
		result.addObject("courseView", data.getData());
		result = getRoleAndNum(result);
		return result;
	}

	*//**
	 * 导师课程页面初始化
	 * 
	 * @param
	 * @return
	 *//*
	@RequestMapping("/teacherCourseInit.action")
	public ModelAndView teacherCourseInit(UserEntity user) {
		ModelAndView mv = new ModelAndView("/course/client/teacher_course_list");
		ResultBean result = courseService.teacherInfo(user);
		UserEntity use = new UserEntity();
		if (result.getData() != null) {
			use = (UserEntity) result.getData();
			if ("0".equals(use.getSex())) {
				use.setSex("男");
			} else {
				use.setSex("女");
			}
		}
		mv.addObject("user", use);
		return mv;
	}

	*//**
	 * 投票页面初始化
	 * 
	 * @param
	 * @return
	 *//*
	@RequestMapping("/voteItemInit.action")
	public ModelAndView voteItemInit(String courseId, String linkType) {
		ModelAndView mv = new ModelAndView("/course/client/vote_item");
		if (StringUtils.isNotEmpty(linkType)) {
			mv.addObject("linkType", linkType);
		}
		mv.addObject("courseId", courseId);
		return mv;
	}

	*//**
	 * 根据用户ID，课程ID检索用户课程
	 * 
	 * @param courseId
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchUserCourse.action")
	@ResponseBody
	public ResultBean searchUserCourse(String courseId) {
		ResultBean result = new ResultBean();
		if (courseId == null || courseId.equals("")) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_NULL_ID));

			return result;
		}

		UserCourseEntity uc = new UserCourseEntity();
		uc.setCourseId(courseId);
		// 从session中取出登录用户的ID
		String userId = WebContext.getSessionUserId();
		if (userId == null) {
			result.setStatus(false);
			result.setErrorCode(ErrorCode.NOT_LOGIN);
			result.setMessages(getMessage(MSG_E_SESSION_TIMEOUT));
			return result;
		}
		uc.setUserId(userId);
		uc = courseService.searchUserCourse(uc);
		result.setData(uc);
		result.setStatus(true);
		return result;
	}

	*//**
	 * 课程作业页面初始化
	 * 
	 * @param linkId   链接ID
	 * @param linkType 链接类型
	 * @return
	 *//*
	@RequestMapping("/homeWorkInit.action")
	public ModelAndView homeWorkInit(String linkId, String linkType) {
		ModelAndView mv = new ModelAndView("/course/client/home_work");
		if (StringUtils.isNotEmpty(linkType)) {
			mv.addObject("linkType", linkType);
		}
		mv.addObject("linkId", linkId);
		return mv;
	}

	*//**
	 * 检索我的作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/honeworkDetail.action")
	@ResponseBody
	public ResultBean honeworkDetail(HomeworkView homeworkView) {
		ResultBean result = new ResultBean();
		String userId = WebContext.getSessionUserId();
		List<HomeworkView> viewlist = new ArrayList<HomeworkView>();
		viewlist = (List<HomeworkView>) (courseService.honeworkList(homeworkView)).getData();
		for (int i = 0; i < viewlist.size(); i++) {
			CourseView courseView = new CourseView();
			courseView.setTeacherId(userId);
			if (courseService.isTeacher(courseView.getTeacherId())) {
				viewlist.get(i).setTeacher(1);
			} else {
				viewlist.get(i).setTeacher(0);
			}

		}

		result.setData(viewlist);
		return result;
	}

	@RequestMapping("/homeworkTitle.action")
	@ResponseBody
	public ResultBean homeworkTitle(HomeworkView homeworkView) {
		ResultBean result = new ResultBean();
		HomeworkEntity homeWork = courseService.getHomeWorkByLinkId(homeworkView);
		if (homeWork != null) {
			result.setStatus(true);
			result.setData(homeWork);
		}
		return result;
	}

	@RequestMapping("/homeWorkEdit.action")
	public ModelAndView homeWorkEdit(String id) {
		ModelAndView result = new ModelAndView("/course/client/home_work_edit");
		HomeworkView view = courseService.getHomeWorkById(id);
		result.addObject("homeWork", view);
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/homeWorkView.action")
	public ModelAndView homeWorkView(String id) {
		ModelAndView result = new ModelAndView("/course/client/home_work_view");
		HomeworkView view = courseService.getHomeWorkById(id);
		view.setCreateTime(sdf.format(view.getCreateDate()));
		result.addObject("homeWork", view);
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/homeWorkAddQuestion.action")
	public ModelAndView homeWorkAddQuestion(String id) {
		ModelAndView result = new ModelAndView("/course/client/home_work_question");
		HomeworkView view = courseService.getHomeWorkById(id);
		view.setCreateTime(sdf.format(view.getCreateDate()));
		result.addObject("homeWork", view);
		result.addObject("aq", new HomeworkAnswerQuestionEntity());
		result = getRoleAndNum(result);
		return result;
	}

	*//**
	 * 上传文件
	 *//*
	@RequestMapping("/upload.action")
	@ResponseBody
	public ResultBean upload(HomeworkView homeworkView) throws Exception {
		WebContext.init4CrossDomainAjax();
		return courseService.uploadDocument(homeworkView);
	}

	@RequestMapping("/download.action")
	public void download(@RequestParam(required = true) String fileId, String fileType, HttpServletResponse response)
			throws Exception {

		OutputStream output = new BufferedOutputStream(response.getOutputStream());
		InputStream input = null;
		try {
			FileEntity file = new FileEntity();
			if (StringUtil.isNotEmpty(fileType)) {
				file = fileService.getFile(fileId, fileType);
			} else {
				file = fileService.getFile(fileId, FILE_TYPE_HOMEWORK);
			}

			byte[] binary = file.getBinary();
			String fileName = file.getFileName();
			fileName = URLEncoder.encode(fileName, "UTF-8");
			response.reset();
			response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			response.addHeader("Content-Length", "" + file.getFileSize());
			response.setContentType("application/octet-stream;charset=UTF-8");
			if (StringUtil.isNotEmpty(file.getPath()) && StringUtil.isNotEmpty(ConfigUtil.getFileStorageRoot())) {
				// 文件有路径
				input = FileUtil.getInputStream(ConfigUtil.getFileStorageRoot(), file.getPath());
				IOUtils.copyLarge(input, output);
			} else {
				output.write(binary);
			}
			output.flush();
		} finally {
			IOUtils.closeQuietly(output);
			IOUtils.closeQuietly(input);
		}
	}

	*//**
	 * 课程作业管理页面初始化
	 * 
	 * @param
	 * @return
	 *//*
	@RequestMapping("/homeWorkReviewInit.action")
	public ModelAndView homeWorkReviewInit(String linkId, String linkType) {
		ModelAndView mv = new ModelAndView("/course/client/home_work_review");
		HomeworkView home = null;
		if (StringUtil.isNotEmpty(linkId) && StringUtil.isNotEmpty(linkType)) {
			home = courseService.searchworkDetail(linkId, linkType);
		}
		if (home == null) {
			home = new HomeworkView();
		}
		BaseEntity condition = new BaseEntity();
		condition.setPageSize(5);
		mv.addObject("linkId", linkId);
		mv.addObject("linkType", linkType);
		mv.addObject("title", home.getTitle());
		mv.addObject("courseTitle", home.getCourseTitle());
		mv.addObject("condition", condition);
		return mv;
	}

	*//**
	 * 导师品鉴作业初始化
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/homeworkReviewList.action")
	@ResponseBody
	public ResultBean homeworkReviewList(HomeworkView home) {

		return courseService.homeworkReviewList(home, PropertyUtil.getProperty("res.context.root"));
	}

	@RequestMapping("/gethomeworkReviewList.action")
	@ResponseBody
	public ResultBean gethomeworkReviewList(HomeworkView home) {
		return courseService.gethomeworkReviewList(home);
	}

	@RequestMapping("/gethomeworkReviewListByJson.action")
	@ResponseBody
	public JSONArray gethomeworkReviewListByJson(String courseId, String linkId, String linkType) throws IOException {

		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		List<HomeworkView> homeworkList = courseService.gethomeworkReviewListJson(courseId, linkId, linkType,
				PropertyUtil.getProperty("res.context.root"));
		if (homeworkList.size() > 0) {
			for (HomeworkView view : homeworkList) {
				Map<String, Object> courseERMap = new HashMap<String, Object>();
				courseERMap.put("userName", view.getCreateUser());
				courseERMap.put("audioContent", view.getContent() == null ? "" : view.getContent());
				// courseERMap.put("answerFileName", view.getAnswerFileName());
				courseERMap.put("score", view.getScore() == null ? "" : view.getScore());
				courseERMap.put("teacherComment", view.getTeacherComment() == null ? "" : view.getTeacherComment());
				// courseERMap.put("teacherScore", view.getTeacherScore() ==
				// null?"":view.getTeacherScore());
				// courseERMap.put("teacherComments", view.getTeacherComments() ==
				// null?"":view.getTeacherComments());
				resultList.add(courseERMap);
			}
		}
		jo.put("resultList", resultList);
		json.add(jo);
		return json;
	}

	*//**
	 * 导师评价
	 * 
	 * @author Id
	 * @param SCORE teacherComment
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/saveHomeworkReview.action")
	@ResponseBody
	public ResultBean saveHomeworkReview(HomeworkView home) {

		return courseService.saveHomeworkReview(home);
	}

	*//**
	 * 检索我的课程列表（检索用户课程，包括已关注、已学、已学完等检索条件以及检索列表中每一项课程的问卷）
	 * 
	 * @param finishType 完成类型（已收藏 1、已学2、3 已学完）
	 * @return 我的课程列表
	 * @author WUXIAOXIANG
	 *//*
	@RequestMapping("/userCourseList.action")
	@ResponseBody
	public ResultBean userCourseList(String finishType, Integer pageSize, Integer pageNumber) {
		// 检索我的课程
		ResultBean result = courseService.userCourseList(finishType, pageSize, pageNumber);
		return result;
	}

	@RequestMapping("/userCompulsoryList.action")
	@ResponseBody
	public ResultBean userCompulsoryList(String courseType, Integer pageSize, Integer pageNumber) {
		// 检索我的课程
		ResultBean result = courseService.userCompulsoryList(courseType, pageSize, pageNumber);
		return result;
	}

	*//**
	 * 检索我的作业列表
	 * 
	 * @author liuzhen
	 * @param
	 * @return 返回结果
	 *//*
	@RequestMapping("/userHomeworkList.action")
	@ResponseBody
	public ResultBean userHomeworkList(HomeworkView homeworkView, Integer pageSize, Integer pageNumber) {
		// 检索我的作业
		ResultBean result = courseService.userHomeworkList(homeworkView, pageSize, pageNumber);
		return result;
	}

	*//**
	 * 检索当前课程的所有作业
	 * 
	 * @author liuzhen
	 * @param
	 * @return 返回结果
	 *//*
	@RequestMapping("/alluserHomeworkList.action")
	@ResponseBody
	public ResultBean alluserHomeworkList(HomeworkView homeworkView, Integer pageSize, Integer pageNumber) {
		ResultBean result = courseService.getAlluserHomeworkList(homeworkView, pageSize, pageNumber);
		return result;
	}

	*//**
	 * 检索手机端导师个人中心的课程列表
	 * 
	 * @param pageSize   每页显示行数
	 * @param pageNumber 当前页
	 * @return 课程列表
	 * @author WUXIAOXIANG
	 *//*
	@RequestMapping("/teacherCourseAppList.action")
	@ResponseBody
	public ResultBean teacherCourseAppList(Integer pageSize, Integer pageNumber) {
		ResultBean result = courseService.teacherCourseAppList(pageSize, pageNumber);
		return result;
	}

	*//**
	 * 加入课程-支付详情初始化
	 * 
	 * @param courseId
	 * @return
	 *//*
	@SuppressWarnings({ "rawtypes" })
	@RequestMapping("/paydetail.action")
	public ModelAndView homeWorkReviewInit(String courseId) {
		ModelAndView mv = new ModelAndView("/course/client/course_pay_detail");
		UserEntity user = userService.searchById(WebContext.getSessionUserId());
		Map map = (Map) courseService.immediatePayDetail(courseId).getData();
		CourseView view = (CourseView) map.get("courseView");
		UserPoint userPoint = (UserPoint) map.get("userPoint");
		BigDecimal remainPoints = (BigDecimal) map.get("remainPoints");
		mv.addObject("user", user);
		mv.addObject("view", view);
		mv.addObject("userPoint", userPoint);
		mv.addObject("remainPoints", remainPoints);
		return mv;
	}

	*//**
	 * 用户管理页面 检索列表
	 * 
	 * @param condition
	 * @return 返回结果
	 *//*
	@RequestMapping("/search.action")
	@ResponseBody
	public ResultBean search(UserOperEntity condition) {
		return courseService.search4Page(condition);
	}

	@RequestMapping("/userOperInit.action")
	public ModelAndView userOperInit(String courseId) {
		ModelAndView result = new ModelAndView("/course/client/userOper");
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/mgrApprove.action")
	public ModelAndView mgrApprove(String courseId) {
		ModelAndView result = new ModelAndView("/course/client/mgr_approve");
		result = getRoleAndNum(result);
		return result;
	}
	
	@RequestMapping("/approveSearch.action")
	@ResponseBody
	public ResultBean approveSearch(CourseApproveEntity entity){
		return courseService.approveSearch(entity);
	}
	
	@RequestMapping("/saveHomeWork.action")
	@ResponseBody
	public ResultBean saveHomeWork(HomeworkView homework) throws Exception {
		return courseService.saveHomeWorkAnswer(homework);
	}

	@RequestMapping("/saveHomeWorkById.action")
	@ResponseBody
	public ResultBean saveHomeWorkById(HomeworkView homework) throws Exception {
		return courseService.saveHomeWorkById(homework);
	}
	
	@RequestMapping("/saveHomeWorkReviewById.action")
	@ResponseBody
	public ResultBean saveHomeWorkReviewById(String courseJson) throws Exception {
		HomeworkView homework = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, HomeworkView.class);
		return courseService.saveHomeWorkReviewById(homework);
	}

	@RequestMapping("/saveHomeWorkAnswerQuestion.action")
	@ResponseBody
	public ResultBean saveHomeWorkAnswerQuestion(HomeworkAnswerQuestionEntity aq) throws Exception {
		return courseService.saveHomeWorkAnswerQuestion(aq);
	}

	@RequestMapping("/answerQuestionList.action")
	@ResponseBody
	public ResultBean answerQuestionList(HomeworkAnswerQuestionEntity aq) {
		return courseService.searchAnswerQuestionList(aq);
	}

	@RequestMapping("/likeHomeWorkById.action")
	@ResponseBody
	public ResultBean likeHomeWorkById(String id) throws Exception {
		return courseService.likeHomeWorkById(id);
	}

	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/institutionlist.action")
	public ModelAndView institutionlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/institution/manager/institution_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		return result;
	}

	*//**
	 * 学校评价总列表
	 * 
	 * @return
	 *//*
	@RequestMapping("/institutionManger.action")
	public ModelAndView institutionManger() {
		ModelAndView result = new ModelAndView();
		UserEntity user = WebContext.getSessionUser();
		if (!"游客".equals(user.getPersonName())) {
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			TpUserEntity entity =new TpUserEntity();
			entity = userManagementService.searchById(userId);
			if ("A".equals(role) && "001".equals(entity.getDuty())) {
				result = new ModelAndView("/institution/manager/institutionManagerBack");
			} else if ("C".equals(role) || ("A".equals(role) && !"001".equals(entity.getDuty()))) {
				result = new ModelAndView("/institution/manager/institutionEvlutionStudentBack");
			}else if("B".equals(role)) {
				result = new ModelAndView("/institution/manager/institutionBack");
			}
		} else {
			result = new ModelAndView("/institution/manager/institutionEvlutionStudentBack");
		}
		
		return result;
	}
	

	*//**
	 * 标签一览
	 *//*
	@RequestMapping("/searchTagList2.action")
	@ResponseBody
	public ResultBean searchTagList2(TagInstitutionEntityView tagEntity) {
		return taxonomyInstitutionService.searchTagList2(tagEntity);

	}
	*//**
	 * 标签一览
	 *//*
	@RequestMapping("/searchInstitutionTagList2.action")
	@ResponseBody
	public ResultBean searchInstitutionTagList2(TagInstitutionEntityView tagEntity) {
		tagEntity.setCreateUser(WebContext.getSessionUserId());
		return taxonomyInstitutionService.searchInstitutionTagList2(tagEntity);

	}
	*//**
	 * 标签一览
	 *//*
	@RequestMapping("/searchTagSchoolList2.action")
	@ResponseBody
	public ResultBean searchTagSchoolList2(TagInstitutionEntityView tagEntity) {
		return taxonomyInstitutionService.searchTagList2(tagEntity);

	}
	*//**
	 * 分类基本列表
	 * 
	 * @return
	 *//*
	@RequestMapping("/institutioninit.action")
	public ModelAndView institutioninit() {
		ModelAndView mv = new ModelAndView("/institutiontaxonomy/manager/taxonomy_course_list");
		BaseEntity condition = new BaseEntity();
		condition.setPageSize(5);
		mv.addObject("condition", condition);
		return mv;
	}

	*//**
	 * 分类查询列表
	 * 
	 * @param businessType
	 * @return
	 *//*
	@RequestMapping("/searchTagTypeList2.action")
	@ResponseBody
	public List<TagTypeInstitutionView> searchTagTypeList2(String businessType) {

		return taxonomyInstitutionService.searchTagTypeList2(businessType);

	}

	*//**
	 * 
	 * 删除标签
	 * 
	 * @param tagIds
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/delTag.action")
	@ResponseBody
	public ResultBean delTag(String tagId, String ico) throws Exception {

		return taxonomyInstitutionService.delTag(tagId, ico);

	}

	*//**
	 * 保存信息
	 * 
	 * @param browserInfo
	 * @param sysInfo
	 * @param IpInfo
	 * @param operDetail
	 * @param operSite
	 * @return
	 *//*
	@RequestMapping("/insertUserInstitution.action")
	@ResponseBody
	public JSONArray insertUserInstitution(String browserInfo, String sysInfo, String IpInfo, String operDetail,
			String operSite) {
		UserOperEntity oper = new UserOperEntity();
		oper.setSysInfo(browserInfo + "(" + sysInfo + ")");
		oper.setOperDetail(operDetail);
		oper.setOperSite(operSite);
		oper.setOperTime(new Date());
		oper.setOperIP(IpInfo);
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		tagInstitutionEvalutionService.inserUserOper(oper);
		jo.put("result", "success");
		json.add(jo);
		return json;
	}

	*//**
	 * 编辑标签
	 * 
	 * @param TagEntity
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/updateTag.action")
	@ResponseBody
	public ResultBean updateTag(TagInstitutionEntity tagEntity) throws Exception {

		return taxonomyInstitutionService.updateTag(tagEntity);

	}

	*//**
	 * 保存新增标签
	 * 
	 * @param parentId 父标签ID,tagTypeId:标签类ID,tagName:标签名称
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveTag2.action")
	@ResponseBody
	public ResultBean saveTag2(TagInstitutionEntityView tagEntity) throws Exception {
		return taxonomyInstitutionService.saveTag2(tagEntity);

	}

	*//**
	 * 
	 * 初始化课程列表
	 * 
	 * @param condition 课程对象
	 * @return 返回课程列表初始化页面
	 *//*
	@RequestMapping("/institutionListInit.action")
	public ModelAndView institutionListInit(InstitutionMessageEntity condition) {
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		result.addObject("course", new InstitutionMessageEntity());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}

	*//**
	 * 
	 * 检索评价列表
	 * 
	 * @param queryInfo 机构对象
	 * @return 返回评价列表
	 *//*
	@RequestMapping("/listSearch.action")
	@ResponseBody
	public ResultBean listSearch(TbInstitutionEvalutionManagerView queryInfo) {
		queryInfo.setCreateUser(WebContext.getSessionUserId());
		return taxonomyInstitutionService.listSearch(queryInfo);
	}

	*//**
	 * 
	 * 检索评价问卷列表
	 * 
	 * @param queryInfo 机构对象
	 * @return 返回评价列表
	 *//*
	@RequestMapping("/listQestionSearch.action")
	@ResponseBody
	public ResultBean listQestionSearch(MeasurementInstitutionEntity queryInfo) {
		UserEntity user = WebContext.getSessionUser();	
		String role = user.getUserKbn();
		//判断权限
		String userId = user.getCreateUser();
		queryInfo.setCreateUser(userId);
		return taxonomyInstitutionService.listSchoolMeasurementSearch(queryInfo);
	}

	*//**
	 * 评价新增页面初始化
	 * 
	 * @return
	 *//*
	@RequestMapping("/addInit.action")
	public ModelAndView addInit() {
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_add");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		result.addObject("pageType", "add");
		result.addObject("course", new TbInstitutionEvalutionManagerView());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}

	*//**
	 * 搜素全部学校
	 * 
	 * @param query
	 * @return
	 *//*
	@RequestMapping("/searchSchoolAll.action")
	@ResponseBody
	public ResultBean searchSchoolAll(InstitutionEntity query) {
		return institutionService.getTeacherAll(query);
	}
	*//**
	 * 搜素评价问卷全部学校
	 * 
	 * @param query
	 * @return
	 *//*
	@RequestMapping("/searchEvalutionSchoolAll.action")
	@ResponseBody
	public ResultBean searchEvalutionSchoolAll(InstitutionEntity query) {
		if(null!=query && StringUtil.isNotBlank(query.getInstitutionNameseacher())) {
			query.setInstitutionName(query.getInstitutionNameseacher());
		}
		return institutionService.getEvalutionTeacherAll(query);
	}

	*//**
	 * 
	 * 根据学校ID，检索学校信息
	 * 
	 * @return
	 *//*
	@RequestMapping("/searchSchool.action")
	@ResponseBody
	public ResultBean searchSchool(String id) {
		ResultBean result = ResultBean.success();
		result.setStatus(true);
		return institutionService.searchSchool(id);
	}

	*//**
	 * 保存评价任务信息
	 * 
	 * @param courseJson 任务信息
	 * @return 返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/saveEvalutionInfo.action")
	@ResponseBody
	public ResultBean saveEvalutionInfo(String courseJson) throws Exception {
		TbInstitutionEvalutionManagerView course = JSONUtil.parse(
				JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson,
				TbInstitutionEvalutionManagerView.class);
		ResultBean result = ResultBean.success();
		course.setStatus("0");
		if (StringUtil.isEmpty(course.getId())) {
			institutionService.insertCourseCatagory(course);
		} else {
			institutionService.updateCourseAll(course);
		}
		return result;
	}

	*//**
	 * 暂存
	 * 
	 * @param courseJson
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/zancunEvalutionInfo.action")
	@ResponseBody
	public ResultBean zancunEvalutionInfo(String courseJson) throws Exception {
		TbInstitutionEvalutionManagerView course = JSONUtil.parse(
				JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson,
				TbInstitutionEvalutionManagerView.class);
		ResultBean result = ResultBean.success();
		course.setStatus("0");
		if (StringUtil.isEmpty(course.getId())) {
			institutionService.insertCourseCatagory(course);
		} else {
			institutionService.updateCourseAll(course);
		}
		return result;
	}

	*//**
	 * 删除评价任务信息
	 * 
	 * @param id
	 * @return
	 *//*
	@RequestMapping("/deleteEvalution.action")
	@ResponseBody
	public ResultBean deleteEvalution(String id) {
		ResultBean result = new ResultBean();
		institutionService.deleteEvalution(id);
		result.setMessages(getMessage(MSG_S_DELETE));
		return result;
	}

	@RequestMapping("/editInit.action")
	public ModelAndView editInit(String id) {
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_add");
		String status = "0";
		String type = "edit";
		TbInstitutionEvalutionManagerView course = tagInstitutionEvalutionService.searchEvalutionById(id, type);
		String evalutionId = course.getId();
		String institutionId = course.getTeacherId();
		List<TagLinkInstitutionEntity> entityList = new ArrayList<TagLinkInstitutionEntity>();
		TagLinkInstitutionEntity entity = new TagLinkInstitutionEntity();
		entity.setLinkId(evalutionId);
		entityList = tagInstitutionEvalutionService.searchList(entity);
		if (null != entityList) {
			for (int i = 0; i < entityList.size(); i++) {
				if (StringUtils.isBlank(entityList.get(i).getParentTagId())) {
					TagInstitutionEntity isentity = new TagInstitutionEntity();
					isentity.setTagId(entityList.get(i).getTagId());
					isentity = tagInstitutionService.searchFirstData(isentity);
					course.setFirstTagName(isentity.getTagName());
					course.setFirstTag(isentity.getTagName());
				} else {
					TagInstitutionEntity isentity = new TagInstitutionEntity();
					isentity.setTagId(entityList.get(i).getTagId());
					isentity = tagInstitutionService.searchFirstData(isentity);
					course.setSecondTag(isentity.getTagName());
					course.setSecondTagName(isentity.getTagName());
				}
			}
		}
		InstitutionEntity institution = new InstitutionEntity();
		institution = institutionService.selectInstitutionById(institutionId);
		course.setInstitutionName(institution.getInstitutionName());
		course.setSchoolType(institution.getSchoolType());
		course.setInstitutionAbbreviation(institution.getInstitutionAbbreviation());
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		result.addObject("tagsSecond", tagsSecond);
		result.addObject("course", course);
		result.addObject("pageType", type);
		return result;
	}

	*//**
	 * 问卷添加
	 * 
	 * @param courseId 评价任务ID
	 * @return
	 *//*
	@RequestMapping("/measureInit.action")
	@ResponseBody
	public ModelAndView measureInit(String courseId) {
		ModelAndView result = new ModelAndView("institution/manager/insitution_measure_add");
		MeasurementInstitutionEntity measure = new MeasurementInstitutionEntity();
		result.addObject("measure", measure);
		result.addObject("courseId", courseId);
		result.addObject("pageType", "add");
		return result;
	}
	*//**
	 * 问卷添加
	 * 
	 * @param courseId 评价任务ID
	 * @return
	 *//*
	@RequestMapping("/measureInit1.action")
	@ResponseBody
	public ModelAndView measureInit1(String courseId) {
		ModelAndView result = new ModelAndView("institution/manager/insitution_measure_editor");
		MeasurementInstitutionEntity measure = new MeasurementInstitutionEntity();
		measure.setLinkId(courseId);
		measure = measurementInstitutionService.findInstitutionMeasurement(measure);
		List<QuestionInstitutionEntity> questionList = measurementInstitutionService.searchQuestionList(measure.getId());
		measure.setQuestionList(questionList);
		result.addObject("measure", measure);
		result.addObject("courseId", courseId);
		result.addObject("pageType", "add");
		return result;
	}
	*//**
	 * 
	 * 问卷编辑页面初始化
	 * 
	 * @param courseId
	 * @return
	 *//*
	@RequestMapping("/measureInsitutionEditInit.action")
	@ResponseBody
	public ModelAndView measureEditInit(String courseId) {
		ModelAndView result = new ModelAndView("institution/manager/insitution_measure_add");
		MeasurementInstitutionEntity measure = measurementInstitutionService.seachMeasureById(courseId);
		result.addObject("measure", measure);
		result.addObject("courseId", courseId);
		result.addObject("isOnlyView", true);
		result.addObject("pageType", "edit");
		return result;
	}

	*//**
	 * 
	 * 问卷添加/编辑
	 * 
	 * @param measureJson
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveMeasure.action")
	@ResponseBody
	public ResultBean saveMeasure(String measureJson) throws Exception {
		MeasurementInstitutionEntity measure = JSONUtil.parse(
				JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), measureJson,
				MeasurementInstitutionEntity.class);
		ResultBean result = ResultBean.success();
		if (StringUtil.isEmpty(measure.getId())) {
			measurementInstitutionService.saveMeasure(measure);
		} else {
			measurementInstitutionService.updateInstitutionMeasure(measure);
		}
		return result;
	}

	*//**
	 * 
	 * 初始化评价问卷列表
	 * 
	 * @param condition 评价对象
	 * @return 返回课程列表初始化页面
	 *//*
	@RequestMapping("/institutionQuestListInit.action")
	public ModelAndView institutionQuestListInit(InstitutionMessageEntity condition) {
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_question_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		result.addObject("course", new InstitutionMessageEntity());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}

	*//**
	 * 
	 * 评价的发布/取消发布
	 * 
	 * @param id     课程id
	 * @param status 评价状态：0 未发布 1 已发布
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/publicEvalution.action")
	@ResponseBody
	public ResultBean publicEvalution(String id, String status) throws Exception {
		tagInstitutionEvalutionService.publicEvalution(id, status);
		return ResultBean.success(MSG_S_SUBMIT);
	}

	*//**
	 * 问卷显示页面
	 *//*
	@RequestMapping("/question.action")
	public ModelAndView question(String linkId, String linkType) {
		ModelAndView result = new ModelAndView("/institution/manager/institution_evalution_questionner_item");
		result.addObject("linkId", linkId);
		result.addObject("linkType", linkType);
		return result;
	}

	*//**
	 * 问卷题目选项
	 *//*
	@RequestMapping("/questionnaireDetial.action")
	@ResponseBody
	public ResultBean questionnaireDetial(String linkId, String linkType) {
		return questionInstitutionService.questionnaireDetial(linkId, linkType);
	}

	*//**
	 * 问卷提交
	 *//*
	@RequestMapping("/submitQuestionnaire.action")
	@ResponseBody
	public ResultBean submitQuestionnaire(String questionIds, String optionIds, String measureId) {
		return questionInstitutionService.submitQuestionnaire(questionIds, optionIds, measureId);
	}

	*//**
	 * 问卷调查结果列表
	 * 
	 * @author heyaqin
	 * @return 返回结果
	 *//*
	@RequestMapping("/questionResultList.action")
	public ModelAndView questionResultList(MeasurementInstitutionEntity condition) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_result_list");
		mv.addObject("condition", condition);
		UserEntity user = WebContext.getSessionUser();
		if (!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if (roleList.size() > 0) {
				for (UserRoleEntity role : roleList) {
					if (role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			} else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}

	*//**
	 * 问卷调查列表检索
	 * 
	 * @author
	 * @return 返回结果
	 *//*
	@RequestMapping("/questionResultListSearch.action")
	@ResponseBody
	public ResultBean questionResultListSearch(MeasurementInstitutionEntity condition) {
			UserEntity user = WebContext.getSessionUser();	
			String userId = user.getCreateUser();
			condition.setCreateUser(userId);
		return userManagementService.selectQuestionResultList(condition);
	}

	*//**
	 * 问卷统计列表初始化
	 * 
	 * @author
	 * @param id 问卷ID
	 * @return 返回结果
	 *//*
	@RequestMapping("/viewMeasurementListInit.action")
	public ModelAndView viewMeasurementListInit(String id) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_statistics");
		int optionNum = userManagementService.searchInstitutionTotalOptions(id) + 1;
		QuestionOptionInstitutionEntity questionOptionEntity = new QuestionOptionInstitutionEntity();
		questionOptionEntity.setMeasureId(id);
		mv.addObject("questionOptionEntity", questionOptionEntity);
		mv.addObject("optionNum", optionNum);
		// 问卷统计结果列表
		ArrayList<Map<String, String>> optionMapList = userManagementService.selectInstitutionOptionsList(id);
		mv.addObject("optionMapList", optionMapList);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 问卷统计列表检索
	 * 
	 * @author HEYAQIN
	 * @param id 问卷ID
	 * @return 返回结果
	 *//*
	@RequestMapping("/viewMeasurementList.action")
	@ResponseBody
	public ArrayList<Map<String, String>> viewMeasurementList(String id) {
		if (StringUtil.isEmpty(id)) {
			return null;
		} else {
			ArrayList<Map<String, String>> optionMapList = userManagementService.selectInstitutionOptionsList(id);
			return optionMapList;
		}
	}

	*//**
	 * 问卷参与人员 检索
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/viewUserSearch.action")
	@ResponseBody
	public ResultBean viewUserSeacher(String measurementId) {
		return userManagementService.selectEvalutionQuestionUserList(measurementId);
	}

	*//**
	 * 问卷参与人员回答详情
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/viewDetailInit.action")
	public ModelAndView viewDetailInit(UserMeasurementResultEntity condition) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_user_detail");
		if (StringUtil.isEmpty(condition.getMeasurementId())) {
			throw new BusinessException("参数错误!");
		}

		List<UserMeasurementResultEntity> list = measuermentService
				.searchInstitutionUserMeasurementResultByEntiy(condition);
		if (list != null && list.size() != 0) {
			mv.addObject("condition", list.get(0));
		} else {
			mv.addObject("condition", new UserMeasurementResultEntity());
		}
		return mv;
	}

	*//**
	 * 问卷回答详情
	 * 
	 * @param measurementId
	 * @param userId
	 * @return
	 *//*
	@RequestMapping("/viewUserAnswerSearch.action")
	@ResponseBody
	public ResultBean viewUserAnswerSearch(String measurementId, String userId) {
		return userManagementService.selectEvalutionAnswerUserList(measurementId, userId);
	}

	*//**
	 * 检索测试信息
	 * 
	 * @param linkId
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchMeasuerment.action")
	@ResponseBody
	public ResultBean searchMeasuerment(String measurementId, String userId) {
		if (StringUtil.isEmpty(measurementId)) {
			return ResultBean.error(getMessage("参数错误!"));
		}
		ResultBean result = ResultBean.success();
		List<QuestionInstitutionEntity> questionList = measurementInstitutionService
				.searchMeasuermentInstitutionNormalAndExam(userId, measurementId);
		result.setData(questionList);
		return result;
	}

	*//**
	 * 问卷参与人员 画面初始化
	 * 
	 * @param condition
	 * @return
	 *//*
	@SuppressWarnings("unchecked")
	@RequestMapping("/viewUserInit.action")
	public ModelAndView viewUserInit(MeasurementInstitutionEntity condition) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_user_list");
		mv.addObject("measurementId", condition.getId());
		List<MeasurementInstitutionEntity> list = measurementInstitutionService.search(condition);
		if (list != null && list.size() != 0) {
			mv.addObject("condition", list.get(0));
		} else {
			throw new BusinessException("参数错误!");
		}
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 删除问卷调查列表
	 * 
	 * @author heyaqin
	 * @return 返回结果
	 *//*
	@RequestMapping("/deleteQuestionList.action")
	@ResponseBody
	public ResultBean deleteQuestionList(String id) {
		userManagementService.deleteEvalutionQuestionList(id);
		return ResultBean.success();
	}

	*//**
	 * 导入页面初始化
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/importInit.action")
	public String importInit() {
		return "institution/manager/institution_import";
	}

	*//**
	 * 导入机构
	 * 
	 * @param linkId
	 * @return 返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/importUpload.action")
	@ResponseBody
	public ResultBean importCourse(@RequestParam("institution") CommonsMultipartFile institution,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		InputStream input = null;
		try {
			input = institution.getInputStream();
			File targetFile = FileUtil.writeToTmpFile(input);
			return institutionService.importInstitution(targetFile);
		} finally {
			IOUtils.closeQuietly(input);
		}
	}

	*//**
	 * 批量导出机构
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/exportInstitution.action")
	@ResponseBody
	public InstitutionManagerView exportInstitution(InstitutionEntity entity) throws Exception {
		List<InstitutionManagerView> viewList = new ArrayList<InstitutionManagerView>();
		UserEntity userEntity = WebContext.getSessionUser();
		if (!"游客".equals(userEntity.getPersonName())) {
			String role = userEntity.getUserKbn();
			String userId=userEntity.getUserId();
			if("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				entity.setCreateUser(userId);
				viewList = institutionService.exportInstitution(entity);
			}else if("B".equals(role)) {
				entity.setCreateUser(userId);
				viewList = institutionService.exportManagerInstitution(entity);
			}
		}
		
		InstitutionManagerView user = new InstitutionManagerView();
		user.setInstitutionList(viewList);
		ExcelDataBind edbUser = new ExcelDataBind(user);
		InputStream is = null;
		OutputStream os = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String date = sdf.format(new Date());
			String fileName = URLEncoder.encode("组织机构" + date + ".xlsx", "UTF-8");
			WebContext.getResponse().reset();
			is = InstitutionService.class.getResourceAsStream("institutionExportTemplate.xlsx");
			WebContext.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			WebContext.getResponse()
					.setContentType(" application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8");
			edbUser.bind(is, WebContext.getResponse().getOutputStream());
		} catch (IOException ex) {

		} finally {

			IOUtils.closeQuietly(is);
			IOUtils.closeQuietly(os);
		}
		return null;
	}

	*//**
	 * 增加机构方法
	 * 
	 * @param entity
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/addJyInstitution.action")
	public ModelAndView addJyInstitution(InstitutionEntity entity) throws Exception {
		ModelAndView mv = new ModelAndView("institution/manager/institution_add");
		InstitutionEntity institutionEntity = new InstitutionEntity();
		UserEntity user = WebContext.getSessionUser();
		String role = user.getUserKbn();
		//判断权限
		List<InstitutionEntity> schoollist=new ArrayList<InstitutionEntity>();
		String userId = user.getCreateUser();
		if ("A".equals(role)) {
			TpUserEntity tpUserEntity =new TpUserEntity();
			tpUserEntity = userManagementService.searchById(userId);
			institutionEntity.setInstitutionName(tpUserEntity.getSchoolName());
			institutionEntity=institutionService.seacherPersonEdution(institutionEntity);
			if(null!=institutionEntity) {
				entity.setInstitutionName(institutionEntity.getInstitutionName());
			}
			schoollist.add(institutionEntity);
		}else if("B".equals(role)) {
			institutionEntity.setCreateUser(userId);
			institutionEntity= institutionService.searchEdution(institutionEntity);
            if(null!=institutionEntity) {
            	entity.setInstitutionName(institutionEntity.getInstitutionName());
			}
            schoollist.add(institutionEntity);
		}
		mv.addObject("schoollist", schoollist);
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 添加学校信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/addInstitution.action")
	public ModelAndView addInstitution(String institutionParentId) throws Exception {
		InstitutionEntity entity = new InstitutionEntity();
		entity.setInstitutionParentId(institutionParentId);
		ModelAndView mv = new ModelAndView("institution/manager/institution_addSchool");
		List<InstitutionEntity> schoollist=new ArrayList<InstitutionEntity>();
		UserEntity user = WebContext.getSessionUser();
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				schoollist = institutionService.seacherSchoolLocationList(entity);
			} else if("B".equals(role)) {
				entity.setCreateUser(userId);
				schoollist= institutionService.searchEdutionSchoolList(entity);
			}
		
		mv.addObject("schoollist", schoollist);
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 删除班级
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/deleteInstitutionById.action")
	@ResponseBody
	public ResultBean deleteGradeById(String id) throws Exception {
		InstitutionEntity entity = new InstitutionEntity();
		entity.setId(id);
		institutionService.deleteInstitutionById(entity);
		return ResultBean.success(DELETE_GRADE_SUCCESS);
	}

	*//**
	 * 跳转到班级编辑页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/institutionEdit.action")
	public ModelAndView institutionEdit(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		ModelAndView mv = new ModelAndView("institution/manager/institution_edit");
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 检索班级信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchInstitution.action")
	@ResponseBody
	public ResultBean searchNotice(InstitutionEntity entity) {
		return institutionService.searchInstitutionEvalutionList(entity);
	}
	*//**
	 * 学校检索班级信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/schoolsearchInstitution.action")
	@ResponseBody
	public ResultBean schoolsearchInstitution(InstitutionEntity entity) {
		return institutionService.searchInstitutionEvalutionList1(entity);
	}
	*//**
	 * 检索班级信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchSchoolInstitution.action")
	@ResponseBody
	public ResultBean searchSchoolInstitution(InstitutionEntity entity) {
		ResultBean result = new ResultBean();
		UserEntity user = WebContext.getSessionUser();
		if (!"游客".equals(user.getPersonName())) {
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				entity.setCreateUser(userId);
				result=institutionService.searchInstitutionTeacherEvalutionList(entity);
			} else if ("C".equals(role)) {
				entity.setCreateUser(userId);
				StudentClassInfoEntity classInfo = new StudentClassInfoEntity();
				classInfo.setStudentId(userId);
				classInfo = studentClassInfoService.searchStudentClassInfo(classInfo);
			    if(null!=classInfo) {
			    	entity.setInstitutionName(classInfo.getSchoolName());
					result= institutionService.searchInstitutionEvalutionList(entity);
			    }
			}else if("B".equals(role)) {
				entity.setCreateUser(userId);
				result= institutionService.searchInstitutionManagerEvalutionList(entity);
			}
		}
		return result;
	}

	*//**
	 * 新增信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/insertInstitution.action")
	@ResponseBody
	public ResultBean insertInstitution(InstitutionEntity entity) throws Exception {
		ResultBean result = new ResultBean();
		InstitutionEntity institutionEntity = new InstitutionEntity();
		institutionEntity.setInstitutionName(entity.getInstitutionName());
		List<InstitutionEntity> datalist = (List<InstitutionEntity>) institutionService.searchInstitutionEvalutionList1(institutionEntity).getData();
		if(null!=datalist && datalist.size()>0) {
			result.setStatus(false);
			result = ResultBean.error("学校名称已存在");
		}else {
//			String institutionType=entity.getInstitutionType();
//			TagInstitutionEntity tagInstitutionEntity = new TagInstitutionEntity();
//			tagInstitutionEntity.setTagId(institutionType);
//			tagInstitutionEntity = tagInstitutionService.searchFirstData(tagInstitutionEntity);
//			entity.setInstitutionType(tagInstitutionEntity.getTagName());
			if(StringUtil.isNotBlank(entity.getSchoolNatrue())) {
				if("1".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("小学");
				}else if("2".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("初中");
				}else if("3".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("高中");
				}else if("4".equals(entity.getSchoolNatrue())) {
				   entity.setInstitutionType("九年一贯制");
			    }
			}
			institutionService.insertInstitution(entity, getCompressConfigByName("imageP"));
			result.setStatus(true);
			result= ResultBean.success(MSG_ADD_GRADE_SUCCESS);
		}
		return result;
	}
	*//**
	 * 新增信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/insertSchoolEvalutionInstitution.action")
	@ResponseBody
	public ResultBean insertSchoolEvalutionInstitution(InstitutionEntity entity) throws Exception {
		institutionService.insertInstitution(entity, getCompressConfigByName("imageP"));
		return ResultBean.success(MSG_ADD_GRADE_SUCCESS);
	}
	*//**
	 * 编辑保存
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/updateInstitution.action")
	@ResponseBody
	public ResultBean updateInstitution(InstitutionEntity entity) throws Exception {
		ResultBean result = new ResultBean();
		InstitutionEntity institutionEntity = new InstitutionEntity();
		institutionEntity.setInstitutionName(entity.getInstitutionName());
		List<InstitutionEntity> datalist = (List<InstitutionEntity>) institutionService.searchInstitutionEvalutionList1(institutionEntity).getData();
		if(null!=datalist && datalist.size()>0 && !entity.getId().equals(datalist.get(0).getId())) {
			result.setStatus(false);
			result = ResultBean.error("学校名称已存在");
		}else {
//			String institutionType=entity.getInstitutionType();
//			TagInstitutionEntity tagInstitutionEntity = new TagInstitutionEntity();
//			tagInstitutionEntity.setTagId(institutionType);
//			tagInstitutionEntity = tagInstitutionService.searchFirstData(tagInstitutionEntity);
//			entity.setInstitutionType(tagInstitutionEntity.getTagName());
			if(StringUtil.isNotBlank(entity.getSchoolNatrue())) {
				if("1".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("小学");
				}else if("2".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("初中");
				}else if("3".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("高中");
				}else if("4".equals(entity.getSchoolNatrue())) {
					   entity.setInstitutionType("九年一贯制");
				}
			}
			institutionService.updateInstitution(entity, getCompressConfigByName("imageP"));
			result.setStatus(true);
			result = ResultBean.success("创建学校成功");
		}
		return result;
	}
	*//**
	 * 编辑保存
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/updateSchoolEvalutionInstitution.action")
	@ResponseBody
	public ResultBean updateSchoolEvalutionInstitution(InstitutionEntity entity) throws Exception {
		entity.setCreateUser(WebContext.getSessionUserId());
		institutionService.updateInstitution(entity, getCompressConfigByName("imageP"));
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/institutionStudentlist.action")
	public ModelAndView institutionStudentlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/institution/manager/institution_student_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		return result;
	}

	*//**
	 * 跳转到班级查看页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/institutionLook.action")
	public ModelAndView institutionLook(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		ModelAndView mv = new ModelAndView("institution/manager/institution_look");
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 老师分享作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/shareWork.action")
	@ResponseBody
	public ResultBean shareWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.shareHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 老师取消分享作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/cancleShareWork.action")
	@ResponseBody
	public ResultBean cancleShareWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.cancleShareHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);

	}

	*//**
	 * 老师置顶作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/topWork.action")
	@ResponseBody
	public ResultBean topWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.topHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 老师取消置顶作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/cancleTopWork.action")
	@ResponseBody
	public ResultBean cancleTopWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.cancleTopHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 老师删除作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/deleteWork.action")
	@ResponseBody
	public ResultBean deleteWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.deleteWork(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/baseinstitutionlist.action")
	public ModelAndView baseinstitutionlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/baseinstitution/manager/base_institution_list");
		result.addObject("condition", condition);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/schoolbaseinstitutionlist.action")
	public ModelAndView schoolbaseinstitutionlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/baseinstitution/manager/base_school_institution_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 增加机构方法
	 * 
	 * @param entity
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/addBaseInstitution.action")
	public ModelAndView addBaseInstitution(InstitutionEntity entity) throws Exception {
		ModelAndView mv = new ModelAndView("baseinstitution/manager/base_institution_add");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList1(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		List<TagInstitutionEntity> tagList = tagInstitutionService.searchTagInstitutionAll();
		mv.addObject("tagList", tagList);
		mv.addObject("catagoryTags", tags);
		mv.addObject("tagsSecond", tagsSecond);
		mv.addObject("institution", entity);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 跳转到班级编辑页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/baseinstitutionEdit.action")
	public ModelAndView baseinstitutionEdit(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		ModelAndView mv = new ModelAndView("baseinstitution/manager/base_institution_edit");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList1(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		List<TagInstitutionEntity> tagList = tagInstitutionService.searchTagInstitutionAll();
        String secondId = entity.getFirstTag();
        TagInstitutionEntity taginfo = tagInstitutionService.searchTagInstitution(secondId);
        entity.setSecondTag(taginfo.getParentId());
		mv.addObject("tagList", tagList);
		mv.addObject("catagoryTags", tags);
		mv.addObject("tagsSecond", tagsSecond);
		mv.addObject("institution", entity);
		mv = getRoleAndNum(mv);
		return mv;
	}
	
	*//**
	 * 跳转到班级编辑页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/baseSchoolinstitutionEdit.action")
	public ModelAndView baseSchoolinstitutionEdit(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		InstitutionEntity institutionEntity = new InstitutionEntity();
		ModelAndView mv = new ModelAndView("baseinstitution/manager/base_school_institution_edit");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList1(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		List<TagInstitutionEntity> tagList = tagInstitutionService.searchTagInstitutionAll();
		List<InstitutionEntity> schoollist=new ArrayList<InstitutionEntity>();
		UserEntity user = WebContext.getSessionUser();
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			if(StringUtil.isNotBlank(entity.getInstitutionParentId())) {
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				schoollist = institutionService.seacherSchoolLocationList(entity);
			} else if("B".equals(role)) {
				entity.setCreateUser(userId);
				schoollist= institutionService.searchEdutionSchoolList(entity);
			}
			
		}else {
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				institutionEntity.setInstitutionName(tpUserEntity.getSchoolName());
				institutionEntity=institutionService.seacherPersonEdution(institutionEntity);
				entity.setInstitutionName(institutionEntity.getInstitutionName());
				schoollist.add(institutionEntity);
			}else if("B".equals(role)) {
				institutionEntity.setCreateUser(userId);
				institutionEntity= institutionService.searchEdution(institutionEntity);
				entity.setInstitutionName(institutionEntity.getInstitutionName());
				schoollist.add(institutionEntity);
			}
		}
		
		mv.addObject("schoollist", schoollist);
		mv.addObject("tagList", tagList);
		mv.addObject("catagoryTags", tags);
		mv.addObject("tagsSecond", tagsSecond);
		mv.addObject("institution", entity);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 分类基本列表
	 * 
	 * @return
	 *//*
	@RequestMapping("/baseinstitutioninit.action")
	public ModelAndView baseinstitutioninit() {
		ModelAndView mv = new ModelAndView("baseinstitutiontaxonomy/manager/base_taxonomy_course_list");
		BaseEntity condition = new BaseEntity();
		condition.setPageSize(5);
		mv.addObject("condition", condition);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 学校列表页面
	 *//*
	@RequestMapping("/schoolListInit.action")
	public ModelAndView schoolListInit(String schoolName) {
		ModelAndView result = new ModelAndView("baseinstitution/manager/school_list");
		String userId= WebContext.getSessionUserId();
		ManagerUserEntity entity= managerUserService.searchByUserId(userId);
		String mangerRole="0";
		if(null!=entity || "2542668".equals(userId)) {
			mangerRole="1";
		}else {
			mangerRole="0";
		}
		result.addObject("schoolName", schoolName);
		result.addObject("mangerRole", mangerRole);
		result = getRoleAndNum(result);
		return result;
	}

	*//**
	 * 检索课程 包括分类
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 * directionTagId 学段
	 * categoryTagId 分类
	 *//*
	@RequestMapping("/schoolList.action")
	@ResponseBody
	public ResultBean schoolList(InstitutionEntity course, String directionTagId, String categoryTagId,
			String courseStage, String sortType, Integer pageSize, Integer pageNumber,String schoolStage) {
		ResultBean result = new ResultBean();
		if(StringUtil.isNotBlank(course.getSchoolName())) {
			course.setInstitutionName(course.getSchoolName());
		}
		//区域
		if(StringUtil.isNotBlank(directionTagId)) {
			course.setFirstTag(directionTagId);
		}
		if(StringUtil.isNotBlank(directionTagId)) {
			TagInstitutionEntity entity = new TagInstitutionEntity();
			entity.setTagId(directionTagId);
			entity = tagInstitutionService.searchFirstData(entity);
			String xdName = entity.getTagName();
			course.setInstitutionType(xdName);
		}
		if(StringUtil.isNotBlank(categoryTagId)) {
			TagInstitutionEntity entity = new TagInstitutionEntity();
			entity.setTagId(categoryTagId);
			entity = tagInstitutionService.searchFirstData(entity);
			String xdName = entity.getTagName();
			course.setFirstTag(categoryTagId);
		}
		//学校阶段
		if(StringUtil.isNotBlank(schoolStage)) {
			course.setSchoolNatrue(schoolStage);
		}
		//权限查看
		String userId = WebContext.getSessionUserId();
		ManagerUserEntity entity= managerUserService.searchByUserId(userId);
		//教育局的人员暂时看到全部
		if(null!=entity || "2542668".equals(userId) || "B".equals(WebContext.getSessionUser().getUserKbn())) {
			result = courseService.schoolList(course, directionTagId, categoryTagId, courseStage, sortType,
					pageSize, pageNumber);
		}else {
			String role = WebContext.getSessionUser().getUserKbn();
			course.setCreateUser(userId);
			if("A".equals(role)) {
				result = courseService.schooTeacherlList(course, directionTagId, categoryTagId, courseStage, sortType,
						pageSize, pageNumber);
			}else if("C".equals(role)) {
				result = courseService.schooStudentlList(course, directionTagId, categoryTagId, courseStage, sortType,
						pageSize, pageNumber);
			}
		}
		
		return result;
	}
	*//**
	 * 日志操作页面 检索列表
	 * 
	 * @param condition
	 * @return 返回结果
	 *//*
	@RequestMapping("/logsearch.action")
	@ResponseBody
	public ResultBean logsearch(UserOperEntity condition) {
		return courseService.search4Page(condition);
	}
	*//**
	 * 已完成
	 * @param finishType
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 *//*
	@RequestMapping("/userCourseFinishList.action")
	@ResponseBody
	public ResultBean userCourseFinishList(String finishType, Integer pageSize, Integer pageNumber) {
		finishType ="3";
		pageSize =5;
		pageNumber=1;
		// 检索我的课程
		ResultBean result = courseService.userCourseList(finishType, pageSize, pageNumber);
		return result;
	}
	*//**
	 * 已完成
	 * @param finishType
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 *//*
	@RequestMapping("/userCourseUnFinishList.action")
	@ResponseBody
	public ResultBean userCourseUnFinishList(String finishType, Integer pageSize, Integer pageNumber) {
		pageSize =5;
		pageNumber=1;
		// 检索我的课程
		ResultBean result = courseService.userCourseUnFinishList(finishType, pageSize, pageNumber);
		return result;
	}
    *//**
                  * 作业列表
     * @return
     *//*
	@RequestMapping("/zuoyeList.action")
	@ResponseBody
	public ResultBean zuoyeList() {
		// 检索我的课程
		ResultBean result = courseService.userZyList();
		return result;
	}
    *//**
     * 
                  * 考试结果查看
     * @param UserMeasurementResultEntity condition 入参
     * @return
     *//*
    @RequestMapping("/userExamDetail.action")
    public ModelAndView userExamDetail(UserMeasurementResultEntity condition) {
    	//考试结果页面
        ModelAndView result = new ModelAndView("course/manager/user_exam_detail");
        //考试结果id是否存在
        if(StringUtil.isEmpty(condition.getMeasurementId())){
            throw new BusinessException("参数错误!");
        }
        condition.setCreateUser(WebContext.getSessionUserId());
        //查询考试结果
        List<UserMeasurementResultEntity> list = measuermentService.searchUserMeasurementResultByEntiy(condition);
        if(list != null && list.size() != 0) {
            result.addObject("condition", list.get(0));
        } else {
            result.addObject("condition", new UserMeasurementResultEntity());
        }
        //返回考试结果
        return result;
    }
	*//**
	 * 查询全部考试
	 * @param ChapterEntity chapter章节参数
	 * @param pageSize  每页展示多少条数据
	 * @param pageNumber 页码
	 * @return
	 *//*
	@RequestMapping("/myAllExam.action")
	public ModelAndView myAllExam(ChapterEntity chapter, Integer pageSize, Integer pageNumber) {
		//考试页面内容展示路径
		ModelAndView mv = new ModelAndView("baseinstitution/manager/my_exam_list");
		//查询数据表
		ResultBean result = chapterService.myAllxam(chapter, pageSize, pageNumber);
		mv.addObject("chapter", result.getData());
		return mv;
	}
	*//**
	 *检索考试
	 * @param chapter
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 *//*
	@RequestMapping("/myAllExamList.action")
	@ResponseBody
	public ResultBean myAllExamList(ChapterEntity chapter, Integer pageSize, Integer pageNumber) {
		ResultBean result = chapterService.myAllxam(chapter, pageSize, pageNumber);
		return result;
	}
	*//**
	 * 查询课工厂方法
	 * @param courseId 课程id
	 * @param content 课程内容
	 * @param type 课程类型
	 * @return
	 *//*
	@RequestMapping("/factory.action")
    @ResponseBody
    public ResultBean factory(String courseId,String content,String type)  {
        ResultBean result = new ResultBean();
        result = courseService.factory(courseId,content,type);
        return result;
    }
	*//**
	 * 用户作业列表
	 * @return
	 *//*
	@RequestMapping("/userHomeWork.action")
	public ModelAndView userHomeWork() {
		//作业列表路径
		ModelAndView result = new ModelAndView("/course/client/user_home_work");
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 查询作业列表
	 * @return
	 *//*
	@RequestMapping("/userHomeWorkSearch.action")
	@ResponseBody
	public ResultBean search(){
		//返回结果
		return courseService.searchUserHomeWork4Page();
	}
	*//**
	 * 查询学校信息
	 * @return
	 *//*
	@RequestMapping("/selectSchoolNameInfo.action")
	@ResponseBody
	public ResultBean selectSchoolNameInfo() {
		ResultBean result = new ResultBean();
		//获取登录信息
		String userId = WebContext.getSessionUserId();
		//获取角色信息
		String role = WebContext.getSessionUser().getUserKbn();
		//判断角色
		if("C".equals(role)) {
			//学生角色
			StudentClassInfoEntity entity =new StudentClassInfoEntity();
			entity.setStudentId(userId);
			List<StudentClassInfoEntity> stuList = studentClassInfoService.searchStudentClassInfoList(entity);
			stuList.get(0).setUserId(userId);
			//设置返回值数据
			result.setData(stuList.get(0));
		}else {
			//教师信息
			TpUserEntity tpUserEntity = userManagementService.searchById(userId);
			result.setData(tpUserEntity);
		}
		 
		return result;
	}
	*//**
	 * 结束课程方法
	 * @param id 课程id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/endCourse.action")
	@ResponseBody
	public ResultBean endCourse(String id) throws Exception {
		courseService.endCourse(id);
		return ResultBean.success(MSG_S_SUBMIT);
	}
	*//**
	 * 图书馆课程复制
	 * @param id 课程id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/toLibCourse.action")
	@ResponseBody
	public ResultBean toLibCourse(String id) throws Exception {
		courseService.toLibCourse(id);
		return ResultBean.success(MSG_S_SUBMIT);
	}
	*//**
	 * 课程复制方法
	 * @param id 课程主键
	 * @return
	 *//*
	@RequestMapping("/copyInfo.action")
    @ResponseBody
    public ResultBean copyInfo(String id)  {
        ResultBean result = new ResultBean();
        //课程复制实现方法
        courseService.copyCourseByHtml(id);
        result.setMessages(getMessage(MSG_S_SAVE));
        return result;
    }
    *//**
                 * 新增课程活动记录方法
     * @param courseId 课程id
     * @return
     *//*
	@RequestMapping("/addActiveInit.action")
	public ModelAndView addActiveInit(String courseId) {
	//课程活动新增页面路径
	ModelAndView result = new ModelAndView("course/manager/course_active_add");
	TagView tagQuery = new TagView();
	//一级分类对象
	List<TagView> tags = courseService.searchTagList2(tagQuery);
	//二级分类对象
	List<TagView> tagsSecond = courseService.searchTagSecondList2(tagQuery);
	//查询章节信息
	ChapterEntity entity = new ChapterEntity();
	entity.setCourseId(courseId);
	List<ChapterEntity> chapterList = courseService.selectByCourseId(entity);
	//返回前台对象
	result.addObject("catagoryTags", chapterList);
	//result.addObject("catagoryTags", tags);
	result.addObject("pageType", "add");
	result.addObject("course", new CourseManagerView());
	result.addObject("tagsSecond", tagsSecond);
	result = getRoleAndNum(result);
	return result;
	}
    *//**
                  * 保存课程章节活动记录信息
     * @param courseJson 课程信息
     * @return 返回结果
     * @throws Exception 
     *//*
    @RequestMapping("/saveActiveChapterInfo.action")
    @ResponseBody
    public ResultBean saveActiveChapterInfo(String courseJson) throws Exception {
    	//实例化对象
        CourseManagerView courseview = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, CourseManagerView.class);
    	ChapterActiveEntity course = new ChapterActiveEntity();
    	course.setChapterId(courseview.getChapterId());
    	course.setActiveFlag(courseview.getActiveFlag());
    	//根据章节id查询章节信息
    	ChapterEntity view= new ChapterEntity();
    	String chapterId = courseview.getChapterId();
    	view = chapterService.selectChapteById(chapterId);
    	//设置章节字段进行保存
    	course.setChapterTitle(view.getTitle());
    	course.setContent(courseview.getContent());
        ResultBean result = ResultBean.success();
        //保存章节活动内容
        chapterActiveService.insertInfo(course);
        return result;
    }
   *//**
             * 查询课程章节阶段活动记录信息
    * @param activeFlag
    * @param chapterId
    * @return
    *//*
	@RequestMapping("/searchActiveChapterInfo.action")
	@ResponseBody
	public ResultBean searchActiveChapterInfo(String activeFlag,String chapterId){
	ChapterActiveEntity course = new ChapterActiveEntity();
	//判断活动记录标识是否为空
	if(StringUtil.isNotBlank(activeFlag)) {
		course.setActiveFlag(activeFlag);
	}
	course.setChapterId(chapterId);
	//查询该章节的全部活动记录
	List<ChapterActiveEntity> view = chapterActiveService.searchList(course);
	ResultBean result = ResultBean.success();
	result.setData(view);
	return result;
	}
	*//**
	 * 查询课程阶段活动记录信息
	 * @param activeFlag 活动记录阶段标识
	 * @param courseId  课程主键
	 * @param chapterId  章节主键
	 * @return
	 *//*
	@RequestMapping("/searchActiveAllChapterInfo.action")
	@ResponseBody
	public ResultBean searchActiveAllChapterInfo(String activeFlag,String courseId,String chapterId){
	ChapterActiveEntity course = new ChapterActiveEntity();
	//判断活动记录标识是否为空
	if(StringUtil.isNotBlank(activeFlag)) {
		course.setActiveFlag(activeFlag);
	}
	course.setCourseId(courseId);
	if(StringUtil.isNotBlank(chapterId)) {
		course.setChapterId(chapterId);
	}
	//返回查询结果
	return chapterActiveService.searchAllCourseChapterPage(course);
	}
	*//**
	 * 查询角色的公共方法
	 * @param result
	 * @return
	 *//*
	private ModelAndView getRoleAndNum(ModelAndView result) {
    	int num = courseService.getStuStatus();
    	result.addObject("num", num);
    	UserEntity user = WebContext.getSessionUser();
    	//判断是否管理员
    	ManagerUserEntity entity = managerUserService.searchByUserId(user.getUserId());
    	if(!user.getUserName().equals("guest")) {
    		List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
    		//老师和教育局角色
    		if(roleList.size() > 0 && "A".equals(user.getUserKbn()) && null==entity && !"2542668".equals(user.getUserId())) {
    			for(UserRoleEntity role : roleList) {
    				if(role.getRoleId().equals("01")) {
    					result.addObject("role", "t");
    				}
    			}
    		//教育局角色	
    		}else if(roleList.size() > 0  && (null!=entity || "2542668".equals(user.getUserId()))){
    			result.addObject("role", "m");
    		}else if("B".equals(user.getUserKbn())){
    			result.addObject("role", "e");
    	    //家长和教研员角色
    		}else if("D".equals(user.getUserKbn()) || "E".equals(user.getUserKbn())){
    			result.addObject("role", "q");
    		//学生角色
    		}else {
    			result.addObject("role", "s");
    		}
    	}
        //管理员角色
    	if(null!=entity || "2542668".equals(user.getUserId())) {
    		result.addObject("manager", "1");
    	}else {
    		result.addObject("manager", "0");
    	}
    	return result;
    }
    *//**
     * 活动记录编辑页面方法
     * @param courseId  课程id
     * @param activeId  活动id
     * @return
     *//*
	@RequestMapping("/editActiveInit.action")
	public ModelAndView editActiveInit(String courseId,String activeId) {
	//活动编辑页面路径
	ModelAndView result = new ModelAndView("course/manager/course_active_editor");
	TagView tagQuery = new TagView();
	//查询一级分类方法
	List<TagView> tags = courseService.searchTagList2(tagQuery);
	//查询e二级分类方法
	List<TagView> tagsSecond = courseService.searchTagSecondList2(tagQuery);
	//根据课程id查询所有对应的章节信息
	ChapterEntity entity = new ChapterEntity();
	entity.setCourseId(courseId);
	List<ChapterEntity> chapterList = courseService.selectByCourseId(entity);
	//根据活动记录id查询活动章节信息
	ChapterActiveEntity active = new ChapterActiveEntity();
	active.setId(activeId);
	List<ChapterActiveEntity> activeList = chapterActiveService.searchList(active);
	//设置返回前台的字段信息
	CourseManagerView courseManagerView = new  CourseManagerView();
	courseManagerView.setChapterId(activeList.get(0).getChapterId());
	courseManagerView.setChapterTitle(activeList.get(0).getChapterTitle());
	courseManagerView.setActiveFlag(activeList.get(0).getActiveFlag());
	courseManagerView.setContent(activeList.get(0).getContent());
	//章节对象
	result.addObject("catagoryTags", chapterList);
	//result.addObject("catagoryTags", tags);
	result.addObject("pageType", "add");
	result.addObject("activeId", activeId);
	//课程对象
	result.addObject("course",courseManagerView);
	result.addObject("tagsSecond", tagsSecond);
	result = getRoleAndNum(result);
	return result;
	}
    *//**
                * 保存活动记录课程章节信息
	* @param courseJson 课程信息
	* @return 返回结果
	* @throws Exception 
	*//*
	@RequestMapping("/updateActiveChapterInfo.action")
	@ResponseBody
	public ResultBean updateActiveChapterInfo(String courseJson) throws Exception {
    //转化实例对象方法
	CourseManagerView courseview = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, CourseManagerView.class);
	ChapterActiveEntity course = new ChapterActiveEntity();
	//删除当前活动记录
	ChapterActiveEntity entity = new ChapterActiveEntity();
	String id = courseview.getActiveId();
	entity.setId(id);
	chapterActiveService.deleteInfo(entity);
	//设置保存课程的信息字段
	course.setChapterId(courseview.getChapterId());
	course.setActiveFlag(courseview.getActiveFlag());
	//查询章节信息
	ChapterEntity view= new ChapterEntity();
	String chapterId = courseview.getChapterId();
	view = chapterService.selectChapteById(chapterId);
	course.setChapterTitle(view.getTitle());
	course.setContent(courseview.getContent());
	ResultBean result = ResultBean.success();
	//保存活动记录的课程章节信息
	chapterActiveService.insertInfo(course);
	return result;
	}
    *//**
                 * 删除活动
	* @param courseJson 课程信息
	* @return 返回结果
	* @throws Exception 
	*//*
	@RequestMapping("/activeDelete.action")
	@ResponseBody
	public ResultBean activeDelete(String id) throws Exception {
	ChapterActiveEntity entity = new ChapterActiveEntity();
	//设置活动记录主键
	entity.setId(id);
	chapterActiveService.deleteInfo(entity);
	ResultBean result = ResultBean.success();
	return result;
	}
	*//**
	 * 添加页面初始化：节
	 * 
	 * @param courseId 课程ID
	 * @return
	 *//*
	@RequestMapping("/courseSmallInit.action")
	public ModelAndView courseSmallInit(String courseId, String parentId) {
		ModelAndView result = new ModelAndView("course/manager/course_three_edit");
		ChapterView chapter = new ChapterView();
		chapter.setMeasureType("4");
		result.addObject("courseId", courseId);
		result.addObject("parentId", parentId);
		result.addObject("chapterFlag", SMALL_CHAPTER);
		result.addObject("chapter", chapter);
		return result;
	}
	*//**
	 * 保存章信息
	 * 
	 * @param chapterJson
	 * @return 返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/saveExamInfo.action")
	@ResponseBody
	public ResultBean saveExamInfo(String chapterJson, String videoLength) throws Exception {
		//将字符串转化成章节对象
		ChapterView chapter = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateMinuteFormatDefualt()),
				chapterJson, ChapterView.class);
		ResultBean result = ResultBean.success();
		//设置章节状态0未发布，1已发布
		chapter.setStatus("1");
		//设置加入课程1是已加入
		chapter.setIsCourse("1");
		if (StringUtil.isEmpty(chapter.getId())) {
			//未存在的章节考试直接保存
			chapterService.insertChapterInfoAll(chapter, videoLength);
		} else {
			//已存在的章节考试更新信息
			chapterService.updateChapterAll(chapter, videoLength);
		}
		//返回信息
		return result;
	}
    *//**
     * 
                  * 查看考试结果
     * @param condition
     * @return
     *//*
    @SuppressWarnings({ "unchecked"})
    @RequestMapping("/userExamInfoInit.action")
    public ModelAndView userExamInfoInit(MeasurementEntity condition) {
        ModelAndView result = new ModelAndView("course/manager/user_exam_info");
        //参数为空抛出异常
        if(StringUtil.isEmpty(condition.getLinkId())){
            throw new BusinessException("参数错误!");
        }
        //根据参数查询考试结果
        List<MeasurementEntity> list = courseService.search(condition);
        if(list != null && list.size() != 0) {
            result.addObject("condition", list.get(0));
        } else {
            throw new BusinessException("参数错误!");
        }
        //返回考试信息
        return result;
    }
    *//**
                 * 根据课程id获取章节考试状态信息
     * @param courseId
     * @return
     * @throws Exception
     *//*
	@RequestMapping("/searchExamStatus.action")
	@ResponseBody
	public ChapterView searchExamStatus(String courseId) throws Exception {
		//查询章节信息
		ChapterView view = chapterService.seacrhExamStatus(courseId);
		return view;
	}
	@SuppressWarnings("unchecked")
	*//**
	* 前端请求-章节详情
	* @param chapterId 章节ID
	* @return 
	*//*
	@RequestMapping("/toChapterDetail.action")
	public ModelAndView toChapterDetail(String courseId,String chapterId){
		ModelAndView mv = new ModelAndView("course/client/course_chapter_detail");
		ResultBean result = courseService.searchCourseDetail(courseId);
		CourseView courseView = (CourseView) result.getData();
		mv.addObject("courseView", courseView);
		mv.addObject("note", new NoteDataEntity());
		mv.addObject("homeWork", new HomeworkView());
		result = chapterService.chapterDetail(courseId, chapterId);
		ChapterView chapterView = (ChapterView) result.getData();
		ChapterEntity entity = new ChapterEntity();
		entity.setCourseId(courseId);
		List<ChapterEntity> chapterCourseList = courseService.selectByCourseId(entity);
		mv.addObject("chapterCourseList", chapterCourseList);
		mv.addObject("chapterView", chapterView);
		//检索章节列表
		List<ChapterView> cvs = (List<ChapterView>) chapterService.chapterList(courseId);
		mv.addObject("chapterList", cvs);
		//相关课程
		result = courseService.recommendedCourses("5", courseId);
		List<CourseEntity> otherCourseList = (List<CourseEntity>) result.getData();
		mv.addObject("otherCourseList", otherCourseList);
		//判断是否需要检索测试信息
		if(CHAPTER_TYPE_MEASUERMENT.equals(chapterView.getMeasureType().toString())){
			result = measuermentService.searchMeasuerment(chapterId, LINK_TYPE_CHAPTER);
			List<QuestionEntity> questionList = (List<QuestionEntity>) result.getData();
			mv.addObject("questionList", questionList);
		}
		//判断是否考试章节
		if(CHAPTER_TYPE_EXAM.equals(chapterView.getMeasureType())){
			//考试章节
			//取用户对于章节的考试信息
			MeasurementEntity me = new MeasurementEntity();
			me.setLinkId(chapterId);
			me.setLinkType(LINK_TYPE_CHAPTER);
			me = measuermentService.findMeasurement(me);
			if(me!=null){
				UserMeasurementResultEntity um = new UserMeasurementResultEntity();
				um.setMeasurementId(me.getId());
				um.setUserId(WebContext.getSessionUserId());
				um = measuermentService.findUserExamResult(um);
				if(um != null && um.getExamStatus()!=null && um.getExamStatus().equals(EXAM_STATUS_END)){
					//已参加过考试提交了
					mv.addObject("goal", um.getGoal()+"");
					mv.addObject("totalGoal", um.getTotalGoal()+"");
					mv.addObject("examTime", DateUtil.formatAny(um.getCreateDate(), ConfigUtil.getDateTimeFormatDefualt()));
				}
			}
		}
		//是否教师
		mv.addObject("isTeacher", courseService.isTeacher(courseView.getTeacherId()));
		mv.addObject("useingChapterId", chapterId);
		String content = chapterView.getContent();
		if(content != null && !"".equals(content)) {
			String ipAndPort = getIpAndPort(content);
			if(!"".equals(ipAndPort)) {
				String serverName = WebContext.getRequest().getServerName();
				int serverPort = WebContext.getRequest().getServerPort();
				content = content.replace(ipAndPort, serverName+":"+serverPort);
				chapterView.setContent(content);
			}
		}
		
		return mv;
	}
	*//**
	 * 判断str中是否有IP和port，若有则取出返回
	 * @param str
	 * @return String
	 *//*
	private static String getIpAndPort(String str) {
		String returnStr = "";
		Pattern p = Pattern.compile("(\\d+\\.\\d+\\.\\d+\\.\\d+)\\:(\\d+)");
		Matcher m = p.matcher(str);
		if(m.find()) {
			returnStr += m.group(1)+":"+m.group(2);
		}
		
		return returnStr;
	}
	*//**
	 * 标签一览
	 * @param flag 是否显示标签 0 不显示 1 显示
	 * @param businessType 业务类型
	 * @return
	 *//*
	@RequestMapping("/searchSchhoolCategory.action")
	@ResponseBody
	public ResultBean searchSchhoolCategory(String flag,String businessType){
		ResultBean  result=tagService.searchSchhoolCategory(flag,businessType);
		return result;
	}
	*//**
	 * 
	 * 初始化待办课程列表
	 * 
	 * @param condition 课程对象
	 * @return 返回课程列表初始化页面
	 *//*
	@RequestMapping("/institutionDbListInit.action")
	public ModelAndView institutionDbListInit(InstitutionMessageEntity condition) {
		//设置代办页面
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_db_list");
		//设置分页参数
		condition.setPageSize(10);
		result.addObject("condition", condition);
		TagInstitutionView tagQuery = new TagInstitutionView();
		//一级标签信息查询
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		//二级标签信息查询
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		//返回课程实例对象
		result.addObject("course", new InstitutionMessageEntity());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}
	*//**
	 * 
	 * 初始化已办课程列表
	 * 
	 * @param condition 课程对象
	 * @return 返回课程列表初始化页面
	 *//*
	@RequestMapping("/institutionYbListInit.action")
	public ModelAndView institutionYbListInit(InstitutionMessageEntity condition) {
		//设置已办页面
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_yb_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		TagInstitutionView tagQuery = new TagInstitutionView();
		//一级标签信息查询
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		//二级标签信息查询
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		//返回课程实例对象
		result.addObject("course", new InstitutionMessageEntity());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}
	*//**
	 * 
	 * 检索评价列表
	 * 
	 * @param queryInfo 机构对象
	 * @return 返回评价列表
	 *//*
	@RequestMapping("/listdbSearch.action")
	@ResponseBody
	public ResultBean listdbSearch(MeasurementInstitutionEntity queryInfo) {
		UserEntity user = WebContext.getSessionUser();	
		//获取人员角色
		String role = user.getUserKbn();
		//获取登录人账号
		String userId = user.getCreateUser();
		//教师角色
		if("A".equals(role)) {
			TpUserEntity tpUserEntity =new TpUserEntity();
			//查询教师信息
			tpUserEntity = userManagementService.searchById(userId);
		    if(null!=tpUserEntity) {
		    	queryInfo.setInstitutionName(tpUserEntity.getSchoolName());
		    }
		    //学生角色
		}else if("C".equals(role)) {
			//查询学生信息
			StudentClassInfoEntity classInfo = new StudentClassInfoEntity();
			classInfo.setStudentId(userId);
			classInfo = studentClassInfoService.searchStudentClassInfo(classInfo);
			if(null!=classInfo && StringUtil.isNotBlank(classInfo.getSchoolName())) {
				queryInfo.setInstitutionName(classInfo.getSchoolName());
			}
		}
		
		return taxonomyInstitutionService.listdbSearch(queryInfo);
	}
	*//**
	 * 
	 * 检索已办评价列表
	 * 
	 * @param queryInfo 机构对象
	 * @return 返回评价列表
	 *//*
	@RequestMapping("/listybSearch.action")
	@ResponseBody
	public ResultBean listybSearch(MeasurementInstitutionEntity queryInfo) {
		UserEntity user = WebContext.getSessionUser();	
		//获取人员角色
		String role = user.getUserKbn();
		//登录账号
		String userId = user.getCreateUser();
		//教师角色
	    if("A".equals(role)) {
	    	//查询用户信息
	    	TpUserEntity tpUserEntity =new TpUserEntity();
			tpUserEntity = userManagementService.searchById(userId);
			if(null!=tpUserEntity) {
				//设置学校名称
				queryInfo.setInstitutionName(tpUserEntity.getSchoolName());
			}
	    }else if("C".equals(role)) {
	    	//查询学生信息
			StudentClassInfoEntity classInfo = new StudentClassInfoEntity();
			classInfo.setStudentId(userId);
			classInfo = studentClassInfoService.searchStudentClassInfo(classInfo);
			if(null!=classInfo && StringUtil.isNotBlank(classInfo.getSchoolName())) {
				//设置学生学校名称
				queryInfo.setInstitutionName(classInfo.getSchoolName());
			}
		}
	    //返回查询结果列表
		return taxonomyInstitutionService.listybSearch(queryInfo);
	}
	*//**
	 * 管理人员基本信息页面
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/managerUserList.action")
	public ModelAndView managerUserList(ManagerUserEntity condition) {
		//管理员信息页面展示路径
		ModelAndView result = new ModelAndView("/baseinstitution/manager/base_manager_user_list");
		//设置每页展示10条数据
		condition.setPageSize(10);
        result.addObject("course", new CourseManagerView());
		result.addObject("condition", condition);
		result = getRoleAndNum(result);
		//返回页面所需信息结果
		return result;
	}
	*//**
	 * 管理者信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchManagerUserList.action")
	@ResponseBody
	public ResultBean searchManagerUserList(ManagerUserEntity entity) {
		//查询管理员数据表全部信息
		return managerUserService.searchPage(entity);
	}
	*//**
	 * 搜素全部人员
	 * 
	 * @param query
	 * @return
	 *//*
	@RequestMapping("/searchUserAll.action")
	@ResponseBody
	public ResultBean searchUserAll(TpUserEntity query) {
		if(null!=query && StringUtil.isNotBlank(query.getUsrName())) {
			query.setUsrName(query.getUsrName().trim());
		}
		//根据用户名查询人员信息表数据
		return userManagementService.searchManagerUserAll(query);
	}
	*//**
	 * 保存超级管理员信息
	 * @param userId 添加人员账号id
	 * @param userName  人员名称
	 * @param personName 名称
	 * @param role 角色
	 * @return
	 *//*
	@RequestMapping("/saveManagerUser.action")
	@ResponseBody
	public ResultBean saveManagerUser(String userId,String userName,String personName,String role) {
		ResultBean result = new ResultBean();
		//查询管理员表中是否已存在该成员
		ManagerUserEntity entity = managerUserService.searchByUserId(userId);
		//存在则返回
		if(null!=entity) {
			result = ResultBean.error();
		}else {
			//不存在则保存到数据表里
			ManagerUserEntity managerUserEntity = new ManagerUserEntity();
			managerUserEntity.setUserId(userId);
			managerUserEntity.setUserName(userName);
			managerUserEntity.setPersonName(personName);
			managerUserEntity.setRole(role);
			managerUserEntity.setCreateDate(new Date());
			//设置该条信息的创建人
			managerUserEntity.setCreateUser(WebContext.getSessionUserId());
			managerUserService.insert(managerUserEntity);
			//保存成功返回信息
			result = ResultBean.success();
		}
		
		return result;
	}
	*//**
	 * 删除管理员
	 * @param id 员工账号
	 * @return  返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/deleteManagerById.action")
	@ResponseBody
	public ResultBean deleteManagerById(String id) throws Exception {
		//根据员工账号在tb_manager_user表里删除其信息
		ManagerUserEntity entity = new ManagerUserEntity();
		entity.setUserId(id);
		managerUserService.delete(entity);
		return ResultBean.success(DELETE_GRADE_SUCCESS);
	}
	*//**
	 * 查询管理信息
	 * 
	 * @param 员工账号
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchByUserId.action")
	@ResponseBody
	public ManagerUserEntity searchByUserId(String id) throws Exception {
		//返回查询结果
		return managerUserService.searchByUserId(id);
	}
	*//**
	 *  作业列表页面
	 * @param homeworkView 入参
	 * @return 返回结果
	 *//*
	@RequestMapping("/zuoyeManager.action")
	public ModelAndView zuoyeManager(HomeworkView homeworkView) {
		//列表页面路径
		ModelAndView result = new ModelAndView("/course/manager/homework_list");
		//返回作业信息对象给前台
		result.addObject("homeworkView", homeworkView);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 *  查询作业列表结果方法
	 * @param homeworkView 前台传入后台参数
	 * @return  返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/zuoyelistSearch.action")
	@ResponseBody
	public ResultBean zuoyelistSearch(HomeworkView homeworkView) throws Exception {
		ResultBean result =new ResultBean();
		//获取登录信息对象
		UserEntity user = WebContext.getSessionUser();
    	//判断是否管理员
    	ManagerUserEntity entity = managerUserService.searchByUserId(user.getUserId());
    	if(null!=entity) {
    		//管理员返回的结果
    		result= courseVRService.seacherHomeworkPage(homeworkView);
    	}else {
    		//普通教师查询返回的结果
    		homeworkView.setTeacherId(user.getUserId());
    		result= courseVRService.seacherHomeworkPage(homeworkView);
    	}
		return result;
	}
	*//**
	 *  根据作业id查询作业列表
	 * @param id 作业主键
	 * @param title  作业标题
	 * @return  返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/zuoyelistById.action")
	public ModelAndView zuoyelistById(String id,String title) throws Exception {
		//作业列表路径
		ModelAndView result = new ModelAndView("/course/manager/homework_user_list");
		HomeworkView homeworkView = new HomeworkView();
		homeworkView.setId(id);
		homeworkView.setTitle(title);
		//作业实例化对象
		result.addObject("homeworkView", homeworkView);
		//作业主键
		result.addObject("id", id);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 预览作业页面方法
	 * @param id  作业主键
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/homeWorkById.action")
	public ModelAndView homeWorkById(String id) throws Exception {
		//预览页面路径
		ModelAndView result = new ModelAndView("/course/client/home_work_teacher_review");
		HomeworkView view = courseService.getHomeWorkById(id);
		//设置创建时间返回前台
		if(null!=view.getCreateDate()) {
			view.setCreateTime(sdf.format(view.getCreateDate()));
		}
        //返回作业实体类对象
		result.addObject("homeWork", view);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 查询作业列表
	 * @param id 作业id集合字符串
	 * @param homeworkView  作业实体类
	 * @param linkId 课程主键
	 * @param linkType  课程/章节类型
	 * @return
	 *//*
	@RequestMapping("/zuoyelistByIdSearch.action")
	@ResponseBody
	public ResultBean zuoyelistByIdSearch(String id,HomeworkView homeworkView,String linkId,String linkType) {
		// 将前台汇总的作业id拆分
		if(StringUtil.isNotBlank(id)) {
			String ids[]=id.split(",");
			homeworkView.setId(ids[0]);
		}else {
			homeworkView.setLinkId(linkId);
			homeworkView.setLinkType(linkType);
			HomeworkEntity homeWork = courseService.getHomeWorkByLinkId(homeworkView);
			if(null!=homeWork) {
				//获取作业主键
				homeworkView.setId(homeWork.getId());
				//获取作业标题
				homeworkView.setTitle(homeWork.getTitle());
			}
		}
		//查询作业信息
		return courseVRService.seacherHomeworkUserPage(homeworkView);
	}
    *//**
                 * 保存作业信息
     * @param courseJson 作业信息
     * @return 返回结果
     * @throws Exception 
     *//*
    @RequestMapping("/saveZyInfo.action")
    @ResponseBody
    public ResultBean saveZyInfo(String courseJson) throws Exception {
    	//将字符串转化成实例化对象
    	HomeworkView course = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, HomeworkView.class);
        ResultBean result = ResultBean.success();
        String homeworkId=course.getId();
        //获取账号id
        String userId = WebContext.getSessionUserId();
        //查询作业数据表
        HomeworkAnswerEntity entity = new  HomeworkAnswerEntity();
        entity.setHomeworkId(homeworkId);
        entity.setCreateUser(userId);
        entity = courseVRService.selectOneAnswer(entity);
        if(null==entity) {
        	//作业作答数据表信息为空，插入数据信息
        	course.setId(IDGenerator.genUID());
        	course.setCreateUser(userId);
        	course.setCreateDate(new Date());
        	//设置学生提交的信息
        	course.setStudentComment(course.getContent());
        	course.setHomeworkId(homeworkId);
        	course.setAnswerUser(userId);
        	//设置作业状态
        	course.setStatus(HOMEWORK_ANSWER_STATUS_ONE);
        	courseVRService.insertAnswer(course);
        }else {
        	//作业作答数据表信息不为空，更新数据信息
        	entity.setStudentComment(course.getContent());
        	entity.setUpdateDate(new Date());
        	entity.setUpdateUser(userId);
        	courseVRService.update(entity);
        }
        return result;
    }
	*//**
	 * 学生作业列表页面
	 * @param HomeworkView homeworkView 
	 * @return
	 *//*
	@RequestMapping("/studentZuoyePage.action")
	public ModelAndView studentZuoyePage(HomeworkView homeworkView) {
		//学生作业列表页面路径
		ModelAndView result = new ModelAndView("/course/manager/homework_student_list");
		//作业实例对象实体类
		result.addObject("homeworkView", homeworkView);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 查询作业列表
	 * 
	 * @param HomeworkView homeworkView 入参
	 * @return 返回结果
	 *//*
	@RequestMapping("/studentZuoyelistSearch.action")
	@ResponseBody
	public ResultBean studentZuoyelistSearch(HomeworkView homeworkView) {
		homeworkView.setCreateUser(WebContext.getSessionUserId());
		//根据账号id获取作业对象信息
		return courseVRService.seacherStudentHomeworkUserPage(homeworkView);
	}
    *//**
                   * 编辑作业
     * @param HomeworkView home 作业实例对象入参 
     * @return
     *//*
    @RequestMapping("/studentTaskInit.action")
    public ModelAndView studentTaskInit(HomeworkView home){
    	//作业编辑页面
        ModelAndView result = new ModelAndView("course/manager/home_student_work");
        //获取当前登录者账号
        UserEntity user = WebContext.getSessionUser();
        home.setCreateUser(user.getUserId());
        HomeworkView homework=courseVRService.searchStudentHomeWork(home);
        //前台页面信息回显对象
        result.addObject("homework", homework);
        result = getRoleAndNum(result);
        return result;
    }
    *//**
                  * 保存作业信息
     * @param id  作业id
     * @param studentComment 学生提交内容
     * @param homeanwserId 作业作答id
     * @param status 提交作业状态
     * @return
     * @throws Exception
     *//*
    @RequestMapping("/saveMystudy.action")
    @ResponseBody
    public ResultBean saveMystudy(String id,String studentComment,String homeanwserId,String status) throws Exception {
        ResultBean result = ResultBean.success();
        String userId = WebContext.getSessionUserId();
        //根据homeanwserId判断当前登陆者是否已经作答过改作业
        if(StringUtil.isBlank(homeanwserId)) {
        	//未作答，保存作答内容
        	HomeworkView course = new HomeworkView();
        	//随机生成作业作答id
        	course.setId(IDGenerator.genUID());
        	course.setCreateUser(userId);
        	course.setCreateDate(new Date());
        	//作业作答内容
        	course.setStudentComment(studentComment);
        	course.setHomeworkId(id);
        	course.setAnswerUser(userId);
        	//设置作业状态
        	course.setStatus(HOMEWORK_ANSWER_STATUS_ONE);
        	courseVRService.insertAnswer(course);
        }else {
        	//更新做大内容
        	HomeworkAnswerEntity entity = new HomeworkAnswerEntity();
        	entity.setId(homeanwserId);
        	entity = courseVRService.selectOneAnswer(entity);
        	entity.setStudentComment(studentComment);
        	entity.setUpdateDate(new Date());
        	entity.setUpdateUser(userId);
        	courseVRService.update(entity);
        }
        return result;
    }
    *//**
                 * 学生查看作业列表方法
     * @param home
     * @return
     *//*
    @RequestMapping("/studentSeacherTaskInit.action")
    public ModelAndView studentSeacherTaskInit(HomeworkView home){
    	//学生作业列表页面路径
        ModelAndView result = new ModelAndView("course/manager/home_student_look_work");
        //根据登录id查询作业信息
        UserEntity user = WebContext.getSessionUser();
        home.setCreateUser(user.getUserId());
        HomeworkView homework=courseVRService.searchStudentHomeWork(home);
        //作业信息返回给前台
        result.addObject("homework", homework);
        result = getRoleAndNum(result);
        return result;
    }
    *//**
                  * 查询年级 课程
     * @param id 课程id
     * @return
     *//*
    @RequestMapping("/searchGrade.action")
    @ResponseBody
    public List<StudentClassInfoEntity> searchGrade(String id){
    	//根据课程id查询课程信息
        CourseEntity entity = new CourseEntity();
        entity.setId(id);
        entity = courseService.searchCourse(entity);
        //获取老师所属学校
        String schoolId = entity.getSchoolId();
        StudentClassInfoEntity studentClassInfoEntity = new StudentClassInfoEntity();
        studentClassInfoEntity.setSchoolId(schoolId);
        //根据学校查询年级
        List<StudentClassInfoEntity> gradeList = new ArrayList<StudentClassInfoEntity>();
        gradeList = studentClassInfoService.searchStudentGradeList(studentClassInfoEntity);
        //返回查询的年级集合给前台
        return gradeList;
    }
     *//**
                     * 根据课程主键和年级主键查询班级信息
      * @param courseId  课程主键
      * @param gradeId 年级主键
      * @return
      *//*
    @RequestMapping("/searchClass.action")
    @ResponseBody
    public List<StudentClassInfoEntity> searchClass(String courseId,String gradeId){
    	//根据课程主键查询课程信息
    	 CourseEntity entity = new CourseEntity();
         entity.setId(courseId);
         entity = courseService.searchCourse(entity);
         //获取老师所属学校
         String schoolId = entity.getSchoolId();
        List<StudentClassInfoEntity> gradeList = new ArrayList<StudentClassInfoEntity>();
        //根据学校主键和年级查询班级信息
        StudentClassInfoEntity studentClassInfoEntity = new StudentClassInfoEntity();
        studentClassInfoEntity.setSchoolId(schoolId);//学校id
        studentClassInfoEntity.setGradeId(gradeId);//年级id
        gradeList = studentClassInfoService.searchStudentClassList(studentClassInfoEntity);
        return gradeList;
    }
    *//**
                  * 添加学生时查询学生列表
     * @param UserView query 用户参数 
     * @param courseId 所属课程信息id
     * @return
     *//*
    @RequestMapping("/searchStudentUserAll.action")
    @ResponseBody
    public ResultBean searchStudentUserAll(UserView query,String courseId)  {
    	//根据课程id查询课程所属老师对应的学校信息
    	CourseEntity entity = new CourseEntity();
        entity.setId(courseId);
        entity = courseService.searchCourse(entity);
        //获取老师所属学校
        String schoolId = entity.getSchoolId();
        //根据学校id获取这个学校下面所有的学生信息
        query.setSchoolId(schoolId);
        return courseService.searchStudentUserAll(query);
    }
	*//**
	 * 用户个人中心信息页面方法
	 * @return 返回结果
	 *//*
	@RequestMapping("/userPblInfo.action")
	public ModelAndView userPblInfo(){
		//查询代办信息表里审核通过的代办信息数量
		int num = courseService.getStuStatus();
		//个人中心页面路径参数
		ModelAndView mv = new ModelAndView("trainingplatform/client/center/center_pbl_user");
		//根据用户id查询用户信息
		UserEntity user = userManagementService.seachUserInfo(WebContext.getSessionUserId());
		//返回用户对象
		mv.addObject("user", user);
		mv.addObject("num", num);
		//获取登录人员的角色信息
		UserEntity userRole = WebContext.getSessionUser();
		//判断用户是否登录状态guest为未登录状态
		if(!userRole.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(userRole.getUserId());
			if(roleList.size() > 0) {
				//循环遍历人员角色
				for(UserRoleEntity role : roleList) {
					//角色包含01的设置role字段为t,即老师角色
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				//s为学生角色
				mv.addObject("role", "s");
			}
		}
		UserEntity user1 = WebContext.getSessionUser();
		//获取工号
		String uid =user1.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		    //获取人员信息
		        //设置头像字段
				String image = "";
				//设置头像字段
				String school="";
				//设置年级字段
				String grade="";
				//设置班级字段
				String className="";
				if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
					//获取头像
					image = userEntity.getHeadurl();
					//获取学校
					school = userEntity.getSchoolName();
				}else {
					//设置默认头像
					image ="http://wisdomsaas-img.oss-cn-beijing.aliyuncs.com/BFAA0FECDD194B7A974C60B7E1B27C18?authorization=bce-auth-v1%2F7642b4b48046452697e90662eed41ace%2F2017-12-29T08%3A33%3A39Z%2F-1%2F%2F0f06024af14dad05694f25b8c85572567591c72bbd0129d3175b4cdbd6e62208";
				}
				//获取班级
				if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
					//实例化学生对象实体类并查询学生信息
					List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
					StudentClassInfoEntity entity = new StudentClassInfoEntity();
					entity.setStudentId(uid);
					entityList = studentClassInfoService.searchStudentClassInfoList(entity);
					//如果学生对象不为空则执行如下赋值
					if(null!=entityList && entityList.size()>0) {
						grade = entityList.get(0).getGradeName();
						className = entityList.get(0).getClassName();
						school =  entityList.get(0).getSchoolName();
					}
				}
				//返回班级信息
				mv.addObject("className", className);
				//向页面返回年级信息
				mv.addObject("grade", grade);
				//返回学生所属学校名称信息
				mv.addObject("school", school);
		       return mv;
	}
	*//**
	 * 批量点评初始化页面方法
	 * @param userIds  学生id集合字符串
	 * @param personNames  学生姓名字符集合
	 * @param id      作业id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/allEvalutionMsg.action")
	public ModelAndView allEvalutionMsg(String userIds,String personNames,String id) throws Exception {
		//批量点评方法
		ModelAndView result = new ModelAndView("/course/client/home_work_teacher_all_review");
		String ids[]=id.split(",");
		HomeworkView view = courseService.getHomeWorkById(ids[0]);
		//判断作业对象返回的时间是否为空，不为空则赋值给新的字段用于前端展示
		if(null!=view.getCreateDate()) {
			if(StringUtil.isNotBlank(sdf.format(view.getCreateDate()))) {
				view.setCreateTime(sdf.format(view.getCreateDate()));
			}
		}
		//收集作业对象
		result.addObject("homeWork", view);
		//收集作业id
		result.addObject("id", id);
		//收集学生账号集合id
		result.addObject("userIds", userIds);
		//收集学生姓名集合
		result.addObject("personNames", personNames);
		//收集作业答案id
		result.addObject("answerIds", id);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 批量点评保存
	 * @param courseJson 前台返回的json字符串
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveAllHomeWorkReviewById.action")
	@ResponseBody
	public ResultBean saveAllHomeWorkReviewById(String courseJson) throws Exception {
		//将字符串转化成实例化对象
		HomeworkView homework = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, HomeworkView.class);
		return courseService.saveAllHomeWorkReviewById(homework);
	}
	*//**
	 * 一键催交
	 * @param userIds  学生id集合字符串参数
	 * @param courseId 课程id
	 * @param homeworkId  作业id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveCjById.action")
	@ResponseBody
	public ResultBean saveCjById(String userIds,String courseId,String homeworkId) throws Exception {
		return courseService.saveACjById(userIds,courseId,homeworkId);
	}
	*//**
	 * 课程考试调用java调查网接口
	 * @param courseId  课程id
	 * @param userId  用户id
	 * @return
	 * @throws ParseException
	 *//*
	@RequestMapping("/searchExamByCourseIdList.action")
	@ResponseBody
	public ResultBean searchExamByCourseIdList(String courseId,String userId) throws ParseException {
		ResultBean result = new ResultBean();
		String param = courseId;
		//声明调用java调查网的webservice接口
		ExamToWebserviceProxy proxy = new ExamToWebserviceProxy();
		String out;
		try {
			//调用外部接口方法
			out = proxy.examList(param,1,20,userId);
			JSONArray jsonArray = new JSONArray(); 
			//将接口返回值转化成数组格式
			jsonArray = jsonArray.fromObject(out);
			UserCourseEntity uc = new UserCourseEntity();
			uc.setCourseId(courseId);
			uc.setUserId(WebContext.getSessionUserId());
			// 检索用户是否存在
			UserCourseEntity uu = courseService.searchOneCourseIdData(uc);
			//根据课程id获取该课程的老师id
			CourseEntity courseEntity = courseService.getCourseId(courseId);
			//转化对应的实体类
			@SuppressWarnings("unchecked")
			List<Questionnaire> list = jsonArray.toList(jsonArray, Questionnaire.class);
			if(null!=list && list.size()>0) {
				//循环遍历集合将字段赋值
				for(int i=0;i<list.size();i++) {
					//已加入的学员或者这个课程的老师创建者都可以看到这个
					if(null!=uu ||(null!=courseEntity && WebContext.getSessionUserId().equals(courseEntity.getTeacherId()))) {
						//已加入课程
						list.get(i).setStudentFlag("0");
						if (list.get(i).getEndDate() != null ) {
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Long end = sdf.parse(list.get(i).getEndDateStr()).getTime();
							Long nowtime = new Date().getTime();
							//判断问卷/考试是否已结束
								if(end-nowtime<0) {
									list.get(i).setExamStatusText("问卷/考试已结束");
									list.get(i).setExamStatus("1");	
								}					
							}
						if (list.get(i).getStartDate() != null ) {
							//将字符串转化成时间再获取秒数
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Long start = sdf.parse(list.get(i).getStartDateStr()).getTime();
							Long nowtime = new Date().getTime();
							//判断问卷/考试是否开始
								if(start-nowtime>0) {
									list.get(i).setExamStatusText("问卷/考试未开始");
									list.get(i).setExamStatus("1");
								}
							}
					}else {
						list.get(i).setStudentFlag("1");//未加入课程
						list.get(i).setExamStatusText("未加入课程");
					}
					
				}
			}
			 result.setData(list);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
		@Autowired
	private CourseService courseService;
	@Autowired
	private ChapterService chapterService;
	@Autowired
	private UserService userService;
	@Autowired
	private UserManagementService userManagementService;
	@Autowired
	private TaxonomyInstitutionService taxonomyInstitutionService;
	@Autowired
	private TagInstitutionEvalutionService tagInstitutionEvalutionService;
	@Autowired
	private InstitutionService institutionService;
	@Autowired
	private TagInstitutionService tagInstitutionService;
	@Autowired
	private MeasuermentInstitutionService measurementInstitutionService;
	@Autowired
	private QuestionInstitutionService questionInstitutionService;
	@Autowired
	private MeasuermentService measuermentService;
	@Autowired
	private StudentClassInfoService studentClassInfoService;
	@Autowired
	private TeacherClassInfoService teacherClassInfoService;
	@Autowired
	private ChapterActiveService chapterActiveService;
	@Autowired
	private TagService tagService;
	@Autowired
	private ManagerUserService managerUserService;
	@Autowired
	private CourseVRService courseVRService;
	*//**
	 * 用户收藏课程
	 * 
	 * @author liuzhen
	 * @param courseId
	 * @return 返回结果
	 *//*
	@RequestMapping("/collectCourse.action")
	@ResponseBody
	public ResultBean collectCourse(String courseId) {
		return courseService.collectCourse(courseId);
	}

	*//**
	 * 用户取消收藏
	 * 
	 * @param courseId 课程Id
	 * @return
	 *//*
	@RequestMapping("/cancelCollectCourse.action")
	@ResponseBody
	public ResultBean cancelCollectCourse(String courseId) {
		return courseService.cancelCollectCourse(courseId);
	}

	*//**
	 * 二级标签查询
	 * 
	 * @param id 课程ID
	 * @return 课程下的二级标签列表
	 *//*
	@RequestMapping("/searchSecondTag2.action")
	@ResponseBody
	public List<TagView> searchSecondTag2(String id) {
		return courseService.searchSecondTagList2(id);
	}
	*//**
	 * 学校评价二级标签查询
	 * 
	 * @param id 课程ID
	 * @return 课程下的二级标签列表
	 *//*
	@RequestMapping("/searchInsitutionSecondTag2.action")
	@ResponseBody
	public List<TagInstitutionView> searchInsitutionSecondTag2(String id) {
		return courseService.searchInstitutionSecondTagList2(id);
	}
	@RequestMapping("/searchSecondTag.action")
	@ResponseBody
	public List<TagView> searchSecondTag(String id) {
		return courseService.searchSecondTagList(id);
	}

	*//**
	 * 体验课程/加入课程操作
	 * 
	 * @param courseId 课程ID
	 * @return 是否加入课程成功
	 * @author WUXIAOXIANG
	 *//*
	@RequestMapping("/joinCourse.action")
	@ResponseBody
	public ResultBean joinCourse(String courseId) {
		return courseService.joinCourse(courseId);
	}

	@RequestMapping("/applyJoinCourse.action")
	@ResponseBody
	public ResultBean applyJoinCourse(String courseId) {
		return courseService.applyJoinCourse(courseId);
	}
	
	*//**
	 * 课程推荐
	 * 
	 * @author FZ
	 * @return 返回结果
	 *//*
	@RequestMapping("/recommendedCourses.action")
	@ResponseBody
	public ResultBean recommendedCourses(String courseId, String displayNum) {

		return courseService.recommendedCourses(displayNum, courseId);
	}

	*//**
	 * 课程评价
	 * 
	 * @param linkId linkType
	 * @return 返回结果
	 * @author dengzhihao
	 *//*
	@RequestMapping("/courseEvaluationList.action")
	@ResponseBody
	public ResultBean courseEvaluationList(ScoreEntity scoreEntity) {

		return courseService.courseEvaluationList(scoreEntity);
	}

	*//**
	 * 对课程评价
	 * 
	 * @param scoreEntity     评价主题entity
	 * @param scoreDataEntity 评价数据entity
	 * @return 是否评价成功
	 * @author WUXIAOXIANG
	 *//*
	@RequestMapping("/evaluateCourse.action")
	@ResponseBody
	public ResultBean evaluateCourse(ScoreEntity scoreEntity, ScoreDataEntity scoreDataEntity) {
		// 插入评价信息
		return courseService.insertEvaluateCourse(scoreEntity, scoreDataEntity);
	}

	*//**
	 * 检索课程 包括分类
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 *//*
	@RequestMapping("/courseList.action")
	@ResponseBody
	public ResultBean courseList(CourseManagerView course, String directionTagId, String categoryTagId,
			String courseStage, String sortType, Integer pageSize, Integer pageNumber,String schoolName,String schoolNatrul,String tagsId) {
		if(StringUtil.isNotBlank(tagsId)) {
			course.setTagsId(tagsId);
		}
		if(StringUtil.isNotBlank(schoolNatrul)) {
			course.setSchoolNatrul(schoolNatrul);
		}
		ResultBean result = courseService.courseList(course, directionTagId, categoryTagId, courseStage, sortType,
				pageSize, pageNumber,schoolName);
		return result;
	}

	@RequestMapping("/coursePublicList.action")
	@ResponseBody
	public ResultBean coursePublicList(CourseManagerView course, Integer pageSize, Integer pageNumber) {
		course.setShareFlag(COURSE_SHARE_FLAG_THREE);
		course.setEndFlag(COURSE_END_STATUS_YES);
		UserEntity user = WebContext.getSessionUser();
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		//获取学校名称信息
		String school="";
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
			//获取学校
			school = userEntity.getSchoolName();
		}
		//获取班级
		if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
			List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
			StudentClassInfoEntity entity = new StudentClassInfoEntity();
			entity.setStudentId(uid);
			entityList = studentClassInfoService.searchStudentClassInfoList(entity);
			if(null!=entityList && entityList.size()>0) {
				school =  entityList.get(0).getSchoolName();
			}
		}
		course.setSchoolName(school);
//		if(StringUtil.isNotBlank(course.getDirectionTagId()) && StringUtil.isBlank(course.getCategoryTagId())) {
//			course.setTagId(course.getDirectionTagId());
//		}else {
//			course.setTagId(course.getCategoryTagId());
//			course.setParentTagId(course.getDirectionTagId());
//		}
		
		ResultBean result = courseService.coursePublicList(course, pageSize, pageNumber);
		return result;
	}

	@RequestMapping("/courseFactoryList.action")
	@ResponseBody
	public ResultBean courseFactoryList(CourseManagerView course, Integer pageSize, Integer pageNumber) {
		course.setShareFlag(COURSE_SHARE_FLAG_TWO);
		//获取学校名字
		//获取工号
		UserEntity user = WebContext.getSessionUser();
				String uid =user.getUserId();
				TpUserEntity userEntity = courseService.searchInfoMessage(uid);
				//获取学校名称信息
				String school="";
				if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
					//获取学校
					school = userEntity.getSchoolName();
				}
				//获取班级
				if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
					List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
					StudentClassInfoEntity entity = new StudentClassInfoEntity();
					entity.setStudentId(uid);
					entityList = studentClassInfoService.searchStudentClassInfoList(entity);
					if(null!=entityList && entityList.size()>0) {
						school =  entityList.get(0).getSchoolName();
					}
				}
//				course.setSchoolName(school);
//		if(StringUtil.isNotBlank(course.getDirectionTagId()) && StringUtil.isBlank(course.getCategoryTagId())) {
//			course.setTagId(course.getDirectionTagId());
//		}else {
//			course.setTagId(course.getCategoryTagId());
//			course.setParentTagId(course.getDirectionTagId());
//		}
		
		
		ResultBean result = courseService.coursePublicList(course, pageSize, pageNumber);
		return result;
	}
	
	@RequestMapping("/courseQualityList.action")
	@ResponseBody
	public ResultBean courseQualityList(CourseManagerView course, Integer pageSize, Integer pageNumber) {
		course.setShareFlag(COURSE_SHARE_FLAG_ONE);
		//获取工号
		UserEntity user = WebContext.getSessionUser();
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		//获取学校名称信息
		String school="";
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
			//获取学校
			school = userEntity.getSchoolName();
		}
		//获取班级
		if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
			List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
			StudentClassInfoEntity entity = new StudentClassInfoEntity();
			entity.setStudentId(uid);
			entityList = studentClassInfoService.searchStudentClassInfoList(entity);
			if(null!=entityList && entityList.size()>0) {
				school =  entityList.get(0).getSchoolName();
			}
		}
		course.setSchoolName(school);
//		if(StringUtil.isNotBlank(course.getDirectionTagId()) && StringUtil.isBlank(course.getCategoryTagId())) {
//			course.setTagId(course.getDirectionTagId());
//		}else {
//			course.setTagId(course.getCategoryTagId());
//			course.setParentTagId(course.getDirectionTagId());
//		}
		ResultBean result = courseService.coursePublicList(course, pageSize, pageNumber);
		return result;
	}
	
	*//**
	 * 检索教师信息
	 * 
	 * @author LILIANG
	 * @return UserEntity
	 *//*
	@RequestMapping("/teacherInfo.action")
	@ResponseBody
	public ResultBean teacherInfo(UserEntity user) {
		ResultBean result = courseService.teacherInfo(user);
		return result;
	}

	*//**
	 * 检索与教师相关的课程信息
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 *//*
	@RequestMapping("/teacherCourseList.action")
	@ResponseBody
	public ResultBean teacherCourseList(CourseEntity c) {
		ResultBean result = courseService.teacherCourseList(c);
		return result;
	}

	*//**
	 * 课程浏览数+1
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 *//*
	@RequestMapping("/addCourseViewNum.action")
	@ResponseBody
	public ResultBean addCourseViewNum(CourseEntity c) {
		ResultBean result = courseService.addCourseViewNum(c);
		return result;
	}

	*//**
	 * 根据课程ID检索课程详情
	 * 
	 * @param courseId 课程ID
	 * @return ResultBean data:CourseView 课程基本信息、视频数、练习数、标签、用户课程等信息
	 *//*
	@RequestMapping("/courseDetail.action")
	@ResponseBody
	public ResultBean courseDetail(String courseId) {
		return courseService.searchCourseDetail(courseId);
	}

	*//**
	 * 课程详情-立即支付详情
	 * 
	 * @param courseId 课程Id
	 * @return
	 *//*
	@RequestMapping("/immediatePayDetail.action")
	@ResponseBody
	public ResultBean immediatePayDetail(String courseId) {
		return courseService.immediatePayDetail(courseId);
	}

	*//**
	 * 课程列表页面
	 *//*
	@RequestMapping("/courseListInit.action")
	public ModelAndView courseListInit(String courseId,String schoolName) {
		ModelAndView result = new ModelAndView("/course/client/course_list");
		result.addObject("schoolName", schoolName);
		result.addObject("courseId", courseId);
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/pubCourseListInit.action")
	public ModelAndView pubCourseListInit() {
		ModelAndView result = new ModelAndView("/course/client/course_list_public");
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/qualityCourseListInit.action")
	public ModelAndView qualityCourseListInit() {
		ModelAndView result = new ModelAndView("/course/client/course_list_quality");
		result = getRoleAndNum(result);
		return result;
	}
	
	@RequestMapping("/courseFactoryListInit.action")
	public ModelAndView courseFactoryListInit() {
		ModelAndView result = new ModelAndView("/course/client/course_list_factory");
		result = getRoleAndNum(result);
		return result;
	}
	
	@RequestMapping("/studentHomePage.action")
	public ModelAndView studentHomePage(String userId) {
		ModelAndView result = new ModelAndView("/course/client/studentHomePage");
		UserEntity loginUser = userManagementService.seachUserInfo(userId);
		// List<CourseEntity> courseList = courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result = getRoleAndNum(result);
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/studentHomePageFromList.action")
	public ModelAndView studentHomePageFromList(String userId) {
		ModelAndView result = new ModelAndView("/course/client/studentHomePageFromList");
		UserEntity loginUser = userManagementService.seachUserInfo(userId);
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result = getRoleAndNum(result);
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/teacherHomePage.action")
	public ModelAndView teacherHomePage(String userId) {
		ModelAndView result = new ModelAndView("/course/client/teacherHomePage");
		result = getRoleAndNum(result);
		UserEntity loginUser = userManagementService.seachUserInfo(userId);
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/teacherHomePageFromList.action")
	public ModelAndView teacherHomePageFromList(String userId) {
		ModelAndView result = new ModelAndView("/course/client/teacherHomePageFromList");
		result = getRoleAndNum(result);
		UserEntity loginUser = userManagementService.seachUserInfo(userId);
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/saveInfo.action")
	@ResponseBody
	public ResultBean saveInfo(String studentJson) throws Exception {
		ArchivesEntity archives = JSONUtil.parse(
				JSONUtil.createObMapperInstance(ConfigUtil.getDateMinuteFormatDefualt()), studentJson,
				ArchivesEntity.class);
		courseService.saveArchives(archives);
		ResultBean result = ResultBean.success();
		return result;
	}

	@RequestMapping("/studentList.action")
	public ModelAndView studentList() {
		ModelAndView result = new ModelAndView("/course/client/student_list");
		result = getRoleAndNum(result);
		UserEntity loginUser = userManagementService.seachUserInfo(WebContext.getSessionUserId());
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/teacherList.action")
	public ModelAndView teacherList() {
		ModelAndView result = new ModelAndView("/course/client/teacher_list");
		result = getRoleAndNum(result);
		UserEntity loginUser = userManagementService.seachUserInfo(WebContext.getSessionUserId());
		// List<CourseEntity> courseList =
		// courseService.getCourseListByUserId(WebContext.getSessionUserId());
		result.addObject("user", loginUser);
		return result;
	}

	@RequestMapping("/archivesList.action")
	@ResponseBody
	public ResultBean archivesList(String userId) {
		ResultBean result = new ResultBean();
		TpUserEntity tpUser = courseService.getUserByUserId(userId);
		List<ArchivesEntity> archivesList = courseService.getArchivesList(userId);
		List<CourseEntity> courseList = courseService.getCourseListByUserId(userId);
		if (courseList.size() > 0) {
			for (CourseEntity entity : courseList) {
				ArchivesEntity archives = new ArchivesEntity();
				archives.setUserId(userId);
				archives.setUserName(tpUser.getPersonName());
				archives.setCreateTime(sdf.format(entity.getCreateDate()));
				archives.setCreateDate(entity.getCreateDate());
				archives.setInfo(sdf.format(entity.getCreateDate()) + ",创建课程，" + entity.getTitle());
				if (entity.getPictureId() != null) {
					archives.setExitImage("1");
					archives.setImageId(entity.getPictureId());
				}
				// archivesList.add(archives);
			}
			listSort(archivesList);
		}
		List<UserCourseEntity> ucList = courseService.getUCListByUserId(userId);
		if (ucList.size() > 0) {
			for (UserCourseEntity uc : ucList) {
				ArchivesEntity archives = new ArchivesEntity();
				archives.setUserId(userId);
				archives.setUserName(tpUser.getPersonName());
				archives.setCreateTime(sdf.format(uc.getCreateDate()));
				archives.setCreateDate(uc.getCreateDate());
				archives.setInfo(sdf.format(uc.getCreateDate()) + ",加入课程，" + uc.getCourseName());
				if (uc.getPictureId() != null) {
					archives.setExitImage("1");
					archives.setImageId(uc.getPictureId());
				}
				// archivesList.add(archives);
			}
			listSort(archivesList);
		}
		result.setData(archivesList);
		return result;
	}

	@RequestMapping("/saveDiscussData.action")
	@ResponseBody
	public ResultBean saveDiscussData(ArchivesDiscussEntity data) throws Exception {
		return courseService.saveDiscuss(data);
	}

	@RequestMapping("/searchUsersList.action")
	@ResponseBody
	public ResultBean searchUsersList(TpUserEntity user) {
		String userKbn = WebContext.getSessionUser().getUserKbn();
		if (userKbn != null && userKbn.contentEquals(TpStateConstants.USER_KBN_C)) {
			user.setUserKbn(TpStateConstants.USER_KBN_C);
		}
		user.setSortName("createDate");
		user.setSortOrder("DESC");
		return userManagementService.search4Page(user);
	}

	@SuppressWarnings("unused")
	private void listSort(List<ArchivesEntity> list) {
		Collections.sort(list, new Comparator<ArchivesEntity>() {
			@Override
			public int compare(ArchivesEntity o1, ArchivesEntity o2) {
				try {
					Date dt1 = o1.getCreateDate();
					Date dt2 = o2.getCreateDate();
					if (dt1.getTime() > dt2.getTime()) {
						return -1;
					} else if (dt1.getTime() < dt2.getTime()) {
						return 1;
					} else {
						return 0;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return 0;
			}
		});
	}

	*//**
	 * 系统首页
	 *//*
	@RequestMapping("/homePage.action")
	public ModelAndView homePage() {
		ModelAndView result = new ModelAndView("/course/client/homePage");
		UserEntity user = WebContext.getSessionUser();
		//获取工号
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getIsFlag())) {
			result.addObject("isFlag","1");
		}else {
			//不是管理员
			result.addObject("isFlag","0");
		}
		
		//获取人员信息
		String image = "";
		String school="";
		String grade="";
		String className="";
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
			//获取头像
			image = userEntity.getHeadurl();
			//获取学校
			school = userEntity.getSchoolName();
		}else {
			//设置默认头像
			image ="http://wisdomsaas-img.oss-cn-beijing.aliyuncs.com/BFAA0FECDD194B7A974C60B7E1B27C18?authorization=bce-auth-v1%2F7642b4b48046452697e90662eed41ace%2F2017-12-29T08%3A33%3A39Z%2F-1%2F%2F0f06024af14dad05694f25b8c85572567591c72bbd0129d3175b4cdbd6e62208";
		}
		//获取班级
		if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
			List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
			StudentClassInfoEntity entity = new StudentClassInfoEntity();
			entity.setStudentId(uid);
			entityList = studentClassInfoService.searchStudentClassInfoList(entity);
			if(null!=entityList && entityList.size()>0) {
				grade = entityList.get(0).getGradeName();
				className = entityList.get(0).getClassName();
				school =  entityList.get(0).getSchoolName();
			}
		}
		result.addObject("className", className);
		result.addObject("grade", grade);
		result.addObject("school", school);
		result.addObject("image", image);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 问卷考试系统首页
	 *//*
	@RequestMapping("/homeJavaPage.action")
	public ModelAndView homeJavaPage(String courseId) {
		ModelAndView result = new ModelAndView("/course/client/homeJavaPage");
		UserEntity user = WebContext.getSessionUser();
		//获取工号
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getIsFlag())) {
			result.addObject("isFlag","1");
		}else {
			//不是管理员
			result.addObject("isFlag","0");
		}
		
		//获取人员信息
		String image = "";
		String school="";
		String grade="";
		String className="";
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
			//获取头像
			image = userEntity.getHeadurl();
			//获取学校
			school = userEntity.getSchoolName();
		}else {
			//设置默认头像
			image ="http://wisdomsaas-img.oss-cn-beijing.aliyuncs.com/BFAA0FECDD194B7A974C60B7E1B27C18?authorization=bce-auth-v1%2F7642b4b48046452697e90662eed41ace%2F2017-12-29T08%3A33%3A39Z%2F-1%2F%2F0f06024af14dad05694f25b8c85572567591c72bbd0129d3175b4cdbd6e62208";
		}
		//获取班级
		if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
			List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
			StudentClassInfoEntity entity = new StudentClassInfoEntity();
			entity.setStudentId(uid);
			entityList = studentClassInfoService.searchStudentClassInfoList(entity);
			if(null!=entityList && entityList.size()>0) {
				grade = entityList.get(0).getGradeName();
				className = entityList.get(0).getClassName();
				school =  entityList.get(0).getSchoolName();
			}
		}
		result.addObject("className", className);
		result.addObject("grade", grade);
		result.addObject("school", school);
		result.addObject("courseId", courseId);
		result.addObject("image", image);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 题库管理
	 *//*
	@RequestMapping("/homeExamPage.action")
	public ModelAndView homeExamPage() {
		ModelAndView result = new ModelAndView("/course/manager/homeExamPage");
		UserEntity user = WebContext.getSessionUser();
		//获取工号
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getIsFlag())) {
			result.addObject("isFlag","1");
		}else {
			//不是管理员
			result.addObject("isFlag","0");
		}
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 题库管理
	 *//*
	@RequestMapping("/homeJsdPage.action")
	public ModelAndView homeJsdPage() {
		ModelAndView result = new ModelAndView("/course/client/homeJsdPage");
		UserEntity user = WebContext.getSessionUser();
		//获取工号
		String uid =user.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		if(null!=userEntity && StringUtil.isNotBlank(userEntity.getIsFlag())) {
			result.addObject("isFlag","1");
		}else {
			//不是管理员
			result.addObject("isFlag","0");
		}
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 获取登录信息
	 * @param code
	 * @param type
	 * @return
	 * @throws UnsupportedEncodingException
	 *//*
	@SuppressWarnings("null")
	@RequestMapping("/getUserInfo.action")
	@ResponseBody
	public JSONArray getUserInfo(String code, String type) throws UnsupportedEncodingException {
		//设置json集合对象和数组
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		if (code != null && !"".equals(code)) {
			//UserEntity user = WebContext.getSessionUser();
			String codeSession = (String) WebContext.getSession().getAttribute("code");
			if (codeSession == null || !code.equals(codeSession)) {
				Long str12 = System.currentTimeMillis();
				String access_tokenStr = "";
				//定义获取应用token的变量
				//String application_tokenStr="";
				if (type.equals("1")) {
					//获取access_tokenStr字符串对象
					access_tokenStr = HttpUtils.sendFormEvalutionPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token",
							"code=" + code
									+ "&grant_type=authorization_code&client_id=1EF8798194B36D72&client_secret=440a4fad895036188ea200a0da1d6156&redirect_uri="
									+ PropertyUtil.getProperty("remote.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "1EF8798194B36D72", "440a4fad895036188ea200a0da1d6156");
				} else if (type.equals("2")) {
					//获取access_tokenStr字符串对象
					access_tokenStr = HttpUtils.sendFormEvalutionPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token",
							"code=" + code
									+ "&grant_type=authorization_code&client_id=BB2650288E091B1C&client_secret=26807915a5269ba07a40d97236b5ec64&redirect_uri="
									+ PropertyUtil.getProperty("teacher.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "BB2650288E091B1C", "26807915a5269ba07a40d97236b5ec64");
				} else if (type.equals("3")) {
					//获取access_tokenStr字符串对象
					access_tokenStr = HttpUtils.sendPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token",
							"code=" + code
									+ "&grant_type=authorization_code&client_id=42F9292D78B1386F&client_secret=e1e034ac44a63ba61e8e9595689efd4c&redirect_uri="
									+ PropertyUtil.getProperty("student.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "42F9292D78B1386F", "e1e034ac44a63ba61e8e9595689efd4c");
				} else if (type.equals("4")) {
					//获取access_tokenStr字符串对象
					   access_tokenStr = HttpUtils.sendFormEvalutionPost(
					            PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", 
					            "code=" + code + 
					            "&grant_type=authorization_code&client_id=6FF0E39AD917FD04&client_secret=ea201b6aff470f9ec25c1fc9be17197a&redirect_uri=" + 
					            PropertyUtil.getProperty("institution.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "6FF0E39AD917FD04", "ea201b6aff470f9ec25c1fc9be17197a");
				} else if (type.equals("5")) {
					//获取access_tokenStr字符串对象
					access_tokenStr = HttpUtils.sendFormEvalutionPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token",
							"code=" + code
									+ "&grant_type=authorization_code&client_id=8275669B807A7B7A&client_secret=229f39d615a8c2cbbef56e56197a22c7&redirect_uri="
									+ PropertyUtil.getProperty("teacherevalution.address.url"));
					//获取应用token
					//application_tokenStr = HttpUtils.sendAuthorDataPost(PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/oauth/token", "8275669B807A7B7A", "229f39d615a8c2cbbef56e56197a22c7");
				}
				//设置执行时间
				Long str333 = System.currentTimeMillis();
				int tokentime =(int) (str333-str12);
				UserLogEntity entity12=new UserLogEntity();
				entity12.setId(IDGenerator.genUID());
				entity12.setUserId(WebContext.getSessionUserId());
				entity12.setUserName(WebContext.getSessionUser().getPersonName());
				entity12.setLogType("获取token信息");
				entity12.setTimes(tokentime);
				entity12.setOperTime(new Date());
				//保存方法执行时间日志信息
				tagInstitutionEvalutionService.insertUserLog(entity12);
				//获取access_token
				if (access_tokenStr != null && !"".equals(access_tokenStr)) {
					//字符转化成json对象
					JSONObject accessjson = JSONObject.fromObject(access_tokenStr);
					//获取字段值
					String access_token = accessjson.getString("access_token");
					Long str3 = System.currentTimeMillis();
					//原有逻辑获取用户信息开始
					String userStr = HttpUtils.sendFormEvalutionPost(
							PropertyUtil.getProperty("huanshuo.remote.address.url") + "/passport/rest/get/userinfo",
							"access_token=" + access_token);
					Long str33 = System.currentTimeMillis();
					int userinfotime =(int) (str33-str3);
					UserLogEntity entity13=new UserLogEntity();
					entity13.setId(IDGenerator.genUID());
					entity13.setUserId(WebContext.getSessionUserId());
					entity13.setUserName(WebContext.getSessionUser().getPersonName());
					entity13.setLogType("获取/passport/rest/get/userinfo信息");
					entity13.setTimes(userinfotime);
					entity13.setOperTime(new Date());
					//保存方法执行需要的时间信息日志
					tagInstitutionEvalutionService.insertUserLog(entity13);
					System.out.println("userStr=:" + userStr);
					
					
					JSONObject userjson = JSONObject.fromObject(userStr);
					//原有逻辑获取用户信息结束
					//用户id
					String userid = userjson.getString("userid");
					//用户账号
					String account = userjson.getString("account");
					//用户类型
					String usertype = userjson.getString("usertype");
					//用户名称
					String name = userjson.getString("name");
					//用户性别
					String sex = userjson.getString("sex");
					//用户角色
					String role = userjson.getString("role");
					//获取tenantId
					String tenantId = userjson.getString("tenantId");
					//获取头像url
					String headUrl = userjson.getString("head_url");
					String teacher = "";
					*//**
					 * usertype=1
					 * 1:平台总系统管理员;2:平台省级系统管理员;3:平台市级系统管理员;4:学校总系统管理员;5:学校分校系统管理员;6:学校年级系统管理员;7:教育部分系统管理员
					 * usertype=0 
					 * 1:教师;2:家长;3:学生;4:管理者;5:教研员
					 *//*
					*//**
					 * 系统角色 01:管理员;04:教师
					 *//*
					UserRoleEntity userRole = new UserRoleEntity();
					UserRoleEntity userTeacherRole = new UserRoleEntity();
					UserEntity loginUser = new UserEntity();
					TpUserEntity saveUser = new TpUserEntity();
					*//**
					 * 获取用户是老师的信息
					 * 包括老师所属的学校信息
					 *//*
					if("1".equals(role)) {
						//获取教师信息
						Long str5 = System.currentTimeMillis();
						//调用外部接口获取教师信息
						String teacherinfo = HttpUtils.sendFormDataPost("https://gwss.yceduyun.com/apis/user/V1.0/teacherInfo",
								access_token,"002");
						Long str6 = System.currentTimeMillis();
						int teachertime =(int) (str6-str5);
						UserLogEntity entity2=new UserLogEntity();
						entity2.setId(IDGenerator.genUID());
						entity2.setUserId(WebContext.getSessionUserId());
						entity2.setUserName(WebContext.getSessionUser().getPersonName());
						entity2.setLogType("获取https://gwss.yceduyun.com/apis/user/V1.0/teacherInfo信息");
						entity2.setTimes(userinfotime);
						entity2.setOperTime(new Date());
						//保存获取教师信息所需要的时间日志信息
						tagInstitutionEvalutionService.insertUserLog(entity2);
						System.out.println(teacherinfo); 
                        //解析教师信息
						JSONObject teacherinfojson = JSONObject.fromObject(teacherinfo);
						JSONObject teacherinfoDatajson = JSONObject.fromObject(teacherinfojson.get("data"));
						//获取基本信息
						if(null!=teacherinfoDatajson) {
							//省份
							String proName=teacherinfoDatajson.getString("proName");
							//市区
							String cityName=teacherinfoDatajson.getString("cityName");
							//县级
							String townName=teacherinfoDatajson.getString("townName");
							//学校id
							String schoolId=teacherinfoDatajson.getString("schoolId");
							//学校名称
							String schoolName=teacherinfoDatajson.getString("schoolName");
							//学校类型
							String schoolType=teacherinfoDatajson.getString("schoolType");
							//
							String subjectId=teacherinfoDatajson.getString("subjectId");
							//
							String subjectName=teacherinfoDatajson.getString("subjectName");
							saveUser.setProName(proName);
							saveUser.setCityName(cityName);
							saveUser.setTownName(townName);
							saveUser.setSchoolId(schoolId);
							saveUser.setSchoolName(schoolName);
							saveUser.setSchoolType(schoolType);
							saveUser.setSubjectId(subjectId);
							saveUser.setSubjectName(subjectName);
						}
						//获取字段信息
						JSONArray teacherClassArray =JSONArray.fromObject(teacherinfoDatajson.get("classList"));
						//获取老师的班级年级信息
						@SuppressWarnings("unchecked")
						List<TeacherClassInfoEntity> list = teacherClassArray.toList(teacherClassArray, TeacherClassInfoEntity.class);
						//判断对象是否为空
						if(null!=list && list.size()>0) {
							List<TeacherClassInfoEntity> entityList = new ArrayList<TeacherClassInfoEntity>();
							//查询该老师下的信息是否存在
							TeacherClassInfoEntity entity = new TeacherClassInfoEntity();
							entity.setTeacherId(userid);
							entityList = teacherClassInfoService.searchTeacherClassInfoList(entity);
							if(null!=entityList && entityList.size()>0) {
								//循环遍历教师班级信息
								for(int j=0;j<entityList.size();j++) {
									TeacherClassInfoEntity entity1 = new TeacherClassInfoEntity();
									entity1.setId(entityList.get(j).getId());
									//删除已存在信息
									teacherClassInfoService.deleteTeacherClassInfoById(entity1);
								}
							}
							//循环保存班级信息
							for(int i=0;i<list.size();i++) {
								TeacherClassInfoEntity infoEntity = new TeacherClassInfoEntity();
								infoEntity.setTeacherId(userid);//老师id
								infoEntity.setGradeId(list.get(i).getGradeId());//年级id
								infoEntity.setGradeName(list.get(i).getGradeName());//年级名称
								infoEntity.setGradeLever(list.get(i).getGradeLever());//年级等级
								infoEntity.setClassId(list.get(i).getClassId());//班级id
								infoEntity.setClassName(list.get(i).getClassName());//班级名称
								infoEntity.setSubjectId(list.get(i).getSubjectId());//
								infoEntity.setSubjectName(list.get(i).getSubjectName());//
								infoEntity.setCreateDate(new Date());//创建日期
								infoEntity.setUpdateDate(new Date());//更新日期
								teacherClassInfoService.insertTeacherClassInfo(infoEntity);
							}
						}
						//获取用户具体信息
						String param=access_token;
						String tenantId1 = "002";
						Long str7 = System.currentTimeMillis();
						//获取教师角色
						String teacherDetailinfo = HttpUtils.sendFormDataDetailPost("https://gwss.yceduyun.com/apis/usercenter/v1.0/getOneUserInfoByUseIdAndRoleId",
								param,tenantId1,role);
						Long str8 = System.currentTimeMillis();
						int studenttime =(int) (str8-str7);
						UserLogEntity entity3=new UserLogEntity();
						entity3.setId(IDGenerator.genUID());
						entity3.setUserId(WebContext.getSessionUserId());
						entity3.setUserName(WebContext.getSessionUser().getPersonName());
						entity3.setLogType("获取https://gwss.yceduyun.com/apis/usercenter/v1.0/getOneUserInfoByUseIdAndRoleId信息");
						entity3.setTimes(userinfotime);
						entity3.setOperTime(new Date());
						//保存调用教师角色花费时间信息
						tagInstitutionEvalutionService.insertUserLog(entity3);
						//字符串转json
						JSONObject teacherDetailinfojson = JSONObject.fromObject(teacherDetailinfo);
						JSONArray teacherinfoDataDetailjson = JSONArray.fromObject(teacherDetailinfojson.get("data"));
						JSONArray dutyJson = JSONObject.fromObject(teacherinfoDataDetailjson.get(0)).getJSONArray("teacherDuties");
						List<InstitutionTeacherDutyEntity> dutylist = JSONArray.toList(dutyJson, InstitutionTeacherDutyEntity.class);
                        if(null!=dutylist && dutylist.size()>0) {
                        	for(int p=0;p<dutylist.size();p++) {
                        		//001代表校长
                        		if("校长".equals(dutylist.get(p).getDutyName())) {
                        			teacher="001";
                        			break;
                        		}else {
                        			//002代表老师
                        			teacher="002";
                        		}
                        	}
                        }
					}else if("3".equals(role)) {
						//获取学生信息
						Long str9 = System.currentTimeMillis();
						//调用学生接口获取信息
						String studentinfo = HttpUtils.sendFormDataPost("https://gwss.yceduyun.com/apis/user/V1.0/studentInfo",
								access_token,"002");
						Long str10 = System.currentTimeMillis();
						int studenttime =(int) (str10-str9);
						UserLogEntity entity3=new UserLogEntity();
						entity3.setId(IDGenerator.genUID());
						entity3.setUserId(WebContext.getSessionUserId());
						entity3.setUserName(WebContext.getSessionUser().getPersonName());
						entity3.setLogType("获取https://gwss.yceduyun.com/apis/user/V1.0/studentInfo信息");
						entity3.setTimes(userinfotime);
						entity3.setOperTime(new Date());
						//保存调用学生接口获取花费的时间日志
						tagInstitutionEvalutionService.insertUserLog(entity3);
						JSONObject studentinfojson = JSONObject.fromObject(studentinfo);
						JSONObject studentinfoDatajson = JSONObject.fromObject(studentinfojson.get("data"));
						if(null!= studentinfoDatajson) {
							String studentId=userid;
							//姓名
							String studentName=studentinfoDatajson.getString("username");
							String schoolId=studentinfoDatajson.getString("schoolId");//学校id
							String schoolName=studentinfoDatajson.getString("schoolName");//学校名称
							String schoolType=studentinfoDatajson.getString("schoolTye");//学校类型
							String gradeId=studentinfoDatajson.getString("gradeId");//年级id
							String gradeName=studentinfoDatajson.getString("gradeName");//年级名称
							String classId=studentinfoDatajson.getString("classId");//班级id
							String className=studentinfoDatajson.getString("className");//班级名称
							String gradeLever=studentinfoDatajson.getString("gradeLever");//年级等级
							String proName=studentinfoDatajson.getString("proName");//省份
							String cityName=studentinfoDatajson.getString("cityName");//市区
							String townName=studentinfoDatajson.getString("townName");//县级
							StudentClassInfoEntity studentClassInfoEntity = new StudentClassInfoEntity();
							studentClassInfoEntity.setStudentId(studentId);
							List<StudentClassInfoEntity> studentlist = new ArrayList<StudentClassInfoEntity>();
							studentlist = studentClassInfoService.searchStudentClassInfoList(studentClassInfoEntity);
							if(null!=studentlist && studentlist.size()>0) {
								//循环遍历学生信息
								for(int k=0;k<studentlist.size();k++) {
									StudentClassInfoEntity student = new StudentClassInfoEntity();
									student.setId(studentlist.get(k).getId());
									//删除学生信息
									studentClassInfoService.deleteStudentClassInfoById(student);
								}
							}
							//保存学生班级信息
							StudentClassInfoEntity stunfoEntity = new StudentClassInfoEntity();
							stunfoEntity.setStudentId(studentId);
							stunfoEntity.setStudentName(studentName);
							stunfoEntity.setSchoolId(schoolId);
							stunfoEntity.setSchoolName(schoolName);
							stunfoEntity.setSchoolType(schoolType);
							stunfoEntity.setGradeId(gradeId);
							stunfoEntity.setGradeName(gradeName);
							stunfoEntity.setGradeLever(gradeLever);
							stunfoEntity.setClassId(classId);
							stunfoEntity.setClassName(className);
							stunfoEntity.setProName(proName);
							stunfoEntity.setCityName(cityName);
							stunfoEntity.setTownName(townName);
							stunfoEntity.setCreateDate(new Date());
							studentClassInfoService.insertStudentClassInfo(stunfoEntity);
							Long str122 = System.currentTimeMillis();
							int studentinerttime =(int) (str122-str10);
							UserLogEntity entity32=new UserLogEntity();
							entity32.setId(IDGenerator.genUID());
							entity32.setUserId(WebContext.getSessionUserId());
							entity32.setUserName(WebContext.getSessionUser().getPersonName());
							entity32.setLogType("保存学生角色信息");
							entity32.setTimes(studentinerttime);
							entity32.setOperTime(new Date());
							tagInstitutionEvalutionService.insertUserLog(entity32);
						}
						
					}
					if (usertype != null && !"".equals(usertype) && usertype.equals("0")) {
						//老师
						if (!"".equals(role) && role.equals("1")) {
							userRole.setRoleId("01");
							userTeacherRole.setRoleId("04");
							loginUser.setUserKbn("A");
							saveUser.setUserKbn("A");
							if (userid != null && !"".equals(userid)) {
								userRole.setUserId(userid);
								userTeacherRole.setUserId(userid);
							}
							//管理者
						} else if (!"".equals(role) && role.equals("4")) {
							userRole.setRoleId("01");
							loginUser.setUserKbn("B");
							saveUser.setUserKbn("B");
							if (userid != null && !"".equals(userid)) {
								userRole.setUserId(userid);
							}
							//学生
						} else if(!"".equals(role) && role.equals("3")){
							loginUser.setUserKbn("C");
							saveUser.setUserKbn("C");
							//家长
						}else if(!"".equals(role) && role.equals("2")){
							loginUser.setUserKbn("D");
							saveUser.setUserKbn("D");
							//教研员
						}else if(!"".equals(role) && role.equals("5")){
							loginUser.setUserKbn("E");
							saveUser.setUserKbn("E");
						}
					}
					
					loginUser.setUserId(userid);
					loginUser.setUserName(account == null ? "" : account);
					loginUser.setSex(sex == null ? "" : sex);
					loginUser.setPersonName(name == null ? "" : name);
					loginUser.setPassword(MD5Util.getPassword4MD5("123456"));
					userRole.setCreateDate(new Date());
					userRole.setCreateUser(userid);
					userTeacherRole.setCreateDate(new Date());
					userTeacherRole.setCreateUser(userid);
					loginUser.setCreateDate(new Date());
					loginUser.setCreateUser(userid);
					loginUser.setLockFlag("0");

					saveUser.setUserId(userid);
					saveUser.setUserName(account == null ? "" : account);
					saveUser.setSex(sex == null ? "" : sex);
					saveUser.setPersonName(name == null ? "" : name);
					saveUser.setPassword(MD5Util.getPassword4MD5("123456"));
					saveUser.setCreateDate(new Date());
					saveUser.setCreateUser(userid);
					saveUser.setCreateDate(new Date());
					saveUser.setCreateUser(userid);
					saveUser.setLockFlag("0");
					saveUser.setTenantId(tenantId);
					saveUser.setHeadurl(headUrl);
					saveUser.setDuty(teacher);
					Long usertime1 = System.currentTimeMillis();
					courseService.saveUserAndRole(userRole, userTeacherRole, saveUser);
					Long usertime2 = System.currentTimeMillis();
					WebContext.loginUser(loginUser, true);
					WebContext.getSession().setAttribute("code", code);
					jo.put("result", "success");
					Long str123 = System.currentTimeMillis();
					System.out.println("计算花费时间---------------------------");
					int times = (int) (usertime2-usertime1);
					UserLogEntity entity=new UserLogEntity();
					entity.setId(IDGenerator.genUID());
					entity.setUserId(WebContext.getSessionUserId());
					entity.setUserName(WebContext.getSessionUser().getPersonName());
					entity.setLogType("获取保存用户信息");
					entity.setTimes(times);
					entity.setOperTime(new Date());
					tagInstitutionEvalutionService.insertUserLog(entity);
					System.out.println(str123-str12);
				}
			}

		}
		json.add(jo);
		return json;
	}
    *//**
     * 手机端首页
     * @return
     *//*
	@RequestMapping("/homePagePhone.action")
	public ModelAndView homePagePhone() {
		ModelAndView result = new ModelAndView("/course/client/homePagePhone");
		return result;
	}
    *//**
                * 教师页面
     * @return
     *//*
	@RequestMapping("/teacherHome.action")
	public ModelAndView teacherHome() {
		ModelAndView result = new ModelAndView("/course/client/teacherHomeBack");
		return result;
	}
    *//**
     * 学生页面
     * @return
     *//*
	@RequestMapping("/studentHome.action")
	public ModelAndView studentHome() {
		ModelAndView result = new ModelAndView("/course/client/studentHomeBack");
		return result;
	}
    *//**
     * 人员信息
     * @return
     *//*
	@RequestMapping("/sysUser.action")
	public ModelAndView sysUser() {
		ModelAndView result = new ModelAndView("/course/client/sysUser");
		result = getRoleAndNum(result);
		return result;
	}
    *//**
     * 系统日志
     * @return
     *//*
	@RequestMapping("/sysLog.action")
	public ModelAndView sysLog() {
		ModelAndView result = new ModelAndView("/course/client/sysLog");
		result = getRoleAndNum(result);
		return result;
	}
   *//**
    * 保存系统日志
    * @param proName
    * @param IpInfo
    * @return
    *//*
	@RequestMapping("/insertSysUser.action")
	@ResponseBody
	public JSONArray insertSysUser(String proName, String IpInfo) {
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		courseService.insertSysUser(proName, IpInfo);
		jo.put("result", "success");
		json.add(jo);
		return json;
	}
    *//**
     * 保存操作日志
     * @param browserInfo
     * @param sysInfo
     * @param IpInfo
     * @param operDetail
     * @param operSite
     * @return
     *//*
	@RequestMapping("/insertUserOper.action")
	@ResponseBody
	public JSONArray insertUserOper(String browserInfo, String sysInfo, String IpInfo, String operDetail,
			String operSite) {
		UserOperEntity oper = new UserOperEntity();
		oper.setSysInfo(browserInfo + "(" + sysInfo + ")");
		oper.setOperDetail(operDetail);
		oper.setOperSite(operSite);
		oper.setOperTime(new Date());
		oper.setOperIP(IpInfo);
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		courseService.inserUserOper(oper);
		jo.put("result", "success");
		json.add(jo);
		return json;
	}
	*//**
	 * 
	 * @param IpInfo
	 * @param logType
	 * @return
	 *//*
	@RequestMapping("/insertUserLog.action")
	@ResponseBody
	public JSONArray insertUserLog(String IpInfo, String logType) {
		UserLogEntity log = new UserLogEntity();
		log.setOperIP(IpInfo);
		log.setLogType(logType);
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		String userId = WebContext.getSessionUserId();
		String userName = WebContext.getSessionUser().getPersonName();
		log.setUserId(userId);
		log.setUserName(userName);
		courseService.insertUserLog(log);
		jo.put("result", "success");
		json.add(jo);
		WebContext.logout();
		return json;
	}
    *//**
                  * 根据用户名查询用户信息
     * @param username
     * @param IpInfo
     * @return
     *//*
	@RequestMapping("/getUserByUsername.action")
	@ResponseBody
	public JSONArray getUserByUserName(String username, String IpInfo) {
		TpUserEntity result = courseService.getUserByUsername(username);
		if (result != null) {
			UserLogEntity log = new UserLogEntity();
			log.setOperIP(IpInfo);
			log.setLogType("登录");
			log.setUserId(result.getUserId());
			log.setUserName(result.getPersonName());
			courseService.insertUserLog(log);
		}
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		if (result != null) {
			jo.put("result", "success");
		} else {
			jo.put("result", "error");
		}
		json.add(jo);
		return json;
	}

	*//**
	 * 系统首页
	 *//*
	@RequestMapping("/webEditor.action")
	public ModelAndView webEditor() {
		ModelAndView result = new ModelAndView("/course/client/webEditor");
		String serverName = WebContext.getRequest().getServerName();
		int serverPort = WebContext.getRequest().getServerPort();
		result.addObject("serverUrl", serverName + ":" + serverPort);
		return result;
	}
    *//**
     * 课程详情页面
     * @param courseId
     * @param schoolName
     * @return
     *//*
	@RequestMapping("/courseDetailInit.action")
	public ModelAndView courseDetailInit(String courseId,String schoolName) {
		ModelAndView result = new ModelAndView("/course/client/course_detail");
		ResultBean data = courseService.searchCourseDetail(courseId);
		if (!data.isStatus()) {
			throw new BusinessException(data.getMessages());
		}
		ChapterEntity entity = new ChapterEntity();
		entity.setCourseId(courseId);
		List<ChapterEntity> chapterCourseList = courseService.selectByCourseId(entity);
		result.addObject("chapterCourseList", chapterCourseList);
		result.addObject("courseView", data.getData());
		result.addObject("schoolName", schoolName);
		result.addObject("courseId", courseId);
		// 检索章节列表
		List<ChapterView> chapterList = chapterService.chapterList(courseId);
		result.addObject("chapterList", chapterList);
		String measureType="2";
		List<ChapterView> chapterList1 = chapterService.chapterList1(courseId,measureType);
		result.addObject("chapterExamList", chapterList1);
		List<CourseMessageEntity> userMsgList = courseService.getUserMsgList(courseId);
		if(userMsgList.size() > 0) {
			CourseMessageEntity msg = userMsgList.get(0);
			if(msg.getCheckStatus().equals("-1")) {
				result.addObject("msgInfo", "-1");
			}else if(msg.getCheckStatus().equals("1")) {
				result.addObject("msgInfo", "1");
			}else {
				result.addObject("msgInfo", "none");
			}
		}
		result = getRoleAndNum(result);
		return result;
	}
    *//**
     * 公开课详情页面
     * @param courseId
     * @return
     *//*
	@RequestMapping("/coursePublicDetail.action")
	public ModelAndView coursePublicDetail(String courseId) {
		ModelAndView result = new ModelAndView("/course/client/course_public_detail");
		ResultBean data = courseService.searchCourseDetail(courseId);
		if (!data.isStatus()) {
			throw new BusinessException(data.getMessages());
		}
		result.addObject("courseView", data.getData());
		result = getRoleAndNum(result);
		return result;
	}

	*//**
	 * 导师课程页面初始化
	 * 
	 * @param
	 * @return
	 *//*
	@RequestMapping("/teacherCourseInit.action")
	public ModelAndView teacherCourseInit(UserEntity user) {
		ModelAndView mv = new ModelAndView("/course/client/teacher_course_list");
		ResultBean result = courseService.teacherInfo(user);
		UserEntity use = new UserEntity();
		if (result.getData() != null) {
			use = (UserEntity) result.getData();
			if ("0".equals(use.getSex())) {
				use.setSex("男");
			} else {
				use.setSex("女");
			}
		}
		mv.addObject("user", use);
		return mv;
	}

	*//**
	 * 投票页面初始化
	 * 
	 * @param
	 * @return
	 *//*
	@RequestMapping("/voteItemInit.action")
	public ModelAndView voteItemInit(String courseId, String linkType) {
		ModelAndView mv = new ModelAndView("/course/client/vote_item");
		if (StringUtils.isNotEmpty(linkType)) {
			mv.addObject("linkType", linkType);
		}
		mv.addObject("courseId", courseId);
		return mv;
	}

	*//**
	 * 根据用户ID，课程ID检索用户课程
	 * 
	 * @param courseId
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchUserCourse.action")
	@ResponseBody
	public ResultBean searchUserCourse(String courseId) {
		ResultBean result = new ResultBean();
		if (courseId == null || courseId.equals("")) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_NULL_ID));

			return result;
		}

		UserCourseEntity uc = new UserCourseEntity();
		uc.setCourseId(courseId);
		// 从session中取出登录用户的ID
		String userId = WebContext.getSessionUserId();
		if (userId == null) {
			result.setStatus(false);
			result.setErrorCode(ErrorCode.NOT_LOGIN);
			result.setMessages(getMessage(MSG_E_SESSION_TIMEOUT));
			return result;
		}
		uc.setUserId(userId);
		uc = courseService.searchUserCourse(uc);
		result.setData(uc);
		result.setStatus(true);
		return result;
	}

	*//**
	 * 课程作业页面初始化
	 * 
	 * @param linkId   链接ID
	 * @param linkType 链接类型
	 * @return
	 *//*
	@RequestMapping("/homeWorkInit.action")
	public ModelAndView homeWorkInit(String linkId, String linkType) {
		ModelAndView mv = new ModelAndView("/course/client/home_work");
		if (StringUtils.isNotEmpty(linkType)) {
			mv.addObject("linkType", linkType);
		}
		mv.addObject("linkId", linkId);
		return mv;
	}

	*//**
	 * 检索我的作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/honeworkDetail.action")
	@ResponseBody
	public ResultBean honeworkDetail(HomeworkView homeworkView) {
		ResultBean result = new ResultBean();
		String userId = WebContext.getSessionUserId();
		List<HomeworkView> viewlist = new ArrayList<HomeworkView>();
		viewlist = (List<HomeworkView>) (courseService.honeworkList(homeworkView)).getData();
		for (int i = 0; i < viewlist.size(); i++) {
			CourseView courseView = new CourseView();
			courseView.setTeacherId(userId);
			if (courseService.isTeacher(courseView.getTeacherId())) {
				viewlist.get(i).setTeacher(1);
			} else {
				viewlist.get(i).setTeacher(0);
			}

		}

		result.setData(viewlist);
		return result;
	}

	@RequestMapping("/homeworkTitle.action")
	@ResponseBody
	public ResultBean homeworkTitle(HomeworkView homeworkView) {
		ResultBean result = new ResultBean();
		HomeworkEntity homeWork = courseService.getHomeWorkByLinkId(homeworkView);
		if (homeWork != null) {
			result.setStatus(true);
			result.setData(homeWork);
		}
		return result;
	}

	@RequestMapping("/homeWorkEdit.action")
	public ModelAndView homeWorkEdit(String id) {
		ModelAndView result = new ModelAndView("/course/client/home_work_edit");
		HomeworkView view = courseService.getHomeWorkById(id);
		result.addObject("homeWork", view);
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/homeWorkView.action")
	public ModelAndView homeWorkView(String id) {
		ModelAndView result = new ModelAndView("/course/client/home_work_view");
		HomeworkView view = courseService.getHomeWorkById(id);
		view.setCreateTime(sdf.format(view.getCreateDate()));
		result.addObject("homeWork", view);
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/homeWorkAddQuestion.action")
	public ModelAndView homeWorkAddQuestion(String id) {
		ModelAndView result = new ModelAndView("/course/client/home_work_question");
		HomeworkView view = courseService.getHomeWorkById(id);
		view.setCreateTime(sdf.format(view.getCreateDate()));
		result.addObject("homeWork", view);
		result.addObject("aq", new HomeworkAnswerQuestionEntity());
		result = getRoleAndNum(result);
		return result;
	}

	*//**
	 * 上传文件
	 *//*
	@RequestMapping("/upload.action")
	@ResponseBody
	public ResultBean upload(HomeworkView homeworkView) throws Exception {
		WebContext.init4CrossDomainAjax();
		return courseService.uploadDocument(homeworkView);
	}

	@RequestMapping("/download.action")
	public void download(@RequestParam(required = true) String fileId, String fileType, HttpServletResponse response)
			throws Exception {

		OutputStream output = new BufferedOutputStream(response.getOutputStream());
		InputStream input = null;
		try {
			FileEntity file = new FileEntity();
			if (StringUtil.isNotEmpty(fileType)) {
				file = fileService.getFile(fileId, fileType);
			} else {
				file = fileService.getFile(fileId, FILE_TYPE_HOMEWORK);
			}

			byte[] binary = file.getBinary();
			String fileName = file.getFileName();
			fileName = URLEncoder.encode(fileName, "UTF-8");
			response.reset();
			response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			response.addHeader("Content-Length", "" + file.getFileSize());
			response.setContentType("application/octet-stream;charset=UTF-8");
			if (StringUtil.isNotEmpty(file.getPath()) && StringUtil.isNotEmpty(ConfigUtil.getFileStorageRoot())) {
				// 文件有路径
				input = FileUtil.getInputStream(ConfigUtil.getFileStorageRoot(), file.getPath());
				IOUtils.copyLarge(input, output);
			} else {
				output.write(binary);
			}
			output.flush();
		} finally {
			IOUtils.closeQuietly(output);
			IOUtils.closeQuietly(input);
		}
	}

	*//**
	 * 课程作业管理页面初始化
	 * 
	 * @param
	 * @return
	 *//*
	@RequestMapping("/homeWorkReviewInit.action")
	public ModelAndView homeWorkReviewInit(String linkId, String linkType) {
		ModelAndView mv = new ModelAndView("/course/client/home_work_review");
		HomeworkView home = null;
		if (StringUtil.isNotEmpty(linkId) && StringUtil.isNotEmpty(linkType)) {
			home = courseService.searchworkDetail(linkId, linkType);
		}
		if (home == null) {
			home = new HomeworkView();
		}
		BaseEntity condition = new BaseEntity();
		condition.setPageSize(5);
		mv.addObject("linkId", linkId);
		mv.addObject("linkType", linkType);
		mv.addObject("title", home.getTitle());
		mv.addObject("courseTitle", home.getCourseTitle());
		mv.addObject("condition", condition);
		return mv;
	}

	*//**
	 * 导师品鉴作业初始化
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/homeworkReviewList.action")
	@ResponseBody
	public ResultBean homeworkReviewList(HomeworkView home) {

		return courseService.homeworkReviewList(home, PropertyUtil.getProperty("res.context.root"));
	}

	@RequestMapping("/gethomeworkReviewList.action")
	@ResponseBody
	public ResultBean gethomeworkReviewList(HomeworkView home) {
		return courseService.gethomeworkReviewList(home);
	}

	@RequestMapping("/gethomeworkReviewListByJson.action")
	@ResponseBody
	public JSONArray gethomeworkReviewListByJson(String courseId, String linkId, String linkType) throws IOException {

		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		List<HomeworkView> homeworkList = courseService.gethomeworkReviewListJson(courseId, linkId, linkType,
				PropertyUtil.getProperty("res.context.root"));
		if (homeworkList.size() > 0) {
			for (HomeworkView view : homeworkList) {
				Map<String, Object> courseERMap = new HashMap<String, Object>();
				courseERMap.put("userName", view.getCreateUser());
				courseERMap.put("audioContent", view.getContent() == null ? "" : view.getContent());
				// courseERMap.put("answerFileName", view.getAnswerFileName());
				courseERMap.put("score", view.getScore() == null ? "" : view.getScore());
				courseERMap.put("teacherComment", view.getTeacherComment() == null ? "" : view.getTeacherComment());
				// courseERMap.put("teacherScore", view.getTeacherScore() ==
				// null?"":view.getTeacherScore());
				// courseERMap.put("teacherComments", view.getTeacherComments() ==
				// null?"":view.getTeacherComments());
				resultList.add(courseERMap);
			}
		}
		jo.put("resultList", resultList);
		json.add(jo);
		return json;
	}

	*//**
	 * 导师评价
	 * 
	 * @author Id
	 * @param SCORE teacherComment
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/saveHomeworkReview.action")
	@ResponseBody
	public ResultBean saveHomeworkReview(HomeworkView home) {

		return courseService.saveHomeworkReview(home);
	}

	*//**
	 * 检索我的课程列表（检索用户课程，包括已关注、已学、已学完等检索条件以及检索列表中每一项课程的问卷）
	 * 
	 * @param finishType 完成类型（已收藏 1、已学2、3 已学完）
	 * @return 我的课程列表
	 * @author WUXIAOXIANG
	 *//*
	@RequestMapping("/userCourseList.action")
	@ResponseBody
	public ResultBean userCourseList(String finishType, Integer pageSize, Integer pageNumber) {
		// 检索我的课程
		ResultBean result = courseService.userCourseList(finishType, pageSize, pageNumber);
		return result;
	}

	@RequestMapping("/userCompulsoryList.action")
	@ResponseBody
	public ResultBean userCompulsoryList(String courseType, Integer pageSize, Integer pageNumber) {
		// 检索我的课程
		ResultBean result = courseService.userCompulsoryList(courseType, pageSize, pageNumber);
		return result;
	}

	*//**
	 * 检索我的作业列表
	 * 
	 * @author liuzhen
	 * @param
	 * @return 返回结果
	 *//*
	@RequestMapping("/userHomeworkList.action")
	@ResponseBody
	public ResultBean userHomeworkList(HomeworkView homeworkView, Integer pageSize, Integer pageNumber) {
		// 检索我的作业
		ResultBean result = courseService.userHomeworkList(homeworkView, pageSize, pageNumber);
		return result;
	}

	*//**
	 * 检索当前课程的所有作业
	 * 
	 * @author liuzhen
	 * @param
	 * @return 返回结果
	 *//*
	@RequestMapping("/alluserHomeworkList.action")
	@ResponseBody
	public ResultBean alluserHomeworkList(HomeworkView homeworkView, Integer pageSize, Integer pageNumber) {
		ResultBean result = courseService.getAlluserHomeworkList(homeworkView, pageSize, pageNumber);
		return result;
	}

	*//**
	 * 检索手机端导师个人中心的课程列表
	 * 
	 * @param pageSize   每页显示行数
	 * @param pageNumber 当前页
	 * @return 课程列表
	 * @author WUXIAOXIANG
	 *//*
	@RequestMapping("/teacherCourseAppList.action")
	@ResponseBody
	public ResultBean teacherCourseAppList(Integer pageSize, Integer pageNumber) {
		ResultBean result = courseService.teacherCourseAppList(pageSize, pageNumber);
		return result;
	}

	*//**
	 * 加入课程-支付详情初始化
	 * 
	 * @param courseId
	 * @return
	 *//*
	@SuppressWarnings({ "rawtypes" })
	@RequestMapping("/paydetail.action")
	public ModelAndView homeWorkReviewInit(String courseId) {
		ModelAndView mv = new ModelAndView("/course/client/course_pay_detail");
		UserEntity user = userService.searchById(WebContext.getSessionUserId());
		Map map = (Map) courseService.immediatePayDetail(courseId).getData();
		CourseView view = (CourseView) map.get("courseView");
		UserPoint userPoint = (UserPoint) map.get("userPoint");
		BigDecimal remainPoints = (BigDecimal) map.get("remainPoints");
		mv.addObject("user", user);
		mv.addObject("view", view);
		mv.addObject("userPoint", userPoint);
		mv.addObject("remainPoints", remainPoints);
		return mv;
	}

	*//**
	 * 用户管理页面 检索列表
	 * 
	 * @param condition
	 * @return 返回结果
	 *//*
	@RequestMapping("/search.action")
	@ResponseBody
	public ResultBean search(UserOperEntity condition) {
		return courseService.search4Page(condition);
	}

	@RequestMapping("/userOperInit.action")
	public ModelAndView userOperInit(String courseId) {
		ModelAndView result = new ModelAndView("/course/client/userOper");
		result = getRoleAndNum(result);
		return result;
	}

	@RequestMapping("/mgrApprove.action")
	public ModelAndView mgrApprove(String courseId) {
		ModelAndView result = new ModelAndView("/course/client/mgr_approve");
		result = getRoleAndNum(result);
		return result;
	}
	
	@RequestMapping("/approveSearch.action")
	@ResponseBody
	public ResultBean approveSearch(CourseApproveEntity entity){
		return courseService.approveSearch(entity);
	}
	
	@RequestMapping("/saveHomeWork.action")
	@ResponseBody
	public ResultBean saveHomeWork(HomeworkView homework) throws Exception {
		return courseService.saveHomeWorkAnswer(homework);
	}

	@RequestMapping("/saveHomeWorkById.action")
	@ResponseBody
	public ResultBean saveHomeWorkById(HomeworkView homework) throws Exception {
		return courseService.saveHomeWorkById(homework);
	}
	
	@RequestMapping("/saveHomeWorkReviewById.action")
	@ResponseBody
	public ResultBean saveHomeWorkReviewById(String courseJson) throws Exception {
		HomeworkView homework = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, HomeworkView.class);
		return courseService.saveHomeWorkReviewById(homework);
	}

	@RequestMapping("/saveHomeWorkAnswerQuestion.action")
	@ResponseBody
	public ResultBean saveHomeWorkAnswerQuestion(HomeworkAnswerQuestionEntity aq) throws Exception {
		return courseService.saveHomeWorkAnswerQuestion(aq);
	}

	@RequestMapping("/answerQuestionList.action")
	@ResponseBody
	public ResultBean answerQuestionList(HomeworkAnswerQuestionEntity aq) {
		return courseService.searchAnswerQuestionList(aq);
	}

	@RequestMapping("/likeHomeWorkById.action")
	@ResponseBody
	public ResultBean likeHomeWorkById(String id) throws Exception {
		return courseService.likeHomeWorkById(id);
	}

	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/institutionlist.action")
	public ModelAndView institutionlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/institution/manager/institution_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		return result;
	}

	*//**
	 * 学校评价总列表
	 * 
	 * @return
	 *//*
	@RequestMapping("/institutionManger.action")
	public ModelAndView institutionManger() {
		ModelAndView result = new ModelAndView();
		UserEntity user = WebContext.getSessionUser();
		if (!"游客".equals(user.getPersonName())) {
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			TpUserEntity entity =new TpUserEntity();
			entity = userManagementService.searchById(userId);
			if ("A".equals(role) && "001".equals(entity.getDuty())) {
				result = new ModelAndView("/institution/manager/institutionManagerBack");
			} else if ("C".equals(role) || ("A".equals(role) && !"001".equals(entity.getDuty()))) {
				result = new ModelAndView("/institution/manager/institutionEvlutionStudentBack");
			}else if("B".equals(role)) {
				result = new ModelAndView("/institution/manager/institutionBack");
			}
		} else {
			result = new ModelAndView("/institution/manager/institutionEvlutionStudentBack");
		}
		
		return result;
	}
	

	*//**
	 * 标签一览
	 *//*
	@RequestMapping("/searchTagList2.action")
	@ResponseBody
	public ResultBean searchTagList2(TagInstitutionEntityView tagEntity) {
		return taxonomyInstitutionService.searchTagList2(tagEntity);

	}
	*//**
	 * 标签一览
	 *//*
	@RequestMapping("/searchInstitutionTagList2.action")
	@ResponseBody
	public ResultBean searchInstitutionTagList2(TagInstitutionEntityView tagEntity) {
		tagEntity.setCreateUser(WebContext.getSessionUserId());
		return taxonomyInstitutionService.searchInstitutionTagList2(tagEntity);

	}
	*//**
	 * 标签一览
	 *//*
	@RequestMapping("/searchTagSchoolList2.action")
	@ResponseBody
	public ResultBean searchTagSchoolList2(TagInstitutionEntityView tagEntity) {
		return taxonomyInstitutionService.searchTagList2(tagEntity);

	}
	*//**
	 * 分类基本列表
	 * 
	 * @return
	 *//*
	@RequestMapping("/institutioninit.action")
	public ModelAndView institutioninit() {
		ModelAndView mv = new ModelAndView("/institutiontaxonomy/manager/taxonomy_course_list");
		BaseEntity condition = new BaseEntity();
		condition.setPageSize(5);
		mv.addObject("condition", condition);
		return mv;
	}

	*//**
	 * 分类查询列表
	 * 
	 * @param businessType
	 * @return
	 *//*
	@RequestMapping("/searchTagTypeList2.action")
	@ResponseBody
	public List<TagTypeInstitutionView> searchTagTypeList2(String businessType) {

		return taxonomyInstitutionService.searchTagTypeList2(businessType);

	}

	*//**
	 * 
	 * 删除标签
	 * 
	 * @param tagIds
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/delTag.action")
	@ResponseBody
	public ResultBean delTag(String tagId, String ico) throws Exception {

		return taxonomyInstitutionService.delTag(tagId, ico);

	}

	*//**
	 * 保存信息
	 * 
	 * @param browserInfo
	 * @param sysInfo
	 * @param IpInfo
	 * @param operDetail
	 * @param operSite
	 * @return
	 *//*
	@RequestMapping("/insertUserInstitution.action")
	@ResponseBody
	public JSONArray insertUserInstitution(String browserInfo, String sysInfo, String IpInfo, String operDetail,
			String operSite) {
		UserOperEntity oper = new UserOperEntity();
		oper.setSysInfo(browserInfo + "(" + sysInfo + ")");
		oper.setOperDetail(operDetail);
		oper.setOperSite(operSite);
		oper.setOperTime(new Date());
		oper.setOperIP(IpInfo);
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		tagInstitutionEvalutionService.inserUserOper(oper);
		jo.put("result", "success");
		json.add(jo);
		return json;
	}

	*//**
	 * 编辑标签
	 * 
	 * @param TagEntity
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/updateTag.action")
	@ResponseBody
	public ResultBean updateTag(TagInstitutionEntity tagEntity) throws Exception {

		return taxonomyInstitutionService.updateTag(tagEntity);

	}

	*//**
	 * 保存新增标签
	 * 
	 * @param parentId 父标签ID,tagTypeId:标签类ID,tagName:标签名称
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveTag2.action")
	@ResponseBody
	public ResultBean saveTag2(TagInstitutionEntityView tagEntity) throws Exception {
		return taxonomyInstitutionService.saveTag2(tagEntity);

	}

	*//**
	 * 
	 * 初始化课程列表
	 * 
	 * @param condition 课程对象
	 * @return 返回课程列表初始化页面
	 *//*
	@RequestMapping("/institutionListInit.action")
	public ModelAndView institutionListInit(InstitutionMessageEntity condition) {
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		result.addObject("course", new InstitutionMessageEntity());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}

	*//**
	 * 
	 * 检索评价列表
	 * 
	 * @param queryInfo 机构对象
	 * @return 返回评价列表
	 *//*
	@RequestMapping("/listSearch.action")
	@ResponseBody
	public ResultBean listSearch(TbInstitutionEvalutionManagerView queryInfo) {
		queryInfo.setCreateUser(WebContext.getSessionUserId());
		return taxonomyInstitutionService.listSearch(queryInfo);
	}

	*//**
	 * 
	 * 检索评价问卷列表
	 * 
	 * @param queryInfo 机构对象
	 * @return 返回评价列表
	 *//*
	@RequestMapping("/listQestionSearch.action")
	@ResponseBody
	public ResultBean listQestionSearch(MeasurementInstitutionEntity queryInfo) {
		UserEntity user = WebContext.getSessionUser();	
		String role = user.getUserKbn();
		//判断权限
		String userId = user.getCreateUser();
		queryInfo.setCreateUser(userId);
		return taxonomyInstitutionService.listSchoolMeasurementSearch(queryInfo);
	}

	*//**
	 * 评价新增页面初始化
	 * 
	 * @return
	 *//*
	@RequestMapping("/addInit.action")
	public ModelAndView addInit() {
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_add");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		result.addObject("pageType", "add");
		result.addObject("course", new TbInstitutionEvalutionManagerView());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}

	*//**
	 * 搜素全部学校
	 * 
	 * @param query
	 * @return
	 *//*
	@RequestMapping("/searchSchoolAll.action")
	@ResponseBody
	public ResultBean searchSchoolAll(InstitutionEntity query) {
		return institutionService.getTeacherAll(query);
	}
	*//**
	 * 搜素评价问卷全部学校
	 * 
	 * @param query
	 * @return
	 *//*
	@RequestMapping("/searchEvalutionSchoolAll.action")
	@ResponseBody
	public ResultBean searchEvalutionSchoolAll(InstitutionEntity query) {
		if(null!=query && StringUtil.isNotBlank(query.getInstitutionNameseacher())) {
			query.setInstitutionName(query.getInstitutionNameseacher());
		}
		return institutionService.getEvalutionTeacherAll(query);
	}

	*//**
	 * 
	 * 根据学校ID，检索学校信息
	 * 
	 * @return
	 *//*
	@RequestMapping("/searchSchool.action")
	@ResponseBody
	public ResultBean searchSchool(String id) {
		ResultBean result = ResultBean.success();
		result.setStatus(true);
		return institutionService.searchSchool(id);
	}

	*//**
	 * 保存评价任务信息
	 * 
	 * @param courseJson 任务信息
	 * @return 返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/saveEvalutionInfo.action")
	@ResponseBody
	public ResultBean saveEvalutionInfo(String courseJson) throws Exception {
		TbInstitutionEvalutionManagerView course = JSONUtil.parse(
				JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson,
				TbInstitutionEvalutionManagerView.class);
		ResultBean result = ResultBean.success();
		course.setStatus("0");
		if (StringUtil.isEmpty(course.getId())) {
			institutionService.insertCourseCatagory(course);
		} else {
			institutionService.updateCourseAll(course);
		}
		return result;
	}

	*//**
	 * 暂存
	 * 
	 * @param courseJson
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/zancunEvalutionInfo.action")
	@ResponseBody
	public ResultBean zancunEvalutionInfo(String courseJson) throws Exception {
		TbInstitutionEvalutionManagerView course = JSONUtil.parse(
				JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson,
				TbInstitutionEvalutionManagerView.class);
		ResultBean result = ResultBean.success();
		course.setStatus("0");
		if (StringUtil.isEmpty(course.getId())) {
			institutionService.insertCourseCatagory(course);
		} else {
			institutionService.updateCourseAll(course);
		}
		return result;
	}

	*//**
	 * 删除评价任务信息
	 * 
	 * @param id
	 * @return
	 *//*
	@RequestMapping("/deleteEvalution.action")
	@ResponseBody
	public ResultBean deleteEvalution(String id) {
		ResultBean result = new ResultBean();
		institutionService.deleteEvalution(id);
		result.setMessages(getMessage(MSG_S_DELETE));
		return result;
	}

	@RequestMapping("/editInit.action")
	public ModelAndView editInit(String id) {
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_add");
		String status = "0";
		String type = "edit";
		TbInstitutionEvalutionManagerView course = tagInstitutionEvalutionService.searchEvalutionById(id, type);
		String evalutionId = course.getId();
		String institutionId = course.getTeacherId();
		List<TagLinkInstitutionEntity> entityList = new ArrayList<TagLinkInstitutionEntity>();
		TagLinkInstitutionEntity entity = new TagLinkInstitutionEntity();
		entity.setLinkId(evalutionId);
		entityList = tagInstitutionEvalutionService.searchList(entity);
		if (null != entityList) {
			for (int i = 0; i < entityList.size(); i++) {
				if (StringUtils.isBlank(entityList.get(i).getParentTagId())) {
					TagInstitutionEntity isentity = new TagInstitutionEntity();
					isentity.setTagId(entityList.get(i).getTagId());
					isentity = tagInstitutionService.searchFirstData(isentity);
					course.setFirstTagName(isentity.getTagName());
					course.setFirstTag(isentity.getTagName());
				} else {
					TagInstitutionEntity isentity = new TagInstitutionEntity();
					isentity.setTagId(entityList.get(i).getTagId());
					isentity = tagInstitutionService.searchFirstData(isentity);
					course.setSecondTag(isentity.getTagName());
					course.setSecondTagName(isentity.getTagName());
				}
			}
		}
		InstitutionEntity institution = new InstitutionEntity();
		institution = institutionService.selectInstitutionById(institutionId);
		course.setInstitutionName(institution.getInstitutionName());
		course.setSchoolType(institution.getSchoolType());
		course.setInstitutionAbbreviation(institution.getInstitutionAbbreviation());
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		result.addObject("tagsSecond", tagsSecond);
		result.addObject("course", course);
		result.addObject("pageType", type);
		return result;
	}

	*//**
	 * 问卷添加
	 * 
	 * @param courseId 评价任务ID
	 * @return
	 *//*
	@RequestMapping("/measureInit.action")
	@ResponseBody
	public ModelAndView measureInit(String courseId) {
		ModelAndView result = new ModelAndView("institution/manager/insitution_measure_add");
		MeasurementInstitutionEntity measure = new MeasurementInstitutionEntity();
		result.addObject("measure", measure);
		result.addObject("courseId", courseId);
		result.addObject("pageType", "add");
		return result;
	}
	*//**
	 * 问卷添加
	 * 
	 * @param courseId 评价任务ID
	 * @return
	 *//*
	@RequestMapping("/measureInit1.action")
	@ResponseBody
	public ModelAndView measureInit1(String courseId) {
		ModelAndView result = new ModelAndView("institution/manager/insitution_measure_editor");
		MeasurementInstitutionEntity measure = new MeasurementInstitutionEntity();
		measure.setLinkId(courseId);
		measure = measurementInstitutionService.findInstitutionMeasurement(measure);
		List<QuestionInstitutionEntity> questionList = measurementInstitutionService.searchQuestionList(measure.getId());
		measure.setQuestionList(questionList);
		result.addObject("measure", measure);
		result.addObject("courseId", courseId);
		result.addObject("pageType", "add");
		return result;
	}
	*//**
	 * 
	 * 问卷编辑页面初始化
	 * 
	 * @param courseId
	 * @return
	 *//*
	@RequestMapping("/measureInsitutionEditInit.action")
	@ResponseBody
	public ModelAndView measureEditInit(String courseId) {
		ModelAndView result = new ModelAndView("institution/manager/insitution_measure_add");
		MeasurementInstitutionEntity measure = measurementInstitutionService.seachMeasureById(courseId);
		result.addObject("measure", measure);
		result.addObject("courseId", courseId);
		result.addObject("isOnlyView", true);
		result.addObject("pageType", "edit");
		return result;
	}

	*//**
	 * 
	 * 问卷添加/编辑
	 * 
	 * @param measureJson
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveMeasure.action")
	@ResponseBody
	public ResultBean saveMeasure(String measureJson) throws Exception {
		MeasurementInstitutionEntity measure = JSONUtil.parse(
				JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), measureJson,
				MeasurementInstitutionEntity.class);
		ResultBean result = ResultBean.success();
		if (StringUtil.isEmpty(measure.getId())) {
			measurementInstitutionService.saveMeasure(measure);
		} else {
			measurementInstitutionService.updateInstitutionMeasure(measure);
		}
		return result;
	}

	*//**
	 * 
	 * 初始化评价问卷列表
	 * 
	 * @param condition 评价对象
	 * @return 返回课程列表初始化页面
	 *//*
	@RequestMapping("/institutionQuestListInit.action")
	public ModelAndView institutionQuestListInit(InstitutionMessageEntity condition) {
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_question_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		result.addObject("course", new InstitutionMessageEntity());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}

	*//**
	 * 
	 * 评价的发布/取消发布
	 * 
	 * @param id     课程id
	 * @param status 评价状态：0 未发布 1 已发布
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/publicEvalution.action")
	@ResponseBody
	public ResultBean publicEvalution(String id, String status) throws Exception {
		tagInstitutionEvalutionService.publicEvalution(id, status);
		return ResultBean.success(MSG_S_SUBMIT);
	}

	*//**
	 * 问卷显示页面
	 *//*
	@RequestMapping("/question.action")
	public ModelAndView question(String linkId, String linkType) {
		ModelAndView result = new ModelAndView("/institution/manager/institution_evalution_questionner_item");
		result.addObject("linkId", linkId);
		result.addObject("linkType", linkType);
		return result;
	}

	*//**
	 * 问卷题目选项
	 *//*
	@RequestMapping("/questionnaireDetial.action")
	@ResponseBody
	public ResultBean questionnaireDetial(String linkId, String linkType) {
		return questionInstitutionService.questionnaireDetial(linkId, linkType);
	}

	*//**
	 * 问卷提交
	 *//*
	@RequestMapping("/submitQuestionnaire.action")
	@ResponseBody
	public ResultBean submitQuestionnaire(String questionIds, String optionIds, String measureId) {
		return questionInstitutionService.submitQuestionnaire(questionIds, optionIds, measureId);
	}

	*//**
	 * 问卷调查结果列表
	 * 
	 * @author heyaqin
	 * @return 返回结果
	 *//*
	@RequestMapping("/questionResultList.action")
	public ModelAndView questionResultList(MeasurementInstitutionEntity condition) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_result_list");
		mv.addObject("condition", condition);
		UserEntity user = WebContext.getSessionUser();
		if (!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if (roleList.size() > 0) {
				for (UserRoleEntity role : roleList) {
					if (role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			} else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}

	*//**
	 * 问卷调查列表检索
	 * 
	 * @author
	 * @return 返回结果
	 *//*
	@RequestMapping("/questionResultListSearch.action")
	@ResponseBody
	public ResultBean questionResultListSearch(MeasurementInstitutionEntity condition) {
			UserEntity user = WebContext.getSessionUser();	
			String userId = user.getCreateUser();
			condition.setCreateUser(userId);
		return userManagementService.selectQuestionResultList(condition);
	}

	*//**
	 * 问卷统计列表初始化
	 * 
	 * @author
	 * @param id 问卷ID
	 * @return 返回结果
	 *//*
	@RequestMapping("/viewMeasurementListInit.action")
	public ModelAndView viewMeasurementListInit(String id) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_statistics");
		int optionNum = userManagementService.searchInstitutionTotalOptions(id) + 1;
		QuestionOptionInstitutionEntity questionOptionEntity = new QuestionOptionInstitutionEntity();
		questionOptionEntity.setMeasureId(id);
		mv.addObject("questionOptionEntity", questionOptionEntity);
		mv.addObject("optionNum", optionNum);
		// 问卷统计结果列表
		ArrayList<Map<String, String>> optionMapList = userManagementService.selectInstitutionOptionsList(id);
		mv.addObject("optionMapList", optionMapList);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 问卷统计列表检索
	 * 
	 * @author HEYAQIN
	 * @param id 问卷ID
	 * @return 返回结果
	 *//*
	@RequestMapping("/viewMeasurementList.action")
	@ResponseBody
	public ArrayList<Map<String, String>> viewMeasurementList(String id) {
		if (StringUtil.isEmpty(id)) {
			return null;
		} else {
			ArrayList<Map<String, String>> optionMapList = userManagementService.selectInstitutionOptionsList(id);
			return optionMapList;
		}
	}

	*//**
	 * 问卷参与人员 检索
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/viewUserSearch.action")
	@ResponseBody
	public ResultBean viewUserSeacher(String measurementId) {
		return userManagementService.selectEvalutionQuestionUserList(measurementId);
	}

	*//**
	 * 问卷参与人员回答详情
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/viewDetailInit.action")
	public ModelAndView viewDetailInit(UserMeasurementResultEntity condition) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_user_detail");
		if (StringUtil.isEmpty(condition.getMeasurementId())) {
			throw new BusinessException("参数错误!");
		}

		List<UserMeasurementResultEntity> list = measuermentService
				.searchInstitutionUserMeasurementResultByEntiy(condition);
		if (list != null && list.size() != 0) {
			mv.addObject("condition", list.get(0));
		} else {
			mv.addObject("condition", new UserMeasurementResultEntity());
		}
		return mv;
	}

	*//**
	 * 问卷回答详情
	 * 
	 * @param measurementId
	 * @param userId
	 * @return
	 *//*
	@RequestMapping("/viewUserAnswerSearch.action")
	@ResponseBody
	public ResultBean viewUserAnswerSearch(String measurementId, String userId) {
		return userManagementService.selectEvalutionAnswerUserList(measurementId, userId);
	}

	*//**
	 * 检索测试信息
	 * 
	 * @param linkId
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchMeasuerment.action")
	@ResponseBody
	public ResultBean searchMeasuerment(String measurementId, String userId) {
		if (StringUtil.isEmpty(measurementId)) {
			return ResultBean.error(getMessage("参数错误!"));
		}
		ResultBean result = ResultBean.success();
		List<QuestionInstitutionEntity> questionList = measurementInstitutionService
				.searchMeasuermentInstitutionNormalAndExam(userId, measurementId);
		result.setData(questionList);
		return result;
	}

	*//**
	 * 问卷参与人员 画面初始化
	 * 
	 * @param condition
	 * @return
	 *//*
	@SuppressWarnings("unchecked")
	@RequestMapping("/viewUserInit.action")
	public ModelAndView viewUserInit(MeasurementInstitutionEntity condition) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_user_list");
		mv.addObject("measurementId", condition.getId());
		List<MeasurementInstitutionEntity> list = measurementInstitutionService.search(condition);
		if (list != null && list.size() != 0) {
			mv.addObject("condition", list.get(0));
		} else {
			throw new BusinessException("参数错误!");
		}
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 删除问卷调查列表
	 * 
	 * @author heyaqin
	 * @return 返回结果
	 *//*
	@RequestMapping("/deleteQuestionList.action")
	@ResponseBody
	public ResultBean deleteQuestionList(String id) {
		userManagementService.deleteEvalutionQuestionList(id);
		return ResultBean.success();
	}

	*//**
	 * 导入页面初始化
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/importInit.action")
	public String importInit() {
		return "institution/manager/institution_import";
	}

	*//**
	 * 导入机构
	 * 
	 * @param linkId
	 * @return 返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/importUpload.action")
	@ResponseBody
	public ResultBean importCourse(@RequestParam("institution") CommonsMultipartFile institution,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		InputStream input = null;
		try {
			input = institution.getInputStream();
			File targetFile = FileUtil.writeToTmpFile(input);
			return institutionService.importInstitution(targetFile);
		} finally {
			IOUtils.closeQuietly(input);
		}
	}

	*//**
	 * 批量导出机构
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/exportInstitution.action")
	@ResponseBody
	public InstitutionManagerView exportInstitution(InstitutionEntity entity) throws Exception {
		List<InstitutionManagerView> viewList = new ArrayList<InstitutionManagerView>();
		UserEntity userEntity = WebContext.getSessionUser();
		if (!"游客".equals(userEntity.getPersonName())) {
			String role = userEntity.getUserKbn();
			String userId=userEntity.getUserId();
			if("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				entity.setCreateUser(userId);
				viewList = institutionService.exportInstitution(entity);
			}else if("B".equals(role)) {
				entity.setCreateUser(userId);
				viewList = institutionService.exportManagerInstitution(entity);
			}
		}
		
		InstitutionManagerView user = new InstitutionManagerView();
		user.setInstitutionList(viewList);
		ExcelDataBind edbUser = new ExcelDataBind(user);
		InputStream is = null;
		OutputStream os = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String date = sdf.format(new Date());
			String fileName = URLEncoder.encode("组织机构" + date + ".xlsx", "UTF-8");
			WebContext.getResponse().reset();
			is = InstitutionService.class.getResourceAsStream("institutionExportTemplate.xlsx");
			WebContext.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			WebContext.getResponse()
					.setContentType(" application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8");
			edbUser.bind(is, WebContext.getResponse().getOutputStream());
		} catch (IOException ex) {

		} finally {

			IOUtils.closeQuietly(is);
			IOUtils.closeQuietly(os);
		}
		return null;
	}

	*//**
	 * 增加机构方法
	 * 
	 * @param entity
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/addJyInstitution.action")
	public ModelAndView addJyInstitution(InstitutionEntity entity) throws Exception {
		ModelAndView mv = new ModelAndView("institution/manager/institution_add");
		InstitutionEntity institutionEntity = new InstitutionEntity();
		UserEntity user = WebContext.getSessionUser();
		String role = user.getUserKbn();
		//判断权限
		List<InstitutionEntity> schoollist=new ArrayList<InstitutionEntity>();
		String userId = user.getCreateUser();
		if ("A".equals(role)) {
			TpUserEntity tpUserEntity =new TpUserEntity();
			tpUserEntity = userManagementService.searchById(userId);
			institutionEntity.setInstitutionName(tpUserEntity.getSchoolName());
			institutionEntity=institutionService.seacherPersonEdution(institutionEntity);
			if(null!=institutionEntity) {
				entity.setInstitutionName(institutionEntity.getInstitutionName());
			}
			schoollist.add(institutionEntity);
		}else if("B".equals(role)) {
			institutionEntity.setCreateUser(userId);
			institutionEntity= institutionService.searchEdution(institutionEntity);
            if(null!=institutionEntity) {
            	entity.setInstitutionName(institutionEntity.getInstitutionName());
			}
            schoollist.add(institutionEntity);
		}
		mv.addObject("schoollist", schoollist);
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 添加学校信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/addInstitution.action")
	public ModelAndView addInstitution(String institutionParentId) throws Exception {
		InstitutionEntity entity = new InstitutionEntity();
		entity.setInstitutionParentId(institutionParentId);
		ModelAndView mv = new ModelAndView("institution/manager/institution_addSchool");
		List<InstitutionEntity> schoollist=new ArrayList<InstitutionEntity>();
		UserEntity user = WebContext.getSessionUser();
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				schoollist = institutionService.seacherSchoolLocationList(entity);
			} else if("B".equals(role)) {
				entity.setCreateUser(userId);
				schoollist= institutionService.searchEdutionSchoolList(entity);
			}
		
		mv.addObject("schoollist", schoollist);
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 删除班级
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/deleteInstitutionById.action")
	@ResponseBody
	public ResultBean deleteGradeById(String id) throws Exception {
		InstitutionEntity entity = new InstitutionEntity();
		entity.setId(id);
		institutionService.deleteInstitutionById(entity);
		return ResultBean.success(DELETE_GRADE_SUCCESS);
	}

	*//**
	 * 跳转到班级编辑页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/institutionEdit.action")
	public ModelAndView institutionEdit(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		ModelAndView mv = new ModelAndView("institution/manager/institution_edit");
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 检索班级信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchInstitution.action")
	@ResponseBody
	public ResultBean searchNotice(InstitutionEntity entity) {
		return institutionService.searchInstitutionEvalutionList(entity);
	}
	*//**
	 * 学校检索班级信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/schoolsearchInstitution.action")
	@ResponseBody
	public ResultBean schoolsearchInstitution(InstitutionEntity entity) {
		return institutionService.searchInstitutionEvalutionList1(entity);
	}
	*//**
	 * 检索班级信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchSchoolInstitution.action")
	@ResponseBody
	public ResultBean searchSchoolInstitution(InstitutionEntity entity) {
		ResultBean result = new ResultBean();
		UserEntity user = WebContext.getSessionUser();
		if (!"游客".equals(user.getPersonName())) {
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				entity.setCreateUser(userId);
				result=institutionService.searchInstitutionTeacherEvalutionList(entity);
			} else if ("C".equals(role)) {
				entity.setCreateUser(userId);
				StudentClassInfoEntity classInfo = new StudentClassInfoEntity();
				classInfo.setStudentId(userId);
				classInfo = studentClassInfoService.searchStudentClassInfo(classInfo);
			    if(null!=classInfo) {
			    	entity.setInstitutionName(classInfo.getSchoolName());
					result= institutionService.searchInstitutionEvalutionList(entity);
			    }
			}else if("B".equals(role)) {
				entity.setCreateUser(userId);
				result= institutionService.searchInstitutionManagerEvalutionList(entity);
			}
		}
		return result;
	}

	*//**
	 * 新增信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/insertInstitution.action")
	@ResponseBody
	public ResultBean insertInstitution(InstitutionEntity entity) throws Exception {
		ResultBean result = new ResultBean();
		InstitutionEntity institutionEntity = new InstitutionEntity();
		institutionEntity.setInstitutionName(entity.getInstitutionName());
		List<InstitutionEntity> datalist = (List<InstitutionEntity>) institutionService.searchInstitutionEvalutionList1(institutionEntity).getData();
		if(null!=datalist && datalist.size()>0) {
			result.setStatus(false);
			result = ResultBean.error("学校名称已存在");
		}else {
//			String institutionType=entity.getInstitutionType();
//			TagInstitutionEntity tagInstitutionEntity = new TagInstitutionEntity();
//			tagInstitutionEntity.setTagId(institutionType);
//			tagInstitutionEntity = tagInstitutionService.searchFirstData(tagInstitutionEntity);
//			entity.setInstitutionType(tagInstitutionEntity.getTagName());
			if(StringUtil.isNotBlank(entity.getSchoolNatrue())) {
				if("1".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("小学");
				}else if("2".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("初中");
				}else if("3".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("高中");
				}else if("4".equals(entity.getSchoolNatrue())) {
				   entity.setInstitutionType("九年一贯制");
			    }
			}
			institutionService.insertInstitution(entity, getCompressConfigByName("imageP"));
			result.setStatus(true);
			result= ResultBean.success(MSG_ADD_GRADE_SUCCESS);
		}
		return result;
	}
	*//**
	 * 新增信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/insertSchoolEvalutionInstitution.action")
	@ResponseBody
	public ResultBean insertSchoolEvalutionInstitution(InstitutionEntity entity) throws Exception {
		institutionService.insertInstitution(entity, getCompressConfigByName("imageP"));
		return ResultBean.success(MSG_ADD_GRADE_SUCCESS);
	}
	*//**
	 * 编辑保存
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/updateInstitution.action")
	@ResponseBody
	public ResultBean updateInstitution(InstitutionEntity entity) throws Exception {
		ResultBean result = new ResultBean();
		InstitutionEntity institutionEntity = new InstitutionEntity();
		institutionEntity.setInstitutionName(entity.getInstitutionName());
		List<InstitutionEntity> datalist = (List<InstitutionEntity>) institutionService.searchInstitutionEvalutionList1(institutionEntity).getData();
		if(null!=datalist && datalist.size()>0 && !entity.getId().equals(datalist.get(0).getId())) {
			result.setStatus(false);
			result = ResultBean.error("学校名称已存在");
		}else {
//			String institutionType=entity.getInstitutionType();
//			TagInstitutionEntity tagInstitutionEntity = new TagInstitutionEntity();
//			tagInstitutionEntity.setTagId(institutionType);
//			tagInstitutionEntity = tagInstitutionService.searchFirstData(tagInstitutionEntity);
//			entity.setInstitutionType(tagInstitutionEntity.getTagName());
			if(StringUtil.isNotBlank(entity.getSchoolNatrue())) {
				if("1".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("小学");
				}else if("2".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("初中");
				}else if("3".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("高中");
				}else if("4".equals(entity.getSchoolNatrue())) {
					   entity.setInstitutionType("九年一贯制");
				}
			}
			institutionService.updateInstitution(entity, getCompressConfigByName("imageP"));
			result.setStatus(true);
			result = ResultBean.success("创建学校成功");
		}
		return result;
	}
	*//**
	 * 编辑保存
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/updateSchoolEvalutionInstitution.action")
	@ResponseBody
	public ResultBean updateSchoolEvalutionInstitution(InstitutionEntity entity) throws Exception {
		entity.setCreateUser(WebContext.getSessionUserId());
		institutionService.updateInstitution(entity, getCompressConfigByName("imageP"));
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/institutionStudentlist.action")
	public ModelAndView institutionStudentlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/institution/manager/institution_student_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		return result;
	}

	*//**
	 * 跳转到班级查看页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/institutionLook.action")
	public ModelAndView institutionLook(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		ModelAndView mv = new ModelAndView("institution/manager/institution_look");
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 老师分享作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/shareWork.action")
	@ResponseBody
	public ResultBean shareWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.shareHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 老师取消分享作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/cancleShareWork.action")
	@ResponseBody
	public ResultBean cancleShareWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.cancleShareHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);

	}

	*//**
	 * 老师置顶作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/topWork.action")
	@ResponseBody
	public ResultBean topWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.topHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 老师取消置顶作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/cancleTopWork.action")
	@ResponseBody
	public ResultBean cancleTopWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.cancleTopHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 老师删除作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/deleteWork.action")
	@ResponseBody
	public ResultBean deleteWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.deleteWork(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/baseinstitutionlist.action")
	public ModelAndView baseinstitutionlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/baseinstitution/manager/base_institution_list");
		result.addObject("condition", condition);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/schoolbaseinstitutionlist.action")
	public ModelAndView schoolbaseinstitutionlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/baseinstitution/manager/base_school_institution_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 增加机构方法
	 * 
	 * @param entity
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/addBaseInstitution.action")
	public ModelAndView addBaseInstitution(InstitutionEntity entity) throws Exception {
		ModelAndView mv = new ModelAndView("baseinstitution/manager/base_institution_add");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList1(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		List<TagInstitutionEntity> tagList = tagInstitutionService.searchTagInstitutionAll();
		mv.addObject("tagList", tagList);
		mv.addObject("catagoryTags", tags);
		mv.addObject("tagsSecond", tagsSecond);
		mv.addObject("institution", entity);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 跳转到班级编辑页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/baseinstitutionEdit.action")
	public ModelAndView baseinstitutionEdit(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		ModelAndView mv = new ModelAndView("baseinstitution/manager/base_institution_edit");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList1(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		List<TagInstitutionEntity> tagList = tagInstitutionService.searchTagInstitutionAll();
        String secondId = entity.getFirstTag();
        TagInstitutionEntity taginfo = tagInstitutionService.searchTagInstitution(secondId);
        entity.setSecondTag(taginfo.getParentId());
		mv.addObject("tagList", tagList);
		mv.addObject("catagoryTags", tags);
		mv.addObject("tagsSecond", tagsSecond);
		mv.addObject("institution", entity);
		mv = getRoleAndNum(mv);
		return mv;
	}
	
	*//**
	 * 跳转到班级编辑页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/baseSchoolinstitutionEdit.action")
	public ModelAndView baseSchoolinstitutionEdit(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		InstitutionEntity institutionEntity = new InstitutionEntity();
		ModelAndView mv = new ModelAndView("baseinstitution/manager/base_school_institution_edit");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList1(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		List<TagInstitutionEntity> tagList = tagInstitutionService.searchTagInstitutionAll();
		List<InstitutionEntity> schoollist=new ArrayList<InstitutionEntity>();
		UserEntity user = WebContext.getSessionUser();
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			if(StringUtil.isNotBlank(entity.getInstitutionParentId())) {
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				schoollist = institutionService.seacherSchoolLocationList(entity);
			} else if("B".equals(role)) {
				entity.setCreateUser(userId);
				schoollist= institutionService.searchEdutionSchoolList(entity);
			}
			
		}else {
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				institutionEntity.setInstitutionName(tpUserEntity.getSchoolName());
				institutionEntity=institutionService.seacherPersonEdution(institutionEntity);
				entity.setInstitutionName(institutionEntity.getInstitutionName());
				schoollist.add(institutionEntity);
			}else if("B".equals(role)) {
				institutionEntity.setCreateUser(userId);
				institutionEntity= institutionService.searchEdution(institutionEntity);
				entity.setInstitutionName(institutionEntity.getInstitutionName());
				schoollist.add(institutionEntity);
			}
		}
		
		mv.addObject("schoollist", schoollist);
		mv.addObject("tagList", tagList);
		mv.addObject("catagoryTags", tags);
		mv.addObject("tagsSecond", tagsSecond);
		mv.addObject("institution", entity);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 分类基本列表
	 * 
	 * @return
	 *//*
	@RequestMapping("/baseinstitutioninit.action")
	public ModelAndView baseinstitutioninit() {
		ModelAndView mv = new ModelAndView("baseinstitutiontaxonomy/manager/base_taxonomy_course_list");
		BaseEntity condition = new BaseEntity();
		condition.setPageSize(5);
		mv.addObject("condition", condition);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 学校列表页面
	 *//*
	@RequestMapping("/schoolListInit.action")
	public ModelAndView schoolListInit(String schoolName) {
		ModelAndView result = new ModelAndView("baseinstitution/manager/school_list");
		String userId= WebContext.getSessionUserId();
		ManagerUserEntity entity= managerUserService.searchByUserId(userId);
		String mangerRole="0";
		if(null!=entity || "2542668".equals(userId)) {
			mangerRole="1";
		}else {
			mangerRole="0";
		}
		result.addObject("schoolName", schoolName);
		result.addObject("mangerRole", mangerRole);
		result = getRoleAndNum(result);
		return result;
	}

	*//**
	 * 检索课程 包括分类
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 * directionTagId 学段
	 * categoryTagId 分类
	 *//*
	@RequestMapping("/schoolList.action")
	@ResponseBody
	public ResultBean schoolList(InstitutionEntity course, String directionTagId, String categoryTagId,
			String courseStage, String sortType, Integer pageSize, Integer pageNumber,String schoolStage) {
		ResultBean result = new ResultBean();
		if(StringUtil.isNotBlank(course.getSchoolName())) {
			course.setInstitutionName(course.getSchoolName());
		}
		//区域
		if(StringUtil.isNotBlank(directionTagId)) {
			course.setFirstTag(directionTagId);
		}
		if(StringUtil.isNotBlank(directionTagId)) {
			TagInstitutionEntity entity = new TagInstitutionEntity();
			entity.setTagId(directionTagId);
			entity = tagInstitutionService.searchFirstData(entity);
			String xdName = entity.getTagName();
			course.setInstitutionType(xdName);
		}
		if(StringUtil.isNotBlank(categoryTagId)) {
			TagInstitutionEntity entity = new TagInstitutionEntity();
			entity.setTagId(categoryTagId);
			entity = tagInstitutionService.searchFirstData(entity);
			String xdName = entity.getTagName();
			course.setFirstTag(categoryTagId);
		}
		//学校阶段
		if(StringUtil.isNotBlank(schoolStage)) {
			course.setSchoolNatrue(schoolStage);
		}
		//权限查看
		String userId = WebContext.getSessionUserId();
		ManagerUserEntity entity= managerUserService.searchByUserId(userId);
		//教育局的人员暂时看到全部
		if(null!=entity || "2542668".equals(userId) || "B".equals(WebContext.getSessionUser().getUserKbn())) {
			result = courseService.schoolList(course, directionTagId, categoryTagId, courseStage, sortType,
					pageSize, pageNumber);
		}else {
			String role = WebContext.getSessionUser().getUserKbn();
			course.setCreateUser(userId);
			if("A".equals(role)) {
				result = courseService.schooTeacherlList(course, directionTagId, categoryTagId, courseStage, sortType,
						pageSize, pageNumber);
			}else if("C".equals(role)) {
				result = courseService.schooStudentlList(course, directionTagId, categoryTagId, courseStage, sortType,
						pageSize, pageNumber);
			}
		}
		
		return result;
	}
	*//**
	 * 日志操作页面 检索列表
	 * 
	 * @param condition
	 * @return 返回结果
	 *//*
	@RequestMapping("/logsearch.action")
	@ResponseBody
	public ResultBean logsearch(UserOperEntity condition) {
		return courseService.search4Page(condition);
	}
	*//**
	 * 已完成
	 * @param finishType
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 *//*
	@RequestMapping("/userCourseFinishList.action")
	@ResponseBody
	public ResultBean userCourseFinishList(String finishType, Integer pageSize, Integer pageNumber) {
		finishType ="3";
		pageSize =5;
		pageNumber=1;
		// 检索我的课程
		ResultBean result = courseService.userCourseList(finishType, pageSize, pageNumber);
		return result;
	}
	*//**
	 * 已完成
	 * @param finishType
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 *//*
	@RequestMapping("/userCourseUnFinishList.action")
	@ResponseBody
	public ResultBean userCourseUnFinishList(String finishType, Integer pageSize, Integer pageNumber) {
		pageSize =5;
		pageNumber=1;
		// 检索我的课程
		ResultBean result = courseService.userCourseUnFinishList(finishType, pageSize, pageNumber);
		return result;
	}
    *//**
                  * 作业列表
     * @return
     *//*
	@RequestMapping("/zuoyeList.action")
	@ResponseBody
	public ResultBean zuoyeList() {
		// 检索我的课程
		ResultBean result = courseService.userZyList();
		return result;
	}
    *//**
     * 
                  * 考试结果查看
     * @param UserMeasurementResultEntity condition 入参
     * @return
     *//*
    @RequestMapping("/userExamDetail.action")
    public ModelAndView userExamDetail(UserMeasurementResultEntity condition) {
    	//考试结果页面
        ModelAndView result = new ModelAndView("course/manager/user_exam_detail");
        //考试结果id是否存在
        if(StringUtil.isEmpty(condition.getMeasurementId())){
            throw new BusinessException("参数错误!");
        }
        condition.setCreateUser(WebContext.getSessionUserId());
        //查询考试结果
        List<UserMeasurementResultEntity> list = measuermentService.searchUserMeasurementResultByEntiy(condition);
        if(list != null && list.size() != 0) {
            result.addObject("condition", list.get(0));
        } else {
            result.addObject("condition", new UserMeasurementResultEntity());
        }
        //返回考试结果
        return result;
    }
	*//**
	 * 查询全部考试
	 * @param ChapterEntity chapter章节参数
	 * @param pageSize  每页展示多少条数据
	 * @param pageNumber 页码
	 * @return
	 *//*
	@RequestMapping("/myAllExam.action")
	public ModelAndView myAllExam(ChapterEntity chapter, Integer pageSize, Integer pageNumber) {
		//考试页面内容展示路径
		ModelAndView mv = new ModelAndView("baseinstitution/manager/my_exam_list");
		//查询数据表
		ResultBean result = chapterService.myAllxam(chapter, pageSize, pageNumber);
		mv.addObject("chapter", result.getData());
		return mv;
	}
	*//**
	 *检索考试
	 * @param chapter
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 *//*
	@RequestMapping("/myAllExamList.action")
	@ResponseBody
	public ResultBean myAllExamList(ChapterEntity chapter, Integer pageSize, Integer pageNumber) {
		ResultBean result = chapterService.myAllxam(chapter, pageSize, pageNumber);
		return result;
	}
	*//**
	 * 查询课工厂方法
	 * @param courseId 课程id
	 * @param content 课程内容
	 * @param type 课程类型
	 * @return
	 *//*
	@RequestMapping("/factory.action")
    @ResponseBody
    public ResultBean factory(String courseId,String content,String type)  {
        ResultBean result = new ResultBean();
        result = courseService.factory(courseId,content,type);
        return result;
    }
	*//**
	 * 用户作业列表
	 * @return
	 *//*
	@RequestMapping("/userHomeWork.action")
	public ModelAndView userHomeWork() {
		//作业列表路径
		ModelAndView result = new ModelAndView("/course/client/user_home_work");
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 查询作业列表
	 * @return
	 *//*
	@RequestMapping("/userHomeWorkSearch.action")
	@ResponseBody
	public ResultBean search(){
		//返回结果
		return courseService.searchUserHomeWork4Page();
	}
	*//**
	 * 查询学校信息
	 * @return
	 *//*
	@RequestMapping("/selectSchoolNameInfo.action")
	@ResponseBody
	public ResultBean selectSchoolNameInfo() {
		ResultBean result = new ResultBean();
		//获取登录信息
		String userId = WebContext.getSessionUserId();
		//获取角色信息
		String role = WebContext.getSessionUser().getUserKbn();
		//判断角色
		if("C".equals(role)) {
			//学生角色
			StudentClassInfoEntity entity =new StudentClassInfoEntity();
			entity.setStudentId(userId);
			List<StudentClassInfoEntity> stuList = studentClassInfoService.searchStudentClassInfoList(entity);
			stuList.get(0).setUserId(userId);
			//设置返回值数据
			result.setData(stuList.get(0));
		}else {
			//教师信息
			TpUserEntity tpUserEntity = userManagementService.searchById(userId);
			result.setData(tpUserEntity);
		}
		 
		return result;
	}
	*//**
	 * 结束课程方法
	 * @param id 课程id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/endCourse.action")
	@ResponseBody
	public ResultBean endCourse(String id) throws Exception {
		courseService.endCourse(id);
		return ResultBean.success(MSG_S_SUBMIT);
	}
	*//**
	 * 图书馆课程复制
	 * @param id 课程id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/toLibCourse.action")
	@ResponseBody
	public ResultBean toLibCourse(String id) throws Exception {
		courseService.toLibCourse(id);
		return ResultBean.success(MSG_S_SUBMIT);
	}
	*//**
	 * 课程复制方法
	 * @param id 课程主键
	 * @return
	 *//*
	@RequestMapping("/copyInfo.action")
    @ResponseBody
    public ResultBean copyInfo(String id)  {
        ResultBean result = new ResultBean();
        //课程复制实现方法
        courseService.copyCourseByHtml(id);
        result.setMessages(getMessage(MSG_S_SAVE));
        return result;
    }
    *//**
                 * 新增课程活动记录方法
     * @param courseId 课程id
     * @return
     *//*
	@RequestMapping("/addActiveInit.action")
	public ModelAndView addActiveInit(String courseId) {
	//课程活动新增页面路径
	ModelAndView result = new ModelAndView("course/manager/course_active_add");
	TagView tagQuery = new TagView();
	//一级分类对象
	List<TagView> tags = courseService.searchTagList2(tagQuery);
	//二级分类对象
	List<TagView> tagsSecond = courseService.searchTagSecondList2(tagQuery);
	//查询章节信息
	ChapterEntity entity = new ChapterEntity();
	entity.setCourseId(courseId);
	List<ChapterEntity> chapterList = courseService.selectByCourseId(entity);
	//返回前台对象
	result.addObject("catagoryTags", chapterList);
	//result.addObject("catagoryTags", tags);
	result.addObject("pageType", "add");
	result.addObject("course", new CourseManagerView());
	result.addObject("tagsSecond", tagsSecond);
	result = getRoleAndNum(result);
	return result;
	}
    *//**
                  * 保存课程章节活动记录信息
     * @param courseJson 课程信息
     * @return 返回结果
     * @throws Exception 
     *//*
    @RequestMapping("/saveActiveChapterInfo.action")
    @ResponseBody
    public ResultBean saveActiveChapterInfo(String courseJson) throws Exception {
    	//实例化对象
        CourseManagerView courseview = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, CourseManagerView.class);
    	ChapterActiveEntity course = new ChapterActiveEntity();
    	course.setChapterId(courseview.getChapterId());
    	course.setActiveFlag(courseview.getActiveFlag());
    	//根据章节id查询章节信息
    	ChapterEntity view= new ChapterEntity();
    	String chapterId = courseview.getChapterId();
    	view = chapterService.selectChapteById(chapterId);
    	//设置章节字段进行保存
    	course.setChapterTitle(view.getTitle());
    	course.setContent(courseview.getContent());
        ResultBean result = ResultBean.success();
        //保存章节活动内容
        chapterActiveService.insertInfo(course);
        return result;
    }
   *//**
             * 查询课程章节阶段活动记录信息
    * @param activeFlag
    * @param chapterId
    * @return
    *//*
	@RequestMapping("/searchActiveChapterInfo.action")
	@ResponseBody
	public ResultBean searchActiveChapterInfo(String activeFlag,String chapterId){
	ChapterActiveEntity course = new ChapterActiveEntity();
	//判断活动记录标识是否为空
	if(StringUtil.isNotBlank(activeFlag)) {
		course.setActiveFlag(activeFlag);
	}
	course.setChapterId(chapterId);
	//查询该章节的全部活动记录
	List<ChapterActiveEntity> view = chapterActiveService.searchList(course);
	ResultBean result = ResultBean.success();
	result.setData(view);
	return result;
	}
	*//**
	 * 查询课程阶段活动记录信息
	 * @param activeFlag 活动记录阶段标识
	 * @param courseId  课程主键
	 * @param chapterId  章节主键
	 * @return
	 *//*
	@RequestMapping("/searchActiveAllChapterInfo.action")
	@ResponseBody
	public ResultBean searchActiveAllChapterInfo(String activeFlag,String courseId,String chapterId){
	ChapterActiveEntity course = new ChapterActiveEntity();
	//判断活动记录标识是否为空
	if(StringUtil.isNotBlank(activeFlag)) {
		course.setActiveFlag(activeFlag);
	}
	course.setCourseId(courseId);
	if(StringUtil.isNotBlank(chapterId)) {
		course.setChapterId(chapterId);
	}
	//返回查询结果
	return chapterActiveService.searchAllCourseChapterPage(course);
	}
	*//**
	 * 查询角色的公共方法
	 * @param result
	 * @return
	 *//*
	private ModelAndView getRoleAndNum(ModelAndView result) {
    	int num = courseService.getStuStatus();
    	result.addObject("num", num);
    	UserEntity user = WebContext.getSessionUser();
    	//判断是否管理员
    	ManagerUserEntity entity = managerUserService.searchByUserId(user.getUserId());
    	if(!user.getUserName().equals("guest")) {
    		List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
    		//老师和教育局角色
    		if(roleList.size() > 0 && "A".equals(user.getUserKbn()) && null==entity && !"2542668".equals(user.getUserId())) {
    			for(UserRoleEntity role : roleList) {
    				if(role.getRoleId().equals("01")) {
    					result.addObject("role", "t");
    				}
    			}
    		//教育局角色	
    		}else if(roleList.size() > 0  && (null!=entity || "2542668".equals(user.getUserId()))){
    			result.addObject("role", "m");
    		}else if("B".equals(user.getUserKbn())){
    			result.addObject("role", "e");
    	    //家长和教研员角色
    		}else if("D".equals(user.getUserKbn()) || "E".equals(user.getUserKbn())){
    			result.addObject("role", "q");
    		//学生角色
    		}else {
    			result.addObject("role", "s");
    		}
    	}
        //管理员角色
    	if(null!=entity || "2542668".equals(user.getUserId())) {
    		result.addObject("manager", "1");
    	}else {
    		result.addObject("manager", "0");
    	}
    	return result;
    }
    *//**
     * 活动记录编辑页面方法
     * @param courseId  课程id
     * @param activeId  活动id
     * @return
     *//*
	@RequestMapping("/editActiveInit.action")
	public ModelAndView editActiveInit(String courseId,String activeId) {
	//活动编辑页面路径
	ModelAndView result = new ModelAndView("course/manager/course_active_editor");
	TagView tagQuery = new TagView();
	//查询一级分类方法
	List<TagView> tags = courseService.searchTagList2(tagQuery);
	//查询e二级分类方法
	List<TagView> tagsSecond = courseService.searchTagSecondList2(tagQuery);
	//根据课程id查询所有对应的章节信息
	ChapterEntity entity = new ChapterEntity();
	entity.setCourseId(courseId);
	List<ChapterEntity> chapterList = courseService.selectByCourseId(entity);
	//根据活动记录id查询活动章节信息
	ChapterActiveEntity active = new ChapterActiveEntity();
	active.setId(activeId);
	List<ChapterActiveEntity> activeList = chapterActiveService.searchList(active);
	//设置返回前台的字段信息
	CourseManagerView courseManagerView = new  CourseManagerView();
	courseManagerView.setChapterId(activeList.get(0).getChapterId());
	courseManagerView.setChapterTitle(activeList.get(0).getChapterTitle());
	courseManagerView.setActiveFlag(activeList.get(0).getActiveFlag());
	courseManagerView.setContent(activeList.get(0).getContent());
	//章节对象
	result.addObject("catagoryTags", chapterList);
	//result.addObject("catagoryTags", tags);
	result.addObject("pageType", "add");
	result.addObject("activeId", activeId);
	//课程对象
	result.addObject("course",courseManagerView);
	result.addObject("tagsSecond", tagsSecond);
	result = getRoleAndNum(result);
	return result;
	}
    *//**
                * 保存活动记录课程章节信息
	* @param courseJson 课程信息
	* @return 返回结果
	* @throws Exception 
	*//*
	@RequestMapping("/updateActiveChapterInfo.action")
	@ResponseBody
	public ResultBean updateActiveChapterInfo(String courseJson) throws Exception {
    //转化实例对象方法
	CourseManagerView courseview = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, CourseManagerView.class);
	ChapterActiveEntity course = new ChapterActiveEntity();
	//删除当前活动记录
	ChapterActiveEntity entity = new ChapterActiveEntity();
	String id = courseview.getActiveId();
	entity.setId(id);
	chapterActiveService.deleteInfo(entity);
	//设置保存课程的信息字段
	course.setChapterId(courseview.getChapterId());
	course.setActiveFlag(courseview.getActiveFlag());
	//查询章节信息
	ChapterEntity view= new ChapterEntity();
	String chapterId = courseview.getChapterId();
	view = chapterService.selectChapteById(chapterId);
	course.setChapterTitle(view.getTitle());
	course.setContent(courseview.getContent());
	ResultBean result = ResultBean.success();
	//保存活动记录的课程章节信息
	chapterActiveService.insertInfo(course);
	return result;
	}
    *//**
                 * 删除活动
	* @param courseJson 课程信息
	* @return 返回结果
	* @throws Exception 
	*//*
	@RequestMapping("/activeDelete.action")
	@ResponseBody
	public ResultBean activeDelete(String id) throws Exception {
	ChapterActiveEntity entity = new ChapterActiveEntity();
	//设置活动记录主键
	entity.setId(id);
	chapterActiveService.deleteInfo(entity);
	ResultBean result = ResultBean.success();
	return result;
	}
	*//**
	 * 添加页面初始化：节
	 * 
	 * @param courseId 课程ID
	 * @return
	 *//*
	@RequestMapping("/courseSmallInit.action")
	public ModelAndView courseSmallInit(String courseId, String parentId) {
		ModelAndView result = new ModelAndView("course/manager/course_three_edit");
		ChapterView chapter = new ChapterView();
		chapter.setMeasureType("4");
		result.addObject("courseId", courseId);
		result.addObject("parentId", parentId);
		result.addObject("chapterFlag", SMALL_CHAPTER);
		result.addObject("chapter", chapter);
		return result;
	}
	*//**
	 * 保存章信息
	 * 
	 * @param chapterJson
	 * @return 返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/saveExamInfo.action")
	@ResponseBody
	public ResultBean saveExamInfo(String chapterJson, String videoLength) throws Exception {
		//将字符串转化成章节对象
		ChapterView chapter = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateMinuteFormatDefualt()),
				chapterJson, ChapterView.class);
		ResultBean result = ResultBean.success();
		//设置章节状态0未发布，1已发布
		chapter.setStatus("1");
		//设置加入课程1是已加入
		chapter.setIsCourse("1");
		if (StringUtil.isEmpty(chapter.getId())) {
			//未存在的章节考试直接保存
			chapterService.insertChapterInfoAll(chapter, videoLength);
		} else {
			//已存在的章节考试更新信息
			chapterService.updateChapterAll(chapter, videoLength);
		}
		//返回信息
		return result;
	}
    *//**
     * 
                  * 查看考试结果
     * @param condition
     * @return
     *//*
    @SuppressWarnings({ "unchecked"})
    @RequestMapping("/userExamInfoInit.action")
    public ModelAndView userExamInfoInit(MeasurementEntity condition) {
        ModelAndView result = new ModelAndView("course/manager/user_exam_info");
        //参数为空抛出异常
        if(StringUtil.isEmpty(condition.getLinkId())){
            throw new BusinessException("参数错误!");
        }
        //根据参数查询考试结果
        List<MeasurementEntity> list = courseService.search(condition);
        if(list != null && list.size() != 0) {
            result.addObject("condition", list.get(0));
        } else {
            throw new BusinessException("参数错误!");
        }
        //返回考试信息
        return result;
    }
    *//**
                 * 根据课程id获取章节考试状态信息
     * @param courseId
     * @return
     * @throws Exception
     *//*
	@RequestMapping("/searchExamStatus.action")
	@ResponseBody
	public ChapterView searchExamStatus(String courseId) throws Exception {
		//查询章节信息
		ChapterView view = chapterService.seacrhExamStatus(courseId);
		return view;
	}
	@SuppressWarnings("unchecked")
	*//**
	* 前端请求-章节详情
	* @param chapterId 章节ID
	* @return 
	*//*
	@RequestMapping("/toChapterDetail.action")
	public ModelAndView toChapterDetail(String courseId,String chapterId){
		ModelAndView mv = new ModelAndView("course/client/course_chapter_detail");
		ResultBean result = courseService.searchCourseDetail(courseId);
		CourseView courseView = (CourseView) result.getData();
		mv.addObject("courseView", courseView);
		mv.addObject("note", new NoteDataEntity());
		mv.addObject("homeWork", new HomeworkView());
		result = chapterService.chapterDetail(courseId, chapterId);
		ChapterView chapterView = (ChapterView) result.getData();
		ChapterEntity entity = new ChapterEntity();
		entity.setCourseId(courseId);
		List<ChapterEntity> chapterCourseList = courseService.selectByCourseId(entity);
		mv.addObject("chapterCourseList", chapterCourseList);
		mv.addObject("chapterView", chapterView);
		//检索章节列表
		List<ChapterView> cvs = (List<ChapterView>) chapterService.chapterList(courseId);
		mv.addObject("chapterList", cvs);
		//相关课程
		result = courseService.recommendedCourses("5", courseId);
		List<CourseEntity> otherCourseList = (List<CourseEntity>) result.getData();
		mv.addObject("otherCourseList", otherCourseList);
		//判断是否需要检索测试信息
		if(CHAPTER_TYPE_MEASUERMENT.equals(chapterView.getMeasureType().toString())){
			result = measuermentService.searchMeasuerment(chapterId, LINK_TYPE_CHAPTER);
			List<QuestionEntity> questionList = (List<QuestionEntity>) result.getData();
			mv.addObject("questionList", questionList);
		}
		//判断是否考试章节
		if(CHAPTER_TYPE_EXAM.equals(chapterView.getMeasureType())){
			//考试章节
			//取用户对于章节的考试信息
			MeasurementEntity me = new MeasurementEntity();
			me.setLinkId(chapterId);
			me.setLinkType(LINK_TYPE_CHAPTER);
			me = measuermentService.findMeasurement(me);
			if(me!=null){
				UserMeasurementResultEntity um = new UserMeasurementResultEntity();
				um.setMeasurementId(me.getId());
				um.setUserId(WebContext.getSessionUserId());
				um = measuermentService.findUserExamResult(um);
				if(um != null && um.getExamStatus()!=null && um.getExamStatus().equals(EXAM_STATUS_END)){
					//已参加过考试提交了
					mv.addObject("goal", um.getGoal()+"");
					mv.addObject("totalGoal", um.getTotalGoal()+"");
					mv.addObject("examTime", DateUtil.formatAny(um.getCreateDate(), ConfigUtil.getDateTimeFormatDefualt()));
				}
			}
		}
		//是否教师
		mv.addObject("isTeacher", courseService.isTeacher(courseView.getTeacherId()));
		mv.addObject("useingChapterId", chapterId);
		String content = chapterView.getContent();
		if(content != null && !"".equals(content)) {
			String ipAndPort = getIpAndPort(content);
			if(!"".equals(ipAndPort)) {
				String serverName = WebContext.getRequest().getServerName();
				int serverPort = WebContext.getRequest().getServerPort();
				content = content.replace(ipAndPort, serverName+":"+serverPort);
				chapterView.setContent(content);
			}
		}
		
		return mv;
	}
	*//**
	 * 判断str中是否有IP和port，若有则取出返回
	 * @param str
	 * @return String
	 *//*
	private static String getIpAndPort(String str) {
		String returnStr = "";
		Pattern p = Pattern.compile("(\\d+\\.\\d+\\.\\d+\\.\\d+)\\:(\\d+)");
		Matcher m = p.matcher(str);
		if(m.find()) {
			returnStr += m.group(1)+":"+m.group(2);
		}
		
		return returnStr;
	}
	*//**
	 * 标签一览
	 * @param flag 是否显示标签 0 不显示 1 显示
	 * @param businessType 业务类型
	 * @return
	 *//*
	@RequestMapping("/searchSchhoolCategory.action")
	@ResponseBody
	public ResultBean searchSchhoolCategory(String flag,String businessType){
		ResultBean  result=tagService.searchSchhoolCategory(flag,businessType);
		return result;
	}
	*//**
	 * 
	 * 初始化待办课程列表
	 * 
	 * @param condition 课程对象
	 * @return 返回课程列表初始化页面
	 *//*
	@RequestMapping("/institutionDbListInit.action")
	public ModelAndView institutionDbListInit(InstitutionMessageEntity condition) {
		//设置代办页面
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_db_list");
		//设置分页参数
		condition.setPageSize(10);
		result.addObject("condition", condition);
		TagInstitutionView tagQuery = new TagInstitutionView();
		//一级标签信息查询
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		//二级标签信息查询
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		//返回课程实例对象
		result.addObject("course", new InstitutionMessageEntity());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}
	*//**
	 * 
	 * 初始化已办课程列表
	 * 
	 * @param condition 课程对象
	 * @return 返回课程列表初始化页面
	 *//*
	@RequestMapping("/institutionYbListInit.action")
	public ModelAndView institutionYbListInit(InstitutionMessageEntity condition) {
		//设置已办页面
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_yb_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		TagInstitutionView tagQuery = new TagInstitutionView();
		//一级标签信息查询
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		//二级标签信息查询
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		//返回课程实例对象
		result.addObject("course", new InstitutionMessageEntity());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}
	*//**
	 * 
	 * 检索评价列表
	 * 
	 * @param queryInfo 机构对象
	 * @return 返回评价列表
	 *//*
	@RequestMapping("/listdbSearch.action")
	@ResponseBody
	public ResultBean listdbSearch(MeasurementInstitutionEntity queryInfo) {
		UserEntity user = WebContext.getSessionUser();	
		//获取人员角色
		String role = user.getUserKbn();
		//获取登录人账号
		String userId = user.getCreateUser();
		//教师角色
		if("A".equals(role)) {
			TpUserEntity tpUserEntity =new TpUserEntity();
			//查询教师信息
			tpUserEntity = userManagementService.searchById(userId);
		    if(null!=tpUserEntity) {
		    	queryInfo.setInstitutionName(tpUserEntity.getSchoolName());
		    }
		    //学生角色
		}else if("C".equals(role)) {
			//查询学生信息
			StudentClassInfoEntity classInfo = new StudentClassInfoEntity();
			classInfo.setStudentId(userId);
			classInfo = studentClassInfoService.searchStudentClassInfo(classInfo);
			if(null!=classInfo && StringUtil.isNotBlank(classInfo.getSchoolName())) {
				queryInfo.setInstitutionName(classInfo.getSchoolName());
			}
		}
		
		return taxonomyInstitutionService.listdbSearch(queryInfo);
	}
	*//**
	 * 
	 * 检索已办评价列表
	 * 
	 * @param queryInfo 机构对象
	 * @return 返回评价列表
	 *//*
	@RequestMapping("/listybSearch.action")
	@ResponseBody
	public ResultBean listybSearch(MeasurementInstitutionEntity queryInfo) {
		UserEntity user = WebContext.getSessionUser();	
		//获取人员角色
		String role = user.getUserKbn();
		//登录账号
		String userId = user.getCreateUser();
		//教师角色
	    if("A".equals(role)) {
	    	//查询用户信息
	    	TpUserEntity tpUserEntity =new TpUserEntity();
			tpUserEntity = userManagementService.searchById(userId);
			if(null!=tpUserEntity) {
				//设置学校名称
				queryInfo.setInstitutionName(tpUserEntity.getSchoolName());
			}
	    }else if("C".equals(role)) {
	    	//查询学生信息
			StudentClassInfoEntity classInfo = new StudentClassInfoEntity();
			classInfo.setStudentId(userId);
			classInfo = studentClassInfoService.searchStudentClassInfo(classInfo);
			if(null!=classInfo && StringUtil.isNotBlank(classInfo.getSchoolName())) {
				//设置学生学校名称
				queryInfo.setInstitutionName(classInfo.getSchoolName());
			}
		}
	    //返回查询结果列表
		return taxonomyInstitutionService.listybSearch(queryInfo);
	}
	*//**
	 * 管理人员基本信息页面
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/managerUserList.action")
	public ModelAndView managerUserList(ManagerUserEntity condition) {
		//管理员信息页面展示路径
		ModelAndView result = new ModelAndView("/baseinstitution/manager/base_manager_user_list");
		//设置每页展示10条数据
		condition.setPageSize(10);
        result.addObject("course", new CourseManagerView());
		result.addObject("condition", condition);
		result = getRoleAndNum(result);
		//返回页面所需信息结果
		return result;
	}
	*//**
	 * 管理者信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchManagerUserList.action")
	@ResponseBody
	public ResultBean searchManagerUserList(ManagerUserEntity entity) {
		//查询管理员数据表全部信息
		return managerUserService.searchPage(entity);
	}
	*//**
	 * 搜素全部人员
	 * 
	 * @param query
	 * @return
	 *//*
	@RequestMapping("/searchUserAll.action")
	@ResponseBody
	public ResultBean searchUserAll(TpUserEntity query) {
		if(null!=query && StringUtil.isNotBlank(query.getUsrName())) {
			query.setUsrName(query.getUsrName().trim());
		}
		//根据用户名查询人员信息表数据
		return userManagementService.searchManagerUserAll(query);
	}
	*//**
	 * 保存超级管理员信息
	 * @param userId 添加人员账号id
	 * @param userName  人员名称
	 * @param personName 名称
	 * @param role 角色
	 * @return
	 *//*
	@RequestMapping("/saveManagerUser.action")
	@ResponseBody
	public ResultBean saveManagerUser(String userId,String userName,String personName,String role) {
		ResultBean result = new ResultBean();
		//查询管理员表中是否已存在该成员
		ManagerUserEntity entity = managerUserService.searchByUserId(userId);
		//存在则返回
		if(null!=entity) {
			result = ResultBean.error();
		}else {
			//不存在则保存到数据表里
			ManagerUserEntity managerUserEntity = new ManagerUserEntity();
			managerUserEntity.setUserId(userId);
			managerUserEntity.setUserName(userName);
			managerUserEntity.setPersonName(personName);
			managerUserEntity.setRole(role);
			managerUserEntity.setCreateDate(new Date());
			//设置该条信息的创建人
			managerUserEntity.setCreateUser(WebContext.getSessionUserId());
			managerUserService.insert(managerUserEntity);
			//保存成功返回信息
			result = ResultBean.success();
		}
		
		return result;
	}
	*//**
	 * 删除管理员
	 * @param id 员工账号
	 * @return  返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/deleteManagerById.action")
	@ResponseBody
	public ResultBean deleteManagerById(String id) throws Exception {
		//根据员工账号在tb_manager_user表里删除其信息
		ManagerUserEntity entity = new ManagerUserEntity();
		entity.setUserId(id);
		managerUserService.delete(entity);
		return ResultBean.success(DELETE_GRADE_SUCCESS);
	}
	*//**
	 * 查询管理信息
	 * 
	 * @param 员工账号
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchByUserId.action")
	@ResponseBody
	public ManagerUserEntity searchByUserId(String id) throws Exception {
		//返回查询结果
		return managerUserService.searchByUserId(id);
	}
	*//**
	 *  作业列表页面
	 * @param homeworkView 入参
	 * @return 返回结果
	 *//*
	@RequestMapping("/zuoyeManager.action")
	public ModelAndView zuoyeManager(HomeworkView homeworkView) {
		//列表页面路径
		ModelAndView result = new ModelAndView("/course/manager/homework_list");
		//返回作业信息对象给前台
		result.addObject("homeworkView", homeworkView);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 *  查询作业列表结果方法
	 * @param homeworkView 前台传入后台参数
	 * @return  返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/zuoyelistSearch.action")
	@ResponseBody
	public ResultBean zuoyelistSearch(HomeworkView homeworkView) throws Exception {
		ResultBean result =new ResultBean();
		//获取登录信息对象
		UserEntity user = WebContext.getSessionUser();
    	//判断是否管理员
    	ManagerUserEntity entity = managerUserService.searchByUserId(user.getUserId());
    	if(null!=entity) {
    		//管理员返回的结果
    		result= courseVRService.seacherHomeworkPage(homeworkView);
    	}else {
    		//普通教师查询返回的结果
    		homeworkView.setTeacherId(user.getUserId());
    		result= courseVRService.seacherHomeworkPage(homeworkView);
    	}
		return result;
	}
	*//**
	 *  根据作业id查询作业列表
	 * @param id 作业主键
	 * @param title  作业标题
	 * @return  返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/zuoyelistById.action")
	public ModelAndView zuoyelistById(String id,String title) throws Exception {
		//作业列表路径
		ModelAndView result = new ModelAndView("/course/manager/homework_user_list");
		HomeworkView homeworkView = new HomeworkView();
		homeworkView.setId(id);
		homeworkView.setTitle(title);
		//作业实例化对象
		result.addObject("homeworkView", homeworkView);
		//作业主键
		result.addObject("id", id);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 预览作业页面方法
	 * @param id  作业主键
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/homeWorkById.action")
	public ModelAndView homeWorkById(String id) throws Exception {
		//预览页面路径
		ModelAndView result = new ModelAndView("/course/client/home_work_teacher_review");
		HomeworkView view = courseService.getHomeWorkById(id);
		//设置创建时间返回前台
		if(null!=view.getCreateDate()) {
			view.setCreateTime(sdf.format(view.getCreateDate()));
		}
        //返回作业实体类对象
		result.addObject("homeWork", view);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 查询作业列表
	 * @param id 作业id集合字符串
	 * @param homeworkView  作业实体类
	 * @param linkId 课程主键
	 * @param linkType  课程/章节类型
	 * @return
	 *//*
	@RequestMapping("/zuoyelistByIdSearch.action")
	@ResponseBody
	public ResultBean zuoyelistByIdSearch(String id,HomeworkView homeworkView,String linkId,String linkType) {
		// 将前台汇总的作业id拆分
		if(StringUtil.isNotBlank(id)) {
			String ids[]=id.split(",");
			homeworkView.setId(ids[0]);
		}else {
			homeworkView.setLinkId(linkId);
			homeworkView.setLinkType(linkType);
			HomeworkEntity homeWork = courseService.getHomeWorkByLinkId(homeworkView);
			if(null!=homeWork) {
				//获取作业主键
				homeworkView.setId(homeWork.getId());
				//获取作业标题
				homeworkView.setTitle(homeWork.getTitle());
			}
		}
		//查询作业信息
		return courseVRService.seacherHomeworkUserPage(homeworkView);
	}
    *//**
                 * 保存作业信息
     * @param courseJson 作业信息
     * @return 返回结果
     * @throws Exception 
     *//*
    @RequestMapping("/saveZyInfo.action")
    @ResponseBody
    public ResultBean saveZyInfo(String courseJson) throws Exception {
    	//将字符串转化成实例化对象
    	HomeworkView course = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, HomeworkView.class);
        ResultBean result = ResultBean.success();
        String homeworkId=course.getId();
        //获取账号id
        String userId = WebContext.getSessionUserId();
        //查询作业数据表
        HomeworkAnswerEntity entity = new  HomeworkAnswerEntity();
        entity.setHomeworkId(homeworkId);
        entity.setCreateUser(userId);
        entity = courseVRService.selectOneAnswer(entity);
        if(null==entity) {
        	//作业作答数据表信息为空，插入数据信息
        	course.setId(IDGenerator.genUID());
        	course.setCreateUser(userId);
        	course.setCreateDate(new Date());
        	//设置学生提交的信息
        	course.setStudentComment(course.getContent());
        	course.setHomeworkId(homeworkId);
        	course.setAnswerUser(userId);
        	//设置作业状态
        	course.setStatus(HOMEWORK_ANSWER_STATUS_ONE);
        	courseVRService.insertAnswer(course);
        }else {
        	//作业作答数据表信息不为空，更新数据信息
        	entity.setStudentComment(course.getContent());
        	entity.setUpdateDate(new Date());
        	entity.setUpdateUser(userId);
        	courseVRService.update(entity);
        }
        return result;
    }
	*//**
	 * 学生作业列表页面
	 * @param HomeworkView homeworkView 
	 * @return
	 *//*
	@RequestMapping("/studentZuoyePage.action")
	public ModelAndView studentZuoyePage(HomeworkView homeworkView) {
		//学生作业列表页面路径
		ModelAndView result = new ModelAndView("/course/manager/homework_student_list");
		//作业实例对象实体类
		result.addObject("homeworkView", homeworkView);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 查询作业列表
	 * 
	 * @param HomeworkView homeworkView 入参
	 * @return 返回结果
	 *//*
	@RequestMapping("/studentZuoyelistSearch.action")
	@ResponseBody
	public ResultBean studentZuoyelistSearch(HomeworkView homeworkView) {
		homeworkView.setCreateUser(WebContext.getSessionUserId());
		//根据账号id获取作业对象信息
		return courseVRService.seacherStudentHomeworkUserPage(homeworkView);
	}
    *//**
                   * 编辑作业
     * @param HomeworkView home 作业实例对象入参 
     * @return
     *//*
    @RequestMapping("/studentTaskInit.action")
    public ModelAndView studentTaskInit(HomeworkView home){
    	//作业编辑页面
        ModelAndView result = new ModelAndView("course/manager/home_student_work");
        //获取当前登录者账号
        UserEntity user = WebContext.getSessionUser();
        home.setCreateUser(user.getUserId());
        HomeworkView homework=courseVRService.searchStudentHomeWork(home);
        //前台页面信息回显对象
        result.addObject("homework", homework);
        result = getRoleAndNum(result);
        return result;
    }
    *//**
                  * 保存作业信息
     * @param id  作业id
     * @param studentComment 学生提交内容
     * @param homeanwserId 作业作答id
     * @param status 提交作业状态
     * @return
     * @throws Exception
     *//*
    @RequestMapping("/saveMystudy.action")
    @ResponseBody
    public ResultBean saveMystudy(String id,String studentComment,String homeanwserId,String status) throws Exception {
        ResultBean result = ResultBean.success();
        String userId = WebContext.getSessionUserId();
        //根据homeanwserId判断当前登陆者是否已经作答过改作业
        if(StringUtil.isBlank(homeanwserId)) {
        	//未作答，保存作答内容
        	HomeworkView course = new HomeworkView();
        	//随机生成作业作答id
        	course.setId(IDGenerator.genUID());
        	course.setCreateUser(userId);
        	course.setCreateDate(new Date());
        	//作业作答内容
        	course.setStudentComment(studentComment);
        	course.setHomeworkId(id);
        	course.setAnswerUser(userId);
        	//设置作业状态
        	course.setStatus(HOMEWORK_ANSWER_STATUS_ONE);
        	courseVRService.insertAnswer(course);
        }else {
        	//更新做大内容
        	HomeworkAnswerEntity entity = new HomeworkAnswerEntity();
        	entity.setId(homeanwserId);
        	entity = courseVRService.selectOneAnswer(entity);
        	entity.setStudentComment(studentComment);
        	entity.setUpdateDate(new Date());
        	entity.setUpdateUser(userId);
        	courseVRService.update(entity);
        }
        return result;
    }
    *//**
                 * 学生查看作业列表方法
     * @param home
     * @return
     *//*
    @RequestMapping("/studentSeacherTaskInit.action")
    public ModelAndView studentSeacherTaskInit(HomeworkView home){
    	//学生作业列表页面路径
        ModelAndView result = new ModelAndView("course/manager/home_student_look_work");
        //根据登录id查询作业信息
        UserEntity user = WebContext.getSessionUser();
        home.setCreateUser(user.getUserId());
        HomeworkView homework=courseVRService.searchStudentHomeWork(home);
        //作业信息返回给前台
        result.addObject("homework", homework);
        result = getRoleAndNum(result);
        return result;
    }
    *//**
                  * 查询年级 课程
     * @param id 课程id
     * @return
     *//*
    @RequestMapping("/searchGrade.action")
    @ResponseBody
    public List<StudentClassInfoEntity> searchGrade(String id){
    	//根据课程id查询课程信息
        CourseEntity entity = new CourseEntity();
        entity.setId(id);
        entity = courseService.searchCourse(entity);
        //获取老师所属学校
        String schoolId = entity.getSchoolId();
        StudentClassInfoEntity studentClassInfoEntity = new StudentClassInfoEntity();
        studentClassInfoEntity.setSchoolId(schoolId);
        //根据学校查询年级
        List<StudentClassInfoEntity> gradeList = new ArrayList<StudentClassInfoEntity>();
        gradeList = studentClassInfoService.searchStudentGradeList(studentClassInfoEntity);
        //返回查询的年级集合给前台
        return gradeList;
    }
     *//**
                     * 根据课程主键和年级主键查询班级信息
      * @param courseId  课程主键
      * @param gradeId 年级主键
      * @return
      *//*
    @RequestMapping("/searchClass.action")
    @ResponseBody
    public List<StudentClassInfoEntity> searchClass(String courseId,String gradeId){
    	//根据课程主键查询课程信息
    	 CourseEntity entity = new CourseEntity();
         entity.setId(courseId);
         entity = courseService.searchCourse(entity);
         //获取老师所属学校
         String schoolId = entity.getSchoolId();
        List<StudentClassInfoEntity> gradeList = new ArrayList<StudentClassInfoEntity>();
        //根据学校主键和年级查询班级信息
        StudentClassInfoEntity studentClassInfoEntity = new StudentClassInfoEntity();
        studentClassInfoEntity.setSchoolId(schoolId);//学校id
        studentClassInfoEntity.setGradeId(gradeId);//年级id
        gradeList = studentClassInfoService.searchStudentClassList(studentClassInfoEntity);
        return gradeList;
    }
    *//**
                  * 添加学生时查询学生列表
     * @param UserView query 用户参数 
     * @param courseId 所属课程信息id
     * @return
     *//*
    @RequestMapping("/searchStudentUserAll.action")
    @ResponseBody
    public ResultBean searchStudentUserAll(UserView query,String courseId)  {
    	//根据课程id查询课程所属老师对应的学校信息
    	CourseEntity entity = new CourseEntity();
        entity.setId(courseId);
        entity = courseService.searchCourse(entity);
        //获取老师所属学校
        String schoolId = entity.getSchoolId();
        //根据学校id获取这个学校下面所有的学生信息
        query.setSchoolId(schoolId);
        return courseService.searchStudentUserAll(query);
    }
	*//**
	 * 用户个人中心信息页面方法
	 * @return 返回结果
	 *//*
	@RequestMapping("/userPblInfo.action")
	public ModelAndView userPblInfo(){
		//查询代办信息表里审核通过的代办信息数量
		int num = courseService.getStuStatus();
		//个人中心页面路径参数
		ModelAndView mv = new ModelAndView("trainingplatform/client/center/center_pbl_user");
		//根据用户id查询用户信息
		UserEntity user = userManagementService.seachUserInfo(WebContext.getSessionUserId());
		//返回用户对象
		mv.addObject("user", user);
		mv.addObject("num", num);
		//获取登录人员的角色信息
		UserEntity userRole = WebContext.getSessionUser();
		//判断用户是否登录状态guest为未登录状态
		if(!userRole.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(userRole.getUserId());
			if(roleList.size() > 0) {
				//循环遍历人员角色
				for(UserRoleEntity role : roleList) {
					//角色包含01的设置role字段为t,即老师角色
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				//s为学生角色
				mv.addObject("role", "s");
			}
		}
		UserEntity user1 = WebContext.getSessionUser();
		//获取工号
		String uid =user1.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		    //获取人员信息
		        //设置头像字段
				String image = "";
				//设置头像字段
				String school="";
				//设置年级字段
				String grade="";
				//设置班级字段
				String className="";
				if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
					//获取头像
					image = userEntity.getHeadurl();
					//获取学校
					school = userEntity.getSchoolName();
				}else {
					//设置默认头像
					image ="http://wisdomsaas-img.oss-cn-beijing.aliyuncs.com/BFAA0FECDD194B7A974C60B7E1B27C18?authorization=bce-auth-v1%2F7642b4b48046452697e90662eed41ace%2F2017-12-29T08%3A33%3A39Z%2F-1%2F%2F0f06024af14dad05694f25b8c85572567591c72bbd0129d3175b4cdbd6e62208";
				}
				//获取班级
				if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
					//实例化学生对象实体类并查询学生信息
					List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
					StudentClassInfoEntity entity = new StudentClassInfoEntity();
					entity.setStudentId(uid);
					entityList = studentClassInfoService.searchStudentClassInfoList(entity);
					//如果学生对象不为空则执行如下赋值
					if(null!=entityList && entityList.size()>0) {
						grade = entityList.get(0).getGradeName();
						className = entityList.get(0).getClassName();
						school =  entityList.get(0).getSchoolName();
					}
				}
				//返回班级信息
				mv.addObject("className", className);
				//向页面返回年级信息
				mv.addObject("grade", grade);
				//返回学生所属学校名称信息
				mv.addObject("school", school);
		       return mv;
	}
	*//**
	 * 批量点评初始化页面方法
	 * @param userIds  学生id集合字符串
	 * @param personNames  学生姓名字符集合
	 * @param id      作业id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/allEvalutionMsg.action")
	public ModelAndView allEvalutionMsg(String userIds,String personNames,String id) throws Exception {
		//批量点评方法
		ModelAndView result = new ModelAndView("/course/client/home_work_teacher_all_review");
		String ids[]=id.split(",");
		HomeworkView view = courseService.getHomeWorkById(ids[0]);
		//判断作业对象返回的时间是否为空，不为空则赋值给新的字段用于前端展示
		if(null!=view.getCreateDate()) {
			if(StringUtil.isNotBlank(sdf.format(view.getCreateDate()))) {
				view.setCreateTime(sdf.format(view.getCreateDate()));
			}
		}
		//收集作业对象
		result.addObject("homeWork", view);
		//收集作业id
		result.addObject("id", id);
		//收集学生账号集合id
		result.addObject("userIds", userIds);
		//收集学生姓名集合
		result.addObject("personNames", personNames);
		//收集作业答案id
		result.addObject("answerIds", id);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 批量点评保存
	 * @param courseJson 前台返回的json字符串
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveAllHomeWorkReviewById.action")
	@ResponseBody
	public ResultBean saveAllHomeWorkReviewById(String courseJson) throws Exception {
		//将字符串转化成实例化对象
		HomeworkView homework = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, HomeworkView.class);
		return courseService.saveAllHomeWorkReviewById(homework);
	}
	*//**
	 * 一键催交
	 * @param userIds  学生id集合字符串参数
	 * @param courseId 课程id
	 * @param homeworkId  作业id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveCjById.action")
	@ResponseBody
	public ResultBean saveCjById(String userIds,String courseId,String homeworkId) throws Exception {
		return courseService.saveACjById(userIds,courseId,homeworkId);
	}
	*//**
	 * 课程考试调用java调查网接口
	 * @param courseId  课程id
	 * @param userId  用户id
	 * @return
	 * @throws ParseException
	 *//*
	@RequestMapping("/searchExamByCourseIdList.action")
	@ResponseBody
	public ResultBean searchExamByCourseIdList(String courseId,String userId) throws ParseException {
		ResultBean result = new ResultBean();
		String param = courseId;
		//声明调用java调查网的webservice接口
		ExamToWebserviceProxy proxy = new ExamToWebserviceProxy();
		String out;
		try {
			//调用外部接口方法
			out = proxy.examList(param,1,20,userId);
			JSONArray jsonArray = new JSONArray(); 
			//将接口返回值转化成数组格式
			jsonArray = jsonArray.fromObject(out);
			UserCourseEntity uc = new UserCourseEntity();
			uc.setCourseId(courseId);
			uc.setUserId(WebContext.getSessionUserId());
			// 检索用户是否存在
			UserCourseEntity uu = courseService.searchOneCourseIdData(uc);
			//根据课程id获取该课程的老师id
			CourseEntity courseEntity = courseService.getCourseId(courseId);
			//转化对应的实体类
			@SuppressWarnings("unchecked")
			List<Questionnaire> list = jsonArray.toList(jsonArray, Questionnaire.class);
			if(null!=list && list.size()>0) {
				//循环遍历集合将字段赋值
				for(int i=0;i<list.size();i++) {
					//已加入的学员或者这个课程的老师创建者都可以看到这个
					if(null!=uu ||(null!=courseEntity && WebContext.getSessionUserId().equals(courseEntity.getTeacherId()))) {
						//已加入课程
						list.get(i).setStudentFlag("0");
						if (list.get(i).getEndDate() != null ) {
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Long end = sdf.parse(list.get(i).getEndDateStr()).getTime();
							Long nowtime = new Date().getTime();
							//判断问卷/考试是否已结束
								if(end-nowtime<0) {
									list.get(i).setExamStatusText("问卷/考试已结束");
									list.get(i).setExamStatus("1");	
								}					
							}
						if (list.get(i).getStartDate() != null ) {
							//将字符串转化成时间再获取秒数
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Long start = sdf.parse(list.get(i).getStartDateStr()).getTime();
							Long nowtime = new Date().getTime();
							//判断问卷/考试是否开始
								if(start-nowtime>0) {
									list.get(i).setExamStatusText("问卷/考试未开始");
									list.get(i).setExamStatus("1");
								}
							}
					}else {
						list.get(i).setStudentFlag("1");//未加入课程
						list.get(i).setExamStatusText("未加入课程");
					}
					
				}
			}
			 result.setData(list);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
			*//**
	 * 查询作业列表
	 * @param id 作业id集合字符串
	 * @param homeworkView  作业实体类
	 * @param linkId 课程主键
	 * @param linkType  课程/章节类型
	 * @return
	 *//*
	@RequestMapping("/zuoyelistByIdSearch.action")
	@ResponseBody
	public ResultBean zuoyelistByIdSearch(String id,HomeworkView homeworkView,String linkId,String linkType) {
		// 将前台汇总的作业id拆分
		if(StringUtil.isNotBlank(id)) {
			String ids[]=id.split(",");
			homeworkView.setId(ids[0]);
		}else {
			homeworkView.setLinkId(linkId);
			homeworkView.setLinkType(linkType);
			HomeworkEntity homeWork = courseService.getHomeWorkByLinkId(homeworkView);
			if(null!=homeWork) {
				//获取作业主键
				homeworkView.setId(homeWork.getId());
				//获取作业标题
				homeworkView.setTitle(homeWork.getTitle());
			}
		}
		//查询作业信息
		return courseVRService.seacherHomeworkUserPage(homeworkView);
	}
    *//**
                 * 保存作业信息
     * @param courseJson 作业信息
     * @return 返回结果
     * @throws Exception 
     *//*
    @RequestMapping("/saveZyInfo.action")
    @ResponseBody
    public ResultBean saveZyInfo(String courseJson) throws Exception {
    	//将字符串转化成实例化对象
    	HomeworkView course = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, HomeworkView.class);
        ResultBean result = ResultBean.success();
        String homeworkId=course.getId();
        //获取账号id
        String userId = WebContext.getSessionUserId();
        //查询作业数据表
        HomeworkAnswerEntity entity = new  HomeworkAnswerEntity();
        entity.setHomeworkId(homeworkId);
        entity.setCreateUser(userId);
        entity = courseVRService.selectOneAnswer(entity);
        if(null==entity) {
        	//作业作答数据表信息为空，插入数据信息
        	course.setId(IDGenerator.genUID());
        	course.setCreateUser(userId);
        	course.setCreateDate(new Date());
        	//设置学生提交的信息
        	course.setStudentComment(course.getContent());
        	course.setHomeworkId(homeworkId);
        	course.setAnswerUser(userId);
        	//设置作业状态
        	course.setStatus(HOMEWORK_ANSWER_STATUS_ONE);
        	courseVRService.insertAnswer(course);
        }else {
        	//作业作答数据表信息不为空，更新数据信息
        	entity.setStudentComment(course.getContent());
        	entity.setUpdateDate(new Date());
        	entity.setUpdateUser(userId);
        	courseVRService.update(entity);
        }
        return result;
    }
	*//**
	 * 学生作业列表页面
	 * @param HomeworkView homeworkView 
	 * @return
	 *//*
	@RequestMapping("/studentZuoyePage.action")
	public ModelAndView studentZuoyePage(HomeworkView homeworkView) {
		//学生作业列表页面路径
		ModelAndView result = new ModelAndView("/course/manager/homework_student_list");
		//作业实例对象实体类
		result.addObject("homeworkView", homeworkView);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 查询作业列表
	 * 
	 * @param HomeworkView homeworkView 入参
	 * @return 返回结果
	 *//*
	@RequestMapping("/studentZuoyelistSearch.action")
	@ResponseBody
	public ResultBean studentZuoyelistSearch(HomeworkView homeworkView) {
		homeworkView.setCreateUser(WebContext.getSessionUserId());
		//根据账号id获取作业对象信息
		return courseVRService.seacherStudentHomeworkUserPage(homeworkView);
	}
    *//**
                   * 编辑作业
     * @param HomeworkView home 作业实例对象入参 
     * @return
     *//*
    @RequestMapping("/studentTaskInit.action")
    public ModelAndView studentTaskInit(HomeworkView home){
    	//作业编辑页面
        ModelAndView result = new ModelAndView("course/manager/home_student_work");
        //获取当前登录者账号
        UserEntity user = WebContext.getSessionUser();
        home.setCreateUser(user.getUserId());
        HomeworkView homework=courseVRService.searchStudentHomeWork(home);
        //前台页面信息回显对象
        result.addObject("homework", homework);
        result = getRoleAndNum(result);
        return result;
    }
    *//**
                  * 保存作业信息
     * @param id  作业id
     * @param studentComment 学生提交内容
     * @param homeanwserId 作业作答id
     * @param status 提交作业状态
     * @return
     * @throws Exception
     *//*
    @RequestMapping("/saveMystudy.action")
    @ResponseBody
    public ResultBean saveMystudy(String id,String studentComment,String homeanwserId,String status) throws Exception {
        ResultBean result = ResultBean.success();
        String userId = WebContext.getSessionUserId();
        //根据homeanwserId判断当前登陆者是否已经作答过改作业
        if(StringUtil.isBlank(homeanwserId)) {
        	//未作答，保存作答内容
        	HomeworkView course = new HomeworkView();
        	//随机生成作业作答id
        	course.setId(IDGenerator.genUID());
        	course.setCreateUser(userId);
        	course.setCreateDate(new Date());
        	//作业作答内容
        	course.setStudentComment(studentComment);
        	course.setHomeworkId(id);
        	course.setAnswerUser(userId);
        	//设置作业状态
        	course.setStatus(HOMEWORK_ANSWER_STATUS_ONE);
        	courseVRService.insertAnswer(course);
        }else {
        	//更新做大内容
        	HomeworkAnswerEntity entity = new HomeworkAnswerEntity();
        	entity.setId(homeanwserId);
        	entity = courseVRService.selectOneAnswer(entity);
        	entity.setStudentComment(studentComment);
        	entity.setUpdateDate(new Date());
        	entity.setUpdateUser(userId);
        	courseVRService.update(entity);
        }
        return result;
    }
    *//**
                 * 学生查看作业列表方法
     * @param home
     * @return
     *//*
    @RequestMapping("/studentSeacherTaskInit.action")
    public ModelAndView studentSeacherTaskInit(HomeworkView home){
    	//学生作业列表页面路径
        ModelAndView result = new ModelAndView("course/manager/home_student_look_work");
        //根据登录id查询作业信息
        UserEntity user = WebContext.getSessionUser();
        home.setCreateUser(user.getUserId());
        HomeworkView homework=courseVRService.searchStudentHomeWork(home);
        //作业信息返回给前台
        result.addObject("homework", homework);
        result = getRoleAndNum(result);
        return result;
    }
    *//**
                  * 查询年级 课程
     * @param id 课程id
     * @return
     *//*
    @RequestMapping("/searchGrade.action")
    @ResponseBody
    public List<StudentClassInfoEntity> searchGrade(String id){
    	//根据课程id查询课程信息
        CourseEntity entity = new CourseEntity();
        entity.setId(id);
        entity = courseService.searchCourse(entity);
        //获取老师所属学校
        String schoolId = entity.getSchoolId();
        StudentClassInfoEntity studentClassInfoEntity = new StudentClassInfoEntity();
        studentClassInfoEntity.setSchoolId(schoolId);
        //根据学校查询年级
        List<StudentClassInfoEntity> gradeList = new ArrayList<StudentClassInfoEntity>();
        gradeList = studentClassInfoService.searchStudentGradeList(studentClassInfoEntity);
        //返回查询的年级集合给前台
        return gradeList;
    }
     *//**
                     * 根据课程主键和年级主键查询班级信息
      * @param courseId  课程主键
      * @param gradeId 年级主键
      * @return
      *//*
    @RequestMapping("/searchClass.action")
    @ResponseBody
    public List<StudentClassInfoEntity> searchClass(String courseId,String gradeId){
    	//根据课程主键查询课程信息
    	 CourseEntity entity = new CourseEntity();
         entity.setId(courseId);
         entity = courseService.searchCourse(entity);
         //获取老师所属学校
         String schoolId = entity.getSchoolId();
        List<StudentClassInfoEntity> gradeList = new ArrayList<StudentClassInfoEntity>();
        //根据学校主键和年级查询班级信息
        StudentClassInfoEntity studentClassInfoEntity = new StudentClassInfoEntity();
        studentClassInfoEntity.setSchoolId(schoolId);//学校id
        studentClassInfoEntity.setGradeId(gradeId);//年级id
        gradeList = studentClassInfoService.searchStudentClassList(studentClassInfoEntity);
        return gradeList;
    }
    *//**
                  * 添加学生时查询学生列表
     * @param UserView query 用户参数 
     * @param courseId 所属课程信息id
     * @return
     *//*
    @RequestMapping("/searchStudentUserAll.action")
    @ResponseBody
    public ResultBean searchStudentUserAll(UserView query,String courseId)  {
    	//根据课程id查询课程所属老师对应的学校信息
    	CourseEntity entity = new CourseEntity();
        entity.setId(courseId);
        entity = courseService.searchCourse(entity);
        //获取老师所属学校
        String schoolId = entity.getSchoolId();
        //根据学校id获取这个学校下面所有的学生信息
        query.setSchoolId(schoolId);
        return courseService.searchStudentUserAll(query);
    }
	*//**
	 * 用户个人中心信息页面方法
	 * @return 返回结果
	 *//*
	@RequestMapping("/userPblInfo.action")
	public ModelAndView userPblInfo(){
		//查询代办信息表里审核通过的代办信息数量
		int num = courseService.getStuStatus();
		//个人中心页面路径参数
		ModelAndView mv = new ModelAndView("trainingplatform/client/center/center_pbl_user");
		//根据用户id查询用户信息
		UserEntity user = userManagementService.seachUserInfo(WebContext.getSessionUserId());
		//返回用户对象
		mv.addObject("user", user);
		mv.addObject("num", num);
		//获取登录人员的角色信息
		UserEntity userRole = WebContext.getSessionUser();
		//判断用户是否登录状态guest为未登录状态
		if(!userRole.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(userRole.getUserId());
			if(roleList.size() > 0) {
				//循环遍历人员角色
				for(UserRoleEntity role : roleList) {
					//角色包含01的设置role字段为t,即老师角色
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				//s为学生角色
				mv.addObject("role", "s");
			}
		}
		UserEntity user1 = WebContext.getSessionUser();
		//获取工号
		String uid =user1.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		    //获取人员信息
		        //设置头像字段
				String image = "";
				//设置头像字段
				String school="";
				//设置年级字段
				String grade="";
				//设置班级字段
				String className="";
				if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
					//获取头像
					image = userEntity.getHeadurl();
					//获取学校
					school = userEntity.getSchoolName();
				}else {
					//设置默认头像
					image ="http://wisdomsaas-img.oss-cn-beijing.aliyuncs.com/BFAA0FECDD194B7A974C60B7E1B27C18?authorization=bce-auth-v1%2F7642b4b48046452697e90662eed41ace%2F2017-12-29T08%3A33%3A39Z%2F-1%2F%2F0f06024af14dad05694f25b8c85572567591c72bbd0129d3175b4cdbd6e62208";
				}
				//获取班级
				if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
					//实例化学生对象实体类并查询学生信息
					List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
					StudentClassInfoEntity entity = new StudentClassInfoEntity();
					entity.setStudentId(uid);
					entityList = studentClassInfoService.searchStudentClassInfoList(entity);
					//如果学生对象不为空则执行如下赋值
					if(null!=entityList && entityList.size()>0) {
						grade = entityList.get(0).getGradeName();
						className = entityList.get(0).getClassName();
						school =  entityList.get(0).getSchoolName();
					}
				}
				//返回班级信息
				mv.addObject("className", className);
				//向页面返回年级信息
				mv.addObject("grade", grade);
				//返回学生所属学校名称信息
				mv.addObject("school", school);
		       return mv;
	}
	*//**
	 * 批量点评初始化页面方法
	 * @param userIds  学生id集合字符串
	 * @param personNames  学生姓名字符集合
	 * @param id      作业id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/allEvalutionMsg.action")
	public ModelAndView allEvalutionMsg(String userIds,String personNames,String id) throws Exception {
		//批量点评方法
		ModelAndView result = new ModelAndView("/course/client/home_work_teacher_all_review");
		String ids[]=id.split(",");
		HomeworkView view = courseService.getHomeWorkById(ids[0]);
		//判断作业对象返回的时间是否为空，不为空则赋值给新的字段用于前端展示
		if(null!=view.getCreateDate()) {
			if(StringUtil.isNotBlank(sdf.format(view.getCreateDate()))) {
				view.setCreateTime(sdf.format(view.getCreateDate()));
			}
		}
		//收集作业对象
		result.addObject("homeWork", view);
		//收集作业id
		result.addObject("id", id);
		//收集学生账号集合id
		result.addObject("userIds", userIds);
		//收集学生姓名集合
		result.addObject("personNames", personNames);
		//收集作业答案id
		result.addObject("answerIds", id);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 批量点评保存
	 * @param courseJson 前台返回的json字符串
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveAllHomeWorkReviewById.action")
	@ResponseBody
	public ResultBean saveAllHomeWorkReviewById(String courseJson) throws Exception {
		//将字符串转化成实例化对象
		HomeworkView homework = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, HomeworkView.class);
		return courseService.saveAllHomeWorkReviewById(homework);
	}
	*//**
	 * 一键催交
	 * @param userIds  学生id集合字符串参数
	 * @param courseId 课程id
	 * @param homeworkId  作业id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveCjById.action")
	@ResponseBody
	public ResultBean saveCjById(String userIds,String courseId,String homeworkId) throws Exception {
		return courseService.saveACjById(userIds,courseId,homeworkId);
	}
	*//**
	 * 课程考试调用java调查网接口
	 * @param courseId  课程id
	 * @param userId  用户id
	 * @return
	 * @throws ParseException
	 *//*
	@RequestMapping("/searchExamByCourseIdList.action")
	@ResponseBody
	public ResultBean searchExamByCourseIdList(String courseId,String userId) throws ParseException {
		ResultBean result = new ResultBean();
		String param = courseId;
		//声明调用java调查网的webservice接口
		ExamToWebserviceProxy proxy = new ExamToWebserviceProxy();
		String out;
		try {
			//调用外部接口方法
			out = proxy.examList(param,1,20,userId);
			JSONArray jsonArray = new JSONArray(); 
			//将接口返回值转化成数组格式
			jsonArray = jsonArray.fromObject(out);
			UserCourseEntity uc = new UserCourseEntity();
			uc.setCourseId(courseId);
			uc.setUserId(WebContext.getSessionUserId());
			// 检索用户是否存在
			UserCourseEntity uu = courseService.searchOneCourseIdData(uc);
			//根据课程id获取该课程的老师id
			CourseEntity courseEntity = courseService.getCourseId(courseId);
			//转化对应的实体类
			@SuppressWarnings("unchecked")
			List<Questionnaire> list = jsonArray.toList(jsonArray, Questionnaire.class);
			if(null!=list && list.size()>0) {
				//循环遍历集合将字段赋值
				for(int i=0;i<list.size();i++) {
					//已加入的学员或者这个课程的老师创建者都可以看到这个
					if(null!=uu ||(null!=courseEntity && WebContext.getSessionUserId().equals(courseEntity.getTeacherId()))) {
						//已加入课程
						list.get(i).setStudentFlag("0");
						if (list.get(i).getEndDate() != null ) {
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Long end = sdf.parse(list.get(i).getEndDateStr()).getTime();
							Long nowtime = new Date().getTime();
							//判断问卷/考试是否已结束
								if(end-nowtime<0) {
									list.get(i).setExamStatusText("问卷/考试已结束");
									list.get(i).setExamStatus("1");	
								}					
							}
						if (list.get(i).getStartDate() != null ) {
							//将字符串转化成时间再获取秒数
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Long start = sdf.parse(list.get(i).getStartDateStr()).getTime();
							Long nowtime = new Date().getTime();
							//判断问卷/考试是否开始
								if(start-nowtime>0) {
									list.get(i).setExamStatusText("问卷/考试未开始");
									list.get(i).setExamStatus("1");
								}
							}
					}else {
						list.get(i).setStudentFlag("1");//未加入课程
						list.get(i).setExamStatusText("未加入课程");
					}
					
				}
			}
			 result.setData(list);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
		
	*//**
	 * 
	 * 评价的发布/取消发布
	 * 
	 * @param id     课程id
	 * @param status 评价状态：0 未发布 1 已发布
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/publicEvalution.action")
	@ResponseBody
	public ResultBean publicEvalution(String id, String status) throws Exception {
		tagInstitutionEvalutionService.publicEvalution(id, status);
		return ResultBean.success(MSG_S_SUBMIT);
	}

	*//**
	 * 问卷显示页面
	 *//*
	@RequestMapping("/question.action")
	public ModelAndView question(String linkId, String linkType) {
		ModelAndView result = new ModelAndView("/institution/manager/institution_evalution_questionner_item");
		result.addObject("linkId", linkId);
		result.addObject("linkType", linkType);
		return result;
	}

	*//**
	 * 问卷题目选项
	 *//*
	@RequestMapping("/questionnaireDetial.action")
	@ResponseBody
	public ResultBean questionnaireDetial(String linkId, String linkType) {
		return questionInstitutionService.questionnaireDetial(linkId, linkType);
	}

	*//**
	 * 问卷提交
	 *//*
	@RequestMapping("/submitQuestionnaire.action")
	@ResponseBody
	public ResultBean submitQuestionnaire(String questionIds, String optionIds, String measureId) {
		return questionInstitutionService.submitQuestionnaire(questionIds, optionIds, measureId);
	}

	*//**
	 * 问卷调查结果列表
	 * 
	 * @author heyaqin
	 * @return 返回结果
	 *//*
	@RequestMapping("/questionResultList.action")
	public ModelAndView questionResultList(MeasurementInstitutionEntity condition) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_result_list");
		mv.addObject("condition", condition);
		UserEntity user = WebContext.getSessionUser();
		if (!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if (roleList.size() > 0) {
				for (UserRoleEntity role : roleList) {
					if (role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			} else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}

	*//**
	 * 问卷调查列表检索
	 * 
	 * @author
	 * @return 返回结果
	 *//*
	@RequestMapping("/questionResultListSearch.action")
	@ResponseBody
	public ResultBean questionResultListSearch(MeasurementInstitutionEntity condition) {
			UserEntity user = WebContext.getSessionUser();	
			String userId = user.getCreateUser();
			condition.setCreateUser(userId);
		return userManagementService.selectQuestionResultList(condition);
	}

	*//**
	 * 问卷统计列表初始化
	 * 
	 * @author
	 * @param id 问卷ID
	 * @return 返回结果
	 *//*
	@RequestMapping("/viewMeasurementListInit.action")
	public ModelAndView viewMeasurementListInit(String id) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_statistics");
		int optionNum = userManagementService.searchInstitutionTotalOptions(id) + 1;
		QuestionOptionInstitutionEntity questionOptionEntity = new QuestionOptionInstitutionEntity();
		questionOptionEntity.setMeasureId(id);
		mv.addObject("questionOptionEntity", questionOptionEntity);
		mv.addObject("optionNum", optionNum);
		// 问卷统计结果列表
		ArrayList<Map<String, String>> optionMapList = userManagementService.selectInstitutionOptionsList(id);
		mv.addObject("optionMapList", optionMapList);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 问卷统计列表检索
	 * 
	 * @author HEYAQIN
	 * @param id 问卷ID
	 * @return 返回结果
	 *//*
	@RequestMapping("/viewMeasurementList.action")
	@ResponseBody
	public ArrayList<Map<String, String>> viewMeasurementList(String id) {
		if (StringUtil.isEmpty(id)) {
			return null;
		} else {
			ArrayList<Map<String, String>> optionMapList = userManagementService.selectInstitutionOptionsList(id);
			return optionMapList;
		}
	}

	*//**
	 * 问卷参与人员 检索
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/viewUserSearch.action")
	@ResponseBody
	public ResultBean viewUserSeacher(String measurementId) {
		return userManagementService.selectEvalutionQuestionUserList(measurementId);
	}

	*//**
	 * 问卷参与人员回答详情
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/viewDetailInit.action")
	public ModelAndView viewDetailInit(UserMeasurementResultEntity condition) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_user_detail");
		if (StringUtil.isEmpty(condition.getMeasurementId())) {
			throw new BusinessException("参数错误!");
		}

		List<UserMeasurementResultEntity> list = measuermentService
				.searchInstitutionUserMeasurementResultByEntiy(condition);
		if (list != null && list.size() != 0) {
			mv.addObject("condition", list.get(0));
		} else {
			mv.addObject("condition", new UserMeasurementResultEntity());
		}
		return mv;
	}

	*//**
	 * 问卷回答详情
	 * 
	 * @param measurementId
	 * @param userId
	 * @return
	 *//*
	@RequestMapping("/viewUserAnswerSearch.action")
	@ResponseBody
	public ResultBean viewUserAnswerSearch(String measurementId, String userId) {
		return userManagementService.selectEvalutionAnswerUserList(measurementId, userId);
	}

	*//**
	 * 检索测试信息
	 * 
	 * @param linkId
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchMeasuerment.action")
	@ResponseBody
	public ResultBean searchMeasuerment(String measurementId, String userId) {
		if (StringUtil.isEmpty(measurementId)) {
			return ResultBean.error(getMessage("参数错误!"));
		}
		ResultBean result = ResultBean.success();
		List<QuestionInstitutionEntity> questionList = measurementInstitutionService
				.searchMeasuermentInstitutionNormalAndExam(userId, measurementId);
		result.setData(questionList);
		return result;
	}

	*//**
	 * 问卷参与人员 画面初始化
	 * 
	 * @param condition
	 * @return
	 *//*
	@SuppressWarnings("unchecked")
	@RequestMapping("/viewUserInit.action")
	public ModelAndView viewUserInit(MeasurementInstitutionEntity condition) {
		ModelAndView mv = new ModelAndView("/institution/manager/institution_evalution_question_user_list");
		mv.addObject("measurementId", condition.getId());
		List<MeasurementInstitutionEntity> list = measurementInstitutionService.search(condition);
		if (list != null && list.size() != 0) {
			mv.addObject("condition", list.get(0));
		} else {
			throw new BusinessException("参数错误!");
		}
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 删除问卷调查列表
	 * 
	 * @author heyaqin
	 * @return 返回结果
	 *//*
	@RequestMapping("/deleteQuestionList.action")
	@ResponseBody
	public ResultBean deleteQuestionList(String id) {
		userManagementService.deleteEvalutionQuestionList(id);
		return ResultBean.success();
	}

	*//**
	 * 导入页面初始化
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/importInit.action")
	public String importInit() {
		return "institution/manager/institution_import";
	}

	*//**
	 * 导入机构
	 * 
	 * @param linkId
	 * @return 返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/importUpload.action")
	@ResponseBody
	public ResultBean importCourse(@RequestParam("institution") CommonsMultipartFile institution,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		InputStream input = null;
		try {
			input = institution.getInputStream();
			File targetFile = FileUtil.writeToTmpFile(input);
			return institutionService.importInstitution(targetFile);
		} finally {
			IOUtils.closeQuietly(input);
		}
	}

	*//**
	 * 批量导出机构
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/exportInstitution.action")
	@ResponseBody
	public InstitutionManagerView exportInstitution(InstitutionEntity entity) throws Exception {
		List<InstitutionManagerView> viewList = new ArrayList<InstitutionManagerView>();
		UserEntity userEntity = WebContext.getSessionUser();
		if (!"游客".equals(userEntity.getPersonName())) {
			String role = userEntity.getUserKbn();
			String userId=userEntity.getUserId();
			if("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				entity.setCreateUser(userId);
				viewList = institutionService.exportInstitution(entity);
			}else if("B".equals(role)) {
				entity.setCreateUser(userId);
				viewList = institutionService.exportManagerInstitution(entity);
			}
		}
		
		InstitutionManagerView user = new InstitutionManagerView();
		user.setInstitutionList(viewList);
		ExcelDataBind edbUser = new ExcelDataBind(user);
		InputStream is = null;
		OutputStream os = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			String date = sdf.format(new Date());
			String fileName = URLEncoder.encode("组织机构" + date + ".xlsx", "UTF-8");
			WebContext.getResponse().reset();
			is = InstitutionService.class.getResourceAsStream("institutionExportTemplate.xlsx");
			WebContext.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			WebContext.getResponse()
					.setContentType(" application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8");
			edbUser.bind(is, WebContext.getResponse().getOutputStream());
		} catch (IOException ex) {

		} finally {

			IOUtils.closeQuietly(is);
			IOUtils.closeQuietly(os);
		}
		return null;
	}

	*//**
	 * 增加机构方法
	 * 
	 * @param entity
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/addJyInstitution.action")
	public ModelAndView addJyInstitution(InstitutionEntity entity) throws Exception {
		ModelAndView mv = new ModelAndView("institution/manager/institution_add");
		InstitutionEntity institutionEntity = new InstitutionEntity();
		UserEntity user = WebContext.getSessionUser();
		String role = user.getUserKbn();
		//判断权限
		List<InstitutionEntity> schoollist=new ArrayList<InstitutionEntity>();
		String userId = user.getCreateUser();
		if ("A".equals(role)) {
			TpUserEntity tpUserEntity =new TpUserEntity();
			tpUserEntity = userManagementService.searchById(userId);
			institutionEntity.setInstitutionName(tpUserEntity.getSchoolName());
			institutionEntity=institutionService.seacherPersonEdution(institutionEntity);
			if(null!=institutionEntity) {
				entity.setInstitutionName(institutionEntity.getInstitutionName());
			}
			schoollist.add(institutionEntity);
		}else if("B".equals(role)) {
			institutionEntity.setCreateUser(userId);
			institutionEntity= institutionService.searchEdution(institutionEntity);
            if(null!=institutionEntity) {
            	entity.setInstitutionName(institutionEntity.getInstitutionName());
			}
            schoollist.add(institutionEntity);
		}
		mv.addObject("schoollist", schoollist);
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 添加学校信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/addInstitution.action")
	public ModelAndView addInstitution(String institutionParentId) throws Exception {
		InstitutionEntity entity = new InstitutionEntity();
		entity.setInstitutionParentId(institutionParentId);
		ModelAndView mv = new ModelAndView("institution/manager/institution_addSchool");
		List<InstitutionEntity> schoollist=new ArrayList<InstitutionEntity>();
		UserEntity user = WebContext.getSessionUser();
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				schoollist = institutionService.seacherSchoolLocationList(entity);
			} else if("B".equals(role)) {
				entity.setCreateUser(userId);
				schoollist= institutionService.searchEdutionSchoolList(entity);
			}
		
		mv.addObject("schoollist", schoollist);
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 删除班级
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/deleteInstitutionById.action")
	@ResponseBody
	public ResultBean deleteGradeById(String id) throws Exception {
		InstitutionEntity entity = new InstitutionEntity();
		entity.setId(id);
		institutionService.deleteInstitutionById(entity);
		return ResultBean.success(DELETE_GRADE_SUCCESS);
	}

	*//**
	 * 跳转到班级编辑页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/institutionEdit.action")
	public ModelAndView institutionEdit(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		ModelAndView mv = new ModelAndView("institution/manager/institution_edit");
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 检索班级信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchInstitution.action")
	@ResponseBody
	public ResultBean searchNotice(InstitutionEntity entity) {
		return institutionService.searchInstitutionEvalutionList(entity);
	}
	*//**
	 * 学校检索班级信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/schoolsearchInstitution.action")
	@ResponseBody
	public ResultBean schoolsearchInstitution(InstitutionEntity entity) {
		return institutionService.searchInstitutionEvalutionList1(entity);
	}
	*//**
	 * 检索班级信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchSchoolInstitution.action")
	@ResponseBody
	public ResultBean searchSchoolInstitution(InstitutionEntity entity) {
		ResultBean result = new ResultBean();
		UserEntity user = WebContext.getSessionUser();
		if (!"游客".equals(user.getPersonName())) {
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				entity.setCreateUser(userId);
				result=institutionService.searchInstitutionTeacherEvalutionList(entity);
			} else if ("C".equals(role)) {
				entity.setCreateUser(userId);
				StudentClassInfoEntity classInfo = new StudentClassInfoEntity();
				classInfo.setStudentId(userId);
				classInfo = studentClassInfoService.searchStudentClassInfo(classInfo);
			    if(null!=classInfo) {
			    	entity.setInstitutionName(classInfo.getSchoolName());
					result= institutionService.searchInstitutionEvalutionList(entity);
			    }
			}else if("B".equals(role)) {
				entity.setCreateUser(userId);
				result= institutionService.searchInstitutionManagerEvalutionList(entity);
			}
		}
		return result;
	}

	*//**
	 * 新增信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/insertInstitution.action")
	@ResponseBody
	public ResultBean insertInstitution(InstitutionEntity entity) throws Exception {
		ResultBean result = new ResultBean();
		InstitutionEntity institutionEntity = new InstitutionEntity();
		institutionEntity.setInstitutionName(entity.getInstitutionName());
		List<InstitutionEntity> datalist = (List<InstitutionEntity>) institutionService.searchInstitutionEvalutionList1(institutionEntity).getData();
		if(null!=datalist && datalist.size()>0) {
			result.setStatus(false);
			result = ResultBean.error("学校名称已存在");
		}else {
//			String institutionType=entity.getInstitutionType();
//			TagInstitutionEntity tagInstitutionEntity = new TagInstitutionEntity();
//			tagInstitutionEntity.setTagId(institutionType);
//			tagInstitutionEntity = tagInstitutionService.searchFirstData(tagInstitutionEntity);
//			entity.setInstitutionType(tagInstitutionEntity.getTagName());
			if(StringUtil.isNotBlank(entity.getSchoolNatrue())) {
				if("1".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("小学");
				}else if("2".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("初中");
				}else if("3".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("高中");
				}else if("4".equals(entity.getSchoolNatrue())) {
				   entity.setInstitutionType("九年一贯制");
			    }
			}
			institutionService.insertInstitution(entity, getCompressConfigByName("imageP"));
			result.setStatus(true);
			result= ResultBean.success(MSG_ADD_GRADE_SUCCESS);
		}
		return result;
	}
	*//**
	 * 新增信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/insertSchoolEvalutionInstitution.action")
	@ResponseBody
	public ResultBean insertSchoolEvalutionInstitution(InstitutionEntity entity) throws Exception {
		institutionService.insertInstitution(entity, getCompressConfigByName("imageP"));
		return ResultBean.success(MSG_ADD_GRADE_SUCCESS);
	}
	*//**
	 * 编辑保存
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/updateInstitution.action")
	@ResponseBody
	public ResultBean updateInstitution(InstitutionEntity entity) throws Exception {
		ResultBean result = new ResultBean();
		InstitutionEntity institutionEntity = new InstitutionEntity();
		institutionEntity.setInstitutionName(entity.getInstitutionName());
		List<InstitutionEntity> datalist = (List<InstitutionEntity>) institutionService.searchInstitutionEvalutionList1(institutionEntity).getData();
		if(null!=datalist && datalist.size()>0 && !entity.getId().equals(datalist.get(0).getId())) {
			result.setStatus(false);
			result = ResultBean.error("学校名称已存在");
		}else {
//			String institutionType=entity.getInstitutionType();
//			TagInstitutionEntity tagInstitutionEntity = new TagInstitutionEntity();
//			tagInstitutionEntity.setTagId(institutionType);
//			tagInstitutionEntity = tagInstitutionService.searchFirstData(tagInstitutionEntity);
//			entity.setInstitutionType(tagInstitutionEntity.getTagName());
			if(StringUtil.isNotBlank(entity.getSchoolNatrue())) {
				if("1".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("小学");
				}else if("2".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("初中");
				}else if("3".equals(entity.getSchoolNatrue())) {
					entity.setInstitutionType("高中");
				}else if("4".equals(entity.getSchoolNatrue())) {
					   entity.setInstitutionType("九年一贯制");
				}
			}
			institutionService.updateInstitution(entity, getCompressConfigByName("imageP"));
			result.setStatus(true);
			result = ResultBean.success("创建学校成功");
		}
		return result;
	}
	*//**
	 * 编辑保存
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/updateSchoolEvalutionInstitution.action")
	@ResponseBody
	public ResultBean updateSchoolEvalutionInstitution(InstitutionEntity entity) throws Exception {
		entity.setCreateUser(WebContext.getSessionUserId());
		institutionService.updateInstitution(entity, getCompressConfigByName("imageP"));
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/institutionStudentlist.action")
	public ModelAndView institutionStudentlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/institution/manager/institution_student_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		return result;
	}

	*//**
	 * 跳转到班级查看页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/institutionLook.action")
	public ModelAndView institutionLook(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		ModelAndView mv = new ModelAndView("institution/manager/institution_look");
		mv.addObject("institution", entity);
		return mv;
	}

	*//**
	 * 老师分享作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/shareWork.action")
	@ResponseBody
	public ResultBean shareWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.shareHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 老师取消分享作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/cancleShareWork.action")
	@ResponseBody
	public ResultBean cancleShareWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.cancleShareHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);

	}

	*//**
	 * 老师置顶作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/topWork.action")
	@ResponseBody
	public ResultBean topWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.topHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 老师取消置顶作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/cancleTopWork.action")
	@ResponseBody
	public ResultBean cancleTopWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.cancleTopHomeWorkById(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 老师删除作业
	 * 
	 * @author linkId
	 * @param linkType
	 * 
	 * @return 返回结果
	 *//*
	@RequestMapping("/deleteWork.action")
	@ResponseBody
	public ResultBean deleteWork(String id) {
		// return courseService.likeHomeWorkById(id);
		courseService.deleteWork(id);
		return ResultBean.success(EDIT_GRADE_SUCCESS);
	}

	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/baseinstitutionlist.action")
	public ModelAndView baseinstitutionlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/baseinstitution/manager/base_institution_list");
		result.addObject("condition", condition);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 机构基本信息
	 * 
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/schoolbaseinstitutionlist.action")
	public ModelAndView schoolbaseinstitutionlist(InstitutionEntity condition) {
		ModelAndView result = new ModelAndView("/baseinstitution/manager/base_school_institution_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 增加机构方法
	 * 
	 * @param entity
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/addBaseInstitution.action")
	public ModelAndView addBaseInstitution(InstitutionEntity entity) throws Exception {
		ModelAndView mv = new ModelAndView("baseinstitution/manager/base_institution_add");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList1(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		List<TagInstitutionEntity> tagList = tagInstitutionService.searchTagInstitutionAll();
		mv.addObject("tagList", tagList);
		mv.addObject("catagoryTags", tags);
		mv.addObject("tagsSecond", tagsSecond);
		mv.addObject("institution", entity);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 跳转到班级编辑页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/baseinstitutionEdit.action")
	public ModelAndView baseinstitutionEdit(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		ModelAndView mv = new ModelAndView("baseinstitution/manager/base_institution_edit");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList1(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		List<TagInstitutionEntity> tagList = tagInstitutionService.searchTagInstitutionAll();
        String secondId = entity.getFirstTag();
        TagInstitutionEntity taginfo = tagInstitutionService.searchTagInstitution(secondId);
        entity.setSecondTag(taginfo.getParentId());
		mv.addObject("tagList", tagList);
		mv.addObject("catagoryTags", tags);
		mv.addObject("tagsSecond", tagsSecond);
		mv.addObject("institution", entity);
		mv = getRoleAndNum(mv);
		return mv;
	}
	
	*//**
	 * 跳转到班级编辑页面
	 * 
	 * @param id
	 * @return 返回结果
	 *//*
	@RequestMapping("/baseSchoolinstitutionEdit.action")
	public ModelAndView baseSchoolinstitutionEdit(String id) {
		InstitutionEntity entity = institutionService.selectInstitutionById(id);
		InstitutionEntity institutionEntity = new InstitutionEntity();
		ModelAndView mv = new ModelAndView("baseinstitution/manager/base_school_institution_edit");
		TagInstitutionView tagQuery = new TagInstitutionView();
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList1(tagQuery);
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		List<TagInstitutionEntity> tagList = tagInstitutionService.searchTagInstitutionAll();
		List<InstitutionEntity> schoollist=new ArrayList<InstitutionEntity>();
		UserEntity user = WebContext.getSessionUser();
			String role = user.getUserKbn();
			//判断权限
			String userId = user.getCreateUser();
			if(StringUtil.isNotBlank(entity.getInstitutionParentId())) {
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				entity.setInstitutionName(tpUserEntity.getSchoolName());
				schoollist = institutionService.seacherSchoolLocationList(entity);
			} else if("B".equals(role)) {
				entity.setCreateUser(userId);
				schoollist= institutionService.searchEdutionSchoolList(entity);
			}
			
		}else {
			if ("A".equals(role)) {
				TpUserEntity tpUserEntity =new TpUserEntity();
				tpUserEntity = userManagementService.searchById(userId);
				institutionEntity.setInstitutionName(tpUserEntity.getSchoolName());
				institutionEntity=institutionService.seacherPersonEdution(institutionEntity);
				entity.setInstitutionName(institutionEntity.getInstitutionName());
				schoollist.add(institutionEntity);
			}else if("B".equals(role)) {
				institutionEntity.setCreateUser(userId);
				institutionEntity= institutionService.searchEdution(institutionEntity);
				entity.setInstitutionName(institutionEntity.getInstitutionName());
				schoollist.add(institutionEntity);
			}
		}
		
		mv.addObject("schoollist", schoollist);
		mv.addObject("tagList", tagList);
		mv.addObject("catagoryTags", tags);
		mv.addObject("tagsSecond", tagsSecond);
		mv.addObject("institution", entity);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 分类基本列表
	 * 
	 * @return
	 *//*
	@RequestMapping("/baseinstitutioninit.action")
	public ModelAndView baseinstitutioninit() {
		ModelAndView mv = new ModelAndView("baseinstitutiontaxonomy/manager/base_taxonomy_course_list");
		BaseEntity condition = new BaseEntity();
		condition.setPageSize(5);
		mv.addObject("condition", condition);
		mv = getRoleAndNum(mv);
		return mv;
	}

	*//**
	 * 学校列表页面
	 *//*
	@RequestMapping("/schoolListInit.action")
	public ModelAndView schoolListInit(String schoolName) {
		ModelAndView result = new ModelAndView("baseinstitution/manager/school_list");
		String userId= WebContext.getSessionUserId();
		ManagerUserEntity entity= managerUserService.searchByUserId(userId);
		String mangerRole="0";
		if(null!=entity || "2542668".equals(userId)) {
			mangerRole="1";
		}else {
			mangerRole="0";
		}
		result.addObject("schoolName", schoolName);
		result.addObject("mangerRole", mangerRole);
		result = getRoleAndNum(result);
		return result;
	}

	*//**
	 * 检索课程 包括分类
	 * 
	 * @author LILIANG
	 * @return CourseEntity
	 * directionTagId 学段
	 * categoryTagId 分类
	 *//*
	@RequestMapping("/schoolList.action")
	@ResponseBody
	public ResultBean schoolList(InstitutionEntity course, String directionTagId, String categoryTagId,
			String courseStage, String sortType, Integer pageSize, Integer pageNumber,String schoolStage) {
		ResultBean result = new ResultBean();
		if(StringUtil.isNotBlank(course.getSchoolName())) {
			course.setInstitutionName(course.getSchoolName());
		}
		//区域
		if(StringUtil.isNotBlank(directionTagId)) {
			course.setFirstTag(directionTagId);
		}
		if(StringUtil.isNotBlank(directionTagId)) {
			TagInstitutionEntity entity = new TagInstitutionEntity();
			entity.setTagId(directionTagId);
			entity = tagInstitutionService.searchFirstData(entity);
			String xdName = entity.getTagName();
			course.setInstitutionType(xdName);
		}
		if(StringUtil.isNotBlank(categoryTagId)) {
			TagInstitutionEntity entity = new TagInstitutionEntity();
			entity.setTagId(categoryTagId);
			entity = tagInstitutionService.searchFirstData(entity);
			String xdName = entity.getTagName();
			course.setFirstTag(categoryTagId);
		}
		//学校阶段
		if(StringUtil.isNotBlank(schoolStage)) {
			course.setSchoolNatrue(schoolStage);
		}
		//权限查看
		String userId = WebContext.getSessionUserId();
		ManagerUserEntity entity= managerUserService.searchByUserId(userId);
		//教育局的人员暂时看到全部
		if(null!=entity || "2542668".equals(userId) || "B".equals(WebContext.getSessionUser().getUserKbn())) {
			result = courseService.schoolList(course, directionTagId, categoryTagId, courseStage, sortType,
					pageSize, pageNumber);
		}else {
			String role = WebContext.getSessionUser().getUserKbn();
			course.setCreateUser(userId);
			if("A".equals(role)) {
				result = courseService.schooTeacherlList(course, directionTagId, categoryTagId, courseStage, sortType,
						pageSize, pageNumber);
			}else if("C".equals(role)) {
				result = courseService.schooStudentlList(course, directionTagId, categoryTagId, courseStage, sortType,
						pageSize, pageNumber);
			}
		}
		
		return result;
	}
	*//**
	 * 日志操作页面 检索列表
	 * 
	 * @param condition
	 * @return 返回结果
	 *//*
	@RequestMapping("/logsearch.action")
	@ResponseBody
	public ResultBean logsearch(UserOperEntity condition) {
		return courseService.search4Page(condition);
	}
	*//**
	 * 已完成
	 * @param finishType
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 *//*
	@RequestMapping("/userCourseFinishList.action")
	@ResponseBody
	public ResultBean userCourseFinishList(String finishType, Integer pageSize, Integer pageNumber) {
		finishType ="3";
		pageSize =5;
		pageNumber=1;
		// 检索我的课程
		ResultBean result = courseService.userCourseList(finishType, pageSize, pageNumber);
		return result;
	}
	*//**
	 * 已完成
	 * @param finishType
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 *//*
	@RequestMapping("/userCourseUnFinishList.action")
	@ResponseBody
	public ResultBean userCourseUnFinishList(String finishType, Integer pageSize, Integer pageNumber) {
		pageSize =5;
		pageNumber=1;
		// 检索我的课程
		ResultBean result = courseService.userCourseUnFinishList(finishType, pageSize, pageNumber);
		return result;
	}
    *//**
                  * 作业列表
     * @return
     *//*
	@RequestMapping("/zuoyeList.action")
	@ResponseBody
	public ResultBean zuoyeList() {
		// 检索我的课程
		ResultBean result = courseService.userZyList();
		return result;
	}
    *//**
     * 
                  * 考试结果查看
     * @param UserMeasurementResultEntity condition 入参
     * @return
     *//*
    @RequestMapping("/userExamDetail.action")
    public ModelAndView userExamDetail(UserMeasurementResultEntity condition) {
    	//考试结果页面
        ModelAndView result = new ModelAndView("course/manager/user_exam_detail");
        //考试结果id是否存在
        if(StringUtil.isEmpty(condition.getMeasurementId())){
            throw new BusinessException("参数错误!");
        }
        condition.setCreateUser(WebContext.getSessionUserId());
        //查询考试结果
        List<UserMeasurementResultEntity> list = measuermentService.searchUserMeasurementResultByEntiy(condition);
        if(list != null && list.size() != 0) {
            result.addObject("condition", list.get(0));
        } else {
            result.addObject("condition", new UserMeasurementResultEntity());
        }
        //返回考试结果
        return result;
    }
	*//**
	 * 查询全部考试
	 * @param ChapterEntity chapter章节参数
	 * @param pageSize  每页展示多少条数据
	 * @param pageNumber 页码
	 * @return
	 *//*
	@RequestMapping("/myAllExam.action")
	public ModelAndView myAllExam(ChapterEntity chapter, Integer pageSize, Integer pageNumber) {
		//考试页面内容展示路径
		ModelAndView mv = new ModelAndView("baseinstitution/manager/my_exam_list");
		//查询数据表
		ResultBean result = chapterService.myAllxam(chapter, pageSize, pageNumber);
		mv.addObject("chapter", result.getData());
		return mv;
	}
	*//**
	 *检索考试
	 * @param chapter
	 * @param pageSize
	 * @param pageNumber
	 * @return
	 *//*
	@RequestMapping("/myAllExamList.action")
	@ResponseBody
	public ResultBean myAllExamList(ChapterEntity chapter, Integer pageSize, Integer pageNumber) {
		ResultBean result = chapterService.myAllxam(chapter, pageSize, pageNumber);
		return result;
	}
	*//**
	 * 查询课工厂方法
	 * @param courseId 课程id
	 * @param content 课程内容
	 * @param type 课程类型
	 * @return
	 *//*
	@RequestMapping("/factory.action")
    @ResponseBody
    public ResultBean factory(String courseId,String content,String type)  {
        ResultBean result = new ResultBean();
        result = courseService.factory(courseId,content,type);
        return result;
    }
	*//**
	 * 用户作业列表
	 * @return
	 *//*
	@RequestMapping("/userHomeWork.action")
	public ModelAndView userHomeWork() {
		//作业列表路径
		ModelAndView result = new ModelAndView("/course/client/user_home_work");
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 查询作业列表
	 * @return
	 *//*
	@RequestMapping("/userHomeWorkSearch.action")
	@ResponseBody
	public ResultBean search(){
		//返回结果
		return courseService.searchUserHomeWork4Page();
	}
	*//**
	 * 查询学校信息
	 * @return
	 *//*
	@RequestMapping("/selectSchoolNameInfo.action")
	@ResponseBody
	public ResultBean selectSchoolNameInfo() {
		ResultBean result = new ResultBean();
		//获取登录信息
		String userId = WebContext.getSessionUserId();
		//获取角色信息
		String role = WebContext.getSessionUser().getUserKbn();
		//判断角色
		if("C".equals(role)) {
			//学生角色
			StudentClassInfoEntity entity =new StudentClassInfoEntity();
			entity.setStudentId(userId);
			List<StudentClassInfoEntity> stuList = studentClassInfoService.searchStudentClassInfoList(entity);
			stuList.get(0).setUserId(userId);
			//设置返回值数据
			result.setData(stuList.get(0));
		}else {
			//教师信息
			TpUserEntity tpUserEntity = userManagementService.searchById(userId);
			result.setData(tpUserEntity);
		}
		 
		return result;
	}
	*//**
	 * 结束课程方法
	 * @param id 课程id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/endCourse.action")
	@ResponseBody
	public ResultBean endCourse(String id) throws Exception {
		courseService.endCourse(id);
		return ResultBean.success(MSG_S_SUBMIT);
	}
	*//**
	 * 图书馆课程复制
	 * @param id 课程id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/toLibCourse.action")
	@ResponseBody
	public ResultBean toLibCourse(String id) throws Exception {
		courseService.toLibCourse(id);
		return ResultBean.success(MSG_S_SUBMIT);
	}
	*//**
	 * 课程复制方法
	 * @param id 课程主键
	 * @return
	 *//*
	@RequestMapping("/copyInfo.action")
    @ResponseBody
    public ResultBean copyInfo(String id)  {
        ResultBean result = new ResultBean();
        //课程复制实现方法
        courseService.copyCourseByHtml(id);
        result.setMessages(getMessage(MSG_S_SAVE));
        return result;
    }
    *//**
                 * 新增课程活动记录方法
     * @param courseId 课程id
     * @return
     *//*
	@RequestMapping("/addActiveInit.action")
	public ModelAndView addActiveInit(String courseId) {
	//课程活动新增页面路径
	ModelAndView result = new ModelAndView("course/manager/course_active_add");
	TagView tagQuery = new TagView();
	//一级分类对象
	List<TagView> tags = courseService.searchTagList2(tagQuery);
	//二级分类对象
	List<TagView> tagsSecond = courseService.searchTagSecondList2(tagQuery);
	//查询章节信息
	ChapterEntity entity = new ChapterEntity();
	entity.setCourseId(courseId);
	List<ChapterEntity> chapterList = courseService.selectByCourseId(entity);
	//返回前台对象
	result.addObject("catagoryTags", chapterList);
	//result.addObject("catagoryTags", tags);
	result.addObject("pageType", "add");
	result.addObject("course", new CourseManagerView());
	result.addObject("tagsSecond", tagsSecond);
	result = getRoleAndNum(result);
	return result;
	}
    *//**
                  * 保存课程章节活动记录信息
     * @param courseJson 课程信息
     * @return 返回结果
     * @throws Exception 
     *//*
    @RequestMapping("/saveActiveChapterInfo.action")
    @ResponseBody
    public ResultBean saveActiveChapterInfo(String courseJson) throws Exception {
    	//实例化对象
        CourseManagerView courseview = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, CourseManagerView.class);
    	ChapterActiveEntity course = new ChapterActiveEntity();
    	course.setChapterId(courseview.getChapterId());
    	course.setActiveFlag(courseview.getActiveFlag());
    	//根据章节id查询章节信息
    	ChapterEntity view= new ChapterEntity();
    	String chapterId = courseview.getChapterId();
    	view = chapterService.selectChapteById(chapterId);
    	//设置章节字段进行保存
    	course.setChapterTitle(view.getTitle());
    	course.setContent(courseview.getContent());
        ResultBean result = ResultBean.success();
        //保存章节活动内容
        chapterActiveService.insertInfo(course);
        return result;
    }
   *//**
             * 查询课程章节阶段活动记录信息
    * @param activeFlag
    * @param chapterId
    * @return
    *//*
	@RequestMapping("/searchActiveChapterInfo.action")
	@ResponseBody
	public ResultBean searchActiveChapterInfo(String activeFlag,String chapterId){
	ChapterActiveEntity course = new ChapterActiveEntity();
	//判断活动记录标识是否为空
	if(StringUtil.isNotBlank(activeFlag)) {
		course.setActiveFlag(activeFlag);
	}
	course.setChapterId(chapterId);
	//查询该章节的全部活动记录
	List<ChapterActiveEntity> view = chapterActiveService.searchList(course);
	ResultBean result = ResultBean.success();
	result.setData(view);
	return result;
	}
	*//**
	 * 查询课程阶段活动记录信息
	 * @param activeFlag 活动记录阶段标识
	 * @param courseId  课程主键
	 * @param chapterId  章节主键
	 * @return
	 *//*
	@RequestMapping("/searchActiveAllChapterInfo.action")
	@ResponseBody
	public ResultBean searchActiveAllChapterInfo(String activeFlag,String courseId,String chapterId){
	ChapterActiveEntity course = new ChapterActiveEntity();
	//判断活动记录标识是否为空
	if(StringUtil.isNotBlank(activeFlag)) {
		course.setActiveFlag(activeFlag);
	}
	course.setCourseId(courseId);
	if(StringUtil.isNotBlank(chapterId)) {
		course.setChapterId(chapterId);
	}
	//返回查询结果
	return chapterActiveService.searchAllCourseChapterPage(course);
	}
	*//**
	 * 查询角色的公共方法
	 * @param result
	 * @return
	 *//*
	private ModelAndView getRoleAndNum(ModelAndView result) {
    	int num = courseService.getStuStatus();
    	result.addObject("num", num);
    	UserEntity user = WebContext.getSessionUser();
    	//判断是否管理员
    	ManagerUserEntity entity = managerUserService.searchByUserId(user.getUserId());
    	if(!user.getUserName().equals("guest")) {
    		List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
    		//老师和教育局角色
    		if(roleList.size() > 0 && "A".equals(user.getUserKbn()) && null==entity && !"2542668".equals(user.getUserId())) {
    			for(UserRoleEntity role : roleList) {
    				if(role.getRoleId().equals("01")) {
    					result.addObject("role", "t");
    				}
    			}
    		//教育局角色	
    		}else if(roleList.size() > 0  && (null!=entity || "2542668".equals(user.getUserId()))){
    			result.addObject("role", "m");
    		}else if("B".equals(user.getUserKbn())){
    			result.addObject("role", "e");
    	    //家长和教研员角色
    		}else if("D".equals(user.getUserKbn()) || "E".equals(user.getUserKbn())){
    			result.addObject("role", "q");
    		//学生角色
    		}else {
    			result.addObject("role", "s");
    		}
    	}
        //管理员角色
    	if(null!=entity || "2542668".equals(user.getUserId())) {
    		result.addObject("manager", "1");
    	}else {
    		result.addObject("manager", "0");
    	}
    	return result;
    }
    *//**
     * 活动记录编辑页面方法
     * @param courseId  课程id
     * @param activeId  活动id
     * @return
     *//*
	@RequestMapping("/editActiveInit.action")
	public ModelAndView editActiveInit(String courseId,String activeId) {
	//活动编辑页面路径
	ModelAndView result = new ModelAndView("course/manager/course_active_editor");
	TagView tagQuery = new TagView();
	//查询一级分类方法
	List<TagView> tags = courseService.searchTagList2(tagQuery);
	//查询e二级分类方法
	List<TagView> tagsSecond = courseService.searchTagSecondList2(tagQuery);
	//根据课程id查询所有对应的章节信息
	ChapterEntity entity = new ChapterEntity();
	entity.setCourseId(courseId);
	List<ChapterEntity> chapterList = courseService.selectByCourseId(entity);
	//根据活动记录id查询活动章节信息
	ChapterActiveEntity active = new ChapterActiveEntity();
	active.setId(activeId);
	List<ChapterActiveEntity> activeList = chapterActiveService.searchList(active);
	//设置返回前台的字段信息
	CourseManagerView courseManagerView = new  CourseManagerView();
	courseManagerView.setChapterId(activeList.get(0).getChapterId());
	courseManagerView.setChapterTitle(activeList.get(0).getChapterTitle());
	courseManagerView.setActiveFlag(activeList.get(0).getActiveFlag());
	courseManagerView.setContent(activeList.get(0).getContent());
	//章节对象
	result.addObject("catagoryTags", chapterList);
	//result.addObject("catagoryTags", tags);
	result.addObject("pageType", "add");
	result.addObject("activeId", activeId);
	//课程对象
	result.addObject("course",courseManagerView);
	result.addObject("tagsSecond", tagsSecond);
	result = getRoleAndNum(result);
	return result;
	}
    *//**
                * 保存活动记录课程章节信息
	* @param courseJson 课程信息
	* @return 返回结果
	* @throws Exception 
	*//*
	@RequestMapping("/updateActiveChapterInfo.action")
	@ResponseBody
	public ResultBean updateActiveChapterInfo(String courseJson) throws Exception {
    //转化实例对象方法
	CourseManagerView courseview = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, CourseManagerView.class);
	ChapterActiveEntity course = new ChapterActiveEntity();
	//删除当前活动记录
	ChapterActiveEntity entity = new ChapterActiveEntity();
	String id = courseview.getActiveId();
	entity.setId(id);
	chapterActiveService.deleteInfo(entity);
	//设置保存课程的信息字段
	course.setChapterId(courseview.getChapterId());
	course.setActiveFlag(courseview.getActiveFlag());
	//查询章节信息
	ChapterEntity view= new ChapterEntity();
	String chapterId = courseview.getChapterId();
	view = chapterService.selectChapteById(chapterId);
	course.setChapterTitle(view.getTitle());
	course.setContent(courseview.getContent());
	ResultBean result = ResultBean.success();
	//保存活动记录的课程章节信息
	chapterActiveService.insertInfo(course);
	return result;
	}
    *//**
                 * 删除活动
	* @param courseJson 课程信息
	* @return 返回结果
	* @throws Exception 
	*//*
	@RequestMapping("/activeDelete.action")
	@ResponseBody
	public ResultBean activeDelete(String id) throws Exception {
	ChapterActiveEntity entity = new ChapterActiveEntity();
	//设置活动记录主键
	entity.setId(id);
	chapterActiveService.deleteInfo(entity);
	ResultBean result = ResultBean.success();
	return result;
	}
	*//**
	 * 添加页面初始化：节
	 * 
	 * @param courseId 课程ID
	 * @return
	 *//*
	@RequestMapping("/courseSmallInit.action")
	public ModelAndView courseSmallInit(String courseId, String parentId) {
		ModelAndView result = new ModelAndView("course/manager/course_three_edit");
		ChapterView chapter = new ChapterView();
		chapter.setMeasureType("4");
		result.addObject("courseId", courseId);
		result.addObject("parentId", parentId);
		result.addObject("chapterFlag", SMALL_CHAPTER);
		result.addObject("chapter", chapter);
		return result;
	}
	*//**
	 * 保存章信息
	 * 
	 * @param chapterJson
	 * @return 返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/saveExamInfo.action")
	@ResponseBody
	public ResultBean saveExamInfo(String chapterJson, String videoLength) throws Exception {
		//将字符串转化成章节对象
		ChapterView chapter = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateMinuteFormatDefualt()),
				chapterJson, ChapterView.class);
		ResultBean result = ResultBean.success();
		//设置章节状态0未发布，1已发布
		chapter.setStatus("1");
		//设置加入课程1是已加入
		chapter.setIsCourse("1");
		if (StringUtil.isEmpty(chapter.getId())) {
			//未存在的章节考试直接保存
			chapterService.insertChapterInfoAll(chapter, videoLength);
		} else {
			//已存在的章节考试更新信息
			chapterService.updateChapterAll(chapter, videoLength);
		}
		//返回信息
		return result;
	}
    *//**
     * 
                  * 查看考试结果
     * @param condition
     * @return
     *//*
    @SuppressWarnings({ "unchecked"})
    @RequestMapping("/userExamInfoInit.action")
    public ModelAndView userExamInfoInit(MeasurementEntity condition) {
        ModelAndView result = new ModelAndView("course/manager/user_exam_info");
        //参数为空抛出异常
        if(StringUtil.isEmpty(condition.getLinkId())){
            throw new BusinessException("参数错误!");
        }
        //根据参数查询考试结果
        List<MeasurementEntity> list = courseService.search(condition);
        if(list != null && list.size() != 0) {
            result.addObject("condition", list.get(0));
        } else {
            throw new BusinessException("参数错误!");
        }
        //返回考试信息
        return result;
    }
    *//**
                 * 根据课程id获取章节考试状态信息
     * @param courseId
     * @return
     * @throws Exception
     *//*
	@RequestMapping("/searchExamStatus.action")
	@ResponseBody
	public ChapterView searchExamStatus(String courseId) throws Exception {
		//查询章节信息
		ChapterView view = chapterService.seacrhExamStatus(courseId);
		return view;
	}
	@SuppressWarnings("unchecked")
	*//**
	* 前端请求-章节详情
	* @param chapterId 章节ID
	* @return 
	*//*
	@RequestMapping("/toChapterDetail.action")
	public ModelAndView toChapterDetail(String courseId,String chapterId){
		ModelAndView mv = new ModelAndView("course/client/course_chapter_detail");
		ResultBean result = courseService.searchCourseDetail(courseId);
		CourseView courseView = (CourseView) result.getData();
		mv.addObject("courseView", courseView);
		mv.addObject("note", new NoteDataEntity());
		mv.addObject("homeWork", new HomeworkView());
		result = chapterService.chapterDetail(courseId, chapterId);
		ChapterView chapterView = (ChapterView) result.getData();
		ChapterEntity entity = new ChapterEntity();
		entity.setCourseId(courseId);
		List<ChapterEntity> chapterCourseList = courseService.selectByCourseId(entity);
		mv.addObject("chapterCourseList", chapterCourseList);
		mv.addObject("chapterView", chapterView);
		//检索章节列表
		List<ChapterView> cvs = (List<ChapterView>) chapterService.chapterList(courseId);
		mv.addObject("chapterList", cvs);
		//相关课程
		result = courseService.recommendedCourses("5", courseId);
		List<CourseEntity> otherCourseList = (List<CourseEntity>) result.getData();
		mv.addObject("otherCourseList", otherCourseList);
		//判断是否需要检索测试信息
		if(CHAPTER_TYPE_MEASUERMENT.equals(chapterView.getMeasureType().toString())){
			result = measuermentService.searchMeasuerment(chapterId, LINK_TYPE_CHAPTER);
			List<QuestionEntity> questionList = (List<QuestionEntity>) result.getData();
			mv.addObject("questionList", questionList);
		}
		//判断是否考试章节
		if(CHAPTER_TYPE_EXAM.equals(chapterView.getMeasureType())){
			//考试章节
			//取用户对于章节的考试信息
			MeasurementEntity me = new MeasurementEntity();
			me.setLinkId(chapterId);
			me.setLinkType(LINK_TYPE_CHAPTER);
			me = measuermentService.findMeasurement(me);
			if(me!=null){
				UserMeasurementResultEntity um = new UserMeasurementResultEntity();
				um.setMeasurementId(me.getId());
				um.setUserId(WebContext.getSessionUserId());
				um = measuermentService.findUserExamResult(um);
				if(um != null && um.getExamStatus()!=null && um.getExamStatus().equals(EXAM_STATUS_END)){
					//已参加过考试提交了
					mv.addObject("goal", um.getGoal()+"");
					mv.addObject("totalGoal", um.getTotalGoal()+"");
					mv.addObject("examTime", DateUtil.formatAny(um.getCreateDate(), ConfigUtil.getDateTimeFormatDefualt()));
				}
			}
		}
		//是否教师
		mv.addObject("isTeacher", courseService.isTeacher(courseView.getTeacherId()));
		mv.addObject("useingChapterId", chapterId);
		String content = chapterView.getContent();
		if(content != null && !"".equals(content)) {
			String ipAndPort = getIpAndPort(content);
			if(!"".equals(ipAndPort)) {
				String serverName = WebContext.getRequest().getServerName();
				int serverPort = WebContext.getRequest().getServerPort();
				content = content.replace(ipAndPort, serverName+":"+serverPort);
				chapterView.setContent(content);
			}
		}
		
		return mv;
	}
	*//**
	 * 判断str中是否有IP和port，若有则取出返回
	 * @param str
	 * @return String
	 *//*
	private static String getIpAndPort(String str) {
		String returnStr = "";
		Pattern p = Pattern.compile("(\\d+\\.\\d+\\.\\d+\\.\\d+)\\:(\\d+)");
		Matcher m = p.matcher(str);
		if(m.find()) {
			returnStr += m.group(1)+":"+m.group(2);
		}
		
		return returnStr;
	}
	*//**
	 * 标签一览
	 * @param flag 是否显示标签 0 不显示 1 显示
	 * @param businessType 业务类型
	 * @return
	 *//*
	@RequestMapping("/searchSchhoolCategory.action")
	@ResponseBody
	public ResultBean searchSchhoolCategory(String flag,String businessType){
		ResultBean  result=tagService.searchSchhoolCategory(flag,businessType);
		return result;
	}
	*//**
	 * 
	 * 初始化待办课程列表
	 * 
	 * @param condition 课程对象
	 * @return 返回课程列表初始化页面
	 *//*
	@RequestMapping("/institutionDbListInit.action")
	public ModelAndView institutionDbListInit(InstitutionMessageEntity condition) {
		//设置代办页面
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_db_list");
		//设置分页参数
		condition.setPageSize(10);
		result.addObject("condition", condition);
		TagInstitutionView tagQuery = new TagInstitutionView();
		//一级标签信息查询
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		//二级标签信息查询
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		//返回课程实例对象
		result.addObject("course", new InstitutionMessageEntity());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}
	*//**
	 * 
	 * 初始化已办课程列表
	 * 
	 * @param condition 课程对象
	 * @return 返回课程列表初始化页面
	 *//*
	@RequestMapping("/institutionYbListInit.action")
	public ModelAndView institutionYbListInit(InstitutionMessageEntity condition) {
		//设置已办页面
		ModelAndView result = new ModelAndView("institution/manager/institution_evalution_yb_list");
		condition.setPageSize(10);
		result.addObject("condition", condition);
		TagInstitutionView tagQuery = new TagInstitutionView();
		//一级标签信息查询
		List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
		//二级标签信息查询
		List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
		result.addObject("catagoryTags", tags);
		//返回课程实例对象
		result.addObject("course", new InstitutionMessageEntity());
		result.addObject("tagsSecond", tagsSecond);
		return result;
	}
	*//**
	 * 
	 * 检索评价列表
	 * 
	 * @param queryInfo 机构对象
	 * @return 返回评价列表
	 *//*
	@RequestMapping("/listdbSearch.action")
	@ResponseBody
	public ResultBean listdbSearch(MeasurementInstitutionEntity queryInfo) {
		UserEntity user = WebContext.getSessionUser();	
		//获取人员角色
		String role = user.getUserKbn();
		//获取登录人账号
		String userId = user.getCreateUser();
		//教师角色
		if("A".equals(role)) {
			TpUserEntity tpUserEntity =new TpUserEntity();
			//查询教师信息
			tpUserEntity = userManagementService.searchById(userId);
		    if(null!=tpUserEntity) {
		    	queryInfo.setInstitutionName(tpUserEntity.getSchoolName());
		    }
		    //学生角色
		}else if("C".equals(role)) {
			//查询学生信息
			StudentClassInfoEntity classInfo = new StudentClassInfoEntity();
			classInfo.setStudentId(userId);
			classInfo = studentClassInfoService.searchStudentClassInfo(classInfo);
			if(null!=classInfo && StringUtil.isNotBlank(classInfo.getSchoolName())) {
				queryInfo.setInstitutionName(classInfo.getSchoolName());
			}
		}
		
		return taxonomyInstitutionService.listdbSearch(queryInfo);
	}
	*//**
	 * 
	 * 检索已办评价列表
	 * 
	 * @param queryInfo 机构对象
	 * @return 返回评价列表
	 *//*
	@RequestMapping("/listybSearch.action")
	@ResponseBody
	public ResultBean listybSearch(MeasurementInstitutionEntity queryInfo) {
		UserEntity user = WebContext.getSessionUser();	
		//获取人员角色
		String role = user.getUserKbn();
		//登录账号
		String userId = user.getCreateUser();
		//教师角色
	    if("A".equals(role)) {
	    	//查询用户信息
	    	TpUserEntity tpUserEntity =new TpUserEntity();
			tpUserEntity = userManagementService.searchById(userId);
			if(null!=tpUserEntity) {
				//设置学校名称
				queryInfo.setInstitutionName(tpUserEntity.getSchoolName());
			}
	    }else if("C".equals(role)) {
	    	//查询学生信息
			StudentClassInfoEntity classInfo = new StudentClassInfoEntity();
			classInfo.setStudentId(userId);
			classInfo = studentClassInfoService.searchStudentClassInfo(classInfo);
			if(null!=classInfo && StringUtil.isNotBlank(classInfo.getSchoolName())) {
				//设置学生学校名称
				queryInfo.setInstitutionName(classInfo.getSchoolName());
			}
		}
	    //返回查询结果列表
		return taxonomyInstitutionService.listybSearch(queryInfo);
	}
	*//**
	 * 管理人员基本信息页面
	 * @param condition
	 * @return
	 *//*
	@RequestMapping("/managerUserList.action")
	public ModelAndView managerUserList(ManagerUserEntity condition) {
		//管理员信息页面展示路径
		ModelAndView result = new ModelAndView("/baseinstitution/manager/base_manager_user_list");
		//设置每页展示10条数据
		condition.setPageSize(10);
        result.addObject("course", new CourseManagerView());
		result.addObject("condition", condition);
		result = getRoleAndNum(result);
		//返回页面所需信息结果
		return result;
	}
	*//**
	 * 管理者信息
	 * 
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchManagerUserList.action")
	@ResponseBody
	public ResultBean searchManagerUserList(ManagerUserEntity entity) {
		//查询管理员数据表全部信息
		return managerUserService.searchPage(entity);
	}
	*//**
	 * 搜素全部人员
	 * 
	 * @param query
	 * @return
	 *//*
	@RequestMapping("/searchUserAll.action")
	@ResponseBody
	public ResultBean searchUserAll(TpUserEntity query) {
		if(null!=query && StringUtil.isNotBlank(query.getUsrName())) {
			query.setUsrName(query.getUsrName().trim());
		}
		//根据用户名查询人员信息表数据
		return userManagementService.searchManagerUserAll(query);
	}
	*//**
	 * 保存超级管理员信息
	 * @param userId 添加人员账号id
	 * @param userName  人员名称
	 * @param personName 名称
	 * @param role 角色
	 * @return
	 *//*
	@RequestMapping("/saveManagerUser.action")
	@ResponseBody
	public ResultBean saveManagerUser(String userId,String userName,String personName,String role) {
		ResultBean result = new ResultBean();
		//查询管理员表中是否已存在该成员
		ManagerUserEntity entity = managerUserService.searchByUserId(userId);
		//存在则返回
		if(null!=entity) {
			result = ResultBean.error();
		}else {
			//不存在则保存到数据表里
			ManagerUserEntity managerUserEntity = new ManagerUserEntity();
			managerUserEntity.setUserId(userId);
			managerUserEntity.setUserName(userName);
			managerUserEntity.setPersonName(personName);
			managerUserEntity.setRole(role);
			managerUserEntity.setCreateDate(new Date());
			//设置该条信息的创建人
			managerUserEntity.setCreateUser(WebContext.getSessionUserId());
			managerUserService.insert(managerUserEntity);
			//保存成功返回信息
			result = ResultBean.success();
		}
		
		return result;
	}
	*//**
	 * 删除管理员
	 * @param id 员工账号
	 * @return  返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/deleteManagerById.action")
	@ResponseBody
	public ResultBean deleteManagerById(String id) throws Exception {
		//根据员工账号在tb_manager_user表里删除其信息
		ManagerUserEntity entity = new ManagerUserEntity();
		entity.setUserId(id);
		managerUserService.delete(entity);
		return ResultBean.success(DELETE_GRADE_SUCCESS);
	}
	*//**
	 * 查询管理信息
	 * 
	 * @param 员工账号
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchByUserId.action")
	@ResponseBody
	public ManagerUserEntity searchByUserId(String id) throws Exception {
		//返回查询结果
		return managerUserService.searchByUserId(id);
	}
	*//**
	 *  作业列表页面
	 * @param homeworkView 入参
	 * @return 返回结果
	 *//*
	@RequestMapping("/zuoyeManager.action")
	public ModelAndView zuoyeManager(HomeworkView homeworkView) {
		//列表页面路径
		ModelAndView result = new ModelAndView("/course/manager/homework_list");
		//返回作业信息对象给前台
		result.addObject("homeworkView", homeworkView);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 *  查询作业列表结果方法
	 * @param homeworkView 前台传入后台参数
	 * @return  返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/zuoyelistSearch.action")
	@ResponseBody
	public ResultBean zuoyelistSearch(HomeworkView homeworkView) throws Exception {
		ResultBean result =new ResultBean();
		//获取登录信息对象
		UserEntity user = WebContext.getSessionUser();
    	//判断是否管理员
    	ManagerUserEntity entity = managerUserService.searchByUserId(user.getUserId());
    	if(null!=entity) {
    		//管理员返回的结果
    		result= courseVRService.seacherHomeworkPage(homeworkView);
    	}else {
    		//普通教师查询返回的结果
    		homeworkView.setTeacherId(user.getUserId());
    		result= courseVRService.seacherHomeworkPage(homeworkView);
    	}
		return result;
	}
	*//**
	 *  根据作业id查询作业列表
	 * @param id 作业主键
	 * @param title  作业标题
	 * @return  返回结果
	 * @throws Exception
	 *//*
	@RequestMapping("/zuoyelistById.action")
	public ModelAndView zuoyelistById(String id,String title) throws Exception {
		//作业列表路径
		ModelAndView result = new ModelAndView("/course/manager/homework_user_list");
		HomeworkView homeworkView = new HomeworkView();
		homeworkView.setId(id);
		homeworkView.setTitle(title);
		//作业实例化对象
		result.addObject("homeworkView", homeworkView);
		//作业主键
		result.addObject("id", id);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 预览作业页面方法
	 * @param id  作业主键
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/homeWorkById.action")
	public ModelAndView homeWorkById(String id) throws Exception {
		//预览页面路径
		ModelAndView result = new ModelAndView("/course/client/home_work_teacher_review");
		HomeworkView view = courseService.getHomeWorkById(id);
		//设置创建时间返回前台
		if(null!=view.getCreateDate()) {
			view.setCreateTime(sdf.format(view.getCreateDate()));
		}
        //返回作业实体类对象
		result.addObject("homeWork", view);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 查询作业列表
	 * @param id 作业id集合字符串
	 * @param homeworkView  作业实体类
	 * @param linkId 课程主键
	 * @param linkType  课程/章节类型
	 * @return
	 *//*
	@RequestMapping("/zuoyelistByIdSearch.action")
	@ResponseBody
	public ResultBean zuoyelistByIdSearch(String id,HomeworkView homeworkView,String linkId,String linkType) {
		// 将前台汇总的作业id拆分
		if(StringUtil.isNotBlank(id)) {
			String ids[]=id.split(",");
			homeworkView.setId(ids[0]);
		}else {
			homeworkView.setLinkId(linkId);
			homeworkView.setLinkType(linkType);
			HomeworkEntity homeWork = courseService.getHomeWorkByLinkId(homeworkView);
			if(null!=homeWork) {
				//获取作业主键
				homeworkView.setId(homeWork.getId());
				//获取作业标题
				homeworkView.setTitle(homeWork.getTitle());
			}
		}
		//查询作业信息
		return courseVRService.seacherHomeworkUserPage(homeworkView);
	}
    *//**
                 * 保存作业信息
     * @param courseJson 作业信息
     * @return 返回结果
     * @throws Exception 
     *//*
    @RequestMapping("/saveZyInfo.action")
    @ResponseBody
    public ResultBean saveZyInfo(String courseJson) throws Exception {
    	//将字符串转化成实例化对象
    	HomeworkView course = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, HomeworkView.class);
        ResultBean result = ResultBean.success();
        String homeworkId=course.getId();
        //获取账号id
        String userId = WebContext.getSessionUserId();
        //查询作业数据表
        HomeworkAnswerEntity entity = new  HomeworkAnswerEntity();
        entity.setHomeworkId(homeworkId);
        entity.setCreateUser(userId);
        entity = courseVRService.selectOneAnswer(entity);
        if(null==entity) {
        	//作业作答数据表信息为空，插入数据信息
        	course.setId(IDGenerator.genUID());
        	course.setCreateUser(userId);
        	course.setCreateDate(new Date());
        	//设置学生提交的信息
        	course.setStudentComment(course.getContent());
        	course.setHomeworkId(homeworkId);
        	course.setAnswerUser(userId);
        	//设置作业状态
        	course.setStatus(HOMEWORK_ANSWER_STATUS_ONE);
        	courseVRService.insertAnswer(course);
        }else {
        	//作业作答数据表信息不为空，更新数据信息
        	entity.setStudentComment(course.getContent());
        	entity.setUpdateDate(new Date());
        	entity.setUpdateUser(userId);
        	courseVRService.update(entity);
        }
        return result;
    }
	*//**
	 * 学生作业列表页面
	 * @param HomeworkView homeworkView 
	 * @return
	 *//*
	@RequestMapping("/studentZuoyePage.action")
	public ModelAndView studentZuoyePage(HomeworkView homeworkView) {
		//学生作业列表页面路径
		ModelAndView result = new ModelAndView("/course/manager/homework_student_list");
		//作业实例对象实体类
		result.addObject("homeworkView", homeworkView);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 查询作业列表
	 * 
	 * @param HomeworkView homeworkView 入参
	 * @return 返回结果
	 *//*
	@RequestMapping("/studentZuoyelistSearch.action")
	@ResponseBody
	public ResultBean studentZuoyelistSearch(HomeworkView homeworkView) {
		homeworkView.setCreateUser(WebContext.getSessionUserId());
		//根据账号id获取作业对象信息
		return courseVRService.seacherStudentHomeworkUserPage(homeworkView);
	}
    *//**
                   * 编辑作业
     * @param HomeworkView home 作业实例对象入参 
     * @return
     *//*
    @RequestMapping("/studentTaskInit.action")
    public ModelAndView studentTaskInit(HomeworkView home){
    	//作业编辑页面
        ModelAndView result = new ModelAndView("course/manager/home_student_work");
        //获取当前登录者账号
        UserEntity user = WebContext.getSessionUser();
        home.setCreateUser(user.getUserId());
        HomeworkView homework=courseVRService.searchStudentHomeWork(home);
        //前台页面信息回显对象
        result.addObject("homework", homework);
        result = getRoleAndNum(result);
        return result;
    }
    *//**
                  * 保存作业信息
     * @param id  作业id
     * @param studentComment 学生提交内容
     * @param homeanwserId 作业作答id
     * @param status 提交作业状态
     * @return
     * @throws Exception
     *//*
    @RequestMapping("/saveMystudy.action")
    @ResponseBody
    public ResultBean saveMystudy(String id,String studentComment,String homeanwserId,String status) throws Exception {
        ResultBean result = ResultBean.success();
        String userId = WebContext.getSessionUserId();
        //根据homeanwserId判断当前登陆者是否已经作答过改作业
        if(StringUtil.isBlank(homeanwserId)) {
        	//未作答，保存作答内容
        	HomeworkView course = new HomeworkView();
        	//随机生成作业作答id
        	course.setId(IDGenerator.genUID());
        	course.setCreateUser(userId);
        	course.setCreateDate(new Date());
        	//作业作答内容
        	course.setStudentComment(studentComment);
        	course.setHomeworkId(id);
        	course.setAnswerUser(userId);
        	//设置作业状态
        	course.setStatus(HOMEWORK_ANSWER_STATUS_ONE);
        	courseVRService.insertAnswer(course);
        }else {
        	//更新做大内容
        	HomeworkAnswerEntity entity = new HomeworkAnswerEntity();
        	entity.setId(homeanwserId);
        	entity = courseVRService.selectOneAnswer(entity);
        	entity.setStudentComment(studentComment);
        	entity.setUpdateDate(new Date());
        	entity.setUpdateUser(userId);
        	courseVRService.update(entity);
        }
        return result;
    }
    *//**
                 * 学生查看作业列表方法
     * @param home
     * @return
     *//*
    @RequestMapping("/studentSeacherTaskInit.action")
    public ModelAndView studentSeacherTaskInit(HomeworkView home){
    	//学生作业列表页面路径
        ModelAndView result = new ModelAndView("course/manager/home_student_look_work");
        //根据登录id查询作业信息
        UserEntity user = WebContext.getSessionUser();
        home.setCreateUser(user.getUserId());
        HomeworkView homework=courseVRService.searchStudentHomeWork(home);
        //作业信息返回给前台
        result.addObject("homework", homework);
        result = getRoleAndNum(result);
        return result;
    }
    *//**
                  * 查询年级 课程
     * @param id 课程id
     * @return
     *//*
    @RequestMapping("/searchGrade.action")
    @ResponseBody
    public List<StudentClassInfoEntity> searchGrade(String id){
    	//根据课程id查询课程信息
        CourseEntity entity = new CourseEntity();
        entity.setId(id);
        entity = courseService.searchCourse(entity);
        //获取老师所属学校
        String schoolId = entity.getSchoolId();
        StudentClassInfoEntity studentClassInfoEntity = new StudentClassInfoEntity();
        studentClassInfoEntity.setSchoolId(schoolId);
        //根据学校查询年级
        List<StudentClassInfoEntity> gradeList = new ArrayList<StudentClassInfoEntity>();
        gradeList = studentClassInfoService.searchStudentGradeList(studentClassInfoEntity);
        //返回查询的年级集合给前台
        return gradeList;
    }
     *//**
                     * 根据课程主键和年级主键查询班级信息
      * @param courseId  课程主键
      * @param gradeId 年级主键
      * @return
      *//*
    @RequestMapping("/searchClass.action")
    @ResponseBody
    public List<StudentClassInfoEntity> searchClass(String courseId,String gradeId){
    	//根据课程主键查询课程信息
    	 CourseEntity entity = new CourseEntity();
         entity.setId(courseId);
         entity = courseService.searchCourse(entity);
         //获取老师所属学校
         String schoolId = entity.getSchoolId();
        List<StudentClassInfoEntity> gradeList = new ArrayList<StudentClassInfoEntity>();
        //根据学校主键和年级查询班级信息
        StudentClassInfoEntity studentClassInfoEntity = new StudentClassInfoEntity();
        studentClassInfoEntity.setSchoolId(schoolId);//学校id
        studentClassInfoEntity.setGradeId(gradeId);//年级id
        gradeList = studentClassInfoService.searchStudentClassList(studentClassInfoEntity);
        return gradeList;
    }
    *//**
                  * 添加学生时查询学生列表
     * @param UserView query 用户参数 
     * @param courseId 所属课程信息id
     * @return
     *//*
    @RequestMapping("/searchStudentUserAll.action")
    @ResponseBody
    public ResultBean searchStudentUserAll(UserView query,String courseId)  {
    	//根据课程id查询课程所属老师对应的学校信息
    	CourseEntity entity = new CourseEntity();
        entity.setId(courseId);
        entity = courseService.searchCourse(entity);
        //获取老师所属学校
        String schoolId = entity.getSchoolId();
        //根据学校id获取这个学校下面所有的学生信息
        query.setSchoolId(schoolId);
        return courseService.searchStudentUserAll(query);
    }
	*//**
	 * 用户个人中心信息页面方法
	 * @return 返回结果
	 *//*
	@RequestMapping("/userPblInfo.action")
	public ModelAndView userPblInfo(){
		//查询代办信息表里审核通过的代办信息数量
		int num = courseService.getStuStatus();
		//个人中心页面路径参数
		ModelAndView mv = new ModelAndView("trainingplatform/client/center/center_pbl_user");
		//根据用户id查询用户信息
		UserEntity user = userManagementService.seachUserInfo(WebContext.getSessionUserId());
		//返回用户对象
		mv.addObject("user", user);
		mv.addObject("num", num);
		//获取登录人员的角色信息
		UserEntity userRole = WebContext.getSessionUser();
		//判断用户是否登录状态guest为未登录状态
		if(!userRole.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(userRole.getUserId());
			if(roleList.size() > 0) {
				//循环遍历人员角色
				for(UserRoleEntity role : roleList) {
					//角色包含01的设置role字段为t,即老师角色
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				//s为学生角色
				mv.addObject("role", "s");
			}
		}
		UserEntity user1 = WebContext.getSessionUser();
		//获取工号
		String uid =user1.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		    //获取人员信息
		        //设置头像字段
				String image = "";
				//设置头像字段
				String school="";
				//设置年级字段
				String grade="";
				//设置班级字段
				String className="";
				if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
					//获取头像
					image = userEntity.getHeadurl();
					//获取学校
					school = userEntity.getSchoolName();
				}else {
					//设置默认头像
					image ="http://wisdomsaas-img.oss-cn-beijing.aliyuncs.com/BFAA0FECDD194B7A974C60B7E1B27C18?authorization=bce-auth-v1%2F7642b4b48046452697e90662eed41ace%2F2017-12-29T08%3A33%3A39Z%2F-1%2F%2F0f06024af14dad05694f25b8c85572567591c72bbd0129d3175b4cdbd6e62208";
				}
				//获取班级
				if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
					//实例化学生对象实体类并查询学生信息
					List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
					StudentClassInfoEntity entity = new StudentClassInfoEntity();
					entity.setStudentId(uid);
					entityList = studentClassInfoService.searchStudentClassInfoList(entity);
					//如果学生对象不为空则执行如下赋值
					if(null!=entityList && entityList.size()>0) {
						grade = entityList.get(0).getGradeName();
						className = entityList.get(0).getClassName();
						school =  entityList.get(0).getSchoolName();
					}
				}
				//返回班级信息
				mv.addObject("className", className);
				//向页面返回年级信息
				mv.addObject("grade", grade);
				//返回学生所属学校名称信息
				mv.addObject("school", school);
		       return mv;
	}
	*//**
	 * 批量点评初始化页面方法
	 * @param userIds  学生id集合字符串
	 * @param personNames  学生姓名字符集合
	 * @param id      作业id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/allEvalutionMsg.action")
	public ModelAndView allEvalutionMsg(String userIds,String personNames,String id) throws Exception {
		//批量点评方法
		ModelAndView result = new ModelAndView("/course/client/home_work_teacher_all_review");
		String ids[]=id.split(",");
		HomeworkView view = courseService.getHomeWorkById(ids[0]);
		//判断作业对象返回的时间是否为空，不为空则赋值给新的字段用于前端展示
		if(null!=view.getCreateDate()) {
			if(StringUtil.isNotBlank(sdf.format(view.getCreateDate()))) {
				view.setCreateTime(sdf.format(view.getCreateDate()));
			}
		}
		//收集作业对象
		result.addObject("homeWork", view);
		//收集作业id
		result.addObject("id", id);
		//收集学生账号集合id
		result.addObject("userIds", userIds);
		//收集学生姓名集合
		result.addObject("personNames", personNames);
		//收集作业答案id
		result.addObject("answerIds", id);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 批量点评保存
	 * @param courseJson 前台返回的json字符串
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveAllHomeWorkReviewById.action")
	@ResponseBody
	public ResultBean saveAllHomeWorkReviewById(String courseJson) throws Exception {
		//将字符串转化成实例化对象
		HomeworkView homework = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, HomeworkView.class);
		return courseService.saveAllHomeWorkReviewById(homework);
	}
	*//**
	 * 一键催交
	 * @param userIds  学生id集合字符串参数
	 * @param courseId 课程id
	 * @param homeworkId  作业id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveCjById.action")
	@ResponseBody
	public ResultBean saveCjById(String userIds,String courseId,String homeworkId) throws Exception {
		return courseService.saveACjById(userIds,courseId,homeworkId);
	}
	*//**
	 * 课程考试调用java调查网接口
	 * @param courseId  课程id
	 * @param userId  用户id
	 * @return
	 * @throws ParseException
	 *//*
	@RequestMapping("/searchExamByCourseIdList.action")
	@ResponseBody
	public ResultBean searchExamByCourseIdList(String courseId,String userId) throws ParseException {
		ResultBean result = new ResultBean();
		String param = courseId;
		//声明调用java调查网的webservice接口
		ExamToWebserviceProxy proxy = new ExamToWebserviceProxy();
		String out;
		try {
			//调用外部接口方法
			out = proxy.examList(param,1,20,userId);
			JSONArray jsonArray = new JSONArray(); 
			//将接口返回值转化成数组格式
			jsonArray = jsonArray.fromObject(out);
			UserCourseEntity uc = new UserCourseEntity();
			uc.setCourseId(courseId);
			uc.setUserId(WebContext.getSessionUserId());
			// 检索用户是否存在
			UserCourseEntity uu = courseService.searchOneCourseIdData(uc);
			//根据课程id获取该课程的老师id
			CourseEntity courseEntity = courseService.getCourseId(courseId);
			//转化对应的实体类
			@SuppressWarnings("unchecked")
			List<Questionnaire> list = jsonArray.toList(jsonArray, Questionnaire.class);
			if(null!=list && list.size()>0) {
				//循环遍历集合将字段赋值
				for(int i=0;i<list.size();i++) {
					//已加入的学员或者这个课程的老师创建者都可以看到这个
					if(null!=uu ||(null!=courseEntity && WebContext.getSessionUserId().equals(courseEntity.getTeacherId()))) {
						//已加入课程
						list.get(i).setStudentFlag("0");
						if (list.get(i).getEndDate() != null ) {
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Long end = sdf.parse(list.get(i).getEndDateStr()).getTime();
							Long nowtime = new Date().getTime();
							//判断问卷/考试是否已结束
								if(end-nowtime<0) {
									list.get(i).setExamStatusText("问卷/考试已结束");
									list.get(i).setExamStatus("1");	
								}					
							}
						if (list.get(i).getStartDate() != null ) {
							//将字符串转化成时间再获取秒数
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Long start = sdf.parse(list.get(i).getStartDateStr()).getTime();
							Long nowtime = new Date().getTime();
							//判断问卷/考试是否开始
								if(start-nowtime>0) {
									list.get(i).setExamStatusText("问卷/考试未开始");
									list.get(i).setExamStatus("1");
								}
							}
					}else {
						list.get(i).setStudentFlag("1");//未加入课程
						list.get(i).setExamStatusText("未加入课程");
					}
					
				}
			}
			 result.setData(list);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
			*//**
	 * 查询作业列表
	 * @param id 作业id集合字符串
	 * @param homeworkView  作业实体类
	 * @param linkId 课程主键
	 * @param linkType  课程/章节类型
	 * @return
	 *//*
	@RequestMapping("/zuoyelistByIdSearch.action")
	@ResponseBody
	public ResultBean zuoyelistByIdSearch(String id,HomeworkView homeworkView,String linkId,String linkType) {
		// 将前台汇总的作业id拆分
		if(StringUtil.isNotBlank(id)) {
			String ids[]=id.split(",");
			homeworkView.setId(ids[0]);
		}else {
			homeworkView.setLinkId(linkId);
			homeworkView.setLinkType(linkType);
			HomeworkEntity homeWork = courseService.getHomeWorkByLinkId(homeworkView);
			if(null!=homeWork) {
				//获取作业主键
				homeworkView.setId(homeWork.getId());
				//获取作业标题
				homeworkView.setTitle(homeWork.getTitle());
			}
		}
		//查询作业信息
		return courseVRService.seacherHomeworkUserPage(homeworkView);
	}
    *//**
                 * 保存作业信息
     * @param courseJson 作业信息
     * @return 返回结果
     * @throws Exception 
     *//*
    @RequestMapping("/saveZyInfo.action")
    @ResponseBody
    public ResultBean saveZyInfo(String courseJson) throws Exception {
    	//将字符串转化成实例化对象
    	HomeworkView course = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, HomeworkView.class);
        ResultBean result = ResultBean.success();
        String homeworkId=course.getId();
        //获取账号id
        String userId = WebContext.getSessionUserId();
        //查询作业数据表
        HomeworkAnswerEntity entity = new  HomeworkAnswerEntity();
        entity.setHomeworkId(homeworkId);
        entity.setCreateUser(userId);
        entity = courseVRService.selectOneAnswer(entity);
        if(null==entity) {
        	//作业作答数据表信息为空，插入数据信息
        	course.setId(IDGenerator.genUID());
        	course.setCreateUser(userId);
        	course.setCreateDate(new Date());
        	//设置学生提交的信息
        	course.setStudentComment(course.getContent());
        	course.setHomeworkId(homeworkId);
        	course.setAnswerUser(userId);
        	//设置作业状态
        	course.setStatus(HOMEWORK_ANSWER_STATUS_ONE);
        	courseVRService.insertAnswer(course);
        }else {
        	//作业作答数据表信息不为空，更新数据信息
        	entity.setStudentComment(course.getContent());
        	entity.setUpdateDate(new Date());
        	entity.setUpdateUser(userId);
        	courseVRService.update(entity);
        }
        return result;
    }
	*//**
	 * 学生作业列表页面
	 * @param HomeworkView homeworkView 
	 * @return
	 *//*
	@RequestMapping("/studentZuoyePage.action")
	public ModelAndView studentZuoyePage(HomeworkView homeworkView) {
		//学生作业列表页面路径
		ModelAndView result = new ModelAndView("/course/manager/homework_student_list");
		//作业实例对象实体类
		result.addObject("homeworkView", homeworkView);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 查询作业列表
	 * 
	 * @param HomeworkView homeworkView 入参
	 * @return 返回结果
	 *//*
	@RequestMapping("/studentZuoyelistSearch.action")
	@ResponseBody
	public ResultBean studentZuoyelistSearch(HomeworkView homeworkView) {
		homeworkView.setCreateUser(WebContext.getSessionUserId());
		//根据账号id获取作业对象信息
		return courseVRService.seacherStudentHomeworkUserPage(homeworkView);
	}
    *//**
                   * 编辑作业
     * @param HomeworkView home 作业实例对象入参 
     * @return
     *//*
    @RequestMapping("/studentTaskInit.action")
    public ModelAndView studentTaskInit(HomeworkView home){
    	//作业编辑页面
        ModelAndView result = new ModelAndView("course/manager/home_student_work");
        //获取当前登录者账号
        UserEntity user = WebContext.getSessionUser();
        home.setCreateUser(user.getUserId());
        HomeworkView homework=courseVRService.searchStudentHomeWork(home);
        //前台页面信息回显对象
        result.addObject("homework", homework);
        result = getRoleAndNum(result);
        return result;
    }
    *//**
                  * 保存作业信息
     * @param id  作业id
     * @param studentComment 学生提交内容
     * @param homeanwserId 作业作答id
     * @param status 提交作业状态
     * @return
     * @throws Exception
     *//*
    @RequestMapping("/saveMystudy.action")
    @ResponseBody
    public ResultBean saveMystudy(String id,String studentComment,String homeanwserId,String status) throws Exception {
        ResultBean result = ResultBean.success();
        String userId = WebContext.getSessionUserId();
        //根据homeanwserId判断当前登陆者是否已经作答过改作业
        if(StringUtil.isBlank(homeanwserId)) {
        	//未作答，保存作答内容
        	HomeworkView course = new HomeworkView();
        	//随机生成作业作答id
        	course.setId(IDGenerator.genUID());
        	course.setCreateUser(userId);
        	course.setCreateDate(new Date());
        	//作业作答内容
        	course.setStudentComment(studentComment);
        	course.setHomeworkId(id);
        	course.setAnswerUser(userId);
        	//设置作业状态
        	course.setStatus(HOMEWORK_ANSWER_STATUS_ONE);
        	courseVRService.insertAnswer(course);
        }else {
        	//更新做大内容
        	HomeworkAnswerEntity entity = new HomeworkAnswerEntity();
        	entity.setId(homeanwserId);
        	entity = courseVRService.selectOneAnswer(entity);
        	entity.setStudentComment(studentComment);
        	entity.setUpdateDate(new Date());
        	entity.setUpdateUser(userId);
        	courseVRService.update(entity);
        }
        return result;
    }
    *//**
                 * 学生查看作业列表方法
     * @param home
     * @return
     *//*
    @RequestMapping("/studentSeacherTaskInit.action")
    public ModelAndView studentSeacherTaskInit(HomeworkView home){
    	//学生作业列表页面路径
        ModelAndView result = new ModelAndView("course/manager/home_student_look_work");
        //根据登录id查询作业信息
        UserEntity user = WebContext.getSessionUser();
        home.setCreateUser(user.getUserId());
        HomeworkView homework=courseVRService.searchStudentHomeWork(home);
        //作业信息返回给前台
        result.addObject("homework", homework);
        result = getRoleAndNum(result);
        return result;
    }
    *//**
                  * 查询年级 课程
     * @param id 课程id
     * @return
     *//*
    @RequestMapping("/searchGrade.action")
    @ResponseBody
    public List<StudentClassInfoEntity> searchGrade(String id){
    	//根据课程id查询课程信息
        CourseEntity entity = new CourseEntity();
        entity.setId(id);
        entity = courseService.searchCourse(entity);
        //获取老师所属学校
        String schoolId = entity.getSchoolId();
        StudentClassInfoEntity studentClassInfoEntity = new StudentClassInfoEntity();
        studentClassInfoEntity.setSchoolId(schoolId);
        //根据学校查询年级
        List<StudentClassInfoEntity> gradeList = new ArrayList<StudentClassInfoEntity>();
        gradeList = studentClassInfoService.searchStudentGradeList(studentClassInfoEntity);
        //返回查询的年级集合给前台
        return gradeList;
    }
     *//**
                     * 根据课程主键和年级主键查询班级信息
      * @param courseId  课程主键
      * @param gradeId 年级主键
      * @return
      *//*
    @RequestMapping("/searchClass.action")
    @ResponseBody
    public List<StudentClassInfoEntity> searchClass(String courseId,String gradeId){
    	//根据课程主键查询课程信息
    	 CourseEntity entity = new CourseEntity();
         entity.setId(courseId);
         entity = courseService.searchCourse(entity);
         //获取老师所属学校
         String schoolId = entity.getSchoolId();
        List<StudentClassInfoEntity> gradeList = new ArrayList<StudentClassInfoEntity>();
        //根据学校主键和年级查询班级信息
        StudentClassInfoEntity studentClassInfoEntity = new StudentClassInfoEntity();
        studentClassInfoEntity.setSchoolId(schoolId);//学校id
        studentClassInfoEntity.setGradeId(gradeId);//年级id
        gradeList = studentClassInfoService.searchStudentClassList(studentClassInfoEntity);
        return gradeList;
    }
    *//**
                  * 添加学生时查询学生列表
     * @param UserView query 用户参数 
     * @param courseId 所属课程信息id
     * @return
     *//*
    @RequestMapping("/searchStudentUserAll.action")
    @ResponseBody
    public ResultBean searchStudentUserAll(UserView query,String courseId)  {
    	//根据课程id查询课程所属老师对应的学校信息
    	CourseEntity entity = new CourseEntity();
        entity.setId(courseId);
        entity = courseService.searchCourse(entity);
        //获取老师所属学校
        String schoolId = entity.getSchoolId();
        //根据学校id获取这个学校下面所有的学生信息
        query.setSchoolId(schoolId);
        return courseService.searchStudentUserAll(query);
    }
	*//**
	 * 用户个人中心信息页面方法
	 * @return 返回结果
	 *//*
	@RequestMapping("/userPblInfo.action")
	public ModelAndView userPblInfo(){
		//查询代办信息表里审核通过的代办信息数量
		int num = courseService.getStuStatus();
		//个人中心页面路径参数
		ModelAndView mv = new ModelAndView("trainingplatform/client/center/center_pbl_user");
		//根据用户id查询用户信息
		UserEntity user = userManagementService.seachUserInfo(WebContext.getSessionUserId());
		//返回用户对象
		mv.addObject("user", user);
		mv.addObject("num", num);
		//获取登录人员的角色信息
		UserEntity userRole = WebContext.getSessionUser();
		//判断用户是否登录状态guest为未登录状态
		if(!userRole.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(userRole.getUserId());
			if(roleList.size() > 0) {
				//循环遍历人员角色
				for(UserRoleEntity role : roleList) {
					//角色包含01的设置role字段为t,即老师角色
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				//s为学生角色
				mv.addObject("role", "s");
			}
		}
		UserEntity user1 = WebContext.getSessionUser();
		//获取工号
		String uid =user1.getUserId();
		TpUserEntity userEntity = courseService.searchInfoMessage(uid);
		    //获取人员信息
		        //设置头像字段
				String image = "";
				//设置头像字段
				String school="";
				//设置年级字段
				String grade="";
				//设置班级字段
				String className="";
				if(null!=userEntity && StringUtil.isNotBlank(userEntity.getHeadurl())) {
					//获取头像
					image = userEntity.getHeadurl();
					//获取学校
					school = userEntity.getSchoolName();
				}else {
					//设置默认头像
					image ="http://wisdomsaas-img.oss-cn-beijing.aliyuncs.com/BFAA0FECDD194B7A974C60B7E1B27C18?authorization=bce-auth-v1%2F7642b4b48046452697e90662eed41ace%2F2017-12-29T08%3A33%3A39Z%2F-1%2F%2F0f06024af14dad05694f25b8c85572567591c72bbd0129d3175b4cdbd6e62208";
				}
				//获取班级
				if(!user.getUserName().equals("guest") && "C".equals(WebContext.getSessionUser().getUserKbn())) {
					//实例化学生对象实体类并查询学生信息
					List<StudentClassInfoEntity> entityList = new ArrayList<StudentClassInfoEntity>();
					StudentClassInfoEntity entity = new StudentClassInfoEntity();
					entity.setStudentId(uid);
					entityList = studentClassInfoService.searchStudentClassInfoList(entity);
					//如果学生对象不为空则执行如下赋值
					if(null!=entityList && entityList.size()>0) {
						grade = entityList.get(0).getGradeName();
						className = entityList.get(0).getClassName();
						school =  entityList.get(0).getSchoolName();
					}
				}
				//返回班级信息
				mv.addObject("className", className);
				//向页面返回年级信息
				mv.addObject("grade", grade);
				//返回学生所属学校名称信息
				mv.addObject("school", school);
		       return mv;
	}
	*//**
	 * 批量点评初始化页面方法
	 * @param userIds  学生id集合字符串
	 * @param personNames  学生姓名字符集合
	 * @param id      作业id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/allEvalutionMsg.action")
	public ModelAndView allEvalutionMsg(String userIds,String personNames,String id) throws Exception {
		//批量点评方法
		ModelAndView result = new ModelAndView("/course/client/home_work_teacher_all_review");
		String ids[]=id.split(",");
		HomeworkView view = courseService.getHomeWorkById(ids[0]);
		//判断作业对象返回的时间是否为空，不为空则赋值给新的字段用于前端展示
		if(null!=view.getCreateDate()) {
			if(StringUtil.isNotBlank(sdf.format(view.getCreateDate()))) {
				view.setCreateTime(sdf.format(view.getCreateDate()));
			}
		}
		//收集作业对象
		result.addObject("homeWork", view);
		//收集作业id
		result.addObject("id", id);
		//收集学生账号集合id
		result.addObject("userIds", userIds);
		//收集学生姓名集合
		result.addObject("personNames", personNames);
		//收集作业答案id
		result.addObject("answerIds", id);
		result = getRoleAndNum(result);
		return result;
	}
	*//**
	 * 批量点评保存
	 * @param courseJson 前台返回的json字符串
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveAllHomeWorkReviewById.action")
	@ResponseBody
	public ResultBean saveAllHomeWorkReviewById(String courseJson) throws Exception {
		//将字符串转化成实例化对象
		HomeworkView homework = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, HomeworkView.class);
		return courseService.saveAllHomeWorkReviewById(homework);
	}
	*//**
	 * 一键催交
	 * @param userIds  学生id集合字符串参数
	 * @param courseId 课程id
	 * @param homeworkId  作业id
	 * @return
	 * @throws Exception
	 *//*
	@RequestMapping("/saveCjById.action")
	@ResponseBody
	public ResultBean saveCjById(String userIds,String courseId,String homeworkId) throws Exception {
		return courseService.saveACjById(userIds,courseId,homeworkId);
	}
	*//**
	 * 课程考试调用java调查网接口
	 * @param courseId  课程id
	 * @param userId  用户id
	 * @return
	 * @throws ParseException
	 *//*
	@RequestMapping("/searchExamByCourseIdList.action")
	@ResponseBody
	public ResultBean searchExamByCourseIdList(String courseId,String userId) throws ParseException {
		ResultBean result = new ResultBean();
		String param = courseId;
		//声明调用java调查网的webservice接口
		ExamToWebserviceProxy proxy = new ExamToWebserviceProxy();
		String out;
		try {
			//调用外部接口方法
			out = proxy.examList(param,1,20,userId);
			JSONArray jsonArray = new JSONArray(); 
			//将接口返回值转化成数组格式
			jsonArray = jsonArray.fromObject(out);
			UserCourseEntity uc = new UserCourseEntity();
			uc.setCourseId(courseId);
			uc.setUserId(WebContext.getSessionUserId());
			// 检索用户是否存在
			UserCourseEntity uu = courseService.searchOneCourseIdData(uc);
			//根据课程id获取该课程的老师id
			CourseEntity courseEntity = courseService.getCourseId(courseId);
			//转化对应的实体类
			@SuppressWarnings("unchecked")
			List<Questionnaire> list = jsonArray.toList(jsonArray, Questionnaire.class);
			if(null!=list && list.size()>0) {
				//循环遍历集合将字段赋值
				for(int i=0;i<list.size();i++) {
					//已加入的学员或者这个课程的老师创建者都可以看到这个
					if(null!=uu ||(null!=courseEntity && WebContext.getSessionUserId().equals(courseEntity.getTeacherId()))) {
						//已加入课程
						list.get(i).setStudentFlag("0");
						if (list.get(i).getEndDate() != null ) {
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Long end = sdf.parse(list.get(i).getEndDateStr()).getTime();
							Long nowtime = new Date().getTime();
							//判断问卷/考试是否已结束
								if(end-nowtime<0) {
									list.get(i).setExamStatusText("问卷/考试已结束");
									list.get(i).setExamStatus("1");	
								}					
							}
						if (list.get(i).getStartDate() != null ) {
							//将字符串转化成时间再获取秒数
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							Long start = sdf.parse(list.get(i).getStartDateStr()).getTime();
							Long nowtime = new Date().getTime();
							//判断问卷/考试是否开始
								if(start-nowtime>0) {
									list.get(i).setExamStatusText("问卷/考试未开始");
									list.get(i).setExamStatus("1");
								}
							}
					}else {
						list.get(i).setStudentFlag("1");//未加入课程
						list.get(i).setExamStatusText("未加入课程");
					}
					
				}
			}
			 result.setData(list);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}*/


}
