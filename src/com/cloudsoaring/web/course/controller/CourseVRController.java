package com.cloudsoaring.web.course.controller;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.CourseExperimentalResultEntity;
import com.cloudsoaring.web.course.entity.SeriesForm;
import com.cloudsoaring.web.course.service.CourseVRService;
import com.cloudsoaring.web.course.view.HomeworkView;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 
 * @author fanglili
 * @date 2019-04-08
 */
@Controller
@RequestMapping("/pub/courseVR")
@ControllerAdvice
public class CourseVRController extends BaseController implements CourseConstants {
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Autowired
	CourseVRService courseVRService;

	/**
	 * 获取所有人员得分
	 * 
	 * @return JSONArray
	 */
	@RequestMapping("/getAllUserScore.action")
	@ResponseBody
	public JSONArray getAllUserScore() {
		JSONArray json = new JSONArray();
		JSONObject jo = new JSONObject();
		List<String> categoryList = new ArrayList<String>();
		// series
		List<SeriesForm> seriesList = new ArrayList<SeriesForm>();
		for(int i=0;i<4;i++) {
			SeriesForm series = new SeriesForm();
			series.setId(i);
			seriesList.add(series);
		}
		List<HomeworkView> courseVRList = courseVRService.getUserStepScore();
		if(courseVRList.size() > 0) {
			int i =1;
			for(HomeworkView view : courseVRList) {
				if(categoryList.size() == 0) {
					categoryList.add(view.getAnswerUser());
				}else {
					if(!categoryList.contains(view.getAnswerUser())) { 
						categoryList.add(view.getAnswerUser());
					}
				}
			}
			Map<String,Map> userMap = new HashMap<String, Map>();
			for(String s :categoryList) {
				Map map = new HashMap();
				map.put("随文识字", "0");
				map.put("单句训练", "0");
				map.put("分段训练", "0");
				map.put("整篇训练", "0");
				for(HomeworkView view : courseVRList) {
					if(view.getAnswerUser().equals(s)) {
						if(view.getTitle().equals("随文识字")) {
							map.put("随文识字", view.getScore());
						}else if(view.getTitle().equals("单句训练")) {
							map.put("单句训练", view.getScore());
						}else if(view.getTitle().equals("分段训练")) {
							map.put("分段训练", view.getScore());
						}else if(view.getTitle().equals("整篇训练")) {
							map.put("整篇训练", view.getScore());
						}
					}
				}
				userMap.put(s, map);
			}
			for(int j=0;j<seriesList.size();j++)  {
				List<String> seriesData = new ArrayList<String>();
				for(String s :categoryList) {
					if(j == 0) {
						seriesData.add(userMap.get(s).get("随文识字").toString());
					}else if(j == 1) {
						seriesData.add(userMap.get(s).get("单句训练").toString());
					}else if(j == 2) {
						seriesData.add(userMap.get(s).get("分段训练").toString());
					}else if(j == 3) {
						seriesData.add(userMap.get(s).get("整篇训练").toString());
					}
				}
				seriesList.get(j).setData(seriesData);
				i++;
			}
//			for(int j=0;j<seriesList.size();j++) {
//				List<String> seriesData = new ArrayList<String>();
//				for(HomeworkView view : courseVRList) {
//					for(int k=0;k<categoryList.size();k++) {
//						if(view.getAnswerUser().equals(categoryList.get(k))) {
//							if(j == 0) {
//								if(view.getTitle().equals("随文识字")) {
//									seriesData.add(view.getScore());
//								}else {
//									//seriesData.add("0");
//								} 
//							}else if(j == 1) {
//								if(view.getTitle().equals("单句训练")) {
//									seriesData.add(view.getScore());
//								}else {
//									//seriesData.add("0");
//								} 
//							}else if(j == 2) {
//								if(view.getTitle().equals("分段训练")) {
//									seriesData.add(view.getScore());
//								}else {
//									//seriesData.add("0");
//								} 
//							}else if(j == 3) {
//								if(view.getTitle().equals("整篇训练")) {
//									seriesData.add(view.getScore());
//								}else {
//									//seriesData.add("0");
//								} 
//							}
//						}else {
//							//seriesData.add("0");
//						}
//					}
//				}
//				seriesList.get(j).setData(seriesData);
//				i++;
//			}
//			for(HomeworkView view : courseERList) {
//				SeriesForm series = new SeriesForm();
//				series.setId(i);
//				List<String> seriesData = new ArrayList<String>();
//				for(int ii=0;ii<categoryList.size();ii++) {
//					seriesData.add("0");
//				}
//				for(int j=0;j<categoryList.size();j++) {
//					if(view.getAnswerUser().equals(categoryList.get(j))) {
//						if(view.getTitle().equals("随文识字")) {
//							seriesData.set(j, view.getScore());
//						}else if(view.getTitle().equals("单句训练")) {
//							seriesData.set(j, view.getScore());
//						}else if(view.getTitle().equals("分段训练")) {
//							seriesData.set(j, view.getScore());
//						}else if(view.getTitle().equals("整篇训练")) {
//							seriesData.set(j, view.getScore());
//						}
//					}
//				}
//				series.setData(seriesData);
//				seriesList.add(series);
//				i++;
//			}
		}
		jo.put("category", categoryList);
		jo.put("series", seriesList);
		json.add(jo);
		return json;
	}
	
	public static void  main(String[] args) {
		Map<String,Map> map = new HashMap<String, Map>();
		Map map1 = new HashMap();
		map1.put("随文识字", "111");
		map1.put("danjuxunlian", "222");
		map1.put("duan", "333");
		map1.put("pian", "444");
		map.put("zs", map1);
		System.out.println(map.get("zs").get("随文识字"));
		map1.put("随文识字", "222");
		System.out.println(map.get("zs").get("随文识字"));
		System.out.println(map.size());
	}
}
