package com.cloudsoaring.web.course.entity;

import java.util.Date;
import java.util.List;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**
 * @Title: Entity
 * @Description: tb_plan
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class PlanEntity extends BaseEntity {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/** 封面图片ID */
	private String pictureId;
	/** 简介 */
	private String content;
	/** 计划类别（内训/创业） */
	private String type;
	/** 是否必修 */
	private String flagRequired;
	/** 浏览数 */
	private java.math.BigDecimal viewNum;
	//分类：1计划，2案例
    private String category;
	/** 标题 */
	private String title;
	/** 学习人数 */
	private java.math.BigDecimal numStudy;
	/** 该计划下所含的计划阶段 */
	private List<PlanNodeEntity> planNodeList;
	/** 当前用户是否加入该计划 */
	private boolean join = false;
	
	/** 课程数 */
	private java.math.BigDecimal numCourse;
	/**课程来源*/
    private String courseSource;
    /**供应商*/
    private String courseSupplier;
    
	public String getCourseSource() {
        return courseSource;
    }

    public void setCourseSource(String courseSource) {
        this.courseSource = courseSource;
    }

    public String getCourseSupplier() {
        return courseSupplier;
    }

    public void setCourseSupplier(String courseSupplier) {
        this.courseSupplier = courseSupplier;
    }

    public java.math.BigDecimal getNumCourse() {
		return numCourse;
	}

	public void setNumCourse(java.math.BigDecimal numCourse) {
		this.numCourse = numCourse;
	}

	/** 周几的索引 1=周日；2=周一 …… 7=周六 */
	private java.math.BigDecimal weekIndex;

	/** 周几对应的日期 */
	private Date dayDate;

	/** 周几对应的学习章节 */
	private java.math.BigDecimal chapterDayNum;

	/** 用户ID */
	private String userId;

	/** 参与计划时间 */
	private Date recordTime;

	/** 参与计划时间Str */
	private String recordTimeStr;

	/** 几天前参与计划 */
	private java.math.BigDecimal intervalDayNum;

	/** 用户名 */
	private String userName;

	/** 用户姓名 */
	private String personName;

	/** 用户头像ID */
	private String image;

	/** 计划总章节数=计划下所有课程的所有章节之和 */
	private java.math.BigDecimal chapterAllNum;

	/** 计划完成章节数=用户章节进度表中课程学习进度为100 的章节之和 */
	private java.math.BigDecimal chapterFinishNum;

	/** 计划学习耗时=计算所学章节的学习耗时之和 */
	private java.math.BigDecimal studyAllTime;
	/** 积分 */
	private java.math.BigDecimal price;

	/** 开始学习时间 */
	private Date planStart;

	/** 结束学习时间 */
	private Date planEnd;

	/** 计划管理员 */
	private String manageUser;

	/**计划管理员*/
	private String manageUserName;
		
	/**折扣数*/
	private java.math.BigDecimal numDiscount;

	/** 是否促销 0为是 1为否 */
	private String flagDiscount;
	/** 学习模式 */
	private String studyModel;
	/** 计划关联的标签ID */
	private String tagIds;

	/** 计划关联的标签NAME */
	private String tagNames;

	/** 计划参与人员列表 */
	private List<PlanUserEntity> planUserList;

	/**计划状态*/
	private String planStatus;
	
	/**是否开始学习 */
	private String isStudy;
	/**最后一次学习课程ID */
	private String lastCourseId;	
	/**一级标签*/
    private String firstTag;    
    private String firstTagName;
    /**二级标签*/
    private String secondTag;   
    private String secondTagName;
	private String manageUserPersonName;

	public String getIsStudy() {
		return isStudy;
	}

	public void setIsStudy(String isStudy) {
		this.isStudy = isStudy;
	}

	public String getLastCourseId() {
		return lastCourseId;
	}

	public void setLastCourseId(String lastCourseId) {
		this.lastCourseId = lastCourseId;
	}

	public String getPlanStatus() {
		return planStatus;
	}

	public void setPlanStatus(String planStatus) {
		this.planStatus = planStatus;
	}

	public String getTagIds() {
		return tagIds;
	}

	public void setTagIds(String tagIds) {
		this.tagIds = tagIds;
	}

	public String getTagNames() {
		return tagNames;
	}

	public void setTagNames(String tagNames) {
		this.tagNames = tagNames;
	}

	public List<PlanUserEntity> getPlanUserList() {
		return planUserList;
	}

	public void setPlanUserList(List<PlanUserEntity> planUserList) {
		this.planUserList = planUserList;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getRecordTime() {
		return recordTime;
	}

	public void setRecordTime(Date recordTime) {
		this.recordTime = recordTime;
	}

	public String getRecordTimeStr() {
		return recordTimeStr;
	}

	public void setRecordTimeStr(String recordTimeStr) {
		this.recordTimeStr = recordTimeStr;
	}

	public java.math.BigDecimal getIntervalDayNum() {
		return intervalDayNum;
	}

	public void setIntervalDayNum(java.math.BigDecimal intervalDayNum) {
		this.intervalDayNum = intervalDayNum;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public java.math.BigDecimal getChapterAllNum() {
		return chapterAllNum;
	}

	public void setChapterAllNum(java.math.BigDecimal chapterAllNum) {
		this.chapterAllNum = chapterAllNum;
	}

	public java.math.BigDecimal getChapterFinishNum() {
		return chapterFinishNum;
	}

	public void setChapterFinishNum(java.math.BigDecimal chapterFinishNum) {
		this.chapterFinishNum = chapterFinishNum;
	}

	public java.math.BigDecimal getStudyAllTime() {
		return studyAllTime;
	}

	public void setStudyAllTime(java.math.BigDecimal studyAllTime) {
		this.studyAllTime = studyAllTime;
	}

	public java.math.BigDecimal getWeekIndex() {
		return weekIndex;
	}

	public void setWeekIndex(java.math.BigDecimal weekIndex) {
		this.weekIndex = weekIndex;
	}

	public Date getDayDate() {
		return dayDate;
	}

	public void setDayDate(Date dayDate) {
		this.dayDate = dayDate;
	}

	public java.math.BigDecimal getChapterDayNum() {
		return chapterDayNum;
	}

	public void setChapterDayNum(java.math.BigDecimal chapterDayNum) {
		this.chapterDayNum = chapterDayNum;
	}

	public boolean isJoin() {
		return join;
	}

	public void setJoin(boolean isJoin) {
		this.join = isJoin;
	}

	public List<PlanNodeEntity> getPlanNodeList() {
		return planNodeList;
	}

	public void setPlanNodeList(List<PlanNodeEntity> planNodeList) {
		this.planNodeList = planNodeList;
	}

	/** 封面图片ID */
	public String getPictureId() {
		return this.pictureId;
	}

	/** 封面图片ID */
	public void setPictureId(String pictureId) {
		this.pictureId = pictureId;
	}

	/** 简介 */
	public String getContent() {
		return this.content;
	}

	/** 简介 */
	public void setContent(String content) {
		this.content = content;
	}

	/** 计划类别（内训/创业） */
	public String getType() {
		return this.type;
	}

	/** 计划类别（内训/创业） */
	public void setType(String type) {
		this.type = type;
	}

	/** 是否必修 */
	public String getFlagRequired() {
		return this.flagRequired;
	}

	/** 是否必修 */
	public void setFlagRequired(String flagRequired) {
		this.flagRequired = flagRequired;
	}

	/** 浏览数 */
	public java.math.BigDecimal getViewNum() {
		return this.viewNum;
	}

	/** 浏览数 */
	public void setViewNum(java.math.BigDecimal viewNum) {
		this.viewNum = viewNum;
	}

	/** 标题 */
	public String getTitle() {
		return this.title;
	}

	/** 标题 */
	public void setTitle(String title) {
		this.title = title;
	}

	/** 学习人数 */
	public java.math.BigDecimal getNumStudy() {
		return this.numStudy;
	}

	/** 学习人数 */
	public void setNumStudy(java.math.BigDecimal numStudy) {
		this.numStudy = numStudy;
	}

	public java.math.BigDecimal getPrice() {
		return price;
	}

	public void setPrice(java.math.BigDecimal price) {
		this.price = price;
	}

	public Date getPlanStart() {
		return planStart;
	}

	public void setPlanStart(Date planStart) {
		this.planStart = planStart;
	}

	public Date getPlanEnd() {
		return planEnd;
	}

	public void setPlanEnd(Date planEnd) {
		this.planEnd = planEnd;
	}

	public String getManageUser() {
		return manageUser;
	}

	public void setManageUser(String manageUser) {
		this.manageUser = manageUser;
	}

	public String getStudyModel() {
		return studyModel;
	}

	public void setStudyModel(String studyModel) {
		this.studyModel = studyModel;
	}

	public java.math.BigDecimal getNumDiscount() {
		return numDiscount;
	}

	public void setNumDiscount(java.math.BigDecimal numDiscount) {
		this.numDiscount = numDiscount;
	}

	public String getFlagDiscount() {
		return flagDiscount;
	}

	public void setFlagDiscount(String flagDiscount) {
		this.flagDiscount = flagDiscount;
	}

	public String getManageUserName() {
		return manageUserName;
	}
	public void setManageUserName(String manageUserName) {
		this.manageUserName = manageUserName;
	}

	public String getFirstTag() {
		return firstTag;
	}

	public void setFirstTag(String firstTag) {
		this.firstTag = firstTag;
	}

	public String getFirstTagName() {
		return firstTagName;
	}

	public void setFirstTagName(String firstTagName) {
		this.firstTagName = firstTagName;
	}

	public String getSecondTag() {
		return secondTag;
	}

	public void setSecondTag(String secondTag) {
		this.secondTag = secondTag;
	}

	public String getSecondTagName() {
		return secondTagName;
	}

	public void setSecondTagName(String secondTagName) {
		this.secondTagName = secondTagName;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	public String getManageUserPersonName() {
		return manageUserPersonName;
	}
	public void setManageUserPersonName(String manageUserPersonName) {
		this.manageUserPersonName = manageUserPersonName;
	}
}
