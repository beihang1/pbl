

package com.cloudsoaring.web.course.entity;


import java.util.Date;
import java.util.List;

import com.cloudsoaring.common.annotation.ClassDefine;
import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_course
 * @date 2016-03-01
 * @version V1.0
 *
 */
@ClassDefine
public class CourseUserEvalutionSaveEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	private String courseId;
	private String userId;
	private String weidu1;
	private String weidu2;
	private String weidu3;
	private String weidu4;
	private String weidu5;
	private String weidu6;
	private String degree;
	
	
	public String getCourseId() {
		return courseId;
	}
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getWeidu1() {
		return weidu1;
	}
	public void setWeidu1(String weidu1) {
		this.weidu1 = weidu1;
	}
	public String getWeidu2() {
		return weidu2;
	}
	public void setWeidu2(String weidu2) {
		this.weidu2 = weidu2;
	}
	public String getWeidu3() {
		return weidu3;
	}
	public void setWeidu3(String weidu3) {
		this.weidu3 = weidu3;
	}
	public String getWeidu4() {
		return weidu4;
	}
	public void setWeidu4(String weidu4) {
		this.weidu4 = weidu4;
	}
	public String getWeidu5() {
		return weidu5;
	}
	public void setWeidu5(String weidu5) {
		this.weidu5 = weidu5;
	}
	public String getDegree() {
		return degree;
	}
	public void setDegree(String degree) {
		this.degree = degree;
	}
	public String getWeidu6() {
		return weidu6;
	}
	public void setWeidu6(String weidu6) {
		this.weidu6 = weidu6;
	}
	
}
