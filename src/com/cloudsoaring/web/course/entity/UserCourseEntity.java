

package com.cloudsoaring.web.course.entity;


import java.util.List;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_user_course
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class UserCourseEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**用户ID*/
	private String userId;
	/**课程ID*/
	private String courseId;
	/**是否加入课程（0:未加入,1已加入）*/
	private java.math.BigDecimal joinCourse;
	/**加入课程时间*/
	private java.util.Date joinDate;
	/**用户投票选项*/
	private String vote;
	/**投票时间*/
	private java.util.Date voteDate;
	/**是否收藏*/
	private java.math.BigDecimal favorite;
	/**收藏时间*/
	private java.util.Date favoriteDate;
	/**最近学习的章节ID*/
	private String lastChapterId;
	/**课程学习进度(百分比）*/
	private java.math.BigDecimal studyProgress;
	/**学习用时(秒)*/
	private java.math.BigDecimal timeCost;
	/**课程名称*/
	private String courseName;
	/**好评百分比*/
	private java.math.BigDecimal voteGood;
	/**中评百分比*/
	private java.math.BigDecimal voteNormal;
	/**差评百分比*/
	private java.math.BigDecimal voteNotGood;
	/**好评票数*/
	private int goodVote;
	/**中评票数*/
	private int normalVote;
	/**差评票数*/
	private int notGoodVote;
	/**总票数*/
	private int totalVote;
	private String personName;//用户名
	
	private String pictureId;
	private List<CourseUserEvalutionEntity> groupList;
	private List<Double> scorseList;
	
	public String getPictureId() {
		return pictureId;
	}
	public void setPictureId(String pictureId) {
		this.pictureId = pictureId;
	}
	public java.math.BigDecimal getVoteGood() {
		return voteGood;
	}
	public void setVoteGood(java.math.BigDecimal voteGood) {
		this.voteGood = voteGood;
	}
	public java.math.BigDecimal getVoteNormal() {
		return voteNormal;
	}
	public void setVoteNormal(java.math.BigDecimal voteNormal) {
		this.voteNormal = voteNormal;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	/**
	 * 评价内容
	 */
	private String voteContent;
	/**加入方式*/
	private java.math.BigDecimal joinType;
	
	public java.math.BigDecimal getJoinType() {
        return joinType;
    }
    public void setJoinType(java.math.BigDecimal joinType) {
        this.joinType = joinType;
    }
    /**用户ID*/
	public String getUserId(){
		return this.userId;
	}
	/**用户ID*/
	public void setUserId(String userId){
		this.userId = userId;
	}
	/**课程ID*/
	public String getCourseId(){
		return this.courseId;
	}
	/**课程ID*/
	public void setCourseId(String courseId){
		this.courseId = courseId;
	}
	/**是否加入课程（0:未加入,1已加入）*/
	public java.math.BigDecimal getJoinCourse(){
		return this.joinCourse;
	}
	/**是否加入课程（0:未加入,1已加入）*/
	public void setJoinCourse(java.math.BigDecimal joinCourse){
		this.joinCourse = joinCourse;
	}
	/**加入课程时间*/
	public java.util.Date getJoinDate(){
		return this.joinDate;
	}
	/**加入课程时间*/
	public void setJoinDate(java.util.Date joinDate){
		this.joinDate = joinDate;
	}
	/**用户投票选项*/
	public String getVote(){
		return this.vote;
	}
	/**用户投票选项*/
	public void setVote(String vote){
		this.vote = vote;
	}
	/**投票时间*/
	public java.util.Date getVoteDate(){
		return this.voteDate;
	}
	/**投票时间*/
	public void setVoteDate(java.util.Date voteDate){
		this.voteDate = voteDate;
	}
	/**是否收藏*/
	public java.math.BigDecimal getFavorite(){
		return this.favorite;
	}
	/**是否收藏*/
	public void setFavorite(java.math.BigDecimal favorite){
		this.favorite = favorite;
	}
	/**收藏时间*/
	public java.util.Date getFavoriteDate(){
		return this.favoriteDate;
	}
	/**收藏时间*/
	public void setFavoriteDate(java.util.Date favoriteDate){
		this.favoriteDate = favoriteDate;
	}
	/**最近学习的章节ID*/
	public String getLastChapterId(){
		return this.lastChapterId;
	}
	/**最近学习的章节ID*/
	public void setLastChapterId(String lastChapterId){
		this.lastChapterId = lastChapterId;
	}
	/**课程学习进度(百分比）*/
	public java.math.BigDecimal getStudyProgress(){
		return this.studyProgress;
	}
	/**课程学习进度(百分比）*/
	public void setStudyProgress(java.math.BigDecimal studyProgress){
		this.studyProgress = studyProgress;
	}
	/**学习用时(秒)*/
	public java.math.BigDecimal getTimeCost(){
		return this.timeCost;
	}
	/**学习用时(秒)*/
	public void setTimeCost(java.math.BigDecimal timeCost){
		this.timeCost = timeCost;
	}
	public String getVoteContent() {
		return voteContent;
	}
	public void setVoteContent(String voteContent) {
		this.voteContent = voteContent;
	}
	public java.math.BigDecimal getVoteNotGood() {
		return voteNotGood;
	}
	public void setVoteNotGood(java.math.BigDecimal voteNotGood) {
		this.voteNotGood = voteNotGood;
	}
	public int getGoodVote() {
		return goodVote;
	}
	public void setGoodVote(int goodVote) {
		this.goodVote = goodVote;
	}
	public int getNormalVote() {
		return normalVote;
	}
	public void setNormalVote(int normalVote) {
		this.normalVote = normalVote;
	}
	public int getNotGoodVote() {
		return notGoodVote;
	}
	public void setNotGoodVote(int notGoodVote) {
		this.notGoodVote = notGoodVote;
	}
	public int getTotalVote() {
		return totalVote;
	}
	public void setTotalVote(int totalVote) {
		this.totalVote = totalVote;
	}

	public List<CourseUserEvalutionEntity> getGroupList() {
		return groupList;
	}
	public void setGroupList(List<CourseUserEvalutionEntity> groupList) {
		this.groupList = groupList;
	}
	public List<Double> getScorseList() {
		return scorseList;
	}
	public void setScorseList(List<Double> scorseList) {
		this.scorseList = scorseList;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	
}
