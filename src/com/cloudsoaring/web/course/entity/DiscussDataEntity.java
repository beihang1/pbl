

package com.cloudsoaring.web.course.entity;



/**   
 * @Title: Entity
 * @Description: tb_discuss_data
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class DiscussDataEntity extends DiscussEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**讨论ID*/
	private String discussId;
	/**发表用户ID*/
	private String userId;
	/**评论内容*/
	private String content;
	/**要回复的评论ID*/
	private String targetDiscussId;
	/**要回复的用户ID*/
	private String targetUserId;
	/**是否可删除评论*/
	private Boolean canDelete = false;
	public String userName;
	//用户头像
	private String image;
	/** 用户名*/
	private String personName;
	/** 参与讨论的话题*/
	private String title;
	/** 辅导定义ID*/
	private String tutorDefId;
	//回复id
	private String targetDiscussUserId;
	//回复人头像
	private String replyImage;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	/**讨论ID*/
	public String getDiscussId(){
		return this.discussId;
	}
	/**讨论ID*/
	public void setDiscussId(String discussId){
		this.discussId = discussId;
	}
	/**发表用户ID*/
	public String getUserId(){
		return this.userId;
	}
	/**发表用户ID*/
	public void setUserId(String userId){
		this.userId = userId;
	}
	/**评论内容*/
	public String getContent(){
		return this.content;
	}
	/**评论内容*/
	public void setContent(String content){
		this.content = content;
	}
	/**要回复的评论ID*/
	public String getTargetDiscussId(){
		return this.targetDiscussId;
	}
	/**要回复的评论ID*/
	public void setTargetDiscussId(String targetDiscussId){
		this.targetDiscussId = targetDiscussId;
	}
	/**要回复的用户ID*/
	public String getTargetUserId(){
		return this.targetUserId;
	}
	/**要回复的用户ID*/
	public void setTargetUserId(String targetUserId){
		this.targetUserId = targetUserId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public Boolean getCanDelete() {
		return canDelete;
	}
	public void setCanDelete(Boolean canDelete) {
		this.canDelete = canDelete;
	}
	/**
	 * @return the tutorDefId
	 */
	public String getTutorDefId() {
		return tutorDefId;
	}
	/**
	 * @param tutorDefId the tutorDefId to set
	 */
	public void setTutorDefId(String tutorDefId) {
		this.tutorDefId = tutorDefId;
	}
	public String getTargetDiscussUserId() {
		return targetDiscussUserId;
	}
	public void setTargetDiscussUserId(String targetDiscussUserId) {
		this.targetDiscussUserId = targetDiscussUserId;
	}
	public String getReplyImage() {
		return replyImage;
	}
	public void setReplyImage(String replyImage) {
		this.replyImage = replyImage;
	}
	
	
}
