package com.cloudsoaring.web.course.entity;

import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;

public class EnumUtil {

    private static <T> Object getMethodValue(String methodName, T obj,
                                             Object... args
    ) {
        Object resut = "";
        try {
            /********************************* start *****************************************/
            Method[] methods = obj.getClass().getMethods(); //获取方法数组，这里只要共有的方法
            if (methods.length <= 0) {
                return resut;
            }
            // String methodstr=Arrays.toString(obj.getClass().getMethods());
            // if(methodstr.indexOf(methodName)<0){ //只能粗略判断如果同时存在 getValue和getValues可能判断错误
            // return resut;
            // }
            // List<Method> methodNamelist=Arrays.asList(methods); //这样似乎还要循环取出名称
            Method method = null;
            for (int i = 0, len = methods.length; i < len; i++) {
                if (methods[i].getName().equalsIgnoreCase(methodName)) { //忽略大小写取方法
                    // isHas = true;
                    methodName = methods[i].getName(); //如果存在，则取出正确的方法名称
                    method = methods[i];
                    break;
                }
            }
            // if(!isHas){
            // return resut;
            // }
            /*************************** end ***********************************************/
            // Method method = obj.getClass().getDeclaredMethod(methodName); ///确定方法
            if (method == null) {
                return resut;
            }
            resut = method.invoke(obj, args); //方法执行
            if (resut == null) {
                resut = "";
            }
            return resut; //返回结果
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resut;
    }


    public static <T> String getNameByValue(Object value, Class<T> enumT,
                                            String... methodNames) {
        if (!enumT.isEnum()) {
            return "";
        }
        T[] enums = enumT.getEnumConstants();
        if (enums == null || enums.length <= 0) {
            return "";
        }
        int count = methodNames.length;
        String valueMathod = "getValue"; //默认方法
        if (count >= 1 && !"".equals(methodNames[0])) { //独立方法
            valueMathod = methodNames[0];
        }
        for (int i = 0, len = enums.length; i < len; i++) {
            T tobj = enums[i];
            try {
                Object resultValue = getMethodValue(valueMathod, tobj);
                if (resultValue != null
                        && resultValue.toString().equals(value + "")) { //存在则返回对应值
                    return tobj + "";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "";
    }


    public static <T> Map<Object, String> ValueAndDescMap(Class<T> enumT) {
        Map<Object, String> enummap = new LinkedHashMap<Object, String>();
        if (!enumT.isEnum()) {
            return enummap;
        }
        T[] enums = enumT.getEnumConstants();
        if (enums == null || enums.length <= 0) {
            return enummap;
        }

        String valueMathod = "getValue"; //默认接口value方法
        String desMathod = "getDescription";//默认接口description方法

        for (int i = 0, len = enums.length; i < len; i++) {
            T tobj = enums[i];
            try {

                Object resultValue = getMethodValue(valueMathod, tobj); //获取value值
                if ("".equals(resultValue)) {
                    continue;
                }
                Object resultDes = getMethodValue(desMathod, tobj); //获取description描述值
                if ("".equals(resultDes)) { //如果描述不存在获取属性值
                    resultDes = tobj;
                }

                enummap.put(resultValue, resultDes + "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return enummap;
    }

    public static <T> Map<Object, String> ValueAndNameMap(Class<T> enumT) {
        Map<Object, String> enummap = new LinkedHashMap<Object, String>();
        if (!enumT.isEnum()) {
            return enummap;
        }
        T[] enums = enumT.getEnumConstants();
        if (enums == null || enums.length <= 0) {
            return enummap;
        }

        String valueMathod = "getValue"; //默认接口value方法
        String desMathod = "getDescription";//默认接口description方法

        for (int i = 0, len = enums.length; i < len; i++) {
            T tobj = enums[i];
            try {

                Object resultValue = getMethodValue(valueMathod, tobj); //获取value值
                if ("".equals(resultValue)) {
                    continue;
                }
                String field = (String) EnumUtil.getNameByValue(resultValue, enumT);

                enummap.put(resultValue, field);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return enummap;
    }

    public static <T> Map< String,Object> NameAndValueMap(Class<T> enumT) {
        Map< String,Object> enummap = new LinkedHashMap<String,Object>();
        if (!enumT.isEnum()) {
            return enummap;
        }
        T[] enums = enumT.getEnumConstants();
        if (enums == null || enums.length <= 0) {
            return enummap;
        }

        String valueMathod = "getValue"; //默认接口value方法
        String desMathod = "getDescription";//默认接口description方法

        for (int i = 0, len = enums.length; i < len; i++) {
            T tobj = enums[i];
            try {

                Object resultValue = getMethodValue(valueMathod, tobj); //获取value值
                if ("".equals(resultValue)) {
                    continue;
                }
                String field = (String) EnumUtil.getNameByValue(resultValue, enumT);

                enummap.put(field,resultValue);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return enummap;
    }
    public static void main(String[] args) {
//
//        Map<Object, String> map= EnumUtil.ValueAndNameMap(EnumData.TAS.class);
//        String enumNameByValue = getNameByValue(1, EnumData.TAS.class);
//        int a2 = Enum.valueOf(EnumData.TAS.class, "A").getValue();
//

//        int a=3;

        String a = "3";
        String b = "3";
        System.out.println("字符串比较"+(a==b));
        System.out.println("字符串比较"+a.equals(b));
    }
}
