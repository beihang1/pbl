

package com.cloudsoaring.web.course.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_plan_course
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class PlanCourseEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**节点ID*/
	private String planNodeId;
	/**
	 * 计划ID
	 */
	private String planId;
	/**课程ID*/
	private String courseId;
	/**排序*/
	private java.math.BigDecimal courseOrder;
	
	/**节点ID*/
	public String getPlanNodeId(){
		return this.planNodeId;
	}
	/**节点ID*/
	public void setPlanNodeId(String planNodeId){
		this.planNodeId = planNodeId;
	}
	/**课程ID*/
	public String getCourseId(){
		return this.courseId;
	}
	/**课程ID*/
	public void setCourseId(String courseId){
		this.courseId = courseId;
	}
	public java.math.BigDecimal getCourseOrder() {
		return courseOrder;
	}
	public void setCourseOrder(java.math.BigDecimal courseOrder) {
		this.courseOrder = courseOrder;
	}
	public String getPlanId() {
		return planId;
	}
	public void setPlanId(String planId) {
		this.planId = planId;
	}
}
