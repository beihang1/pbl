package com.cloudsoaring.web.course.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_course_experimental_result
 * @date 2019-03-12
 * @version V1.0
 *
 */
public class CourseExperimentalResultEntity extends BaseEntity{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	/**大步骤ID*/
	private String firstStepId;
	/**小步骤ID*/
	private String secondStepId;
	/**大步骤名称*/
	private String firstStepName;
	/**小步骤名称*/
	private String secondStepName;
	/**用户ID*/
	private String userId;
	/**用户名称*/
	private String userName;
	/**创建时间*/
	private Date createTime;
	/**得分*/
	private BigDecimal score;
	/**时长*/
	private BigDecimal duration;
	/**课程ID*/
	private String courseID;
	/**章节ID*/
	private String chapterId;
	/**开始时间*/
	private Date enterTime;
	/**实际操作*/
	private String actualOperation;
	/**标准操作*/
	private String standardOperation;
	
	public String getFirstStepId() {
		return firstStepId;
	}
	public void setFirstStepId(String firstStepId) {
		this.firstStepId = firstStepId;
	}
	public String getSecondStepId() {
		return secondStepId;
	}
	public void setSecondStepId(String secondStepId) {
		this.secondStepId = secondStepId;
	}
	public String getFirstStepName() {
		return firstStepName;
	}
	public void setFirstStepName(String firstStepName) {
		this.firstStepName = firstStepName;
	}
	public String getSecondStepName() {
		return secondStepName;
	}
	public void setSecondStepName(String secondStepName) {
		this.secondStepName = secondStepName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public BigDecimal getScore() {
		return score;
	}
	public void setScore(BigDecimal score) {
		this.score = score;
	}
	public BigDecimal getDuration() {
		return duration;
	}
	public void setDuration(BigDecimal duration) {
		this.duration = duration;
	}
	public String getCourseID() {
		return courseID;
	}
	public void setCourseID(String courseID) {
		this.courseID = courseID;
	}
	public Date getEnterTime() {
		return enterTime;
	}
	public void setEnterTime(Date enterTime) {
		this.enterTime = enterTime;
	}
	public String getActualOperation() {
		return actualOperation;
	}
	public void setActualOperation(String actualOperation) {
		this.actualOperation = actualOperation;
	}
	public String getStandardOperation() {
		return standardOperation;
	}
	public void setStandardOperation(String standardOperation) {
		this.standardOperation = standardOperation;
	}
	public String getChapterId() {
		return chapterId;
	}
	public void setChapterId(String chapterId) {
		this.chapterId = chapterId;
	}
	
	
	
}
