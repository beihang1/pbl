

package com.cloudsoaring.web.course.entity;


import java.util.Date;

import com.cloudsoaring.common.annotation.ClassDefine;
import com.cloudsoaring.common.annotation.FieldDefine;
import com.cloudsoaring.common.validate.FieldType;
import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_chapter
 * @date 2016-03-01
 * @version V1.0
 *
 */
@ClassDefine(startRow=14)
public class ChapterActiveEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;
	//章节ID
	private String chapterId;
	//活动章节标题
	private String chapterTitle;
	//活动属性，课前-1，课中-2，课后-3
	private String activeFlag;
	//活动记录内容
	private String content;
	//创建时间
	private Date createDate;
	//创建人
	private String createUser;
	//更新时间
	private Date updateDate;
	//更新人
	private String updateUser;
	//课程主键
	private String courseId;
	//教师名称
	private String personName;
	//课程
	private String title;
	//创建时间
	private String createDateStr;
	//课程性质
	private String flag;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getChapterId() {
		return chapterId;
	}
	public void setChapterId(String chapterId) {
		this.chapterId = chapterId;
	}
	public String getChapterTitle() {
		return chapterTitle;
	}
	public void setChapterTitle(String chapterTitle) {
		this.chapterTitle = chapterTitle;
	}
	public String getActiveFlag() {
		return activeFlag;
	}
	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getCourseId() {
		return courseId;
	}
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCreateDateStr() {
		return createDateStr;
	}
	public void setCreateDateStr(String createDateStr) {
		this.createDateStr = createDateStr;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	 
	
}
