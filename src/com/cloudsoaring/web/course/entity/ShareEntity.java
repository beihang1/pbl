

package com.cloudsoaring.web.course.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_share
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class ShareEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**标题*/
	private String title;
	/**内容*/
	private String content;
	/**图片ID*/
	private String pictureId;
	/**状态（0：未发布，1：已发布）*/
	private String status;
	/**案例关联_课程ID*/
	private String relationCourseId;
	/**案例关联_章节ID*/
	private String relationChapterId;
	/**是否开启评论*/
	private String flagDiscuss;
	/**浏览数*/
	private java.math.BigDecimal numView;
	/**点赞数*/
	private java.math.BigDecimal numScoreUser;
	/**标签*/
	private String ownTagName;
	/**标签ID*/
	private String ownTagId;
	
	public String getOwnTagName() {
		return ownTagName;
	}
	public void setOwnTagName(String ownTagName) {
		this.ownTagName = ownTagName;
	}
	public String getOwnTagId() {
		return ownTagId;
	}
	public void setOwnTagId(String ownTagId) {
		this.ownTagId = ownTagId;
	}
	/**标题*/
	public String getTitle(){
		return this.title;
	}
	/**标题*/
	public void setTitle(String title){
		this.title = title;
	}
	/**内容*/
	public String getContent(){
		return this.content;
	}
	/**内容*/
	public void setContent(String content){
		this.content = content;
	}
	/**图片ID*/
	public String getPictureId(){
		return this.pictureId;
	}
	/**图片ID*/
	public void setPictureId(String pictureId){
		this.pictureId = pictureId;
	}
	/**状态（0：未发布，1：已发布）*/
	public String getStatus(){
		return this.status;
	}
	/**状态（0：未发布，1：已发布）*/
	public void setStatus(String status){
		this.status = status;
	}
	/**案例关联_课程ID*/
	public String getRelationCourseId(){
		return this.relationCourseId;
	}
	/**案例关联_课程ID*/
	public void setRelationCourseId(String relationCourseId){
		this.relationCourseId = relationCourseId;
	}
	/**案例关联_章节ID*/
	public String getRelationChapterId(){
		return this.relationChapterId;
	}
	/**案例关联_章节ID*/
	public void setRelationChapterId(String relationChapterId){
		this.relationChapterId = relationChapterId;
	}
	/**是否开启评论*/
	public String getFlagDiscuss(){
		return this.flagDiscuss;
	}
	/**是否开启评论*/
	public void setFlagDiscuss(String flagDiscuss){
		this.flagDiscuss = flagDiscuss;
	}
	/**浏览数*/
	public java.math.BigDecimal getNumView(){
		return this.numView;
	}
	/**浏览数*/
	public void setNumView(java.math.BigDecimal numView){
		this.numView = numView;
	}
	/**点赞数*/
	public java.math.BigDecimal getNumScoreUser(){
		return this.numScoreUser;
	}
	/**点赞数*/
	public void setNumScoreUser(java.math.BigDecimal numScoreUser){
		this.numScoreUser = numScoreUser;
	}
}
