

package com.cloudsoaring.web.course.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_user_measuerment_answer
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class UserMeasuermentAnswerEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**用户ID*/
	private String userId;
	/**题目ID*/
	private String questionId;
	/**答案ID*/
	private String optionId;
	/**试卷ID*/
    private String measuermentId;
    /**问题*/
    private String questionContent;
    /**选项*/
    private String optionContent;
    /**选项排序*/
    private String optionOrder;
    private String questionOrder;
    /*
                  * 填空或简单的内容
     */
    private String content;
    
	public String getQuestionOrder() {
        return questionOrder;
    }
    public void setQuestionOrder(String questionOrder) {
        this.questionOrder = questionOrder;
    }
    public String getQuestionContent() {
        return questionContent;
    }
    public void setQuestionContent(String questionContent) {
        this.questionContent = questionContent;
    }
    public String getOptionContent() {
        return optionContent;
    }
    public void setOptionContent(String optionContent) {
        this.optionContent = optionContent;
    }
    public String getOptionOrder() {
        return optionOrder;
    }
    public void setOptionOrder(String optionOrder) {
        this.optionOrder = optionOrder;
    }
    public String getMeasuermentId() {
        return measuermentId;
    }
    public void setMeasuermentId(String measuermentId) {
        this.measuermentId = measuermentId;
    }
    /**用户ID*/
	public String getUserId(){
		return this.userId;
	}
	/**用户ID*/
	public void setUserId(String userId){
		this.userId = userId;
	}
	/**题目ID*/
	public String getQuestionId(){
		return this.questionId;
	}
	/**题目ID*/
	public void setQuestionId(String questionId){
		this.questionId = questionId;
	}
	/**答案ID*/
	public String getOptionId(){
		return this.optionId;
	}
	/**答案ID*/
	public void setOptionId(String optionId){
		this.optionId = optionId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
}
