

package com.cloudsoaring.web.course.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_chapter_file
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class ChapterFileEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**CHAPTER_ID*/
	private String chapterId;
	/**FILE_ID*/
	private String fileId;
	
	/**CHAPTER_ID*/
	public String getChapterId(){
		return this.chapterId;
	}
	/**CHAPTER_ID*/
	public void setChapterId(String chapterId){
		this.chapterId = chapterId;
	}
	/**FILE_ID*/
	public String getFileId(){
		return this.fileId;
	}
	/**FILE_ID*/
	public void setFileId(String fileId){
		this.fileId = fileId;
	}
}
