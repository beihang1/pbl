package com.cloudsoaring.web.course.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_force
 * @date 2019-09-25
 * @version V1.0
 *
 */
public class BbsReplyEntity extends BaseEntity{

	/**serialVersionUID*/
	private static final long serialVersionUID = 1L;
	
	private String bbsId;
	/**内容*/
	private String content;
	
	/**创建时间*/
	private Date createTime;
	
	/**创建用户*/
	private String createUser;
	
	private String personName;
	
	public String getBbsId() {
		return bbsId;
	}

	public void setBbsId(String bbsId) {
		this.bbsId = bbsId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getCreateTimeStr() {
		return createTimeStr;
	}

	public void setCreateTimeStr(String createTimeStr) {
		this.createTimeStr = createTimeStr;
	}

	private String createTimeStr;

	
}
