

package com.cloudsoaring.web.course.entity;


import java.util.Date;
import java.util.List;

import com.cloudsoaring.common.annotation.ClassDefine;
import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_course
 * @date 2016-03-01
 * @version V1.0
 *
 */
@ClassDefine
public class CourseUserEvalutionEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**课程id*/
	private String courseId;
	/**教师id*/
	private String teacherId;
	private String userId;
	private String userName;
	private java.util.Date createTime;
	private java.util.Date updateTime;
	private String weidu;
	private java.math.BigDecimal score;
	private String degree;
	private List<CourseUserEvalutionEntity> evalutionList;
	private List<Double> scorseList;
	private String title;//课程名字
	private String teacherName;//老师名字
	public String getCourseId() {
		return courseId;
	}
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
	public String getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public java.util.Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(java.util.Date createTime) {
		this.createTime = createTime;
	}
	public java.util.Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(java.util.Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getWeidu() {
		return weidu;
	}
	public void setWeidu(String weidu) {
		this.weidu = weidu;
	}
	public java.math.BigDecimal getScore() {
		return score;
	}
	public void setScore(java.math.BigDecimal score) {
		this.score = score;
	}
	public String getDegree() {
		return degree;
	}
	public void setDegree(String degree) {
		this.degree = degree;
	}
	public List<CourseUserEvalutionEntity> getEvalutionList() {
		return evalutionList;
	}
	public void setEvalutionList(List<CourseUserEvalutionEntity> evalutionList) {
		this.evalutionList = evalutionList;
	}
	public List<Double> getScorseList() {
		return scorseList;
	}
	public void setScorseList(List<Double> scorseList) {
		this.scorseList = scorseList;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTeacherName() {
		return teacherName;
	}
	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}
	

	
}
