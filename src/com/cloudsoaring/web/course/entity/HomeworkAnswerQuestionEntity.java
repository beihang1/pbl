

package com.cloudsoaring.web.course.entity;


import java.util.Date;

import com.cloudsoaring.web.common.entity.BaseEntity;

public class HomeworkAnswerQuestionEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	private String content;
	private String answerId;
	private String createUser;
	private Date createDate;
	private String questionUserName;
	private String questionUserImage;
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getAnswerId() {
		return answerId;
	}
	public void setAnswerId(String answerId) {
		this.answerId = answerId;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getQuestionUserName() {
		return questionUserName;
	}
	public void setQuestionUserName(String questionUserName) {
		this.questionUserName = questionUserName;
	}
	public String getQuestionUserImage() {
		return questionUserImage;
	}
	public void setQuestionUserImage(String questionUserImage) {
		this.questionUserImage = questionUserImage;
	}
	
	
}
