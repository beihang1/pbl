

package com.cloudsoaring.web.course.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_operate_record
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class OperateRecordEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**操作类型(笔记点赞，笔记采集）*/
	private String operateType;
	/**操作对象ID*/
	private String link;
	/**操作用户ID*/
	private String operateUser;
	/**备注*/
	private String remark;
	
	/**操作类型(笔记点赞，笔记采集）*/
	public String getOperateType(){
		return this.operateType;
	}
	/**操作类型(笔记点赞，笔记采集）*/
	public void setOperateType(String operateType){
		this.operateType = operateType;
	}
	/**操作对象ID*/
	public String getLink(){
		return this.link;
	}
	/**操作对象ID*/
	public void setLink(String link){
		this.link = link;
	}
	/**操作用户ID*/
	public String getOperateUser(){
		return this.operateUser;
	}
	/**操作用户ID*/
	public void setOperateUser(String operateUser){
		this.operateUser = operateUser;
	}
	/**备注*/
	public String getRemark(){
		return this.remark;
	}
	/**备注*/
	public void setRemark(String remark){
		this.remark = remark;
	}
}
