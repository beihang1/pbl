

package com.cloudsoaring.web.course.entity;


import java.util.List;

import com.cloudsoaring.common.annotation.ClassDefine;
import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_course
 * @date 2016-03-01
 * @version V1.0
 *
 */
@ClassDefine
public class EvalutionTimeEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	private String userId;//用户id
	private String echarts;//图形数据
	private String tianshu;//学习天数
	private String djs;//等级内容
	private String tcs;//突出内容
	private String zds;//中等内容
	private String dxs;//地下内容
	private java.util.Date createDate;//创建时间
	private String createDateStr;//创建时间字符串
	private List<Double> echartsList;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getEcharts() {
		return echarts;
	}
	public void setEcharts(String echarts) {
		this.echarts = echarts;
	}
	public String getTianshu() {
		return tianshu;
	}
	public void setTianshu(String tianshu) {
		this.tianshu = tianshu;
	}
	public String getDjs() {
		return djs;
	}
	public void setDjs(String djs) {
		this.djs = djs;
	}
	public String getTcs() {
		return tcs;
	}
	public void setTcs(String tcs) {
		this.tcs = tcs;
	}
	public String getZds() {
		return zds;
	}
	public void setZds(String zds) {
		this.zds = zds;
	}
	public String getDxs() {
		return dxs;
	}
	public void setDxs(String dxs) {
		this.dxs = dxs;
	}
	public java.util.Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}
	public String getCreateDateStr() {
		return createDateStr;
	}
	public void setCreateDateStr(String createDateStr) {
		this.createDateStr = createDateStr;
	}
	public List<Double> getEchartsList() {
		return echartsList;
	}
	public void setEchartsList(List<Double> echartsList) {
		this.echartsList = echartsList;
	}
	
	
}
