

package com.cloudsoaring.web.course.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_discuss
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class DiscussEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**讨论标题*/
	private String title;
	/**链接ID*/
	private String linkId;
	/**链接类型(COURSE:课程,CHAPTER:章节)*/
	private String linkType;
	
	//回复人姓名
	private String replyName;
	//回复内容
	private String replyContent;
	//回复时间
	private String replyDate;
	/**讨论标题*/
	public String getTitle(){
		return this.title;
	}
	/**讨论标题*/
	public void setTitle(String title){
		this.title = title;
	}
	/**链接ID*/
	public String getLinkId(){
		return this.linkId;
	}
	/**链接ID*/
	public void setLinkId(String linkId){
		this.linkId = linkId;
	}
	/**链接类型(COURSE:课程,CHAPTER:章节)*/
	public String getLinkType(){
		return this.linkType;
	}
	/**链接类型(COURSE:课程,CHAPTER:章节)*/
	public void setLinkType(String linkType){
		this.linkType = linkType;
	}
	public String getReplyName() {
		return replyName;
	}
	public void setReplyName(String replyName) {
		this.replyName = replyName;
	}
	public String getReplyContent() {
		return replyContent;
	}
	public void setReplyContent(String replyContent) {
		this.replyContent = replyContent;
	}
	public String getReplyDate() {
		return replyDate;
	}
	public void setReplyDate(String replyDate) {
		this.replyDate = replyDate;
	}
	
}
