

package com.cloudsoaring.web.course.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: his_user_measuerment_answer
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class HisUserMeasuermentAnswerEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**用户ID*/
	private String userId;
	/**题目ID*/
	private String questionId;
	/**答案ID*/
	private String optionId;
	
	/**用户ID*/
	public String getUserId(){
		return this.userId;
	}
	/**用户ID*/
	public void setUserId(String userId){
		this.userId = userId;
	}
	/**题目ID*/
	public String getQuestionId(){
		return this.questionId;
	}
	/**题目ID*/
	public void setQuestionId(String questionId){
		this.questionId = questionId;
	}
	/**答案ID*/
	public String getOptionId(){
		return this.optionId;
	}
	/**答案ID*/
	public void setOptionId(String optionId){
		this.optionId = optionId;
	}
}
