

package com.cloudsoaring.web.course.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_teacher_apply
 * @date 2016-03-23
 * @version V1.0
 *
 */
public class TeacherApplyEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**USER_ID*/
	private String userId;
	/**姓名*/
	private String personName;
	/**职业*/
	private String position;
	/**联系方式*/
	private String connectWay;
	/**个人简介*/
	private String summary;
	/**试讲视频*/
	private String testVideo;
	/**状态*/
	private String status;
	/**提交时间*/
	private java.util.Date submitTime;
	/**审核人ID*/
	private String applyUser;
	/**审核时间*/
	private java.util.Date applyTime;
	/**审核意见*/
	private String applyOpinion;
	/**标签*/
	private String tags;
	/**标签名*/
	private String tagNames;
	/**话题内容*/
	private String topicContents;
	
	public String getTopicContents() {
        return topicContents;
    }
    public void setTopicContents(String topicContents) {
        this.topicContents = topicContents;
    }
    /**USER_ID*/
	public String getUserId(){
		return this.userId;
	}
	/**USER_ID*/
	public void setUserId(String userId){
		this.userId = userId;
	}
	/**姓名*/
	public String getPersonName(){
		return this.personName;
	}
	/**姓名*/
	public void setPersonName(String personName){
		this.personName = personName;
	}
	/**职业*/
	public String getPosition(){
		return this.position;
	}
	/**职业*/
	public void setPosition(String position){
		this.position = position;
	}
	/**联系方式*/
	public String getConnectWay(){
		return this.connectWay;
	}
	/**联系方式*/
	public void setConnectWay(String connectWay){
		this.connectWay = connectWay;
	}
	/**个人简介*/
	public String getSummary(){
		return this.summary;
	}
	/**个人简介*/
	public void setSummary(String summary){
		this.summary = summary;
	}
	/**试讲视频*/
	public String getTestVideo(){
		return this.testVideo;
	}
	/**试讲视频*/
	public void setTestVideo(String testVideo){
		this.testVideo = testVideo;
	}
	/**状态*/
	public String getStatus(){
		return this.status;
	}
	/**状态*/
	public void setStatus(String status){
		this.status = status;
	}
	/**提交时间*/
	public java.util.Date getSubmitTime(){
		return this.submitTime;
	}
	/**提交时间*/
	public void setSubmitTime(java.util.Date submitTime){
		this.submitTime = submitTime;
	}
	/**审核人ID*/
	public String getApplyUser(){
		return this.applyUser;
	}
	/**审核人ID*/
	public void setApplyUser(String applyUser){
		this.applyUser = applyUser;
	}
	/**审核时间*/
	public java.util.Date getApplyTime(){
		return this.applyTime;
	}
	/**审核时间*/
	public void setApplyTime(java.util.Date applyTime){
		this.applyTime = applyTime;
	}
	/**审核意见*/
	public String getApplyOpinion(){
		return this.applyOpinion;
	}
	/**审核意见*/
	public void setApplyOpinion(String applyOpinion){
		this.applyOpinion = applyOpinion;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	public String getTagNames() {
		return tagNames;
	}
	public void setTagNames(String tagNames) {
		this.tagNames = tagNames;
	}
		
}
