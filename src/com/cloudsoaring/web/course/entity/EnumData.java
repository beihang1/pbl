package com.cloudsoaring.web.course.entity;

interface EnumCommon {
    public int getValue();
    //public String getDescription();
}
public class EnumData {



    public enum TAS {
        A(1),
        B(2);
        private int value;

        public int getValue() {
            return value;
        }

        TAS(int value) {
            this.value = value;
        }
    }

    public enum PageSize implements EnumCommon {
        每页5条(5),
        每页10条(10),
        每页15条(15),
        每页20条(20),
        每页50条(50),
        每页100条(100);
        private int value;

        @Override
        public int getValue() {
            return value;
        }

        PageSize(int value) { //构造初始化赋值
            this.value = value;
        }
    }
    public enum Questionnaire_Type implements EnumCommon {
        问卷调查(1),
        在线考试(2);
        private int value;

        @Override
        public int getValue() {
            return value;
        }

        Questionnaire_Type(int value) { //构造初始化赋值
            this.value = value;
        }
    }
    public enum Questionnaire_Status implements EnumCommon {
        未发布(1),
        已发布(2),
        已暂停(3);
        private int value;

        @Override
        public int getValue() {
            return value;
        }

        Questionnaire_Status(int value) { //构造初始化赋值
            this.value = value;
        }
    }

    public enum Question_Type implements EnumCommon {
        单选(1),
        多选(2),
        填空(3),
        矩阵(4),
        段落说明(5),
        排序(6),
        下拉菜单(7),
        二级下拉菜单(8) ;
        private int value;

        @Override
        public int getValue() {
            return value;
        }

        Question_Type(int value) { //构造初始化赋值
            this.value = value;
        }
    }

    public enum Collection_Question_Type implements EnumCommon {
        单选(1),
        多选(2),
        填空(3);
        private int value;

        @Override
        public int getValue() {
            return value;
        }

        Collection_Question_Type(int value) { //构造初始化赋值
            this.value = value;
        }
    }

    public enum Question_TianKongType implements EnumCommon {
        单行输入(1),
        多行输入(2),
        数字(3),
        手机号(4),
        邮箱(5),
        身份证号(6),
        网址(7),
        日期(8),
        时间(9);
        private int value;

        @Override
        public int getValue() {
            return value;
        }

        Question_TianKongType(int value) { //构造初始化赋值
            this.value = value;
        }
    }


    public enum Question_JuZhenType implements EnumCommon {
        单选(1),
        多选(2),
        文本(3),
        数字(4);
        private int value;

        @Override
        public int getValue() {
            return value;
        }

        Question_JuZhenType(int value) { //构造初始化赋值
            this.value = value;
        }
    }
    public enum Question_RowCounts implements EnumCommon {
        横排(0),
        竖排(1),
        每行2个(2),
        每行3个(3),
        每行4个(4);
        private int value;

        @Override
        public int getValue() {
            return value;
        }

        Question_RowCounts(int value) { //构造初始化赋值
            this.value = value;
        }
    }


    public enum Questionnaire_SendType implements EnumCommon {
        公开问卷(1),
        单密码问卷(2),
        密码列表问卷(3),
        短信验证问卷(4);
        private int value;

        @Override
        public int getValue() {
            return value;
        }

        Questionnaire_SendType(int value) { //构造初始化赋值
            this.value = value;
        }
    }

    public enum Record_Status implements EnumCommon {
        未查看(0),
        未作答(1),
        已作答(2);
        private int value;

        @Override
        public int getValue() {
            return value;
        }

        Record_Status(int value) { //构造初始化赋值
            this.value = value;
        }
    }

    public enum Answer_Record_Source implements EnumCommon {
        手机(1),
        电脑(2);
        private int value;

        @Override
        public int getValue() {
            return value;
        }

        Answer_Record_Source(int value) { //构造初始化赋值
            this.value = value;
        }
    }
}
