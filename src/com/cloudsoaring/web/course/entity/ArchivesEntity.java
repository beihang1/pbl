package com.cloudsoaring.web.course.entity;

import java.util.Date;
import java.util.List;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**
 * 个人档案信息Entity
 * @author liuyanshuang
 *
 */
public class ArchivesEntity extends BaseEntity{

	private static final long serialVersionUID = 1L;
	
	private String userId;
	private String userName;
	private String createTime;
	private Date createDate;
	private String info;
	private String content;
	private String exitImage;//0:文字；1：图片；2：视频；3：附件
	private String imageId;
	private List<ArchivesDiscussEntity> discussList;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getImageId() {
		return imageId;
	}
	public void setImageId(String imageId) {
		this.imageId = imageId;
	}
	public String getExitImage() {
		return exitImage;
	}
	public void setExitImage(String exitImage) {
		this.exitImage = exitImage;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	public List<ArchivesDiscussEntity> getDiscussList() {
		return discussList;
	}
	public void setDiscussList(List<ArchivesDiscussEntity> discussList) {
		this.discussList = discussList;
	}
	
}
