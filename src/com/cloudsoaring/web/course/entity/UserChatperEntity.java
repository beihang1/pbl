

package com.cloudsoaring.web.course.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_user_chatper
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class UserChatperEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**用户ID*/
	private String userId;
	/**章节ID*/
	private String chapterId;
	/**投票选项*/
	private String vote;
	/**投票时间*/
	private java.util.Date voteDate;
	/**视频学习时间戳*/
	private java.math.BigDecimal lastVideoTime;
	/**课程学习进度(百分比）*/
	private java.math.BigDecimal studyProgress;
	/**学习用时(秒)*/
	private java.math.BigDecimal timeCost;
	
	/**用户ID*/
	public String getUserId(){
		return this.userId;
	}
	/**用户ID*/
	public void setUserId(String userId){
		this.userId = userId;
	}
	/**章节ID*/
	public String getChapterId(){
		return this.chapterId;
	}
	/**章节ID*/
	public void setChapterId(String chapterId){
		this.chapterId = chapterId;
	}
	/**投票选项*/
	public String getVote(){
		return this.vote;
	}
	/**投票选项*/
	public void setVote(String vote){
		this.vote = vote;
	}
	/**投票时间*/
	public java.util.Date getVoteDate(){
		return this.voteDate;
	}
	/**投票时间*/
	public void setVoteDate(java.util.Date voteDate){
		this.voteDate = voteDate;
	}
	/**视频学习时间戳*/
	public java.math.BigDecimal getLastVideoTime(){
		return this.lastVideoTime;
	}
	/**视频学习时间戳*/
	public void setLastVideoTime(java.math.BigDecimal lastVideoTime){
		this.lastVideoTime = lastVideoTime;
	}
	/**课程学习进度(百分比）*/
	public java.math.BigDecimal getStudyProgress(){
		return this.studyProgress;
	}
	/**课程学习进度(百分比）*/
	public void setStudyProgress(java.math.BigDecimal studyProgress){
		this.studyProgress = studyProgress;
	}
	/**学习用时(秒)*/
	public java.math.BigDecimal getTimeCost(){
		return this.timeCost;
	}
	/**学习用时(秒)*/
	public void setTimeCost(java.math.BigDecimal timeCost){
		this.timeCost = timeCost;
	}
}
