package com.cloudsoaring.web.course.entity;

import java.util.Date;

import com.cloudsoaring.web.common.entity.BaseEntity;
public class ArchivesDiscussEntity extends BaseEntity{

	private static final long serialVersionUID = 1L;
	
	private String archivesId;
	private String userId;
	private String userName;
	private String content;
	private Date createTime;
	public String getArchivesId() {
		return archivesId;
	}
	public void setArchivesId(String archivesId) {
		this.archivesId = archivesId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
