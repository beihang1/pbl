

package com.cloudsoaring.web.course.entity;


import java.util.ArrayList;
import java.util.List;
import com.cloudsoaring.web.common.entity.BaseEntity;




/**   
 * @Title: Entity
 * @Description: tb_question
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class QuestionEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**测验ID*/
	private String measureId;
	/**测验类型*/
	private String measuretype;
	/**题目描述*/
	private String content;
	/**排序号*/
	private java.math.BigDecimal questionOrder;
	/**REFERENCE_ANSWER_ID*/
	private String referenceAnswerId;
	/**分值*/
	private java.math.BigDecimal score;
	/**题目类型（1：单选题，2：多选题，3，判断题）*/
	private String questionType;
	/**参与数*/
	private int numUser;
	/**用户选择的答案*/
	private String userAnswerId;
	/**用户参与过答题的标记*/
	private String userMeasurementFlag; 
	/**选项*/
	private List<QuestionOptionEntity> options=new ArrayList<QuestionOptionEntity>();
	/**测验ID*/
	public String getMeasureId(){
		return this.measureId;
	}
	/**测验ID*/
	public void setMeasureId(String measureId){
		this.measureId = measureId;
	}
	
	public String getMeasuretype() {
        return measuretype;
    }
    public void setMeasuretype(String measuretype) {
        this.measuretype = measuretype;
    }
    /**题目描述*/
	public String getContent(){
		return this.content;
	}
	/**题目描述*/
	public void setContent(String content){
		this.content = content;
	}
	/**REFERENCE_ANSWER_ID*/
	public String getReferenceAnswerId(){
		return this.referenceAnswerId;
	}
	/**REFERENCE_ANSWER_ID*/
	public void setReferenceAnswerId(String referenceAnswerId){
		this.referenceAnswerId = referenceAnswerId;
	}
	/**分值*/
	public java.math.BigDecimal getScore(){
		return this.score;
	}
	/**分值*/
	public void setScore(java.math.BigDecimal score){
		this.score = score;
	}
	/**题目类型（1：单选题，2：多选题，3，判断题）*/
	public String getQuestionType(){
		return this.questionType;
	}
	/**题目类型（1：单选题，2：多选题，3，判断题）*/
	public void setQuestionType(String questionType){
		this.questionType = questionType;
	}
	public java.math.BigDecimal getQuestionOrder() {
		return questionOrder;
	}
	public void setQuestionOrder(java.math.BigDecimal questionOrder) {
		this.questionOrder = questionOrder;
	}
	public String getUserAnswerId() {
		return userAnswerId;
	}
	public void setUserAnswerId(String userAnswerId) {
		this.userAnswerId = userAnswerId;
	}
	public String getUserMeasurementFlag() {
		return userMeasurementFlag;
	}
	public void setUserMeasurementFlag(String userMeasurementFlag) {
		this.userMeasurementFlag = userMeasurementFlag;
	}
	public List<QuestionOptionEntity> getOptions() {
		return options;
	}
	public void setOptions(List<QuestionOptionEntity> options) {
		this.options = options;
	}
	public int getNumUser() {
		return numUser;
	}
	public void setNumUser(int numUser) {
		this.numUser = numUser;
	}
	
}
