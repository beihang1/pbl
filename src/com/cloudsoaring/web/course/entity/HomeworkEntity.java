

package com.cloudsoaring.web.course.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_homework
 * @date 2016-03-18
 * @version V1.0
 *
 */
public class HomeworkEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**LINK_TYPE*/
	private String linkType;
	/**LINK_ID*/
	private String linkId;
	/**TITLE*/
	private String title;
	/**CONTENT*/
	private String content;
	/**附件ID*/
	private String fileId;
	/**linkTitle*/
	private String linkTitle;
	
	
	
	
	
	public String getLinkTitle() {
		return linkTitle;
	}
	public void setLinkTitle(String linkTitle) {
		this.linkTitle = linkTitle;
	}
	/**LINK_TYPE*/
	public String getLinkType(){
		return this.linkType;
	}
	/**LINK_TYPE*/
	public void setLinkType(String linkType){
		this.linkType = linkType;
	}
	/**LINK_ID*/
	public String getLinkId(){
		return this.linkId;
	}
	/**LINK_ID*/
	public void setLinkId(String linkId){
		this.linkId = linkId;
	}
	/**TITLE*/
	public String getTitle(){
		return this.title;
	}
	/**TITLE*/
	public void setTitle(String title){
		this.title = title;
	}
	/**CONTENT*/
	public String getContent(){
		return this.content;
	}
	/**CONTENT*/
	public void setContent(String content){
		this.content = content;
	}
	/**附件ID*/
	public String getFileId(){
		return this.fileId;
	}
	/**附件ID*/
	public void setFileId(String fileId){
		this.fileId = fileId;
	}
	
	
}
