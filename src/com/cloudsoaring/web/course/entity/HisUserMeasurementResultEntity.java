

package com.cloudsoaring.web.course.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: his_user_measurement_result
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class HisUserMeasurementResultEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**测验ID*/
	private String measurementId;
	/**用户ID*/
	private String userId;
	/**得分*/
	private java.math.BigDecimal goal;
	/**开始时间*/
	private java.util.Date startDate;
	/**提交时间*/
	private java.util.Date submitDate;
	/**提交类型*/
	private String submitType;
	/**考试状态，一般为空值, 1/开始， 2/结束*/
	private String examStatus;
	
	/**测验ID*/
	public String getMeasurementId(){
		return this.measurementId;
	}
	/**测验ID*/
	public void setMeasurementId(String measurementId){
		this.measurementId = measurementId;
	}
	/**用户ID*/
	public String getUserId(){
		return this.userId;
	}
	/**用户ID*/
	public void setUserId(String userId){
		this.userId = userId;
	}
	/**得分*/
	public java.math.BigDecimal getGoal(){
		return this.goal;
	}
	/**得分*/
	public void setGoal(java.math.BigDecimal goal){
		this.goal = goal;
	}
	/**开始时间*/
	public java.util.Date getStartDate(){
		return this.startDate;
	}
	/**开始时间*/
	public void setStartDate(java.util.Date startDate){
		this.startDate = startDate;
	}
	/**提交时间*/
	public java.util.Date getSubmitDate(){
		return this.submitDate;
	}
	/**提交时间*/
	public void setSubmitDate(java.util.Date submitDate){
		this.submitDate = submitDate;
	}
	/**提交类型*/
	public String getSubmitType(){
		return this.submitType;
	}
	/**提交类型*/
	public void setSubmitType(String submitType){
		this.submitType = submitType;
	}
	/**考试状态，一般为空值, 1/开始， 2/结束*/
	public String getExamStatus(){
		return this.examStatus;
	}
	/**考试状态，一般为空值, 1/开始， 2/结束*/
	public void setExamStatus(String examStatus){
		this.examStatus = examStatus;
	}
}
