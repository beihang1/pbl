

package com.cloudsoaring.web.course.entity;


import java.util.ArrayList;
import java.util.List;

import com.cloudsoaring.common.annotation.ClassDefine;
import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_course
 * @date 2016-03-01
 * @version V1.0
 *
 */
@ClassDefine
public class PjScoreEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	private String pjScore;
	private List<Double> echarts;
	private String num;//学习天数
	private List<String> tuchu;//突出
	private List<String> zhongdeng;//中等
	private List<String> dixia;//低下
	public String getPjScore() {
		return pjScore;
	}
	public void setPjScore(String pjScore) {
		this.pjScore = pjScore;
	}
	public List<Double> getEcharts() {
		return echarts;
	}
	public void setEcharts(List<Double> echarts) {
		this.echarts = echarts;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public List<String> getTuchu() {
		return tuchu;
	}
	public void setTuchu(List<String> tuchu) {
		this.tuchu = tuchu;
	}
	public List<String> getZhongdeng() {
		return zhongdeng;
	}
	public void setZhongdeng(List<String> zhongdeng) {
		this.zhongdeng = zhongdeng;
	}
	public List<String> getDixia() {
		return dixia;
	}
	public void setDixia(List<String> dixia) {
		this.dixia = dixia;
	}
	
	
	
}
