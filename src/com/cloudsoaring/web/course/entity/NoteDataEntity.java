

package com.cloudsoaring.web.course.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_note_data
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class NoteDataEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**笔记ID*/
	private String noteId;
	/**发表用户ID*/
	private String userId;
	
	/**用户名*/
	private String personName;
	
	/**用户图像Id*/
	private String userImageId;
	/**笔记内容*/
	private String content;
	/**截图时间戳*/
	private java.math.BigDecimal screenshotTime;
	/**截图文件ID*/
	private String screenshotId;
	/**点赞数*/
	private java.math.BigDecimal likeNum;
	/**采集数*/
	private java.math.BigDecimal collectionNum;
	/**用户是否点赞，值为0时，未点赞*/
	private Integer userLike;
	/**用户是否采集，值为0时，未采集*/
	private Integer userCollection;
	/**当前登录用户的ID*/
	private String loginId;
	/**是否可删除笔记*/
	private Boolean canDelete = false;
	
	/**笔记合集标题*/
	private String title;
	//检索条件
	 
	/**链接ID*/
	private String linkId;
	
	/**链接类型(COURSE:课程,CHAPTER:章节)*/
	private String linkType;
	/**页面类型*/
	private String pageType;
	
	private String fileId;
	
	private String type;
	
	private String fileName;
	/**笔记ID*/
	public String getNoteId(){
		return this.noteId;
	}
	/**笔记ID*/
	public void setNoteId(String noteId){
		this.noteId = noteId;
	}
	/**发表用户ID*/
	public String getUserId(){
		return this.userId;
	}
	/**发表用户ID*/
	public void setUserId(String userId){
		this.userId = userId;
	}
	/**笔记内容*/
	public String getContent(){
		return this.content;
	}
	/**笔记内容*/
	public void setContent(String content){
		this.content = content;
	}
	/**截图时间戳*/
	public java.math.BigDecimal getScreenshotTime(){
		return this.screenshotTime;
	}
	/**截图时间戳*/
	public void setScreenshotTime(java.math.BigDecimal screenshotTime){
		this.screenshotTime = screenshotTime;
	}
	/**截图文件ID*/
	public String getScreenshotId(){
		return this.screenshotId;
	}
	/**截图文件ID*/
	public void setScreenshotId(String screenshotId){
		this.screenshotId = screenshotId;
	}
	/**点赞数*/
	public java.math.BigDecimal getLikeNum(){
		return this.likeNum;
	}
	/**点赞数*/
	public void setLikeNum(java.math.BigDecimal likeNum){
		this.likeNum = likeNum;
	}
	/**采集数*/
	public java.math.BigDecimal getCollectionNum(){
		return this.collectionNum;
	}
	/**采集数*/
	public void setCollectionNum(java.math.BigDecimal collectionNum){
		this.collectionNum = collectionNum;
	}
	 
	public String getLinkId() {
		return linkId;
	}
	public void setLinkId(String linkId) {
		this.linkId = linkId;
	}
	public String getLinkType() {
		return linkType;
	}
	public void setLinkType(String linkType) {
		this.linkType = linkType;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getUserImageId() {
		return userImageId;
	}
	public void setUserImageId(String userImageId) {
		this.userImageId = userImageId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPageType() {
		return pageType;
	}
	public void setPageType(String pageType) {
		this.pageType = pageType;
	}
	public Integer getUserLike() {
		return userLike;
	}
	public void setUserLike(Integer userLike) {
		this.userLike = userLike;
	}
	public Integer getUserCollection() {
		return userCollection;
	}
	public void setUserCollection(Integer userCollection) {
		this.userCollection = userCollection;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public Boolean getCanDelete() {
		return canDelete;
	}
	public void setCanDelete(Boolean canDelete) {
		this.canDelete = canDelete;
	}
	
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
