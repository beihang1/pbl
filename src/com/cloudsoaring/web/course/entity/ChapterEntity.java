

package com.cloudsoaring.web.course.entity;


import com.cloudsoaring.common.annotation.ClassDefine;
import com.cloudsoaring.common.annotation.FieldDefine;
import com.cloudsoaring.common.validate.FieldType;
import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_chapter
 * @date 2016-03-01
 * @version V1.0
 *
 */
@ClassDefine(startRow=14)
public class ChapterEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;
	/**课程ID*/
	private String courseId;
	/**标题*/
	@FieldDefine(description="标题",maxLength="100",required=true,type=FieldType.String, columnIndex ="A")
	private String title;
	/**内容*/
	
	private String content;
	private String contentApp;
	/**视频ID*/
	@FieldDefine(description="视频ID",maxLength="50",required=true,type=FieldType.String, columnIndex ="C")
	private String videoId;
	/**排序*/
	private java.math.BigDecimal chapterOrder;
	/**图片ID*/
	private String pictureId;
	/**章节FLAG(1:大章节,0:小节)*/
	private java.math.BigDecimal chapterFlag;
	/**好评百分比*/
	private java.math.BigDecimal voteGood;
	/**中评百分比*/
	private java.math.BigDecimal voteNormal;
	/**差评百分比*/
	private java.math.BigDecimal voteNotGood;
	/**好评票数*/
	private int goodVote;
	/**总票数*/
	private int totalVote;
	/**本地视频文件ID*/
	private String localVideoId;
	//课程名称
	private String courseName;
	//考试名称
	private String examName;
	//得分
	private String goal;
	
	private String isMakerFlag;
	
	public String getIsMakerFlag() {
		return isMakerFlag;
	}
	public void setIsMakerFlag(String isMakerFlag) {
		this.isMakerFlag = isMakerFlag;
	}
	public java.math.BigDecimal getVoteNotGood() {
		return voteNotGood;
	}
	public void setVoteNotGood(java.math.BigDecimal voteNotGood) {
		this.voteNotGood = voteNotGood;
	}
	public int getGoodVote() {
		return goodVote;
	}
	public void setGoodVote(int goodVote) {
		this.goodVote = goodVote;
	}
	public int getNormalVote() {
		return normalVote;
	}
	public void setNormalVote(int normalVote) {
		this.normalVote = normalVote;
	}
	public int getNotGoodVote() {
		return notGoodVote;
	}
	public void setNotGoodVote(int notGoodVote) {
		this.notGoodVote = notGoodVote;
	}
	/**中评票数*/
	private int normalVote;
	/**差评票数*/
	private int notGoodVote;
	
	/**状态（0：未发布，1：已发布）*/
	private String status;
	/**章节类型(1:普通，2:考试，3：练习，4：视频)*/
	private String measureType;
	/**考试时长(分钟)*/
	private java.math.BigDecimal timeDuration;
	/**考试结束时间*/
	private java.util.Date effectiveEndDate;
	/**考试开始时间*/
	private java.util.Date effectiveStartDate;
	/**父章节ID*/
	private String parentId;
	/**视频时长（秒）*/
	private java.math.BigDecimal videoLength;
	/**教师ID*/
	private String teacherId;
	/**简介*/
	@FieldDefine(description="简介",maxLength="1000",required=true,type=FieldType.String, columnIndex ="B")
	private String summary;
	/**是否开启评论*/
	private String flagDiscuss;
	/**是否开启问答*/
	private String flagQa;
	/**是否开启投票*/
	private String flagVote;
	/**是否开启笔记*/
	private String flagNote;
	/**是否开启案例*/
	private String flagShare;
	/**学习人数*/
	private java.math.BigDecimal numStudy;
	/**浏览数*/
	private java.math.BigDecimal numView;
	/**评价人数*/
	private java.math.BigDecimal numScoreUser;
	/**关键词*/
	@FieldDefine(description="关键词",maxLength="50",required=true,type=FieldType.String, columnIndex ="D")
	private String keyWord;
	/**是否开启作业*/
    private String flagTask;
    /**是否开启测试*/
    private String flagTest;
    
    private String videoName; 
    private String measurementId;
    private String isCourse;
    
    
    public String getVideoName() {
        return videoName;
    }
    public void setVideoName(String videoName) {
        this.videoName = videoName;
    }
    public String getFlagTask() {
        return flagTask;
    }
    public void setFlagTask(String flagTask) {
        this.flagTask = flagTask;
    }
    public String getFlagTest() {
        return flagTest;
    }
    public void setFlagTest(String flagTest) {
        this.flagTest = flagTest;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getKeyWord() {
        return keyWord;
    }
    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }
    /**课程ID*/
	public String getCourseId(){
		return this.courseId;
	}
	/**课程ID*/
	public void setCourseId(String courseId){
		this.courseId = courseId;
	}
	/**标题*/
	public String getTitle(){
		return this.title;
	}
	/**标题*/
	public void setTitle(String title){
		this.title = title;
	}
	/**内容*/
	public String getContent(){
		return this.content;
	}
	/**内容*/
	public void setContent(String content){
		this.content = content;
	}
	
	public String getContentApp() {
		return contentApp;
	}
	public void setContentApp(String contentApp) {
		this.contentApp = contentApp;
	}
	/**视频ID*/
	public String getVideoId(){
		return this.videoId;
	}
	/**视频ID*/
	public void setVideoId(String videoId){
		this.videoId = videoId;
	}
	/**图片ID*/
	public String getPictureId(){
		return this.pictureId;
	}
	/**图片ID*/
	public void setPictureId(String pictureId){
		this.pictureId = pictureId;
	}
	/**章节FLAG(1:大章节,0:小节)*/
	public java.math.BigDecimal getChapterFlag(){
		return this.chapterFlag;
	}
	/**章节FLAG(1:大章节,0:小节)*/
	public void setChapterFlag(java.math.BigDecimal chapterFlag){
		this.chapterFlag = chapterFlag;
	}
	/**好评百分比*/
	public java.math.BigDecimal getVoteGood(){
		return this.voteGood;
	}
	/**好评百分比*/
	public void setVoteGood(java.math.BigDecimal voteGood){
		this.voteGood = voteGood;
	}
	/**中评百分比*/
	public java.math.BigDecimal getVoteNormal(){
		return this.voteNormal;
	}
	/**中评百分比*/
	public void setVoteNormal(java.math.BigDecimal voteNormal){
		this.voteNormal = voteNormal;
	}
	/**状态（0：未发布，1：已发布）*/
	public String getStatus(){
		return this.status;
	}
	/**状态（0：未发布，1：已发布）*/
	public void setStatus(String status){
		this.status = status;
	}
	/**测验类型(1:测试，2:考试)*/
	public String getMeasureType(){
		return this.measureType;
	}
	/**测验类型(1:测试，2:考试)*/
	public void setMeasureType(String measureType){
		this.measureType = measureType;
	}
	/**考试时长(分钟)*/
	public java.math.BigDecimal getTimeDuration(){
		return this.timeDuration;
	}
	/**考试时长(分钟)*/
	public void setTimeDuration(java.math.BigDecimal timeDuration){
		this.timeDuration = timeDuration;
	}
	/**考试结束时间*/
	public java.util.Date getEffectiveEndDate(){
		return this.effectiveEndDate;
	}
	/**考试结束时间*/
	public void setEffectiveEndDate(java.util.Date effectiveEndDate){
		this.effectiveEndDate = effectiveEndDate;
	}
	/**考试开始时间*/
	public java.util.Date getEffectiveStartDate(){
		return this.effectiveStartDate;
	}
	/**考试开始时间*/
	public void setEffectiveStartDate(java.util.Date effectiveStartDate){
		this.effectiveStartDate = effectiveStartDate;
	}
	/**父章节ID*/
	public String getParentId(){
		return this.parentId;
	}
	/**父章节ID*/
	public void setParentId(String parentId){
		this.parentId = parentId;
	}
	/**视频时长（秒）*/
	public java.math.BigDecimal getVideoLength(){
		return this.videoLength;
	}
	/**视频时长（秒）*/
	public void setVideoLength(java.math.BigDecimal videoLength){
		this.videoLength = videoLength;
	}
	/**教师ID*/
	public String getTeacherId(){
		return this.teacherId;
	}
	/**教师ID*/
	public void setTeacherId(String teacherId){
		this.teacherId = teacherId;
	}
	/**简介*/
	public String getSummary(){
		return this.summary;
	}
	/**简介*/
	public void setSummary(String summary){
		this.summary = summary;
	}
	/**是否开启评论*/
	public String getFlagDiscuss(){
		return this.flagDiscuss;
	}
	/**是否开启评论*/
	public void setFlagDiscuss(String flagDiscuss){
		this.flagDiscuss = flagDiscuss;
	}
	/**是否开启问答*/
	public String getFlagQa(){
		return this.flagQa;
	}
	/**是否开启问答*/
	public void setFlagQa(String flagQa){
		this.flagQa = flagQa;
	}
	/**是否开启投票*/
	public String getFlagVote(){
		return this.flagVote;
	}
	/**是否开启投票*/
	public void setFlagVote(String flagVote){
		this.flagVote = flagVote;
	}
	/**是否开启笔记*/
	public String getFlagNote(){
		return this.flagNote;
	}
	/**是否开启笔记*/
	public void setFlagNote(String flagNote){
		this.flagNote = flagNote;
	}
	/**是否开启案例*/
	public String getFlagShare(){
		return this.flagShare;
	}
	/**是否开启案例*/
	public void setFlagShare(String flagShare){
		this.flagShare = flagShare;
	}
	/**学习人数*/
	public java.math.BigDecimal getNumStudy(){
		return this.numStudy;
	}
	/**学习人数*/
	public void setNumStudy(java.math.BigDecimal numStudy){
		this.numStudy = numStudy;
	}
	/**浏览数*/
	public java.math.BigDecimal getNumView(){
		return this.numView;
	}
	/**浏览数*/
	public void setNumView(java.math.BigDecimal numView){
		this.numView = numView;
	}
	/**评价人数*/
	public java.math.BigDecimal getNumScoreUser(){
		return this.numScoreUser;
	}
	/**评价人数*/
	public void setNumScoreUser(java.math.BigDecimal numScoreUser){
		this.numScoreUser = numScoreUser;
	}
	public java.math.BigDecimal getChapterOrder() {
		return chapterOrder;
	}
	public void setChapterOrder(java.math.BigDecimal chapterOrder) {
		this.chapterOrder = chapterOrder;
	}
	public int getTotalVote() {
		return totalVote;
	}
	public void setTotalVote(int totalVote) {
		this.totalVote = totalVote;
	}
	public String getLocalVideoId() {
		return localVideoId;
	}
	public void setLocalVideoId(String localVideoId) {
		this.localVideoId = localVideoId;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getExamName() {
		return examName;
	}
	public void setExamName(String examName) {
		this.examName = examName;
	}
	public String getGoal() {
		return goal;
	}
	public void setGoal(String goal) {
		this.goal = goal;
	}
	public String getMeasurementId() {
		return measurementId;
	}
	public void setMeasurementId(String measurementId) {
		this.measurementId = measurementId;
	}
	public String getIsCourse() {
		return isCourse;
	}
	public void setIsCourse(String isCourse) {
		this.isCourse = isCourse;
	}
	
}
