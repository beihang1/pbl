package com.cloudsoaring.web.course.entity;

import java.util.Date;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**
 * 
 * @author fanglili
 * table:tb_scene_info
 *
 */
public class SceneInfoBean {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String sceneId;
	private String createUser;
	private String createDate;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSceneId() {
		return sceneId;
	}
	public void setSceneId(String sceneId) {
		this.sceneId = sceneId;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	
}
