package com.cloudsoaring.web.course.entity;

import java.util.Date;

import com.cloudsoaring.web.common.entity.BaseEntity;

public class CourseApproveEntity extends BaseEntity {
	
	private static final long serialVersionUID = 1L;
	
	private String courseId;
	private String userId;
	private String content;
	private String approvedUserId;
	private String approveStatus;
	private String approveComment;
	private Date createTime;
	private String type;
	
	private String courseName;
	private String personName;
	private String approvePersonName;
	
	public String getCourseId() {
		return courseId;
	}
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getApprovedUserId() {
		return approvedUserId;
	}
	public void setApprovedUserId(String approvedUserId) {
		this.approvedUserId = approvedUserId;
	}
	public String getApproveStatus() {
		return approveStatus;
	}
	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}
	public String getApproveComment() {
		return approveComment;
	}
	public void setApproveComment(String approveComment) {
		this.approveComment = approveComment;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getApprovePersonName() {
		return approvePersonName;
	}
	public void setApprovePersonName(String approvePersonName) {
		this.approvePersonName = approvePersonName;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
