package com.cloudsoaring.web.course.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class Questionnaire {
    private Integer id;

    private Integer memberId;

    private String memberLoginName;

    private Integer questionnaireType;

    private String code;

    private String name;

    private Integer status;

    private String beforeSummary;

    private String afterSummary;

    private Date startDate;

    private Date endDate;

    private Integer maxAnswerCount;

    private Integer answeredCount;

    private Boolean showValidateCode;

    private Boolean showQuestionIndex;

    private Boolean showProgressBar;

    private Integer sendType;

    private String password;

    private String passwordHint;

    private BigDecimal score;
    private Integer examTimeSecond;

    private Boolean deleted;

    private Date createdTime;

    private  Boolean controlAnswerTimes;
    private  Integer controlLevel;
    private  Integer maxAnswerTimes;
    private String courseId;//课程或者章节id
    private String startDateStr;//开始时间字符串
    private String endDateStr;//结束时间字符串
    private String examUrls;//考试链接
    private List<String> list;
    private String questionnaireTypes;//类型
    private String examStatus;//1:已经做答过，2：未作答
    private String examStatusText;//状态内容
    private Integer pageSize = Integer.valueOf(20);
    private String studentFlag;//学生是否点击考试问卷标识 ；0可以点击，1不可以

    private Integer pageNumber = Integer.valueOf(1);
    public Boolean getControlAnswerTimes() {
        return controlAnswerTimes;
    }

    public void setControlAnswerTimes(Boolean controlAnswerTimes) {
        this.controlAnswerTimes = controlAnswerTimes;
    }

    public Integer getControlLevel() {
        return controlLevel;
    }

    public void setControlLevel(Integer controlLevel) {
        this.controlLevel = controlLevel;
    }

    public Integer getMaxAnswerTimes() {
        return maxAnswerTimes;
    }

    public void setMaxAnswerTimes(Integer maxAnswerTimes) {
        this.maxAnswerTimes = maxAnswerTimes;
    }

    public Questionnaire() {
        this.id = 0;
//        this.memberId=0;
//        this.showValidateCode=false;
//        this.showProgressBar=false;
//        this.showQuestionIndex=false;
//        this.score=new BigDecimal(0);
//        this.deleted=false;

    }

    public Map<Object, String> getSendTypeMap(){
        Map<Object, String> map = EnumUtil.ValueAndNameMap(EnumData.Questionnaire_SendType.class);
        return map;
    }
    public String getQuestionnaireTypeDesc(){
        return EnumUtil.getNameByValue(this.questionnaireType, EnumData.Questionnaire_Type.class);
    }
    public String getQuestionnaireStatusDesc(){
        return EnumUtil.getNameByValue(this.questionnaireType, EnumData.Questionnaire_Status.class);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getMemberLoginName() {
        return memberLoginName;
    }

    public void setMemberLoginName(String memberLoginName) {
        this.memberLoginName = memberLoginName == null ? null : memberLoginName.trim();
    }

    public Integer getQuestionnaireType() {
        return questionnaireType;
    }

    public void setQuestionnaireType(Integer questionnaireType) {
        this.questionnaireType = questionnaireType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getBeforeSummary() {
        return beforeSummary;
    }

    public void setBeforeSummary(String beforeSummary) {
        this.beforeSummary = beforeSummary == null ? null : beforeSummary.trim();
    }

    public String getAfterSummary() {
        return afterSummary;
    }

    public void setAfterSummary(String afterSummary) {
        this.afterSummary = afterSummary == null ? null : afterSummary.trim();
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getMaxAnswerCount() {
        return maxAnswerCount;
    }

    public void setMaxAnswerCount(Integer maxAnswerCount) {
        this.maxAnswerCount = maxAnswerCount;
    }

    public Integer getAnsweredCount() {
        return answeredCount;
    }

    public void setAnsweredCount(Integer answeredCount) {
        this.answeredCount = answeredCount;
    }

    public Boolean getShowValidateCode() {
        return showValidateCode;
    }

    public void setShowValidateCode(Boolean showValidateCode) {
        this.showValidateCode = showValidateCode;
    }

    public Boolean getShowQuestionIndex() {
        return showQuestionIndex;
    }

    public void setShowQuestionIndex(Boolean showQuestionIndex) {
        this.showQuestionIndex = showQuestionIndex;
    }

    public Boolean getShowProgressBar() {
        return showProgressBar;
    }

    public void setShowProgressBar(Boolean showProgressBar) {
        this.showProgressBar = showProgressBar;
    }

    public Integer getSendType() {
        return sendType;
    }

    public void setSendType(Integer sendType) {
        this.sendType = sendType;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getPasswordHint() {
        return passwordHint;
    }

    public void setPasswordHint(String passwordHint) {
        this.passwordHint = passwordHint == null ? null : passwordHint.trim();
    }

    public BigDecimal getScore() {
        return score;
    }

    public void setScore(BigDecimal score) {
        this.score = score;
    }
    public Integer getExamTimeSecond() {
        return examTimeSecond;
    }

    public void setExamTimeSecond(Integer examTimeSecond) {
        this.examTimeSecond = examTimeSecond;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getStartDateStr() {
		return startDateStr;
	}

	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}

	public String getEndDateStr() {
		return endDateStr;
	}

	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}

	public String getExamUrls() {
		return examUrls;
	}

	public void setExamUrls(String examUrls) {
		this.examUrls = examUrls;
	}
	
	 public Integer getPageSize() {
		    return Integer.valueOf(this.pageSize != null ? this.pageSize.intValue() : 0);
	  }
	  public void setPageSize(Integer pageSize) {
	    this.pageSize = pageSize;
	  }
	  public Integer getPageNumber() {
		    return Integer.valueOf(this.pageNumber != null ? this.pageNumber.intValue() : 0);
	  }
	  public void setPageNumber(Integer pageNumber) {
	    this.pageNumber = pageNumber;
	  }

	public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

	public String getQuestionnaireTypes() {
		return questionnaireTypes;
	}

	public void setQuestionnaireTypes(String questionnaireTypes) {
		this.questionnaireTypes = questionnaireTypes;
	}

	public String getExamStatus() {
		return examStatus;
	}

	public void setExamStatus(String examStatus) {
		this.examStatus = examStatus;
	}

	public String getExamStatusText() {
		return examStatusText;
	}

	public void setExamStatusText(String examStatusText) {
		this.examStatusText = examStatusText;
	}

	public String getStudentFlag() {
		return studentFlag;
	}

	public void setStudentFlag(String studentFlag) {
		this.studentFlag = studentFlag;
	}
	
	
    
}