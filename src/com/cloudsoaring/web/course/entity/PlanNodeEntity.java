

package com.cloudsoaring.web.course.entity;


import java.util.List;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_plan_node
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class PlanNodeEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	
	private static final long serialVersionUID = 1L;
	private String planNodeId;
	/**计划ID*/
	private String planId;
	/**父节点ID*/
	private String parentId;
	/**节点层级(1，2)*/
	private java.math.BigDecimal level;
	/**排序（同节点下不能重复）*/
	private java.math.BigDecimal nodeOrder;
	/**节点描述*/
	private String content;
	/**节点名称*/
	private String nodeName;
	/**该节点下的课程列表*/
	private List nodeCourses;
	/**该节点下所包含的子节点*/
	private List childPlanNodes;
	/**该节点下的课程*/
	private List<CourseMessageEntity> courses;	
	/**该节点是否可学*/
	private String studyNodeFlag;
	
	public String getStudyNodeFlag() {
		return studyNodeFlag;
	}
	public void setStudyNodeFlag(String studyNodeFlag) {
		this.studyNodeFlag = studyNodeFlag;
	}
	public List getChildPlanNodes() {
		return childPlanNodes;
	}
	public void setChildPlanNodes(List childPlanNodes) {
		this.childPlanNodes = childPlanNodes;
	}
	public String getPlanNodeId() {
		return planNodeId;
	}
	public void setPlanNodeId(String planNodeId) {
		this.planNodeId = planNodeId;
	}
	 
	public List getNodeCourses() {
		return nodeCourses;
	}
	public void setNodeCourses(List nodeCourses) {
		this.nodeCourses = nodeCourses;
	}
	/**计划ID*/
	public String getPlanId(){
		return this.planId;
	}
	/**计划ID*/
	public void setPlanId(String planId){
		this.planId = planId;
	}
	/**父节点ID*/
	public String getParentId(){
		return this.parentId;
	}
	/**父节点ID*/
	public void setParentId(String parentId){
		this.parentId = parentId;
	}
	/**节点层级(1，2)*/
	public java.math.BigDecimal getLevel(){
		return this.level;
	}
	/**节点层级(1，2)*/
	public void setLevel(java.math.BigDecimal level){
		this.level = level;
	}
	/**节点描述*/
	public String getContent(){
		return this.content;
	}
	/**节点描述*/
	public void setContent(String content){
		this.content = content;
	}
	/**节点名称*/
	public String getNodeName(){
		return this.nodeName;
	}
	/**节点名称*/
	public void setNodeName(String nodeName){
		this.nodeName = nodeName;
	}
	public java.math.BigDecimal getNodeOrder() {
		return nodeOrder;
	}
	public void setNodeOrder(java.math.BigDecimal nodeOrder) {
		this.nodeOrder = nodeOrder;
	}
	public List<CourseMessageEntity> getCourses() {
		return courses;
	}
	public void setCourses(List<CourseMessageEntity> courses) {
		this.courses = courses;
	}
}
