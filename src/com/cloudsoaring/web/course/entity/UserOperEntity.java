

package com.cloudsoaring.web.course.entity;


import java.util.Date;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_course
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class UserOperEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	private String userId;
	private String operUser;
	private String operDetail;
	private Date operTime;
	private String operSite;
	private String operIP;
	private String sysInfo;
	
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getOperUser() {
		return operUser;
	}
	public void setOperUser(String operUser) {
		this.operUser = operUser;
	}
	public String getOperDetail() {
		return operDetail;
	}
	public void setOperDetail(String operDetail) {
		this.operDetail = operDetail;
	}
	public Date getOperTime() {
		return operTime;
	}
	public void setOperTime(Date operTime) {
		this.operTime = operTime;
	}
	public String getOperSite() {
		return operSite;
	}
	public void setOperSite(String operSite) {
		this.operSite = operSite;
	}
	
	public String getOperIP() {
		return operIP;
	}
	public void setOperIP(String operIP) {
		this.operIP = operIP;
	}
	public String getSysInfo() {
		return sysInfo;
	}
	public void setSysInfo(String sysInfo) {
		this.sysInfo = sysInfo;
	}
	
}
