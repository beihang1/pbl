

package com.cloudsoaring.web.course.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_score_data
 * @date 2016-03-02
 * @version V1.0
 *
 */
public class ScoreDataEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**评价主题ID*/
	private String scoreId;
	/**发表用户ID*/
	private String userId;
	/**评论内容*/
	private String content;
	/***/
	private java.math.BigDecimal score;
	/***/
	private java.math.BigDecimal subScore1;
	/***/
	private java.math.BigDecimal subScore2;
	/***/
	private java.math.BigDecimal subScore3;
	
	/**评价主题ID*/
	public String getScoreId(){
		return this.scoreId;
	}
	/**评价主题ID*/
	public void setScoreId(String scoreId){
		this.scoreId = scoreId;
	}
	/**发表用户ID*/
	public String getUserId(){
		return this.userId;
	}
	/**发表用户ID*/
	public void setUserId(String userId){
		this.userId = userId;
	}
	/**评论内容*/
	public String getContent(){
		return this.content;
	}
	/**评论内容*/
	public void setContent(String content){
		this.content = content;
	}
	/***/
	public java.math.BigDecimal getScore(){
		return this.score;
	}
	/***/
	public void setScore(java.math.BigDecimal score){
		this.score = score;
	}
	/***/
	public java.math.BigDecimal getSubScore1(){
		return this.subScore1;
	}
	/***/
	public void setSubScore1(java.math.BigDecimal subScore1){
		this.subScore1 = subScore1;
	}
	/***/
	public java.math.BigDecimal getSubScore2(){
		return this.subScore2;
	}
	/***/
	public void setSubScore2(java.math.BigDecimal subScore2){
		this.subScore2 = subScore2;
	}
	/***/
	public java.math.BigDecimal getSubScore3(){
		return this.subScore3;
	}
	/***/
	public void setSubScore3(java.math.BigDecimal subScore3){
		this.subScore3 = subScore3;
	}
}
