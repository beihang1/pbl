

package com.cloudsoaring.web.course.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_qa_answer
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class QaAnswerEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**问题表ID*/
	private String qaQuestionId;
	/**回答用户ID*/
	private String userId;
	/**回答内容*/
	private String content;
	
	public String userName;
	/**用户头像*/
	private String image;
	/**是否需要采纳*/
	private boolean isSend=false;

	/**是否已经采纳*/
	private boolean adopt=false;
	

	

	
	public boolean isAdopt() {
		return adopt;
	}
	public void setAdopt(boolean adopt) {
		this.adopt = adopt;
	}
	public boolean isSend() {
		return isSend;
	}
	public void setSend(boolean isSend) {
		this.isSend = isSend;
	}
	/**问题表ID*/
	public String getQaQuestionId(){
		return this.qaQuestionId;
	}
	/**问题表ID*/
	public void setQaQuestionId(String qaQuestionId){
		this.qaQuestionId = qaQuestionId;
	}
	/**回答用户ID*/
	public String getUserId(){
		return this.userId;
	}
	/**回答用户ID*/
	public void setUserId(String userId){
		this.userId = userId;
	}
	/**回答内容*/
	public String getContent(){
		return this.content;
	}
	/**回答内容*/
	public void setContent(String content){
		this.content = content;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	
}
