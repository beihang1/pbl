package com.cloudsoaring.web.course.entity;

import java.util.Date;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**
 * 
 * @author fanglili
 * table:tb_scene_info
 *
 */
public class SceneInfo extends BaseEntity {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String prefabId;//fileId
	private String position;
	private String rotation;
	private String localScale;
	private String color;
	private String imageUrl;
	private String resourceUrl;
	private String imageName;
	private String currentModelIndex;
	private String selectedId;
	private String sceneId;
	private String createUser;
	private Date createDate;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPrefabId() {
		return prefabId;
	}
	public void setPrefabId(String prefabId) {
		this.prefabId = prefabId;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getRotation() {
		return rotation;
	}
	public void setRotation(String rotation) {
		this.rotation = rotation;
	}
	public String getLocalScale() {
		return localScale;
	}
	public void setLocalScale(String localScale) {
		this.localScale = localScale;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getResourceUrl() {
		return resourceUrl;
	}
	public void setResourceUrl(String resourceUrl) {
		this.resourceUrl = resourceUrl;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public String getCurrentModelIndex() {
		return currentModelIndex;
	}
	public void setCurrentModelIndex(String currentModelIndex) {
		this.currentModelIndex = currentModelIndex;
	}
	public String getSelectedId() {
		return selectedId;
	}
	public void setSelectedId(String selectedId) {
		this.selectedId = selectedId;
	}
	public String getSceneId() {
		return sceneId;
	}
	public void setSceneId(String sceneId) {
		this.sceneId = sceneId;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
}
