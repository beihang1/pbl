

package com.cloudsoaring.web.course.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_plan_study_history
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class PlanStudyHistoryEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**用户ID*/
	private String userId;
	/**章节ID*/
	private String chapterId;
	/**记录日期（天）*/
	private java.util.Date recordTime;
	
	/**计划ID*/
	private String planId;
	/**课程ID*/
	private String courseId;
	
	/**用户ID*/
	public String getUserId(){
		return this.userId;
	}
	/**用户ID*/
	public void setUserId(String userId){
		this.userId = userId;
	}
	/**章节ID*/
	public String getChapterId(){
		return this.chapterId;
	}
	/**章节ID*/
	public void setChapterId(String chapterId){
		this.chapterId = chapterId;
	}
	/**记录日期（天）*/
	public java.util.Date getRecordTime(){
		return this.recordTime;
	}
	/**记录日期（天）*/
	public void setRecordTime(java.util.Date recordTime){
		this.recordTime = recordTime;
	}
	public String getPlanId() {
		return planId;
	}
	public void setPlanId(String planId) {
		this.planId = planId;
	}
	public String getCourseId() {
		return courseId;
	}
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
}
