

package com.cloudsoaring.web.course.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_homework_answer
 * @date 2016-03-18
 * @version V1.0
 *
 */
public class HomeworkAnswerEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**HOMEWORK_ID*/
	private String homeworkId;
	/**ANSWER_USER*/
	private String answerUser;
	/**SCORE*/
	private String score;
	/**TEACHER_COMMENT*/
	private String teacherComment;
	/**CONTENT*/
	private String content;
	/**附件ID*/
	private String fileId;
	private String fileName;
	
	private String audioType;
	private String audioId;
	private String audioContent;
	private java.math.BigDecimal likeNum;
	//分享标识：0取消分享，1分享
	private String share;
	//置顶标识：0取消置顶标识，1置顶标识
	private String top;
	//置顶标识排序字段
	private Integer topNum;
	//课程id
	private String courseId;
	//课程名称
	private String courseName;
	//作业名称
	private String zyName;
	//章节id
    private String chapterId;
	//学生提交作业内容
	private String studentComment;
	//老师点评内容
	private String teacherEvalution;
	private String status;//作业状态，1待查看；2：已查看未评价；3：已评价
	//增加教师评分
	private String teacherScore;
	//
	private String teacherComments;
	public java.math.BigDecimal getLikeNum() {
		return likeNum;
	}
	public void setLikeNum(java.math.BigDecimal likeNum) {
		this.likeNum = likeNum;
	}
	/**HOMEWORK_ID*/
	public String getHomeworkId(){
		return this.homeworkId;
	}
	/**HOMEWORK_ID*/
	public void setHomeworkId(String homeworkId){
		this.homeworkId = homeworkId;
	}
	/**ANSWER_USER*/
	public String getAnswerUser(){
		return this.answerUser;
	}
	/**ANSWER_USER*/
	public void setAnswerUser(String answerUser){
		this.answerUser = answerUser;
	}
	/**SCORE*/
	public String getScore(){
		return this.score;
	}
	/**SCORE*/
	public void setScore(String score){
		this.score = score;
	}
	/**TEACHER_COMMENT*/
	public String getTeacherComment(){
		return this.teacherComment;
	}
	/**TEACHER_COMMENT*/
	public void setTeacherComment(String teacherComment){
		this.teacherComment = teacherComment;
	}
	/**CONTENT*/
	public String getContent(){
		return this.content;
	}
	/**CONTENT*/
	public void setContent(String content){
		this.content = content;
	}
	/**附件ID*/
	public String getFileId(){
		return this.fileId;
	}
	/**附件ID*/
	public void setFileId(String fileId){
		this.fileId = fileId;
	}
	
	public String getAudioType() {
		return audioType;
	}
	public void setAudioType(String audioType) {
		this.audioType = audioType;
	}
	public String getAudioId() {
		return audioId;
	}
	public void setAudioId(String audioId) {
		this.audioId = audioId;
	}
	public String getAudioContent() {
		return audioContent;
	}
	public void setAudioContent(String audioContent) {
		this.audioContent = audioContent;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getShare() {
		return share;
	}
	public void setShare(String share) {
		this.share = share;
	}
	public String getTop() {
		return top;
	}
	public void setTop(String top) {
		this.top = top;
	}
	public Integer getTopNum() {
		return topNum;
	}
	public void setTopNum(Integer topNum) {
		this.topNum = topNum;
	}
	public String getCourseId() {
		return courseId;
	}
	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getZyName() {
		return zyName;
	}
	public void setZyName(String zyName) {
		this.zyName = zyName;
	}
	public String getChapterId() {
		return chapterId;
	}
	public void setChapterId(String chapterId) {
		this.chapterId = chapterId;
	}
	public String getStudentComment() {
		return studentComment;
	}
	public void setStudentComment(String studentComment) {
		this.studentComment = studentComment;
	}
	public String getTeacherEvalution() {
		return teacherEvalution;
	}
	public void setTeacherEvalution(String teacherEvalution) {
		this.teacherEvalution = teacherEvalution;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTeacherScore() {
		return teacherScore;
	}
	public void setTeacherScore(String teacherScore) {
		this.teacherScore = teacherScore;
	}
	public String getTeacherComments() {
		return teacherComments;
	}
	public void setTeacherComments(String teacherComments) {
		this.teacherComments = teacherComments;
	}
	
}
