

package com.cloudsoaring.web.course.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_plan_user
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class PlanUserEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**计划ID*/
	private String planId;
	/**参与的用户ID*/
	private String userId;
	/**参与时间*/
	private java.util.Date recordTime;
	/**加入计划状态*/
	private String flagJoin;
	/**计划下的章节总数*/
	private String chapterAllNum;
	/**已学习完成章节数*/
	private String chapterFinishNum;
	/**学习耗时*/
	private String studyAllTime;
	
	
	public String getChapterAllNum() {
        return chapterAllNum;
    }
    public void setChapterAllNum(String chapterAllNum) {
        this.chapterAllNum = chapterAllNum;
    }
    public String getChapterFinishNum() {
        return chapterFinishNum;
    }
    public void setChapterFinishNum(String chapterFinishNum) {
        this.chapterFinishNum = chapterFinishNum;
    }
    public String getStudyAllTime() {
        return studyAllTime;
    }
    public void setStudyAllTime(String studyAllTime) {
        this.studyAllTime = studyAllTime;
    }
    public String getFlagJoin() {
		return flagJoin;
	}
	public void setFlagJoin(String flagJoin) {
		this.flagJoin = flagJoin;
	}
	/**计划ID*/
	public String getPlanId(){
		return this.planId;
	}
	/**计划ID*/
	public void setPlanId(String planId){
		this.planId = planId;
	}
	/**参与的用户ID*/
	public String getUserId(){
		return this.userId;
	}
	/**参与的用户ID*/
	public void setUserId(String userId){
		this.userId = userId;
	}
	/**参与时间*/
	public java.util.Date getRecordTime(){
		return this.recordTime;
	}
	/**参与时间*/
	public void setRecordTime(java.util.Date recordTime){
		this.recordTime = recordTime;
	}
}
