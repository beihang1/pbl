

package com.cloudsoaring.web.course.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_score
 * @date 2016-03-02
 * @version V1.0
 *
 */
public class ScoreEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**讨论标题*/
	private String title;
	/**链接ID*/
	private String linkId;
	/**链接类型(COURSE:课程,CHAPTER:章节)*/
	private String linkType;
	
	private java.math.BigDecimal score;
	/***/
	private java.math.BigDecimal subScore1;
	/***/
	private java.math.BigDecimal subScore2;
	/***/
	private java.math.BigDecimal subScore3;
	public java.math.BigDecimal getScore() {
		return score;
	}
	public void setScore(java.math.BigDecimal score) {
		this.score = score;
	}
	public java.math.BigDecimal getSubScore1() {
		return subScore1;
	}
	public void setSubScore1(java.math.BigDecimal subScore1) {
		this.subScore1 = subScore1;
	}
	public java.math.BigDecimal getSubScore2() {
		return subScore2;
	}
	public void setSubScore2(java.math.BigDecimal subScore2) {
		this.subScore2 = subScore2;
	}
	public java.math.BigDecimal getSubScore3() {
		return subScore3;
	}
	public void setSubScore3(java.math.BigDecimal subScore3) {
		this.subScore3 = subScore3;
	}
	/**讨论标题*/
	public String getTitle(){
		return this.title;
	}
	/**讨论标题*/
	public void setTitle(String title){
		this.title = title;
	}
	/**链接ID*/
	public String getLinkId(){
		return this.linkId;
	}
	/**链接ID*/
	public void setLinkId(String linkId){
		this.linkId = linkId;
	}
	/**链接类型(COURSE:课程,CHAPTER:章节)*/
	public String getLinkType(){
		return this.linkType;
	}
	/**链接类型(COURSE:课程,CHAPTER:章节)*/
	public void setLinkType(String linkType){
		this.linkType = linkType;
	}
}
