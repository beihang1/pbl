

package com.cloudsoaring.web.course.entity;


import java.util.Date;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_qa_question
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class QaQuestionEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**链接ID*/
	private String linkId;
	/**链接类型(COURSE:课程,CHAPTER:章节)*/
	private String linkType;
	/**讨论标题*/
	private String title;
	/**问题内容*/
	private String content;
	/**采纳的回答ID*/
	private String adoptAnswerId;
	/**送出的积分*/
	private java.math.BigDecimal sendPoint;
	/**浏览数*/
	private java.math.BigDecimal viewNum;
	
	/** 全部 null 已解决 0 待解决1*/
	private String flagBest;
	
	/** 回答数*/
	private java.math.BigDecimal answerCount;
	
	/** 回答内容*/
	private String answerContent;
	
	/** 回答时间*/
	private Date answerCreateDate;
	
	/** 回答人*/
	private String answerUserName;
	
	/** 回答人头像Id*/
	private String answerUserImage;
	
	/** 源自*/
	private String answerFromTitle;

	/** 提问人*/
	private String questionUserName;
	
	/** 提问人头像Id*/
	private String questionUserImage;
	
	private String qaQuestionId;

	private String userId;
	
	public String getFlagBest() {
		return flagBest;
	}
	public void setFlagBest(String flagBest) {
		this.flagBest = flagBest;
	}
	public String getQaQuestionId() {
		return qaQuestionId;
	}
	public void setQaQuestionId(String qaQuestionId) {
		this.qaQuestionId = qaQuestionId;
	}
	public String getQuestionUserName() {
		return questionUserName;
	}
	public void setQuestionUserName(String questionUserName) {
		this.questionUserName = questionUserName;
	}
	public String getQuestionUserImage() {
		return questionUserImage;
	}
	public void setQuestionUserImage(String questionUserImage) {
		this.questionUserImage = questionUserImage;
	}
	public java.math.BigDecimal getAnswerCount() {
		return answerCount;
	}
	public void setAnswerCount(java.math.BigDecimal answerCount) {
		this.answerCount = answerCount;
	}
	public String getAnswerContent() {
		return answerContent;
	}
	public void setAnswerContent(String answerContent) {
		this.answerContent = answerContent;
	}
	public Date getAnswerCreateDate() {
		return answerCreateDate;
	}
	public void setAnswerCreateDate(Date answerCreateDate) {
		this.answerCreateDate = answerCreateDate;
	}
	public String getAnswerUserName() {
		return answerUserName;
	}
	public void setAnswerUserName(String answerUserName) {
		this.answerUserName = answerUserName;
	}
	public String getAnswerUserImage() {
		return answerUserImage;
	}
	public void setAnswerUserImage(String answerUserImage) {
		this.answerUserImage = answerUserImage;
	}
	public String getAnswerFromTitle() {
		return answerFromTitle;
	}
	public void setAnswerFromTitle(String answerFromTitle) {
		this.answerFromTitle = answerFromTitle;
	}
	/**链接ID*/
	public String getLinkId(){
		return this.linkId;
	}
	/**链接ID*/
	public void setLinkId(String linkId){
		this.linkId = linkId;
	}
	/**链接类型(COURSE:课程,CHAPTER:章节)*/
	public String getLinkType(){
		return this.linkType;
	}
	/**链接类型(COURSE:课程,CHAPTER:章节)*/
	public void setLinkType(String linkType){
		this.linkType = linkType;
	}
	/**讨论标题*/
	public String getTitle(){
		return this.title;
	}
	/**讨论标题*/
	public void setTitle(String title){
		this.title = title;
	}
	/**问题内容*/
	public String getContent(){
		return this.content;
	}
	/**问题内容*/
	public void setContent(String content){
		this.content = content;
	}
	/**采纳的回答ID*/
	public String getAdoptAnswerId(){
		return this.adoptAnswerId;
	}
	/**采纳的回答ID*/
	public void setAdoptAnswerId(String adoptAnswerId){
		this.adoptAnswerId = adoptAnswerId;
	}
	/**送出的积分*/
	public java.math.BigDecimal getSendPoint(){
		return this.sendPoint;
	}
	/**送出的积分*/
	public void setSendPoint(java.math.BigDecimal sendPoint){
		this.sendPoint = sendPoint;
	}
	/**浏览数*/
	public java.math.BigDecimal getViewNum(){
		return this.viewNum;
	}
	/**浏览数*/
	public void setViewNum(java.math.BigDecimal viewNum){
		this.viewNum = viewNum;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
}
