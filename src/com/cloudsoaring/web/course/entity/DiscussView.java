package com.cloudsoaring.web.course.entity;
import java.util.ArrayList;
import java.util.List;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_discuss
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class DiscussView extends BaseEntity{

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**讨论标题*/
	private String title;
	/**链接ID*/
	private String linkId;
	/**链接类型(COURSE:课程,CHAPTER:章节)*/
	private String linkType;
	
	/**讨论ID*/
	private String discussId;
	/**发表用户ID*/
	private String userId;
	/**评论内容*/
	private String content;
	/**要回复的评论ID*/
	private String targetDiscussId;
	/**要回复的用户ID*/
	private String targetUserId;
	/**要回复的用户昵称*/
	private String targetNickname;
	/**发表用户呢称*/
	private String nickName;
	/**发表用户头像*/
	private String image;
	private List<DiscussDataEntity>DiscussDataList=new ArrayList<DiscussDataEntity>();
	
	public String getTargetNickname() {
		return targetNickname;
	}

	public void setTargetNickname(String targetNickname) {
		this.targetNickname = targetNickname;
	}



	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLinkId() {
		return linkId;
	}

	public void setLinkId(String linkId) {
		this.linkId = linkId;
	}

	public String getLinkType() {
		return linkType;
	}

	public void setLinkType(String linkType) {
		this.linkType = linkType;
	}

	public List<DiscussDataEntity> getDiscussDataList() {
		return DiscussDataList;
	}

	public void setDiscussDataList(List<DiscussDataEntity> discussDataList) {
		DiscussDataList = discussDataList;
	}

	public String getDiscussId() {
		return discussId;
	}

	public void setDiscussId(String discussId) {
		this.discussId = discussId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTargetDiscussId() {
		return targetDiscussId;
	}

	public void setTargetDiscussId(String targetDiscussId) {
		this.targetDiscussId = targetDiscussId;
	}

	public String getTargetUserId() {
		return targetUserId;
	}

	public void setTargetUserId(String targetUserId) {
		this.targetUserId = targetUserId;
	}
	
	
}
