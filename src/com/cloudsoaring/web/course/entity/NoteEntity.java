

package com.cloudsoaring.web.course.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_note
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class NoteEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**笔记合集标题*/
	private String title;
	/**链接ID*/
	private String linkId;
	/**链接类型(COURSE:课程,CHAPTER:章节)*/
	private String linkType;
	
	private String fileId;
	
	private String type;
	
	private String fileName;
	
	/**笔记合集标题*/
	public String getTitle(){
		return this.title;
	}
	/**笔记合集标题*/
	public void setTitle(String title){
		this.title = title;
	}
	/**链接ID*/
	public String getLinkId(){
		return this.linkId;
	}
	/**链接ID*/
	public void setLinkId(String linkId){
		this.linkId = linkId;
	}
	/**链接类型(COURSE:课程,CHAPTER:章节)*/
	public String getLinkType(){
		return this.linkType;
	}
	/**链接类型(COURSE:课程,CHAPTER:章节)*/
	public void setLinkType(String linkType){
		this.linkType = linkType;
	}
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	
}
