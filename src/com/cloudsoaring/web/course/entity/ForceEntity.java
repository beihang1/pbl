package com.cloudsoaring.web.course.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_force
 * @date 2019-09-25
 * @version V1.0
 *
 */
public class ForceEntity extends BaseEntity{

	/**serialVersionUID*/
	private static final long serialVersionUID = 1L;
	
	/**拉力*/
	private String pullingForce;
	
	/**重力*/
	private String weight;
	
	/**摩擦力*/
	private String frictionalForce;
	
	/**移动方向*/
	private String movingDirection;
	
	/**用户ID*/
	private String userId;
	
	/**用户名称*/
	private String userName;
	
	/**创建时间*/
	private Date createTime;
	
	public String getPullingForce() {
		return pullingForce;
	}
	public void setPullingForce(String pullingForce) {
		this.pullingForce = pullingForce;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getFrictionalForce() {
		return frictionalForce;
	}
	public void setFrictionalForce(String frictionalForce) {
		this.frictionalForce = frictionalForce;
	}
	public String getMovingDirection() {
		return movingDirection;
	}
	public void setMovingDirection(String movingDirection) {
		this.movingDirection = movingDirection;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
