package com.cloudsoaring.web.course.common.utils;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.cloudsoaring.common.utils.StringUtil;
/**
 * 该实体类主要是根据表达式获取script的正则表达式规则
 * img标签
 * html的正则表达式
 * 以及所有以<>开头结尾的信息
 * @author Admin
 *
 */
public class CommonUtils {
	private final static String regxpForHtml = "<[^>]+>|<[/s]*?script[^>]*?>[/s/S]*?<[/s]*?//[/s]*?script[/s]*?>"; // 过滤所有以<开头以>结尾的标签

	private final static String regxpForImgTag = "<\\s*img\\s+([^>]*)\\s*>"; // 找出IMG标签

	private final static String regxpForImaTagSrcAttrib = "src=\"([^\"]+)\""; // 找出IMG标签的SRC属性
	
	private final static String regEx_html = "<[^>]+>"; // 定义HTML标签的正则表达式

	private final static String regEx_script = "<[/s]*?script[^>]*?>[/s/S]*?<[/s]*?//[/s]*?script[/s]*?>"; // 定义script的正则表达式{或<script[^>]*?>[/s/S]*?<//script>

	/**
	 * 
	 * 基本功能：替换标记以正常显示
	 * <p>
	 * 
	 * @param input
	 * @return String
	 */
	public String replaceTag(String input) {
		//判断是否为特殊字符
		if (!hasSpecialChars(input)) {
			return input;
		}
		//定义字符
		StringBuffer filtered = new StringBuffer(input.length());
		//定义字符变量
		char c;
		//循环判断字符
		for (int i = 0; i <= input.length() - 1; i++) {
			//获取字符下标
			c = input.charAt(i);
			switch (c) {
			case '<':
				filtered.append("&lt;");//小于号
				break;
			case '>':
				filtered.append("&gt;");//大于号
				break;
			case '"':
				filtered.append("&quot;");//双引号
				break;
			case '&':
				filtered.append("&amp;");//&符号
				break;
			default:
				filtered.append(c);//添加到字符流
			}

		}
		//返回结果集
		return (filtered.toString());
	}

	/**
	 * 
	 * 基本功能：判断标记是否存在
	 * <p>
	 * 
	 * @param input
	 * @return boolean
	 */
	public boolean hasSpecialChars(String input) {
		boolean flag = false;
		//判断字符是否为空
		if ((input != null) && (input.length() > 0)) {
			//定义字符变量
			char c;
			//循环遍历字符产
			for (int i = 0; i <= input.length() - 1; i++) {
				c = input.charAt(i);
				switch (c) {
				case '>':
					flag = true;//大于号
					break;//跳出循环
				case '<':
					flag = true;//小于号
					break;
				case '"':
					flag = true;//双引号
					break;
				case '&':
					flag = true;//与逻辑
					break;
				}
			}
		}
		//返回结果集
		return flag;
	}

	/**
	 * 
	 * 基本功能：过滤所有以"<"开头以">"结尾的标签
	 * <p>
	 * 
	 * @param str
	 * @return String
	 */
	public static String filterHtml(String str) {
	    //定义script正则表达式规则
		Pattern p_script = Pattern.compile(regEx_script, Pattern.CASE_INSENSITIVE);
		//匹配规则
		Matcher m_script = p_script.matcher(str);
        str = m_script.replaceAll(""); // 过滤script标签
        //定义html正则表达式规则
        Pattern p_html = Pattern.compile(regEx_html, Pattern.CASE_INSENSITIVE);
        //匹配html规则
        Matcher m_html = p_html.matcher(str);
        str = m_html.replaceAll(""); // 过滤html标签
        //返回结果
        return str;
		
	}

	/**
	 * 
	 * 基本功能：过滤指定标签
	 * <p>
	 * 
	 * @param str
	 * @param tag
	 *            指定标签
	 * @return String
	 */
	public static String fiterHtmlTag(String str, String tag) {
		//定义规则
		String regxp = "<\\s*" + tag + "\\s+([^>]*)\\s*>";
		Pattern pattern = Pattern.compile(regxp);
		//匹配规则
		Matcher matcher = pattern.matcher(str);
		//定义字符集
		StringBuffer sb = new StringBuffer();
		boolean result1 = matcher.find();
		//循环结果
		while (result1) {
			//添加元素
			matcher.appendReplacement(sb, "");
			//判断结果
			result1 = matcher.find();
		}
		matcher.appendTail(sb);
		//返回结果
		return sb.toString();
	}

	/**
	 * 
	 * 基本功能：替换指定的标签
	 * <p>
	 * 
	 * @param str
	 * @param beforeTag
	 *            要替换的标签
	 * @param tagAttrib
	 *            要替换的标签属性值
	 * @param startTag
	 *            新标签开始标记
	 * @param endTag
	 *            新标签结束标记
	 * @return String
	 * @如：替换img标签的src属性值为[img]属性值[/img]
	 */
	public static String replaceHtmlTag(String str, String beforeTag,
			String tagAttrib, String startTag, String endTag) {
		//定义标签规则
		String regxpForTag = "<\\s*" + beforeTag + "\\s+([^>]*)\\s*>";
		String regxpForTagAttrib = tagAttrib + "=\"([^\"]+)\"";
		//声明规则对象
		Pattern patternForTag = Pattern.compile(regxpForTag);
		Pattern patternForAttrib = Pattern.compile(regxpForTagAttrib);
		Matcher matcherForTag = patternForTag.matcher(str);
		//声明字符集
		StringBuffer sb = new StringBuffer();
		boolean result = matcherForTag.find();
		//循环结果集
		while (result) {
			StringBuffer sbreplace = new StringBuffer();
			//匹配小标为1的信息
			Matcher matcherForAttrib = patternForAttrib.matcher(matcherForTag
					.group(1));
			//判断是否存在
			if (matcherForAttrib.find()) {
				//存在就添加元素
				matcherForAttrib.appendReplacement(sbreplace, startTag
						+ matcherForAttrib.group(1) + endTag);
			}
			//添加元素
			matcherForTag.appendReplacement(sb, sbreplace.toString());
			//判断是否存在
			result = matcherForTag.find();
		}
		matcherForTag.appendTail(sb);
		//返回结果
		return sb.toString();
	}

	/***
	 * 将秒数转为时分秒
	 * 
	 * @param seconds
	 *            秒数
	 * @return
	 */
	public static String formatSeconds(BigDecimal seconds) {
		//判断数据是否存在
		if (seconds == null) {
			seconds = BigDecimal.ZERO;
		}
		//获取地区秒数
		String ss = StringUtil.getOrElse(seconds);
		//判断是否有。结尾数据
		if (ss.indexOf(".") != -1) {
			//截取整数
			ss = ss.substring(0, ss.indexOf("."));
		}
		//将时间秒数转化成integer类型
		int sss = Integer.parseInt(ss);
		//获取小时
		int h = sss / 3600;
		//获取分钟
		int m = (sss - h * 3600) / 60;
		//获取秒数
		int s = (sss - h * 3600) % 60;
		String str = "";
		//拼接字符串
		if (h > 0) {
			str += h + ":";
		}
		//判断分钟逻辑
		if (m > 9) {
			str += m + ":";
		} else {
			str += "0" + m + ":";//小于9在前面加0
		}
		//判断秒数逻辑
		if (s > 9) {
			str += s + "";
		} else {
			str += "0" + s;//小于9在前面加0
		}
		//返回结果
		return str;
	}

	/***
	 * 用户学习课程耗时格式化
	 * 
	 * @param time
	 *            学习耗时 单位秒
	 * @return
	 */
	public static final String formatTimeCose(BigDecimal time) {
		//判断字符是否为空
		if (time == null) {
			return "0分";
		}
		//获取时间
		long ss = time.longValue();
		//年份
		long year = ss / (60 * 60 * 24 * 365);
		//月份
		long month = (ss % (60 * 60 * 24 * 365)) / (60 * 60 * 24 * 30);
		//日期天数
		long day = (ss % (60 * 60 * 24 * 30)) / (60 * 60 * 24);
		//小时
		long hh = (ss % (60 * 60 * 24)) / (60 * 60);
		//分钟
		long mm = (ss % (60 * 60)) / (60);
		StringBuffer sb = new StringBuffer();
		//判断年份
		if (year > 1) {
			sb.append(year + "年");
		}
		//月份
		if (month > 0) {
			sb.append(month + "个月");
		}
		//天数
		if (day > 0) {
			sb.append(day + "天");
		}
		//小时数
		if (hh > 0) {
			sb.append(hh + "小时");
		}
		//分钟数
		sb.append(mm + "分");
		//返回结果集
		return sb.toString();
	}

	/**
	 * 对超过指定长度的字符串截取，并在其后加上...
	 * 
	 * @param str
	 *            要截取的字符串
	 * @param length
	 *            要截取的长度
	 * @return 截取后的字符串
	 */
	public static String hideStringByMax(String str, Integer length) {
		//判断字符是否为空
		if (StringUtil.isNotEmpty(str)) {
			//截取字符长度
			if (str.length() > length) {
				//返回结果集
				return str.substring(0, length) + "...";
			} else {
				//结果
				return str;
			}
		} else {
			//返回空字符串
			return "";
		}
	}

	/**
	 * 将时分秒转成秒数
	 * 
	 * @param strTime
	 *            时分秒
	 * @return
	 */
	public static String formatFromSeconds(String strTime) {
		//判断字符是否为空
		if (strTime == null) {
			strTime = "0";
		}
		//获取下标
		int index1 = strTime.indexOf(":");
		//获取第index1+1的个的下标
		int index2 = strTime.indexOf(":", index1 + 1);
		//截取小时数据
		int hh = Integer.parseInt(strTime.substring(0, index1));
		//截取分钟数据
		int mi = Integer.parseInt(strTime.substring(index1 + 1, index2));
		//截取秒数数据
		int ss = Integer.parseInt(strTime.substring(index2 + 1));
		//返回结果
		return Integer.toString(hh * 60 * 60 + mi * 60 + ss);
	}

}
