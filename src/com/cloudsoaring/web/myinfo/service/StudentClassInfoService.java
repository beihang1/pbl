package com.cloudsoaring.web.myinfo.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.myinfo.entity.StudentClassInfoEntity;
import com.cloudsoaring.web.trainingplatform.constants.TpConstants;

/***
 * 班级管理相关的业务处理Service
 */
@Service
@SuppressWarnings("unchecked")
public class StudentClassInfoService extends FileService implements TpConstants{
    /**
                  * 分页查询学生集合信息
     * @param entity
     * @return
     */
	public ResultBean searchStudentClassInfoPage(StudentClassInfoEntity entity){
		return commonDao.searchList4Page(entity, STUDENT_CLASS_SEARCH);
	}
	/**
	 * 查询学生集合列表方法
	 * @param entity
	 * @return
	 */
	public List<StudentClassInfoEntity> searchStudentClassInfoList(StudentClassInfoEntity entity){
		return commonDao.searchList(entity, STUDENT_CLASS_SEARCH);
	}
	/**
	 * 保存学生信息结果
	 * @param entity
	 */
	public void insertStudentClassInfo(StudentClassInfoEntity entity){
		entity.setCreateDate(new Date());
		//随机创建学生信息主键
		entity.setId(IDGenerator.genUID());
		this.commonDao.insertData(entity,STUDENT_CLASS_ADD);
	}
	/**
	 * 根据tb_student_class_info主键查询学生信息
	 * @param id
	 * @return
	 */
	public StudentClassInfoEntity selectStudentClassInfoById(String id) {
		StudentClassInfoEntity entity = new StudentClassInfoEntity();
		entity.setId(id);
		//返回查询结果
		return commonDao.searchOneData(entity, STUDENT_CLASS_SEARCH_BY_ID);
	}
	/**
	 * 更新学生年级班级的信息
	 * @param entity
	 */
	public void updateStudentClassInfoById(StudentClassInfoEntity entity){
		entity.setUpdateUser(WebContext.getSessionUserId());
		//设置更新时间
		entity.setUpdateDate(new Date());
		//更新数据表
		this.commonDao.updateData(entity, STUDENT_CLASS_UPDATE_BY_ID);
	}
	/**
	 * 单条查询学校信息在数据表tb_student_class_info表
	 * @param entity
	 * @return
	 */
	public StudentClassInfoEntity searchStudentClassInfo(StudentClassInfoEntity entity){
		return commonDao.searchOneData(entity, STUDENT_ONE_LASS_SEARCH);
	}
	/**
	 * 删除学生信息的方法
	 * @param entity
	 */
	public void deleteStudentClassInfoById(StudentClassInfoEntity entity) {
		this.commonDao.deleteData(entity, STUDENT_CLASS_DELETE_BY_ID);
	}
	/**
	 * 集合查询学生年级信息的方法
	 * @param entity
	 * @return
	 */
	public List<StudentClassInfoEntity> searchStudentGradeList(StudentClassInfoEntity entity){
		return commonDao.searchList(entity, STUDENT_GRADE_BY_ID);
	}
    /**
                  *  集合查询学生班级信息的方法
     * @param studentClassInfoEntity
     * @return
     */
	public List<StudentClassInfoEntity> searchStudentClassList(StudentClassInfoEntity studentClassInfoEntity) {
		return commonDao.searchList(studentClassInfoEntity, STUDENT_CLASS_BY_ID);
	}
	public StudentClassInfoEntity searchStudentInfo(StudentClassInfoEntity studentClassInfoEntity) {
		return commonDao.searchOneData(studentClassInfoEntity, STUDENT_CLASS_SEARCH);
	}
	public StudentClassInfoEntity searchGradeClassInfo(StudentClassInfoEntity studentClassInfoEntity) {
		return commonDao.searchOneData(studentClassInfoEntity, STUDENT_TEACHER_CLASS_SEARCH);
	}
}
