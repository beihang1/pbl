package com.cloudsoaring.web.myinfo.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.CompressConfig;
import com.cloudsoaring.web.bus.entity.FileEntity;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseDbConstants;
import com.cloudsoaring.web.course.entity.MeasurementEntity;
import com.cloudsoaring.web.course.entity.TagLinkEntity;
import com.cloudsoaring.web.course.view.CourseManagerView;
import com.cloudsoaring.web.course.view.InstitutionManagerView;
import com.cloudsoaring.web.institution.entity.InstitutionEntity;
import com.cloudsoaring.web.institution.entity.TagInstitutionEntity;
import com.cloudsoaring.web.institution.entity.TagLinkInstitutionEntity;
import com.cloudsoaring.web.institution.entity.TbInstitutionEvalutionEntity;
import com.cloudsoaring.web.myinfo.entity.TeacherClassInfoEntity;
import com.cloudsoaring.web.taxonomy.entity.TbInstitutionEvalutionManagerView;
import com.cloudsoaring.web.trainingplatform.constants.TpConstants;
import com.cloudsoaring.web.trainingplatform.utils.TpConfigUtil;

/***
 * 班级管理相关的业务处理Service
 */
@Service
@SuppressWarnings("unchecked")
public class TeacherClassInfoService extends FileService implements TpConstants{

	public ResultBean searchTeacherClassInfoListPage(TeacherClassInfoEntity entity){
		return commonDao.searchList4Page(entity, TEACHER_CLASS_SEARCH);
	}
	
	public List<TeacherClassInfoEntity> searchTeacherClassInfoList(TeacherClassInfoEntity entity){
		return commonDao.searchList(entity, TEACHER_CLASS_SEARCH);
	}
	
	public void insertTeacherClassInfo(TeacherClassInfoEntity entity){
		entity.setId(IDGenerator.genUID());
		entity.setCreateDate(new Date());
		this.commonDao.insertData(entity,TEACHER_CLASS_ADD);
	}
	
	public TeacherClassInfoEntity selectTeacherClassInfoById(String id) {
		TeacherClassInfoEntity entity = new TeacherClassInfoEntity();
		entity.setId(id);
		return commonDao.searchOneData(entity, TEACHER_CLASS_SEARCH_BY_ID);
	}
	
	public void updateTeacherClassInfoById(TeacherClassInfoEntity entity){
		entity.setUpdateUser(WebContext.getSessionUserId());
		entity.setUpdateDate(new Date());
		this.commonDao.updateData(entity, TEACHER_CLASS_UPDATE_BY_ID);
	}
	
	public void deleteTeacherClassInfoById(TeacherClassInfoEntity entity) {
		this.commonDao.deleteData(entity, TEACHER_CLASS_DELETE_BY_ID);
	}
}
