package com.cloudsoaring.web.myinfo.entity;

import java.util.Date;

import com.cloudsoaring.common.annotation.ClassDefine;
import com.cloudsoaring.web.common.entity.BaseEntity;
/**
 * 教师信息实例化对象
 * @author ljl
 *
 */
@ClassDefine
public class ClassInfoEntity extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	 private Integer classId;
	 private Integer classOrgId;
	 private String coursesIds;
	 private Integer coursesId;
	 private String className;
	 private Integer gradeId;
	 private String schoolId;
	 private String gradeName;
	 private String classCode;
	 private Integer isUniOrcoh;
	 private Integer classCategory;
	 private String createTime;
	 private String endTime;
	 private Integer masterId;
	 private String masterName;
	 private Integer classStudentNum;
	 private String students;
	 private String teachers;
	 private String classDutyId;
	 private String classDutyName;
	public Integer getClassId() {
		return classId;
	}
	public void setClassId(Integer classId) {
		this.classId = classId;
	}
	public Integer getClassOrgId() {
		return classOrgId;
	}
	public void setClassOrgId(Integer classOrgId) {
		this.classOrgId = classOrgId;
	}
	public String getCoursesIds() {
		return coursesIds;
	}
	public void setCoursesIds(String coursesIds) {
		this.coursesIds = coursesIds;
	}
	public Integer getCoursesId() {
		return coursesId;
	}
	public void setCoursesId(Integer coursesId) {
		this.coursesId = coursesId;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public Integer getGradeId() {
		return gradeId;
	}
	public void setGradeId(Integer gradeId) {
		this.gradeId = gradeId;
	}
	public String getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
	}
	public String getGradeName() {
		return gradeName;
	}
	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	}
	public String getClassCode() {
		return classCode;
	}
	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}
	public Integer getIsUniOrcoh() {
		return isUniOrcoh;
	}
	public void setIsUniOrcoh(Integer isUniOrcoh) {
		this.isUniOrcoh = isUniOrcoh;
	}
	public Integer getClassCategory() {
		return classCategory;
	}
	public void setClassCategory(Integer classCategory) {
		this.classCategory = classCategory;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public Integer getMasterId() {
		return masterId;
	}
	public void setMasterId(Integer masterId) {
		this.masterId = masterId;
	}
	public String getMasterName() {
		return masterName;
	}
	public void setMasterName(String masterName) {
		this.masterName = masterName;
	}
	public Integer getClassStudentNum() {
		return classStudentNum;
	}
	public void setClassStudentNum(Integer classStudentNum) {
		this.classStudentNum = classStudentNum;
	}
	public String getStudents() {
		return students;
	}
	public void setStudents(String students) {
		this.students = students;
	}
	public String getTeachers() {
		return teachers;
	}
	public void setTeachers(String teachers) {
		this.teachers = teachers;
	}
	public String getClassDutyId() {
		return classDutyId;
	}
	public void setClassDutyId(String classDutyId) {
		this.classDutyId = classDutyId;
	}
	public String getClassDutyName() {
		return classDutyName;
	}
	public void setClassDutyName(String classDutyName) {
		this.classDutyName = classDutyName;
	}
	
	 
	 
	
	
}
