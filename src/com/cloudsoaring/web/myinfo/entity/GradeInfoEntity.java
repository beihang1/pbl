package com.cloudsoaring.web.myinfo.entity;

import java.util.Date;

import com.cloudsoaring.common.annotation.ClassDefine;
import com.cloudsoaring.web.common.entity.BaseEntity;
/**
 * 教师信息实例化对象
 * @author ljl
 *
 */
@ClassDefine
public class GradeInfoEntity extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	 private Integer gradeId;
	 private String gradeLever;
	 private String gradeName;
	 private Integer schoolType;
	 private String schoolTypes;
	 private String schoolId;
	 private String gradeDutyId;
	 private String gradeDutyName;
	 private String clazz;
	 private String clazzInfo;
	 private String createTime;
	 private String courseGradeId;
	 private String updateTime;
	 private Integer gradeStudentNum;
	 private Integer dictGradeId;
	 private String classInfo;
	public Integer getGradeId() {
		return gradeId;
	}
	public void setGradeId(Integer gradeId) {
		this.gradeId = gradeId;
	}
	public String getGradeLever() {
		return gradeLever;
	}
	public void setGradeLever(String gradeLever) {
		this.gradeLever = gradeLever;
	}
	public String getGradeName() {
		return gradeName;
	}
	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	}
	public Integer getSchoolType() {
		return schoolType;
	}
	public void setSchoolType(Integer schoolType) {
		this.schoolType = schoolType;
	}
	public String getSchoolTypes() {
		return schoolTypes;
	}
	public void setSchoolTypes(String schoolTypes) {
		this.schoolTypes = schoolTypes;
	}
	public String getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
	}
	public String getGradeDutyId() {
		return gradeDutyId;
	}
	public void setGradeDutyId(String gradeDutyId) {
		this.gradeDutyId = gradeDutyId;
	}
	public String getGradeDutyName() {
		return gradeDutyName;
	}
	public void setGradeDutyName(String gradeDutyName) {
		this.gradeDutyName = gradeDutyName;
	}
	public String getClazz() {
		return clazz;
	}
	public void setClazz(String clazz) {
		this.clazz = clazz;
	}
	public String getClazzInfo() {
		return clazzInfo;
	}
	public void setClazzInfo(String clazzInfo) {
		this.clazzInfo = clazzInfo;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getCourseGradeId() {
		return courseGradeId;
	}
	public void setCourseGradeId(String courseGradeId) {
		this.courseGradeId = courseGradeId;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public Integer getGradeStudentNum() {
		return gradeStudentNum;
	}
	public void setGradeStudentNum(Integer gradeStudentNum) {
		this.gradeStudentNum = gradeStudentNum;
	}
	public Integer getDictGradeId() {
		return dictGradeId;
	}
	public void setDictGradeId(Integer dictGradeId) {
		this.dictGradeId = dictGradeId;
	}
	public String getClassInfo() {
		return classInfo;
	}
	public void setClassInfo(String classInfo) {
		this.classInfo = classInfo;
	}
	 
	 
	 
	
	
}
