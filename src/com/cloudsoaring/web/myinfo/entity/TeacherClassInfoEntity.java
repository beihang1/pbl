package com.cloudsoaring.web.myinfo.entity;

import java.util.Date;

import com.cloudsoaring.common.annotation.ClassDefine;
import com.cloudsoaring.web.common.entity.BaseEntity;
/**
 * 教师信息实例化对象
 * @author ljl
 *
 */
@ClassDefine
public class TeacherClassInfoEntity extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//教师id
	private String teacherId;
	//年级名称
	private String gradeName;
	//班级主键
	private String classId;
	//年级主键
	private String gradeId;
	//班级名称
	private String className;
	//年级等级
	private String gradeLever;
	//课程id
	private String subjectId;
	//课程名称
	private String subjectName;
	//创建时间
	private Date createDate;
	//更新时间
	private Date updateDate;
	public String getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}
	public String getGradeName() {
		return gradeName;
	}
	public void setGradeName(String gradeName) {
		this.gradeName = gradeName;
	}
	public String getClassId() {
		return classId;
	}
	public void setClassId(String classId) {
		this.classId = classId;
	}
	public String getGradeId() {
		return gradeId;
	}
	public void setGradeId(String gradeId) {
		this.gradeId = gradeId;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getGradeLever() {
		return gradeLever;
	}
	public void setGradeLever(String gradeLever) {
		this.gradeLever = gradeLever;
	}
	public String getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
	
}
