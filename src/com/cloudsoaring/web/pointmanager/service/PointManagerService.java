package com.cloudsoaring.web.pointmanager.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.exception.BusinessException;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.common.dao.CommonDao;
import com.cloudsoaring.web.common.service.BaseService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseDbConstants;
import com.cloudsoaring.web.course.view.UserView;
import com.cloudsoaring.web.pointmanager.constant.PointConstants;
import com.cloudsoaring.web.pointmanager.entity.PointHisEntity;
import com.cloudsoaring.web.pointmanager.entity.PointsRechargeEntity;
import com.cloudsoaring.web.pointmanager.view.PointTypeView;
import com.cloudsoaring.web.points.entity.UserPoint;
import com.cloudsoaring.web.points.service.PointService;
import com.cloudsoaring.web.trainingplatform.constants.TpDbConstants;
import com.cloudsoaring.web.trainingplatform.entity.TeacherLevelEntity;
import com.cloudsoaring.web.trainingplatform.service.TeacherLevelManagerService;

/**
 *积分管理
 * @author lijuan
 */
@Service
public class PointManagerService extends BaseService implements PointConstants{
	@Autowired
	private PointService pointService;
	
	@Autowired
    private TeacherLevelManagerService teacherLevelManagerService;

	@Resource(name="ssoCommonDao")
	private CommonDao ssoCommonDao;
	
	/**
	 * 所有人员
	 * 
	 * @return 返回人员列表信息
	 */
	public ResultBean searchUserAll(UserView query) {
		return ssoCommonDao.searchList4Page(query, "SsoUser.seachUserAll");
	}
	
    /**
     * 
     * 查询积分来源列表
     * @param entity 积分来源
     * @return
     */
    public ResultBean sourceList(PointTypeView entity) {
        assertNotGuest();
        return commonDao.searchList4Page(entity, SQL_POINT_TYPE_LIST);
    }
    /**
     * 根据编号查询积分来源信息
     * @param typeCode 编号
     * @return
     */
    public PointTypeView searchOnePointType(String typeCode) {
        assertNotGuest();
        PointTypeView query=new PointTypeView();
        query.setTypeCode(typeCode);
        return commonDao.searchOneData(query, SQL_SEARCH_ONE_TYPE);
    }
    /**
     * 变更积分来源信息
     * @param entity
     */
    public void editPoint(PointTypeView entity) {
       entity.setUpdateUser(WebContext.getSessionUserId());
      commonDao.updateData(entity, SQL_UPDATE_POINT_TYPE);
    }
    /**
     * 充值记录查询
     * @param entity
     * @return
     */
    public ResultBean rechargeList(PointsRechargeEntity entity) {
        return commonDao.searchList4Page(entity, SQL_SEARCH_RECHARGE_LIST);
    }
    /**
     * 根据用户名获取兑换人手机
     * @param userName
     * @return
     */
    public Boolean searchExchangerName(String userName) {
        UserView user=new UserView();
        user.setUserName(userName);
        user=commonDao.searchOneData(user, CourseDbConstants.SQL_SEARCH_MOBILE);
        if(user==null){
            return false;
        }else{
            return true;
        }
    }
    
    /**
     * 保存充值记录
     * @param entity 充值记录对象
     */
    public void submitPointRecharge(PointsRechargeEntity entity) {
        assertNotGuest();
        if(!searchExchangerName(entity.getExchangerName())){
            throw new BusinessException("用户不存在!");
        }else{
            entity.setCreateDate(new Date());
            entity.setCreateUser(WebContext.getSessionUser().getUserName());
            entity.setRechargeId(IDGenerator.genUID());
            UserPoint changePointReuslt = null;
            if(entity.getExchangePonts().compareTo(BigDecimal.ZERO)==1){
                entity.setSumKbn("A");
                changePointReuslt = pointService.earnPoint("HS", entity.getExchangerId(), "A0001", entity.getExchangePonts(), entity.getId(),"");
            }else{
                entity.setSumKbn("D");
                changePointReuslt = pointService.consumePoint("HS", entity.getExchangerId(), "D0001", entity.getExchangePonts().abs(), entity.getId(),"");
            }
            if(!changePointReuslt.isStatus()) {
            	throw new BusinessException(changePointReuslt.getErrorCode(),changePointReuslt.getMessages());
            }
            commonDao.insertData(entity, SQL_INSERT_POINT_RECHARGE);
        }
        
    }
    /**
     * 根据充值Id检索充值记录
     * @param rechargeId充值记录Id
     * @return
     */
    public PointsRechargeEntity searchRechargeById(String rechargeId) {
        PointsRechargeEntity entity=new PointsRechargeEntity();
        entity.setRechargeId(rechargeId);
        return commonDao.searchOneData(entity, SQL_SEARCH_RECHARGE_BY_ID);
    }
    /**
     * 更新充值记录
     * @param entity 充值记录对象
     */
    public void updatePointRecharge(PointsRechargeEntity entity) {
        entity.setUpdateDate(new Date());
        entity.setUpdateUser(WebContext.getSessionUser().getUserName());
        commonDao.updateData(entity, SQL_UPDATE_RECHARGE);
        
    }
    /**
     * 
     * 个人首页-积分获取及消费记录
     * @param userId 用户ID
     * @return 积分获取及消费记录信息
     */
    public ResultBean userPointsList(PointHisEntity pointHisEntity) {
    	pointHisEntity.setUserId(WebContext.getSessionUserId());
    	return commonDao.searchList4Page(pointHisEntity,SQL_SEARCH_MYPOINTS);

    }
    
    /**
     * 获取用户当前积分
     */
   public UserPoint getUserPoints(){
	   return pointService.fetchPoint(WebContext.getSessionUserId());
	   
   }
   /**
    * 老师积分分成
    * @param userId 导师ID
    * @param points 总积分
    * @return
    */
   @SuppressWarnings({ "unchecked" })
  public  ResultBean pointDivided(String userId,String points,String typeCode){
       ResultBean result=new ResultBean();
       //导师当前等级计算并获取导师分成比例
       UserPoint userPoint=pointService.fetchPoint(userId);
       TeacherLevelEntity entity=new TeacherLevelEntity();
       //导师等级信息
       List<TeacherLevelEntity> list=commonDao.searchList(entity, TpDbConstants.SQL_SEARCH_TEAHER_LEVEL_LIST);
       String splitRadio="";
       //循环导师信息获取当前导师等级
       for(int i=0;i<list.size();i++){
           entity=list.get(i);
           BigDecimal levelSet=new BigDecimal(entity.getLevelSet());
           if(userPoint.getTotalPoints().compareTo(levelSet)==-1){
               continue;
           }else if(userPoint.getTotalPoints().compareTo(levelSet)==1){
               splitRadio= entity.getSplitRadio();
               break;
           }
       }
      //根据导师分成比例计算导师应得的积分分成divide  teacherPoints
       if(StringUtil.isNotEmpty(splitRadio)){
           BigDecimal split=new BigDecimal(splitRadio).divide(new BigDecimal("100"));
           BigDecimal teacherPoints=new BigDecimal(points).multiply(split);
           //调用接口，给导师添加积分
           UserPoint changePointReuslt= pointService.earnPoint("HS", userId, typeCode, teacherPoints, "","");
           if(!changePointReuslt.isStatus()) {
               throw new BusinessException(changePointReuslt.getErrorCode(),changePointReuslt.getMessages());
           }
       }
       return result;
   }
   public  ResultBean pointReturn (String userId,String points,String typeCode){
       ResultBean result=new ResultBean();
       UserPoint changePointReuslt= pointService.earnPoint("HS", userId, typeCode, new BigDecimal(points), "","");
       if(!changePointReuslt.isStatus()) {
           throw new BusinessException(changePointReuslt.getErrorCode(),changePointReuslt.getMessages());
       }
       return result;
   }
   /**
    * 获取用户积分余额
    * @param userId
    * @return
    */
   public UserPoint searchPoint(String userId){
       UserPoint userPoint=pointService.fetchPoint(userId);
       return userPoint;
   }
}
