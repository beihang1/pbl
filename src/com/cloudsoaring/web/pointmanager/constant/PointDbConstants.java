package com.cloudsoaring.web.pointmanager.constant;

public interface PointDbConstants {
    /**积分来源*/
    public static final String SQL_POINT_TYPE_LIST = "PointTypeManager.select";
    /**根据编号查询积分来源信息*/
    public static final String SQL_SEARCH_ONE_TYPE="PointTypeManager.selectByPk";
    /**变更积分来源*/
    public static final String SQL_UPDATE_POINT_TYPE="PointTypeManager.updatePoint";
    /**积分记录查询*/
    public static final String SQL_SEARCH_RECHARGE_LIST="PointsRecharge.select";
    /**保存充值记录*/
    public static final String SQL_INSERT_POINT_RECHARGE="PointsRecharge.insert";
    /**根据ID查询充值记录*/
    public static final String SQL_SEARCH_RECHARGE_BY_ID="PointsRecharge.selectByPk";
    /**更新充值记录*/
    public static final String  SQL_UPDATE_RECHARGE="PointsRecharge.updateRecharge";
    /**根据用户ID查询*/
    public static final String  SQL_SEARCH_MYPOINTS="PointHis.searchMyPoints";
    
}
