package com.cloudsoaring.web.pointmanager.constant;

public interface PointConstants extends PointConfigNames, PointDbConstants, PointLogConstants, PointDefaultValues, PointMsgNames, PointStateConstants{
   
}
