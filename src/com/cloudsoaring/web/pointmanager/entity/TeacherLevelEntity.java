

package com.cloudsoaring.web.pointmanager.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_teacher_level 积分等级
 * @date 2016-04-06
 * @version V1.0
 *
 */
public class TeacherLevelEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**积分等级id*/
	private String levelId;
	/**编号*/
	private String levelNo;
	/**级别名称*/
	private String levelName;
	/**级别设定*/
	private String levelSet;
	/**分成比例*/
	private String splitRadio;
	
	/**积分等级id*/
	public String getLevelId(){
		return this.levelId;
	}
	/**积分等级id*/
	public void setLevelId(String levelId){
		this.levelId = levelId;
	}
	/**编号*/
	public String getLevelNo(){
		return this.levelNo;
	}
	/**编号*/
	public void setLevelNo(String levelNo){
		this.levelNo = levelNo;
	}
	/**级别名称*/
	public String getLevelName(){
		return this.levelName;
	}
	/**级别名称*/
	public void setLevelName(String levelName){
		this.levelName = levelName;
	}
	/**级别设定*/
	public String getLevelSet(){
		return this.levelSet;
	}
	/**级别设定*/
	public void setLevelSet(String levelSet){
		this.levelSet = levelSet;
	}
	/**分成比例*/
	public String getSplitRadio(){
		return this.splitRadio;
	}
	/**分成比例*/
	public void setSplitRadio(String splitRadio){
		this.splitRadio = splitRadio;
	}
}
