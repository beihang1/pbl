

package com.cloudsoaring.web.pointmanager.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_points_recharge
 * @date 2016-04-05
 * @version V1.0
 *
 */
public class PointsRechargeEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**充值id*/
	private String rechargeId;
	/**兑换人id*/
	private String exchangerId;
	/**兑换人姓名*/
	private String exchangerName;
	/**兑换人证件类型*/
	private String exchangerLicType;
	/**手机号*/
	private String mobile;
	/**兑换积分*/
	private java.math.BigDecimal exchangePonts;
	/**备注*/
	private String remark;
	/**积分汇总标识(A:增加,D：减少)*/
	private String sumKbn;
	/**积分抵扣规则*/
	private String discount;
	
	private String date;
	
	public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getDiscount() {
        return discount;
    }
    public void setDiscount(String discount) {
        this.discount = discount;
    }
    /**充值id*/
	public String getRechargeId(){
		return this.rechargeId;
	}
	/**充值id*/
	public void setRechargeId(String rechargeId){
		this.rechargeId = rechargeId;
	}
	/**兑换人id*/
	public String getExchangerId(){
		return this.exchangerId;
	}
	/**兑换人id*/
	public void setExchangerId(String exchangerId){
		this.exchangerId = exchangerId;
	}
	/**兑换人姓名*/
	public String getExchangerName(){
		return this.exchangerName;
	}
	/**兑换人姓名*/
	public void setExchangerName(String exchangerName){
		this.exchangerName = exchangerName;
	}
	/**兑换人证件类型*/
	public String getExchangerLicType(){
		return this.exchangerLicType;
	}
	/**兑换人证件类型*/
	public void setExchangerLicType(String exchangerLicType){
		this.exchangerLicType = exchangerLicType;
	}
	/**手机号*/
	public String getMobile(){
		return this.mobile;
	}
	/**手机号*/
	public void setMobile(String mobile){
		this.mobile = mobile;
	}
	/**兑换积分*/
	public java.math.BigDecimal getExchangePonts(){
		return this.exchangePonts;
	}
	/**兑换积分*/
	public void setExchangePonts(java.math.BigDecimal exchangePonts){
		this.exchangePonts = exchangePonts;
	}
	/**备注*/
	public String getRemark(){
		return this.remark;
	}
	/**备注*/
	public void setRemark(String remark){
		this.remark = remark;
	}
	/**积分汇总标识(A:增加,D：减少)*/
	public String getSumKbn(){
		return this.sumKbn;
	}
	/**积分汇总标识(A:增加,D：减少)*/
	public void setSumKbn(String sumKbn){
		this.sumKbn = sumKbn;
	}
}
