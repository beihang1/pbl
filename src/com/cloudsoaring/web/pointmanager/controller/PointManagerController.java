package com.cloudsoaring.web.pointmanager.controller;


import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.service.SystemService;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.view.UserView;
import com.cloudsoaring.web.pointmanager.entity.PointHisEntity;
import com.cloudsoaring.web.pointmanager.entity.PointsRechargeEntity;
import com.cloudsoaring.web.pointmanager.service.PointManagerService;
import com.cloudsoaring.web.pointmanager.view.PointTypeView;
import com.cloudsoaring.web.points.entity.UserPoint;
import com.cloudsoaring.web.points.service.PointService;
/**
 * 
 * 积分管理
 * @author lijuan
 */
@Controller
public class PointManagerController extends BaseController {
    @Autowired
    private SystemService systemService;
    @Autowired
    private PointManagerService pointnManagerService;
    @Autowired
    private PointService pointService;
    /**
     * 查询所有用户
     * @return
     */
    @RequestMapping("/manager/pointmanager/searchUserAll.action")
    @ResponseBody
    public ResultBean searchUserAll(UserView query)  {
    	ResultBean result =  pointnManagerService.searchUserAll(query);
    	return result;
    }
    /**
     * 用户积分
     * 
     * @return
     */
    @RequestMapping("/manager/pointmanager/searchPoint.action")
    @ResponseBody
    public UserPoint searchPoint(String userId)  {
        return pointnManagerService.searchPoint(userId);
    }
    /**
     * 积分抵扣规则
     * @return
     */
    @RequestMapping("/manager/pointmanager/pointDiscountInit.action")
    public ModelAndView listInit(){
        ModelAndView result = new ModelAndView("pointmanager/manager/point_discount");
        result.addObject("systemConfigs", systemService.searchAll());
        result.addObject("pageType","discount");
        return result;
    }
    
    /**
     * 积分来源列表初始化
     * @return
     */
    @RequestMapping("/manager/pointmanager/sourceInit.action")
    public ModelAndView sourceInit(PointTypeView condition){
        ModelAndView result = new ModelAndView("pointmanager/manager/point_source");
        condition.setPageSize(5);
        result.addObject("condition", condition);
        result.addObject("pageType","list");
        return result;
    }
    /**
     * 积分来源列表查询
     * @param entity
     * @return
     */
    @RequestMapping("/manager/pointmanager/sourceList.action")
    @ResponseBody
    public ResultBean sourceList(PointTypeView entity) {
        return pointnManagerService.sourceList(entity);
    }
    /**
     * 积分来源变更页面初始化
     * @param typeCode 编号
     * @return
     */
    @RequestMapping("/manager/pointmanager/editInit.action")
    public ModelAndView editInit(String typeCode){
        ModelAndView result = new ModelAndView("pointmanager/manager/point_edit");
        PointTypeView entity=pointnManagerService.searchOnePointType(typeCode);
        result.addObject("pageType","edit");
        result.addObject("entity",entity);
        return result;
    }
    /**
     * 积分来源变更
     * @param entity 积分来源对象
     * @return
     */
    @SuppressWarnings("static-access")
    @RequestMapping("/manager/pointmanager/editPoint.action")
    @ResponseBody
    public ResultBean editPoint(PointTypeView entity) {
        ResultBean result = new ResultBean().success();
        pointnManagerService.editPoint(entity);
        return result;
    }
    /**
     * 充值记录列表初始化
     * @param condition 充值记录对象
     * @return
     */
    @RequestMapping("/manager/pointmanager/rechargeListInit.action")
    public ModelAndView rechargeListInit(PointsRechargeEntity condition){
        ModelAndView result = new ModelAndView("pointmanager/manager/point_recharge");
        result.addObject("pageType","rechargeList");
        condition.setPageSize(10);
        result.addObject("condition",condition);
        return result;
    }
    /**
     * 
     * 充值记录列表查询
     * @param entity充值记录对象
     * @return
     */
    @RequestMapping("/manager/pointmanager/rechargeList.action")
    @ResponseBody
    public ResultBean rechargeList(PointsRechargeEntity entity) {
        return pointnManagerService.rechargeList(entity);
    }
    /**
     * 积分充值新增页面初始化
     * @return
     */
    @RequestMapping("/manager/pointmanager/addInit.action")
    public ModelAndView addInit(){
        ModelAndView result = new ModelAndView("pointmanager/manager/point_recharge_add");
        PointsRechargeEntity recharge=new PointsRechargeEntity();
        recharge.setCreateDate(new Date());
        recharge.setCreateUser(WebContext.getSessionUser().getUserName());
        result.addObject("pageType","add");
        result.addObject("recharge",recharge);
        return result;
    }
    /**
     * 积分充值编辑页面初始化
     * @param recharge
     * @return
     */
    @RequestMapping("/manager/pointmanager/editRechargeInit.action")
    public ModelAndView editRechargeInit(String rechargeId){
        ModelAndView result = new ModelAndView("pointmanager/manager/point_recharge_add");
        PointsRechargeEntity recharge=pointnManagerService.searchRechargeById(rechargeId);
        result.addObject("pageType","editRecharge");
        result.addObject("recharge",recharge);
        return result;
    }
    /**
     * 根据用户名获取兑换人手机
     * @param userName用户名
     * @return
     */
   /* @RequestMapping("/manager/pointmanager/searchExchangerName.action")
    @ResponseBody
    public ResultBean searchExchangerName(String userName) {
        return pointnManagerService.searchExchangerName(userName);
    }*/
    /**
     * 积分充值保存
     * @param entity 积分充值对象
     * @return
     */
    @SuppressWarnings("static-access")
    @RequestMapping("/manager/pointmanager/submitPointRecharge.action")
    @ResponseBody
    public ResultBean submitPointRecharge(PointsRechargeEntity entity) {
        ResultBean result = new ResultBean().success();
        if(StringUtil.isEmpty(entity.getRechargeId())){
           pointnManagerService.submitPointRecharge(entity); 
        }else{
            pointnManagerService.updatePointRecharge(entity);
        }
        return result;
    }
   
    /**
     * 查询我的积分记录
     * @param pointHisEntity
     * @return
     */
    @RequestMapping("client/mypoints.action")
    @ResponseBody
    public ResultBean mypoints() {
        pointnManagerService.assertNotGuest(); 		
        return ResultBean.success().setData(pointnManagerService.searchPoint(WebContext.getSessionUserId()));
    }
    
    /**
     * 查询我的积分
     * @param pointHisEntity
     * @return
     */
    @RequestMapping("client/pointmanager/userPointsList.action")
    @ResponseBody
    public ResultBean userPointsList(PointHisEntity pointHisEntity) {
        UserPoint point = pointService.fetchPoint(WebContext.getSessionUserId());
        WebContext.session("POINTS", point.getPoints());
        return pointnManagerService.userPointsList(pointHisEntity);
    }
    
    /**
     * 我的积分页面跳转
     * @param 
     * @return
     */
    @RequestMapping("/client/myPontsInit.action")
    public ModelAndView myPontsInit(){
        ModelAndView result = new ModelAndView("pointmanager/client/my_Point");
        UserPoint user= pointnManagerService.getUserPoints();
        result.addObject("totalPoints", String.valueOf(user.getPoints()));
        return result;
    }
   
    
    
}
