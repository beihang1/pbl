
package com.cloudsoaring.web.ws.client.point;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.cloudsoaring.web.ws.client.point package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ConsumePoint_QNAME = new QName("http://service.points.web.cloudsoaring.com/", "consumePoint");
    private final static QName _ConsumePointResponse_QNAME = new QName("http://service.points.web.cloudsoaring.com/", "consumePointResponse");
    private final static QName _EarnPoint_QNAME = new QName("http://service.points.web.cloudsoaring.com/", "earnPoint");
    private final static QName _EarnPointResponse_QNAME = new QName("http://service.points.web.cloudsoaring.com/", "earnPointResponse");
    private final static QName _FetchPoint_QNAME = new QName("http://service.points.web.cloudsoaring.com/", "fetchPoint");
    private final static QName _FetchPointResponse_QNAME = new QName("http://service.points.web.cloudsoaring.com/", "fetchPointResponse");
    private final static QName _GetPointsPerSmallChange_QNAME = new QName("http://service.points.web.cloudsoaring.com/", "getPointsPerSmallChange");
    private final static QName _GetPointsPerSmallChangeResponse_QNAME = new QName("http://service.points.web.cloudsoaring.com/", "getPointsPerSmallChangeResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.cloudsoaring.web.ws.client.point
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ConsumePoint }
     * 
     */
    public ConsumePoint createConsumePoint() {
        return new ConsumePoint();
    }

    /**
     * Create an instance of {@link ConsumePointResponse }
     * 
     */
    public ConsumePointResponse createConsumePointResponse() {
        return new ConsumePointResponse();
    }

    /**
     * Create an instance of {@link EarnPoint }
     * 
     */
    public EarnPoint createEarnPoint() {
        return new EarnPoint();
    }

    /**
     * Create an instance of {@link EarnPointResponse }
     * 
     */
    public EarnPointResponse createEarnPointResponse() {
        return new EarnPointResponse();
    }

    /**
     * Create an instance of {@link FetchPoint }
     * 
     */
    public FetchPoint createFetchPoint() {
        return new FetchPoint();
    }

    /**
     * Create an instance of {@link FetchPointResponse }
     * 
     */
    public FetchPointResponse createFetchPointResponse() {
        return new FetchPointResponse();
    }

    /**
     * Create an instance of {@link GetPointsPerSmallChange }
     * 
     */
    public GetPointsPerSmallChange createGetPointsPerSmallChange() {
        return new GetPointsPerSmallChange();
    }

    /**
     * Create an instance of {@link GetPointsPerSmallChangeResponse }
     * 
     */
    public GetPointsPerSmallChangeResponse createGetPointsPerSmallChangeResponse() {
        return new GetPointsPerSmallChangeResponse();
    }

    /**
     * Create an instance of {@link UserPoint }
     * 
     */
    public UserPoint createUserPoint() {
        return new UserPoint();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsumePoint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.points.web.cloudsoaring.com/", name = "consumePoint")
    public JAXBElement<ConsumePoint> createConsumePoint(ConsumePoint value) {
        return new JAXBElement<ConsumePoint>(_ConsumePoint_QNAME, ConsumePoint.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsumePointResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.points.web.cloudsoaring.com/", name = "consumePointResponse")
    public JAXBElement<ConsumePointResponse> createConsumePointResponse(ConsumePointResponse value) {
        return new JAXBElement<ConsumePointResponse>(_ConsumePointResponse_QNAME, ConsumePointResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EarnPoint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.points.web.cloudsoaring.com/", name = "earnPoint")
    public JAXBElement<EarnPoint> createEarnPoint(EarnPoint value) {
        return new JAXBElement<EarnPoint>(_EarnPoint_QNAME, EarnPoint.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EarnPointResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.points.web.cloudsoaring.com/", name = "earnPointResponse")
    public JAXBElement<EarnPointResponse> createEarnPointResponse(EarnPointResponse value) {
        return new JAXBElement<EarnPointResponse>(_EarnPointResponse_QNAME, EarnPointResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FetchPoint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.points.web.cloudsoaring.com/", name = "fetchPoint")
    public JAXBElement<FetchPoint> createFetchPoint(FetchPoint value) {
        return new JAXBElement<FetchPoint>(_FetchPoint_QNAME, FetchPoint.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FetchPointResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.points.web.cloudsoaring.com/", name = "fetchPointResponse")
    public JAXBElement<FetchPointResponse> createFetchPointResponse(FetchPointResponse value) {
        return new JAXBElement<FetchPointResponse>(_FetchPointResponse_QNAME, FetchPointResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPointsPerSmallChange }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.points.web.cloudsoaring.com/", name = "getPointsPerSmallChange")
    public JAXBElement<GetPointsPerSmallChange> createGetPointsPerSmallChange(GetPointsPerSmallChange value) {
        return new JAXBElement<GetPointsPerSmallChange>(_GetPointsPerSmallChange_QNAME, GetPointsPerSmallChange.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPointsPerSmallChangeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.points.web.cloudsoaring.com/", name = "getPointsPerSmallChangeResponse")
    public JAXBElement<GetPointsPerSmallChangeResponse> createGetPointsPerSmallChangeResponse(GetPointsPerSmallChangeResponse value) {
        return new JAXBElement<GetPointsPerSmallChangeResponse>(_GetPointsPerSmallChangeResponse_QNAME, GetPointsPerSmallChangeResponse.class, null, value);
    }

}
