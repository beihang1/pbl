package com.cloudsoaring.web.social.entity;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**
 * @Title: Entity
 * @Description: tb_tutor_students
 * @date 2016-03-23
 * @version V1.0
 *
 */
public class TutorStudentsEntity extends BaseEntity {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/** 辅导定义ID */
	private String tutorDefId;
	/** 辅导实例ID */
	private String tutorInstId;
	/** 学员ID */
	private String studentId;
	/** 学员问题 */
	private String question;
	/** 自我介绍 */
	private String introduction;
	/** 报名时间(预约) */
	private java.util.Date applyTime;
	/** 回复时间 */
	private java.util.Date replyTime;
	/** 响应时长 */
	private java.math.BigDecimal responseTime;
	/** 回复内容 */
	private String replyContent;
	/** 状态（待支付，待确认，已确认，已完成，已点评，已取消） */
	private String status;
	/** 姓名 */
	public String personName;
	/** 邮箱 */
	public String email;
	/** 手机 */
	public String mobile;
	/** 电话 */
	public String phone;
	/** 用户头像 */
	private String image;
	
	/** 是否显示见过按钮标识 */
	private boolean showMeetBtn = false;

	/** 辅导实例ID */
	public String getTutorInstId() {
		return this.tutorInstId;
	}

	/** 辅导实例ID */
	public void setTutorInstId(String tutorInstId) {
		this.tutorInstId = tutorInstId;
	}

	/** 学员ID */
	public String getStudentId() {
		return this.studentId;
	}

	/** 学员ID */
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	/** 学员问题 */
	public String getQuestion() {
		return this.question;
	}

	/** 学员问题 */
	public void setQuestion(String question) {
		this.question = question;
	}

	/** 自我介绍 */
	public String getIntroduction() {
		return this.introduction;
	}

	/** 自我介绍 */
	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	/** 报名时间(预约) */
	public java.util.Date getApplyTime() {
		return this.applyTime;
	}

	/** 报名时间(预约) */
	public void setApplyTime(java.util.Date applyTime) {
		this.applyTime = applyTime;
	}

	/** 回复时间 */
	public java.util.Date getReplyTime() {
		return this.replyTime;
	}

	/** 回复时间 */
	public void setReplyTime(java.util.Date replyTime) {
		this.replyTime = replyTime;
	}

	/** 响应时长 */
	public java.math.BigDecimal getResponseTime() {
		return this.responseTime;
	}

	/** 响应时长 */
	public void setResponseTime(java.math.BigDecimal responseTime) {
		this.responseTime = responseTime;
	}

	/** 回复内容 */
	public String getReplyContent() {
		return this.replyContent;
	}

	/** 回复内容 */
	public void setReplyContent(String replyContent) {
		this.replyContent = replyContent;
	}

	/** 状态（待支付，待确认，已确认，已完成，已点评，已取消） */
	public String getStatus() {
		return this.status;
	}

	/** 状态（待支付，待确认，已确认，已完成，已点评，已取消） */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the personName
	 */
	public String getPersonName() {
		return personName;
	}

	/**
	 * @param personName
	 *            the personName to set
	 */
	public void setPersonName(String personName) {
		this.personName = personName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile
	 *            the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone
	 *            the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * @return the tutorDefId
	 */
	public String getTutorDefId() {
		return tutorDefId;
	}

	/**
	 * @param tutorDefId the tutorDefId to set
	 */
	public void setTutorDefId(String tutorDefId) {
		this.tutorDefId = tutorDefId;
	}

	/**
	 * @return the showMeetBtn
	 */
	public boolean isShowMeetBtn() {
		return showMeetBtn;
	}

	/**
	 * @param showMeetBtn the showMeetBtn to set
	 */
	public void setShowMeetBtn(boolean showMeetBtn) {
		this.showMeetBtn = showMeetBtn;
	}

}
