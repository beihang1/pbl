package com.cloudsoaring.web.social.entity;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**
 * 辅导定义对象
 * 
 * @Title: Entity
 * @Description: tb_tutor_def
 * @date 2016-03-23
 * @version V1.0
 *
 */
public class TutorDefEntity extends BaseEntity {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/** 辅导定义ID */
	private String tutorDefId;
	/**用户ID*/
	private String userId;
	/**是否预约进行中（0:预约完成或未预约，1：预约中）*/
	private String joinAppointment;
	/** 辅导定义编号 */
	private java.math.BigDecimal tutorDefNo;
	/** 导师ID */
	private String teacherId;
	/** 标题 */
	private String title;
	/** 内容描述 */
	private String content;
	/** 辅导时长 */
	private String tutorDuration;
	/** 需要积分 */
	private java.math.BigDecimal price;
	/** 辅导类型(1V1：1对1，1VN:1对多） */
	private String tutorType;
	/** 有效开始日期 */
	private java.util.Date startDate;
	/** 有效结束日期 */
	private java.util.Date endDate;
	/** 开课人数 */
	private java.math.BigDecimal numStudents;
	/** 最少开课人数 */
    private java.math.BigDecimal numStudentsMin;
	/** 辅导得分 */
	private java.math.BigDecimal tutorScore;
	/** 总报名人数（预约） */
	private java.math.BigDecimal totalNumApply;
	/** 总见过人数 */
	private java.math.BigDecimal totalNumMeet;
	/** 最后一次辅导ID */
	private String lastInstId;
	/** 状态(0:未发布，1：已发布) */
	private String status;

	/** 分类ID */
	private String parentTagId;
	/** TAG_ID */
	private String tagId;
	/** 用户名 */
	private String personName;
	/** 工作场所 */
	private String workplace;
	/** 职位 */
	private String position;
	/** 报名人数 */
	private java.math.BigDecimal numApply;
	/** 用户头像 */
	private String image;
	/** TAG_ID */
	private String firstTag;
	/** PARENT_TAG_ID */
	private String secondTag;
	/**平均响应时长*/
	private java.math.BigDecimal avgResponseTime;
	/**接受率*/
	private java.math.BigDecimal avgAccept;
	
	/**辅导实例ID*/
	private String tutorInstId;
	
	/**提交类型 0 保存 1 发布*/
	private String type;

	public java.math.BigDecimal getNumStudentsMin() {
        return numStudentsMin;
    }

    public void setNumStudentsMin(java.math.BigDecimal numStudentsMin) {
        this.numStudentsMin = numStudentsMin;
    }

    public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getParentTagId() {
		return parentTagId;
	}

	public void setParentTagId(String parentTagId) {
		this.parentTagId = parentTagId;
	}

	public String getTagId() {
		return tagId;
	}

	public void setTagId(String tagId) {
		this.tagId = tagId;
	}

	/** 辅导定义ID */
	public String getTutorDefId() {
		return this.tutorDefId;
	}

	/** 辅导定义ID */
	public void setTutorDefId(String tutorDefId) {
		this.tutorDefId = tutorDefId;
	}

	/** 辅导定义编号 */
	public java.math.BigDecimal getTutorDefNo() {
		return this.tutorDefNo;
	}

	/** 辅导定义编号 */
	public void setTutorDefNo(java.math.BigDecimal tutorDefNo) {
		this.tutorDefNo = tutorDefNo;
	}

	/** 导师ID */
	public String getTeacherId() {
		return this.teacherId;
	}

	/** 导师ID */
	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	/** 标题 */
	public String getTitle() {
		return this.title;
	}

	/** 标题 */
	public void setTitle(String title) {
		this.title = title;
	}

	/** 内容描述 */
	public String getContent() {
		return this.content;
	}

	/** 内容描述 */
	public void setContent(String content) {
		this.content = content;
	}

	/** 辅导时长 */
	public String getTutorDuration() {
		return this.tutorDuration;
	}

	/** 辅导时长 */
	public void setTutorDuration(String tutorDuration) {
		this.tutorDuration = tutorDuration;
	}

	/** 价格 */
	public java.math.BigDecimal getPrice() {
		return this.price;
	}

	/** 价格 */
	public void setPrice(java.math.BigDecimal price) {
		this.price = price;
	}

	/** 辅导类型(1V1：1对1，1VN:1对多） */
	public String getTutorType() {
		return this.tutorType;
	}

	/** 辅导类型(1V1：1对1，1VN:1对多） */
	public void setTutorType(String tutorType) {
		this.tutorType = tutorType;
	}

	/** 有效开始日期 */
	public java.util.Date getStartDate() {
		return this.startDate;
	}

	/** 有效开始日期 */
	public void setStartDate(java.util.Date startDate) {
		this.startDate = startDate;
	}

	/** 有效结束日期 */
	public java.util.Date getEndDate() {
		return this.endDate;
	}

	/** 有效结束日期 */
	public void setEndDate(java.util.Date endDate) {
		this.endDate = endDate;
	}

	/** 开课人数 */
	public java.math.BigDecimal getNumStudents() {
		return this.numStudents;
	}

	/** 开课人数 */
	public void setNumStudents(java.math.BigDecimal numStudents) {
		this.numStudents = numStudents;
	}

	/** 辅导得分 */
	public java.math.BigDecimal getTutorScore() {
		return this.tutorScore;
	}

	/** 辅导得分 */
	public void setTutorScore(java.math.BigDecimal tutorScore) {
		this.tutorScore = tutorScore;
	}

	/** 总报名人数（预约） */
	public java.math.BigDecimal getTotalNumApply() {
		return this.totalNumApply;
	}

	/** 总报名人数（预约） */
	public void setTotalNumApply(java.math.BigDecimal totalNumApply) {
		this.totalNumApply = totalNumApply;
	}

	/** 总见过人数 */
	public java.math.BigDecimal getTotalNumMeet() {
		return this.totalNumMeet;
	}

	/** 总见过人数 */
	public void setTotalNumMeet(java.math.BigDecimal totalNumMeet) {
		this.totalNumMeet = totalNumMeet;
	}

	/** 最后一次辅导ID */
	public String getLastInstId() {
		return this.lastInstId;
	}

	/** 最后一次辅导ID */
	public void setLastInstId(String lastInstId) {
		this.lastInstId = lastInstId;
	}

	/** 状态(0:未发布，1：已发布) */
	public String getStatus() {
		return this.status;
	}

	/** 状态(0:未发布，1：已发布) */
	public void setStatus(String status) {
		this.status = status;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public java.math.BigDecimal getNumApply() {
		return numApply;
	}

	public void setNumApply(java.math.BigDecimal numApply) {
		this.numApply = numApply;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * @return the firstTag
	 */
	public String getFirstTag() {
		return firstTag;
	}

	/**
	 * @param firstTag
	 *            the firstTag to set
	 */
	public void setFirstTag(String firstTag) {
		this.firstTag = firstTag;
	}


	public String getSecondTag() {
        return secondTag;
    }

    public void setSecondTag(String secondTag) {
        this.secondTag = secondTag;
    }

    public String getTutorInstId() {
		return tutorInstId;
	}

	public void setTutorInstId(String tutorInstId) {
		this.tutorInstId = tutorInstId;
	}

	/**
	 * @return the workplace
	 */
	public String getWorkplace() {
		return workplace;
	}

	/**
	 * @param workplace the workplace to set
	 */
	public void setWorkplace(String workplace) {
		this.workplace = workplace;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	public java.math.BigDecimal getAvgResponseTime() {
		return avgResponseTime;
	}

	public void setAvgResponseTime(java.math.BigDecimal avgResponseTime) {
		this.avgResponseTime = avgResponseTime;
	}

	public java.math.BigDecimal getAvgAccept() {
		return avgAccept;
	}

	public void setAvgAccept(java.math.BigDecimal avgAccept) {
		this.avgAccept = avgAccept;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getJoinAppointment() {
		return joinAppointment;
	}

	public void setJoinAppointment(String joinAppointment) {
		this.joinAppointment = joinAppointment;
	}
	
}
