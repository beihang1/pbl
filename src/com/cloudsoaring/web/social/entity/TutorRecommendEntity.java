package com.cloudsoaring.web.social.entity;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_tutor_recommend
 * @date 2016-03-23
 * @version V1.0
 *
 */
public class TutorRecommendEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**教师ID*/
	private String tutorTeacherId;
	/**推荐老师ID*/
	private String recommendTeacherId;
	/**推荐语*/
	private String recommend;
	
	/** 用户名*/
	private String personName;
	//用户地区
	private String region;
	//职位
	private String position;
	//图像
	private String image;
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	/**教师ID*/
	public String getTutorTeacherId(){
		return this.tutorTeacherId;
	}
	/**教师ID*/
	public void setTutorTeacherId(String tutorTeacherId){
		this.tutorTeacherId = tutorTeacherId;
	}
	/**推荐老师ID*/
	public String getRecommendTeacherId(){
		return this.recommendTeacherId;
	}
	/**推荐老师ID*/
	public void setRecommendTeacherId(String recommendTeacherId){
		this.recommendTeacherId = recommendTeacherId;
	}
	/**推荐语*/
	public String getRecommend(){
		return this.recommend;
	}
	/**推荐语*/
	public void setRecommend(String recommend){
		this.recommend = recommend;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
}
