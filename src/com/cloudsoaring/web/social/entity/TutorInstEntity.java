package com.cloudsoaring.web.social.entity;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**
 * 辅导实例对象
 * @Title: Entity
 * @Description: tb_tutor_inst
 * @date 2016-03-23
 * @version V1.0
 *
 */
public class TutorInstEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**辅导定义ID*/
	private String tutorDefId;
	/**辅导实例ID*/
	private String tutorInstId;
	/**辅导开课时间*/
	private java.util.Date startTime;
	/**开课地点*/
	private String address;
	/**辅导结束时间*/
	private java.util.Date endTime;
	/**开课人数*/
	private java.math.BigDecimal numStudents;
	/**报名人数*/
	private java.math.BigDecimal numApply;
	/**见过人数*/
	private java.math.BigDecimal numMeet;
	/**备注*/
	private String remark;
	/**报名截至时间*/
	private java.util.Date applyStartDate;
	/**报名开始时间*/
	private java.util.Date applyEndDate;
	/**状态（0：众筹中，1：辅导中，2：已解决，3：关闭/取消)*/
	private String status;
	/**取消原因*/
	private String cancelNote;
	
	/**辅导定义ID*/
	public String getTutorDefId(){
		return this.tutorDefId;
	}
	/**辅导定义ID*/
	public void setTutorDefId(String tutorDefId){
		this.tutorDefId = tutorDefId;
	}
	/**辅导实例ID*/
	public String getTutorInstId(){
		return this.tutorInstId;
	}
	/**辅导实例ID*/
	public void setTutorInstId(String tutorInstId){
		this.tutorInstId = tutorInstId;
	}
	/**辅导开课时间*/
	public java.util.Date getStartTime(){
		return this.startTime;
	}
	/**辅导开课时间*/
	public void setStartTime(java.util.Date startTime){
		this.startTime = startTime;
	}
	/**开课地点*/
	public String getAddress(){
		return this.address;
	}
	/**开课地点*/
	public void setAddress(String address){
		this.address = address;
	}
	/**辅导结束时间*/
	public java.util.Date getEndTime(){
		return this.endTime;
	}
	/**辅导结束时间*/
	public void setEndTime(java.util.Date endTime){
		this.endTime = endTime;
	}
	/**开课人数*/
	public java.math.BigDecimal getNumStudents(){
		return this.numStudents;
	}
	/**开课人数*/
	public void setNumStudents(java.math.BigDecimal numStudents){
		this.numStudents = numStudents;
	}
	/**报名人数*/
	public java.math.BigDecimal getNumApply(){
		return this.numApply;
	}
	/**报名人数*/
	public void setNumApply(java.math.BigDecimal numApply){
		this.numApply = numApply;
	}
	/**见过人数*/
	public java.math.BigDecimal getNumMeet(){
		return this.numMeet;
	}
	/**见过人数*/
	public void setNumMeet(java.math.BigDecimal numMeet){
		this.numMeet = numMeet;
	}
	/**备注*/
	public String getRemark(){
		return this.remark;
	}
	/**备注*/
	public void setRemark(String remark){
		this.remark = remark;
	}
	/**报名截至时间*/
	public java.util.Date getApplyStartDate(){
		return this.applyStartDate;
	}
	/**报名截至时间*/
	public void setApplyStartDate(java.util.Date applyStartDate){
		this.applyStartDate = applyStartDate;
	}
	/**报名开始时间*/
	public java.util.Date getApplyEndDate(){
		return this.applyEndDate;
	}
	/**报名开始时间*/
	public void setApplyEndDate(java.util.Date applyEndDate){
		this.applyEndDate = applyEndDate;
	}
	/**状态（0：众筹中，1：辅导中，2：已解决，3：关闭/取消)*/
	public String getStatus(){
		return this.status;
	}
	/**状态（0：众筹中，1：辅导中，2：已解决，3：关闭/取消)*/
	public void setStatus(String status){
		this.status = status;
	}
	/**取消原因*/
	public String getCancelNote(){
		return this.cancelNote;
	}
	/**取消原因*/
	public void setCancelNote(String cancelNote){
		this.cancelNote = cancelNote;
	}
}
