package com.cloudsoaring.web.social.task;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.scheduling.quartz.QuartzJobBean;

import com.cloudsoaring.common.utils.FileUtil;
import com.cloudsoaring.web.bus.service.MessageService;
import com.cloudsoaring.web.points.service.PointService;
import com.cloudsoaring.web.social.service.TutorService;
import com.cloudsoaring.web.social.view.TutorView;
/**
 * 众筹辅导报名时间截止但报名人数未达到最小数时，设置定时任务关闭辅导
 * 
 */
public class TutorTimerTaskJob extends QuartzJobBean{
    private Logger logger = Logger.getLogger(TutorTimerTaskJob.class);
    @Autowired
    private TutorService tutorService;
    @Autowired
    private MessageService messageService;
    @Autowired
    private PointService pointService;
    @Override
    protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
        TutorView view = new TutorView();
        view.setEndDate(new Date());
        view.setStatus("3");
        List<TutorView> tutorList=tutorService.searchTutorList(view);
       for(TutorView tutor:tutorList){
           FileUtil.beginTransaction();
           try{
           //  if(tutor.getEndDate() !=null&&compare(tutor.getEndDate(), new Date())<0) {
               //类型,-1表示小于,0是等于,1是大于.
               tutorService.executeInternal(tutor);
             //}
             FileUtil.commitTransaction();
           }catch(Exception ex){
               ex.printStackTrace();
               logger.error(ex.getMessage(), ex);
               FileUtil.rollbackTransaction();
           }
           
       }
        
    }
}


