package com.cloudsoaring.web.social.task;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;


import org.springframework.scheduling.quartz.QuartzJobBean;

import com.cloudsoaring.common.utils.FileUtil;
import com.cloudsoaring.web.bus.service.MessageService;
import com.cloudsoaring.web.course.controller.HttpUtils;
import com.cloudsoaring.web.course.controller.PropertyUtil;
import com.cloudsoaring.web.points.service.PointService;
import com.cloudsoaring.web.social.service.TutorService;
import com.cloudsoaring.web.social.view.TutorView;

import net.sf.json.JSONObject;
/**
 * 众筹辅导报名时间截止但报名人数未达到最小数时，设置定时任务关闭辅导
 * 
 */
public class runOriginTaskJob extends QuartzJobBean{
    private Logger logger = Logger.getLogger(runOriginTaskJob.class);
   
    @Override
    protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		String application_tokenStr = HttpUtils.sendAuthorDataPost("https://passport.yceduyun.com/passport/oauth/token", "42F9292D78B1386F", "e1e034ac44a63ba61e8e9595689efd4c");
		JSONObject application_tokenJson = JSONObject.fromObject(application_tokenStr);
		String application_token = application_tokenJson.getString("access_token");
		//获取已授权机构
		String orgStr = HttpUtils.sendGet(
				PropertyUtil.getProperty("https://gwss.yceduyun.com") + "/apis/organzation/v1.0/getPageOrganizationByApply",
				"access_token=" + application_token+"&tenantId=002"+"&url=remote.hseduyun.com"+"&page=1&rows=10");
		System.out.println("---------------------------------------启动定时--------------------");
		System.out.println(orgStr);
		JSONObject userjson = JSONObject.fromObject(orgStr);
		System.out.println("---------------------------------------"+userjson+"--------------------");
        
    }
}


