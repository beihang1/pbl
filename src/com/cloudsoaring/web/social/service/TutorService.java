package com.cloudsoaring.web.social.service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudsoaring.common.utils.DateUtil;
import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.exception.BusinessException;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.service.MessageService;
import com.cloudsoaring.web.common.service.BaseService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseDbConstants;
import com.cloudsoaring.web.course.constant.CourseDefaultValues;
import com.cloudsoaring.web.course.entity.DiscussDataEntity;
import com.cloudsoaring.web.course.entity.DiscussEntity;
import com.cloudsoaring.web.course.entity.ScoreDataEntity;
import com.cloudsoaring.web.course.entity.ScoreEntity;
import com.cloudsoaring.web.course.entity.TagLinkEntity;
import com.cloudsoaring.web.course.view.DiscussView;
import com.cloudsoaring.web.course.view.TagView;
import com.cloudsoaring.web.pointmanager.service.PointManagerService;
import com.cloudsoaring.web.points.constant.PointConstants;
import com.cloudsoaring.web.points.entity.UserPoint;
import com.cloudsoaring.web.points.service.PointService;
import com.cloudsoaring.web.social.constant.SocialConstants;
import com.cloudsoaring.web.social.entity.TutorDefEntity;
import com.cloudsoaring.web.social.entity.TutorInstEntity;
import com.cloudsoaring.web.social.entity.TutorRecommendEntity;
import com.cloudsoaring.web.social.entity.TutorStudentsEntity;
import com.cloudsoaring.web.social.view.TutorView;
import com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity;
import com.cloudsoaring.web.trainingplatform.utils.TpConfigUtil;

/***
 * 我的辅导模块相关的业务处理Service
 * 
 * @author chenyong
 *
 */
@Service
public class TutorService extends BaseService implements SocialConstants {
	/** 支付 */
	@Autowired
	private PointService pointService;
	/** 消息处理Service */
	@Autowired
	private MessageService messageService;
	@Autowired
    private PointManagerService pointManagerService;
	/**
	 * 辅导列表
	 * 
	 * @author chenyong
	 * @param tutorDefEntity
	 *            辅导定义对象
	 * @param pageSize
	 *            每页显示行数
	 * @param pageNumber
	 *            当前页码
	 * @return 返回结果
	 */
	public ResultBean searchTutorList(TutorDefEntity tutorDefEntity) {
		// 判断用户是否登录
		assertNotGuest();
		tutorDefEntity.setTeacherId(WebContext.getSessionUserId());
		/*
		 * if(pageSize != null){ tutorDefEntity.setPageSize(pageSize); }
		 * if(pageNumber != null){ tutorDefEntity.setPageNumber(pageNumber); }
		 */
		tutorDefEntity.setTeacherId(WebContext.getSessionUserId());
		tutorDefEntity.setSortName("updateDate");
		tutorDefEntity.setSortOrder("desc");
		ResultBean result = searchList4Page(tutorDefEntity, TUTOR_DEF_SELECT_TUTOR_DEF);
		return result;
	}

	/**
	 * 辅导提交
	 * 
	 * @author chenyong
	 * @param tutorView
	 *            辅导对象；
	 * @return 返回结果
	 */
	@ResponseBody
	public ResultBean submitTutor(TutorView tutorView) {
		ResultBean result = new ResultBean();
		// 判断用户是否登录
		assertNotGuest();
		// 判断报名截止日期是否在当前日期之后
		if (tutorView.getApplyEndDate() != null) {
			if (tutorView.getApplyEndDate().before(new Date())) {
				result.setStatus(false);
				result.setMessages(getMessage(MSG_E_TUTOR_APPLY_END_DATE_AFTER_NOW));
				return result;
			}
		}
		// 判断开课时间是否在报名截止日期之后
		/*
		 * if (tutorView.getApplyEndDate() != null && tutorView.getStartTime()
		 * != null) { if (tutorView.getApplyEndDate().after(new Date())) {
		 * result.setStatus(false);
		 * result.setMessages(getMessage(TUTOR_START_TIME_AFTER_APPLY_END_DATE
		 * )); return result; } }
		 */
		// 判断辅导类型
		boolean tutorTypeSingleFlag = true;
		if (StringUtil.isNotEmpty(tutorView.getTutorType())) {
			if (TUTOR_TYPE_1VN.equalsIgnoreCase(tutorView.getTutorType())) {
				tutorTypeSingleFlag = false;
			} else {
				tutorView.setNumStudents(new BigDecimal(1));
			}
		} else {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_EMPTY_TUTOR_TYPE));
			return result;
		}
		// 判断新增或者编辑
		boolean insertFlag = true;
		if (StringUtil.isNotEmpty(tutorView.getTutorDefId())) {
			insertFlag = false;
		}
		String tutorDefId = "";
		// 新增
		if (insertFlag) {
			tutorDefId = IDGenerator.genUID();
			tutorView.setTutorDefId(tutorDefId);
			tutorView.setLastInstId(tutorDefId);
			tutorView.setTeacherId(WebContext.getSessionUserId());
			tutorView.setStartDate(tutorView.getApplyStartDate());
			tutorView.setEndDate(tutorView.getApplyEndDate());
			tutorView.setStatus(TUTOR_DEF_STATUS_UNRELEASED);
			tutorView.setCreateUser(WebContext.getSessionUserId());
			tutorView.setUpdateUser(WebContext.getSessionUserId());
			if (StringUtil.isNotEmpty(tutorView.getType())) {
				if (TUTOR_DEF_STATUS_UNRELEASED.equalsIgnoreCase(tutorView
						.getType())) {
					tutorView.setStatus(TUTOR_DEF_STATUS_UNRELEASED);
				} else if (TUTOR_DEF_STATUS_RELEASED.equalsIgnoreCase(tutorView
						.getType())) {
					tutorView.setStatus(TUTOR_DEF_STATUS_RELEASED);
				}
			} else {
				tutorView.setStatus(TUTOR_DEF_STATUS_UNRELEASED);
			}
			if (tutorView.getPrice() != null) {
                if (TpConfigUtil.displayPriceByPoints()) {
                    tutorView.setPrice(tutorView.getPrice().divide(new BigDecimal(TpConfigUtil.getPointsForSmallChange())));
                }
            }
			if(tutorView.getNumStudentsMin()==null){
			    tutorView.setNumStudentsMin(BigDecimal.ONE); 
			}
			super.insert(tutorView, TUTOR_DEF_INSERT_TUTOR_DEF);
			// 辅导实例
			if (!tutorTypeSingleFlag) {
				tutorView.setTutorInstId(IDGenerator.genUID());
				tutorView.setStatus(TUTOR_INST_STATUS_CROWDFUNDING);
				super.insert(tutorView, TUTOR_INST_INSERT_TUTOR_INST);
			}
			// 辅导分类
			if (StringUtil.isNotEmpty(tutorView.getFirstTag())) {
				insertCategory(tutorDefId, tutorView.getFirstTag(),
						tutorView.getSecondTag());
			}
			// 通过积分显示价格
			if (tutorView.getPrice() != null) {
				if (TpConfigUtil.displayPriceByPoints()) {
					tutorView.setTotalPoints(tutorView.getPrice());
					tutorView.setPrice(tutorView.getPrice().divide(new BigDecimal(TpConfigUtil.getPointsForSmallChange())));
				}else{
					tutorView.setTotalPoints(tutorView.getPrice().multiply(
							new BigDecimal(TpConfigUtil
									.getPointsForSmallChange())));
				}
			}
		} else {
			// 编辑
			tutorDefId = tutorView.getTutorDefId();
			// 判断是否可编辑
			TutorDefEntity existDefEntity = new TutorDefEntity();
			existDefEntity.setTutorDefId(tutorDefId);
			existDefEntity = (TutorDefEntity) super.searchOneData(
					existDefEntity, TUTOR_DEF_SELECT_TUTOR_DEF);
			if (existDefEntity != null
					&& existDefEntity.getStatus() != null
					&& TUTOR_DEF_STATUS_RELEASED
							.equalsIgnoreCase(existDefEntity.getStatus()
									.toString())) {
				result.setStatus(false);
				result.setMessages(getMessage(MSG_E_TUTOR_STATUS_FORBID_UPDATE));
				return result;
			}
			tutorView.setStartDate(tutorView.getApplyStartDate());
			tutorView.setEndDate(tutorView.getApplyEndDate());
			tutorView.setUpdateUser(WebContext.getSessionUserId());
			tutorView.setTeacherId(existDefEntity.getTeacherId());
			// 通过积分显示价格
			if (tutorView.getPrice() != null) {
				if (TpConfigUtil.displayPriceByPoints()) {
					tutorView.setTotalPoints(tutorView.getPrice());
					tutorView.setPrice(tutorView.getPrice().divide(new BigDecimal(TpConfigUtil.getPointsForSmallChange())));
				}else{
					tutorView.setTotalPoints(tutorView.getPrice().multiply(
							new BigDecimal(TpConfigUtil
									.getPointsForSmallChange())));
				}
			}
			if (StringUtil.isNotEmpty(tutorView.getType())) {
				if (TUTOR_DEF_STATUS_UNRELEASED.equalsIgnoreCase(tutorView
						.getType())) {
					tutorView.setStatus(TUTOR_DEF_STATUS_UNRELEASED);
				} else if (TUTOR_DEF_STATUS_RELEASED.equalsIgnoreCase(tutorView
						.getType())) {
					tutorView.setStatus(TUTOR_DEF_STATUS_RELEASED);
				}
			} else {
				tutorView.setStatus(existDefEntity.getStatus());
			}
			super.update(tutorView, TUTOR_DEF_UPDATE_TUTOR_DEF);

			// 辅导实例
			if (!tutorTypeSingleFlag) {
				super.update(tutorView, TUTOR_INST_UPDATE_TUTOR_INST);
			}
			// 辅导分类
			if (StringUtil.isNotEmpty(tutorView.getFirstTag())) {
				// 先删除现有分类信息
				deleteCategory(tutorDefId);
				// 插入
				insertCategory(tutorDefId, tutorView.getFirstTag(),
						tutorView.getSecondTag());
			}
		}
		return result;
	}

	/**
	 * 处理辅导分类
	 * 
	 * @param tutorDefId
	 * @param firstTag
	 * @param secondTags
	 */
	private void insertCategory(String tutorDefId, String firstTag,
			String secondTag) {
		TagLinkEntity tagLinkEntity = new TagLinkEntity();
		tagLinkEntity.setLinkId(tutorDefId);
		tagLinkEntity.setTagId(firstTag);
		tagLinkEntity.setLinkType(LINK_TYPE_TOTUR);
		super.insert(tagLinkEntity, CourseDbConstants.TAG_INSERT);
		TagLinkEntity tagLinkSecond = new TagLinkEntity();
		tagLinkSecond.setTagId(secondTag);
		tagLinkSecond.setParentTagId(firstTag);
		tagLinkSecond.setLinkType(LINK_TYPE_TOTUR);
		tagLinkSecond.setLinkId(tutorDefId);
		super.insert(tagLinkSecond, CourseDbConstants.TAG_INSERT);
	}

	/**
	 * 删除当前辅导所处分类信息
	 * 
	 * @param tutorDefId
	 * @param firstTag
	 * @param secondTags
	 */
	private void deleteCategory(String tutorDefId) {
		TagLinkEntity tagLinkEntity = new TagLinkEntity();
		tagLinkEntity.setLinkId(tutorDefId);
		tagLinkEntity.setLinkType(LINK_TYPE_TOTUR);
		super.delete(tagLinkEntity, CourseDbConstants.SQL_RAG_DELETE);
	}

	/**
	 * 辅导方向等信息
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private TutorView searchTagList(String linkId, TutorView tutorView) {
		// 标签信息
		TagView tag = new TagView();
		tag.setLinkId(linkId);
		tag.setLinkType(LINK_TYPE_TOTUR);
		List<TagView> tagList = commonDao.searchList(tag,
				CourseDbConstants.SQL_SEARCH_TAG_BY_COURSE_ID);
		if (tagList != null && tagList.size() > 0) {
			for (int i = 0; i < tagList.size(); i++) {
				if (tagList.get(i).getParentTagId() == null) {
					tutorView.setFirstTag(tagList.get(i).getTagId());
					tutorView.setFirstTagName(tagList.get(i).getTagName());
				} else {
					tutorView.setSecondTag(tagList.get(i).getTagId());
					tutorView.setSecondTagName(tagList.get(i).getTagName());
				}
			}
		}
		return tutorView;
	}

	/**
	 * 辅导删除
	 * 
	 * @author chenyong
	 * @param tutorDefIdArray
	 *            辅导定义ID数组；
	 * @return 返回结果
	 */
	@ResponseBody
	public ResultBean deleteTutor(String[] tutorDefIdArray) {
		ResultBean result = new ResultBean();
		// 判断用户是否登录
		assertNotGuest();

		// 判断是否可删除
		if (tutorDefIdArray != null && tutorDefIdArray.length != 0) {
			for (String tutorDefId : tutorDefIdArray) {
				TutorDefEntity existDefEntity = new TutorDefEntity();
				existDefEntity.setTutorDefId(tutorDefId);
				existDefEntity = (TutorDefEntity) super.searchOneData(
						existDefEntity, TUTOR_DEF_SELECT_TUTOR_DEF);
				if (existDefEntity != null
						&& existDefEntity.getStatus() != null
						&& TUTOR_DEF_STATUS_RELEASED
								.equalsIgnoreCase(existDefEntity.getStatus()
										.toString())) {
					result.setStatus(false);
					result.setMessages(getMessage(TUTOR_STATUS_FORBID_DELETE));
					return result;
				}
				// 判断辅导类型
				boolean tutorTypeSingleFlag = true;
				if (existDefEntity != null
						&& StringUtil.isNotEmpty(existDefEntity.getTutorType())) {
					if (TUTOR_TYPE_1VN.equalsIgnoreCase(existDefEntity
							.getTutorType())) {
						tutorTypeSingleFlag = false;
					}
				} else {
					result.setStatus(false);
					result.setMessages(getMessage(MSG_E_EMPTY_TUTOR_TYPE));
					return result;
				}

				// 删除实例
				if (!tutorTypeSingleFlag) {
					TutorInstEntity delInstEntity = new TutorInstEntity();
					delInstEntity.setTutorDefId(tutorDefId);
					super.delete(delInstEntity, TUTOR_INST_DELETE_TUTOR_INST);
				}
				// 删除分类
				deleteCategory(tutorDefId);
				// 删除定义
				super.delete(existDefEntity, TUTOR_DEF_DELETE_TUTOR_DEF);
			}
		}

		return result;
	}

	/**
	 * 辅导取消
	 * 
	 * @author chenyong
	 * @param tutorInstId
	 *            辅导实例ID；
	 * @return 返回结果
	 */
	@ResponseBody
	public ResultBean cancelTutor(String tutorDefId) {
		ResultBean result = new ResultBean();
		// 判断用户是否登录
		assertNotGuest();

		// 判断是否可取消
		TutorDefEntity existDefEntity = new TutorDefEntity();
		existDefEntity.setTutorDefId(tutorDefId);
		existDefEntity = (TutorDefEntity) super.searchOneData(existDefEntity,
				TUTOR_DEF_SELECT_TUTOR_DEF);
		if (existDefEntity == null) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_TUTOR_STATUS_FORBID_CANCEL));
			return result;
		}
		// 判断辅导类型
		boolean tutorTypeSingleFlag = true;
		if (StringUtil.isNotEmpty(existDefEntity.getTutorType())) {
			if (TUTOR_TYPE_1VN.equalsIgnoreCase(existDefEntity.getTutorType())) {
				tutorTypeSingleFlag = false;
			}
		} else {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_EMPTY_TUTOR_TYPE));
			return result;
		}

		// 取消实例
		if (!tutorTypeSingleFlag) {
			TutorInstEntity cancelInstEntity = new TutorInstEntity();
			cancelInstEntity.setTutorDefId(tutorDefId);
			cancelInstEntity.setUpdateUser(WebContext.getSessionUserId());
			cancelInstEntity.setStatus(TUTOR_INST_STATUS_CLOSED);
			super.update(cancelInstEntity,
					TUTOR_INST_UPDATE_TUTOR_INST_FOR_CANCEL);
		}

		return result;
	}

	/**
	 * 辅导发布和取消发布
	 * 
	 * @author chenyong
	 * @param tutorDefId
	 *            辅导定义ID；
	 * @param statusType
	 *            辅导状态 cancel 取消发布 issue 发布；
	 * @return 返回结果
	 */
	@SuppressWarnings("unchecked")
	@ResponseBody
	public ResultBean updateTutorStatus(String tutorDefId, String statusType) {
		ResultBean result = new ResultBean();
		// 判断用户是否登录
		assertNotGuest();

		// 判断是否可取消
		TutorDefEntity existDefEntity = new TutorDefEntity();
		existDefEntity.setTutorDefId(tutorDefId);
		existDefEntity = (TutorDefEntity) super.searchOneData(existDefEntity,
				TUTOR_DEF_SELECT_TUTOR_DEF);

		if (existDefEntity == null) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_TUTOR_STATUS_FORBID_CANCEL));
			return result;
		} else {
			if (existDefEntity != null
					&& TUTOR_DEF_STATUS_TYPE_CANCEL
							.equalsIgnoreCase(statusType)) {
				TutorInstEntity existInstEntity = new TutorInstEntity();
				existInstEntity.setTutorDefId(tutorDefId);
				List<TutorInstEntity> existInstList = super.search(
						existInstEntity, TUTOR_INST_SEARCH_TUTOR_INST);
				if (TUTOR_TYPE_1V1.equalsIgnoreCase(existDefEntity
						.getTutorType())) {
					if (existInstList != null && existInstList.size() != 0) {
						result.setStatus(false);
						result.setMessages(getMessage(MSG_E_TUTOR_STATUS_FORBID_CANCEL));
						return result;
					}
				} else if (TUTOR_TYPE_1VN.equalsIgnoreCase(existDefEntity
						.getTutorType())) {
					if (existInstList != null && existInstList.size() != 0) {
						existInstEntity = existInstList.get(0);
						if (existInstEntity.getNumApply() != null
								&& existInstEntity.getNumApply().intValue() > 0) {
							result.setStatus(false);
							result.setMessages(getMessage(MSG_E_TUTOR_STATUS_FORBID_CANCEL));
							return result;
						}
					}
				}
			}

			TutorDefEntity cancelDefEntity = new TutorDefEntity();
			cancelDefEntity.setTutorDefId(tutorDefId);
			cancelDefEntity.setUpdateUser(WebContext.getSessionUserId());
			if (TUTOR_DEF_STATUS_TYPE_ISSUE.equalsIgnoreCase(statusType)) {
				cancelDefEntity.setStatus(TUTOR_DEF_STATUS_RELEASED);
			} else if (TUTOR_DEF_STATUS_TYPE_CANCEL
					.equalsIgnoreCase(statusType)) {
				cancelDefEntity.setStatus(TUTOR_DEF_STATUS_UNRELEASED);
			}
			super.update(cancelDefEntity, TUTOR_DEF_UPDATE_FOR_ISSUE);
		}

		return result;
	}

	/**
	 * 编辑/详情页面需要的信息
	 * 
	 * @param
	 * @return
	 * @throws ParseException
	 */
	public TutorView setTutorById(String tutorDefId, String tutorType,
			String pageType) {
		ResultBean result = tutorDetail(tutorDefId, tutorType);
		return (TutorView) result.getData();
	}

	/**
	 * 辅导详情
	 * 
	 * @author chenyong
	 * @param tutorDefId
	 *            辅导定义ID；
	 * @param tutorType
	 *            辅导类型；
	 * @return 返回结果
	 */
	@SuppressWarnings({ "static-access" })
	@ResponseBody
	public ResultBean tutorDetail(String tutorDefId, String tutorType) {
		ResultBean result = new ResultBean();
		// 判断用户是否登录
		assertNotGuest();
		TutorView tutorView = new TutorView();
		tutorView.setTutorDefId(tutorDefId);
		if (TUTOR_TYPE_1VN.equalsIgnoreCase(tutorType)) {
			tutorView.setTutorType(TUTOR_TYPE_1VN);
			tutorView = super.searchOneData(tutorView,
					TUTOR_INST_SEARCH_TUTOR_VIEW);
		} else {
			tutorView = super.searchOneData(tutorView,
					TUTOR_DEF_SELECT_TUTOR_VIEW_FOR_DETAIL);
		}

		// 查询辅导方向信息
		tutorView = searchTagList(tutorDefId, tutorView);

		result.setData(tutorView).success();
		return result;
	}


	/**
	 * 
	 * 老师首页：人员列表
	 * 
	 * @param tutorInstId
	 * @return
	 */
	public ResultBean seachStudentsList(String tutorDefId, Integer pageSize,
			Integer pageNumber) {
		return tutorStudentsList(tutorDefId, pageSize, pageNumber);
	}

	/**
	 * 辅导详情-人员列表
	 * 
	 * @author chenyong
	 * @param tutorDefId
	 *            辅导定义ID；
	 * @param pageSize
	 *            每页显示行数
	 * @param pageNumber
	 *            当前页码
	 * @return 返回结果
	 */
	@SuppressWarnings({ "unchecked" })
	@ResponseBody
	public ResultBean tutorStudentsList(String tutorDefId, Integer pageSize,
			Integer pageNumber) {
		ResultBean result = new ResultBean();
		// 判断用户是否登录
		assertNotGuest();
		//
		TutorView tutorStuView = new TutorView();
		tutorStuView.setTutorDefId(tutorDefId);
		tutorStuView = super.searchOneData(tutorStuView,
				TUTOR_DEF_SELECT_TUTOR_VIEW_FOR_DETAIL);
		if (tutorStuView != null) {
			// 判断是否老师登录且辅导属于自己的众筹类型
			if (StringUtil.isNotEmpty(tutorStuView.getTeacherId())
					&& StringUtil.isNotEmpty(tutorStuView.getTutorType())
					&& WebContext.getSessionUserId().equalsIgnoreCase(
							tutorStuView.getTeacherId())
					&& TUTOR_TYPE_1VN.equalsIgnoreCase(tutorStuView
							.getTutorType())) {
				tutorStuView.setShowMeetBtn(true);
			}
			//
			TutorStudentsEntity tutorStuEntity = new TutorStudentsEntity();
			tutorStuEntity.setTutorDefId(tutorDefId);
			if (pageSize != null) {
				tutorStuEntity.setPageSize(pageSize);
			}
			if (pageNumber != null) {
				tutorStuEntity.setPageNumber(pageNumber);
			}
			result = super.searchList4Page(tutorStuEntity,
					TUTOR_STUDENTS_SEARCH_TUTOR_STUDENTS_BY_TUTOR_DEF_ID);
			if (result != null && result.getData() != null) {
				List<TutorStudentsEntity> tutorStuList = (List<TutorStudentsEntity>) result
						.getData();
				if (tutorStuList != null && tutorStuList.size() != 0) {
					for (TutorStudentsEntity stuEntity : tutorStuList) {
						stuEntity.setShowMeetBtn(true);
					}
				}
			}
		}
		return result;
	}

	/**
	 * 老师首页-点评列表
	 * 
	 * @author chenyong
	 * @param tutorView
	 *            辅导定义对象
	 * @return 返回结果
	 */
	@SuppressWarnings({ "unchecked" })
	@ResponseBody
	public ResultBean tutorDiscussList(TutorView tutorView) {
		ResultBean result = new ResultBean();
		// 判断用户是否登录
		assertNotGuest();
		DiscussDataEntity discussDataEntity = new DiscussDataEntity();
		discussDataEntity.setLinkType(LINK_TYPE_TOTUR);
		discussDataEntity.setTutorDefId(tutorView.getTutorDefId());
		if (tutorView.getPageSize() != null) {
			discussDataEntity.setPageSize(tutorView.getPageSize());
		}
		if (tutorView.getPageNumber() != null) {
			discussDataEntity.setPageNumber(tutorView.getPageNumber());
		}
		result = commonDao.searchList4Page(discussDataEntity,
				DISCUSS_DATA_SEARCH_DISCUSS_DATA_FOR_TUTOR);
		List<DiscussView> discussDataList = (List<DiscussView>) result
				.getData();

		if (discussDataList != null && discussDataList.size() != 0) {
			for (DiscussView discu : discussDataList) {
				if (isTeacher()) {
					// 当前登录用户教师角色
					discu.setCanDelete(true);
				} else {
					// 当前登录用户不是教师角色
					if (discu.getUserId() != null
							&& discu.getUserId().equals(
									WebContext.getSessionUserId())) {
						discu.setCanDelete(true);
					}
				}
				int count = commonDao.searchCount(discu,
						OPERATE_RECORD_SEARCH_COUNT_FOR_TUTOR);
				discu.setUserLike(count);
			}
		}
		return result;
	}

	/**
	 * 判断当前登录用户是否为教师角色
	 * 
	 * @return true:是教师，false:不是教师
	 */
	private boolean isTeacher() {
		UserRoleEntity ur = new UserRoleEntity();
		ur.setUserId(WebContext.getSessionUserId());
		ur.setRoleId(CourseDefaultValues.ROLE_TEACHER_ROLE);
		int count = commonDao.searchCount(ur, SQL_IS_TEACHHER);
		return count > 0;
	}

	/**
	 * 辅导详情-点评-删除评论
	 * 
	 * @author chenyong
	 * @param tutorInstId
	 *            辅导实例ID；
	 * @param linkId
	 *            评论链接ID
	 * @return 返回结果
	 */
	@ResponseBody
	public ResultBean deleteDiscuss(String tutorInstId, String discussId) {
		ResultBean result = new ResultBean();
		// 判断用户是否登录
		assertNotGuest();
		// 链接ID为空
		assertNotEmpty(tutorInstId, NULL_TUTOR_INST_ID);

		if (isTeacher()) {
			result.setStatus(false);
			result.setMessages(getMessage(TUTOR_DISCUSS_FORBID_DELETE_FOR_STUDENT));
			return result;
		}

		DiscussDataEntity dd = new DiscussDataEntity();
		dd.setDiscussId(discussId);
		commonDao.deleteData(dd, CourseDbConstants.DELETE_ALL_DISCUSS_DATA);
		// //删除点赞的数据
		// OperateRecordEntity or = new OperateRecordEntity();
		// or.setLink(linkId);
		// or.setOperateType(CourseStateConstants.OPERATE_LIKE_DISCUSS);
		// commonDao.deleteData(or,
		// CourseDbConstants.SQL_DELETE_OPERATE_BY_LINK_AND_TYPE);
		return result.setStatus(true);
	}

	/**
	 * 我的辅导列表
	 * 
	 * @author chenyong
	 * @param tutorView
	 *            辅导对象
	 * @return 返回结果
	 */
	public ResultBean userTutorList(TutorView tutorView) {
		// 判断用户是否登录
		assertNotGuest();
		tutorView.setStudentId(WebContext.getSessionUserId());
		return super.searchList4Page(tutorView,
				TUTOR_INST_SEARCH_TUTOR_VIEW_FOR_STU);
	}

	/**
	 * 我的辅导（进行中）列表
	 * 
	 * @author chenyong
	 * @param tutorView
	 *            辅导对象
	 * @return 返回结果
	 */
	public ResultBean userTutorGoingOnList(TutorView tutorView) {
		// 判断用户是否登录
		assertNotGuest();
		tutorView.setStudentId(WebContext.getSessionUserId());
		return super.searchList4Page(tutorView,
				TUTOR_INST_SEARCH_TUTOR_VIEW_FOR_STU_AUDITING);
	}

	/**
	 * 我的辅导-取消预约
	 * 
	 * @author chenyong
	 * @param tutorInstId
	 *            辅导实例ID；
	 * @return 返回结果
	 */
	@ResponseBody
	public ResultBean cancelReserve(String tutorInstId) {
		ResultBean result = new ResultBean();
		// 判断用户是否登录
		assertNotGuest();
		TutorStudentsEntity cancelReserveEntity = new TutorStudentsEntity();
		cancelReserveEntity.setTutorInstId(tutorInstId);
		cancelReserveEntity.setStudentId(WebContext.getSessionUserId());
		cancelReserveEntity.setUpdateUser(WebContext.getSessionUserId());
		cancelReserveEntity.setStatus(TUTOR_STUDENTS_STATUS_CANCEL);
		
		super.update(cancelReserveEntity,TUTOR_STUDENTS_UPDATE_FOR_CANCEL_RESERVE);
		
		//判断学员状态
        TutorStudentsEntity studentsEntity=commonDao.searchOneData(cancelReserveEntity, SQL_SEARCH_STATUS_STUDENT);
        if(TUTOR_STUDENTS_STATUS_CANCEL.equals(studentsEntity.getStatus())){
         // 获取辅导学员积分价格
            TutorView entity=commonDao.searchOneData(tutorInstId, SQL_SEARCH_TEACHER_ID);
            BigDecimal needPayPoints = entity.getPrice().multiply(new BigDecimal(TpConfigUtil.getPointsForSmallChange()));
            UserPoint changePointReuslt= pointService.earnPoint("HS", WebContext.getSessionUserId(), "A0010",needPayPoints, "","");
            if(!changePointReuslt.isStatus()) {
               throw new BusinessException(changePointReuslt.getErrorCode(),changePointReuslt.getMessages());
            }  
        }
		return result;
	}

	/**
	 * 我的辅导-编辑预约
	 * 
	 * @author chenyong
	 * @param tutorInstId
	 *            辅导实例ID；
	 * @return 返回结果
	 */
	@ResponseBody
	public ResultBean editReserve(TutorStudentsEntity tutorStudentsEntity) {
		ResultBean result = new ResultBean();
		// 判断用户是否登录
		assertNotGuest();
		tutorStudentsEntity.setStudentId(WebContext.getSessionUserId());
		tutorStudentsEntity.setUpdateUser(WebContext.getSessionUserId());
		super.update(tutorStudentsEntity,
				TUTOR_STUDENTS_UPDATE_FOR_EDIT_RESERVE);

		return result;
	}

	/**
	 * 
	 * @param tutorView
	 * @return
	 */
	public TutorView seachMeetInfo(TutorView tutorView) {
		TutorView tutor = (TutorView) tutorMeetApprovalDetail(tutorView)
				.getData();
		return tutor;
	}

	/**
	 * 约见管理-约见待审核详情
	 * 
	 * @author chenyong
	 * @param tutorView
	 *            辅导对象
	 * @return 返回结果
	 */
	@SuppressWarnings("static-access")
	@ResponseBody
	public ResultBean tutorMeetApprovalDetail(TutorView tutorView) {
		ResultBean result = new ResultBean();
		// 判断用户是否登录
		assertNotGuest();
		// 判断辅导实例ID是否为空
		assertNotEmpty(tutorView.getTutorInstId(), NULL_TUTOR_INST_ID);
		tutorView = super.searchOneData(tutorView,
				TUTOR_INST_SELECT_TUTOR_VIEW_FOR_MEET_APPROVAL);
		result.setData(tutorView).success();
		return result;
	}

	/**
	 * 约见管理-约见待审核列表
	 * 
	 * @author chenyong
	 * @param tutorView
	 * @param tutorView
	 *            辅导对象
	 * @return 返回结果
	 */
	@ResponseBody
	public ResultBean tutorMeetApprovalList(TutorView tutorView) {
//		 判断用户是否登录
		assertNotGuest();
		tutorView.setTutorType(TUTOR_TYPE_1V1);
		tutorView.setTeacherId(WebContext.getSessionUserId());
		return super.searchList4Page(tutorView,
				TUTOR_INST_SELECT_TUTOR_TEACHER_HANDLE);
	}

	/**
	 * 约见管理-约见待审核-通过、不通过
	 * 
	 * @author chenyong
	 * @param tutorView
	 *            辅导对象
	 * @return 返回结果
	 */
	@SuppressWarnings("static-access")
	@ResponseBody
	public ResultBean tutorMeetApproval(TutorView tutorView) {
		ResultBean result = new ResultBean();
		// 判断用户是否登录
		assertNotGuest();
		
		assertExist(tutorView, "TutorInst.selectCount4Apply", "不存在可操作的辅导");
		
		
		tutorView.setTeacherId(WebContext.getSessionUserId());
		tutorView.setUpdateUser(WebContext.getSessionUserId());
		if (StringUtil.isNotEmpty(tutorView.getStudentStatus())) {
			if (TUTOR_STUDENTS_STATUS_CONFIRM.equalsIgnoreCase(tutorView
					.getStatus())) {
				tutorView.setStatus(TUTOR_INST_STATUS_CROWDFUNDING);
			} else if (TUTOR_STUDENTS_STATUS_REFUSE.equalsIgnoreCase(tutorView
					.getStatus())) {
				tutorView.setStatus(TUTOR_INST_STATUS_REFUSED);
			}
		}
		// 更新实例信息
		update(tutorView, TUTOR_INST_UPDATE_TUTOR_INST_FOR_MEET_APPROVAL);
		
		
		// 更新学员信息表
		TutorStudentsEntity meetApprovalStuEntity = new TutorStudentsEntity();
		meetApprovalStuEntity.setTutorInstId(tutorView.getTutorInstId());
		meetApprovalStuEntity.setStudentId(tutorView.getStudentId());
		meetApprovalStuEntity = super.searchOneData(meetApprovalStuEntity,TUTOR_STUDENTS_SEARCH_TUTOR_STUDENTS);
		// 判断用户是否登录
		if (meetApprovalStuEntity == null) {
			result.setStatus(false);
			result.setMessages(getMessage(NULL_TUTOR_INST_ID));
			return result;
		} else {
			Date applyTime = meetApprovalStuEntity.getApplyTime();
			Date replyTime = new Date();
			int responseTime = DateUtil.getRemainder(replyTime, applyTime) * 24;
			meetApprovalStuEntity.setResponseTime(new BigDecimal(responseTime));
			meetApprovalStuEntity.setUpdateUser(WebContext.getSessionUserId());
			meetApprovalStuEntity.setReplyContent(tutorView.getReplyContent());
			if (StringUtil.isNotEmpty(tutorView.getStudentStatus())) {
				meetApprovalStuEntity.setStatus(tutorView.getStudentStatus());
				super.update(meetApprovalStuEntity,
						TUTOR_STUDENTS_UPDATE_TUTOR_STUDENTS_FOR_MEET_APPROVAL);
			}
		}
		//导师拒绝，返还积分给学员
		if(tutorView.getStudentStatus().equals(TUTOR_STUDENTS_STATUS_REFUSE)){
		    // 获取辅导xue学员积分
		    TutorView entity=commonDao.searchOneData(tutorView.getTutorInstId(), SQL_SEARCH_TEACHER_ID);
		    BigDecimal needPayPoints = entity.getPrice().multiply(new BigDecimal(TpConfigUtil.getPointsForSmallChange()));
		    UserPoint changePointReuslt= pointService.earnPoint("HS", tutorView.getStudentId(), "A0010",needPayPoints, "","");
		    if(!changePointReuslt.isStatus()) {
		       throw new BusinessException(changePointReuslt.getErrorCode(),changePointReuslt.getMessages());
		    } 
        }
		
		// 更新辅导表中约见人数 - 老师通过，不通过不影响报名人数
//		TutorView meetView = new TutorView();
//		meetView.setTutorInstId(tutorView.getTutorInstId());
//		meetView = super.searchOneData(meetView, TUTOR_INST_SEARCH_TUTOR_VIEW);
//		updateTutorInst4APPLY(tutorView.getTutorInstId(),  meetView.getTutorDefId(), meetView.getTeacherId());
				
		return result.success();
	}


	/**
	 * 我的辅导-立即支付详情
	 * 
	 * @author chenyong
	 * @param tutorView
	 *            辅导对象
	 * @return 返回结果
	 */
	@SuppressWarnings("unchecked")
	public ResultBean immediatePayDetail(String tutorInstId) {
		ResultBean resultBean = new ResultBean();
		TutorView conditionTutorView = new TutorView();
		// 判断用户是否登录
		assertNotGuest();
		// 判断辅导实例ID是否为空
		assertNotEmpty(tutorInstId, NULL_TUTOR_INST_ID);
		conditionTutorView.setTutorInstId(tutorInstId);
		TutorView tutorView = commonDao.searchOneData(conditionTutorView,
				TUTOR_INST_SELECT_TUTOR_VIEW_FOR_PAY);

		// 查询当前辅导学员状态
		TutorStudentsEntity tutorStuEntity = new TutorStudentsEntity();
		tutorStuEntity.setTutorInstId(conditionTutorView.getTutorInstId());
		tutorStuEntity.setStudentId(WebContext.getSessionUserId());
		tutorStuEntity = commonDao.searchOneData(tutorStuEntity,
				TUTOR_STUDENTS_SEARCH_TUTOR_STUDENTS);
		if (tutorView != null && tutorStuEntity != null) {
			tutorView.setStudentStatus(tutorStuEntity.getStatus());
		}

		// 查询学生积分
		if (tutorView != null) {
			UserPoint studentPoint = pointService.fetchPoint(WebContext
					.getSessionUserId());
			if (studentPoint != null) {
				tutorView.setTotalPoints(studentPoint.getPoints());
			}

			// 辅导耗用积分
			if (tutorView.getPrice() != null) {
				// 通过积分显示价格
				if (TpConfigUtil.displayPriceByPoints()) {
					tutorView.setPrice(tutorView.getPrice());
				} else {
					tutorView.setPrice(tutorView.getPrice());
				}
			}
		}

		// 积分余额
		if (tutorView != null && tutorView.getTotalPoints() != null) {
			BigDecimal remainPoints = tutorView.getTotalPoints();
			if (tutorView.getPrice() != null) {
				remainPoints = remainPoints.subtract(tutorView.getPrice()
						.multiply(
								new BigDecimal(TpConfigUtil
										.getPointsForSmallChange())));
			}
			tutorView.setRemainPoints(remainPoints);
		}

		// 查询参与者信息
		TutorStudentsEntity tutorStudentsEntity = new TutorStudentsEntity();
		tutorStudentsEntity.setTutorInstId(tutorInstId);
		List<TutorStudentsEntity> tutorStudentsList = commonDao.searchList(
				tutorStudentsEntity, TUTOR_STUDENTS_SEARCH_TUTOR_STUDENTS);
		if (tutorStudentsList != null && tutorStudentsList.size() != 0) {
			// for (int i=0; i<tutorStudentsList.size(); i++) {
			// tutorStudentsEntity = tutorStudentsList.get(i);
			// if
			// (WebContext.getSessionUserId().equalsIgnoreCase(tutorStudentsEntity.getStudentId()))
			// {
			// tutorStudentsList.remove(i);
			// }
			// }
		}
		tutorView.setTutorStudentList(tutorStudentsList);

		resultBean.setData(tutorView);
		return resultBean;
	}


	/**
	 * 添加辅导的人数，并修改状态
	 * 
	 * @param tutor
	 * @return
	 */
	private boolean changeTutorStatus4Full(String tutorInstId,
			BigDecimal tutorNum) {
		TutorView tutor = new TutorView();
		tutor.setTutorInstId(tutorInstId);
		int studentJoinedNum = searchOneData(tutor,
				SQL_TUTOR_STUDENTS_JOINED_NUM);
		if (tutorNum != null && tutorNum.intValue() == studentJoinedNum) {
			TutorInstEntity query = new TutorInstEntity();
			query.setTutorInstId(tutor.getTutorInstId());
			TutorInstEntity info = searchOneData(query);
			info.setUpdateUser(WebContext.getSessionUserId());
			info.setStatus(TUTOR_INST_STATUS_TUTORING);
			update(info);
		}
		return true;
	}

	/**
	 * 我的辅导-支付成功
	 * 
	 * @author chenyong
	 * @param tutorView
	 *            辅导对象
	 * @return 返回结果
	 */
	@ResponseBody
	public ResultBean paySuccess(TutorView conditionTutorView) {
		ResultBean resultBean = new ResultBean();
		// 判断用户是否登录
		assertNotGuest();
		TutorView tutorView = commonDao.searchOneData(conditionTutorView,
				TUTOR_INST_SELECT_TUTOR_VIEW_FOR_PAY);
		// 手机端需要信息
		if (tutorView != null) {
			tutorView.setTeacherPersonName(tutorView.getPersonName());
			tutorView.setTeacherEmail(tutorView.getEmail());
			tutorView.setTeacherPhone(tutorView.getPhone());
			tutorView.setTeacherMobile(tutorView.getMobile());
			tutorView.setTeacherImage(tutorView.getImage());
			tutorView.setTeacherWorkplace(tutorView.getWorkplace());
			tutorView.setTeacherPosition(tutorView.getPosition());
		}

		// 查询当前辅导学员状态
		TutorStudentsEntity tutorStuEntity = new TutorStudentsEntity();
		tutorStuEntity.setTutorInstId(conditionTutorView.getTutorInstId());
		tutorStuEntity.setStudentId(WebContext.getSessionUserId());
		tutorStuEntity = commonDao.searchOneData(tutorStuEntity,
				TUTOR_STUDENTS_SEARCH_TUTOR_STUDENTS);
		if (tutorView != null && tutorStuEntity != null) {
			tutorView.setStudentStatus(tutorStuEntity.getStatus());
			// 手机端需要信息
			tutorView.setPersonName(tutorStuEntity.getPersonName());
			tutorView.setEmail(tutorStuEntity.getEmail());
			tutorView.setPhone(tutorStuEntity.getPhone());
			tutorView.setMobile(tutorStuEntity.getMobile());
			tutorView.setImage(tutorStuEntity.getImage());
		}

		// 查询学生积分
		if (tutorView != null) {
			UserPoint studentPoint = pointService.fetchPoint(WebContext
					.getSessionUserId());
			if (studentPoint != null) {
				tutorView.setTotalPoints(studentPoint.getPoints());
			}
		}

		// 支付积分
		if (tutorView != null && tutorView.getTotalPoints() != null) {
			if (tutorView.getPrice() != null) {
				tutorView
						.setRemainPoints(tutorView.getPrice().multiply(
								new BigDecimal(TpConfigUtil
										.getPointsForSmallChange())));
			}
		}
		messageService.sendSystemMessage( WebContext.getSessionUserId(), getMessage(MSG_PAY_SUCCESS,"导师辅导"));
		resultBean.setData(tutorView).setStatus(true);
		return resultBean;
	}

	/**
	 * 我的辅导-创客点评
	 * 
	 * @author chenyong
	 * @param tutorView
	 *            辅导对象
	 * @return 返回结果
	 */
	@ResponseBody
	public ResultBean userDiscuss(TutorView tutorView) {
		ResultBean result = new ResultBean();
		// 从session中取出登录用户的ID，判断用户是否登录
		assertNotGuest();
		// 链接ID为空
		assertNotEmpty(tutorView.getTutorDefId(), "辅导定义ID为空");
		// 内容为空
		assertNotEmpty(tutorView.getContent(), TUTOR_DISCUSS_NULL);
		DiscussEntity de = new DiscussEntity();
		de.setLinkId(tutorView.getTutorDefId());
		de.setLinkType(LINK_TYPE_TOTUR);
		// 检索该业务下是否存在评论
		de = commonDao.searchOneData(de, CourseDbConstants.SQL_DISCUSS_SELECT);

		if (de == null) {
			// 评论不存在，插入
			de = new DiscussEntity();
			de.setId(IDGenerator.genUID());
			de.setLinkId(tutorView.getTutorDefId());
			de.setLinkType(LINK_TYPE_TOTUR);
			de.setTitle(tutorView.getTitle());
			de.setCreateUser(WebContext.getSessionUserId());
			de.setUpdateUser(WebContext.getSessionUserId());
			commonDao.insertData(de, CourseDbConstants.INSERT_DISCUSS);
		}
		// 新增评论数据信息
		DiscussView tutorDiscussView = new DiscussView();
		tutorDiscussView.setDiscussId(de.getId());
		tutorDiscussView.setId(IDGenerator.genUID());
		tutorDiscussView.setUserId(WebContext.getSessionUserId());
		tutorDiscussView.setContent(tutorView.getContent());
		tutorDiscussView.setTargetDiscussId(tutorView
				.getDiscussTargetDiscussId());
		tutorDiscussView.setTargetUserId(tutorView.getDiscussTargetUserId());
		tutorDiscussView.setCreateUser(WebContext.getSessionUserId());
		tutorDiscussView.setUpdateUser(WebContext.getSessionUserId());
		commonDao.insertData(tutorDiscussView,
				CourseDbConstants.INSERT_DISCUSS_DATA);

		// 评分
		ScoreEntity score = new ScoreEntity();
		score.setLinkId(tutorView.getTutorDefId());
		score.setLinkType(LINK_TYPE_TOTUR);
		score = commonDao.searchOneData(score, CourseDbConstants.SEARCH_SCORE);
		if (score == null) {
			score = new ScoreEntity();
			score.setId(IDGenerator.genUID());
			score.setTitle(tutorView.getTitle());
			score.setLinkId(tutorView.getTutorDefId());
			score.setLinkType(LINK_TYPE_TOTUR);
			score.setScore(new BigDecimal(StringUtil.getOrElse(
					tutorView.getScore(), "0")));
			commonDao.insertData(score, CourseDbConstants.INSERT_SCORE);

		}
		ScoreDataEntity scoreDate = new ScoreDataEntity();
		scoreDate.setId(IDGenerator.genUID());
		scoreDate.setScoreId(score.getId());
		scoreDate.setContent(tutorView.getContent());
		scoreDate.setUserId(WebContext.getSessionUserId());
		scoreDate.setScore(new BigDecimal(StringUtil.getOrElse(
				tutorView.getScore(), "0")));
		commonDao.insertData(scoreDate, CourseDbConstants.INSERT_SCORE_DATA);
		if (score != null) {
			commonDao.updateData(score, CourseDbConstants.UPDATE_SCORE_AVG);
		}
		//更新辅导定义表的得分
		commonDao.updateData(tutorView.getTutorDefId(), CourseDbConstants.UPDATE_TUTOR_SCORE);
		// 更新表的评价人数 +1
		commonDao.updateData(score.getLinkId(),
				CourseDbConstants.UPDATE_COURSE_NUM_SCORE_USER);
		// 更新当前辅导学员状态为已点评
		TutorStudentsEntity tutorStuEntity = new TutorStudentsEntity();
		tutorStuEntity.setTutorInstId(tutorView.getTutorInstId());
		tutorStuEntity.setUpdateUser(WebContext.getSessionUserId());
		tutorStuEntity.setStudentId(WebContext.getSessionUserId());
		tutorStuEntity.setStatus(TUTOR_STUDENTS_STATUS_COMMENT);
		commonDao.updateData(tutorStuEntity, TUTOR_STUDENTS_UPDATE_FOR_DISCUSS);

		result.setStatus(true);
		return result;
	}

	/**
	 * 
	 * @param userId
	 * @return
	 */
	public ResultBean tutorRecommendTeacher(String userId) {
		return null;
	}

	/**
	 * 辅导列表
	 * 
	 * @author heyaqin
	 * @param directionTagId
	 *            方向
	 * @param categoryTagId
	 *            分类
	 * @param tutorType
	 *            辅导类型(1V1：1对1，1VN:众筹听课）
	 * @param pageSize
	 *            每页显示多少数据
	 * @param pageNumber
	 *            当前页
	 * @return 返回结果
	 */
	public ResultBean teacherList(String directionTagId, String categoryTagId,
			String tutorType, Integer pageSize, Integer pageNumber) {
		ResultBean result = new ResultBean();
		TutorDefEntity tutorDefEntity = new TutorDefEntity();
		tutorDefEntity.setTagId(categoryTagId);
		tutorDefEntity.setParentTagId(directionTagId);
		// 辅导类型是否是全部
		if (StringUtil.equals(tutorType, SocialConstants.TUTOR_TYPE_1V1)
				|| StringUtil.equals(tutorType, SocialConstants.TUTOR_TYPE_1VN)) {
			tutorDefEntity.setTutorType(tutorType);
		}
		if (pageSize != null) {
			tutorDefEntity.setPageSize(pageSize);
		}
		if (pageNumber != null) {
			tutorDefEntity.setPageNumber(pageNumber);
		}
		result = searchList4Page(tutorDefEntity, SQL_SEARCH_TUTOR_TEACHER_LIST);
		return result;
	}


	/**
	 * 导师详情——导师的所有辅导定义信息
	 * 
	 * @author heyaqin
	 * @param userId
	 *            导师ID
	 * @return 返回结果
	 */
	@SuppressWarnings("unchecked")
	public ResultBean searchTeacherAllDef(String userId) {
		ResultBean result = new ResultBean();
		TutorDefEntity tutorDefEntity = new TutorDefEntity();
		tutorDefEntity.setTeacherId(userId);
		// 检索老师的所有辅导定义信息
		ArrayList<TutorDefEntity> tutorDefList = (ArrayList<TutorDefEntity>) commonDao
				.searchList(tutorDefEntity, SQL_SEARCH_TUTOR_TEACHER_LIST);

		if (StringUtil.isNotEmpty(WebContext.getSessionUserId())) {
			// 当前学生的所有预约中的辅导定义
			tutorDefEntity.setUserId(WebContext.getSessionUserId());
			ArrayList<TutorDefEntity> tutorAppintmentList = (ArrayList<TutorDefEntity>) commonDao
					.searchList(tutorDefEntity, SQL_SEARCH_APPONTMENT_DEF);
			if (tutorDefList.size() > 0) {
				for (TutorDefEntity entity : tutorDefList) {
					for (TutorDefEntity defEntity : tutorAppintmentList) {

						if (StringUtil.equals(defEntity.getTutorDefId(),
								entity.getTutorDefId())) {
							// 设定为预约状态为：预约中
							entity.setJoinAppointment(JOIN_APPOINTMENT);
							break;
						} else {
							entity.setJoinAppointment(JOIN_NO_APPOINTMENT);
						}
					}
				}
			}
		}
		result.setData(tutorDefList);
		return result;
	}

	/**
	 * 导师详情——导师的所有点评信息
	 * 
	 * @param userId
	 *            导师ID
	 * @return 返回结果
	 */
	public ResultBean tutorTeacherDiscussData(String userId) {
		TutorDefEntity tutorDefEntity = new TutorDefEntity();
		tutorDefEntity.setTeacherId(userId);
		// 检索老师的所有点评数据
		return searchList4Page(tutorDefEntity,
				SQL_SEARCH_TUTOR_TEACHER_DISCUSS_DATA);
	}

	/**
	 * 导师详情——导师的所有点评信息
	 * 
	 * @author heyaqin
	 * @param userId
	 *            导师ID
	 * @param pageNumber
	 * @param pageSize
	 * @return 返回结果
	 */
	public ResultBean tutorTeacherDiscuss(String userId, Integer pageSize,
			Integer pageNumber) {
		TutorDefEntity tutorDefEntity = new TutorDefEntity();
		tutorDefEntity.setTeacherId(userId);
		if (pageSize != null) {
			tutorDefEntity.setPageSize(pageSize);
		}
		if (pageNumber != null) {
			tutorDefEntity.setPageNumber(pageNumber);
		}
		// 检索老师的所有点评数据
		return searchList4Page(tutorDefEntity,
				SQL_SEARCH_TUTOR_TEACHER_DISCUSS_DATA);
	}

	/**
	 * 导师详情——导师的所有推荐信息
	 * 
	 * @param userId
	 *            导师ID
	 * @return 返回结果
	 */
	public ResultBean tutorTeacherRecommendData(String userId) {
		TutorDefEntity tutorDefEntity = new TutorDefEntity();
		tutorDefEntity.setTeacherId(userId);
		// 检索所有对老师的推荐语列表信息
		return searchList4Page(tutorDefEntity,
				SQL_SEARCH_TUTOR_TEACHER_RECOMMEND_DATA);
	}

	/**
	 * 导师详情——导师的所有推荐信息
	 * 
	 * @author heyaqin
	 * @param userId
	 *            导师ID
	 * @param pageNumber
	 * @param pageSize
	 * @return 返回结果
	 */
	public ResultBean tutorTeacherRecommend(String userId, Integer pageSize,
			Integer pageNumber) {
		TutorDefEntity tutorDefEntity = new TutorDefEntity();
		tutorDefEntity.setTeacherId(userId);
		if (pageSize != null) {
			tutorDefEntity.setPageSize(pageSize);
		}
		if (pageNumber != null) {
			tutorDefEntity.setPageNumber(pageNumber);
		}
		// 检索所有对老师的推荐语列表信息
		return searchList4Page(tutorDefEntity,
				SQL_SEARCH_TUTOR_TEACHER_RECOMMEND_DATA);
	}

	/**
	 * 插入推荐语（老师推荐表）
	 * 
	 * @param recommend
	 *            推荐语
	 * @param teacherId
	 *            老师ID
	 * @return
	 */
	public ResultBean insertTutorRecommend(String recommend, String teacherId) {
		ResultBean result = new ResultBean();
		// 判断用户是否登录
		assertNotGuest();
		// 推荐语非空判断
		assertNotEmpty(recommend, MSG_E_RECOMMEND_CAN_NOT_EMPTY);
		TutorRecommendEntity tutorRecommendEntity = new TutorRecommendEntity();
		tutorRecommendEntity.setId(IDGenerator.genUID());
		tutorRecommendEntity.setTutorTeacherId(teacherId);
		tutorRecommendEntity.setRecommend(recommend);
		tutorRecommendEntity.setRecommendTeacherId(WebContext
				.getSessionUserId());
		// 插入推荐语
		insert(tutorRecommendEntity);
		result.setStatus(true);
		return result;
	}

	/**
	 * 导师详情——推荐老师列表
	 * 
	 * @author heyaqin
	 * @param userId
	 *            导师ID
	 * @return 返回结果
	 */
	public ResultBean recommendTutorTeacherList(String userId) {
		TutorDefEntity tutorDefEntity = new TutorDefEntity();
		tutorDefEntity.setTeacherId(userId);
		tutorDefEntity.setPageSize(6);
		ResultBean result = searchList4Page(tutorDefEntity,
				SQL_SEARCH_RECOMMEND_TEACHERS);
		return result;
	}

	/**
	 * 导师详情——相关主题列表
	 * 
	 * @author heyaqin
	 * @param userId
	 *            导师ID
	 * @return 返回结果
	 */
	public ResultBean tutorRelatedSubjectList(String userId) {
		TutorDefEntity tutorDefEntity = new TutorDefEntity();
		tutorDefEntity.setTeacherId(userId);
		tutorDefEntity.setSortOrder("asc");
		tutorDefEntity.setPageSize(6);
		ResultBean result = searchList4Page(tutorDefEntity,
				SQL_SEARCH_RECOMMEND_TEACHER_DATA);
		return result;
	}


	/**
	 * 
	 * 老师首页：点评信息
	 * 
	 * @param tutorDefId
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<DiscussView> seachDiscuss(String tutorInstId, Integer pageSize,
			Integer pageNumber) {
		TutorView tutorView = new TutorView();
		tutorView.setTutorInstId(tutorInstId);
		tutorView.setPageSize(2);
		tutorView.setPageNumber(pageNumber);
		ResultBean result = tutorDiscussList(tutorView);
		return (List<DiscussView>) result.getData();
	}


	/**
	 * 预约辅导详情
	 * 
	 * @param tutorDefId
	 *            实例ID
	 * @return
	 */
	public ResultBean tutorAppointmentDetail(String tutorDefId) {
		ResultBean result = new ResultBean();
		assertNotEmpty(tutorDefId, NULL_TUTOR_INST_ID);
		TutorDefEntity tutorDefEntity = new TutorDefEntity();
		tutorDefEntity.setTutorDefId(tutorDefId);
		tutorDefEntity.setUserId(WebContext.getSessionUserId());
		TutorDefEntity entity = searchOneData(tutorDefEntity,
				TUTOR_INST_SEARCH_TUTOR);
		result.setData(entity);
		return result;
	}


	
	/***
	 * 检索辅导学员留言
	 * @param dv
	 * @return
	 */
	public ResultBean remarkList(DiscussView dv){
		if(dv==null){
			return ResultBean.error().setMessages("链接ID为空");
		}
		assertNotEmpty(dv.getLinkId(), "链接ID为空");
		dv.setLinkType(LINK_TYPE_TUTOR_REMARK);
		dv.setSortName("CREATE_DATE");
		dv.setSortOrder("DESC");
		ResultBean result = commonDao.searchList4Page(dv, SQL_SEARCH_TUTOR_REMARK);
		return result;
	}
	
	/**
	 * 提交留言或回复
	 * @param dd
	 * @param linkId
	 * @return
	 */
	public ResultBean submitRemark(DiscussDataEntity dd){
		assertNotGuest();
		if(dd==null){
			return ResultBean.error().setMessages("留言内容为空");
		}
		assertNotEmpty(dd.getLinkId(), "链接ID为空");
		assertNotEmpty(dd.getContent(), "留言内容为空");
		//检索DiscussEntity是否存在
		DiscussEntity entity = new DiscussEntity();
		entity.setLinkId(dd.getLinkId());
		entity.setLinkType(LINK_TYPE_TUTOR_REMARK);
		entity = commonDao.searchOneData(entity, SQL_SEARCH_DISCUSS_BY_PK);
		if(entity==null){
			//插入一条新的
			entity = new DiscussEntity();
			entity.setId(IDGenerator.genUID());
			entity.setLinkId(dd.getLinkId());
			entity.setTitle("给辅导老师"+dd.getLinkId()+"的留言");
			entity.setLinkType(LINK_TYPE_TUTOR_REMARK);
			commonDao.insertData(entity, SQL_SAVE_DISCUSS_TUTOR_REMARK);
		}
		//插入留言数据
		dd.setId(IDGenerator.genUID());
		dd.setDiscussId(entity.getId());
		dd.setUserId(WebContext.getSessionUserId());
		commonDao.insertData(dd, SQL_SAVE_DISCUSS_DATA_TUTOR_REMARK);
		return ResultBean.success();
	}
	/**
	 * 查询众筹辅导列表
	 * @return
	 */
	@SuppressWarnings("unchecked")
    public List<TutorView> searchTutorList(TutorView view){
	    return commonDao.searchList(view, SQL_SEARCH_TUTOR_CROWD);
	}
	/**
	 * 关闭众筹辅导
	 * @param tutor
	 */
	public void closeTutor(TutorView tutor){
	    commonDao.updateData(tutor, SQL_UPDATE_TUTOR_CLOSE);
	}
	/**
	 * 报名的学员列表
	 * @param tutor
	 * @return
	 */
	@SuppressWarnings("unchecked")
    public List<TutorStudentsEntity> searchStudents(TutorView tutor){
	     return commonDao.searchList(tutor.getTutorInstId(), SQL_STUDENTS_BY_TUTORINST_ID);
	}
	public void executeInternal(TutorView tutor){
	    if(tutor.getNumStudentsMin()!=null&&tutor.getNumApply()!=null&&tutor.getNumStudentsMin().compareTo(tutor.getNumApply())==1){
            tutor.setStatus("3");
            closeTutor(tutor);
            List<TutorStudentsEntity> students=searchStudents(tutor);
            for(TutorStudentsEntity ss:students){
                //发送信息
                messageService.sendSystemMessage( ss.getStudentId(), "因辅导["+tutor.getTitle()+"]报名人数未达到，现已关闭此辅导");
                //退积分
                pointService.earnPoint("HS", ss.getStudentId(), "A0010", tutor.getPrice(), "", "");
            }
        }else if(tutor.getNumStudentsMin()!=null&&tutor.getNumApply()!=null&&tutor.getNumStudentsMin().compareTo(tutor.getNumApply())==-1){
            tutor.setStatus("1");
            closeTutor(tutor);
        } 
	    
	}
}
