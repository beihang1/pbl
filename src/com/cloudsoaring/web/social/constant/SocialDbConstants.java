package com.cloudsoaring.web.social.constant;

/**
 * 社交数据库常量定义文件
 * @author yuntengsoft
 *
 */
public interface SocialDbConstants {

	/**已预约社交列表检索 */ 
	public static final String SQL_SEARCH_SOCIAL_FINISH = "SocialInst.select4FinishSocial";
	/**进行中社交列表检索 */ 
	public static final String SQL_SEARCH_SOCIAL_GOING = "SocialInst.select4GoingSocial";
	/**取消预约*/ 
	public static final String SQL_CANCEL_SOCIAL_INST = "SocialInst.update4CancelSocial";
	/**立即支付详情*/
	public static final String SQL_SEARCH_PAY_DETAIL_ORDER ="SocialInst.selectByPk";
	public static final String SQL_SEACH_TEACHER_ID="SocialInst.seachTeacherId";
	/**立即支付*/
	public static final String SQL_PAY_NOW ="UserPoint.update";
	/**支付成功*/
	public static final String SQL_PAY_SUCCESS ="SocialInst.select4TeacherInfo";
	/**支付后更新状态为未确认*/
	public static final String SQL_UPDATE_THEN_IMMEDIATE_PAY ="SocialInst.immediatePay";
	/**预约编辑保存操作*/
	public static final String SQL_UPDATE_SOCIAL_ORDER = "SocialInst.updateSocialOrder";
	/**社交管理一览*/
	public static final String SQL_SEARCH_SOCIAL_MANAGER_LIST = "SocialInst.select";
	/**社交管理-编辑状态-通过*/
	public static final String SQL_UPDATE_SOCIAL_STATUS_SURE = "SocialInst.updateSocialStatusSure";
	/**社交管理-编辑状态-未通过*/
	public static final String SQL_UPDATE_SOCIAL_STATUS_REFUSE = "SocialInst.updateSocialStatusRefuse";
	/**社交管理-编辑状态-见过*/
	public static final String SQL_UPDATE_SOCIAL_STATUS_FINISH = "SocialInst.updateSocialStatusFinish";
	/**社交管理列表-删除*/
	public static final String SQL_DELETE_SOCIAL = "SocialInst.delete";
	/**社交管理列表-编辑约见价格*/
	public static final String SQL_UPDATE_MEET_POINT = "SocialMaster.updateMeetPoints";
	public static final String SQL_SOCIAL_UPDATE_MEET = "SocialMaster.updateMeetNum";
	/**评论后更新状态为已点评*/
	public static final String SQL_UPDATE_4_DISCUSS = "SocialInst.update4Discuss";
	/**社交管理列表-详情*/
	public static final String SQL_SELECT_DETAIL_4_SOCIAL_MANAGER = "SocialInst.selectDetail4SocialManager";
	/**社交-是否存在未结束的社交预约*/
	public static final String SQL_SEARCH_EXITS_SOCIAL_INST = "SocialInst.selectExitsSocialInst";
	
	public static final String SQL_SELECT_DETAIL_4_SOCIAL_MANAGER_STUDENT_INFO = "SocialInst.select4StudentInfo";

	/**社交导师评论列表*/
	public static final String SQL_SELECT_SOCIAL_DISCUSS_LIST = "DiscussData.selectSocialMasterDiscussData";
	/**社交导师课程列表*/
	public static final String SQL_SELECT_SOCIAL_COURSE_LIST = "Course.selectCourseList";
	/**社交导师相关视频列表*/
	public static final String SQL_SELECT_ORTHER_COURSE_LIST = "Course.selectOrtherCourseList";
	/**社交导师列表-一览*/
	public static final String SQL_SOCIAL_MASTER_LIST = "SocialMaster.selectSocialTeacherList";
	/**社交问答列表*/
	public static final String SQL_SOCIAL_QUESTION_ANSWER_LIST = "QaQuestion.selectQuestionAnswerList";
	/**更新背景图片*/
	public static final String SQL_UPDATE_IMAGE= "SocialMaster.updateImage";
	public static final String SQL_SEACH_ONE= "SocialMaster.selectByPk";
	// ////////////辅导///////////////////////
	/***/
	public static final String DISCUSS_DATA_SEARCH_DISCUSS_DATA_FOR_TUTOR = "DiscussData.searchDiscussDataForTutor";
	
	/** 判断是否为教师 */
	public static final String SQL_IS_TEACHHER = "NoteData.isTeacher";
	
	/** 用户是否点赞 */
	public static final String OPERATE_RECORD_SEARCH_COUNT_FOR_TUTOR = "OperateRecord.seachCountForTutor";
	
	/** 辅导列表_查询_定义 (TutorDefEntity) */
	public static final String TUTOR_DEF_SELECT_TUTOR_DEF = "TutorDef.select";
	/** 辅导列表_查询_定义 (TutorView) */
	public static final String TUTOR_DEF_SELECT_TUTOR_VIEW_FOR_DETAIL = "TutorDef.selectTutorViewForDetail";
	
	/** 辅导列表_新增_定义 (TutorView) */
	public static final String TUTOR_DEF_INSERT_TUTOR_DEF = "TutorDef.insert";
	
	/** 辅导列表_更新_定义 (TutorView) */
	public static final String TUTOR_DEF_UPDATE_TUTOR_DEF = "TutorDef.updateTutorDef";
	
	/** 辅导列表_删除_定义 (TutorDefEntity) */
	public static final String TUTOR_DEF_DELETE_TUTOR_DEF = "TutorDef.delete";
	
	/** 辅导列表_删除_定义 (TutorInstEntity) */
	public static final String TUTOR_DEF_SELECT_FOR_MEET = "TutorDef.selectForMeet";
	
	/** 辅导列表_发布_定义 (TutorDefEntity) */
	public static final String TUTOR_DEF_UPDATE_FOR_ISSUE = "TutorDef.updateForIssue";
	public static final String SQL_SEARCH_TEACHER_ID="TutorDef.searchTeacherId";
	/** 辅导列表_见过_定义 (TutorDefEntity) */
	public static final String TUTOR_DEF_UPDATE_FOR_MEET = "TutorDef.updateForMeet";
	
	/** 辅导列表_老师首页 (TutorView) */
	public static final String TUTOR_INST_SEARCH_TUTOR_VIEW = "TutorInst.selectTutorView";
	
	/** 我的辅导_列表 (TutorView) */
	public static final String TUTOR_INST_SEARCH_TUTOR_VIEW_FOR_STU = "TutorInst.selectTutorViewForStu";
	/** 预约辅导——详情 */
	public static final String TUTOR_INST_SEARCH_TUTOR = "TutorDef.selectTutorDetail";
	
	/** 我的辅导_列表_进行中 (TutorView) */
	public static final String TUTOR_INST_SEARCH_TUTOR_VIEW_FOR_STU_AUDITING = "TutorInst.selectTutorViewForStuAuditing";
	
	/** (TutorInstEntity) */
	public static final String TUTOR_INST_SEARCH_TUTOR_INST = "TutorInst.select";
	
	/**  */
	public static final String SQL_SEACH_TUTOR_BY_ID = "TutorInst.seachById";
	
	/** 辅导列表_新增_实例 (TutorView) */
	public static final String TUTOR_INST_INSERT_TUTOR_INST = "TutorInst.insert";
	
	/** 辅导列表_更新_实例 (TutorView)*/
	public static final String TUTOR_INST_UPDATE_TUTOR_INST = "TutorInst.updateTutorInst";
	
	
	/** 辅导列表_删除_实例 (TutorInstEntity) */
	public static final String TUTOR_INST_DELETE_TUTOR_INST = "TutorInst.deleteTutorInst";
	
	/** 辅导列表_取消_实例 (TutorInstEntity) */
	public static final String TUTOR_INST_UPDATE_TUTOR_INST_FOR_CANCEL = "TutorInst.updateTutorInstForCancel";
	
	/** 辅导列表_见过_实例 (TutorInstEntity) */
	public static final String TUTOR_INST_UPDATE_FOR_MEET = "TutorInst.updateForMeet";
	
	/** 约见管理-约见审核-列表 (TutorView) */
	public static final String TUTOR_INST_SELECT_TUTOR_TEACHER_HANDLE = "TutorInst.selectTutor4TeacherHandle";
	public static final String TUTOR_INST_SELECT_TUTOR_VIEW_FOR_MEET_APPROVAL = "TutorInst.selectTutorViewForMeetApproval";
	
	/** 约见管理-约见待审核状态 */
	public static final String TUTOR_INST_UPDATE_TUTOR_INST_FOR_MEET_APPROVAL = "TutorInst.updateTutorInstForMeetApproval";
	
	/** 我的辅导-立即支付详情 (TutorView) */
	public static final String TUTOR_INST_SELECT_TUTOR_VIEW_FOR_PAY = "TutorInst.selectTutorViewForPay";
	
	/** 辅导列表_见过_学员 (TutorStudentsEntity) */
	public static final String TUTOR_STUDENTS_UPDATE_FOR_MEET = "TutorStudents.updateForMeet";
	
	/** 我的辅导-取消预约 (TutorStudentsEntity) */
	public static final String TUTOR_STUDENTS_UPDATE_FOR_CANCEL_RESERVE = "TutorStudents.updateForCancelReserve";
	public static final String SQL_SEARCH_STATUS_STUDENT= "TutorStudents.selectStatus";
	/** 我的辅导-编辑预约 (TutorStudentsEntity) */
	public static final String TUTOR_STUDENTS_UPDATE_FOR_EDIT_RESERVE = "TutorStudents.updateForEditReserve";

	/** 辅导列表_见过_学员 (TutorStudentsEntity) */
	public static final String TUTOR_STUDENTS_SEARCH_TUTOR_STUDENTS_BY_TUTOR_DEF_ID = "TutorStudents.selectTutorStudentsByTutorDefId";
	
	/** 辅导列表_见过_学员 (TutorStudentsEntity) */
	public static final String TUTOR_STUDENTS_SEARCH_TUTOR_STUDENTS = "TutorStudents.selectTutorStudents";
	
	/** 社交列表_已完成_学员 (SocialInstEntity) */
	public static final String SOCIAL_INST_SEARCH_SOCIAL_CUSTOMERS = "SocialInst.selectSocialCustomers";
	
	/** 约见管理-约见待审核-通过、不通过 (TutorStudentsEntity) */
	public static final String TUTOR_STUDENTS_UPDATE_TUTOR_STUDENTS_FOR_MEET_APPROVAL = "TutorStudents.updateTutorStudentsForMeetApproval";
	
	/** 我的辅导_支付详情 (TutorStudentsEntity) */
	public static final String TUTOR_STUDENTS_SEARCH_TUTOR_STUDENTS_TOTAL_POINTS = "TutorStudents.selectTutorStudentsTotalPoints";
	
	/** 我的辅导_支付成功 (TutorStudentsEntity) */
	public static final String TUTOR_STUDENTS_UPDATE_FOR_PAY_SUCCESS = "TutorStudents.updateForPaySuccess";
	
	/** 我的辅导_创客点评 (TutorStudentsEntity) */
	public static final String TUTOR_STUDENTS_UPDATE_FOR_DISCUSS = "TutorStudents.updateForDiscuss";
	
	
	/** 导师列表 */
	public static final String SQL_SEARCH_TUTOR_TEACHER_LIST = "TutorDef.selectTutorTeacherList";
	/** 导师详情——导师个人信息 */
	public static final String SQL_SEARCH_TUTOR_TEACHER_DETAIL = "SocialMaster.selectTutorDetail";
	/** 导师详情——社交导师个人信息 */
	public static final String SQL_SEARCH_SOCIAL_TEACHER_DETAIL = "SocialMaster.selectSocialTeacherDetail";
	/** 导师详情——导师所有点评信息 */
	public static final String SQL_SEARCH_TUTOR_TEACHER_DISCUSS_DATA = "DiscussData.selectTutorTeacherDiscussData";
	/** 导师详情——导师所有推荐语列表信息 */
	public static final String SQL_SEARCH_TUTOR_TEACHER_RECOMMEND_DATA = "TutorRecommend.selectTutorRecommendData";
	/** 导师详情——推荐辅导信息 */
	public static final String SQL_SEARCH_RECOMMEND_TEACHER_DATA = "TutorDef.selectRecommendTeacherList";
	/** 导师详情——推荐老师信息 */
	public static final String SQL_SEARCH_RECOMMEND_TEACHERS = "TutorDef.selectRecommendTeachers";
	/** 加入心愿单——更新社交教师表--社交老师的总想见人数 */
	public static final String SQL_UPDATE_SOCIAL_MASTER_WISH_NUMBER = "SocialMaster.updateTotalNumWish";
	/** 预约辅导--众筹听课类型下，检索辅导定义信息数据 */
	public static final String SQL_SEARCH_DATA_BY_TUTOR_DEF_NO = "TutorDef.selectByTutorDefNo";
	/** 预约辅导--众筹听课类型下，更新定义表总报名人数 */
	public static final String SQL_UPDATE_TOTAL_NUM_APPLY_BY_PK = "TutorDef.updateTotalNumApply";
	/** 预约辅导--众筹听课类型下，更新实例表的报名人数 */
	public static final String SQL_UPDATE_INST_DATA = "TutorInst.updateNumApply";
	/**众筹辅导列表*/
	public static final String SQL_SEARCH_TUTOR_CROWD= "TutorInst.searchTutorCrowd";
	/**关闭辅导*/
	public static final String SQL_UPDATE_TUTOR_CLOSE= "TutorInst.updateTutorClose";
	/** 预约辅导--是否预约过 */
	public static final String SQL_SEARCH_EXITS_APPOINTMENT = "TutorStudents.searchExitsAppointment";
	/**是否预约中*/
	public static final String SQL_SEARCH_JOIN_APPOINTMENT = "TutorStudents.searchJoinAppointment";
	/**根据实例ID查询学员信息*/
	public static final String SQL_STUDENTS_BY_TUTORINST_ID= "TutorStudents.searchStudentsByTutorInstId";
	/**当前学生正在预约中的所有辅导*/
	public static final String SQL_SEARCH_APPONTMENT_DEF = "TutorDef.searchJoinAppointmentDef";
	
	/** 预约社交--检索社交主人ID的数据 --总想见人数+1*/
	public static final String SQL_SEARCH_DATA_BY_MASTER_ID = "SocialMaster.searchDataByMaterId";
	/** 预约社交--更新社交教师表的数据*/
	public static final String SQL_UPDATE_SOCIAL_MASTER_BY_PK = "SocialMaster.updateTotalNumWish";
	/** 心愿单列表*/
	public static final String SQL_SEARCH_HEART_ORDER_LIST="UserSocial.searchHeartOrderList";
	
	/**检索辅导下已支付的用户数**/
	public static final String SQL_TUTOR_STUDENTS_JOINED_NUM = "TutorStudents.searchJoinedNum";
	
	/**检索辅导留言列表*/
	public static final String SQL_SEARCH_TUTOR_REMARK = "DiscussData.searchTutorRemarkList";
	/**根据ID检索DiscussEntity*/
	public static final String SQL_SEARCH_DISCUSS_BY_PK = "Discuss.select";
	/**保存给辅导老师的留言DiscussEntity*/
	public static final String SQL_SAVE_DISCUSS_TUTOR_REMARK = "Discuss.saveRemarkDiscuss";
	/**保存留言数据*/
	public static final String SQL_SAVE_DISCUSS_DATA_TUTOR_REMARK = "DiscussData.insert";
	
	public static final String SQL_IS_EXIST_MASTER = "SocialMaster.isExist";
	
	public static final String SQL_UPDATE_NUM_FAVORABLE= "SocialMaster.updateNumFavorable";
}
