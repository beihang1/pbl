package com.cloudsoaring.web.social.constant;

/**
 * 社交消息定义文件
 * @author yuntengsoft
 *
 */
public interface SocialMsgNames {

	/** 用户尚未登录 */
	public static final String NULL_USER_ID = "msg.e.userIdNull";
	/**保存成功*/
    public static final String CENTER_SAVE_SUCCESS="center.save_success";

	/** 预约编号为空*/
	public static final String NULL_INST_ID = "msg.e.instIdNull";

	/** 辅导报名截止日期应该在当前日期之后 */
	public static final String MSG_E_TUTOR_APPLY_END_DATE_AFTER_NOW = "msg.e.tutorApplyEndDateAfterNow";

	/** 辅导开始日期应该在报名截止之后 */
	public static final String TUTOR_START_TIME_AFTER_APPLY_END_DATE = "msg.e.tutorStartTimeAfterApplyEndDate";

	/** 辅导类型为空 */
	public static final String MSG_E_EMPTY_TUTOR_TYPE = "msg.e.tutorTypeNull";
	
	/** 辅导课程处于不可编辑状态 */
	public static final String MSG_E_TUTOR_STATUS_FORBID_UPDATE = "msg.e.tutorStatusForbidUpdate";
	
	/** 辅导课程处于不可删除状态 */
	public static final String TUTOR_STATUS_FORBID_DELETE = "msg.e.tutorStatusForbidDelete";
	
	/** 学员不可删除辅导评论 */
	public static final String TUTOR_DISCUSS_FORBID_DELETE_FOR_STUDENT = "msg.e.tutorDiscussForbidDeleteForStudent";
	
	/** 推荐语不能为空 */
	public static final String MSG_E_RECOMMEND_CAN_NOT_EMPTY = "msg.e.recommendCanNotEmpty";
	/** 老师ID不可为空 */
	public static final String MSG_E_TEACHER_ID_CAN_NOT_EMPTY = "msg.e.teacherIdCanNotEmpty";
	/** 问题内容不可为空 */
	public static final String MSG_E_QUESTION_CONTENT_CAN_NOT_EMPTY = "msg.e.questionContentCanNotEmpty";
	/** 自我介绍不可为空 */
	public static final String MSG_E_INTRODUCTION_CAN_NOT_EMPTY = "msg.e.introductionCanNotEmpty";
	/** 问题标题不可为空 */
	public static final String MSG_E_QUESTION_TITLE_CAN_NOT_EMPTY = "msg.e.questionTitleCanNotEmpty";
	/** 回答ID不可为空 */
	public static final String MSG_E_QUESTION_ID_CAN_NOT_EMPTY = "msg.e.questionIdCanNotEmpty";
	/** 回答内容不可为空 */
	public static final String MSG_E_ANSWER_CONTENT_CAN_NOT_EMPTY = "msg.e.answerContentCanNotEmpty";
	/** 不可以将自己加入心愿单*/
	public static final String MSG_E_CAN_NOT_JOIN_HEART_YOURSELF = "msg.e.canNotJoinHeartYourself";
	/** 已经预约过辅导*/
	public static final String MSG_E_ALREADY_APPOINTMENT = "msg.e.alreadyAppointment";
	/** 存在未完成的辅导预约*/
	public static final String MSG_E_HAS_NO_END_APPOINTMENT = "msg.e.hasNoEndAppointment";
	/** 存在未完成的社交预约*/
	public static final String MSG_E_HAS_NO_END_SOCIAL_APPOINTMENT = "msg.e.hasNoEndSocialAppointment";
	/** 老师不可预约自己*/
	public static final String MSG_E_CAN_NOT_APPOINTMENT_YOURSELF = "msg.e.cannotAppointmentYourself";
	
	/** 辅导课程处于不可取消状态 */
	public static final String MSG_E_TUTOR_STATUS_FORBID_CANCEL = "msg.e.tutorStatusForbidCancel";
	/**确认要删除社交信息吗？*/
	public static final String CENTER_SURE_DELETE_SOCIAL = "center.e.deleteSocial";

	/** 辅导实例为空*/
	public static final String NULL_TUTOR_INST_ID = "msg.e.tutorInstIdNull";
	/** 社交实例为空*/
	public static final String MSG_NULL_SOCIAL_INST_ID = "msg.e.socialInstIdNull";
	/** 社交评论内容*/
	public static final String MSG_SOCIAL_DISCUSS_NULL = "msg.e.socialDiscussNull";
	/** 积分或零钱不够支付*/
	public static final String MSG_POINTS_NOT_ENOUGH = "msg.e.pointsNotEnough";
	/** 积分或零件不够支付*/
	public static final String POINTS_INSUFFICIENT_FOR_TUTOR = "msg.e.pointsInsufficientForTutor";
	/** 社交状态更新失败*/
	public static final String MSG_SOCIAL_STATUS_UPDATE_FALSE = "msg.e.socialStatusUpdateFalse";
	/** 辅导评论内容不能为空*/
	public static final String TUTOR_DISCUSS_NULL = "msg.e.tutorDiscussNull";
	
	/**全部课程*/
	public static final String TXT_ALL_COURSE = "txt.all.course";
	/**筛选*/
	public static final String TXT_FILTER = "txt.filter";
	/**分*/
	public static final String TXT_MINITER = "txt.miniter";
	/**元/次*/
	public static final String TXT_PRICE_MONEY = "txt.price.money";
	/**积分/次*/
	public static final String TXT_PRICE_POINT = "txt.price.point";
	/**5人见过*/
	public static final String TXT_NUM_SEE = "txt.num.see";
	/**10人/总数*/
	public static final String TXT_NUM_TOTAL = "txt.num.total";
	/**10人/最少*/
	public static final String TXT_NUM_MIN = "txt.num.min";
	/**20人/已报名*/
	public static final String TXT_NUM_SIGN = "txt.num.sign";
	/**全部*/
	public static final String TXT_ALL = "txt.all";
	/**一对一*/
	public static final String TXT_ONE_TO_ONE = "txt.one.to.one";
	/**众筹听课*/
	public static final String TXT_ONE_TO_N = "txt.one.to.n";
	/**系统繁忙，请稍后再试*/
	public static final String MSG_E_REQUEST_ERROR = "msg.e.request.error";
	/**半天内回应*/
	public static final String TXT_RESPONSE_TIME = "txt.response.time";
	/**10人想见*/
	public static final String TXT_WISH_METTING = "txt.wish.metting";
	/**加入心愿单*/
	public static final String TXT_JOIN_WISH_ORDER = "txt.join.wish.order";
	/**只需付几百元，就可以与老师一对一线下面谈*/
	public static final String TXT_TUTOR_INFORMATION_ONE = "txt.tutor.information.one";
	/**为您答疑解惑、出谋划策。不满意还能“无忧退款”。*/
	public static final String TXT_TUTOR_INFORMATION_TWO = "txt.tutor.information.two";
	/**教学平台*/
	public static final String TXT_SYS_LOGO_TEXT = "txt.sys.logo.text";
	/**约3小时*/
	public static final String TXT_TUTOR_HOUR = "txt.tutor.hour";
	/**10人聊过*/
	public static final String TXT_TUTOR_TALK = "txt.tutor.talk";
	/**辅导预约*/
	public static final String TXT_TUROR_APPOINTMENT = "txt.tutor.appointment";
	/**关于老师*/
	public static final String TXT_ABOUT_TEACHER = "txt.about.teacher";
	/**学员点评*/
	public static final String TXT_STUDENT_DISCUSS = "txt.student.discuss";
	/**老师推荐*/
	public static final String TXT_TEACHER_COMMOND = "txt.teacher.commond";
	/**参与话题：*/
	public static final String TXT_ABOUT_TOPIC = "txt.about.topic";
	/**请输入对该老师的推荐语*/
	public static final String TXT_INPUT_TEACHER_COMMOND = "txt.input.teacher.commond";
	/**提交*/
	public static final String TXT_BTN_SUBMIT = "txt.btn.submit";
	/**推荐老师*/
	public static final String TXT_COMMOND_TEACHER = "txt.commond.teacher";
	/**相关主题*/
	public static final String TXT_ABOUT_THEME = "txt.about.theme";
	/**最多输入500个字符*/
	public static final String MSG_STRING_LENGTH_MAX = "msg.string.length.max";
	/**推荐语不能为空！*/
	public static final String MSG_COMMOND_CAN_NOT_EMPTY = "msg.commond.can.not.empty";
	/**提交成功！*/
	public static final String MSG_BTN_SUBMIT_SUCCESS = "msg.btn.submit.success";
	/**成功加入心愿单！*/
	public static final String MSG_JOIN_WISH_ORDER_SUCCESS = "msg.join.wish.order.success";
	/**暂无课程*/
	public static final String TXT_EMPTY_DATA = "txt.empty.data";
	/**20人想预约*/
	public static final String TXT_WISH_PRE_METTING = "txt.wish.pre.metting";
	/**立即预约*/
	public static final String TXT_METTING_NOW = "txt.metting.now";
	/**连载中*/
	public static final String TXT_PUBLISH_ING = "txt.publish.ing";
	/**已完成*/
	public static final String TXT_PUBLISH_FINISH = "txt.publish.finish";
	/**已结束*/
	public static final String TXT_PUBLISH_END = "txt.publish.end";
	/**10人学习*/
	public static final String TXT_STUDY_NUM = "txt.study.num";
	/**问答*/
	public static final String TXT_QUESTION_AND_ANSWER = "txt.question.and.answer";
	/**提问*/
	public static final String TXT_QUESITON = "txt.question";
	/**回答*/
	public static final String TXT_ANSWER = "txt.answer";
	/**请输入想要咨询老师的问题*/
	public static final String TXT_INPUT_QUESTION_TO_ASK_TEACHER = "txt.input.question.to.ask.teacher";
	/**回答XXX的问题*/
	public static final String TXT_INPUT_ANSWER_SOMEONE_QUESTION = "txt.input.answer.someone.question";
	/**回复*/
	public static final String TXT_REPLY = "txt.reply";
	/**专家答案*/
	public static final String TXT_ANSWER_BY_DOCTER = "txt.answer.by.docter";
	/**展开*/
	public static final String TXT_OPEN_DOWN = "txt.open.down";
	/**收起*/
	public static final String TXT_HIDE_UP = "txt.hide.up";
	/**评论*/
	public static final String TXT_DISCUSS = "txt.discuss";
	/**暂无评论*/
	public static final String TXT_EMPTY_DISCUSS_DATA = "txt.empty.discuss.data";
	/**年*/
	public static final String TXT_YEAR = "txt.year";
	/**月*/
	public static final String TXT_MONTH = "txt.month";
	/**日*/
	public static final String TXT_DAY = "txt.day";
	/**XX的其他视频*/
	public static final String TXT_OTHER_VIDEO = "txt.other.video";
	/**暂无相关视频*/
	public static final String TXT_EMPTY_OTHER_VIDEO_DATA = "txt.empty.other.video.data";
	/**内容不能为空*/
	public static final String MSG_CONTENT_CAN_NOT_EMPTY = "msg.content.can.not.empty";
	/**支付成功*/
	public static final String MSG_PAY_SUCCESS = "msg.pay.success";
}
