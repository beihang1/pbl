package com.cloudsoaring.web.social.constant;

public interface SocialDefaultValues {
	
	
	/**连接类型-心愿单*/
	public static final String WISH_SOCIAL_TYPE ="2";
	
	/**讨论类型-辅导留言*/
	public static final String LINK_TYPE_TUTOR_REMARK = "TUTOR_REMARK";
}
