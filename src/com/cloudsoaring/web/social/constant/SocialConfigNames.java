package com.cloudsoaring.web.social.constant;

public interface SocialConfigNames {
	
	/**辅导列表-一对一*/
	public static final String TUTOR_TEACHER_1V1 ="一对一";
	/**辅导列表-众筹听课*/
	public static final String TUTOR_TEACHER_1VN = "众筹听课";
	
	/**辅导列表，默认5条*/
	public static final Integer DEFAULT_SIZE = 5;
	/**社交列表，默认15条*/
	public static final Integer DEFAULT_SIZE_15 = 15;

	/**社交老师的默认背景图片**/
	public static final String CONFIG_SOCIAL_BACKGROUND_IMAGE = "CONFIG_SOCIAL_BACKGROUND_IMAGE";

	/**数据库是否显示辅导约见*/
	public static final String SHOW_TUTOR_METTING = "SHOW_TUTOR_METTING";
}
