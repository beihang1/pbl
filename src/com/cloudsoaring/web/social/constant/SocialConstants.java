package com.cloudsoaring.web.social.constant;

import com.cloudsoaring.web.common.dao.ibatis.plugin.MybatisConstant;

/**
 * 社交常量定义文件
 * @author yuntengsoft
 *
 */
@MybatisConstant
public interface SocialConstants extends SocialConfigNames, SocialDbConstants,
		SocialLogConstants, SocialDefaultValues, SocialMsgNames, SocialStateConstants {
	
	/**辅导类型之一对一*/
	public static String TUTOR_TYPE_1V1 = "1V1";
	
	/**辅导类型之一对多*/
	public static String TUTOR_TYPE_1VN = "1VN";
	
	/**链接类型_课程*/
	public static String LINK_TYPE_COURSE = "COURSE";
	/**链接类型_计划*/
	public static String LINK_TYPE_PLAN = "PLAN";
	/**链接类型_老师*/
	public static String LINK_TYPE_TEACHER = "TEACHER";
	/**链接类型_辅导*/
	public static String LINK_TYPE_TOTUR = "TUTOR";
	
	/** 笔记点赞 */
    public static final String OPERATE_LIKE = "1";
	/** 笔记采集 */
    public static final String OPERATE_COLLECT = "2";
    /** 评论点赞 */
    public static final String OPERATE_LIKE_DISCUSS = "3";
    /**连接类型-已约见*/
	public static final String FINISH_SOCIAL_TYPE = "0";
	/**连接类型-进行中*/
	public static final String GOING_SOCIAL_TYPE ="1";
    /** 辅导点赞 */
    public static final String OPERATE_LIKE_TUTOR = "4";
	
	/**积分消费类型_参加辅导消费*/
	public static String POINT_TYPE_JOIN_TUTOR_EXPEND = "D0006";
	/**积分消费类型_参加社交消费*/
	public static String POINT_TYPE_JOIN_SOCIAL_EXPEND = "D0007";
	
	/**积分消费类型_提供辅导收入*/
	public static String POINT_TYPE_SUPPLY_TUTOR_INCOME = "A0006";
	/**链接类型_社交*/
	public static String LINK_TYPE_SOCIAL = "SOCIAL";
}
