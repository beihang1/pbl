package com.cloudsoaring.web.social.constant;

import com.cloudsoaring.web.common.dao.ibatis.plugin.MybatisConstant;

/**
 * 社交状态常量定义文件
 * @author yuntengsoft
 *
 */
@MybatisConstant
public interface SocialStateConstants {
   
    /**辅导 类型*/
    public static final String TUTOR_TYPE = "tutor_type";
    /**社交状态/进度*/
    public static final String SOCISL_STATUS = "social_status";
	/** 辅导定义状态_issue：发布 */
	public static final String TUTOR_DEF_STATUS_TYPE_ISSUE = "issue";
	/** 辅导定义状态_cancel：取消发布 */
	public static final String TUTOR_DEF_STATUS_TYPE_CANCEL = "cancel";
	/** 辅导定义状态_0：未发布 */
	public static final String TUTOR_DEF_STATUS_UNRELEASED = "0";
	/** 辅导定义状态_1：已发布 */
	public static final String TUTOR_DEF_STATUS_RELEASED = "1";

	/** 辅导实例状态_0：众筹中 */
	public static final String TUTOR_INST_STATUS_CROWDFUNDING = "0";
	/** 辅导实例状态_1：辅导中 */
	public static final String TUTOR_INST_STATUS_TUTORING = "1";
	/** 辅导实例状态_2：已解决 */
	public static final String TUTOR_INST_STATUS_SOLVED = "2";
	/** 辅导实例状态_3：关闭/取消 */
	public static final String TUTOR_INST_STATUS_CLOSED = "3";
	/** 辅导实例状态_4：已拒绝 */
	public static final String TUTOR_INST_STATUS_REFUSED = "4";

	/** 辅导学员状态_0：待支付 */
	public static final String TUTOR_STUDENTS_STATUS_UNPAID = "0";
	/** 辅导学员状态_1：待确认 */
	public static final String TUTOR_STUDENTS_STATUS_UNCONFIRM = "1";
	/** 辅导学员状态_2：已确认 */
	public static final String TUTOR_STUDENTS_STATUS_CONFIRM = "2";
	/** 辅导学员状态_3：已完成 */
	public static final String TUTOR_STUDENTS_STATUS_COMPLETE = "3";
	/** 辅导学员状态_4：已点评 */
	public static final String TUTOR_STUDENTS_STATUS_COMMENT = "4";
	/** 辅导学员状态_5：已取消 */
	public static final String TUTOR_STUDENTS_STATUS_CANCEL = "5";
	/** 辅导学员状态_6：已拒绝 */
	public static final String TUTOR_STUDENTS_STATUS_REFUSE = "6";
	
	/** 用户心愿表状态_0：未收藏 */
	public static final String USER_SOCIAL_NO_FAVORITE = "0";
	/** 用户心愿表状态_1：已收藏 */
	public static final String USER_SOCIAL_FAVORITE = "1";
	
	/** 预约辅导状态_0：未预约 */
	public static final String JOIN_NO_APPOINTMENT = "0";
	/** 预约辅导状态_1：预约中*/
	public static final String JOIN_APPOINTMENT = "1";
	
	/** 社交实例状态_0:待支付 */
	public static final String SOCIAL_STATUS_UNPAY = "0";
	/** 社交实例状态_1:待确认 */
	public static final String SOCIAL_STATUS_UNSURE = "1";
	/** 社交实例状态_2:已确认 */
	public static final String SOCIAL_STATUS_SURE = "2";
	/** 社交实例状态_3:已完成 */
	public static final String SOCIAL_STATUS_FINISH = "3";
	/** 社交实例状态_4:已点评 */
	public static final String SOCIAL_STATUS_REMARK = "4";
	/** 社交实例状态_5:已取消 */
	public static final String SOCIAL_STATUS_CANCEL = "5";
	/** 社交实例状态_6:已拒绝 */
	public static final String SOCIAL_STATUS_REFUSE = "6";
	/** 社交导师状态_0：关 */
	public static final String SOCIAL_MASTER_STATUS_CLOSE = "0";
	/** 社交导师状态_1：开 */
	public static final String SOCIAL_MASTER_STATUS_OPEN = "1";
	/**辅导分类：一对一*/
	public static final String TUTOR_TYPE_1V1="1V1";
	/**辅导分类：众筹*/
	public static final String TUTOR_TYPE_1VN="1VN";
	
	/**系统*/
	public static final String SYSTEM_USER_ID="system";
	/**是否可预约*/
    public static final String SOCIAL_FLAG="social_flag";
}
