/**
 * 
 */
package com.cloudsoaring.web.social.view;

import java.util.List;

import com.cloudsoaring.web.social.entity.TutorDefEntity;
import com.cloudsoaring.web.social.entity.TutorStudentsEntity;

/**
 * 辅导定义VO
 * 
 * @Title: Entity
 * @Description: tb_tutor_def
 * @date 2016-03-23
 * @version V1.0
 *
 */
public class TutorView extends TutorDefEntity {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/** 辅导实例ID */
	private String tutorInstId;
	/** 辅导ID */
	private String  tutorDefId;
	/** 辅导开课时间 */
	private java.util.Date startTime;
	/**  */
	private String startInsTime;
	/**  */
	private String startTutorTime;
	/** 开课地点 */
	private String address;
	/** 辅导结束时间 */
	private java.util.Date endTime;
	/**  */
	private String endTutorTime;
	/** 开课人数 */
	private java.math.BigDecimal numStudents;
	/** 报名人数 */
	private java.math.BigDecimal numApply;
	/** 见过人数 */
	private java.math.BigDecimal numMeet;
	/** 备注 */
	private String remark;
	/** 报名截至时间 */
	private java.util.Date applyStartDate;
	/** 报名开始时间 */
	private java.util.Date applyEndDate;
	/** 实例状态（0：众筹中，1：辅导中，2：已解决，3：关闭/取消) */
	private String instStatus;
	/** 取消原因 */
	private String cancelNote;
	/** 学员ID */
	private String studentId;
	/** 学员信息 */
	private List<TutorStudentsEntity> tutorStudentList;
	/**一级标签名称*/
	private String firstTagName;
	/**二级标签名称*/
	private String secondTagName;
	
	/** 姓名 */
	private String personName;
	/** 邮箱 */
	private String email;
	/** 手机 */
	private String mobile;
	/** 电话 */
	private String phone;
	/** 用户头像 */
	private String image;
	/** 回复内容 */
	private String replyContent;
	/** 老师姓名 */
	private String teacherPersonName;
	/** 老师邮箱 */
	private String teacherEmail;
	/** 老师手机 */
	private String teacherMobile;
	/** 老师电话 */
	private String teacherPhone;
	/** 老师用户头像 */
	private String teacherImage;
	/** 老师工作场所 */
	private String teacherWorkplace;
	/** 老师职位 */
	private String teacherPosition;
	/** 剩余积分 (也用于显示支付积分)*/
	private java.math.BigDecimal remainPoints;
	/** 积分总额 */
	private java.math.BigDecimal totalPoints;
	/** 辅导学员状态 */
	private String studentStatus;
	/** 辅导学员问题 */
	private String question;
	/** 辅导学员自我介绍 */
	private String introduction;
	
	/** 要回复的评论ID */
	private String discussTargetDiscussId;
	
	/** 要回复的用户ID */
	private String discussTargetUserId;
	/** 评分 */
	private String  score;
	
	
	
	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getTutorDefId() {
		return tutorDefId;
	}

	public void setTutorDefId(String tutorDefId) {
		this.tutorDefId = tutorDefId;
	}

	/** 是否显示见过按钮标识 */
	private boolean showMeetBtn = false;
	
    public String getFirstTagName() {
        return firstTagName;
    }

    public void setFirstTagName(String firstTagName) {
        this.firstTagName = firstTagName;
    }

    public String getSecondTagName() {
        return secondTagName;
    }

    public void setSecondTagName(String secondTagName) {
        this.secondTagName = secondTagName;
    }

    /** 辅导实例ID */
	public String getTutorInstId() {
		return this.tutorInstId;
	}

	/** 辅导实例ID */
	public void setTutorInstId(String tutorInstId) {
		this.tutorInstId = tutorInstId;
	}

	/** 辅导开课时间 */
	public java.util.Date getStartTime() {
		return this.startTime;
	}

	/** 辅导开课时间 */
	public void setStartTime(java.util.Date startTime) {
		this.startTime = startTime;
	}

	/** 开课地点 */
	public String getAddress() {
		return this.address;
	}

	/** 开课地点 */
	public void setAddress(String address) {
		this.address = address;
	}

	/** 辅导结束时间 */
	public java.util.Date getEndTime() {
		return this.endTime;
	}

	/** 辅导结束时间 */
	public void setEndTime(java.util.Date endTime) {
		this.endTime = endTime;
	}

	/** 开课人数 */
	public java.math.BigDecimal getNumStudents() {
		return this.numStudents;
	}

	/** 开课人数 */
	public void setNumStudents(java.math.BigDecimal numStudents) {
		this.numStudents = numStudents;
	}

	/** 报名人数 */
	public java.math.BigDecimal getNumApply() {
		return this.numApply;
	}

	/** 报名人数 */
	public void setNumApply(java.math.BigDecimal numApply) {
		this.numApply = numApply;
	}

	/** 见过人数 */
	public java.math.BigDecimal getNumMeet() {
		return this.numMeet;
	}

	/** 见过人数 */
	public void setNumMeet(java.math.BigDecimal numMeet) {
		this.numMeet = numMeet;
	}

	/** 备注 */
	public String getRemark() {
		return this.remark;
	}

	/** 备注 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/** 报名截至时间 */
	public java.util.Date getApplyStartDate() {
		return this.applyStartDate;
	}

	/** 报名截至时间 */
	public void setApplyStartDate(java.util.Date applyStartDate) {
		this.applyStartDate = applyStartDate;
	}

	/** 报名开始时间 */
	public java.util.Date getApplyEndDate() {
		return this.applyEndDate;
	}

	/** 报名开始时间 */
	public void setApplyEndDate(java.util.Date applyEndDate) {
		this.applyEndDate = applyEndDate;
	}

	/** 实例状态（0：众筹中，1：辅导中，2：已解决，3：关闭/取消) */
	public String getInstStatus() {
		return this.instStatus;
	}

	/** 实例状态（0：众筹中，1：辅导中，2：已解决，3：关闭/取消) */
	public void setInstStatus(String instStatus) {
		this.instStatus = instStatus;
	}

	/** 取消原因 */
	public String getCancelNote() {
		return this.cancelNote;
	}

	/** 取消原因 */
	public void setCancelNote(String cancelNote) {
		this.cancelNote = cancelNote;
	}

	/**
	 * @return the tutorStudentList
	 */
	public List<TutorStudentsEntity> getTutorStudentList() {
		return tutorStudentList;
	}

	/**
	 * @param tutorStudentList the tutorStudentList to set
	 */
	public void setTutorStudentList(List<TutorStudentsEntity> tutorStudentList) {
		this.tutorStudentList = tutorStudentList;
	}

	/**
	 * @return the studentId
	 */
	public String getStudentId() {
		return studentId;
	}

	/**
	 * @param studentId the studentId to set
	 */
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	/**
	 * @return the personName
	 */
	public String getPersonName() {
		return personName;
	}

	/**
	 * @param personName the personName to set
	 */
	public void setPersonName(String personName) {
		this.personName = personName;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/** 回复内容 */
	public String getReplyContent() {
		return this.replyContent;
	}

	/** 回复内容 */
	public void setReplyContent(String replyContent) {
		this.replyContent = replyContent;
	}

	/**
	 * @return the teacherPersonName
	 */
	public String getTeacherPersonName() {
		return teacherPersonName;
	}

	/**
	 * @param teacherPersonName the teacherPersonName to set
	 */
	public void setTeacherPersonName(String teacherPersonName) {
		this.teacherPersonName = teacherPersonName;
	}

	/**
	 * @return the remainPoints
	 */
	public java.math.BigDecimal getRemainPoints() {
		return remainPoints;
	}

	/**
	 * @param remainPoints the remainPoints to set
	 */
	public void setRemainPoints(java.math.BigDecimal remainPoints) {
		this.remainPoints = remainPoints;
	}

	/**
	 * @return the totalPoints
	 */
	public java.math.BigDecimal getTotalPoints() {
		return totalPoints;
	}

	/**
	 * @param totalPoints the totalPoints to set
	 */
	public void setTotalPoints(java.math.BigDecimal totalPoints) {
		this.totalPoints = totalPoints;
	}

	/**
	 * @return the studentStatus
	 */
	public String getStudentStatus() {
		return studentStatus;
	}

	/**
	 * @param studentStatus the studentStatus to set
	 */
	public void setStudentStatus(String studentStatus) {
		this.studentStatus = studentStatus;
	}

	/**
	 * @return the discussTargetUserId
	 */
	public String getDiscussTargetUserId() {
		return discussTargetUserId;
	}

	/**
	 * @param discussTargetUserId the discussTargetUserId to set
	 */
	public void setDiscussTargetUserId(String discussTargetUserId) {
		this.discussTargetUserId = discussTargetUserId;
	}

	/**
	 * @return the discussTargetDiscussId
	 */
	public String getDiscussTargetDiscussId() {
		return discussTargetDiscussId;
	}

	/**
	 * @param discussTargetDiscussId the discussTargetDiscussId to set
	 */
	public void setDiscussTargetDiscussId(String discussTargetDiscussId) {
		this.discussTargetDiscussId = discussTargetDiscussId;
	}

	/**
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}

	/**
	 * @param question the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
	}

	/**
	 * @return the introduction
	 */
	public String getIntroduction() {
		return introduction;
	}

	/**
	 * @param introduction the introduction to set
	 */
	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

    public String getStartTutorTime() {
        return startTutorTime;
    }

    public void setStartTutorTime(String startTutorTime) {
        this.startTutorTime = startTutorTime;
    }

    public String getEndTutorTime() {
        return endTutorTime;
    }

    public void setEndTutorTime(String endTutorTime) {
        this.endTutorTime = endTutorTime;
    }

    public String getStartInsTime() {
        return startInsTime;
    }

    public void setStartInsTime(String startInsTime) {
        this.startInsTime = startInsTime;
    }

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * @return the teacherEmail
	 */
	public String getTeacherEmail() {
		return teacherEmail;
	}

	/**
	 * @param teacherEmail the teacherEmail to set
	 */
	public void setTeacherEmail(String teacherEmail) {
		this.teacherEmail = teacherEmail;
	}

	/**
	 * @return the teacherMobile
	 */
	public String getTeacherMobile() {
		return teacherMobile;
	}

	/**
	 * @param teacherMobile the teacherMobile to set
	 */
	public void setTeacherMobile(String teacherMobile) {
		this.teacherMobile = teacherMobile;
	}

	/**
	 * @return the teacherPhone
	 */
	public String getTeacherPhone() {
		return teacherPhone;
	}

	/**
	 * @param teacherPhone the teacherPhone to set
	 */
	public void setTeacherPhone(String teacherPhone) {
		this.teacherPhone = teacherPhone;
	}

	/**
	 * @return the teacherImage
	 */
	public String getTeacherImage() {
		return teacherImage;
	}

	/**
	 * @param teacherImage the teacherImage to set
	 */
	public void setTeacherImage(String teacherImage) {
		this.teacherImage = teacherImage;
	}

	/**
	 * @return the showMeetBtn
	 */
	public boolean isShowMeetBtn() {
		return showMeetBtn;
	}

	/**
	 * @param showMeetBtn the showMeetBtn to set
	 */
	public void setShowMeetBtn(boolean showMeetBtn) {
		this.showMeetBtn = showMeetBtn;
	}

	/**
	 * @return the teacherWorkplace
	 */
	public String getTeacherWorkplace() {
		return teacherWorkplace;
	}

	/**
	 * @param teacherWorkplace the teacherWorkplace to set
	 */
	public void setTeacherWorkplace(String teacherWorkplace) {
		this.teacherWorkplace = teacherWorkplace;
	}

	/**
	 * @return the teacherPosition
	 */
	public String getTeacherPosition() {
		return teacherPosition;
	}

	/**
	 * @param teacherPosition the teacherPosition to set
	 */
	public void setTeacherPosition(String teacherPosition) {
		this.teacherPosition = teacherPosition;
	}

}
