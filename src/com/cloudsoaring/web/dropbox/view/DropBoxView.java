package com.cloudsoaring.web.dropbox.view;

import java.io.File;
import java.util.Date;

import com.cloudsoaring.web.dropbox.entity.DirectoryEntity;
/**
 * 我的网盘View
 * @author scythe
 *
 */
public class DropBoxView extends DirectoryEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**文件id*/
	private String fileId;
	/**类型 */
	private String type;
	private String fileName;
	private Long fileSize;
	private String contentType;
	private boolean isDirectory;
	private String fileOrFolderType;
	private byte[] binary;
	private String searchType;
	
	//是否删除
	private Boolean delete;
	private File file;
	private Date startCreateDate;
	private Date endCreateDate;
	//session
	private String description;
	private String createUserName;
	private String createPersonName;
	private String path;
	private String businessId;
	private String thumbnailPath;
	private byte[] thumbnail;
	private String proportionThumbnailPath;
	private byte[] proportionThumbnail;
	
	
	//查询字段
	private String[] excludeContentTypes;
	private String[] includeContentTypes;
	//要排除的文件夹
	private String[] excludeDirectoryIds;
	
	private boolean onlyDirect = false;
	/**分享标记*/
    private String flagShare;
    
	public String getFlagShare() {
        return flagShare;
    }
    public void setFlagShare(String flagShare) {
        this.flagShare = flagShare;
    }
    public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Long getFileSize() {
		return fileSize;
	}
	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public byte[] getBinary() {
		return binary;
	}
	public void setBinary(byte[] binary) {
		this.binary = binary;
	}
	public Boolean getDelete() {
		return delete;
	}
	public void setDelete(Boolean delete) {
		this.delete = delete;
	}
	public File getFile() {
		return file;
	}
	public void setFile(File file) {
		this.file = file;
	}
	public Date getStartCreateDate() {
		return startCreateDate;
	}
	public void setStartCreateDate(Date startCreateDate) {
		this.startCreateDate = startCreateDate;
	}
	public Date getEndCreateDate() {
		return endCreateDate;
	}
	public void setEndCreateDate(Date endCreateDate) {
		this.endCreateDate = endCreateDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCreateUserName() {
		return createUserName;
	}
	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}
	public String getCreatePersonName() {
		return createPersonName;
	}
	public void setCreatePersonName(String createPersonName) {
		this.createPersonName = createPersonName;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getBusinessId() {
		return businessId;
	}
	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}
	public String getThumbnailPath() {
		return thumbnailPath;
	}
	public void setThumbnailPath(String thumbnailPath) {
		this.thumbnailPath = thumbnailPath;
	}
	public byte[] getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(byte[] thumbnail) {
		this.thumbnail = thumbnail;
	}
	public String getProportionThumbnailPath() {
		return proportionThumbnailPath;
	}
	public void setProportionThumbnailPath(String proportionThumbnailPath) {
		this.proportionThumbnailPath = proportionThumbnailPath;
	}
	public byte[] getProportionThumbnail() {
		return proportionThumbnail;
	}
	public void setProportionThumbnail(byte[] proportionThumbnail) {
		this.proportionThumbnail = proportionThumbnail;
	}
	public boolean isDirectory() {
		return isDirectory;
	}
	public void setDirectory(boolean isDirectory) {
		this.isDirectory = isDirectory;
	}
	public String getFileOrFolderType() {
		return fileOrFolderType;
	}
	public void setFileOrFolderType(String fileOrFolderType) {
		this.fileOrFolderType = fileOrFolderType;
	}
	public String getSearchType() {
		return searchType;
	}
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}
	public String[] getExcludeContentTypes() {
		return excludeContentTypes;
	}
	public void setExcludeContentTypes(String[] excludeContentTypes) {
		this.excludeContentTypes = excludeContentTypes;
	}
	public String[] getIncludeContentTypes() {
		return includeContentTypes;
	}
	public void setIncludeContentTypes(String[] includeContentTypes) {
		this.includeContentTypes = includeContentTypes;
	}
	public boolean isOnlyDirect() {
		return onlyDirect;
	}
	public void setOnlyDirect(boolean onlyDirect) {
		this.onlyDirect = onlyDirect;
	}
	public String[] getExcludeDirectoryIds() {
		return excludeDirectoryIds;
	}
	public void setExcludeDirectoryIds(String[] excludeDirectoryIds) {
		this.excludeDirectoryIds = excludeDirectoryIds;
	}
}
