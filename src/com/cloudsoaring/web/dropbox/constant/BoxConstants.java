package com.cloudsoaring.web.dropbox.constant;

import com.cloudsoaring.web.common.dao.ibatis.plugin.MybatisConstant;

@MybatisConstant
public interface BoxConstants extends BoxConfigNames, BoxDbConstants, BoxLogConstants, BoxDefaultValues, BoxMsgNames, BoxStateConstants{
	
	/**文件类型-全部*/
	public static final String FILE_BUSINESS_TYPE_ALL = "0";
	/**文件类型-image*/
	public static final String FILE_BUSINESS_TYPE_IMAGE = "1";
	public static final String FILE_TYPE_JPG = "jpg";
	public static final String FILE_TYPE_JPEG = "jpeg";
	public static final String FILE_TYPE_PNG = "png";
	/**文件类型-video*/
	public static final String FILE_BUSINESS_TYPE_VIDEO = "2";
	public static final String FILE_TYPE_MP4 = "mp4";
	public static final String FILE_TYPE_FLV = "flv";
	public static final String FILE_TYPE_AVI = "avi";
	public static final String FILE_TYPE_WMV = "wmv";
	public static final String FILE_TYPE_ZIP = "zip";
	/**文件类型-文档*/
	public static final String FILE_BUSINESS_TYPE_TXT = "3";
	/**文件类型-文件夹*/
	public static final String FILE_BUSINESS_TYPE_DIRECTORY = "4";
	/**指定修改目标为文件类型*/
	public static final String MOVE_TYPE_FILE = "0";
	/**指定修改目标为目录类型*/
	public static final String MOVE_TYPE_DIRECTORY = "1";
	/**共享标识：0否 1是*/
    public static final String FLAG_SHARE_IS = "1";
    public static final String FLAG_SHARE_NOT = "0";
    /** 状态（用于资源管理：1【审批通过】，2【撤销】）*/
    public static final String  STATE_SHARE_PASS="1";
    public static final String  STATE_SHARE_CANCEL="2";
    /** 官方资源根目录*/
    public static final String OFFICIAL_ROOT_DERACTORY="10000";
	public static final String FILE_TYPE_NETDISK = "NETDIST";
}
