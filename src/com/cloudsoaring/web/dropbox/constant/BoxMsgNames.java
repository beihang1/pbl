package com.cloudsoaring.web.dropbox.constant;

public interface BoxMsgNames {
	/**文件id不能为空*/
	public static final String MSG_FILE_ID_NOT_NULL = "msg.e.fileIdNotNull";
	/**指定复制目录不能为空*/
	public static final String MSG_DIRECTORY_ID_NOT_NULL = "msg.e.directoryIdNotNull";
	/**指定文件类型不能为空*/
	public static final String MSG_FILE_TYPE_NOT_NULL = "msg.e.fileTypeNotNull";
	/**文件名称不能为空*/
	public static final String MSG_DIRECTORY_NAME_NOT_NULL = "msg.e.directoryNameNotNull";
	/**文件名称不能重复*/
	public static final String MSG_FILE_NAME_EXIST = "msg.e.fileNameExsit";
	/**文件夹名称不能重复*/
	public static final String MSG_DIRECTORY_NAME_EXIST = "msg.e.directoryNameExsit";
	/**文件不存在！*/
	public static final String MSG_FILE_NAME_NOTG_EXIST = "msg.e.fileNameNotExsit";
	/**文件已存在*/
	public static final String MSG_FILE_EXIST = "msg.e.fileExsit";
	/**文件目录不存在*/
	public static final String MSG_DIRECTORY_NOT_FOUND = "msg.e.directoryNotFound";
}
