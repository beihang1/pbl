package com.cloudsoaring.web.dropbox.constant;

public interface BoxDbConstants {

	/**检索文件目录表*/
	public static final String SQL_SELECT_DIRECTORY_LIST = "Directory.select";
	/**创建文件夹*/
	public static final String SQL_INSERT_DIRECTORY = "Directory.insert";
	/**检索根目录详细信息*/
	public static final String SQL_SEARCH_DETAIL_4_ROOT = "Directory.selectRootDetail";
	/**全部文件列表*/
	public static final String SQL_SELECT_FILE_LIST = "Directory.selectAllFileList";
	/**复制文件*/
	public static final String SQL_FILE_COPY = "FileDirectory.insert";
	/**移动前检索*/
	public static final String SQL_SELECT_4_MOVE = "FileDirectory.select4Move";
	/**共享提交*/
	public static final String SQL_UPDATE_FLAG_SHAER = "FileDirectory.updateFlagShare";
	/**资源列表*/
	public static final String SQL_SEARCH_RESOURCE_LIST= "FileDirectory.searchResourceList";
	/**在线编辑器资源列表*/
	public static final String SQL_SEARCH_RESOURCE_LIST_BY_TAGS= "FileDirectory.searchResourceListByTags";
	
	public static final String SEARCH_TAG_LINK_ALL_LINKID = "TagLink.selectAllByLinkId";
	/**删除目录*/
	public static final String SQL_DELETE_DIRECTORY = "Directory.delete";
	/**删除文件*/
	public static final String SQL_DELETE_FILE_DIRECTORY_4_MOVE = "FileDirectory.delete";
	/**移动文件*/
	public static final String SQL_FILE_MOVE = "FileDirectory.moveFile";
	/**移动目录*/
	public static final String SQL_DIRECTORY_MOVE = "Directory.moveDirectory";
	/**文件重命名*/
	public static final String SQL_FIEL_RENAME = "FileDirectory.fileRename";
	/**目录重命名*/
	public static final String SQL_DIRECTORY_RENAME = "Directory.directoryRename";
	/**目录删除文件关联*/
	public static final String SQL_DELETE_FILE_DIRECTORY = "FileDirectory.deleteFileDirectory";
	/**检索文件其他引用*/
	public static final String SQL_SELECT_ORTHER_FILE_DIRECTORY = "FileDirectory.select4Rename";
	/**检索文件其他引用*/
	public static final String SQL_SELECT_ORTHER_FILE= "FileDirectory.select";
	/**检索文件关联详情*/
	public static final String SQL_SELECT_FILE_DIRECTORY_DIRECTORY = "FileDirectory.selectByPk";
	/**检索文件关联详情*/
	public static final String SQL_SELECT_4_RENAME_DIRECTORY = "Directory.select4RenameDirectory";
	/**检索文件夹详情*/
	public static final String SQL_SELECT_DIRECTORY_DETAIAL = "Directory.selectByPk";
	/**添加共享分类*/
	public static final String SQL_INSERT_TAG="TagLink.insertShareTag";
	
	public static final String SQL_SELECT_FILE_DIRECTORY_DIRECTORY_BY_FILE_ID = "FileDirectory.selectByFileId";
}
