package com.cloudsoaring.web.dropbox.service;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.FileEntity;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.entity.TreeItem;
import com.cloudsoaring.web.common.utils.TreeUtil;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.entity.TagLinkEntity;
import com.cloudsoaring.web.dropbox.constant.BoxConstants;
import com.cloudsoaring.web.dropbox.entity.DirectoryEntity;
import com.cloudsoaring.web.dropbox.entity.FileDirectoryEntity;
import com.cloudsoaring.web.dropbox.view.DropBoxView;

/**
 * 我的网盘
 * @author jincongsheng
 *
 */
@Service
public class DropBoxService extends FileService implements BoxConstants {

	/**
	 * 文件列表
	 * @param	fileId(目录id),searchType(0:检索全部，1：检索图片，2：检索视频，3：检索文档, 4：检索文件夹),type(0:检索根目录，1：检索其他目录)
	 * 			excludeDirectoryIds 检索结果要排除的文件夹
	 * @return resultBean  
	 */
	public ResultBean userFileList(DropBoxView entity){
		ResultBean resultBean = new ResultBean();
		// 判断用户是否登录
		assertNotGuest();
		
		if(StringUtil.isEmpty(entity.getSortName())){
			entity.setSortName("CREATE_DATE");
			entity.setSortOrder("ASC");
		}
		
		if(StringUtil.isEmpty(entity.getFileId())) { //目录为空，则认为是根目录
			DropBoxView boxView = new DropBoxView();
			boxView.setUserId(WebContext.getSessionUserId());
			//用户第一次进入网盘，检索，判断是否有根目录
			DirectoryEntity rootFile = getRoot(WebContext.getSessionUserId());
			//若没有，创建根目录
			if(rootFile == null){
				rootFile = new DirectoryEntity();
				rootFile.setFlagRoot(MOVE_TYPE_DIRECTORY);
				rootFile.setCreateUser(WebContext.getSessionUserId());
				rootFile.setDirectoryId(IDGenerator.genUID());
				rootFile.setUserId(WebContext.getSessionUserId());
				rootFile.setParentId("");
				commonDao.insertData(rootFile, SQL_INSERT_DIRECTORY);
			} 
			
			entity.setParentId(rootFile.getDirectoryId());
		} else {
			entity.setParentId(entity.getFileId());
		}
		
		String[] type_image = {FILE_TYPE_JPG, FILE_TYPE_PNG, FILE_TYPE_JPEG};
		String[] type_video = {FILE_TYPE_MP4, FILE_TYPE_FLV, FILE_TYPE_AVI, FILE_TYPE_WMV};
		
		entity.setUserId(WebContext.getSessionUserId());
		//检索根目录下所有文件和目录
		if(FILE_BUSINESS_TYPE_ALL.equals(entity.getSearchType())){
			
		}else if(FILE_BUSINESS_TYPE_IMAGE.equals(entity.getSearchType())){
			entity.setIncludeContentTypes(type_image);
		}else if(FILE_BUSINESS_TYPE_VIDEO.equals(entity.getSearchType())){
			entity.setIncludeContentTypes(type_video);
		}else if(FILE_BUSINESS_TYPE_TXT.equals(entity.getSearchType())){
			entity.setExcludeContentTypes((String[])ArrayUtils.addAll(type_image, type_video));
		}else if(FILE_BUSINESS_TYPE_DIRECTORY.equals(entity.getSearchType())){
			entity.setOnlyDirect(true);
		}
		
		resultBean = commonDao.searchList4Page(entity, SQL_SELECT_FILE_LIST);
		return resultBean;
	}
	
	/**
	 * 目录树结构
	 * @param  fileId(当前选中文件id) 
	 * @return resultBean  
	 */
	public ResultBean getTree(String fileId){
		ResultBean result = new ResultBean();
		List<TreeItem> treeItem=directoryTree(fileId);
		result.setData(treeItem);
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<TreeItem> directoryTree(String fileId){
		//检索所有目录
		DirectoryEntity condition = new DirectoryEntity();
		condition.setUserId(WebContext.getSessionUserId());
		List<DirectoryEntity> firstDirectoryList = commonDao.searchList(condition, SQL_SELECT_DIRECTORY_LIST);
		List<TreeItem> result = TreeUtil.getTree(firstDirectoryList);
		//禁用当前目录以及子目录
		String[] fileIds = StringUtil.split(fileId);
		for(TreeItem item:result){
			if(item.disabled(true, fileIds)){
				break;
			}
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public ResultBean directoryTreeList(String fileId){
		ResultBean resul=new ResultBean();
		//检索所有目录
		DirectoryEntity condition = new DirectoryEntity();
		condition.setUserId(WebContext.getSessionUserId());
		List<DirectoryEntity> firstDirectoryList = commonDao.searchList(condition, SQL_SELECT_DIRECTORY_LIST);
		List<TreeItem> result = TreeUtil.getTree(firstDirectoryList);
		//禁用当前目录以及子目录
		String[] fileIds = StringUtil.split(fileId);
		for(TreeItem item:result){
			if(item.disabled(true, fileIds)){
				break;
			}
		}
		return resul.setData(result);
	}
	
	/**
	 * 文件复制
	 * @param  ids(文件id集合),directoryId(指定复制目录的id)
	 * @return resultBean  
	 */
	public ResultBean fileCopy(String ids, String directoryId){
		ResultBean resultBean = new ResultBean();
		// 判断用户是否登录
		assertNotGuest();
		// 文件id不能为空
		assertNotEmpty(ids, MSG_FILE_ID_NOT_NULL);
		
		String[] idList = ids.split(",");
		for(String id:idList){
			FileDirectoryEntity fileEntity = new FileDirectoryEntity();
			fileEntity.setId(id);
			//检索文件关联详情
			FileDirectoryEntity resultEntity = commonDao.searchOneData(fileEntity, SQL_SELECT_FILE_DIRECTORY_DIRECTORY);
			
			FileDirectoryEntity boxView = new FileDirectoryEntity();
			boxView.setDirectoryId(directoryId);
			boxView.setFileName(resultEntity.getFileName());
			/*int other = commonDao.getTotal(boxView, SQL_SELECT_ORTHER_FILE);
			if(other>0){
				resultBean.setStatus(false);
				resultBean.setMessages(getMessage(MSG_FILE_NAME_EXIST,boxView.getFileName()));
				return resultBean;
			}*/
			boxView.setId(IDGenerator.genUID());
			boxView.setFileId(resultEntity.getFileId());
			//在指定目录下新增一个文件
			commonDao.insertData(boxView, SQL_FILE_COPY);
		}
		resultBean.setData(true);
		return resultBean;
	}
	
	/**
	 * 文件上传
	 * @param  htmlInputName(页面input标签中的file类型的name值),directoryId(指定上传目录id),fileName(上传文件的文件名)  
	 * @return   
	 */
	public ResultBean fileUpload(String htmlInputName,String directoryId,String fileName)throws Exception{
		ResultBean resultBean = new ResultBean();
		// 判断用户是否登录
		assertNotGuest();
		FileEntity resultData = processRequestFile(htmlInputName, BoxConstants.FILE_TYPE_NETDISK);
		resultBean.setData(resultData);
		if(StringUtil.isEmpty(resultData.getFileId())){
			return ResultBean.error("未上传文件或上传文件内容为空");
		}
		String fileId = resultData.getFileId();
		String upLoadFileName = StringUtil.getOrElse(fileName,resultData.getFileName());
		FileDirectoryEntity entity = new FileDirectoryEntity();
		//检索根目录信息
		DropBoxView boxView = new DropBoxView();
		boxView.setUserId(WebContext.getSessionUserId());
		boxView.setParentId("");
		DirectoryEntity resultRootEntity = commonDao.searchOneData(boxView, SQL_SEARCH_DETAIL_4_ROOT);
		
		DirectoryEntity directoryEntity = new DirectoryEntity();
		directoryEntity.setUserId(WebContext.getSessionUserId());
		if("".equals(directoryId)){
			directoryEntity.setDirectoryId(resultRootEntity.getDirectoryId());
		}else{
			directoryEntity.setDirectoryId(directoryId);
		}
		int myDirectory = commonDao.getTotal(directoryEntity, SQL_SELECT_DIRECTORY_LIST);
		if(myDirectory<=0){
			resultBean.setStatus(false);
			resultBean.setMessages(getMessage(MSG_DIRECTORY_NOT_FOUND,entity.getFileName()));
			return resultBean;
		}
		
		if("".equals(directoryId)){
			entity.setDirectoryId(resultRootEntity.getDirectoryId());
		}else{
			entity.setDirectoryId(directoryId);
		}
		entity.setFileName(upLoadFileName);
		//判断文件名是否存在
		/*int other = commonDao.getTotal(entity, SQL_SELECT_ORTHER_FILE);
		if(other>0){
			resultBean.setStatus(false);
			resultBean.setMessages(getMessage(MSG_FILE_NAME_EXIST,entity.getFileName()));
			return resultBean;
		}*/
		entity.setFileId(fileId);
		entity.setId(IDGenerator.genUID());
		commonDao.insertData(entity, SQL_FILE_COPY);
		
		resultBean.setStatus(true);
		return resultBean;
	}
	
	
	/**
	 * 文件删除
	 * @param fileIds(当前选中文件id集合),directoryIds(选中的目录id集合),parentId(父目录id)
	 * @return   
	 */
	public ResultBean fileDelete(String fileIds,String directoryIds,String parentId)throws Exception{
		//对文件操作时，userId是必须条件
		ResultBean resultBean = new ResultBean();
		// 判断用户是否登录
		assertNotGuest();
		// 文件id不能为空
		assertNotEmpty(StringUtil.getOrElse(fileIds) +StringUtil.getOrElse(directoryIds), MSG_FILE_ID_NOT_NULL);
		String[] fileIdList = fileIds.split(",");
		String[] directoryIdList = directoryIds.split(",");
		for(String fileId:fileIdList){
			deleteFile(fileId,parentId);
		}	
		for(String directoryId:directoryIdList){
			deleteDirectory(directoryId,parentId);
		}
		resultBean.setStatus(true);
		return resultBean;
	}
	
	//删除文件
	private boolean deleteFile(String fileId, String parentId)throws Exception{
		boolean result = false;
		//删除
		DropBoxView entity = new DropBoxView();
		entity.setId(fileId);
		entity.setDirectoryId(parentId);
		entity.setUserId(WebContext.getSessionUserId());
		int deleteCnt = commonDao.deleteData(entity, SQL_DELETE_FILE_DIRECTORY);
		if(deleteCnt > 0){
			//判断是否存在其他的引用
			int other = commonDao.getTotal(entity, SQL_SELECT_ORTHER_FILE_DIRECTORY);
			if(other == 0){
				//删除文件
				deleteFileById(fileId, BoxConstants.FILE_TYPE_NETDISK);
			}
		}
		result= true;
		return result;
	}
	//删除目录
	@SuppressWarnings({ "unchecked" })
	private boolean deleteDirectory(String directoryId, String parentId)throws Exception{
		boolean result = false;
		//获取directoryId下的子目录和子文件
		DropBoxView entity = new DropBoxView();
		entity.setParentId(directoryId);
		entity.setUserId(WebContext.getSessionUserId());
		List<DropBoxView> list = commonDao.searchList(entity, SQL_SELECT_FILE_LIST);
		//删除子目录和子文件
		for(DropBoxView dbv : list){
			if(dbv.isDirectory()){
				deleteDirectory(dbv.getId(), directoryId);
			}else{
				deleteFile(dbv.getId(), directoryId);
			}
		}
		//删除自己
		DropBoxView myEntity = new DropBoxView();
		myEntity.setDirectoryId(directoryId);
		commonDao.deleteData(myEntity, SQL_DELETE_DIRECTORY);
		result = true;
		return result;
	}
	
	/**
	 * 文件移动
	 * @param  ids(当前选中文件的id集合),directoryIds(当前选中文件夹id集合),fatherId(指定移动目录的id)
	 * @return   
	 */
	public ResultBean fileMove(String ids,String fatherId ,String directoryIds){
		ResultBean resultBean = new ResultBean();
		// 判断用户是否登录
		assertNotGuest();
		
		if(StringUtil.isEmpty(fatherId)){
			DirectoryEntity resultRootEntity = getRoot(WebContext.getSessionUserId());
			fatherId = resultRootEntity.getDirectoryId();
		}
		
		String[] idList = ids.split(",");
		//针对文件移动
		if(StringUtil.isNotEmpty(ids)){
			for(String id:idList){
				FileDirectoryEntity fileEntity = new FileDirectoryEntity();
				fileEntity.setId(id);
				//检索文件关联详情
				FileDirectoryEntity resultEntity = commonDao.searchOneData(fileEntity, SQL_SELECT_FILE_DIRECTORY_DIRECTORY);
				
				FileDirectoryEntity boxView = new FileDirectoryEntity();
				boxView.setDirectoryId(fatherId);
				/*if(StringUtil.isEmpty(resultEntity.getFileName())){
					resultBean.setStatus(false);
					resultBean.setMessages(getMessage(MSG_FILE_NAME_EXIST));
					return resultBean;
				}*/
				boxView.setFileName(resultEntity.getFileName());
				/*int other = commonDao.getTotal(boxView, SQL_SELECT_ORTHER_FILE);
				if(other>0){
					resultBean.setStatus(false);
					resultBean.setMessages(getMessage(MSG_FILE_NAME_EXIST,boxView.getFileName()));
					return resultBean;
				}*/
				boxView.setId(id);
				boxView.setFileId(resultEntity.getFileId());
				commonDao.updateData(boxView, SQL_FILE_MOVE);
			}
		}
		String[] directoryIdList = directoryIds.split(",");
		//针对目录移动
		if(StringUtil.isNotEmpty(directoryIds)){
			for(String directoryId:directoryIdList){
				DirectoryEntity entity = new DirectoryEntity();
				entity.setDirectoryId(directoryId);
				DirectoryEntity directoryEntity = commonDao.searchOneData(entity, SQL_SELECT_DIRECTORY_DETAIAL);
				entity.setParentId(fatherId);
				
				DirectoryEntity de = new DirectoryEntity();
				de.setParentId(fatherId);
				de.setDirectoryName(directoryEntity.getDirectoryName());
				de.setUserId(WebContext.getSessionUserId());
				int other = commonDao.getTotal(de, SQL_SELECT_DIRECTORY_LIST);
				if(other>0){
					resultBean.setStatus(false);
					resultBean.setMessages(getMessage(MSG_DIRECTORY_NAME_EXIST,de.getDirectoryName()));
					return resultBean;
				}
				entity.setUpdateUser(WebContext.getSessionUserId());
				commonDao.updateData(entity, SQL_DIRECTORY_MOVE);
			}
		}
		resultBean.setStatus(true);
		return resultBean;
	}
	
	/**
	 * 文件重命名
	 * @param  id(当前选中文件或目录的id),fileOrFloderType(0:针对文件的重命名,1:针对目录的重命名),name(修改过后的文件名)
	 * @return   
	 */
	public ResultBean fileRename(String id,String fileOrFolderType,String name){
		ResultBean resultBean = new ResultBean();
		// 判断用户是否登录
		assertNotGuest();
		// 文件id不能为空
		assertNotEmpty(id, MSG_FILE_ID_NOT_NULL);
		if(MOVE_TYPE_FILE.equals(fileOrFolderType)){
			FileDirectoryEntity fileEntity = new FileDirectoryEntity();
			fileEntity.setFileName(name);
			fileEntity.setId(id);
			//判断名称是否重复
			/*int other = commonDao.getTotal(fileEntity, SQL_SELECT_ORTHER_FILE_DIRECTORY);
			if(other>0){
				resultBean.setStatus(false);
				resultBean.setMessages(getMessage(MSG_FILE_NAME_EXIST,fileEntity.getFileName()));
				return resultBean;
			}*/
			FileEntity entity = new FileEntity();
			entity.setFileName(name);
			entity.setId(id);
			entity.setUpdateUser(WebContext.getSessionUserId());
			commonDao.updateData(entity, SQL_FIEL_RENAME);
		}else if(MOVE_TYPE_DIRECTORY.equals(fileOrFolderType)){
			DirectoryEntity entity = new DirectoryEntity();
			entity.setDirectoryId(id);
			entity.setDirectoryName(name);
			entity.setUserId(WebContext.getSessionUserId());
			//判断名称是否重复
			/*int other = commonDao.getTotal(entity, SQL_SELECT_4_RENAME_DIRECTORY);
			if(other >0){
				resultBean.setStatus(false);
				resultBean.setMessages(getMessage(MSG_FILE_NAME_EXIST,entity.getDirectoryName()));
				return resultBean;
			}*/
			entity.setDirectoryId(id);
			entity.setUpdateUser(WebContext.getSessionUserId());
			commonDao.updateData(entity, SQL_DIRECTORY_RENAME);
		}
		resultBean.setStatus(true);
		return resultBean;
	}
	
	/**
	 * 新建文件夹
	 * @param  parentId(父目录id),directoryName(文件夹名称)
	 * @return   
	 */
	public ResultBean newFolder(DropBoxView entity){
		ResultBean resultBean = new ResultBean();
		// 判断用户是否登录
		assertNotGuest();
		//文件夹名称不能为空
		assertNotEmpty(entity.getDirectoryName(), MSG_DIRECTORY_NAME_NOT_NULL);
		//检索根目录详细信息
		DirectoryEntity resultRootEntity = getRoot(WebContext.getSessionUserId());
		//将根目录的directoryId作为新建文件夹的parentId
		DropBoxView insertEntity = new DropBoxView();
		//在根目录下新建目录
		if(StringUtil.isEmpty(entity.getParentId())){
			insertEntity.setParentId(resultRootEntity.getDirectoryId());
			//在其他目录下新建目录
		}else{
			insertEntity.setParentId(entity.getParentId());
		}
		insertEntity.setDirectoryName(entity.getDirectoryName());
		insertEntity.setUserId(WebContext.getSessionUserId());
		//判断名称是否重复
	/*	int other = commonDao.getTotal(insertEntity, SQL_SELECT_DIRECTORY_LIST);
		if(other>0){
			resultBean.setStatus(false);
			resultBean.setMessages(getMessage(MSG_FILE_NAME_EXIST,insertEntity.getDirectoryName()));
			return resultBean;
		}*/
		insertEntity.setDirectoryId(IDGenerator.genUID());
		insertEntity.setUserId(WebContext.getSessionUserId());
		insertEntity.setCreateUser(WebContext.getSessionUserId());
		insertEntity.setFlagRoot(MOVE_TYPE_FILE);
		commonDao.insertData(insertEntity, SQL_INSERT_DIRECTORY);
		return resultBean;
	}

	private DirectoryEntity getRoot(String userId) {
		DirectoryEntity rootEntity = new DirectoryEntity();
		rootEntity.setFlagRoot(MOVE_TYPE_DIRECTORY);
		rootEntity.setParentId("");
		rootEntity.setUserId(userId);
		DirectoryEntity resultRootEntity = commonDao.searchOneData(rootEntity, SQL_SEARCH_DETAIL_4_ROOT);
		return resultRootEntity;
	}

    public ResultBean submitShare(FileDirectoryEntity entity) {
        ResultBean resultBean = new ResultBean();
        entity.setTimeShare(new Date());
        commonDao.updateData(entity, SQL_UPDATE_FLAG_SHAER);
        //共享分类
        if(StringUtil.isNotEmpty(entity.getTagFirstId())){
            TagLinkEntity first= new TagLinkEntity();
            first.setTagId(entity.getTagFirstId());
            first.setLinkType("SHARE");
            first.setLinkId(entity.getId());
            commonDao.insertData(first, SQL_INSERT_TAG);
            if(StringUtil.isNotEmpty(entity.getTagSecondId())){
                TagLinkEntity second=new TagLinkEntity();
                second.setTagId(entity.getTagSecondId());
                second.setLinkId(entity.getId());
                second.setLinkType("SHARE"); 
                second.setParentTagId(entity.getTagFirstId());
                commonDao.insertData(second, SQL_INSERT_TAG);
            }
        }
        return resultBean.setMessages("分享成功");
    }
    /**
     * 资源列表
     * @param entity 资源对象
     * @param searchType 检索条件：全部：0/官方：1/非官方：2
     * @param sortType  排序类型： new/hot 最新（共享时间）/最热（下载量）；
     * @param pageSize 每页显示行数
     * @param pageNumber 当前页
     * @return
     */
    public ResultBean searchResourceList(FileDirectoryEntity entity,String searchType,String sortType,
            Integer pageSize, Integer pageNumber,String directionTagId, String categoryTagId) {
        if(StringUtil.isNotEmpty(searchType)){
            if("1".equals(searchType)){
                entity.setDirectoryId(OFFICIAL_ROOT_DERACTORY);
            }else if("2".equals(searchType)){
                entity.setDirectoryId1(OFFICIAL_ROOT_DERACTORY);
            }
        } 
        if(StringUtil.isNotEmpty(sortType)){
            if("new".equalsIgnoreCase(sortType)){
                entity.setSortName("timeShare");
                entity.setSortOrder("DESC");
            }else if("hot".equalsIgnoreCase(sortType)){
                entity.setSortName("numDowload");
                entity.setSortOrder("DESC");
            }
        }
        if (pageSize != null) {
            entity.setPageSize(pageSize);
        }
        if (pageNumber != null) {
            entity.setPageNumber(pageNumber);
        }
        if(StringUtil.isNotEmpty(directionTagId)){
            entity.setTagFirstId(directionTagId);
        }
        if(StringUtil.isNotEmpty(categoryTagId)){
            entity.setTagSecondId(categoryTagId);
        }
        entity.setFlagShare(FLAG_SHARE_IS);
        entity.setStateShare(STATE_SHARE_PASS);
        return commonDao.searchList4Page(entity, SQL_SEARCH_RESOURCE_LIST);
    }
    
    
    public List<FileDirectoryEntity> getResourceListByTags(String firstTagId, String secondTagId){
    	FileDirectoryEntity entity = new FileDirectoryEntity();
    	if(StringUtil.isNotEmpty(firstTagId) && firstTagId !=null){
    		entity.setTagFirstId(firstTagId);
    	}
    	if(StringUtil.isNotEmpty(secondTagId) && secondTagId !=null){
    		entity.setTagSecondId(secondTagId);
    	}
    	entity.setSortName("timeShare");
    	entity.setSortOrder("DESC");
    	return commonDao.searchList(entity, SQL_SEARCH_RESOURCE_LIST_BY_TAGS);
    }
    
    public List<FileDirectoryEntity> getResourceListByDate(Date startDate, Date endDate){
    	FileDirectoryEntity entity = new FileDirectoryEntity();
    	entity.setStartDate(startDate);
    	entity.setEndDate(endDate);
        entity.setSortName("timeShare");
        entity.setSortOrder("DESC");
        return commonDao.searchList(entity, SQL_SEARCH_RESOURCE_LIST_BY_TAGS);
    }
    
    
    public List<TagLinkEntity> getTagListByResourceId(String linkId){
        return commonDao.searchList(linkId, SEARCH_TAG_LINK_ALL_LINKID);
    }
    
    public FileDirectoryEntity getFileDirectoryByFileId(String fileId) {
    	FileDirectoryEntity entity = new FileDirectoryEntity();
    	entity.setFileId(fileId);
    	return commonDao.searchOneData(entity, SQL_SELECT_FILE_DIRECTORY_DIRECTORY_BY_FILE_ID);
    }
}
