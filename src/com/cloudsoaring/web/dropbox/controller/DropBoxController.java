package com.cloudsoaring.web.dropbox.controller;

import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.common.utils.FileUtil;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.FileEntity;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.entity.TreeItem;
import com.cloudsoaring.web.common.utils.ConfigUtil;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.dropbox.constant.BoxConstants;
import com.cloudsoaring.web.dropbox.entity.FileDirectoryEntity;
import com.cloudsoaring.web.dropbox.service.DropBoxService;
import com.cloudsoaring.web.dropbox.view.DropBoxView;
/**
 * 网盘管理
 * @author jincongsheng
 *
 */
@Controller
public class DropBoxController extends BaseController implements BoxConstants{
	@Autowired
	private DropBoxService dropBoxService;
	
	/**
	 * 一览  
	 * @return
	 */
	@RequestMapping("/client/dropBox/dropBoxList.action")
	public ModelAndView dropBoxList(){
		ModelAndView mv = new ModelAndView("dropBox/dropBox_list");
		return mv;
	}
	
	/**
	 * 文件列表
	 * @param  searchType(0:检索全部，1：检索图片，2：检索视频，3：检索文档),fileId(目录id),type(0:检索根目录，1：检索其他目录)
	 * @return resultBean  
	 */
	@RequestMapping("/client/dropBox/userFileList.action")
	@ResponseBody
	public ResultBean userFileList(DropBoxView entity){
		return dropBoxService.userFileList(entity);
	}
	
	/**
	 * 目录树结构
	 * @param  fileId(当前选中文件id) 
	 * @return resultBean  
	 */
	@RequestMapping("/client/dropBox/getDirectoryTree.action")
	@ResponseBody
	public List<TreeItem> getTree(String fileId){
		return dropBoxService.directoryTree(fileId);
	}
	
	
	/**
	 * 目录树结构
	 * @param  fileId(当前选中文件id) 
	 * @return resultBean  
	 */
	@RequestMapping("/client/dropBox/getDirectoryTreeList.action")
	@ResponseBody
	public ResultBean getTreeList(String fileId){
		return dropBoxService.directoryTreeList(fileId);
	}
	/**
	 * 文件复制
	 * @param  ids(当前选中文件id集合) directoryId(指定复制目录的id) 
	 * @return resultBean  
	 */
	@RequestMapping("/client/dropBox/fileCopy.action")
	@ResponseBody
	public ResultBean fileCopy(String ids, String directoryId){
		return dropBoxService.fileCopy(ids,directoryId);
	}
	
	/**
	 * 文件上传
	 * @param  htmlInputName(页面input标签中的file类型的name值),directoryId(指定上传目录id),fileName(上传文件的文件名)  
	 * @return   
	 */
	@RequestMapping("/client/dropBox/fileUpload.action")
	@ResponseBody
	public ResultBean fileUpload(String htmlInputName,String directoryId,String fileName)throws Exception{
		WebContext.init4CrossDomainAjax();
		return dropBoxService.fileUpload(htmlInputName,directoryId,fileName);
	}
	
	
	/**
	 * 文件删除
	 * @param  fileIds(当前选中文件id集合),directoryIds(目录id集合),parentId(父目录id)
	 * @return   
	 */
	@RequestMapping("/client/dropBox/fileDelete.action")
	@ResponseBody
	public ResultBean fileDelete(String fileIds,String directoryIds,String parentId)throws Exception{
		return dropBoxService.fileDelete(fileIds,directoryIds,parentId);
	}
	
	/**
	 * 文件移动
	 * @param  ids(当前选中文件或目录的id集合),fatherId(指定移动目录的id),directoryIds(当前选中文件夹id集合)
	 * @return   
	 */
	@RequestMapping("/client/dropBox/fileMove.action")
	@ResponseBody
	public ResultBean fileMove(String ids,String fatherId,String directoryIds){
		return dropBoxService.fileMove(ids,fatherId,directoryIds);
	}
	
	/**
	 * 文件重命名
	 * @param  id(当前选中文件或目录的id),fileOrFolderType(0:针对文件的重命名,1:针对目录的重命名),name(修改过后的文件名)
	 * @return   
	 */
	@RequestMapping("/client/dropBox/fileRename.action")
	@ResponseBody
	public ResultBean fileRename(String id,String fileOrFolderType,String name){
		return dropBoxService.fileRename(id,fileOrFolderType,name);
	}
	/**
	 * 新建文件夹
	 * @param  parentId(父目录id),directoryName(文件夹名称)
	 * @return   
	 */
	@RequestMapping("/client/dropBox/newFolder.action")
	@ResponseBody
	public ResultBean newFolder(DropBoxView entity){
		return dropBoxService.newFolder(entity);
	}
	/**
	 * 文件下载
	 * @param  fileId(指定下载文件的fileId)
	 * @return   
	 */
	@RequestMapping("/client/dropBox/fileDownload.action")
	@ResponseBody
	public void fileDownload(@RequestParam(required = true) String fileId, HttpServletResponse response) throws Exception {
		OutputStream output = new BufferedOutputStream(response.getOutputStream());
		InputStream input = null;
		try {
			FileEntity file = fileService.getFile(fileId, BoxConstants.FILE_TYPE_NETDISK);
			byte[] binary = file.getBinary();
			String fileName = file.getFileName();
			fileName = URLEncoder.encode(fileName, "UTF-8");
			response.reset();
			response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			response.addHeader("Content-Length", "" + file.getFileSize());
			response.setContentType("application/octet-stream;charset=UTF-8");
			if(StringUtil.isNotEmpty(file.getPath())&& StringUtil.isNotEmpty(ConfigUtil.getFileStorageRoot())){
				//文件有路径
				input = FileUtil.getInputStream(ConfigUtil.getFileStorageRoot(), file.getPath());
				IOUtils.copyLarge(input, output);
			}else{
				output.write(binary);
			}
			output.flush();
		} finally {
			IOUtils.closeQuietly(output);
			IOUtils.closeQuietly(input);
		}
	}
	/**
	 *文件共享提交 
	 * @param entity
	 * @return
	 */
    @RequestMapping("/client/dropBox/submitShare.action")
    @ResponseBody
    public ResultBean submitShare(FileDirectoryEntity entity){
        return dropBoxService.submitShare(entity);
    }
    /**
     * 资源 管理列表检索
     * @param entity
     * @return
     */
    @RequestMapping("/pub/dropBox/searchResourceList.action")
    @ResponseBody
    public ResultBean searchResourceList(FileDirectoryEntity entity,String searchType,String sortType,
            Integer pageSize, Integer pageNumber,String directionTagId, String categoryTagId){
        return dropBoxService.searchResourceList(entity,searchType,sortType, pageSize,pageNumber,directionTagId,categoryTagId);
    }
}
