

package com.cloudsoaring.web.dropbox.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;
import com.cloudsoaring.web.common.entity.ITreeItem;

/**   
 * @Title: Entity
 * @Description: tb_directory
 * @date 2016-04-06
 * @version V1.0
 *
 */
public class DirectoryEntity extends BaseEntity implements ITreeItem {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**目录id*/
	private String directoryId;
	/**父级目录id*/
	private String parentId;
	/**用户id*/
	private String userId;
	/**目录名称*/
	private String directoryName;
	/**根目录*/
	private String flagRoot;
	
	/**目录id*/
	public String getDirectoryId(){
		return this.directoryId;
	}
	/**目录id*/
	public void setDirectoryId(String directoryId){
		this.directoryId = directoryId;
	}
	/**父级目录id*/
	public String getParentId(){
		return this.parentId;
	}
	/**父级目录id*/
	public void setParentId(String parentId){
		this.parentId = parentId;
	}
	/**用户id*/
	public String getUserId(){
		return this.userId;
	}
	/**用户id*/
	public void setUserId(String userId){
		this.userId = userId;
	}
	/**目录名称*/
	public String getDirectoryName(){
		return this.directoryName;
	}
	/**目录名称*/
	public void setDirectoryName(String directoryName){
		this.directoryName = directoryName;
	}
	/**根目录*/
	public String getFlagRoot(){
		return this.flagRoot;
	}
	/**根目录*/
	public void setFlagRoot(String flagRoot){
		this.flagRoot = flagRoot;
	}
	@Override
	public String getTreeId() {
		// TODO Auto-generated method stub
		return this.directoryId;
	}
	@Override
	public String getTreeParentId() {
		// TODO Auto-generated method stub
		return parentId;
	}
	@Override
	public String getTreeName() {
		// TODO Auto-generated method stub
		return directoryName;
	}
	@Override
	public int getTreeOrder() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public boolean disabled() {
		// TODO Auto-generated method stub
		return false;
	}
}
