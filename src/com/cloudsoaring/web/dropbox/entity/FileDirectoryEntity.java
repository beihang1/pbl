

package com.cloudsoaring.web.dropbox.entity;


import java.util.Date;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_file_directory
 * @date 2016-04-06
 * @version V1.0
 *
 */
public class FileDirectoryEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**文件id*/
	private String fileId;
	/**目录id*/
	private String directoryId;
	/**目录id*/
    private String directoryId1;
	/**文件名*/
	private String fileName;
	 /**分享标记*/
    private String flagShare;
    /**状态（用于资源管理：1【审批通过】，2【撤销】）*/
    private String stateShare;
    /**备注*/
    private String markShare;
    /**分享分类：来源课程分类*/
    private String typeShare;
   /**共享时间*/
    private Date timeShare;
   /**NUM_DOWLOAD下载量*/
    private Integer numDowload;
    
    private String tagFirstId;
    
    private String tagSecondId;
    /**资源大小*/
    private String fileSize;
    
    private String contentType;
    
    private String fileNameTf;
    private String pictureId;
    /**资源简介*/
    private String briefInTroduction;
    /**上传人员*/
    private String userId;
    private Date startDate;
    private Date endDate;
    public String getPictureId(){
        return this.pictureId;
    }
    public void setPictureId(String pictureId){
        this.pictureId = pictureId;
    }
    
    public String getBriefInTroduction() {
		return briefInTroduction;
	}
	public void setBriefInTroduction(String briefInTroduction) {
		this.briefInTroduction = briefInTroduction;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getFileNameTf() {
        return fileNameTf;
    }
    public void setFileNameTf(String fileNameTf) {
        this.fileNameTf = fileNameTf;
    }
    public String getContentType() {
        return contentType;
    }
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
    public String getFileSize() {
        return fileSize;
    }
    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }
    public String getDirectoryId1() {
        return directoryId1;
    }
    public void setDirectoryId1(String directoryId1) {
        this.directoryId1 = directoryId1;
    }
    public String getTagFirstId() {
        return tagFirstId;
    }
    public void setTagFirstId(String tagFirstId) {
        this.tagFirstId = tagFirstId;
    }
    public String getTagSecondId() {
        return tagSecondId;
    }
    public void setTagSecondId(String tagSecondId) {
        this.tagSecondId = tagSecondId;
    }
    public Date getTimeShare() {
        return timeShare;
    }
    public void setTimeShare(Date timeShare) {
        this.timeShare = timeShare;
    }
    public Integer getNumDowload() {
        return numDowload;
    }
    public void setNumDowload(Integer numDowload) {
        this.numDowload = numDowload;
    }
    public String getFlagShare() {
        return flagShare;
    }
    public void setFlagShare(String flagShare) {
        this.flagShare = flagShare;
    }
    public String getStateShare() {
        return stateShare;
    }
    public void setStateShare(String stateShare) {
        this.stateShare = stateShare;
    }
    public String getMarkShare() {
        return markShare;
    }
    public void setMarkShare(String markShare) {
        this.markShare = markShare;
    }
    public String getTypeShare() {
        return typeShare;
    }
    public void setTypeShare(String typeShare) {
        this.typeShare = typeShare;
    }
    /**文件id*/
	public String getFileId(){
		return this.fileId;
	}
	/**文件id*/
	public void setFileId(String fileId){
		this.fileId = fileId;
	}
	/**目录id*/
	public String getDirectoryId(){
		return this.directoryId;
	}
	/**目录id*/
	public void setDirectoryId(String directoryId){
		this.directoryId = directoryId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
