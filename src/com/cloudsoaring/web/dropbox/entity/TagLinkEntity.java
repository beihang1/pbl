

package com.cloudsoaring.web.dropbox.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_tag_link
 * @date 2016-03-01
 * @version V1.0
 *
 */
public class TagLinkEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**LINK_ID*/
	private String linkId;
	/**TAG_ID*/
	private String tagId;
	/**PARENT_TAG_ID*/
	private String parentTagId;
	/**LINK_TYPE*/
	private String linkType;
	/**LINK_ID*/
	public String getLinkId(){
		return this.linkId;
	}
	/**LINK_ID*/
	public void setLinkId(String linkId){
		this.linkId = linkId;
	}
	/**TAG_ID*/
	public String getTagId(){
		return this.tagId;
	}
	/**TAG_ID*/
	public void setTagId(String tagId){
		this.tagId = tagId;
	}
	/**PARENT_TAG_ID*/
	public String getParentTagId(){
		return this.parentTagId;
	}
	/**PARENT_TAG_ID*/
	public void setParentTagId(String parentTagId){
		this.parentTagId = parentTagId;
	}
    public String getLinkType() {
        return linkType;
    }
    public void setLinkType(String linkType) {
        this.linkType = linkType;
    }
	
	
}
