package com.cloudsoaring.web.vote.constant;

public interface VoteConstants extends VoteConfigNames, VoteDbConstants, VoteLogConstants, VoteDefaultValues, VoteMsgNames, VoteStateConstants{
  
}
