

package com.cloudsoaring.web.grade.entity;


import java.util.Date;
import java.util.List;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_class
 *
 */
public class GradeEntity extends BaseEntity{
	
	/**serialVersionUID*/
	private static final long serialVersionUID = 1L;
	
	/**班级编号*/
	private String classNo;
	/**班级名称*/
	private String className;
	/**班级备注*/
	private String classDesc;
	/**创建时间*/
	private Date createTime;
	/**创建人员*/
	private String createUser;
	/**更新时间*/
	private Date updateTime;
	/**更新人员*/
	private String updateUser;
	/**所属学院*/
	private String affiliatedCollege;
	/**人员工号*/
	private String userId;
	public String getClassNo() {
		return classNo;
	}
	public void setClassNo(String classNo) {
		this.classNo = classNo;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getClassDesc() {
		return classDesc;
	}
	public void setClassDesc(String classDesc) {
		this.classDesc = classDesc;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getAffiliatedCollege() {
		return affiliatedCollege;
	}
	public void setAffiliatedCollege(String affiliatedCollege) {
		this.affiliatedCollege = affiliatedCollege;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
}
