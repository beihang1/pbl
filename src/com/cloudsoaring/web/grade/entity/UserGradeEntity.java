

package com.cloudsoaring.web.grade.entity;


import java.util.Date;
import java.util.List;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_user_class
 */
public class UserGradeEntity extends BaseEntity{
	
	/**serialVersionUID*/
	private static final long serialVersionUID = 1L;
	
	/**用户ID*/
	private String userId;
	/**班级ID*/
	private String classId;
	/**创建时间*/
	private Date createTime;
	/**创建人员*/
	private String createUser;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getClassId() {
		return classId;
	}
	public void setClassId(String classId) {
		this.classId = classId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	
}
