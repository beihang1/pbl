package com.cloudsoaring.web.grade.service;

import java.util.Date;
import org.springframework.stereotype.Service;
import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.exception.BusinessException;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.view.UserView;
import com.cloudsoaring.web.grade.entity.GradeEntity;
import com.cloudsoaring.web.grade.entity.UserGradeEntity;
import com.cloudsoaring.web.trainingplatform.constants.TpConstants;

/***
 * 班级管理相关的业务处理Service
 */
@Service
@SuppressWarnings("unchecked")
public class GradeService extends FileService implements TpConstants{
    /**
                    * 分页查询年级信息
     * @param entity
     * @return
     */
	public ResultBean searchGradeList(GradeEntity entity){
		return commonDao.searchList4Page(entity, GRADE_SEARCH);
	}
	/**
	 * 保存年级信息
	 * @param entity
	 * @throws Exception
	 */
	public void insertGrade(GradeEntity entity) throws Exception{
		//随机生成主键id
		entity.setId(IDGenerator.genUID());
		//创建时间
		entity.setCreateTime(new Date());
		//创建者
		entity.setCreateUser(WebContext.getSessionUserId());
		this.commonDao.insertData(entity,GRADE_ADD);
	}
	/**
	 * 根据年级id查询年级信息
	 * @param id
	 * @return
	 */
	public GradeEntity selectGradeById(String id) {
		GradeEntity entity = new GradeEntity();
		entity.setId(id);
		return commonDao.searchOneData(entity, GRADE_SEARCH_BY_ID);
	}
	/**
	 * 更新年级信息
	 * @param entity
	 */
	public void updateGrade(GradeEntity entity) {
		//设置更新人员
		entity.setUpdateUser(WebContext.getSessionUserId());
		//更新时间
		entity.setUpdateTime(new Date());
		this.commonDao.updateData(entity, GRADE_UPDATE_BY_ID);
	}
	/**
	 * 删除年级信息
	 * @param entity
	 */
	public void deleteGradeById(GradeEntity entity) {
		this.commonDao.deleteData(entity, GRADE_DELETE_BY_ID);
	}
	/**
	 * 添加学员
	 * @param courseId 班级id
	 * @param userIds  添加用户的id集合
	 */
	public void saveUser(String courseId, String... userIds) {
		if (userIds != null) {
			UserGradeEntity user = new UserGradeEntity();
			//循环遍历用户id
			for (String userId : userIds) {
				//用户id和课程id不能为空
				if (StringUtil.isNotEmpty(userId) && StringUtil.isNotEmpty(courseId)) {
					user.setClassId(courseId);
					user.setUserId(userId);
					user.setCreateTime(new Date());
					user.setCreateUser(WebContext.getSessionUserId());
					//保存
					commonDao.insertData(user, GRADE_ADD_STUDENTS);
				} else {
					throw new BusinessException("学生添加失败");
				}
			}
		}
	}
	/**
	 * 查询学员信息
	 * @param query
	 * @return
	 */
	public ResultBean searchUser(UserView query) {
		return commonDao.searchList4Page(query, GRADE_SEARCH_STUDENTS);
	}
	/**
	 * 删除学员
	 * @param userId   学员id
	 * @param courseId 班级id
	 */
	public void deleteStudent(String userId, String courseId) {
		//拆分学员id
		String[] userIds = StringUtil.split(userId, ",");
		if (userIds != null) {
			//遍历学员id
			for (String id : userIds) {
				if (StringUtil.isNotEmpty(id)) {
					UserGradeEntity user = new UserGradeEntity();
					user.setUserId(id);
					user.setClassId(courseId);
					//删除
					commonDao.deleteData(user, GRADE_DELETE_STUDENTS);
				}
			}
		}
	}
	/**
	 * 查询学员信息
	 * @param courseId
	 * @param userIds
	 * @return
	 */
	public String searchUserUnique(String courseId, String... userIds) {
		String userName = "";
		if (userIds != null) {
			UserGradeEntity user = new UserGradeEntity();
			for (String userId : userIds) {
				if (StringUtil.isNotEmpty(userId) && StringUtil.isNotEmpty(courseId)) {
					//班级id
					user.setClassId(courseId);
					user.setUserId(userId);
					//常见时间
					user.setCreateTime(new Date());
					user.setCreateUser(WebContext.getSessionUserId());
					//查询年级信息获取学员名称
					UserView view = commonDao.searchOneData(user, GRADE_UNIQUE_STUDENTS);
					if(view != null) {
						userName += view.getPersonName()+",";
					}
				} 
			}
		}
		return userName;
	}
    /**
     * 
     * @param entity
     * @return
     */
	public GradeEntity selectClassInfo(GradeEntity entity) {
		return commonDao.searchOneData(entity, USER_GRADE_SEARCH_BY_ID);
	}
}
