package com.cloudsoaring.web.grade.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.view.TagView;
import com.cloudsoaring.web.course.view.UserView;
import com.cloudsoaring.web.grade.entity.GradeEntity;
import com.cloudsoaring.web.grade.service.GradeService;
import com.cloudsoaring.web.trainingplatform.constants.TpConstants;

/**
 * 后台——班级管理
 */
@Controller
@RequestMapping("/manager/grade")
public class GradeController extends BaseController implements TpConstants{

	 @Autowired
	 private GradeService gradeService;
	 
	    /**
	     * 初始化班级列表
	     */
	 	@RequestMapping("/gradelist.action")
		public ModelAndView gradelist(){
			ModelAndView mv = new ModelAndView("grade/manager/grade_list");
			return mv;
		}
	 	
	 	/**
		 * 检索班级信息
		 * @param entity
		 * @return 返回结果
		 */
		@RequestMapping("/searchGrade.action")
		@ResponseBody
		public ResultBean searchNotice(GradeEntity entity){
			return gradeService.searchGradeList(entity);
		}
		
		/**
		 * 跳转到班级添加页面
		 * @return 返回结果
		 */
		
		@RequestMapping("/gradeadd.action")
		public ModelAndView gradeAdd(TagView view){
			ModelAndView mv = new ModelAndView("grade/manager/grade_add");
			return mv;
		}
		
		/**
		 * 新增班级信息
		 * @param entity
		 * @return 返回结果
		 */
		@RequestMapping("/insertGrade.action")
		@ResponseBody
		public ResultBean insertGrade(GradeEntity entity) throws Exception {
			gradeService.insertGrade(entity);
			return ResultBean.success(MSG_ADD_GRADE_SUCCESS);
		}
		
		/**
		 * 跳转到班级编辑页面
		 * @param id
		 * @return 返回结果
		 */
		@RequestMapping("/gradeEdit.action")
		public ModelAndView gradeEdit(String id){
			GradeEntity entity = gradeService.selectGradeById(id);
			ModelAndView mv = new ModelAndView("grade/manager/grade_edit");
			mv.addObject("grade", entity);
			return mv;
		}
		
		/**
		 * 编辑保存
		 * @param entity
		 * @return 返回结果
		 */
		@RequestMapping("/updateGrade.action")
		@ResponseBody
		public ResultBean updateGrade(GradeEntity entity) throws Exception{
			gradeService.updateGrade(entity);
			return ResultBean.success(EDIT_GRADE_SUCCESS);
		}
		
		/**
		 * 删除班级
		 * @param entity
		 * @return 返回结果
		 */
		@RequestMapping("/deleteGradeById.action")
		@ResponseBody
		public ResultBean deleteGradeById(GradeEntity entity)throws Exception{
			gradeService.deleteGradeById(entity);
			return ResultBean.success(DELETE_GRADE_SUCCESS);
		}
		
		/**
	     * 班级管理-学生管理页面初始化
	     */
	    @RequestMapping("/searchUserInit.action")
	    @ResponseBody
	    public ModelAndView searchUserInit(String id) {
	    	 ModelAndView result = new ModelAndView("grade/manager/grade_user_join");
	    	 result.addObject("id", id);
	         return result;
	    }
	    
	    /**
	     * 班级管理-添加学生
	     */
	    @RequestMapping("/saveUser.action")
	    @ResponseBody
	    public ResultBean saveUser(String courseId, String userIds)  {
	        ResultBean result = new ResultBean();
	        gradeService.saveUser(courseId,StringUtil.split(StringUtil.getOrElse(userIds), ","));
	        result.setMessages(getMessage(MSG_S_SAVE));
	        return result;
	    }
	    
	    /**
	     * 班级管理-查询学生
	     * @param id
	     * @return
	     */
	     @RequestMapping("/searchUser.action")
	     @ResponseBody
	     public ResultBean searchUser(UserView query)  {
	         return gradeService.searchUser(query);
	     }
	     
	     /**
		     * 班级管理-删除学生
	     * @param id
	     * @return
	     */
	     @RequestMapping("/deleteStudent.action")
	     @ResponseBody
	     public ResultBean deleteStudent(String userId,String courseId){
	    	 //设置返回对象
	         ResultBean result = new ResultBean();
	         gradeService.deleteStudent(userId,courseId);
	         result.setMessages(getMessage("删除成功"));
	         return result;
	     }
	     
	     /**
		     * 班级管理-检查是否有重复学生添加
	     * @param id
	     * @return
	     */
	     @RequestMapping("/searchUserUnique.action")
	     @ResponseBody
	     public ResultBean searchUserUnique(String courseId, String userIds)  {
	    	 ResultBean result = new ResultBean();
	         String resultMsg = gradeService.searchUserUnique(courseId,StringUtil.split(StringUtil.getOrElse(userIds), ","));
	         if(!"".equals(resultMsg) && resultMsg != null) {
	        	 result.setMessages(resultMsg+"已在班级中！");
	         }
	         return result;
	     }
	     
}
