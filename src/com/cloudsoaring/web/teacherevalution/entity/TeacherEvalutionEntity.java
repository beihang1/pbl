package com.cloudsoaring.web.teacherevalution.entity;

import java.util.Date;
import java.util.List;

import com.cloudsoaring.common.annotation.ClassDefine;
import com.cloudsoaring.common.annotation.FieldDefine;
import com.cloudsoaring.common.validate.FieldType;
import com.cloudsoaring.web.common.entity.BaseEntity;
@ClassDefine
public class TeacherEvalutionEntity extends BaseEntity{
	
	/**serialVersionUID*/
	private static final long serialVersionUID = 1L;
	private String userId;
	private String userName;
	private String pictureId;//个人照片
	private String pictureName;
	private String companyId;//所属单位
	@FieldDefine(description="姓名",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String personName;//姓名
	@FieldDefine(description="性别",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String sex;//性别
	@FieldDefine(description="身份证号",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String idCard;//身份证号
	@FieldDefine(description="出生年月",maxLength="100",columnIndex="D",rowIndex="1" ,required=true, type=FieldType.Date)
	private Date birthday;//出生年月
	@FieldDefine(description="手机号",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String telephone;//手机号
	@FieldDefine(description="邮件",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String email;//邮件
	@FieldDefine(description="政治面貌",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String politicsStaus;//政治面貌
	@FieldDefine(description="婚姻状况",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String maritalStatus;//婚姻状况
	@FieldDefine(description="学历",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String education;//学历
	@FieldDefine(description="学位",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String degreeOfFirstDegree;//第一学历:学位
	@FieldDefine(description="毕业院校",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String schoolOfFirstDegree;//毕业院校
	@FieldDefine(description="所学专业",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String majorOfFirstDegree;//所学专业
	@FieldDefine(description="学习方式",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String styleOfFirstDegree;//学习方式
	@FieldDefine(description="毕业时间",maxLength="100",columnIndex="D",rowIndex="1" ,required=true, type=FieldType.Date)
	private Date graduateTimeOfFirstDegree;//毕业时间
	@FieldDefine(description="最高学历",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String degreeOfHighestDegree;//最高学历：学位
	@FieldDefine(description="毕业院校",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String schoolOfHighestDegree;//毕业院校
	@FieldDefine(description="所学专业",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String majorOfHighestDegree;//所学专业
	@FieldDefine(description="学习方式",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String styleOfHighestDegree;//学习方式
	@FieldDefine(description="毕业时间",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private Date graduateTimeOfHighestDegree;//毕业时间
	@FieldDefine(description="职务",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String duty;//职务
	@FieldDefine(description="职称",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String jobTitle;//职称
	@FieldDefine(description="参加工作时间",maxLength="100",columnIndex="D",rowIndex="1" ,required=true, type=FieldType.Date)
	private Date workTime;//参加工作时间
	@FieldDefine(description="工龄",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String workAge;//工龄
	@FieldDefine(description="教育工作时间",maxLength="100",columnIndex="D",rowIndex="1" ,required=true, type=FieldType.Date)
	private Date educationWorkTime;//教育工作时间
	@FieldDefine(description="教龄",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
	private String schoolAge;//教龄
	private Date createTime;
	private String createUser;
	private Date updateTime;
	private String updateUser;
	private String status;
	private List<TeacherEvalutionEntity> teacherList;
	
	private String createStartDate;
	private String  createEndDate;   
	
	public String getCreateStartDate() {
		return createStartDate;
	}
	public void setCreateStartDate(String createStartDate) {
		this.createStartDate = createStartDate;
	}
	public String getCreateEndDate() {
		return createEndDate;
	}
	public void setCreateEndDate(String createEndDate) {
		this.createEndDate = createEndDate;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPictureId() {
		return pictureId;
	}
	public void setPictureId(String pictureId) {
		this.pictureId = pictureId;
	}
	
	public String getPictureName() {
		return pictureName;
	}
	public void setPictureName(String pictureName) {
		this.pictureName = pictureName;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getIdCard() {
		return idCard;
	}
	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPoliticsStaus() {
		return politicsStaus;
	}
	public void setPoliticsStaus(String politicsStaus) {
		this.politicsStaus = politicsStaus;
	}
	public String getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public String getEducation() {
		return education;
	}
	public void setEducation(String education) {
		this.education = education;
	}
	public String getDegreeOfFirstDegree() {
		return degreeOfFirstDegree;
	}
	public void setDegreeOfFirstDegree(String degreeOfFirstDegree) {
		this.degreeOfFirstDegree = degreeOfFirstDegree;
	}
	public String getSchoolOfFirstDegree() {
		return schoolOfFirstDegree;
	}
	public void setSchoolOfFirstDegree(String schoolOfFirstDegree) {
		this.schoolOfFirstDegree = schoolOfFirstDegree;
	}
	public String getMajorOfFirstDegree() {
		return majorOfFirstDegree;
	}
	public void setMajorOfFirstDegree(String majorOfFirstDegree) {
		this.majorOfFirstDegree = majorOfFirstDegree;
	}
	public String getStyleOfFirstDegree() {
		return styleOfFirstDegree;
	}
	public void setStyleOfFirstDegree(String styleOfFirstDegree) {
		this.styleOfFirstDegree = styleOfFirstDegree;
	}
	public Date getGraduateTimeOfFirstDegree() {
		return graduateTimeOfFirstDegree;
	}
	public void setGraduateTimeOfFirstDegree(Date graduateTimeOfFirstDegree) {
		this.graduateTimeOfFirstDegree = graduateTimeOfFirstDegree;
	}
	public String getDegreeOfHighestDegree() {
		return degreeOfHighestDegree;
	}
	public void setDegreeOfHighestDegree(String degreeOfHighestDegree) {
		this.degreeOfHighestDegree = degreeOfHighestDegree;
	}
	public String getSchoolOfHighestDegree() {
		return schoolOfHighestDegree;
	}
	public void setSchoolOfHighestDegree(String schoolOfHighestDegree) {
		this.schoolOfHighestDegree = schoolOfHighestDegree;
	}
	public String getMajorOfHighestDegree() {
		return majorOfHighestDegree;
	}
	public void setMajorOfHighestDegree(String majorOfHighestDegree) {
		this.majorOfHighestDegree = majorOfHighestDegree;
	}
	public String getStyleOfHighestDegree() {
		return styleOfHighestDegree;
	}
	public void setStyleOfHighestDegree(String styleOfHighestDegree) {
		this.styleOfHighestDegree = styleOfHighestDegree;
	}
	public Date getGraduateTimeOfHighestDegree() {
		return graduateTimeOfHighestDegree;
	}
	public void setGraduateTimeOfHighestDegree(Date graduateTimeOfHighestDegree) {
		this.graduateTimeOfHighestDegree = graduateTimeOfHighestDegree;
	}
	public String getDuty() {
		return duty;
	}
	public void setDuty(String duty) {
		this.duty = duty;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	public Date getWorkTime() {
		return workTime;
	}
	public void setWorkTime(Date workTime) {
		this.workTime = workTime;
	}
	public String getWorkAge() {
		return workAge;
	}
	public void setWorkAge(String workAge) {
		this.workAge = workAge;
	}
	public Date getEducationWorkTime() {
		return educationWorkTime;
	}
	public void setEducationWorkTime(Date educationWorkTime) {
		this.educationWorkTime = educationWorkTime;
	}
	public String getSchoolAge() {
		return schoolAge;
	}
	public void setSchoolAge(String schoolAge) {
		this.schoolAge = schoolAge;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getCreateUser() {
		return createUser;
	}
	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}
	public Date getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
	public String getUpdateUser() {
		return updateUser;
	}
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public List<TeacherEvalutionEntity> getTeacherList() {
		return teacherList;
	}
	public void setTeacherList(List<TeacherEvalutionEntity> teacherList) {
		this.teacherList = teacherList;
	}
	
	
	
}
