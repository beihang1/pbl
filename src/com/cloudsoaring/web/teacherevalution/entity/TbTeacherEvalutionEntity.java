

package com.cloudsoaring.web.teacherevalution.entity;


import java.util.Date;
import java.util.List;

import com.cloudsoaring.common.annotation.ClassDefine;
import com.cloudsoaring.common.annotation.FieldDefine;
import com.cloudsoaring.common.validate.FieldType;
import com.cloudsoaring.web.common.entity.BaseEntity;
import com.cloudsoaring.web.course.entity.QuestionEntity;
import com.cloudsoaring.web.course.view.InstitutionManagerView;
import com.cloudsoaring.web.taxonomy.entity.TagInstitutionEntityView;

/**   
 * @Title: Entity
 * @Description: tb_course
 * @date 2016-03-01
 * @version V1.0
 *
 */
@ClassDefine
public class TbTeacherEvalutionEntity extends BaseEntity{
	 /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;
    private  String id;
    /**课程名称*/
    @FieldDefine(description="课程名称",maxLength="100",columnIndex="B",rowIndex="1" ,required=true, type=FieldType.String)
    private String title;
    /**封面*/
    @FieldDefine(description="封面",maxLength="100",columnIndex="D",rowIndex="2" ,required=true, type=FieldType.String)
    private String pictureId;
    /**封面名称*/
    private String pictureName;
    /**是否免费（0：收费*/
    private java.math.BigDecimal freeFlag;
    /**价格*/
    @FieldDefine(description="价格",maxLength="12",columnIndex="B",rowIndex="6" ,required=true, type=FieldType.Numeric)
    private java.math.BigDecimal price;
    /**教师ID*/
    @FieldDefine(description="教师",maxLength="50",columnIndex="B",rowIndex="9" ,required=true, type=FieldType.String)
    private String teacherId;
    /**课程描述*/
    @FieldDefine(description="课程描述",maxLength="500",columnIndex="B",rowIndex="10" , type=FieldType.String)
    private String content;
    /**课程详情*/
    @FieldDefine(description="课程详情",maxLength="500",columnIndex="B",rowIndex="10" , type=FieldType.String)
	private String courseDetail;
    /**好评百分比*/
    private java.math.BigDecimal voteGood;
    /**中评百分比*/
    private java.math.BigDecimal voteNormal;
    /**课程状态(1:连载中,2:已完成,3:已结束)*/
    @FieldDefine(description="课程状态",maxLength="10",columnIndex="B",rowIndex="5" ,required=true, type=FieldType.String)
    private String courseStatus;
    /**开课时间起始*/
    @FieldDefine(description="开课时间起始",columnIndex="D",rowIndex="3" ,required=true, type=FieldType.Date)
    private Date startCourseDate;
    private String startDate;
    /**开课时间结束*/
    @FieldDefine(description="开课时间结束",columnIndex="D",rowIndex="4" ,required=true, type=FieldType.Date)
    private Date endCourseDate;
    private String endDate;
    /**学习时长*/
    @FieldDefine(description="学习时长",maxLength="50",columnIndex="B",rowIndex="2" ,required=true, type=FieldType.String)
    private String studyDuration;
    /**备注*/
    @FieldDefine(description="封面简介",maxLength="400",columnIndex="B",rowIndex="3" ,required=true, type=FieldType.String)
    private String summary;
    /**状态（0：未发布，1：已发布）*/
    private String status;
    /**测验类型(1:测试，2:考试)*/
    private String measureType;
    /**考试开始时间*/
    private java.util.Date effectiveStartDate;
    /**考试结束时间*/
    private java.util.Date effectiveEndDate;
    /**考试时长(分钟)*/
    private java.math.BigDecimal timeDuration;
    /**视频*/
    private String videoId;
    /**课程须知*/
    @FieldDefine(description="课程须知",maxLength="500",columnIndex="B",rowIndex="4" , type=FieldType.String)
    private String notice;
    /**课程目标-导师提示*/
    @FieldDefine(description="导师提示",maxLength="500",columnIndex="D",rowIndex="9" , type=FieldType.String)
    private String target;
    /**课程范围*/
    @FieldDefine(description="课程类别",maxLength="10",columnIndex="D",rowIndex="1" ,required=true,type=FieldType.String)
    private String crowdType;
    /**是否必修*/
    @FieldDefine(description="是否必修",maxLength="10",columnIndex="B",rowIndex="7" ,required=true,type=FieldType.String)
    private String flagRequired;
    /**是否可拖动进度*/
    @FieldDefine(description="是否可拖动进度",maxLength="10",columnIndex="B",rowIndex="8" ,required=true,type=FieldType.String)
    private String flagSkipVideo;
    /**是否开启评论*/
    private String flagDiscuss;
    /**是否开启问答*/
    private String flagQa;
    /**是否开启投票*/
    private String flagVote;
    /**是否开启笔记*/
    private String flagNote;
    /**是否开启案例*/
    private String flagShare;
    /**是否开启问卷*/
    private String flagQuestionnaire;
    /**是否按照顺序学习*/
    @FieldDefine(description="是否按照顺序学习",maxLength="10",columnIndex="D",rowIndex="6" ,type=FieldType.String)
    private String flagOrderStudy;
    /**学习人数*/
    private java.math.BigDecimal numStudy;
    /**关注人数*/
    private java.math.BigDecimal numFavorite;
    /**浏览数*/
    private java.math.BigDecimal numView;
    /**评价人数*/
    private java.math.BigDecimal numScoreUser;
    /**总票数*/
    private java.math.BigDecimal totalVote;
    /**TAG_ID*/
    private String tagId;
    /**LINK_ID*/
    private String linkId;
    /**用户检索条件，不要删*/
    private String conditionUserId;
    /**用来显示用户是否已经学习*/
    private String sessionUserStudy;
    private List<String> tagIds;
    /**是否精品课程*/
    private String flagBoutique;
    /**是否首页轮播*/
    private String flagHomeCarousel;
    /**教师名称*/
    private String personName;
    /**一级标签*/
    @FieldDefine(description="一级标签",maxLength="10",columnIndex="D",rowIndex="7" ,required=true,type=FieldType.String)
    private String firstTag;
    
    private String firstTagName;
    /**二级标签*/
    @FieldDefine(description="一级标签",maxLength="10",columnIndex="D",rowIndex="8" ,required=true,type=FieldType.String)
    private String secondTag;
    
    private String secondTagName;
    /**活动标签*/
    private String activitys;
    /**问题列表*/
    private List<QuestionEntity> questionList;
    /**分类字段*/
    private String[] catagorys;
    /**课程标签ID*/
    private String categoryId;
    
    private String courseNo;
    
    //课程分类
    private List<TagInstitutionEntityView> tagList;
    private   String[] tagSecondList;
    private String[] tagFirstList;
    private InstitutionManagerView institution;
    private String institutionName;
    private String schoolType;
    private String institutionAbbreviation;
    private String measureId;
    
     
//    private List<ChapterView> chapterList;
//    //大章节
//    private List<ChapterView> chapterBigList;
//   //小章节
//    private List<ChapterView> chapterSmallList;
    /**阶段设置*/
    @FieldDefine(description="阶段设置",maxLength="10",columnIndex="D",rowIndex="5" ,type=FieldType.String)
    private String courseStage;

    private String parentTagId;

    /**是否开启作业*/
    private String flagTask;
   /**是否开启测试*/
    private String flagTest;
    /**是否开启问卷*/
    private String flagWj;
   /**创建开始时间*/
    private String createStartDate;
    /**创建结束时间*/
    private String  createEndDate;    
    /**标签类型*/
    private String  linkType;
    
    /**是否有考试章节*/
    private String isExam;
    private String userId;
    /**课程来源*/
    private String courseRource;
    /**供应商*/
    private String supplier;
    /**推荐度NUM_RECOMMEND*/
    private java.math.BigDecimal numRecommend;
    private String sortRecommend;
	private String companyId;
	private String telephone;
    
    public String getSortRecommend() {
        return sortRecommend;
    }
    public void setSortRecommend(String sortRecommend) {
        this.sortRecommend = sortRecommend;
    }
    public java.math.BigDecimal getNumRecommend() {
        return numRecommend;
    }
    public void setNumRecommend(java.math.BigDecimal numRecommend) {
        this.numRecommend = numRecommend;
    }
    public String getCourseRource() {
        return courseRource;
    }
    public void setCourseRource(String courseRource) {
        this.courseRource = courseRource;
    }
    public String getSupplier() {
        return supplier;
    }
    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }
    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getIsExam() {
        return isExam;
    }
    public void setIsExam(String isExam) {
        this.isExam = isExam;
    }
    public Date getStartCourseDate() {
        return startCourseDate;
    }
    public void setStartCourseDate(Date startCourseDate) {
        this.startCourseDate = startCourseDate;
    }
    public Date getEndCourseDate() {
        return endCourseDate;
    }
    public void setEndCourseDate(Date endCourseDate) {
        this.endCourseDate = endCourseDate;
    }
    public String getCreateStartDate() {
        return createStartDate;
    }
    public void setCreateStartDate(String createStartDate) {
        this.createStartDate = createStartDate;
    }
    public String getCreateEndDate() {
        return createEndDate;
    }
    public void setCreateEndDate(String createEndDate) {
        this.createEndDate = createEndDate;
    }
    public String getFirstTagName() {
        return firstTagName;
    }
    public void setFirstTagName(String firstTagName) {
        this.firstTagName = firstTagName;
    }
  
    public String getSecondTag() {
        return secondTag;
    }
    public void setSecondTag(String secondTag) {
        this.secondTag = secondTag;
    }
    public String getSecondTagName() {
        return secondTagName;
    }
    public void setSecondTagName(String secondTagName) {
        this.secondTagName = secondTagName;
    }
    public String getFlagTask() {
        return flagTask;
    }
    public String getFlagWj() {
            return flagWj;
    }
    public void setFlagWj(String flagWj) {
        this.flagWj = flagWj;
    }
    public void setFlagTask(String flagTask) {
        this.flagTask = flagTask;
    }
    public String getFlagTest() {
        return flagTest;
    }
    public void setFlagTest(String flagTest) {
        this.flagTest = flagTest;
    }
    public String getActivitys() {
        return activitys;
    }
    public void setActivitys(String activitys) {
        this.activitys = activitys;
    }
    public String[] getTagSecondList() {
        return tagSecondList;
    }
    public void setTagSecondList(String[] tagSecondList) {
        this.tagSecondList = tagSecondList;
    }
    public String[] getTagFirstList() {
        return tagFirstList;
    }
    public void setTagFirstList(String[] tagFirstList) {
        this.tagFirstList = tagFirstList;
    }
    public String getCourseStage() {
        return courseStage;
    }
    public void setCourseStage(String courseStage) {
        this.courseStage = courseStage;
    }
//    public List<ChapterView> getChapterBigList() {
//        return chapterBigList;
//    }
//    public void setChapterBigList(List<ChapterView> chapterBigList) {
//        this.chapterBigList = chapterBigList;
//    }
//    public List<ChapterView> getChapterSmallList() {
//        return chapterSmallList;
//    }
//    public void setChapterSmallList(List<ChapterView> chapterSmallList) {
//        this.chapterSmallList = chapterSmallList;
//    }
//    public List<ChapterView> getChapterList() {
//        return chapterList;
//    }
//    public void setChapterList(List<ChapterView> chapterList) {
//        this.chapterList = chapterList;
//    }
//    public TeacherView getTeacher() {
//        return teacher;
//    }
//    public void setTeacher(TeacherView teacher) {
//        this.teacher = teacher;
//    }
//    public List<TagView> getTagList() {
//        return tagList;
//    }
//    public void setTagList(List<TagView> tagList) {
//        this.tagList = tagList;
//    }
    public String getFirstTag() {
        return firstTag;
    }
    public void setFirstTag(String firstTag) {
        this.firstTag = firstTag;
    }
  
    public String getCourseNo() {
        return courseNo;
    }
    public void setCourseNo(String courseNo) {
        this.courseNo = courseNo;
    }
    public String getCategoryId() {
        return categoryId;
    }
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }
    public String[] getCatagorys() {
        return catagorys;
    }
    public void setCatagorys(String[] catagorys) {
        this.catagorys = catagorys;
    }
    public List<QuestionEntity> getQuestionList() {
        return questionList;
    }
    public void setQuestionList(List<QuestionEntity> questionList) {
        this.questionList = questionList;
    }
    public String getPictureName() {
        return pictureName;
    }
    public void setPictureName(String pictureName) {
        this.pictureName = pictureName;
    }
    public String getFlagBoutique() {
        return flagBoutique;
    }
    public void setFlagBoutique(String flagBoutique) {
        this.flagBoutique = flagBoutique;
    }
    public String getFlagHomeCarousel() {
        return flagHomeCarousel;
    }
    public void setFlagHomeCarousel(String flagHomeCarousel) {
        this.flagHomeCarousel = flagHomeCarousel;
    }
    public String getPersonName() {
        return personName;
    }
    public void setPersonName(String personName) {
        this.personName = personName;
    }
    
  
    public java.math.BigDecimal getTotalVote() {
        return totalVote;
    }
    public void setTotalVote(java.math.BigDecimal totalVote) {
        this.totalVote = totalVote;
    }
    /**课程名称*/
    public String getTitle(){
        return this.title;
    }
    /**课程名称*/
    public void setTitle(String title){
        this.title = title;
    }
    /**封面*/
    public String getPictureId(){
        return this.pictureId;
    }
    /**封面*/
    public void setPictureId(String pictureId){
        this.pictureId = pictureId;
    }
    /**是否免费（0：收费*/
    public java.math.BigDecimal getFreeFlag(){
        return this.freeFlag;
    }
    /**是否免费（0：收费*/
    public void setFreeFlag(java.math.BigDecimal freeFlag){
        this.freeFlag = freeFlag;
    }
    /**价格*/
    public java.math.BigDecimal getPrice(){
        return this.price;
    }
    /**价格*/
    public void setPrice(java.math.BigDecimal price){
        this.price = price;
    }
    /**教师ID*/
    public String getTeacherId(){
        return this.teacherId;
    }
    /**教师ID*/
    public void setTeacherId(String teacherId){
        this.teacherId = teacherId;
    }
    /**课程描述*/
    public String getContent(){
        return this.content;
    }
    /**课程描述*/
    public void setContent(String content){
        this.content = content;
    }
    public String getCourseDetail() {
		return courseDetail;
	}
	public void setCourseDetail(String courseDetail) {
		this.courseDetail = courseDetail;
	}
    /**好评百分比*/
    public java.math.BigDecimal getVoteGood(){
        return this.voteGood;
    }
    /**好评百分比*/
    public void setVoteGood(java.math.BigDecimal voteGood){
        this.voteGood = voteGood;
    }
    /**中评百分比*/
    public java.math.BigDecimal getVoteNormal(){
        return this.voteNormal;
    }
    /**中评百分比*/
    public void setVoteNormal(java.math.BigDecimal voteNormal){
        this.voteNormal = voteNormal;
    }
    /**课程状态(1:连载中,2:已完成,3:已结束)*/
    public String getCourseStatus(){
        return this.courseStatus;
    }
    /**课程状态(1:连载中,2:已完成,3:已结束)*/
    public void setCourseStatus(String courseStatus){
        this.courseStatus = courseStatus;
    }
    
    public String getStartDate() {
        return startDate;
    }
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
    public String getEndDate() {
        return endDate;
    }
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
    /**学习时长*/
    public String getStudyDuration(){
        return this.studyDuration;
    }
    /**学习时长*/
    public void setStudyDuration(String studyDuration){
        this.studyDuration = studyDuration;
    }
    /**备注*/
    public String getSummary(){
        return this.summary;
    }
    /**备注*/
    public void setSummary(String summary){
        this.summary = summary;
    }
    /**状态（0：未发布，1：已发布）*/
    public String getStatus(){
        return this.status;
    }
    /**状态（0：未发布，1：已发布）*/
    public void setStatus(String status){
        this.status = status;
    }
    /**测验类型(1:测试，2:考试)*/
    public String getMeasureType(){
        return this.measureType;
    }
    /**测验类型(1:测试，2:考试)*/
    public void setMeasureType(String measureType){
        this.measureType = measureType;
    }
    /**考试开始时间*/
    public java.util.Date getEffectiveStartDate(){
        return this.effectiveStartDate;
    }
    /**考试开始时间*/
    public void setEffectiveStartDate(java.util.Date effectiveStartDate){
        this.effectiveStartDate = effectiveStartDate;
    }
    /**考试结束时间*/
    public java.util.Date getEffectiveEndDate(){
        return this.effectiveEndDate;
    }
    /**考试结束时间*/
    public void setEffectiveEndDate(java.util.Date effectiveEndDate){
        this.effectiveEndDate = effectiveEndDate;
    }
    /**考试时长(分钟)*/
    public java.math.BigDecimal getTimeDuration(){
        return this.timeDuration;
    }
    /**考试时长(分钟)*/
    public void setTimeDuration(java.math.BigDecimal timeDuration){
        this.timeDuration = timeDuration;
    }
    /**视频*/
    public String getVideoId(){
        return this.videoId;
    }
    /**视频*/
    public void setVideoId(String videoId){
        this.videoId = videoId;
    }
    /**课程须知*/
    public String getNotice(){
        return this.notice;
    }
    /**课程须知*/
    public void setNotice(String notice){
        this.notice = notice;
    }
    /**课程目标*/
    public String getTarget(){
        return this.target;
    }
    /**课程目标*/
    public void setTarget(String target){
        this.target = target;
    }
    /**课程范围*/
    public String getCrowdType(){
        return this.crowdType;
    }
    /**课程范围*/
    public void setCrowdType(String crowdType){
        this.crowdType = crowdType;
    }
    /**是否必修*/
    public String getFlagRequired(){
        return this.flagRequired;
    }
    /**是否必修*/
    public void setFlagRequired(String flagRequired){
        this.flagRequired = flagRequired;
    }
    /**是否可拖动进度*/
    public String getFlagSkipVideo(){
        return this.flagSkipVideo;
    }
    /**是否可拖动进度*/
    public void setFlagSkipVideo(String flagSkipVideo){
        this.flagSkipVideo = flagSkipVideo;
    }
    /**是否开启评论*/
    public String getFlagDiscuss(){
        return this.flagDiscuss;
    }
    /**是否开启评论*/
    public void setFlagDiscuss(String flagDiscuss){
        this.flagDiscuss = flagDiscuss;
    }
    /**是否开启问答*/
    public String getFlagQa(){
        return this.flagQa;
    }
    /**是否开启问答*/
    public void setFlagQa(String flagQa){
        this.flagQa = flagQa;
    }
    /**是否开启投票*/
    public String getFlagVote(){
        return this.flagVote;
    }
    /**是否开启投票*/
    public void setFlagVote(String flagVote){
        this.flagVote = flagVote;
    }
    /**是否开启笔记*/
    public String getFlagNote(){
        return this.flagNote;
    }
    /**是否开启笔记*/
    public void setFlagNote(String flagNote){
        this.flagNote = flagNote;
    }
    /**是否开启案例*/
    public String getFlagShare(){
        return this.flagShare;
    }
    /**是否开启案例*/
    public void setFlagShare(String flagShare){
        this.flagShare = flagShare;
    }
    /**是否开启问卷*/
    public String getFlagQuestionnaire(){
        return this.flagQuestionnaire;
    }
    /**是否开启问卷*/
    public void setFlagQuestionnaire(String flagQuestionnaire){
        this.flagQuestionnaire = flagQuestionnaire;
    }
    /**是否按照顺序学习*/
    public String getFlagOrderStudy(){
        return this.flagOrderStudy;
    }
    /**是否按照顺序学习*/
    public void setFlagOrderStudy(String flagOrderStudy){
        this.flagOrderStudy = flagOrderStudy;
    }
    /**学习人数*/
    public java.math.BigDecimal getNumStudy(){
        return this.numStudy;
    }
    /**学习人数*/
    public void setNumStudy(java.math.BigDecimal numStudy){
        this.numStudy = numStudy;
    }
    /**关注人数*/
    public java.math.BigDecimal getNumFavorite(){
        return this.numFavorite;
    }
    /**关注人数*/
    public void setNumFavorite(java.math.BigDecimal numFavorite){
        this.numFavorite = numFavorite;
    }
    /**浏览数*/
    public java.math.BigDecimal getNumView(){
        return this.numView;
    }
    /**浏览数*/
    public void setNumView(java.math.BigDecimal numView){
        this.numView = numView;
    }
    /**评价人数*/
    public java.math.BigDecimal getNumScoreUser(){
        return this.numScoreUser;
    }
    /**评价人数*/
    public void setNumScoreUser(java.math.BigDecimal numScoreUser){
        this.numScoreUser = numScoreUser;
    }
    /**TAG_ID*/
    public String getTagId(){
        return this.tagId;
    }
    /**TAG_ID*/
    public void setTagId(String tagId){
        this.tagId = tagId;
    }
    /**LINK_ID*/
    public String getLinkId(){
        return this.linkId;
    }
    /**LINK_ID*/
    public void setLinkId(String linkId){
        this.linkId = linkId;
    }
    public String getConditionUserId() {
        return conditionUserId;
    }
    public void setConditionUserId(String conditionUserId) {
        this.conditionUserId = conditionUserId;
    }
    public String getSessionUserStudy() {
        return sessionUserStudy;
    }
    public void setSessionUserStudy(String sessionUserStudy) {
        this.sessionUserStudy = sessionUserStudy;
    }
    public List<String> getTagIds() {
        return tagIds;
    }
    public void setTagIds(List<String> tagIds) {
        this.tagIds = tagIds;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
	public String getParentTagId() {
		return parentTagId;
	}
	public void setParentTagId(String parentTagId) {
		this.parentTagId = parentTagId;
	}
	public String getLinkType() {
		return linkType;
	}
	public void setLinkType(String linkType) {
		this.linkType = linkType;
	}
	public String getMeasureId() {
		return measureId;
	}
	public void setMeasureId(String measureId) {
		this.measureId = measureId;
	}
	
	private String shareFlag;
	private String shareComment;
	private Date shareTime;
	private String shareUserId;
	public String getShareFlag() {
		return shareFlag;
	}
	public void setShareFlag(String shareFlag) {
		this.shareFlag = shareFlag;
	}
	public String getShareComment() {
		return shareComment;
	}
	public void setShareComment(String shareComment) {
		this.shareComment = shareComment;
	}
	public Date getShareTime() {
		return shareTime;
	}
	public void setShareTime(Date shareTime) {
		this.shareTime = shareTime;
	}
	public String getShareUserId() {
		return shareUserId;
	}
	public void setShareUserId(String shareUserId) {
		this.shareUserId = shareUserId;
	}
    
	private List<String> userCourseList;
	    
    public List<String> getUserCourseList() {
		return userCourseList;
	}
	public void setUserCourseList(List<String> userCourseList) {
		this.userCourseList = userCourseList;
	}
	public List<TagInstitutionEntityView> getTagList() {
		return tagList;
	}
	public void setTagList(List<TagInstitutionEntityView> tagList) {
		this.tagList = tagList;
	}
	public InstitutionManagerView getInstitution() {
		return institution;
	}
	public void setInstitution(InstitutionManagerView institution) {
		this.institution = institution;
	}
	public String getInstitutionName() {
		return institutionName;
	}
	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}
	public String getSchoolType() {
		return schoolType;
	}
	public void setSchoolType(String schoolType) {
		this.schoolType = schoolType;
	}
	public String getInstitutionAbbreviation() {
		return institutionAbbreviation;
	}
	public void setInstitutionAbbreviation(String institutionAbbreviation) {
		this.institutionAbbreviation = institutionAbbreviation;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	

	
	
}
