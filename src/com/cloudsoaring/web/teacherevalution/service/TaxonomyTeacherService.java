package com.cloudsoaring.web.teacherevalution.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.FileEntity;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseStateConstants;
import com.cloudsoaring.web.course.entity.UserMeasurementResultEntity;
import com.cloudsoaring.web.institution.entity.MeasurementInstitutionEntity;
import com.cloudsoaring.web.institution.entity.TagInstitutionEntity;
import com.cloudsoaring.web.taxonomy.constant.TaxonomyConstants;
import com.cloudsoaring.web.taxonomy.entity.TagInstitutionEntityView;
import com.cloudsoaring.web.taxonomy.entity.TagInstitutionView;
import com.cloudsoaring.web.taxonomy.entity.TagTypeInstitutionView;
import com.cloudsoaring.web.taxonomy.entity.TbInstitutionEvalutionManagerView;

/**
 * 课程分类管理
 * 
 * @author JGJ
 *
 */
@Service
public class TaxonomyTeacherService extends FileService implements TaxonomyConstants {

	/**
	 * 标签一览
	 * 
	 * @param BusinessType
	 *            业务类型
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ResultBean searchTagList(TagInstitutionEntityView tag) {
		ResultBean result = new ResultBean();
		if (StringUtil.isEmpty(tag.getBusinessType())) {
			tag.setBusinessType(CourseStateConstants.TAG_BUSSINESS_TYPE_INSTITUTION);
		}

		result = commonDao.searchList4Page(tag, SEARCH_TAG_LIST_BYTYPE);
		List<TagInstitutionEntityView> list = (List<TagInstitutionEntityView>) result.getData();
		if (list.size() == 0) {
			return result;
		}
		List<TagInstitutionEntityView> listAll = new ArrayList<TagInstitutionEntityView>();
		for (TagInstitutionEntityView tagw : list) {
			listAll.add(tagw);
			for (TagInstitutionEntityView taglist : tagw.getList()) {
				taglist.setType(CourseStateConstants.TAG_TYPE_RE);
				listAll.add(taglist);
			}

		}

		result.setData(listAll);

		return result;
	}

	/**
	 * 标签一览
	 * 
	 * @param BusinessType
	 *            业务类型
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ResultBean searchTagList2(TagInstitutionEntityView tag) {
		ResultBean result = new ResultBean();
		if (StringUtil.isEmpty(tag.getBusinessType())) {
			tag.setBusinessType(CourseStateConstants.TAG_BUSSINESS_TYPE_PLAN_INSTITUTION);
		}

		result = commonDao.searchList4Page(tag, SEARCH_TAG_LIST_BYTYPE_INSTITUTION);
		List<TagInstitutionEntityView> list = (List<TagInstitutionEntityView>) result.getData();
		if (list.size() == 0) {
			return result;
		}
		List<TagInstitutionEntityView> listAll = new ArrayList<TagInstitutionEntityView>();
		for (TagInstitutionEntityView tagw : list) {
			listAll.add(tagw);
			for (TagInstitutionEntityView taglist : tagw.getList()) {
				taglist.setType(CourseStateConstants.TAG_TYPE_RE);
				listAll.add(taglist);
			}

		}

		result.setData(listAll);

		return result;
	}
	/**
	 * 保存新增标签
	 * 
	 * @param tagEntity
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saveTag.action")
	@ResponseBody
	public ResultBean saveTag(TagInstitutionEntityView tagEntity) throws Exception {
		ResultBean result = new ResultBean();
		tagEntity.setTagId(IDGenerator.genUID());
		FileEntity resultData = processRequestFile("f1", "image");
		if (resultData != null) {
			tagEntity.setIco(resultData.getFileId());
		}

		if (StringUtil.isEmpty(tagEntity.getParentId())) {

			tagEntity.setBusinessType(StringUtil.getOrElse(
					tagEntity.getBusinessType(),
					CourseStateConstants.TAG_BUSSINESS_TYPE_INSTITUTION));

			commonDao.insertData(tagEntity, INSERT_TAG_FA_INSTITUTION);
		} else {
			assertNotEmpty(tagEntity.getTagTypeId(), "标签类Id不能为空");
			commonDao.insertData(tagEntity, INSERT_TAG_INSTITUTION);
		}

		return result;

	}
	
	/**
	 * 保存新增标签
	 * 
	 * @param tagEntity
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saveTag2.action")
	@ResponseBody
	public ResultBean saveTag2(TagInstitutionEntityView tagEntity) throws Exception {
		ResultBean result = new ResultBean();
		tagEntity.setTagId(IDGenerator.genUID());
		FileEntity resultData = processRequestFile("f1", "image");
		if (resultData != null) {
			tagEntity.setIco(resultData.getFileId());
		}

		if (StringUtil.isEmpty(tagEntity.getParentId())) {

			tagEntity.setBusinessType(StringUtil.getOrElse(
					tagEntity.getBusinessType(),
					CourseStateConstants.TAG_BUSSINESS_TYPE_PLAN_INSTITUTION));

			commonDao.insertData(tagEntity, INSERT_TAG_FA_INSTITUTION);
		} else {
			assertNotEmpty(tagEntity.getTagTypeId(), "标签类Id不能为空");
			commonDao.insertData(tagEntity, INSERT_TAG_INSTITUTION);
		}

		return result;

	}

	/**
	 * 删除标签
	 * 
	 * @param tagIds
	 * @return
	 * @throws Exception
	 */
	public ResultBean delTag(String tagId, String ico) throws Exception {
		ResultBean result = new ResultBean();
		String[] tagIds = StringUtil.split(tagId, ",");
		if (tagIds != null&&tagIds.length>0) {
			int count = commonDao.searchCount(tagIds, SEARCH_TAG_LINK_COUNT_INSTITUTION);
			if (count > 0) {
				return result.setStatus(false).setMessages("不能删除正在使用的标签");
			}
			int countOne = commonDao.searchCount(tagIds, SEARCH_TAG_COUNT);
			if (countOne > 0) {
				return result.setStatus(false).setMessages("不能删除父标签");
			}

			commonDao.deleteData(tagIds, DELECTE_TAG_INSTITUTION);
			String[] icos = StringUtil.split(ico, ",");
			for (String ic : icos) {
				super.deleteFileById(ic, "image");
			}
		}
		return result;

	}

	/**
	 * 编辑标签
	 * 
	 * @param TagEntity
	 * @return
	 * @throws Exception
	 */
	public ResultBean updateTag(TagInstitutionEntity tagEntity) throws Exception {
		ResultBean result = new ResultBean();
		FileEntity resultData = processRequestFile("f1", "image");
		if (resultData != null && StringUtil.isNotEmpty(resultData.getFileId())) {
			super.deleteFileById(tagEntity.getIco(), "image");
			tagEntity.setIco(resultData.getFileId());
		}
		commonDao.updateData(tagEntity, UPDATE_TAG_INSTITUTION);
		return result;

	}

	/**
	 * 检索子标签类
	 * 
	 * @param businessType
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<TagTypeInstitutionView> searchTagTypeList(String businessType) {

		return commonDao.searchList(StringUtil.getOrElse(businessType,
				CourseStateConstants.TAG_BUSSINESS_TYPE_INSTITUTION),
				SQL_SEARCH_TAG_TYPE_LIST_INSTITUTION);

	}

	/**
	 * 检索子标签类
	 * 
	 * @param businessType
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<TagTypeInstitutionView> searchTagTypeList2(String businessType) {

		return commonDao.searchList(StringUtil.getOrElse(businessType,
				CourseStateConstants.TAG_BUSSINESS_TYPE_PLAN_INSTITUTION),
				SQL_SEARCH_TAG_TYPE_LIST_INSTITUTION);

	}
    /**
     * 查询一级标签
     * @param tagQuery
     * @return
     */
	public List<TagInstitutionView> searchTagInstitutionList(TagInstitutionView tagQuery) {
		tagQuery.setTagTypeId("3");
		tagQuery.setBusinessType("2");
		return commonDao.searchList(tagQuery, SQL_SEARCH_FIRST_TAG_TYPE_INSTITUTION);
	}
    /**
     * 查询二级标签
     * @param tagQuery
     * @return
     */
	public List<TagInstitutionView> searchTagInstitutionSecondList(TagInstitutionView tagQuery) {
		tagQuery.setTagTypeId("4");
		tagQuery.setBusinessType("2");
		return commonDao.searchList(tagQuery, SQL_SEARCH_SECOND_TAG_TYPE_INSTITUTION);
	}
    /**
     * 查询评价管理列表
     * @param queryInfo
     * @return
     */
	public ResultBean listSearch(TbInstitutionEvalutionManagerView queryInfo) {
		if (!WebContext.hasAccess(CourseStateConstants.ALL_LIST)) {
			queryInfo.setTeacherId(WebContext.getSessionUserId());
		}
		return commonDao.searchList4Page(queryInfo, SELECT_INSTITUTION_LIST_MANAGER);
	}

	@SuppressWarnings("unchecked")
	public ResultBean listMeasurementSearch(MeasurementInstitutionEntity queryInfo) {
		ResultBean result = new ResultBean();
		String userId=WebContext.getSessionUserId();
		List<MeasurementInstitutionEntity> list = new ArrayList<MeasurementInstitutionEntity>();
		result = commonDao.searchList4Page(queryInfo, SELECT_INSTITUTION_QUETION_LIST_MANAGER);
		Object object = result.getData();
		list = (List<MeasurementInstitutionEntity>) object;
		if(!list.isEmpty()) {
			for(int i=0;i<list.size();i++) {
				String id = list.get(i).getId();
				UserMeasurementResultEntity entity = new UserMeasurementResultEntity();
				entity.setMeasurementId(id);
				entity.setCreateUser(userId);
				entity = commonDao.searchOneData(entity, SELECT_QUETION_RESULT_LIST_MANAGER);
				if(null==entity) {
					list.get(i).setFlag("0");
				}else {
					list.get(i).setFlag("1");
				}
			}
		}
		result.setData((Object)list);
		return result;
				
	}
}
