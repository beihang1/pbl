package com.cloudsoaring.web.teacherevalution.service;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.exception.BusinessException;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.FileEntity;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.constant.Constants;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.entity.CourseEntity;
import com.cloudsoaring.web.course.entity.UserMeasuermentAnswerEntity;
import com.cloudsoaring.web.course.entity.UserMeasurementResultEntity;
import com.cloudsoaring.web.teacher.entity.CertificateEntity;
import com.cloudsoaring.web.teacher.entity.FamilyMemberEntity;
import com.cloudsoaring.web.teacher.entity.TeacherEntity;
import com.cloudsoaring.web.teacherevalution.constant.EvalutionDbConstants;
import com.cloudsoaring.web.teacherevalution.constant.EvalutionTeacherConstants;
import com.cloudsoaring.web.teacherevalution.entity.MeasurementTeacherEntity;
import com.cloudsoaring.web.teacherevalution.entity.QuestionOptionTeacherEntity;
import com.cloudsoaring.web.teacherevalution.entity.QuestionTeacherEntity;
import com.cloudsoaring.web.teacherevalution.entity.TbTeacherEvalutionEntity;
import com.cloudsoaring.web.teacherevalution.entity.TeacherEvalutionEntity;
import com.cloudsoaring.web.trainingplatform.constants.TpDbConstants;
import com.cloudsoaring.web.trainingplatform.entity.TpUserEntity;
import com.cloudsoaring.web.trainingplatform.utils.TpConfigUtil;

/***
 * 模块相关的业务处理Service
 * 
 * @author ljl
 *
 */
@Service
@SuppressWarnings("unchecked")
public class TeacherEvalutionService extends FileService implements EvalutionTeacherConstants {
	
	public ResultBean listSearch(TeacherEntity queryInfo) {
		return commonDao.searchList4Page(queryInfo, SELECT_TEACHER_LIST_MANAGER);
	}
	
	public ResultBean familyMemberList(FamilyMemberEntity entity) {
		return commonDao.searchList4Page(entity, SELECT_TEACHER_FAMILY_MEMBER_LIST);
	}
	
	public ResultBean certificateList(CertificateEntity entity) {
		return commonDao.searchList4Page(entity, SELECT_TEACHER_CERTIFICATE_LIST);
	}
	
	public void save(TeacherEntity entity) throws Exception {
		// 图片处理
		FileEntity pictureFile = processRequestFile("picture", Constants.FILE_TYPE_IMAGE);
		// 缩略图
		compress(pictureFile, false);

		if (pictureFile != null) {
			entity.setPictureId(pictureFile.getFileId());
		}
		if(!StringUtil.isNotEmpty(entity.getId()) || "".equals(entity.getId())) {
			String id = IDGenerator.genUID();
			entity.setId(id);
			entity.setUserId(WebContext.getSessionUserId());
			entity.setCreateTime(new Date());
			entity.setCreateUser(WebContext.getSessionUser().getUserId());
			entity.setStatus("0");
			/*TpUserEntity user = new TpUserEntity();
			user.setUserId(id);
			user.setUserName(entity.getUserName());
			user.setSex(entity.getSex());
			user.setPhone(entity.getTelephone());
			user.setEmail(entity.getEmail());
			user.setPassword(MD5Util.getPassword4MD5("123"));*/
			entity.setWorkAge(dayComparePrecise(entity.getWorkTime(),new Date()));
			entity.setSchoolAge(dayComparePrecise(entity.getEducationWorkTime(),new Date()));
			commonDao.insertData(entity, TpDbConstants.TEACHER_INSERT);
			//commonDao.insertData(user, TpDbConstants.USER_INSERT);
		}else {
			entity.setUpdateTime(new Date());
			entity.setUpdateUser(WebContext.getSessionUser().getUserId());
			entity.setWorkAge(dayComparePrecise(entity.getWorkTime(),new Date()));
			entity.setSchoolAge(dayComparePrecise(entity.getEducationWorkTime(),new Date()));
			commonDao.updateData(entity, TpDbConstants.TEACHER_UPDATE);
		}
	}
	
	public TeacherEntity getTeacherById(TeacherEntity entity) {
		return commonDao.searchOneData(entity, TpDbConstants.TEACHER_SELECT_BY_PK);
	}
	
	public TeacherEntity getTeacherByUserId(TeacherEntity entity) {
		//return commonDao.searchOneData(entity, TpDbConstants.TEACHER_SELECT_BY_PK);
		return commonDao.searchOneData(entity, TpDbConstants.TEACHER_SELECT_BY_USER_ID);
	}
	
	public TpUserEntity getUserByUserId(String userId) {
		TpUserEntity user = new TpUserEntity();
		user.setUserId(userId);
		return commonDao.searchOneData(user, TpDbConstants.USER_SEARCH_NAME_BY_USERID);
	}
	
	public void deleteTeacher(String id) {
		TeacherEntity entity = new TeacherEntity();
		entity.setId(id);
		commonDao.deleteData(entity, DELETE_TEACHER_BY_ID);
	}
	
	public CertificateEntity searchCertificateById(String id) {
		CertificateEntity entity = new CertificateEntity();
		entity.setId(id);
		return commonDao.searchOneData(entity, TpDbConstants.TEACHER_CERTIFICATE_SELECT_BY_PK);
	}
	
	public void deleteCertificate(String id) {
		CertificateEntity entity = new CertificateEntity();
		entity.setId(id);
		commonDao.deleteData(entity,TpDbConstants.TEACHER_CERTIFICATE_DELETE);
	}
	
	public void deleteFamilyMember(String id) {
		FamilyMemberEntity entity = new FamilyMemberEntity();
		entity.setId(id);
		commonDao.deleteData(entity,TpDbConstants.TEACHER_FAMILY_MEMBER_DELETE);
	}
	
	public void saveFamilyMember(FamilyMemberEntity entity) {
		if(!StringUtil.isNotEmpty(entity.getId()) || "".equals(entity.getId()) || entity.getId() == null) {
			entity.setId(IDGenerator.genUID());
			entity.setCreateTime(new Date());
			entity.setCreateUser(WebContext.getSessionUser().getUserId());
			commonDao.insertData(entity, TpDbConstants.TEACHER_FAMILY_MEMBER_INSERT);
		}else {
			entity.setUpdateUser(WebContext.getSessionUser().getUserId());
			commonDao.updateData(entity, TpDbConstants.TEACHER_FAMILY_MEMBER_UPDATE);
		}
	}
	
	public FamilyMemberEntity searchFamilyMemberById(String id) {
		FamilyMemberEntity entity = new FamilyMemberEntity();
		entity.setId(id);
		return commonDao.searchOneData(entity, TpDbConstants.TEACHER_FAMILY_MEMBER_SELECT_BY_PK);
	}
	
	public void saveCertificate(CertificateEntity entity) throws Exception {
		// 图片处理
		FileEntity pictureFile = processRequestFile("picture", Constants.FILE_TYPE_IMAGE);
		// 缩略图
		compress(pictureFile, false);

		if (pictureFile != null) {
			entity.setPictureId(pictureFile.getFileId());
		}
		if(!StringUtil.isNotEmpty(entity.getId()) || "".equals(entity.getId()) || entity.getId() == null) {
			entity.setId(IDGenerator.genUID());
			entity.setCreateTime(new Date());
			entity.setCreateUser(WebContext.getSessionUser().getUserId());
			commonDao.insertData(entity, TpDbConstants.TEACHER_CERTIFICATE_INSERT);
		}else {
			entity.setUpdateUser(WebContext.getSessionUser().getUserId());
			commonDao.updateData(entity, TpDbConstants.TEACHER_CERTIFICATE_UPDATE);
		}
	}
	
	
	
	/**
	 * 计算2个日期之间相差的  相差多少年月日
	 * 比如：2011-02-02 到  2017-03-02 相差 6年，1个月，0天
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	private static String dayComparePrecise(Date fromDate,Date toDate){
	    Calendar  from  =  Calendar.getInstance();
	    from.setTime(fromDate);
	    Calendar  to  =  Calendar.getInstance();
	    to.setTime(toDate);
	    int fromYear = from.get(Calendar.YEAR);
	    int toYear = to.get(Calendar.YEAR);
	    int year = toYear  -  fromYear;
	    return String.valueOf(year);
	}
	
	public static void main(String[] args) {
		Date date = new Date(118, 11, 02);
		System.out.println(dayComparePrecise(date,new Date()));
	}

	public List<TeacherEvalutionEntity> exportTeacher(TeacherEvalutionEntity entity) {
		return commonDao.searchList(entity, SELECT_TEACHER_LIST_MANAGER);
	}

	public ResultBean listSearch(TeacherEvalutionEntity queryInfo) {
		return commonDao.searchList4Page(queryInfo, SELECT_EVALUTION_LIST_MANAGER);
	}

	public ResultBean getTeacherAll(TeacherEvalutionEntity query) {
		return commonDao.searchList4Page(query, SELECT_TEACHER_LIST_MANAGER);
	}

	public ResultBean searchTeacher(String id) {
		TeacherEvalutionEntity entity = new TeacherEvalutionEntity();
		ResultBean result = new ResultBean();
		  entity.setId(id);
	   return result.setData(commonDao.searchOneData(entity, SELECT_EVALUTION_SEARCH_BY_ID));
	}

	public void insertCourseCatagory(TbTeacherEvalutionEntity data) {
		data.setId(IDGenerator.genUID());
		// 通过积分显示价格
		if (data.getPrice() != null) {
			if (TpConfigUtil.displayPriceByPoints()) {
				data.setPrice(data.getPrice().divide(new BigDecimal(TpConfigUtil.getPointsForSmallChange())));
			}
		}
		TbTeacherEvalutionEntity view = splitActivity(data.getActivitys());
		data.setFlagDiscuss(view.getFlagDiscuss());
		data.setFlagNote(view.getFlagNote());
		data.setFlagQa(view.getFlagQa());
		data.setFlagVote(view.getFlagVote());
		commonDao.insertData(data, SELECT_EVALUTION_INSERT);
		
	}
	/**
	 * 获取活动标识
	 * 
	 * @param activitys 活动标识值
	 * @return 返回活动标识
	 */
	private TbTeacherEvalutionEntity splitActivity(String activitys) {
		TbTeacherEvalutionEntity view = new TbTeacherEvalutionEntity();

		view.setFlagNote(activitys.indexOf(FLAG_NOTE) > -1 ? FLAG : FLAG_NOT);
		view.setFlagDiscuss(activitys.indexOf(FLAG_DISCUSS) > -1 ? FLAG : FLAG_NOT);
		view.setFlagQa(activitys.indexOf(FLAG_QA) > -1 ? FLAG : FLAG_NOT);
		view.setFlagVote(activitys.indexOf(FLAG_VOTE) > -1 ? FLAG : FLAG_NOT);

		return view;
	}

	public void updateCourseAll(TbTeacherEvalutionEntity data) {
		TbTeacherEvalutionEntity view = splitActivity(data.getActivitys());
		data.setFlagDiscuss(view.getFlagDiscuss());
		data.setFlagNote(view.getFlagNote());
		data.setFlagQa(view.getFlagQa());
		data.setFlagVote(view.getFlagVote());
		commonDao.updateData(data, SELECT_EVALUTION_UPDATE);
		
	}

	public ResultBean lisAlltSearch(TbTeacherEvalutionEntity queryInfo) {
		return commonDao.searchList4Page(queryInfo, SELECT_EVALUTION_LIST_MANAGER);
	}

	public void deleteEvalution(String id) {
		TbTeacherEvalutionEntity entity = new TbTeacherEvalutionEntity();
		entity.setId(id);
		this.commonDao.deleteData(entity, SELECT_TEACHER_EVALUTION_LIST_MANAGER);
		
	}

	public void publicTeacherEvalution(String id, String status) {
		TbTeacherEvalutionEntity view = new TbTeacherEvalutionEntity();
		view.setId(id);
		if (COURSE_STATUS_NOT.equals(status)) {
			view.setStatus(COURSE_STATUS_PUBLIC);
		} else {
			view.setStatus(COURSE_STATUS_NOT);
		}
		commonDao.updateData(view, UPDATE_EVALUTION_STATUS);
		
	}

	public TbTeacherEvalutionEntity searchEvalutionById(String id, String type) {
		TbTeacherEvalutionEntity view  = new TbTeacherEvalutionEntity();
		view.setId(id);
		view = commonDao.searchOneData(view, EvalutionDbConstants.USER_SEARCH_NAME_BY_TEACHER_EVALUTION);
		String activitys = "";
		if (PAGE_TYPE_EDIT.equals(type)) {
			if (FLAG.equals(view.getFlagNote())) {
				activitys += FLAG_NOTE + ",";
			}
			if (FLAG.equals(view.getFlagVote())) {
				activitys += FLAG_VOTE + ",";
			}
			if (FLAG.equals(view.getFlagQa())) {
				activitys += FLAG_QA + ",";
			}
			if (FLAG.equals(view.getFlagDiscuss())) {
				activitys += FLAG_DISCUSS + ",";
			}
		}
		view.setActivitys(activitys);
		return view;
	}

	public void saveMeasure(MeasurementTeacherEntity measure) {
		measure.setId(IDGenerator.genUID());
		measure.setMeasureType(MEASUERMENT_TYPE_QUESTIO);
		measure.setLinkType(LINK_TYPE_EVALUTION);
		if (isHasMeasure(measure) == 0) {
			commonDao.insertData(measure, MEASUREMENT_INSTITUTION_INSERT);
			CourseEntity entity = new CourseEntity();
			entity.setFlagQuestionnaire(FLAG);
			entity.setId(measure.getLinkId());
			commonDao.updateData(entity, SQL_UPDATE_FLAG_QUEATION_EVALUTION);
			// 保存问题
			if (measure.getQuestionList() != null) {
				insertQuestion(measure.getQuestionList(), measure.getId());
			}
		} else {
			throw new BusinessException("此课程下已经存在问卷，不允许添加！");
		}
		
	}
	/**
	 * 问卷存在
	 * 
	 * @param measure
	 * @return
	 */
	public int isHasMeasure(MeasurementTeacherEntity measure) {
		return commonDao.searchCount(measure, SQL_SEACH_ONE_IS_MEASURE);
	}
	/**
	 * 插入问题
	 * 
	 * @param newQuestionList
	 * @param measurementId
	 */
	public void insertQuestion(List<QuestionTeacherEntity> newQuestionList, String measurementId) {
		QuestionTeacherEntity entity = new QuestionTeacherEntity();
		entity.setMeasureId(measurementId);
		Integer maxOrder = commonDao.searchOneData(entity, QUESTION_EVALUTION_MAX_ORDER);
		if (maxOrder == null) {
			maxOrder = 0;
		}
		Integer order = maxOrder + 1;
		for (QuestionTeacherEntity questionData : newQuestionList) {
			questionData.setId(IDGenerator.genUID());
			questionData.setMeasureId(measurementId);
			questionData.setQuestionOrder(new BigDecimal(order));
			order++;
			// 参考答案序号,等选项保存后再保存问题
			int ii = 0;
			String referenceAnswerNo = null;
			if (questionData != null && StringUtil.isNotEmpty(questionData.getReferenceAnswerId())) {
				String[] ss = questionData.getReferenceAnswerId().split(",");
				for (String gg : ss) {
					ii = Integer.parseInt(gg) - 1;
					referenceAnswerNo += String.valueOf(ii) + ",";
				}
			}
			String referenceAnswerId = null;
			List<QuestionOptionTeacherEntity> options = questionData.getOptions();
			if (options != null) {
				referenceAnswerId = insertQuestionOption(questionData, referenceAnswerNo);
			}
			questionData.setReferenceAnswerId(referenceAnswerId);
			insert(questionData);
		}
	}
	/**
	 * 插入问题选项
	 * 
	 * @param newQuestion
	 * @param referenceAnswerNo
	 * @return 返回结果
	 */
	private String insertQuestionOption(QuestionTeacherEntity newQuestion, String referenceAnswerNo) {

		List<QuestionOptionTeacherEntity> options = newQuestion.getOptions();
		String referenceAnswerId = "";
		if (options != null) {
			// 保存问题选项
			int optionSize = options.size();

			for (int i = 0; i < optionSize; i++) {
				QuestionOptionTeacherEntity option = options.get(i);
				option.setId(IDGenerator.genUID());
				option.setQuestionId(newQuestion.getId());
				option.setOptionOrder(new BigDecimal(i + ""));
				insert(option);
				if (StringUtil.isNotEmpty(referenceAnswerNo) && referenceAnswerNo.contains(i + "")) {
					referenceAnswerId = referenceAnswerId + "," + option.getId();
				}
			}
			if (StringUtil.isNotEmpty(referenceAnswerId)) {
				referenceAnswerId = referenceAnswerId.substring(1);
			}
		}
		return referenceAnswerId;
	}

	public void updateMeasure(MeasurementTeacherEntity measure) {
		MeasurementTeacherEntity oldEntity = seachMeasureByCourseId(measure.getLinkId());
		oldEntity.setTitile(measure.getTitile());
		commonDao.updateData(oldEntity, SQL_UPDATE_MEASURE);
		// 保存问题
		if (measure.getQuestionList() != null) {
			updateQuestion(measure.getId(), measure.getQuestionList(), oldEntity.getQuestionList());
		}
		
	}
	/**
	 * 保存问题
	 * 
	 * @param measurementId   测试ID
	 * @param newQuestionList 新问题内容
	 * @param oldQuestionList 旧问题内容
	 */
	public void updateQuestion(String measurementId, List<QuestionTeacherEntity> newQuestionList,
			List<QuestionTeacherEntity> oldQuestionList) {
		Map<String, QuestionTeacherEntity> questionMap = getMapQuestionByID1(newQuestionList);
		Map<String, QuestionTeacherEntity> oldQuestionMap = getMapQuestionByID1(oldQuestionList);

		for (QuestionTeacherEntity oldQuestion : oldQuestionList) {
			// 正确答案的ID
			String referenceAnswerId = "";
			// 问题为旧问题
			if (questionMap.containsKey(oldQuestion.getId())) {
				QuestionTeacherEntity newQuestionEntity = questionMap.get(oldQuestion.getId());
				// 更新选项问题
				referenceAnswerId = updateOption(oldQuestion, newQuestionEntity.getReferenceAnswerId(),
						newQuestionEntity);
				oldQuestion.setQuestionType(newQuestionEntity.getQuestionType());
				oldQuestion.setScore(newQuestionEntity.getScore());
				oldQuestion.setContent(newQuestionEntity.getContent());
				oldQuestion.setQuestionOrder(newQuestionEntity.getQuestionOrder());
				oldQuestion.setReferenceAnswerId(referenceAnswerId);
				this.update(oldQuestion);

			} else {
				delete(oldQuestion);
				delete(oldQuestion, QUESTION_DELET_OPTION_BY_QUESTION);
				delete(oldQuestion, QUESTION_DELET_USER_ANSWER_BY_QUESTION);
			}
		}
		for (QuestionTeacherEntity newQuestion : newQuestionList) {
			// 正确答案的ID
			String referenceAnswerId = "";
			// 问题为新问题
			if (!oldQuestionMap.containsKey(newQuestion.getId())) {

				newQuestion.setId(IDGenerator.genUID());
				newQuestion.setMeasureId(measurementId);
				referenceAnswerId = insertQuestionOption(newQuestion, newQuestion.getReferenceAnswerId());

				newQuestion.setReferenceAnswerId(referenceAnswerId);
				insert(newQuestion);
			}
		}
	}
	/**
	 * 以问题ID为MAP
	 * 
	 * @param questions
	 * @return 返回结果
	 */
	private Map<String, QuestionTeacherEntity> getMapQuestionByID1(List<QuestionTeacherEntity> questions) {
		Map<String, QuestionTeacherEntity> result = new HashMap<String, QuestionTeacherEntity>();
		// 如果问题列表为空，则已空的MAP返回
		if (questions != null) {
			for (QuestionTeacherEntity question : questions) {
				result.put(question.getId(), question);
			}
		}
		return result;
	}
	/**
	 * 更新选项
	 * 
	 * @param oldQuestion
	 * @param referenceAnswerNo 正确答案序号
	 * @param newQuestion
	 * @return 正确答案的ID
	 */
	private String updateOption(QuestionTeacherEntity oldQuestion, String referenceAnswerNo, QuestionTeacherEntity newQuestion) {
		String referenceAnswerId = "";
		// 旧问题选项的ID
		// key为选项位置， value为选项ID
		Map<String, String> oldOptionIdMap = new HashMap<String, String>();
		if (oldQuestion.getOptions() != null) {
			for (int i = 0; i < oldQuestion.getOptions().size(); i++) {
				QuestionOptionTeacherEntity option = oldQuestion.getOptions().get(i);
				oldOptionIdMap.put(i + "", option.getId());
			}
		}
		// 删除旧问题
		commonDao.deleteData(oldQuestion, QUESTION_DELET_OPTION_BY_QUESTION_INSTITUTION);

		// 插入新问题
		if (newQuestion.getOptions() != null) {
			for (int i = 0; i < newQuestion.getOptions().size(); i++) {
				QuestionOptionTeacherEntity newOption = newQuestion.getOptions().get(i);
				// 新问题选项，相同位置的option必须沿用之前老的ID
				if (oldOptionIdMap.containsKey(i + "")) {
					newOption.setId(oldOptionIdMap.get(i + ""));
				} else {
					newOption.setId(IDGenerator.genDomID());
				}
				newOption.setQuestionId(oldQuestion.getId());
				insert(newOption);
				if (StringUtil.isNotEmpty(referenceAnswerNo) && referenceAnswerNo.contains(i + "")) {
					referenceAnswerId = referenceAnswerId + "," + newOption.getId();
				}
			}
		}
		if (StringUtil.isNotEmpty(referenceAnswerId)) {
			referenceAnswerId = referenceAnswerId.substring(1);
		}
		return referenceAnswerId;
	}
	/**
	 * 
	 * 根据linkId查询问卷信息
	 * 
	 * @param id
	 * @return
	 */
	public MeasurementTeacherEntity seachMeasureByCourseId(String id) {
		MeasurementTeacherEntity entity = new MeasurementTeacherEntity();
		entity.setLinkId(id);
		entity.setLinkType(LINK_TYPE_EVALUTION);
		entity.setMeasureType(MEASUERMENT_TYPE_QUESTIO);
		entity = commonDao.searchOneData(entity, SQL_SEACH_MEASURE_BY_EVALUTION_ID);
		List<QuestionTeacherEntity> measurementResult = (List<QuestionTeacherEntity>) search(entity,
				MEASUERMENT_SEARCH_QUESTION_LIST_BY_MEASUREMENT_INSTITUTION);
		// 检索测试题中的选项
		for (QuestionTeacherEntity quest : measurementResult) {
			QuestionOptionTeacherEntity optionQuery = new QuestionOptionTeacherEntity();
			optionQuery.setQuestionId(quest.getId());
			optionQuery.setSortName("sortOrder");
			optionQuery.setSortOrder("asc");
			List<QuestionOptionTeacherEntity> options = (List<QuestionOptionTeacherEntity>) search(optionQuery,
					MEASUERMENT_SEARCH_QUESTION_EVALUTION_OPTIONS_LIST);
			quest.setOptions(options);
		}
		entity.setQuestionList(measurementResult);
		return entity;

	}

	public ResultBean listMeasurementSearch(MeasurementTeacherEntity queryInfo) {
		ResultBean result = new ResultBean();
		String userId=WebContext.getSessionUserId();
		List<MeasurementTeacherEntity> list = new ArrayList<MeasurementTeacherEntity>();
		result = commonDao.searchList4Page(queryInfo, SELECT_INSTITUTION_QUETION_LIST_MANAGER);
		Object object = result.getData();
		list = (List<MeasurementTeacherEntity>) object;
		if(!list.isEmpty()) {
			for(int i=0;i<list.size();i++) {
				String id = list.get(i).getId();
				UserMeasurementResultEntity entity = new UserMeasurementResultEntity();
				entity.setMeasurementId(id);
				entity.setCreateUser(userId);
				entity = commonDao.searchOneData(entity, SELECT_QUETION_RESULT_LIST_MANAGER);
				if(null==entity) {
					list.get(i).setFlag("0");
					list.get(i).setFlagName("未评价");
				}else {
					list.get(i).setFlag("1");
					list.get(i).setFlagName("已评价");
				}
			}
		}
		result.setData((Object)list);
		return result;
	}

	public ResultBean questionnaireDetial(String linkId, String linkType) {
		ResultBean result = new ResultBean();
		// 链接ID为空
		assertNotEmpty(linkId, MSG_E_NULL_ID);
		// 链接类型为空
		assertNotEmpty(linkType, MSG_E_EMPTY_LINK_TYPE);
		// 链接类型错误：不等于COURSE
		if (!linkType.equalsIgnoreCase(LINK_TYPE_EVALUTION)) {
			result.setStatus(false);
			result.setMessages(MSG_E_ERROR_LINK_TYPE);
			return result;
		}
		MeasurementTeacherEntity meas = new MeasurementTeacherEntity();
		meas.setLinkId(linkId);
		meas.setLinkType(linkType);
		meas.setMeasureType(MEASUERMENT_TYPE_QUESTIO);
		meas = commonDao.searchOneData(meas, MEASUREMENT_EVALUTION_SELECT);
		// 问卷不存在
		if (meas == null) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_MEAS_NOT_EXIST));
			return result;
		}
		List<QuestionTeacherEntity> questionViews = new ArrayList<QuestionTeacherEntity>();
		QuestionTeacherEntity questionView = new QuestionTeacherEntity();
		questionView.setMeasureId(meas.getId());
		questionView.setUserId(WebContext.getSessionUserId());
		questionViews = commonDao
				.searchList(questionView, QUESTION_EVALUTION_SELECT_VIEW);
		// 题目不存在
		if (questionViews == null || questionViews.size() == 0) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_QUESTION_NOT_EXIST));
			return result;
		}
		for (int i = 0; i < questionViews.size(); i++) {
			QuestionOptionTeacherEntity questionOptionEntity = new QuestionOptionTeacherEntity();
			questionOptionEntity.setQuestionId(questionViews.get(i).getId());
			List<QuestionOptionTeacherEntity> option = commonDao.searchList(
					questionOptionEntity, QUESTION_EVALUTION_OPTION_SELECT);
			// 选项不存在
			if (option == null || option.size() == 0) {
				result.setStatus(false);
				result.setMessages(getMessage(MSG_E_OPTION_NOT_EXIST));
				return result;
			}
			questionViews.get(i).setOption(option);
		}

		return result.setData(questionViews);
	}

	public ResultBean submitQuestionnaire(String questionIds, String optionIds, String measureId) {
		ResultBean result = new ResultBean();
		// 题目或选项为空
		if (questionIds == null || optionIds == null || optionIds .equals("") 
				|| questionIds .equals("")) {
			result.setStatus(false);
			result.setMessages(MSG_E_QUESTION_OPTION_NULL);
			return result;
		}
		String[] question = questionIds.split(",");
		String[] option = optionIds.split(",");
		List<String> questionId = Arrays.asList(question);
		List<String> optionId = Arrays.asList(option);
		// 判断用户是否登录
		if (WebContext.isGuest()) {
			result.setStatus(false);
			result.setMessages(getMessage(NULL_USER_ID));
			return result;
		}
		if(StringUtil.isEmpty(measureId)){
			QuestionTeacherEntity query = new QuestionTeacherEntity();
			query.setId(question[0]);
			QuestionTeacherEntity queryOne = searchOneData(query);
			if(queryOne == null){
				result.setStatus(false);
				result.setMessages("系统异常，请联系管理员");
				return result;
			}
			measureId = queryOne.getMeasureId();
		}
		for(int i=0;i<questionId.size();i++){
			String[] opIds = optionId.get(i).split(";");
			for(int j=0;j<opIds.length;j++){
				QuestionOptionTeacherEntity questionOptionEntity = new QuestionOptionTeacherEntity();
				questionOptionEntity.setId(opIds[j]);
				commonDao.updateData(questionOptionEntity, QUESTION_EVALUTION_OPTION_UPDATE_NUM);
			}
			
			UserMeasuermentAnswerEntity umae = new UserMeasuermentAnswerEntity();
			umae.setUserId(WebContext.getSessionUserId());
			umae.setOptionId(optionId.get(i));
			umae.setQuestionId(questionId.get(i));
			delete(umae);
			commonDao.insertData(umae, MEASUERMENT_ANSWER_INSERT);
			
			QuestionTeacherEntity questionEntity = new QuestionTeacherEntity();
			questionEntity.setId(questionId.get(i));
			commonDao.updateData(questionEntity, QUESTION_EVALUTION_UPDATE_NUM);
		}
		UserMeasurementResultEntity userMeasurementResultEntity = new UserMeasurementResultEntity();
		userMeasurementResultEntity.setMeasurementId(measureId);
		userMeasurementResultEntity.setUserId(WebContext.getSessionUserId());
		delete(userMeasurementResultEntity);
		commonDao.insertData(userMeasurementResultEntity, MEASUERMENT_RESULT_INSERT);
		return result.setStatus(true);
	}

	public ResultBean selectQuestionResultList(MeasurementTeacherEntity condition) {
		        //判断是否登录
				assertNotGuest();
				//condition.setUserId(WebContext.getSessionUserId());
				return commonDao.searchList4Page(condition,SQL_SELECT_MEASURE_RESULT_INFO_LIST);
	}

	public int searchInstitutionTotalOptions(String id) {
		QuestionOptionTeacherEntity questionOptionEntity = new QuestionOptionTeacherEntity();
		questionOptionEntity.setMeasureId(id);
		int optionNum = commonDao.searchCount(questionOptionEntity,
				SQL_SELECT_INSTITUTION_MAX_OPTIONS_INFO_QUESTION);
		return optionNum;
	}

	public List<UserMeasurementResultEntity> searchInstitutionUserMeasurementResultByEntiy(
			UserMeasurementResultEntity condition) {
		return search(condition,SQL_INSTITUTION_MEASUERMENT_USER_SEARCH);
	}

	public List<QuestionTeacherEntity> searchMeasuermentInstitutionNormalAndExam(String userId, String measurementId) {
		 //补充信息
        MeasurementTeacherEntity query = new MeasurementTeacherEntity();
        query.setId(measurementId);
        query = searchOneData(query);
        if(query == null) {
            return null;
        } 
        
        query.setUserId(userId);
        List<QuestionTeacherEntity> result = (List<QuestionTeacherEntity>) search(query, SQL_EVALUTION_MEASUERMENT_SEARCH_QUESTION_LIST_BY_MEASUREMENT);
        
        for(QuestionTeacherEntity quest : result) {
            QuestionOptionTeacherEntity optionQuery = new QuestionOptionTeacherEntity();
            optionQuery.setQuestionId(quest.getId());
            List<QuestionOptionTeacherEntity> options = (List<QuestionOptionTeacherEntity>) search(optionQuery, MEASUERMENT_EVALUTION_SEARCH_QUESTION_OPTIONS_LIST);
            quest.setOptions(options);
        }
        return result;
	}
	public ResultBean listYpMeasurementSearch(MeasurementTeacherEntity queryInfo) {
		ResultBean result = new ResultBean();
		String userId=WebContext.getSessionUserId();
		List<MeasurementTeacherEntity> yplist = new ArrayList<MeasurementTeacherEntity>();
		List<MeasurementTeacherEntity> list = new ArrayList<MeasurementTeacherEntity>();
		result = commonDao.searchList4Page(queryInfo, SELECT_INSTITUTION_QUETION_LIST_MANAGER);
		Object object = result.getData();
		list = (List<MeasurementTeacherEntity>) object;
		if(!list.isEmpty()) {
			for(int i=0;i<list.size();i++) {
				String id = list.get(i).getId();
				UserMeasurementResultEntity entity = new UserMeasurementResultEntity();
				entity.setMeasurementId(id);
				entity.setCreateUser(userId);
				entity = commonDao.searchOneData(entity, SELECT_QUETION_RESULT_LIST_MANAGER);
				if(null!=entity) {
					list.get(i).setFlag("0");
					list.get(i).setFlagName("已评价");
					yplist.add(list.get(i));
				}
			}
		}
		result.setData((Object)yplist);
		return result;
	}
	public ResultBean listWpMeasurementSearch(MeasurementTeacherEntity queryInfo) {
		ResultBean result = new ResultBean();
		String userId=WebContext.getSessionUserId();
		List<MeasurementTeacherEntity> yplist = new ArrayList<MeasurementTeacherEntity>();
		List<MeasurementTeacherEntity> list = new ArrayList<MeasurementTeacherEntity>();
		result = commonDao.searchList4Page(queryInfo, SELECT_INSTITUTION_QUETION_LIST_MANAGER);
		Object object = result.getData();
		list = (List<MeasurementTeacherEntity>) object;
		if(!list.isEmpty()) {
			for(int i=0;i<list.size();i++) {
				String id = list.get(i).getId();
				UserMeasurementResultEntity entity = new UserMeasurementResultEntity();
				entity.setMeasurementId(id);
				entity.setCreateUser(userId);
				entity = commonDao.searchOneData(entity, SELECT_QUETION_RESULT_LIST_MANAGER);
				if(null==entity) {
					list.get(i).setFlag("0");
					list.get(i).setFlagName("未评价");
					yplist.add(list.get(i));
				}
			}
		}
		result.setData((Object)yplist);
		return result;
	}
}
