package com.cloudsoaring.web.teacherevalution.constant;

import com.cloudsoaring.web.common.dao.ibatis.plugin.MybatisConstant;
@MybatisConstant
public interface EvalutionStateConstants {
    
    public static final String PERSON_SEX = "person_sex";
    
    public static final String POLITICS_STATUS = "politics_staus";
    
    public static final String MARITAL_STATUS = "marital_status";
    
    public static final String EDUCATION = "education";
    
    public static final String STATUS = "teacher_status";
    
    public static final String DEGREE = "teacher_degree";
    
    public static final String JOB_TITLE = "teacher_job_title";
    /** 课程状态 - 发布 */
	public static final String COURSE_STATUS_PUBLIC = "1";
	/** 课程状态-未发布 */
	public static final String COURSE_STATUS_NOT_PUBLIC = "2";
	/** 课程状态-未发布 */
    public static final String COURSE_STATUS_NOT = "0";
    /**页面类型：编辑*/
    public static final String PAGE_TYPE_EDIT="edit";
    
}