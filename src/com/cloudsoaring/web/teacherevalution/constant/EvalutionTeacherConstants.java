package com.cloudsoaring.web.teacherevalution.constant;

public interface EvalutionTeacherConstants extends EvalutionConfigNames, EvalutionDbConstants,
EvalutionLogConstants, EvalutionDefaultValues, EvalutionMsgNames,
EvalutionStateConstants {
	public static final String FILE_TYPE_HOMEWORK = "homework";
	public static final String SUM_KBN_EARN = "A";
	public static final String PLAN_CATEGORY_PLAN = "1";
	public static final String PLAN_CATEGORY_CASE = "2";
	public static final String FILE_TYPE_ZIP = "users";
	/**活动标识：笔记*/
    public static final String FLAG_NOTE="1";
    /**活动标识：评论flagDiscuss*/
    public static final String FLAG_DISCUSS="2";
    /**活动标识：问答flagQa*/
    public static final String FLAG_QA="3";
    /**活动标识：投票flagVote*/
    public static final String FLAG_VOTE="4";
    /**活动标识：1开启 0关闭*/
    public static final String FLAG="1";
    public static final String FLAG_NOT="0";
    //非考试的测试类型 - 问卷
    public static final String MEASUERMENT_TYPE_QUESTIO= "3";
    /** 链接类型 -评价) */
	public static final String LINK_TYPE_EVALUTION = "EVALUTION";
}
