package com.cloudsoaring.web.teacherevalution.constant;

public interface EvalutionDbConstants {

    public static final String SELECT_TEACHER_LIST_MANAGER = "TeacherEvalution.selectManagerList";
    
    public static final String SELECT_TEACHER_FAMILY_MEMBER_LIST = "FamilyMember.selectList";
    
    public static final String SELECT_TEACHER_CERTIFICATE_LIST = "Certificate.selectList";
    
    public static final String DELETE_TEACHER_BY_ID = "Teacher.delete";
    
    
    public static final String SELECT_TEACHER_HONOR_CERTIFICATE_LIST = "TeacherHonorCertificate.selectList";
    public static final String SELECT_EVALUTION_LIST_MANAGER = "TbTeacherEvalution.selectManagerList";
	/**根据ID查询教师信息*/
	public static final String SELECT_EVALUTION_SEARCH_BY_ID = "TeacherEvalution.selectByPk";
	/**保存评价*/
	public static final String SELECT_EVALUTION_INSERT = "TbTeacherEvalution.insert";
	/**更新评价*/
	public static final String SELECT_EVALUTION_UPDATE = "TbTeacherEvalution.update";
	/**删除教师*/
	public static final String SELECT_TEACHER_EVALUTION_LIST_MANAGER = "TbTeacherEvalution.delete";
	/**评价发布/取消发布*/
    public static final String UPDATE_EVALUTION_STATUS = "TbTeacherEvalution.updateStatus";
	//根据评价id检索
	public static final String USER_SEARCH_NAME_BY_TEACHER_EVALUTION = "TbTeacherEvalution.selectById";
	/** 评价问卷 */
	public static final String MEASUREMENT_INSTITUTION_INSERT = "MeasurementTeacher.insert";
	/**开启评价问卷标识*/
    public static final String SQL_UPDATE_FLAG_QUEATION_EVALUTION="TbTeacherEvalution.updateFlagQuestion";
    /**查询问卷是否存在*/
    public static final String SQL_SEACH_ONE_IS_MEASURE="MeasurementTeacher.seachIsByLinkId";
    /**问卷的最大数*/
    public static final String QUESTION_EVALUTION_MAX_ORDER = "QuestionTeacher.searchMaxOrder";
    public static final String SQL_SEACH_MEASURE_BY_EVALUTION_ID="MeasurementTeacher.seachMeasureByCourseId";
    /** 检索问题选项 */
	public static final String MEASUERMENT_SEARCH_QUESTION_LIST_BY_MEASUREMENT_INSTITUTION = "MeasurementTeacher.selectQuestionResult";
	/** 检索问题 */
	public static final String MEASUERMENT_SEARCH_QUESTION_EVALUTION_OPTIONS_LIST = "QuestionOptionTeacher.select";
	/**更新问卷*/
	public static final String SQL_UPDATE_MEASURE="MeasurementTeacher.update";
	/**更新问题信息删除问题选项*/
 	public static final String QUESTION_DELET_OPTION_BY_QUESTION_INSTITUTION = "QuestionTeacher.deletOptionByQuestion";
 // 更新问题信息删除问题选项
 	public static final String QUESTION_DELET_OPTION_BY_QUESTION = "QuestionTeacher.deletOptionByQuestion";
 // 根据问题信息删除用户回答的问题
 	public static final String QUESTION_DELET_USER_ANSWER_BY_QUESTION = "QuestionTeacher.deletUserAnswerByQuestion";
 	/**查询结构评价权限*/
    public static final String SELECT_INSTITUTION_QUETION_LIST_MANAGER = "MeasurementTeacher.selectManagerList";
 	/**查询结构评价权限*/
    public static final String SELECT_INSTITUTION_DBINFO_LIST_MANAGER = "MeasurementInstitution.selectManagerList";
    /**查询问卷结果权限*/
    public static final String SELECT_QUETION_RESULT_LIST_MANAGER = "UserMeasurementResult.select";
    /** 问卷 */
	public static final String MEASUREMENT_EVALUTION_SELECT = "MeasurementTeacher.select";
	/** 问卷题目 */
	public static final String QUESTION_EVALUTION_SELECT_VIEW = "QuestionTeacher.selectView";
	/** 问卷题目加选项 */
	public static final String QUESTION_EVALUTION_OPTION_SELECT = "QuestionOptionTeacher.select";
	/**更新评论选项*/
	public static final String QUESTION_EVALUTION_OPTION_UPDATE_NUM = "QuestionOptionTeacher.updateNum";
	/**问卷提交answer*/
	public static final String MEASUERMENT_ANSWER_INSERT = "UserMeasuermentAnswer.insert";
	/** 提交问卷问题参与人数加1 */
	public static final String QUESTION_EVALUTION_UPDATE_NUM = "QuestionTeacher.updateNumUser";
	/**问卷提交result*/
	public static final String MEASUERMENT_RESULT_INSERT = "UserMeasurementResult.insert";
	/**学校-问卷调查检索*/
    public static final String SQL_SELECT_MEASURE_RESULT_INFO_LIST="MeasurementTeacher.selectMeasureInfoList";
    /**老师首页-问卷详情-检索选项的最大值*/
    public static final String SQL_SELECT_INSTITUTION_MAX_OPTIONS_INFO_QUESTION="QuestionOptionTeacher.selectMaxOptionsByMeasureId";
    /** 检索评价用户答题情况 */
	public static final String SQL_INSTITUTION_MEASUERMENT_USER_SEARCH = "UserMeasurementResult.selectTeacherMeasurementUser";
	public static final String SQL_EVALUTION_MEASUERMENT_SEARCH_QUESTION_LIST_BY_MEASUREMENT = "MeasurementTeacher.selectQuestionResult";
	/** 检索问题 */
	public static final String MEASUERMENT_EVALUTION_SEARCH_QUESTION_OPTIONS_LIST = "QuestionOptionTeacher.select";

    
}
