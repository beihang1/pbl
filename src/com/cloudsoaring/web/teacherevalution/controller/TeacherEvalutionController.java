package com.cloudsoaring.web.teacherevalution.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.common.excel.ExcelDataBind;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.exception.BusinessException;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.UserEntity;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.utils.ConfigUtil;
import com.cloudsoaring.web.common.utils.JSONUtil;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.entity.UserMeasurementResultEntity;
import com.cloudsoaring.web.course.service.CourseService;
import com.cloudsoaring.web.institution.entity.InstitutionEntity;
import com.cloudsoaring.web.institution.entity.InstitutionMessageEntity;
import com.cloudsoaring.web.institution.entity.MeasurementInstitutionEntity;
import com.cloudsoaring.web.institution.entity.QuestionInstitutionEntity;
import com.cloudsoaring.web.institution.entity.QuestionOptionInstitutionEntity;
import com.cloudsoaring.web.institution.entity.TagInstitutionEntity;
import com.cloudsoaring.web.institution.entity.TagLinkInstitutionEntity;
import com.cloudsoaring.web.institution.service.TaxonomyInstitutionService;
import com.cloudsoaring.web.taxonomy.entity.TagInstitutionView;
import com.cloudsoaring.web.taxonomy.entity.TbInstitutionEvalutionManagerView;
import com.cloudsoaring.web.teacher.entity.TeacherEntity;
import com.cloudsoaring.web.teacher.service.TeacherService;
import com.cloudsoaring.web.teacherevalution.constant.EvalutionTeacherConstants;
import com.cloudsoaring.web.teacherevalution.entity.MeasurementTeacherEntity;
import com.cloudsoaring.web.teacherevalution.entity.QuestionOptionTeacherEntity;
import com.cloudsoaring.web.teacherevalution.entity.QuestionTeacherEntity;
import com.cloudsoaring.web.teacherevalution.entity.TbTeacherEvalutionEntity;
import com.cloudsoaring.web.teacherevalution.entity.TeacherEvalutionEntity;
import com.cloudsoaring.web.teacherevalution.service.TeacherEvalutionService;
import com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity;
import com.cloudsoaring.web.trainingplatform.service.UserManagementService;

/***
 * 课程模块相关请求、响应控制的Controller
 * 
 * @author ljl
 *
 */
@Controller
@RequestMapping("/pub/teacherevalution")
public class TeacherEvalutionController extends BaseController implements EvalutionTeacherConstants {
	@Autowired
	private CourseService courseService;
	@Autowired
	private TeacherEvalutionService teacherEvalutionService;
	@Autowired
	private TeacherService teacherService;
	@Autowired
	private UserManagementService userManagementService;
	@Autowired
	private TaxonomyInstitutionService taxonomyInstitutionService;
	
    @RequestMapping("/teacherevalutionManger.action")
 		public ModelAndView institutionManger() {
    	ModelAndView result = new ModelAndView();
    	UserEntity user = WebContext.getSessionUser();
    	if(!"游客".equals(user.getPersonName())) {
    		String role = user.getUserKbn();
        	if("A".equals(role) || "B".equals(role)) {
        		result = new ModelAndView("/teacherEvalution/manager/teacherEvalutionBack");
        	}else if("C".equals(role)) {
        		result = new ModelAndView("/teacherEvalution/manager/studentEvalutionBack");
        	}
    	}else {
    		result = new ModelAndView("/teacherEvalution/manager/studentEvalutionBack");
    	}
 		return result;
  }
	/**
	 * 机构基本信息
	 * @param condition
	 * @return
	 */
	@RequestMapping("/teacherEvalutionlist.action")
	public ModelAndView institutionlist(InstitutionEntity condition){
        ModelAndView result = new ModelAndView("/teacherEvalution/manager/teacherEvalution_list");
		condition.setPageSize(10);
        result.addObject("condition", condition);
		return result;
	}
    /**
     * 
     * 初始化课程列表
     * @param condition 课程对象
     * @return 返回课程列表初始化页面
     */
    @RequestMapping("/evalutionListInit.action")
    public ModelAndView institutionListInit(TbTeacherEvalutionEntity condition){
        ModelAndView result = new ModelAndView("/teacherEvalution/manager/teacherEvalution_evalution_list");
        condition.setPageSize(10);
        result.addObject("condition", condition);
        result.addObject("course", new TbTeacherEvalutionEntity());
        return result;
    }
    /**
     * 
     * 初始化评价问卷列表
     * @param condition 评价对象
     * @return 返回课程列表初始化页面
     */
     @RequestMapping("/teacherQuestListInit.action")
    public ModelAndView teacherQuestListInit(InstitutionMessageEntity condition){
        ModelAndView result = new ModelAndView("/teacherEvalution/manager/teacherEvalution_evalution_question_list");
        condition.setPageSize(10);
        result.addObject("condition", condition);
        return result;
    }
	/**
	 * 问卷调查结果列表
	 * @author heyaqin
	 * @return 返回结果
	 */
	@RequestMapping("/questionResultList.action")
	public ModelAndView questionResultList(MeasurementTeacherEntity condition){
		ModelAndView mv = new ModelAndView("/teacherEvalution/manager/teacherEvalution_evalution_question_result_list");
		mv.addObject("condition", condition);
		UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}
 	/**
	 * 检索班级信息
	 * @param entity
	 * @return 返回结果
	 */
	@RequestMapping("/searchTeacherEvalution.action")
	@ResponseBody
	public ResultBean searchNotice(TeacherEntity entity){
		return teacherEvalutionService.listSearch(entity);
	}
	/**
	 * 查看教师基本信息
	 * @param userId
	 * @return
	 */
	@RequestMapping("/edit.action")
	public ModelAndView edit(@RequestParam("userId")String userId){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		ModelAndView mv = new ModelAndView("/teacherEvalution/manager/teacher_edit");
		TeacherEntity entity = new TeacherEntity();
		entity.setUserId(userId);
		TeacherEntity teacher = teacherService.getTeacherByUserId(entity);
		if(teacher !=null) {
			mv.addObject("teacher", teacher);
			if(teacher.getBirthday() != null) {
				String birthDay = formatter.format(teacher.getBirthday());
				mv.addObject("birthDay", birthDay);
			}
			if(teacher.getGraduateTimeOfFirstDegree() != null) {
				String graduateTimeOfFirstDegree = formatter.format(teacher.getGraduateTimeOfFirstDegree());
				mv.addObject("graduateTimeOfFirstDegree", graduateTimeOfFirstDegree);
			}
			if(teacher.getGraduateTimeOfHighestDegree() != null) {
				String graduateTimeOfHighestDegree = formatter.format(teacher.getGraduateTimeOfHighestDegree());
				mv.addObject("graduateTimeOfHighestDegree", graduateTimeOfHighestDegree);
			}
			if(teacher.getWorkTime() != null) {
				String workTime = formatter.format(teacher.getWorkTime());
				mv.addObject("workTime", workTime);
			}
			if(teacher.getEducationWorkTime() != null) {
				String educationWorkTime = formatter.format(teacher.getEducationWorkTime());
				mv.addObject("educationWorkTime", educationWorkTime);
			}
		}else {
			TeacherEntity resultTeacher = new TeacherEntity();
			resultTeacher.setUserId(userId);
			resultTeacher.setUserName(WebContext.getSessionUser().getUserName());
			resultTeacher.setSex(WebContext.getSessionUser().getSex());
			resultTeacher.setPersonName(WebContext.getSessionUser().getPersonName());
			mv.addObject("teacher", resultTeacher);
		}
		return mv;
	}
    /**
     * 批量导出机构
* @param userId
* @return
* @throws Exception 
*/
	@RequestMapping("/exportInstitution.action")
	@ResponseBody
	public TeacherEvalutionEntity exportInstitution(TeacherEvalutionEntity entity) throws Exception{
		List<TeacherEvalutionEntity> viewList = new ArrayList<TeacherEvalutionEntity>();
		viewList = teacherEvalutionService.exportTeacher(entity);
		TeacherEvalutionEntity user = new TeacherEvalutionEntity();
		user.setTeacherList(viewList);
		ExcelDataBind edbUser = new ExcelDataBind(user);
		InputStream is = null;
		OutputStream os = null;
		try {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String date = sdf.format(new Date());
		String fileName = URLEncoder.encode("教师基本信息" + date +".xlsx", "UTF-8");
		WebContext.getResponse().reset();
		is = TeacherEvalutionService.class.getResourceAsStream("teacherExportTemplate.xlsx");
		WebContext.getResponse().setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
		WebContext.getResponse().setContentType(" application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8");
		edbUser.bind(is,WebContext.getResponse().getOutputStream());
		} catch(IOException ex){
		
		} finally{
		
		IOUtils.closeQuietly(is);
		IOUtils.closeQuietly(os);
		}
		return null;
	}
    /**
     * 
     * 检索评价列表
     * @param queryInfo 机构对象
     * @return 返回评价列表
     */
    @RequestMapping("/listSearch.action")
    @ResponseBody
    public ResultBean listSearch(TbTeacherEvalutionEntity queryInfo) {
        return teacherEvalutionService.lisAlltSearch(queryInfo);
    }
    /** 
     * 评价新增页面初始化
     * @return
     */
     @RequestMapping("/addInit.action")
     public ModelAndView addInit() {
         ModelAndView result = new ModelAndView("/teacherEvalution/manager/teacherEvalution_evalution_add");
         result.addObject("pageType", "add");
         result.addObject("course", new TbTeacherEvalutionEntity());
         return result;
     }
       /**
        * 搜素全部学校
		* @param query
		* @return
		*/
		@RequestMapping("/searchTeacherAll.action")
		@ResponseBody
		public ResultBean searchTeacherAll(TeacherEvalutionEntity query){
		return  teacherEvalutionService.getTeacherAll(query);
        }
	     /**
	      * 
	      * 根据学校ID，检索学校信息
	      * @return
	      */
	     @RequestMapping("/searchTeacher.action")
	     @ResponseBody
	     public ResultBean searchTeacher(String id) {
	    	 ResultBean result=ResultBean.success();
	         return teacherEvalutionService.searchTeacher(id);
	     }
	      /**
           * 暂存
			* @param courseJson
			* @return
			* @throws Exception
			*/
			@RequestMapping("/zancunEvalutionInfo.action")
			@ResponseBody
				public ResultBean zancunEvalutionInfo(String courseJson) throws Exception {
				TbTeacherEvalutionEntity course = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, TbTeacherEvalutionEntity.class);
				ResultBean result = ResultBean.success();
				course.setStatus("0");
				if(StringUtil.isEmpty(course.getId())) {
					teacherEvalutionService.insertCourseCatagory(course);
				} else {
					teacherEvalutionService.updateCourseAll(course);
				}
			return result;
        }
		     /**
             * 保存评价任务信息
			* @param courseJson 任务信息
			* @return 返回结果
			* @throws Exception 
			*/
			@RequestMapping("/saveEvalutionInfo.action")
			@ResponseBody
			public ResultBean saveEvalutionInfo(String courseJson) throws Exception {
				TbTeacherEvalutionEntity course = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), courseJson, TbTeacherEvalutionEntity.class);
			 ResultBean result = ResultBean.success();
			 course.setStatus("0");
			 if(StringUtil.isEmpty(course.getId())) {
					teacherEvalutionService.insertCourseCatagory(course);
				} else {
					teacherEvalutionService.updateCourseAll(course);
				}
			 return result;
			}
			 /**
             *删除评价任务信息
			* @param id
			* @return
			*/
			@RequestMapping("/deleteEvalution.action")
			@ResponseBody
			public ResultBean deleteEvalution(String id){
				ResultBean result = new ResultBean();
				teacherEvalutionService.deleteEvalution(id);
				result.setMessages(getMessage(MSG_S_DELETE));
				return result;
			}
		    /**
		     * 
		     * 课程发布/取消发布
		     * @param id 课程id
		     * @param status 课程状态：0 未发布 1 已发布
		     * @return 
		     * @throws Exception
		     */
		    @RequestMapping("/publicTeacher.action")
		    @ResponseBody
		    public ResultBean publicTeacher(String id,String status) throws Exception {
		    	teacherEvalutionService.publicTeacherEvalution(id, status);
		        return ResultBean.success(MSG_S_SUBMIT);
		    }
		    @RequestMapping("/editInit.action")
		     public ModelAndView editInit(String id) {
		         ModelAndView result = new ModelAndView("teacherEvalution/manager/teacherEvalution_evalution_add");
		         String status="0";
		         String type = "edit";
		         TbTeacherEvalutionEntity course = teacherEvalutionService.searchEvalutionById(id,type);
		         result.addObject("course", course);
		         result.addObject("pageType",type);
		         return result;
		     }
		     /**
		      *问卷添加
		      * @param courseId 评价任务ID
		      * @return
		      */
		     @RequestMapping("/measureInit.action")
		     @ResponseBody
		     public ModelAndView measureInit(String courseId){
		         ModelAndView result = new ModelAndView("teacherEvalution/manager/teacherEvalution_measure_add");
		         MeasurementTeacherEntity measure=new MeasurementTeacherEntity();
		         result.addObject("measure", measure);
		         result.addObject("courseId", courseId);
		         result.addObject("pageType","add");
		         return result;
		     }
		     /**
		       * 
		       * 问卷添加/编辑
		       * @param measureJson
		       * @return
		       * @throws Exception
		       */
		       @RequestMapping("/saveMeasure.action")
		       @ResponseBody
		       public ResultBean saveMeasure(String measureJson) throws Exception {
		    	   MeasurementTeacherEntity measure = JSONUtil.parse(JSONUtil.createObMapperInstance(ConfigUtil.getDateFormatDefualt()), measureJson, MeasurementTeacherEntity.class);
		           ResultBean result = ResultBean.success();
		           if(StringUtil.isEmpty(measure.getId())) {
		        	   teacherEvalutionService.saveMeasure(measure);
		           } else {
		        	   teacherEvalutionService.updateMeasure(measure);
		           }
		           return result;
		       }
		       /**
		        * 
		        * 检索评价问卷列表
		        * @param queryInfo 机构对象
		        * @return 返回评价列表
		        */
		       @RequestMapping("/listQestionSearch.action")
		       @ResponseBody
		       public ResultBean listQestionSearch(MeasurementTeacherEntity condition) {
		    	   if(StringUtils.isNotBlank(condition.getCourseTitle())) {
		    		   condition.setTitile(condition.getCourseTitle());
		    	   }
		           return teacherEvalutionService.listMeasurementSearch(condition);
		       }
		      	/**
		      	 * 问卷显示页面
		      	 */
		      	@RequestMapping("/question.action")
		      	public ModelAndView question(String linkId,String linkType) {
		      		ModelAndView result = new ModelAndView("/teacherEvalution/manager/teacherEvalution_evalution_questionner_item");
		      		result.addObject("linkId", linkId);
		      		result.addObject("linkType",linkType);
		      		return result;
		      	}
		    	/**
		    	 * 问卷题目选项
		    	 */
		    	@RequestMapping("/questionnaireDetial.action")
		    	@ResponseBody
		    	public ResultBean questionnaireDetial(String linkId,String linkType) {
		    		return teacherEvalutionService.questionnaireDetial(  linkId,  linkType);
		    	}
		    	/**
		    	 * 问卷提交
		    	 */
		    	@RequestMapping("/submitQuestionnaire.action")
		    	@ResponseBody
		    	public ResultBean submitQuestionnaire(String questionIds, String optionIds,	String measureId) {
		    		return teacherEvalutionService.submitQuestionnaire( questionIds,optionIds,measureId);
		    	}
		    	/**
		    	 * 问卷调查列表检索
		    	 * @author 
		    	 * @return 返回结果
		    	 */
		    	@RequestMapping("/questionResultListSearch.action")
		    	@ResponseBody
		    	public ResultBean questionResultListSearch(MeasurementTeacherEntity condition){
		    		return teacherEvalutionService.selectQuestionResultList(condition);
		    	}
		    	/**
		    	 * 问卷统计列表初始化
		    	 * @author 
		    	 * @param id 问卷ID
		    	 * @return 返回结果
		    	 */
		    	@RequestMapping("/viewMeasurementListInit.action")
		    	public ModelAndView viewMeasurementListInit(String id) {
		    		ModelAndView mv = new ModelAndView("/teacherEvalution/manager/teacherEvalution_evalution_question_statistics");
		    		int optionNum = teacherEvalutionService.searchInstitutionTotalOptions(id)+1;
		    		QuestionOptionTeacherEntity questionOptionEntity = new QuestionOptionTeacherEntity();
		    		questionOptionEntity.setMeasureId(id);
		    		mv.addObject("questionOptionEntity",questionOptionEntity);
		    		mv.addObject("optionNum",optionNum);
		    		//问卷统计结果列表
		    		ArrayList<Map<String, String>> optionMapList = userManagementService.selectTeacherOptionsList(id);
		    		mv.addObject("optionMapList",optionMapList);
		    		int num = courseService.getStuStatus();
		    		mv.addObject("num", num);
		    		UserEntity user = WebContext.getSessionUser();
		    		if(!user.getUserName().equals("guest")) {
		    			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
		    			if(roleList.size() > 0) {
		    				for(UserRoleEntity role : roleList) {
		    					if(role.getRoleId().equals("01")) {
		    						mv.addObject("role", "t");
		    					}
		    				}
		    			}else {
		    				mv.addObject("role", "s");
		    			}
		    		}
		    		return mv;
		    	}
		    	/**
		    	 * 问卷统计列表检索
		    	 * @author HEYAQIN
		    	 * @param id 问卷ID
		    	 * @return 返回结果
		    	 */
		    	@RequestMapping("/viewMeasurementList.action")
		    	@ResponseBody
		    	public ArrayList<Map<String, String>> viewMeasurementList(String id) {
		    		if (StringUtil.isEmpty(id)) {
		    			return null ;
		    		}else {
		    			ArrayList<Map<String, String>> optionMapList = userManagementService.selectInstitutionOptionsList(id);
		    			return optionMapList;
		    		}
		    	}
		    	
		    	/**
		    	 * 问卷参与人员 画面初始化
		    	 * @param condition
		    	 * @return
		    	 */
		    	@SuppressWarnings("unchecked")
		        @RequestMapping("/viewUserInit.action")
		        public ModelAndView viewUserInit(MeasurementTeacherEntity condition){
		            ModelAndView mv = new ModelAndView("/teacherEvalution/manager/teacherEvalution_evalution_question_user_list");
		            mv.addObject("measurementId", condition.getId());
		            List<MeasurementTeacherEntity> list = teacherEvalutionService.search(condition);
		            if(list != null && list.size() != 0) {
		                mv.addObject("condition", list.get(0));
		            } else {
		                throw new BusinessException("参数错误!");
		            }
		            int num = courseService.getStuStatus();
		    		mv.addObject("num", num);
		    		UserEntity user = WebContext.getSessionUser();
		    		if(!user.getUserName().equals("guest")) {
		    			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
		    			if(roleList.size() > 0) {
		    				for(UserRoleEntity role : roleList) {
		    					if(role.getRoleId().equals("01")) {
		    						mv.addObject("role", "t");
		    					}
		    				}
		    			}else {
		    				mv.addObject("role", "s");
		    			}
		    		}
		            return mv;
		        }
		    	/**
                 * 问卷参与人员 检索
			    * @param condition
			    * @return
			    */
			   @RequestMapping("/viewUserSearch.action")
			   @ResponseBody
			   public ResultBean viewUserSeacher(String  measurementId){
			       return userManagementService.selectTeacherEvalutionQuestionUserList(measurementId);
			   }
			    /**
                * 问卷参与人员回答详情
			    * @param condition
			    * @return
			    */
			   @RequestMapping("/viewDetailInit.action")
			   public ModelAndView viewDetailInit(UserMeasurementResultEntity condition){
			       ModelAndView mv = new ModelAndView("/teacherEvalution/manager/teacherEvalution_evalution_question_user_detail");
			       if(StringUtil.isEmpty(condition.getMeasurementId())){
			           throw new BusinessException("参数错误!");
			       }
			       
			       List<UserMeasurementResultEntity> list = teacherEvalutionService.searchInstitutionUserMeasurementResultByEntiy(condition);
			       if(list != null && list.size() != 0) {
			           mv.addObject("condition", list.get(0));
			       } else {
			           mv.addObject("condition", new UserMeasurementResultEntity());
			       }
			       return mv;
			   }
			      /**
                   * 问卷回答详情
				   * @param measurementId
				   * @param userId
				   * @return
				   */
				  @RequestMapping("/viewUserAnswerSearch.action")
				  @ResponseBody
				  public ResultBean viewUserAnswerSearch(String  measurementId,String userId){
				      return userManagementService.selectEvalutionAnswerUserList(measurementId,userId);
				  }
				    /**
	                 * 检索测试信息
				     * @param linkId
				     * @return 返回结果
				     */
				    @RequestMapping("/searchMeasuerment.action")
				    @ResponseBody
				    public ResultBean searchMeasuerment(String measurementId,String userId) {
				        if(StringUtil.isEmpty(measurementId)) {
				            return ResultBean.error(getMessage("参数错误!"));
				        }
				        ResultBean result = ResultBean.success();
				        List<QuestionTeacherEntity> questionList = teacherEvalutionService.searchMeasuermentInstitutionNormalAndExam(userId, measurementId);
				        result.setData(questionList);
				        return result;
				    }
				    /**
				        * 
				                             * 初始化评价问卷列表
				        * @param condition 评价对象
				        * @return 返回课程列表初始化页面
				        */
				       @RequestMapping("/institutionQuestListInit.action")
				       public ModelAndView institutionQuestListInit(MeasurementTeacherEntity condition){
				           ModelAndView result = new ModelAndView("teacherEvalution/manager/teacher_evalution_question_list");
				           condition.setPageSize(10);
				           result.addObject("condition", condition);
				           TagInstitutionView tagQuery = new TagInstitutionView();
				           List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
				           List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
				           result.addObject("catagoryTags", tags);
				           result.addObject("course", new MeasurementTeacherEntity());
				           result.addObject("tagsSecond", tagsSecond);
				           return result;
				       }
				   	/**
				   	 * 机构基本信息
				   	 * @param condition
				   	 * @return
				   	 */
				   	@RequestMapping("/studentEvalutionlist.action")
				   	public ModelAndView studentEvalutionlist(InstitutionEntity condition){
				           ModelAndView result = new ModelAndView("/teacherEvalution/manager/studentEvalution_list");
				   		condition.setPageSize(10);
				           result.addObject("condition", condition);
				   		return result;
				   	}
				    /**
				        * 
				                             * 初始化评价问卷列表
				        * @param condition 评价对象
				        * @return 返回课程列表初始化页面
				        */
				       @RequestMapping("/institutionYpQuestListInit.action")
				       public ModelAndView institutionYpQuestListInit(MeasurementTeacherEntity condition){
				           ModelAndView result = new ModelAndView("teacherEvalution/manager/teacher_yp_evalution_question_list");
				           condition.setPageSize(10);
				           result.addObject("condition", condition);
				           TagInstitutionView tagQuery = new TagInstitutionView();
				           List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
				           List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
				           result.addObject("catagoryTags", tags);
				           result.addObject("course", new MeasurementTeacherEntity());
				           result.addObject("tagsSecond", tagsSecond);
				           return result;
				       }
				       /**
				        * 
				                              * 检索评价问卷列表
				        * @param queryInfo 机构对象
				        * @return 返回评价列表
				        */
				       @RequestMapping("/listYpQestionSearch.action")
				       @ResponseBody
				       public ResultBean listYpQestionSearch(MeasurementTeacherEntity condition) {
				    	   if(StringUtils.isNotBlank(condition.getCourseTitle())) {
				    		   condition.setTitile(condition.getCourseTitle());
				    	   }
				           return teacherEvalutionService.listYpMeasurementSearch(condition);
				       }
				       /**
				        * 
				                             * 初始化评价问卷列表
				        * @param condition 评价对象
				        * @return 返回课程列表初始化页面
				        */
				       @RequestMapping("/institutionWpQuestListInit.action")
				       public ModelAndView institutionWpQuestListInit(MeasurementTeacherEntity condition){
				           ModelAndView result = new ModelAndView("teacherEvalution/manager/teacher_wp_evalution_question_list");
				           condition.setPageSize(10);
				           result.addObject("condition", condition);
				           TagInstitutionView tagQuery = new TagInstitutionView();
				           List<TagInstitutionView> tags = taxonomyInstitutionService.searchTagInstitutionList(tagQuery);
				           List<TagInstitutionView> tagsSecond = taxonomyInstitutionService.searchTagInstitutionSecondList(tagQuery);
				           result.addObject("catagoryTags", tags);
				           result.addObject("course", new MeasurementTeacherEntity());
				           result.addObject("tagsSecond", tagsSecond);
				           return result;
				       }
				       /**
				        * 
				                              * 检索评价问卷列表
				        * @param queryInfo 机构对象
				        * @return 返回评价列表
				        */
				       @RequestMapping("/listWpQestionSearch.action")
				       @ResponseBody
				       public ResultBean listWpQestionSearch(MeasurementTeacherEntity condition) {
				    	   if(StringUtils.isNotBlank(condition.getCourseTitle())) {
				    		   condition.setTitile(condition.getCourseTitle());
				    	   }
				           return teacherEvalutionService.listWpMeasurementSearch(condition);
				       }
}
