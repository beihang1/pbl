package com.cloudsoaring.web.teacher.constant;

public interface TeacherMsgNames {
    /**删除*/
    public static final String CENTER_DELETE="center.delete";
    /**编辑*/
    public static final String CENTER_EDIT="center.edit";
}
