package com.cloudsoaring.web.teacher.constant;

import com.cloudsoaring.web.common.dao.ibatis.plugin.MybatisConstant;
@MybatisConstant
public interface TeacherStateConstants {
    
    public static final String PERSON_SEX = "person_sex";
    
    public static final String POLITICS_STATUS = "politics_staus";
    
    public static final String MARITAL_STATUS = "marital_status";
    
    public static final String EDUCATION = "education";
    
    public static final String STATUS = "teacher_status";
    
    public static final String DEGREE = "teacher_degree";
    
    public static final String JOB_TITLE = "teacher_job_title";
    
}