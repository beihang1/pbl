package com.cloudsoaring.web.teacher.constant;

public interface TeacherConfigNames {

	/**每隔多长时间更新一次视频学习进度*/
	public static final String CONFIG_UPDATE_USER_STUDY_PROGRESS_TIME = "update.user.study.progress.time";
}
