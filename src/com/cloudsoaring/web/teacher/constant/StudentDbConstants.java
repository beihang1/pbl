package com.cloudsoaring.web.teacher.constant;

public interface StudentDbConstants {

    public static final String SELECT_STUDENT_LIST_MANAGER = "Student.selectManagerList";
    
    public static final String SELECT_STUDENT_FAMILY_MEMBER_LIST = "StudentFamilyMember.selectList";
    
    public static final String SELECT_STUDENT_CERTIFICATE_LIST = "StudentCertificate.selectList";
    
    public static final String DELETE_STUDENT_BY_ID = "Student.delete";
    
}
