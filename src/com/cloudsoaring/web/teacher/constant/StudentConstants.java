package com.cloudsoaring.web.teacher.constant;

public interface StudentConstants extends TeacherConfigNames, StudentDbConstants,
		TeacherLogConstants, TeacherDefaultValues, TeacherMsgNames,
		TeacherStateConstants {
	public static final String FILE_TYPE_HOMEWORK = "homework";
	public static final String SUM_KBN_EARN = "A";
	public static final String PLAN_CATEGORY_PLAN = "1";
	public static final String PLAN_CATEGORY_CASE = "2";
	public static final String FILE_TYPE_ZIP = "users";
}
