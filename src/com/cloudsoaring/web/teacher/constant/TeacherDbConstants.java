package com.cloudsoaring.web.teacher.constant;

public interface TeacherDbConstants {

    public static final String SELECT_TEACHER_LIST_MANAGER = "Teacher.selectManagerList";
    
    public static final String SELECT_TEACHER_FAMILY_MEMBER_LIST = "FamilyMember.selectList";
    
    public static final String SELECT_TEACHER_CERTIFICATE_LIST = "Certificate.selectList";
    
    public static final String DELETE_TEACHER_BY_ID = "Teacher.delete";
    
    
    public static final String SELECT_TEACHER_HONOR_CERTIFICATE_LIST = "TeacherHonorCertificate.selectList";
    
}
