package com.cloudsoaring.web.teacher.service;
import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.MD5Util;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.FileEntity;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.constant.Constants;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.teacher.constant.TeacherConstants;
import com.cloudsoaring.web.teacher.entity.CertificateEntity;
import com.cloudsoaring.web.teacher.entity.FamilyMemberEntity;
import com.cloudsoaring.web.teacher.entity.StudentFamilyMemberEntity;
import com.cloudsoaring.web.teacher.entity.TeacherEntity;
import com.cloudsoaring.web.trainingplatform.constants.TpDbConstants;
import com.cloudsoaring.web.trainingplatform.entity.TpUserEntity;

/***
 * 模块相关的业务处理Service
 * 
 * @author liuyanshuang
 *
 */
@Service
@SuppressWarnings("unchecked")
public class TeacherService extends FileService implements TeacherConstants {
	
	public ResultBean listSearch(TeacherEntity queryInfo) {
		return commonDao.searchList4Page(queryInfo, SELECT_TEACHER_LIST_MANAGER);
	}
	
	public ResultBean familyMemberList(FamilyMemberEntity entity) {
		return commonDao.searchList4Page(entity, SELECT_TEACHER_FAMILY_MEMBER_LIST);
	}
	
	public ResultBean certificateList(CertificateEntity entity) {
		return commonDao.searchList4Page(entity, SELECT_TEACHER_CERTIFICATE_LIST);
	}
	
	public void save(TeacherEntity entity) throws Exception {
		// 图片处理
		FileEntity pictureFile = processRequestFile("picture", Constants.FILE_TYPE_IMAGE);
		// 缩略图
		compress(pictureFile, false);

		if (pictureFile != null) {
			entity.setPictureId(pictureFile.getFileId());
		}
		if(!StringUtil.isNotEmpty(entity.getId()) || "".equals(entity.getId())) {
			String id = IDGenerator.genUID();
			entity.setId(id);
			entity.setUserId(WebContext.getSessionUserId());
			entity.setCreateTime(new Date());
			entity.setCreateUser(WebContext.getSessionUser().getUserId());
			entity.setStatus("0");
			/*TpUserEntity user = new TpUserEntity();
			user.setUserId(id);
			user.setUserName(entity.getUserName());
			user.setSex(entity.getSex());
			user.setPhone(entity.getTelephone());
			user.setEmail(entity.getEmail());
			user.setPassword(MD5Util.getPassword4MD5("123"));*/
			entity.setWorkAge(dayComparePrecise(entity.getWorkTime(),new Date()));
			entity.setSchoolAge(dayComparePrecise(entity.getEducationWorkTime(),new Date()));
			commonDao.insertData(entity, TpDbConstants.TEACHER_INSERT);
			//commonDao.insertData(user, TpDbConstants.USER_INSERT);
		}else {
			entity.setUpdateTime(new Date());
			entity.setUpdateUser(WebContext.getSessionUser().getUserId());
			entity.setWorkAge(dayComparePrecise(entity.getWorkTime(),new Date()));
			entity.setSchoolAge(dayComparePrecise(entity.getEducationWorkTime(),new Date()));
			commonDao.updateData(entity, TpDbConstants.TEACHER_UPDATE);
		}
	}
	
	public TeacherEntity getTeacherById(TeacherEntity entity) {
		return commonDao.searchOneData(entity, TpDbConstants.TEACHER_SELECT_BY_PK);
	}
	
	public TeacherEntity getTeacherByUserId(TeacherEntity entity) {
		//return commonDao.searchOneData(entity, TpDbConstants.TEACHER_SELECT_BY_PK);
		return commonDao.searchOneData(entity, TpDbConstants.TEACHER_SELECT_BY_USER_ID);
	}
	
	public TpUserEntity getUserByUserId(String userId) {
		TpUserEntity user = new TpUserEntity();
		user.setUserId(userId);
		return commonDao.searchOneData(user, TpDbConstants.USER_SEARCH_NAME_BY_USERID);
	}
	
	public void deleteTeacher(String id) {
		TeacherEntity entity = new TeacherEntity();
		entity.setId(id);
		commonDao.deleteData(entity, DELETE_TEACHER_BY_ID);
	}
	
	public CertificateEntity searchCertificateById(String id) {
		CertificateEntity entity = new CertificateEntity();
		entity.setId(id);
		return commonDao.searchOneData(entity, TpDbConstants.TEACHER_CERTIFICATE_SELECT_BY_PK);
	}
	
	public void deleteCertificate(String id) {
		CertificateEntity entity = new CertificateEntity();
		entity.setId(id);
		commonDao.deleteData(entity,TpDbConstants.TEACHER_CERTIFICATE_DELETE);
	}
	
	public void deleteFamilyMember(String id) {
		FamilyMemberEntity entity = new FamilyMemberEntity();
		entity.setId(id);
		commonDao.deleteData(entity,TpDbConstants.TEACHER_FAMILY_MEMBER_DELETE);
	}
	
	public void saveFamilyMember(FamilyMemberEntity entity) {
		if(!StringUtil.isNotEmpty(entity.getId()) || "".equals(entity.getId()) || entity.getId() == null) {
			entity.setId(IDGenerator.genUID());
			entity.setCreateTime(new Date());
			entity.setCreateUser(WebContext.getSessionUser().getUserId());
			commonDao.insertData(entity, TpDbConstants.TEACHER_FAMILY_MEMBER_INSERT);
		}else {
			entity.setUpdateUser(WebContext.getSessionUser().getUserId());
			commonDao.updateData(entity, TpDbConstants.TEACHER_FAMILY_MEMBER_UPDATE);
		}
	}
	
	public FamilyMemberEntity searchFamilyMemberById(String id) {
		FamilyMemberEntity entity = new FamilyMemberEntity();
		entity.setId(id);
		return commonDao.searchOneData(entity, TpDbConstants.TEACHER_FAMILY_MEMBER_SELECT_BY_PK);
	}
	
	public void saveCertificate(CertificateEntity entity) throws Exception {
		// 图片处理
		FileEntity pictureFile = processRequestFile("picture", Constants.FILE_TYPE_IMAGE);
		// 缩略图
		compress(pictureFile, false);

		if (pictureFile != null) {
			entity.setPictureId(pictureFile.getFileId());
		}
		if(!StringUtil.isNotEmpty(entity.getId()) || "".equals(entity.getId()) || entity.getId() == null) {
			entity.setId(IDGenerator.genUID());
			entity.setCreateTime(new Date());
			entity.setCreateUser(WebContext.getSessionUser().getUserId());
			commonDao.insertData(entity, TpDbConstants.TEACHER_CERTIFICATE_INSERT);
		}else {
			entity.setUpdateUser(WebContext.getSessionUser().getUserId());
			commonDao.updateData(entity, TpDbConstants.TEACHER_CERTIFICATE_UPDATE);
		}
	}
	
	
	
	/**
	 * 计算2个日期之间相差的  相差多少年月日
	 * 比如：2011-02-02 到  2017-03-02 相差 6年，1个月，0天
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	private static String dayComparePrecise(Date fromDate,Date toDate){
	    Calendar  from  =  Calendar.getInstance();
	    from.setTime(fromDate);
	    Calendar  to  =  Calendar.getInstance();
	    to.setTime(toDate);
	    int fromYear = from.get(Calendar.YEAR);
	    int toYear = to.get(Calendar.YEAR);
	    int year = toYear  -  fromYear;
	    return String.valueOf(year);
	}
	
	public static void main(String[] args) {
		Date date = new Date(118, 11, 02);
		System.out.println(dayComparePrecise(date,new Date()));
	}
}
