package com.cloudsoaring.web.teacher.service;
import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.MD5Util;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.FileEntity;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.constant.Constants;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseStateConstants;
import com.cloudsoaring.web.teacher.constant.TeacherConstants;
import com.cloudsoaring.web.teacher.entity.CertificateEntity;
import com.cloudsoaring.web.teacher.entity.FamilyMemberEntity;
import com.cloudsoaring.web.teacher.entity.StudentFamilyMemberEntity;
import com.cloudsoaring.web.teacher.entity.TeacherEntity;
import com.cloudsoaring.web.teacher.entity.TeacherHonorCertificateEntity;
import com.cloudsoaring.web.trainingplatform.constants.TpDbConstants;
import com.cloudsoaring.web.trainingplatform.entity.TpUserEntity;

/***
 * 模块相关的业务处理Service
 * 
 * @author liuyanshuang
 *
 */
@Service
@SuppressWarnings("unchecked")
public class TeacherHonorService extends FileService implements TeacherConstants {
	
	public ResultBean certificateList(TeacherHonorCertificateEntity entity) {
		return commonDao.searchList4Page(entity, SELECT_TEACHER_HONOR_CERTIFICATE_LIST);
	}
	
	public TeacherHonorCertificateEntity searchCertificateById(String id) {
		TeacherHonorCertificateEntity entity = new TeacherHonorCertificateEntity();
		entity.setId(id);
		return commonDao.searchOneData(entity, TpDbConstants.TEACHER_HONOR_CERTIFICATE_SELECT_BY_PK);
	}
	
	public void deleteCertificate(String id) {
		TeacherHonorCertificateEntity entity = new TeacherHonorCertificateEntity();
		entity.setId(id);
		commonDao.deleteData(entity,TpDbConstants.TEACHER_HONOR_CERTIFICATE_DELETE);
	}
	
	public void saveCertificate(TeacherHonorCertificateEntity entity) throws Exception {
		FileEntity resultData = processRequestFile("file1", CourseStateConstants.FILE_TYPE_NETDISK);
		entity.setFileId(resultData.getFileId());
		if(!StringUtil.isNotEmpty(entity.getId()) || "".equals(entity.getId()) || entity.getId() == null) {
			entity.setId(IDGenerator.genUID());
			entity.setTeacherId(WebContext.getSessionUserId());
			entity.setCreateTime(new Date());
			entity.setCreateUser(WebContext.getSessionUser().getUserId());
			commonDao.insertData(entity, TpDbConstants.TEACHER_HONOR_CERTIFICATE_INSERT);
		}else {
			entity.setUpdateUser(WebContext.getSessionUser().getUserId());
			commonDao.updateData(entity, TpDbConstants.TEACHER_HONOR_CERTIFICATE_UPDATE);
		}
	}
	
	
	
	
}
