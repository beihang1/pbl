package com.cloudsoaring.web.teacher.controller;

import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.teacher.constant.TeacherConstants;
import com.cloudsoaring.web.teacher.entity.CertificateEntity;
import com.cloudsoaring.web.teacher.entity.FamilyMemberEntity;
import com.cloudsoaring.web.teacher.entity.TeacherEntity;
import com.cloudsoaring.web.teacher.service.TeacherService;
import com.cloudsoaring.web.trainingplatform.entity.TpUserEntity;

@Controller
@RequestMapping("/pub/teacher")
public class TeacherController extends BaseController implements TeacherConstants {
	
	@Autowired
	private TeacherService teacherService;
	
	@RequestMapping("/listInit.action")
    public ModelAndView listInit(TeacherEntity condition){
        ModelAndView result = new ModelAndView("teacher/client/teacher_list");
        condition.setPageSize(10);
        result.addObject("condition", condition);
        return result;
    }
	
	@RequestMapping("/listSearch.action")
    @ResponseBody
    public ResultBean listSearch(TeacherEntity queryInfo) {
        return teacherService.listSearch(queryInfo);
    }
	
	@RequestMapping("/addInit.action")
	public ModelAndView addInit() {
		ModelAndView result = new ModelAndView("teacher/client/teacher_add");
        result.addObject("teacher", new TeacherEntity());
        return result;
	}
	
	@RequestMapping("/familyMembers.action")
	public ModelAndView familyMembers(String teacherId) {
		ModelAndView result = new ModelAndView("teacher/client/teacher_family_member");
		TeacherEntity teacher = new TeacherEntity();
		teacher.setId(teacherId);
		TeacherEntity entity = teacherService.getTeacherById(teacher);
        result.addObject("teacher",entity);
        return result;
	}
	
	@RequestMapping("/familyMembersView.action")
	public ModelAndView familyMembersView(String teacherId) {
		ModelAndView result = new ModelAndView("teacher/client/teacher_family_member_view");
		TeacherEntity teacher = new TeacherEntity();
		teacher.setId(teacherId);
		TeacherEntity entity = teacherService.getTeacherById(teacher);
        result.addObject("teacher",entity);
        return result;
	}
	
	@RequestMapping("/saveFamilyMember.action")
	@ResponseBody
	public ResultBean saveFamilyMember(FamilyMemberEntity teacher) {
		ResultBean result = new ResultBean();
		teacherService.saveFamilyMember(teacher);
		return result;
	}
	
	@RequestMapping("/familyMemberEdit.action")
	public ModelAndView familyMemberEdit(String id) {
		ModelAndView mv = new ModelAndView("teacher/client/teacher_family_member_edit");
		FamilyMemberEntity entity = teacherService.searchFamilyMemberById(id);
		TeacherEntity teachers  = new TeacherEntity();
		teachers.setId(entity.getTeacherId());
		TeacherEntity teacher = teacherService.getTeacherById(teachers);
		mv.addObject("teacher",teacher);
		mv.addObject("familyMember", entity);
		return mv;
	}
	
	@RequestMapping("/deleteFamilyMember.action")
	@ResponseBody
	public ResultBean deleteFamilyMember(String id) {
		ResultBean result = new ResultBean();
		teacherService.deleteFamilyMember(id);
		result.setMessages(getMessage(MSG_S_DELETE));
		return result;
	}
	
	
	@RequestMapping("/familyMemberList.action")
	@ResponseBody
	public ResultBean familyMemberList(String teacherId) {
		FamilyMemberEntity entity = new FamilyMemberEntity();
		entity.setTeacherId(teacherId);
		 return teacherService.familyMemberList(entity);
	}
	
	@RequestMapping("/certificate.action")
	public ModelAndView certificate(String teacherId) {
		ModelAndView result = new ModelAndView("teacher/client/teacher_certificate");
		TeacherEntity teacher = new TeacherEntity();
		teacher.setId(teacherId);
		TeacherEntity entity = teacherService.getTeacherById(teacher);
        result.addObject("teacher",entity);
        result.addObject("certificate",new CertificateEntity());
        return result;
	}
	
	@RequestMapping("/certificateView.action")
	public ModelAndView certificateView(String teacherId) {
		ModelAndView result = new ModelAndView("teacher/client/teacher_certificate_view");
		TeacherEntity teacher = new TeacherEntity();
		teacher.setId(teacherId);
		TeacherEntity entity = teacherService.getTeacherById(teacher);
        result.addObject("teacher",entity);
        result.addObject("certificate",new CertificateEntity());
        return result;
	}
	
	@RequestMapping("/certificateList.action")
	@ResponseBody
	public ResultBean certificateList(String teacherId) {
		CertificateEntity entity = new CertificateEntity();
		entity.setTeacherId(teacherId);
		 return teacherService.certificateList(entity);
	}
	
	@RequestMapping("/certificateEdit.action")
	public ModelAndView certificateEdit(String id) {
		ModelAndView mv = new ModelAndView("teacher/client/teacher_certificate_edit");
		CertificateEntity entity = teacherService.searchCertificateById(id);
		TeacherEntity teachers  = new TeacherEntity();
		teachers.setId(entity.getTeacherId());
		TeacherEntity teacher = teacherService.getTeacherById(teachers);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String timeString = formatter.format(entity.getGetTime());
		mv.addObject("teacher",teacher);
		mv.addObject("certificate", entity);
		mv.addObject("timeString", timeString);
		return mv;
	}
	
	@RequestMapping("/saveCertificate.action")
	@ResponseBody
	public ResultBean saveCertificate(CertificateEntity certificate) throws Exception {
		ResultBean result = new ResultBean();
		teacherService.saveCertificate(certificate);
		return result;
	}
	
	@RequestMapping("/deleteCertificate.action")
	@ResponseBody
	public ResultBean deleteCertificate(String id) {
		ResultBean result = new ResultBean();
		teacherService.deleteCertificate(id);
		result.setMessages(getMessage(MSG_S_DELETE));
		return result;
	}
	
	@RequestMapping("/save.action")
	@ResponseBody
	public ResultBean save(TeacherEntity teacher) throws Exception {
		ResultBean result = new ResultBean();
		teacherService.save(teacher);
		result.setData(teacher);
		return result;
	}
	
	@RequestMapping("/importInit.action")
	public String importInit(){
		return "teacher/client/teacher_import";
	}
	
	@RequestMapping("/edit.action")
	public ModelAndView edit(@RequestParam("userId")String userId){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		ModelAndView mv = new ModelAndView("teacher/client/teacher_edit");
		TeacherEntity entity = new TeacherEntity();
		entity.setUserId(userId);
		TeacherEntity teacher = teacherService.getTeacherByUserId(entity);
		if(teacher !=null) {
			mv.addObject("teacher", teacher);
			if(teacher.getBirthday() != null) {
				String birthDay = formatter.format(teacher.getBirthday());
				mv.addObject("birthDay", birthDay);
			}
			if(teacher.getGraduateTimeOfFirstDegree() != null) {
				String graduateTimeOfFirstDegree = formatter.format(teacher.getGraduateTimeOfFirstDegree());
				mv.addObject("graduateTimeOfFirstDegree", graduateTimeOfFirstDegree);
			}
			if(teacher.getGraduateTimeOfHighestDegree() != null) {
				String graduateTimeOfHighestDegree = formatter.format(teacher.getGraduateTimeOfHighestDegree());
				mv.addObject("graduateTimeOfHighestDegree", graduateTimeOfHighestDegree);
			}
			if(teacher.getWorkTime() != null) {
				String workTime = formatter.format(teacher.getWorkTime());
				mv.addObject("workTime", workTime);
			}
			if(teacher.getEducationWorkTime() != null) {
				String educationWorkTime = formatter.format(teacher.getEducationWorkTime());
				mv.addObject("educationWorkTime", educationWorkTime);
			}
		}else {
			TeacherEntity resultTeacher = new TeacherEntity();
			resultTeacher.setUserId(userId);
			resultTeacher.setUserName(WebContext.getSessionUser().getUserName());
			resultTeacher.setSex(WebContext.getSessionUser().getSex());
			resultTeacher.setPersonName(WebContext.getSessionUser().getPersonName());
			mv.addObject("teacher", resultTeacher);
		}
		return mv;
	}
	
	@RequestMapping("/view.action")
	public ModelAndView view(@RequestParam("userId")String userId){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		ModelAndView mv = new ModelAndView("teacher/client/teacher_view");
		TeacherEntity entity = new TeacherEntity();
		entity.setUserId(userId);
		TeacherEntity teacher = teacherService.getTeacherByUserId(entity);
		if(teacher !=null) {
			mv.addObject("teacher", teacher);
			if(teacher.getBirthday() != null) {
				String birthDay = formatter.format(teacher.getBirthday());
				mv.addObject("birthDay", birthDay);
			}
			if(teacher.getGraduateTimeOfFirstDegree() != null) {
				String graduateTimeOfFirstDegree = formatter.format(teacher.getGraduateTimeOfFirstDegree());
				mv.addObject("graduateTimeOfFirstDegree", graduateTimeOfFirstDegree);
			}
			if(teacher.getGraduateTimeOfHighestDegree() != null) {
				String graduateTimeOfHighestDegree = formatter.format(teacher.getGraduateTimeOfHighestDegree());
				mv.addObject("graduateTimeOfHighestDegree", graduateTimeOfHighestDegree);
			}
			if(teacher.getWorkTime() != null) {
				String workTime = formatter.format(teacher.getWorkTime());
				mv.addObject("workTime", workTime);
			}
			if(teacher.getEducationWorkTime() != null) {
				String educationWorkTime = formatter.format(teacher.getEducationWorkTime());
				mv.addObject("educationWorkTime", educationWorkTime);
			}
		}else {
			TeacherEntity resultTeacher = new TeacherEntity();
			TpUserEntity userInfo = teacherService.getUserByUserId(userId);
			resultTeacher.setUserId(userId);
			resultTeacher.setUserName(userInfo.getUserName());
			resultTeacher.setSex(userInfo.getUserName());
			resultTeacher.setPersonName(userInfo.getPersonName());
			mv.addObject("teacher", resultTeacher);
		}
		return mv;
	}
	
	@RequestMapping("/delete.action")
	@ResponseBody
	public ResultBean delete(String id){
		ResultBean result = new ResultBean();
		teacherService.deleteTeacher(id);
		result.setMessages(getMessage(MSG_S_DELETE));
		return result;
	}
}
