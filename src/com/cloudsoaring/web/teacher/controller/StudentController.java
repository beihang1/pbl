package com.cloudsoaring.web.teacher.controller;

import java.io.InputStream;
import java.text.SimpleDateFormat;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.teacher.constant.StudentConstants;
import com.cloudsoaring.web.teacher.entity.StudentCertificateEntity;
import com.cloudsoaring.web.teacher.entity.StudentEntity;
import com.cloudsoaring.web.teacher.entity.StudentFamilyMemberEntity;
import com.cloudsoaring.web.teacher.entity.StudentQualityReportEntity;
import com.cloudsoaring.web.teacher.service.StudentService;
import com.cloudsoaring.web.trainingplatform.entity.TpUserEntity;

@Controller
@RequestMapping("/pub/student")
public class StudentController extends BaseController implements StudentConstants {
	
	@Autowired
	private StudentService teacherService;
	
	@RequestMapping("/listInit.action")
    public ModelAndView listInit(StudentEntity condition){
        ModelAndView result = new ModelAndView("teacher/client/teacher_list");
        condition.setPageSize(10);
        result.addObject("condition", condition);
        return result;
    }
	
	@RequestMapping("/listSearch.action")
    @ResponseBody
    public ResultBean listSearch(StudentEntity queryInfo) {
        return teacherService.listSearch(queryInfo);
    }
	
	@RequestMapping("/addInit.action")
	public ModelAndView addInit() {
		ModelAndView result = new ModelAndView("teacher/client/student_add");
        result.addObject("teacher", new StudentEntity());
        return result;
	}
	
	@RequestMapping("/familyMembers.action")
	public ModelAndView familyMembers(String teacherId) {
		ModelAndView result = new ModelAndView("teacher/client/student_family_member");
		StudentEntity teacher = new StudentEntity();
		teacher.setId(teacherId);
		StudentEntity entity = teacherService.getStudentById(teacher);
        result.addObject("teacher",entity);
        return result;
	}
	
	@RequestMapping("/familyMembersView.action")
	public ModelAndView familyMembersView(String teacherId) {
		ModelAndView result = new ModelAndView("teacher/client/student_family_member_from_list");
		StudentEntity teacher = new StudentEntity();
		teacher.setId(teacherId);
		StudentEntity entity = teacherService.getStudentById(teacher);
        result.addObject("teacher",entity);
        return result;
	}
	
	@RequestMapping("/familyMemberEdit.action")
	public ModelAndView familyMemberEdit(String id) {
		ModelAndView mv = new ModelAndView("teacher/client/student_family_member_edit");
		StudentFamilyMemberEntity entity = teacherService.searchFamilyMemberById(id);
		StudentEntity teachers  = new StudentEntity();
		teachers.setId(entity.getStudentId());
		StudentEntity teacher = teacherService.getStudentById(teachers);
		mv.addObject("teacher",teacher);
		mv.addObject("familyMember", entity);
		return mv;
	}
	
	@RequestMapping("/deleteFamilyMember.action")
	@ResponseBody
	public ResultBean deleteFamilyMember(String id) {
		ResultBean result = new ResultBean();
		teacherService.deleteFamilyMember(id);
		result.setMessages(getMessage(MSG_S_DELETE));
		return result;
	}
	
	@RequestMapping("/saveFamilyMember.action")
	@ResponseBody
	public ResultBean saveFamilyMember(StudentFamilyMemberEntity teacher) {
		ResultBean result = new ResultBean();
		teacherService.saveFamilyMember(teacher);
		return result;
	}
	
	@RequestMapping("/familyMemberList.action")
	@ResponseBody
	public ResultBean familyMemberList(String teacherId) {
		StudentFamilyMemberEntity entity = new StudentFamilyMemberEntity();
		entity.setStudentId(teacherId);
		 return teacherService.familyMemberList(entity);
	}
	
	@RequestMapping("/certificate.action")
	public ModelAndView certificate(String teacherId) {
		ModelAndView result = new ModelAndView("teacher/client/student_certificate");
		StudentEntity teacher = new StudentEntity();
		teacher.setId(teacherId);
		StudentEntity entity = teacherService.getStudentById(teacher);
        result.addObject("teacher",entity);
        result.addObject("certificate",new StudentCertificateEntity());
        return result;
	}
	
	@RequestMapping("/certificateView.action")
	public ModelAndView certificateView(String teacherId) {
		ModelAndView result = new ModelAndView("teacher/client/student_certificate_from_list");
		StudentEntity teacher = new StudentEntity();
		teacher.setId(teacherId);
		StudentEntity entity = teacherService.getStudentById(teacher);
        result.addObject("teacher",entity);
        result.addObject("certificate",new StudentCertificateEntity());
        return result;
	}
	
	@RequestMapping("/certificateList.action")
	@ResponseBody
	public ResultBean certificateList(String teacherId) {
		StudentCertificateEntity entity = new StudentCertificateEntity();
		entity.setStudentId(teacherId);
		 return teacherService.certificateList(entity);
	}
	
	@RequestMapping("/certificateEdit.action")
	public ModelAndView certificateEdit(String id) {
		ModelAndView mv = new ModelAndView("teacher/client/student_certificate_edit");
		StudentCertificateEntity entity = teacherService.searchCertificateById(id);
		StudentEntity teachers  = new StudentEntity();
		teachers.setId(entity.getStudentId());
		StudentEntity teacher = teacherService.getStudentById(teachers);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String timeString = formatter.format(entity.getGetTime());
		mv.addObject("teacher",teacher);
		mv.addObject("certificate", entity);
		mv.addObject("timeString", timeString);
		return mv;
	}
	
	@RequestMapping("/saveCertificate.action")
	@ResponseBody
	public ResultBean saveCertificate(StudentCertificateEntity certificate) throws Exception {
		ResultBean result = new ResultBean();
		teacherService.saveCertificate(certificate);
		return result;
	}
	
	@RequestMapping("/deleteCertificate.action")
	@ResponseBody
	public ResultBean deleteCertificate(String id) {
		ResultBean result = new ResultBean();
		teacherService.deleteCertificate(id);
		result.setMessages(getMessage(MSG_S_DELETE));
		return result;
	}
	
	@RequestMapping("/save.action")
	@ResponseBody
	public ResultBean save(StudentEntity teacher) throws Exception {
		ResultBean result = new ResultBean();
		teacherService.save(teacher);
		result.setData(teacher);
		return result;
	}
	
	@RequestMapping("/importInit.action")
	public String importInit(){
		return "teacher/client/teacher_import";
	}
	
	@RequestMapping("/edit.action")
	public ModelAndView edit(@RequestParam("userId")String userId){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		ModelAndView mv = new ModelAndView("teacher/client/student_edit");
		StudentEntity entity = new StudentEntity();
		entity.setUserId(userId);
		StudentEntity teacher = teacherService.getStudentByUserId(entity);
		if(teacher !=null) {
			mv.addObject("teacher", teacher);
			if(teacher.getBirthday() != null) {
				String birthDay = formatter.format(teacher.getBirthday());
				mv.addObject("birthDay", birthDay);
			}
		}else {
			StudentEntity resultStudent = new StudentEntity();
			resultStudent.setUserId(userId);
			resultStudent.setUserName(WebContext.getSessionUser().getUserName());
			resultStudent.setSex(WebContext.getSessionUser().getSex());
			resultStudent.setPersonName(WebContext.getSessionUser().getPersonName());
			mv.addObject("teacher", resultStudent);
		}
		return mv;
	}
	
	@RequestMapping("/view.action")
	public ModelAndView view(@RequestParam("userId")String userId){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		ModelAndView mv = new ModelAndView("teacher/client/student_view");
		StudentEntity entity = new StudentEntity();
		entity.setUserId(userId);
		StudentEntity teacher = teacherService.getStudentByUserId(entity);
		if(teacher !=null) {
			mv.addObject("teacher", teacher);
			if(teacher.getBirthday() != null) {
				String birthDay = formatter.format(teacher.getBirthday());
				mv.addObject("birthDay", birthDay);
			}
		}else {
			StudentEntity resultStudent = new StudentEntity();
			TpUserEntity userInfo = teacherService.getUserByUserId(userId);
			resultStudent.setUserId(userId);
			resultStudent.setUserName(userInfo.getUserName());
			resultStudent.setSex(userInfo.getSex());
			resultStudent.setPersonName(userInfo.getPersonName());
			mv.addObject("teacher", resultStudent);
		}
		return mv;
	}
	
	@RequestMapping("/delete.action")
	@ResponseBody
	public ResultBean delete(String id){
		ResultBean result = new ResultBean();
		teacherService.deleteStudent(id);
		result.setMessages(getMessage(MSG_S_DELETE));
		return result;
	}
	
	@RequestMapping("/reportInit.action")
	public ModelAndView reportInit(StudentQualityReportEntity condition){
		ModelAndView mv = new ModelAndView("teacher/client/student_report_list");
		StudentQualityReportEntity report = new StudentQualityReportEntity();
		report.setUserId(condition.getUserId());
		mv.addObject("report", report);
		return mv;
	}
	
	@RequestMapping("/reportInitFromList.action")
	public ModelAndView reportInitFromList(StudentQualityReportEntity condition){
		ModelAndView mv = new ModelAndView("teacher/client/student_report_list_from_list");
		StudentQualityReportEntity report = new StudentQualityReportEntity();
		report.setUserId(condition.getUserId());
		mv.addObject("report", report);
		return mv;
	}
	
	@RequestMapping("/reportImportInit.action")
	public String reportImportInit(){
		return "teacher/client/student_report_import";
	}
	
	@RequestMapping("/reportList.action")
	@ResponseBody
	public ResultBean reportList(String userId){
		return teacherService.search4Page(userId);
	}
	
	@RequestMapping("/reportView.action")
	public ModelAndView reportView(String id){
		ModelAndView mv = new ModelAndView("teacher/client/student_report_view");
		StudentQualityReportEntity entity = new StudentQualityReportEntity();
		entity.setId(id);
		StudentQualityReportEntity report = teacherService.getStudentQualityReportById(entity);
		if(report !=null) {
			mv.addObject("report", report);
		}
		return mv;
	}
	
	
	@ResponseBody
	@RequestMapping("/doImport.action")
	public ResultBean doImport(@RequestParam("users") CommonsMultipartFile users) throws Exception{
		InputStream input = null;;
		try{
			input = users.getInputStream();
			return ResultBean.success().setMessages(getMessage(MSG_S_OPERATION)).setData(teacherService.doImport(input));
		}finally{
			IOUtils.closeQuietly(input);
		}
	}
}
