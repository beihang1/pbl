package com.cloudsoaring.web.teacher.controller;

import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.teacher.constant.TeacherConstants;
import com.cloudsoaring.web.teacher.entity.CertificateEntity;
import com.cloudsoaring.web.teacher.entity.TeacherEntity;
import com.cloudsoaring.web.teacher.entity.TeacherHonorCertificateEntity;
import com.cloudsoaring.web.teacher.service.TeacherHonorService;

@Controller
@RequestMapping("/pub/teacherHonor")
public class TeacherHonorController extends BaseController implements TeacherConstants {
	
	@Autowired
	private TeacherHonorService teacherHonorService;
	
	@RequestMapping("/honorList.action")
    public ModelAndView honorList(TeacherHonorCertificateEntity condition){
        ModelAndView result = new ModelAndView("teacherHonor/client/teacher_honor_list");
        condition.setPageSize(10);
        result.addObject("condition", condition);
        TeacherHonorCertificateEntity certificate = new TeacherHonorCertificateEntity();
        certificate.setTeacherId(condition.getTeacherId());
        result.addObject("certificate",certificate);
        return result;
    }
	
	@RequestMapping("/honorListFromList.action")
    public ModelAndView honorListFromList(TeacherHonorCertificateEntity condition){
        ModelAndView result = new ModelAndView("teacherHonor/client/teacher_honor_list_from_list");
        condition.setPageSize(10);
        result.addObject("condition", condition);
        TeacherHonorCertificateEntity certificate = new TeacherHonorCertificateEntity();
        certificate.setTeacherId(condition.getTeacherId());
        result.addObject("certificate",certificate);
        return result;
    }
	
	@RequestMapping("/trainingList.action")
    public ModelAndView trainingList(TeacherHonorCertificateEntity condition){
        ModelAndView result = new ModelAndView("teacherHonor/client/teacher_training_list");
        condition.setPageSize(10);
        result.addObject("condition", condition);
        TeacherHonorCertificateEntity certificate = new TeacherHonorCertificateEntity();
        certificate.setTeacherId(condition.getTeacherId());
        result.addObject("certificate",certificate);
        return result;
    }

	@RequestMapping("/trainingListFromList.action")
    public ModelAndView trainingListFromList(TeacherHonorCertificateEntity condition){
        ModelAndView result = new ModelAndView("teacherHonor/client/teacher_training_list_from_list");
        condition.setPageSize(10);
        result.addObject("condition", condition);
        TeacherHonorCertificateEntity certificate = new TeacherHonorCertificateEntity();
        certificate.setTeacherId(condition.getTeacherId());
        result.addObject("certificate",certificate);
        return result;
    }
	
	@RequestMapping("/scienttificPayoffsList.action")
    public ModelAndView scienttificPayoffsList(TeacherHonorCertificateEntity condition){
        ModelAndView result = new ModelAndView("teacherHonor/client/teacher_scienttificPayoffs_list");
        condition.setPageSize(10);
        result.addObject("condition", condition);
        TeacherHonorCertificateEntity certificate = new TeacherHonorCertificateEntity();
        certificate.setTeacherId(condition.getTeacherId());
        result.addObject("certificate",certificate);
        return result;
    }

	@RequestMapping("/scienttificPayoffsListFromList.action")
    public ModelAndView scienttificPayoffsListFromList(TeacherHonorCertificateEntity condition){
        ModelAndView result = new ModelAndView("teacherHonor/client/teacher_scienttificPayoffs_list_from_list");
        condition.setPageSize(10);
        result.addObject("condition", condition);
        TeacherHonorCertificateEntity certificate = new TeacherHonorCertificateEntity();
        certificate.setTeacherId(condition.getTeacherId());
        result.addObject("certificate",certificate);
        return result;
    }
	
	@RequestMapping("/annualAppraisalList.action")
    public ModelAndView annualAppraisalList(TeacherHonorCertificateEntity condition){
        ModelAndView result = new ModelAndView("teacherHonor/client/teacher_annualAppraisal_list");
        condition.setPageSize(10);
        result.addObject("condition", condition);
        TeacherHonorCertificateEntity certificate = new TeacherHonorCertificateEntity();
        certificate.setTeacherId(condition.getTeacherId());
        result.addObject("certificate",certificate);
        return result;
    }
	
	@RequestMapping("/annualAppraisalListFromList.action")
    public ModelAndView annualAppraisalListFromList(TeacherHonorCertificateEntity condition){
        ModelAndView result = new ModelAndView("teacherHonor/client/teacher_annualAppraisal_list_from_list");
        condition.setPageSize(10);
        result.addObject("condition", condition);
        TeacherHonorCertificateEntity certificate = new TeacherHonorCertificateEntity();
        certificate.setTeacherId(condition.getTeacherId());
        result.addObject("certificate",certificate);
        return result;
    }
	
	@RequestMapping("/certificateList.action")
	@ResponseBody
	public ResultBean certificateList(String type,String teacherId) {
		TeacherHonorCertificateEntity entity = new TeacherHonorCertificateEntity();
		entity.setTeacherId(teacherId);
		entity.setType(type);
		return teacherHonorService.certificateList(entity);
	}
	
	@RequestMapping("/certificateEdit.action")
	public ModelAndView certificateEdit(String id) {
		ModelAndView mv = null;
		TeacherHonorCertificateEntity entity = teacherHonorService.searchCertificateById(id);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String timeString = formatter.format(entity.getGetTime());
		if(entity.getType().equals("0")) {
			mv = new ModelAndView("teacherHonor/client/teacher_honor_certificate_edit");
		}else if(entity.getType().equals("1")) {
			mv = new ModelAndView("teacherHonor/client/teacher_training_certificate_edit");
		}else if(entity.getType().equals("2")) {
			mv = new ModelAndView("teacherHonor/client/teacher_scienttificPayoffs_certificate_edit");
		}else if(entity.getType().equals("3")) {
			mv = new ModelAndView("teacherHonor/client/teacher_annualAppraisal_certificate_edit");
		}
		mv.addObject("certificate", entity);
		mv.addObject("timeString", timeString);
		return mv;
	}
	
	@RequestMapping("/saveCertificate.action")
	@ResponseBody
	public ResultBean saveCertificate(TeacherHonorCertificateEntity certificate) throws Exception {
		ResultBean result = new ResultBean();
		teacherHonorService.saveCertificate(certificate);
		return result;
	}
	
	@RequestMapping("/deleteCertificate.action")
	@ResponseBody
	public ResultBean deleteCertificate(String id) {
		ResultBean result = new ResultBean();
		teacherHonorService.deleteCertificate(id);
		result.setMessages(getMessage(MSG_S_DELETE));
		return result;
	}
	
}
