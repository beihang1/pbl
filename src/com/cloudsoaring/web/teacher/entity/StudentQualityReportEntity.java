package com.cloudsoaring.web.teacher.entity;

import java.util.Date;

import javax.xml.bind.annotation.XmlTransient;

import com.cloudsoaring.common.annotation.ClassDefine;
import com.cloudsoaring.common.annotation.FieldDefine;
import com.cloudsoaring.web.common.entity.BaseEntity;

@ClassDefine
public class StudentQualityReportEntity extends BaseEntity{
	 @XmlTransient
	/**serialVersionUID*/
	private static final long serialVersionUID = 1L;
	private String userId;
	@FieldDefine(description="学籍编号", maxLength="50", columnIndex="0")
	private String studentNo;
	@FieldDefine(description="姓名", maxLength="50", columnIndex="1")
	private String userName;
	@FieldDefine(description="性别", maxLength="50", columnIndex="2")
	private String sex;
	@FieldDefine(description="年级", maxLength="50", columnIndex="3")
	private String gradeId;
	@FieldDefine(description="班级", maxLength="50", columnIndex="4")
	private String classId;
	@FieldDefine(description="语文", maxLength="10", columnIndex="5")
	private String chinese;
	@FieldDefine(description="数学", maxLength="10", columnIndex="6")
	private String math;
	@FieldDefine(description="品德与生活", maxLength="10", columnIndex="7")
	private String moralityAndLife;
	@FieldDefine(description="音乐", maxLength="10", columnIndex="8")
	private String music;
	@FieldDefine(description="体育", maxLength="10", columnIndex="9")
	private String sports;
	@FieldDefine(description="美术", maxLength="10", columnIndex="10")
	private String arts;
	@FieldDefine(description="书法", maxLength="10", columnIndex="11")
	private String calligraphy;
	@FieldDefine(description="综合实践活动", maxLength="10", columnIndex="12")
	private String spac;
	@FieldDefine(description="道德品质", maxLength="10", columnIndex="13")
	private String motalTrait;
	@FieldDefine(description="公民素质", maxLength="10", columnIndex="14")
	private String qualityCitizens;
	@FieldDefine(description="学习能力", maxLength="10", columnIndex="15")
	private String learningAbility;
	@FieldDefine(description="交流与合作", maxLength="10", columnIndex="16")
	private String eac;
	@FieldDefine(description="运动与健康", maxLength="10", columnIndex="17")
	private String sportAndHealth;
	@FieldDefine(description="审美与表现", maxLength="10", columnIndex="18")
	private String aae;
	@FieldDefine(description="总体评价", maxLength="255", columnIndex="19")
	private String overallEvaluation;
	@FieldDefine(description="学生自我评价", maxLength="255", columnIndex="20")
	private String selfEvaluation;
	@FieldDefine(description="获奖情况", maxLength="255", columnIndex="21")
	private String honorAndAward;
	@FieldDefine(description="教师评语", maxLength="255", columnIndex="22")
	private String teacherEvaluation;
	@FieldDefine(description="家长寄语", maxLength="255", columnIndex="23")
	private String patriarchEvaluation;
	private Date createTime;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getStudentNo() {
		return studentNo;
	}
	public void setStudentNo(String studentNo) {
		this.studentNo = studentNo;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getGradeId() {
		return gradeId;
	}
	public void setGradeId(String gradeId) {
		this.gradeId = gradeId;
	}
	public String getClassId() {
		return classId;
	}
	public void setClassId(String classId) {
		this.classId = classId;
	}
	public String getChinese() {
		return chinese;
	}
	public void setChinese(String chinese) {
		this.chinese = chinese;
	}
	public String getMath() {
		return math;
	}
	public void setMath(String math) {
		this.math = math;
	}
	public String getMoralityAndLife() {
		return moralityAndLife;
	}
	public void setMoralityAndLife(String moralityAndLife) {
		this.moralityAndLife = moralityAndLife;
	}
	public String getMusic() {
		return music;
	}
	public void setMusic(String music) {
		this.music = music;
	}
	public String getSports() {
		return sports;
	}
	public void setSports(String sports) {
		this.sports = sports;
	}
	public String getArts() {
		return arts;
	}
	public void setArts(String arts) {
		this.arts = arts;
	}
	public String getCalligraphy() {
		return calligraphy;
	}
	public void setCalligraphy(String calligraphy) {
		this.calligraphy = calligraphy;
	}
	public String getSpac() {
		return spac;
	}
	public void setSpac(String spac) {
		this.spac = spac;
	}
	public String getMotalTrait() {
		return motalTrait;
	}
	public void setMotalTrait(String motalTrait) {
		this.motalTrait = motalTrait;
	}
	public String getQualityCitizens() {
		return qualityCitizens;
	}
	public void setQualityCitizens(String qualityCitizens) {
		this.qualityCitizens = qualityCitizens;
	}
	public String getLearningAbility() {
		return learningAbility;
	}
	public void setLearningAbility(String learningAbility) {
		this.learningAbility = learningAbility;
	}
	public String getEac() {
		return eac;
	}
	public void setEac(String eac) {
		this.eac = eac;
	}
	public String getSportAndHealth() {
		return sportAndHealth;
	}
	public void setSportAndHealth(String sportAndHealth) {
		this.sportAndHealth = sportAndHealth;
	}
	public String getAae() {
		return aae;
	}
	public void setAae(String aae) {
		this.aae = aae;
	}
	public String getOverallEvaluation() {
		return overallEvaluation;
	}
	public void setOverallEvaluation(String overallEvaluation) {
		this.overallEvaluation = overallEvaluation;
	}
	public String getSelfEvaluation() {
		return selfEvaluation;
	}
	public void setSelfEvaluation(String selfEvaluation) {
		this.selfEvaluation = selfEvaluation;
	}
	public String getHonorAndAward() {
		return honorAndAward;
	}
	public void setHonorAndAward(String honorAndAward) {
		this.honorAndAward = honorAndAward;
	}
	public String getTeacherEvaluation() {
		return teacherEvaluation;
	}
	public void setTeacherEvaluation(String teacherEvaluation) {
		this.teacherEvaluation = teacherEvaluation;
	}
	public String getPatriarchEvaluation() {
		return patriarchEvaluation;
	}
	public void setPatriarchEvaluation(String patriarchEvaluation) {
		this.patriarchEvaluation = patriarchEvaluation;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
