/**
 * 
 */
package com.cloudsoaring.web.points.entity;

import java.math.BigDecimal;


/**
 *Copyright (c),2015-2016, Cloudsoaring
 *<br/>This program is protected by copyright laws; 
 *<br/>Program Name: <b>CSWEB<b>
 *<br/>
 *<br/>(用一句话描述该类的功能，替换括号及括号内的文字)
 *<br/>2016年3月14日 上午11:09:25
 *@author adlay
 *@since JDK 1.6
 *@version 1.0
 */
public class UserPoint {
	
	private boolean status;
	private String messages;
	private String errorCode;

	//用户
	private String userId;
	//可用积分
	private BigDecimal points;
	//历史总积分
	private BigDecimal totalPoints; 
	//积分流水号
	private String pointsSerialNo;
	
	public UserPoint(){
		
	}
	
	public UserPoint(String userId){
		this.userId = userId;
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public BigDecimal getPoints() {
		return points;
	}

	public void setPoints(BigDecimal points) {
		this.points = points;
	}

	public BigDecimal getTotalPoints() {
		return totalPoints;
	}

	public void setTotalPoints(BigDecimal totalPoints) {
		this.totalPoints = totalPoints;
	}

	public String getPointsSerialNo() {
		return pointsSerialNo;
	}

	public void setPointsSerialNo(String pointsSerialNo) {
		this.pointsSerialNo = pointsSerialNo;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getMessages() {
		return messages;
	}

	public void setMessages(String messages) {
		this.messages = messages;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	
	public void fill(UserPoint userPoint){
		points = userPoint.points;
		userId = userPoint.userId;
		totalPoints = userPoint.totalPoints;
		pointsSerialNo = userPoint.pointsSerialNo;
	}
}
