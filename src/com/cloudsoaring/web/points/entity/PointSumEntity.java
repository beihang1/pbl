package com.cloudsoaring.web.points.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_point_sum
 * @date 2016-03-14
 * @version V1.0
 *
 */
public class PointSumEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/***/
	private String userId;
	/***/
	private BigDecimal sumPoints;
	/***/
	private java.util.Date expirationDate;
	
	public PointSumEntity(String userId, Date ex){
		this.userId = userId;
		this.expirationDate = ex;
	}
	
	public PointSumEntity(String userId){
		this.userId = userId;
	}
	
	public PointSumEntity(){
		
	}
	
	/***/
	public String getUserId(){
		return this.userId;
	}
	/***/
	public void setUserId(String userId){
		this.userId = userId;
	}
	/***/
	public BigDecimal getSumPoints(){
		return this.sumPoints;
	}
	/***/
	public void setSumPoints(BigDecimal sumPoints){
		this.sumPoints = sumPoints;
	}
	/***/
	public java.util.Date getExpirationDate(){
		return this.expirationDate;
	}
	/***/
	public void setExpirationDate(java.util.Date expirationDate){
		this.expirationDate = expirationDate;
	}
}
