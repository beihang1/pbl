/**
 * 
 */
package com.cloudsoaring.web.points.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.xml.bind.annotation.XmlTransient;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**
 *Copyright (c),2015-2016, Cloudsoaring
 *<br/>This program is protected by copyright laws; 
 *<br/>Program Name: <b>CSWEB<b>
 *<br/>
 *<br/>(用一句话描述该类的功能，替换括号及括号内的文字)
 *<br/>2016年3月14日 上午11:09:25
 *@author adlay
 *@since JDK 1.6
 *@version 1.0
 */
public class UserPointEntity extends BaseEntity {

	//用户
	private String userId;
	//操作积分
	private BigDecimal points;
	//失效日期
	private BigDecimal totalPoints; 
	
	public UserPointEntity(){
		
	}
	
	public UserPointEntity(String userId){
		this.userId = userId;
	}
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public BigDecimal getPoints() {
		return points;
	}

	public void setPoints(BigDecimal points) {
		this.points = points;
	}

	public BigDecimal getTotalPoints() {
		return totalPoints;
	}

	public void setTotalPoints(BigDecimal totalPoints) {
		this.totalPoints = totalPoints;
	}
	
	public UserPoint get(){
		UserPoint up = new UserPoint();
		up.setUserId(userId);
		up.setTotalPoints(totalPoints);
		up.setPoints(points);
		return up;
	}
}
