package com.cloudsoaring.web.points.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_point_earn
 * @date 2016-03-14
 * @version V1.0
 *
 */
public class PointHisEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/***/
	private String seqNo;
	/***/
	private String userId;
	/***/
	private String typeCode;
	/***/
	private BigDecimal points;
	private BigDecimal requestPoints;
	private BigDecimal resultPoints;
	/***/
	private java.util.Date expirationDate;
	/***/
	private String businessId;
	private String remark;
	private Date createDateStart;
	private Date createDateEnd;
	private String sumKbn;
	private String systemCode;
	
	/***/
	public String getSeqNo(){
		return this.seqNo;
	}
	/***/
	public void setSeqNo(String seqNo){
		this.seqNo = seqNo;
	}
	/***/
	public String getUserId(){
		return this.userId;
	}
	/***/
	public void setUserId(String userId){
		this.userId = userId;
	}
	/***/
	public String getTypeCode(){
		return this.typeCode;
	}
	/***/
	public void setTypeCode(String typeCode){
		this.typeCode = typeCode;
	}
	/***/
	public BigDecimal getPoints(){
		return this.points;
	}
	/***/
	public void setPoints(BigDecimal points){
		this.points = points;
	}
	
	public BigDecimal getRequestPoints() {
		return requestPoints;
	}
	public void setRequestPoints(BigDecimal requestPoints) {
		this.requestPoints = requestPoints;
	}
	/***/
	public java.util.Date getExpirationDate(){
		return this.expirationDate;
	}
	/***/
	public void setExpirationDate(java.util.Date expirationDate){
		this.expirationDate = expirationDate;
	}
	/***/
	public String getBusinessId(){
		return this.businessId;
	}
	/***/
	public void setBusinessId(String businessId){
		this.businessId = businessId;
	}
	public Date getCreateDateStart() {
		return createDateStart;
	}
	public void setCreateDateStart(Date createDateStart) {
		this.createDateStart = createDateStart;
	}
	public Date getCreateDateEnd() {
		return createDateEnd;
	}
	public void setCreateDateEnd(Date createDateEnd) {
		this.createDateEnd = createDateEnd;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getSumKbn() {
		return sumKbn;
	}
	public void setSumKbn(String sumKbn) {
		this.sumKbn = sumKbn;
	}
	public String getSystemCode() {
		return systemCode;
	}
	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}
	public BigDecimal getResultPoints() {
		return resultPoints;
	}
	public void setResultPoints(BigDecimal resultPoints) {
		this.resultPoints = resultPoints;
	}
}
