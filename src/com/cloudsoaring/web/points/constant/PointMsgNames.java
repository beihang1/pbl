/**
 * 
 */
package com.cloudsoaring.web.points.constant;

/**
 *Copyright (c),2015-2016, Cloudsoaring
 *<br/>This program is protected by copyright laws; 
 *<br/>Program Name: <b>CSWEB<b>
 *<br/>
 *<br/>积分系统消息定义
 *<br/>2016年3月14日 下午3:51:13
 *@author adlay
 *@since JDK 1.6
 *@version 1.0
 */
public interface PointMsgNames {

	public static final String MSG_POINT_TYPE_UNSUPPORT = "point.type.unsupport";
	public static final String MSG_POINT_DAY_LIMIT = "point.day.limit";
	public static final String MSG_POINT_MONTH_LIMIT = "point.month.limit";
	public static final String MSG_POINT_YEAR_LIMIT = "point.year.limit";
	public static final String MSG_POINT_NOT_ENOUGH = "point.not.enough";
	public static final String MSG_POINT_EARN_SUCCESS = "point.earn.success";
	public static final String MSG_POINT_CONSUME_SUCCESS = "point.consume.success";
}
