/**
 * 
 */
package com.cloudsoaring.web.points.constant;

import com.cloudsoaring.web.common.constant.Constants;
import com.cloudsoaring.web.common.dao.ibatis.plugin.MybatisConstant;

/**
 *Copyright (c),2015-2016, Cloudsoaring
 *<br/>This program is protected by copyright laws; 
 *<br/>Program Name: <b>CSWEB<b>
 *<br/>
 *<br/>积分系统常量定义
 *<br/>2016年3月14日 下午1:24:29
 *@author adlay
 *@since JDK 1.6
 *@version 1.0
 */
public interface PointConstants extends Constants, PointConfigName, PointDbConstants, PointMsgNames {

	//积分按天汇总
	public static final String POINT_SUM_PERIOD_DAY = "D";
	//积分按月汇总
	public static final String POINT_SUM_PERIOD_MONTH = "M";
	//积分按年汇总
	public static final String POINT_SUM_PERIOD_YEAR = "Y";
	//积分统一汇总
	public static final String POINT_SUM_PERIOD_NONE = "N";
	//汇总区分：增加
	public static final String SUM_KBN_EARN = "A";
	//汇总区分：减少
	public static final String SUM_KBN_CONSUME = "D";
	//积分发放流水号前缀
	public static final String SEQ_PRE_POINT_EARN = "PEARN";
	//积分消耗流水号前缀
	public static final String SEQ_PRE_POINT_CONSUME = "PCONS";
	public static final int POINT_SEQ_NUM_LENGTH = 9;
	
	//系统编码-hackerspace
	public static final String SYSTEM_CODE_HS = "HS";
}
