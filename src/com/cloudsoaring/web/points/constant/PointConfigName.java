/**
 * 
 */
package com.cloudsoaring.web.points.constant;

/**
 *Copyright (c),2015-2016, Cloudsoaring
 *<br/>This program is protected by copyright laws; 
 *<br/>Program Name: <b>CSWEB<b>
 *<br/>
 *<br/>积分系统的配置定义
 *<br/>2016年3月14日 下午1:24:42
 *@author adlay
 *@since JDK 1.6
 *@version 1.0
 */
public interface PointConfigName {

	//积分汇总周期
	public static final String CONFIG_POINT_SUM_PERIOD = "point.sum.period";
}
