/**
 * 
 */
package com.cloudsoaring.web.points.constant;

/**
 *Copyright (c),2015-2016, Cloudsoaring
 *<br/>This program is protected by copyright laws; 
 *<br/>Program Name: <b>CSWEB<b>
 *<br/>
 *<br/> 积分系统数据库语句定义
 *<br/>2016年3月14日 下午2:27:39
 *@author adlay
 *@since JDK 1.6
 *@version 1.0
 */
public interface PointDbConstants {

	public static final String SQL_USER_POINT_REFRESH = "UserPoint.refresh";
	
	public static final String SQL_USER_POINT_LOCK = "UserPoint.lockUserPoint";
	
	public static final String SQL_USER_POINT_SELECT_BY_KEY = "UserPoint.selectByPk";
	
	public static final String SQL_POINT_SUM_SELECT_EARLIEST = "PointSum.selectEarliest";
	
	public static final String SQL_POINT_SUM_SELECT_NOTEXPIRATION = "PointSum.selectNotExpiration";
	
	public static final String SQL_USER_POINT_INSERT = "UserPoint.insert";

	public static final String SQL_POINT_SUM_SELECT_BY_USEREXPIRATION = "PointSum.selectByUserExpiration";
	
	public static final String SQL_POINT_SUM_DELETE_EXPIRATION = "PointSum.deleteExpiration";
	
	public static final String SQL_POINT_SUM_SELECT_FOR_DELETE_EXPIRATION = "PointSum.select4deleteExpiration";
	
	public static final String SQL_POINT_EARN_SELECT_SUM_POINTS = "PointEarn.selectSumPoints";
	
	public static final String SQL_POINT_CONSUME_SELECT_SUM_POINTS = "PointConsume.selectSumPoints";
}
