/**
 * 
 */
package com.cloudsoaring.web.points.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.BigDecimalUtil;
import com.cloudsoaring.common.utils.DateUtil;
import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.web.common.service.BaseService;
import com.cloudsoaring.web.common.utils.ConfigUtil;
import com.cloudsoaring.web.points.constant.PointConstants;
import com.cloudsoaring.web.points.entity.PointConsumeEntity;
import com.cloudsoaring.web.points.entity.PointEarnEntity;
import com.cloudsoaring.web.points.entity.PointSumEntity;
import com.cloudsoaring.web.points.entity.PointTypeEntity;
import com.cloudsoaring.web.points.entity.UserPoint;
import com.cloudsoaring.web.points.entity.UserPointEntity;

/**
 *Copyright (c),2015-2016, Cloudsoaring
 *<br/>This program is protected by copyright laws; 
 *<br/>Program Name: <b>CSWEB<b>
 *<br/>
 *<br/> 积分处理逻辑
 *<br/>2016年3月14日 上午11:08:13
 *@author adlay
 *@since JDK 1.6
 *@version 1.0
 */
@Service
@WebService
public class PointService extends BaseService implements PointConstants {

	@Autowired
	private UserPointService UserPointEntityService;
	
	/**
	 * 刷新并获取用户积分
	 * @param userId 用户ID
	 * @return 返回用户积分信息
	 * @author adlay
	 */
	@WebMethod
	@WebResult(name="result")
	public UserPoint fetchPoint(@WebParam(name="useId")String userId){
		//用户积分锁定
		lockUserPointEntity(userId);
		//刷新后返回
		UserPointEntity entity = refreshUserPointEntity(userId, new BigDecimal(0));
		UserPoint result =entity.get();
		result.setStatus(true);
		return result;
	}
	
	@WebMethod
	public String getPointsPerSmallChange(){
		return ConfigUtil.getProperty("POINTS_FOR_SMALL_CHANGE");
	}
	
	private UserPointEntity lockUserPointEntity(String userId){
		//刷新
		UserPointEntity uc = new UserPointEntity(userId);
		UserPointEntity up = searchOneData(uc, SQL_USER_POINT_LOCK);
		if(up == null){
			try{
//				UserPointEntityService.doCommitUserPointEntity(userId);
				uc.setTotalPoints(new BigDecimal(0));
				uc.setPoints(new BigDecimal(0));
				commonDao.insertData(uc, SQL_USER_POINT_INSERT);
			}catch(Exception ex){
				System.out.println(ex);
			}
			up = searchOneData(uc, SQL_USER_POINT_LOCK);
		}
		return up;
	}
	
	private UserPointEntity refreshUserPointEntity(String userId, BigDecimal points){
		//刷新
		UserPointEntity up = new UserPointEntity(userId);
		//up.setTotalPoints(points);
		this.update(up, SQL_USER_POINT_REFRESH);
		//删除过期的sum
		List<String> list = (List<String>) search(up, SQL_POINT_SUM_SELECT_FOR_DELETE_EXPIRATION);
		for(String id : list){
			PointSumEntity one = new PointSumEntity();
			one.setId(id);
			//this.delete(one);	
			
		}
		UserPointEntity entity = searchOneData(up, SQL_USER_POINT_LOCK);
		return entity;
	}
	
	/**
	 * 计算汇总失效日期
	 * @param realExpiratioNDate
	 * @param sumPeriod
	 * @return
	 * @author adlay
	 */
	private Date getSumExpirationDate(Date realExpiratioNDate, String sumPeriod){
		if(realExpiratioNDate != null){
			if(POINT_SUM_PERIOD_DAY.equals(sumPeriod)){
				return realExpiratioNDate;
			}else if(POINT_SUM_PERIOD_MONTH.equals(sumPeriod)){
				return DateUtil.getMonthLastDay(realExpiratioNDate);
			}else if(POINT_SUM_PERIOD_DAY.equals(sumPeriod)){
				return DateUtil.getYearLastDay(realExpiratioNDate);
			}
		}
		return null;
	}
	
	/**
	 * 获得积分
	 * @param userId 用户ID
	 * @param systemCode 获取积分的业务类型
	 * @param typeCode 获取积分的业务类型
	 * @param point 发放的积分点数，可以为空，若该业务类型已指定积分，则以类型指定的积分为获取的积分
	 * @return 返回操作结果及用户剩余的积分
	 * @author adlay
	 */
	@WebMethod
	@WebResult(name="result")
	public UserPoint earnPoint(@WebParam(name="systemCode")String systemCode, 
			@WebParam(name="useId")String userId, 
			@WebParam(name="typeCode")String typeCode, 
			@WebParam(name="points")BigDecimal points,
			@WebParam(name="businessId")String businessId,
			@WebParam(name="remark")String remark){
		UserPoint result = new UserPoint();
		result.setStatus(true);
		PointEarnEntity pe = new PointEarnEntity();
		BigDecimal rps = points;
		UserPoint up = null;
		if(points != null &&  points.compareTo(new BigDecimal(0)) > 0){
			//用户积分锁定
			lockUserPointEntity(userId);
			PointTypeEntity ptc = new PointTypeEntity(systemCode, typeCode); 
			ptc.setSumKbn(SUM_KBN_EARN);
			PointTypeEntity ptr = this.searchOneData(ptc);
			//该类型的积分操作是否支持
			if(ptr == null){
				ptr = new PointTypeEntity(systemCode, typeCode); 
				ptr.setValidity(0);
//				result.setStatus(false);
//				result.setMessages(getMessage(MSG_POINT_TYPE_UNSUPPORT));
//				return result;
			} 
			//往积分发放表中插入一条记录
			pe.setUserId(userId);
			pe.setRequestPoints(points);
			pe.setBusinessId(businessId);
			pe.setRemark(remark);
			pe.setSystemCode(systemCode);
			pe.setTypeCode(typeCode);
			//流水号
			pe.setSeqNo(getSeqNo(SEQ_PRE_POINT_EARN, POINT_SEQ_NUM_LENGTH));
			//系统定义的该类型操作的积分优先级更高
			rps = ptr.getPoints() != null 
					&& ptr.getPoints().compareTo(new BigDecimal(0)) > 0 ? ptr.getPoints() : points;
			rps = rps.abs();
			Date now = new Date();
			//当天有限制
			if(ptr.getLimitDaily() != null && ptr.getLimitDaily().compareTo(new BigDecimal(0))  > 0){
				pe.setCreateDateStart(DateUtil.getDayStart(now));
				pe.setCreateDateEnd(DateUtil.getTomorrowStart(now));
				Long daySum = (Long)searchOneData(pe, SQL_POINT_EARN_SELECT_SUM_POINTS);
				if(daySum == null){
					daySum = 0L;
				}
				//允许增加的积分
				rps = BigDecimalUtil.min(ptr.getLimitDaily().subtract(new BigDecimal(daySum)), rps);
			}
			if(rps.compareTo(new BigDecimal(0)) <= 0){
				rps = new BigDecimal(0);
				result.setStatus(false);
				result.setMessages(getMessage(MSG_POINT_DAY_LIMIT, ptr.getTypeName()));
			}else if(ptr.getLimitMonth() != null &&  ptr.getLimitMonth().compareTo(new BigDecimal(0)) >0){
				//当月有限制
				pe.setCreateDateStart(DateUtil.getMonthFirstDay(now));
				pe.setCreateDateEnd(DateUtil.addDays(DateUtil.getMonthLastDay(now),1));
				Long monthSum = (Long)searchOneData(pe, SQL_POINT_EARN_SELECT_SUM_POINTS);
				if(monthSum == null){
					monthSum = 0L;
				}
				//允许增加的积分
				rps = BigDecimalUtil.min(ptr.getLimitMonth().subtract(new BigDecimal(monthSum)), rps);
			}
			if(result.isStatus() && !BigDecimalUtil.compare4Big(rps, 0)){
				rps = new BigDecimal(0);
				result.setStatus(false);
				result.setMessages(getMessage(MSG_POINT_MONTH_LIMIT, ptr.getTypeName()));
			}else if(BigDecimalUtil.compare4Big(rps, 0)
					&& ptr.getLimitYear() != null 
					&&  BigDecimalUtil.compare4Big(ptr.getLimitYear(), 0)){
				//当年有限制
				pe.setCreateDateStart(DateUtil.getYearFirstDay(now));
				pe.setCreateDateEnd(DateUtil.addDays(DateUtil.getYearLastDay(now),1));
				Long yearSum = (Long)searchOneData(pe, SQL_POINT_EARN_SELECT_SUM_POINTS);
				if(yearSum == null){
					yearSum = 0L;
				}
				//允许增加的积分
				rps = BigDecimalUtil.min(ptr.getLimitYear().subtract(new BigDecimal(yearSum)), rps);
				if(!BigDecimalUtil.compare4Big(rps, 0)){
					rps = new BigDecimal(0);
					result.setStatus(false);
					result.setMessages(getMessage(MSG_POINT_YEAR_LIMIT, ptr.getTypeName()));
				}
			}
			//记录该积分发放记录
			if(ptr.getValidity() != null && ptr.getValidity() > 0){
				pe.setExpirationDate(DateUtil.addDays(DateUtil.getTomorrowStart(now), ptr.getValidity()));
			}
			pe.setPoints(rps);
			pe.setId(IDGenerator.genUID());
			
			if(result.isStatus()){
				//更新汇总数据
				Date sumExDate = this.getSumExpirationDate(pe.getExpirationDate(), ConfigUtil.getProperty(CONFIG_POINT_SUM_PERIOD));
				PointSumEntity pse = new PointSumEntity(userId, sumExDate);
				pse.setSumPoints(rps);
				//判断汇总数据是否已经存在
				PointSumEntity psr = searchOneData(pse, SQL_POINT_SUM_SELECT_BY_USEREXPIRATION);
				if(psr != null){
					//更新汇总数据
					psr.setSumPoints(rps);
					update(psr);
				}else{
					pse.setId(IDGenerator.genUID());
					insert(pse);
				}
				
				result.setMessages(getMessage(MSG_POINT_EARN_SUCCESS));
			}
			up = refreshUserPointEntity(userId, rps).get();
			pe.setResultPoints(up.getPoints());
			insert(pe);
		}else{
			//刷新用户总累计积分
			up = refreshUserPointEntity(userId, rps).get();
		}
		up.setPointsSerialNo(pe.getSeqNo());
		result.fill(up);
		return result;
	}
	
	/**
	 * 消耗积分
	 * @param userId 用户ID
	 * @param typeCode 消耗积分的业务类型
	 * @param point 消耗的积分点数，可以为空，若该业务类型已指定积分，则以类型指定的积分为获取的积分
	 * @return 返回操作结果及用户剩余的积分
	 * @author adlay
	 */
	@WebMethod
	@WebResult(name="result")
	public UserPoint consumePoint(
			@WebParam(name="systemCode")String systemCode,
			@WebParam(name="useId")String userId, 
			@WebParam(name="typeCode")String typeCode, 
			@WebParam(name="points")BigDecimal points,
			@WebParam(name="businessId")String businessId,
			@WebParam(name="remark")String remark){
		UserPoint result = new UserPoint();
		result.setStatus(true);
		PointConsumeEntity pc = new PointConsumeEntity();
		UserPoint rup = null;
		if(BigDecimalUtil.compare4Big(points, 0)){
			//积分是否够用
			UserPoint up = fetchPoint(userId);
			PointTypeEntity ptc = new PointTypeEntity(systemCode, typeCode); 
			ptc.setSumKbn(SUM_KBN_CONSUME);
			PointTypeEntity ptr = this.searchOneData(ptc);
			//该类型的积分操作是否支持
			if(ptr == null){
				ptr = new PointTypeEntity(systemCode, typeCode); 
				ptr.setValidity(0);
//				result.setStatus(false);
//				result.setMessages(getMessage(MSG_POINT_TYPE_UNSUPPORT));
//				return result;
			}
			//系统定义的该类型操作的积分优先级更高
			BigDecimal rps = ptr.getPoints() != null && BigDecimalUtil.compare4Big(ptr.getPoints(), 0) ? ptr.getPoints() : points;
			rps = rps.abs();
			if(up.getPoints().compareTo(rps) < 0){
				result.setStatus(false);
				result.setMessages(getMessage(MSG_POINT_NOT_ENOUGH));
				result.fill(up);
				return result;
			}
			//往积分消耗表中插入一条记录
			pc.setUserId(userId);
			pc.setRequestPoints(points);
			pc.setBusinessId(businessId);
			pc.setSystemCode(systemCode);
			pc.setTypeCode(typeCode);
			pc.setRemark(remark);
			//流水号
			pc.setSeqNo(getSeqNo(SEQ_PRE_POINT_CONSUME, POINT_SEQ_NUM_LENGTH));
			
			BigDecimal notAllowConsume = new BigDecimal(0);
			Date now = new Date();
			//当天有限制
			if(ptr.getLimitDaily() != null &&  BigDecimalUtil.compare4Big(ptr.getLimitDaily(), 0)){
				pc.setCreateDateStart(DateUtil.getDayStart(now));
				pc.setCreateDateEnd(DateUtil.getTomorrowStart(now));
				BigDecimal daySum = (BigDecimal)searchOneData(pc, SQL_POINT_CONSUME_SELECT_SUM_POINTS);
				if(daySum == null){
					daySum = new BigDecimal(0);
				}
				//允许扣除的积分
				notAllowConsume = ptr.getLimitDaily().subtract(daySum).subtract(rps);
			}
			if(BigDecimalUtil.compare4Small(notAllowConsume, 0)){
				rps = new BigDecimal(0);
				result.setStatus(false);
				result.setMessages(getMessage(MSG_POINT_DAY_LIMIT, ptr.getTypeName()));
			}else if(ptr.getLimitMonth() != null &&  BigDecimalUtil.compare4Big(ptr.getLimitMonth(), 0)){
				//当月有限制
				pc.setCreateDateStart(DateUtil.getMonthFirstDay(now));
				pc.setCreateDateEnd(DateUtil.addDays(DateUtil.getMonthLastDay(now),1));
				BigDecimal monthSum = (BigDecimal)searchOneData(pc, SQL_POINT_CONSUME_SELECT_SUM_POINTS);
				if(monthSum == null){
					monthSum = new BigDecimal(0);
				}
				//允许扣除的积分
				notAllowConsume = ptr.getLimitMonth().subtract(monthSum).subtract(rps);
			}
			if(result.isStatus() && BigDecimalUtil.compare4Small(notAllowConsume, 0)){
				rps = new BigDecimal(0);
				result.setStatus(false);
				result.setMessages(getMessage(MSG_POINT_MONTH_LIMIT, ptr.getTypeName()));
			}else if(BigDecimalUtil.compare4Big(rps, 0)  
					&& ptr.getLimitYear() != null 
					&& BigDecimalUtil.compare4Big(ptr.getLimitYear(), 0)){
				//当年有限制
				pc.setCreateDateStart(DateUtil.getYearFirstDay(now));
				pc.setCreateDateEnd(DateUtil.addDays(DateUtil.getYearLastDay(now),1));
				BigDecimal yearSum = (BigDecimal)searchOneData(pc, SQL_POINT_CONSUME_SELECT_SUM_POINTS);
				if(yearSum == null){
					yearSum = new BigDecimal(0);
				}
				//允许扣除的积分
				notAllowConsume = ptr.getLimitYear().subtract(yearSum).subtract(rps);
				if(BigDecimalUtil.compare4Small(notAllowConsume, 0)){
					rps = new BigDecimal(0);
					result.setStatus(false);
					result.setMessages(getMessage(MSG_POINT_YEAR_LIMIT, ptr.getTypeName()));
				}
			}
			
			pc.setPoints(rps);
			if(result.isStatus()){
				//统计表扣除积分操作
				BigDecimal needCutPoints = rps;
				if(BigDecimalUtil.compare4Big(needCutPoints, 0)){
					//查找最早的积分记录
					PointSumEntity ebse = this.searchOneData(new PointSumEntity(userId), SQL_POINT_SUM_SELECT_EARLIEST);
					while(BigDecimalUtil.compare4Big(needCutPoints, 0) && ebse != null){
						if(needCutPoints.compareTo(ebse.getSumPoints()) > 0){
							this.delete(ebse);
							//需要继续扣除
							needCutPoints = needCutPoints.subtract(ebse.getSumPoints());
							ebse = this.searchOneData(new PointSumEntity(userId), SQL_POINT_SUM_SELECT_EARLIEST);
						}else{
							ebse.setSumPoints(new BigDecimal(0).subtract(needCutPoints));
							this.update(ebse);
							needCutPoints = new BigDecimal(0);
							break;
						}
					}
				}
				//继续扣除无期限的积分
				if(BigDecimalUtil.compare4Big(needCutPoints, 0)){
					PointSumEntity neps = this.searchOneData(new PointSumEntity(userId), SQL_POINT_SUM_SELECT_NOTEXPIRATION);
					if(neps.getSumPoints().compareTo(needCutPoints) >= 0){
						neps.setSumPoints(new BigDecimal(0).subtract(needCutPoints));
						update(neps);
					}else{
						result.setMessages(getMessage(MSG_POINT_NOT_ENOUGH));
						result.fill(up);
						return result;
					}
				}
				result.setStatus(true);
				result.setMessages(getMessage(MSG_POINT_CONSUME_SUCCESS));
			}	
			//往积分消耗表中插入一条记录
			pc.setId(IDGenerator.genUID());
			pc.setPoints(rps);
			
			rup = refreshUserPointEntity(userId, new BigDecimal(0)).get();
			pc.setResultPoints(rup.getPoints());
			insert(pc);
		}else{
			//刷新用户总累计积分
			rup = refreshUserPointEntity(userId, new BigDecimal(0)).get();
		}
		rup.setPointsSerialNo(pc.getSeqNo());
		result.fill(rup);
		return result;
	}
}
