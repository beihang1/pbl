/**
 * 
 */
package com.cloudsoaring.web.points.service;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.cloudsoaring.web.common.service.BaseService;
import com.cloudsoaring.web.points.constant.PointDbConstants;
import com.cloudsoaring.web.points.entity.UserPointEntity;

/**
 *Copyright (c),2015-2016, Cloudsoaring
 *<br/>This program is protected by copyright laws; 
 *<br/>Program Name: <b>CSWEB<b>
 *<br/>
 *<br/>
 *<br/>2016年3月15日 下午3:12:27
 *@author adlay
 *@since JDK 1.6
 *@version 1.0
 */
@Service
public class UserPointService extends BaseService implements PointDbConstants{

	public void doCommitUserPointEntity(String userId){
		UserPointEntity up = new UserPointEntity(userId);
		up.setTotalPoints(new BigDecimal(0));
		up.setPoints(new BigDecimal(0));
		this.insert(up, SQL_USER_POINT_INSERT);
	}
	
}
