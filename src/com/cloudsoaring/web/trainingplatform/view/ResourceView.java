package com.cloudsoaring.web.trainingplatform.view;

import java.util.Date;
import java.util.List;

import com.cloudsoaring.web.bus.entity.TagEntity;
import com.cloudsoaring.web.common.entity.BaseEntity;

/**
 * 资源管理
 */
public class ResourceView extends BaseEntity {

	private static final long serialVersionUID = 1L;
	private String id;
	/** 文件id */
	private String fileId;
	/** 封面 */
	private String pictureId;
	/** 目录id */
	private String directoryId;
	/** 文件名 */
	private String fileName;
	
	private String fileRealName;
	/** 分享标记 */
	private String flagShare;
	/** 状态（用于资源管理：1【审批通过】，2【撤销】） */
	private String stateShare;
	/** 备注 */
	private String markShare;
	/** 分享分类：来源课程分类 */
	private String typeShare;
	/** 共享时间 */
	private Date timeShare;
	/** NUM_DOWLOAD下载量 */
	private Integer numDowload;

	private String secondTag;
	private String secondTagName;
	private String firstTag;
	private String firstTagName;
	private String tagName;
	/** 资源简介 */
	private String briefInTroduction;
	/** 上传人员 */
	private String userId;

	/** 方向标签 */
	private List<TagEntity> directTag;
	/** 分类标签 */
	private List<TagEntity> categoryTag;

	private String userName;
	private String createtime;

	public String getUserId() {
		return userId;
	}

	public List<TagEntity> getDirectTag() {
		return directTag;
	}

	public void setDirectTag(List<TagEntity> directTag) {
		this.directTag = directTag;
	}

	public List<TagEntity> getCategoryTag() {
		return categoryTag;
	}

	public void setCategoryTag(List<TagEntity> categoryTag) {
		this.categoryTag = categoryTag;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCreatetime() {
		return createtime;
	}

	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBriefInTroduction() {
		return briefInTroduction;
	}

	public void setBriefInTroduction(String briefInTroduction) {
		this.briefInTroduction = briefInTroduction;
	}

	public Integer getNumDowload() {
		return numDowload;
	}

	public void setNumDowload(Integer numDowload) {
		this.numDowload = numDowload;
	}

	public Date getTimeShare() {
		return timeShare;
	}

	public void setTimeShare(Date timeShare) {
		this.timeShare = timeShare;
	}

	public String getSecondTag() {
		return secondTag;
	}

	public void setSecondTag(String secondTag) {
		this.secondTag = secondTag;
	}

	public String getFirstTag() {
		return firstTag;
	}

	public void setFirstTag(String firstTag) {
		this.firstTag = firstTag;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getDirectoryId() {
		return directoryId;
	}

	public void setDirectoryId(String directoryId) {
		this.directoryId = directoryId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFlagShare() {
		return flagShare;
	}

	public void setFlagShare(String flagShare) {
		this.flagShare = flagShare;
	}

	public String getStateShare() {
		return stateShare;
	}

	public void setStateShare(String stateShare) {
		this.stateShare = stateShare;
	}

	public String getMarkShare() {
		return markShare;
	}

	public void setMarkShare(String markShare) {
		this.markShare = markShare;
	}

	public String getTypeShare() {
		return typeShare;
	}

	public void setTypeShare(String typeShare) {
		this.typeShare = typeShare;
	}

	public String getPictureId() {
		return pictureId;
	}

	public void setPictureId(String pictureId) {
		this.pictureId = pictureId;
	}

	public String getFileRealName() {
		return fileRealName;
	}

	public void setFileRealName(String fileRealName) {
		this.fileRealName = fileRealName;
	}

	public String getSecondTagName() {
		return secondTagName;
	}

	public void setSecondTagName(String secondTagName) {
		this.secondTagName = secondTagName;
	}

	public String getFirstTagName() {
		return firstTagName;
	}

	public void setFirstTagName(String firstTagName) {
		this.firstTagName = firstTagName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
