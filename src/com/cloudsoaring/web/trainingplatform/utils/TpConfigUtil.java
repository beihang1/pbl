package com.cloudsoaring.web.trainingplatform.utils;

import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.common.utils.ConfigUtil;
import com.cloudsoaring.web.trainingplatform.constants.TpConstants;

public class TpConfigUtil extends ConfigUtil implements TpConstants {

	/**
	 * 单价显示模式
	 * @return
	 */
	public static final String getPriceDisplayMode(){
		return getProperty(CONFIG_PRICE_DISPLAY_MODE, PRICE_DISPLAY_MODE_POINTS);
	}
	
	/**
	 * 通过积分显示价格
	 * @return
	 */
	public static final boolean displayPriceByPoints(){
		return PRICE_DISPLAY_MODE_POINTS.equals(getPriceDisplayMode());
	}
	
	/**
	 * 通过零钱显示价格
	 * @return
	 */
	public static final boolean displayPriceBySmallChange(){
		return PRICE_DISPLAY_MODE_SMALL_CHANGE.equals(getPriceDisplayMode());
	}
	
	/**
	 * 单位零钱兑换的积分数量
	 * @return
	 */
	public static final int getPointsForSmallChange(){
		return getInteger(CONFIG_POINTS_FOR_SMALL_CHANGE, 1);
	}
	
	/**
	 * 系统支持的支付模式
	 * @return
	 */
	private static final String getSupportedPayModes(){
		return StringUtil.removeSpace(getProperty(CONFIG_SUPPORTED_PAY_MODES, PAY_MODE_POINTS));
	}
	
	/**
	 * 是否支持积分支付
	 * @return
	 */
	public static final boolean supportPointsPayment(){
		return ("," + getSupportedPayModes() + ",").indexOf("," + PAY_MODE_POINTS + ",") > -1;
	}
	
	/**
	 * 是否支持零钱支付
	 * @return
	 */
	public static final boolean supportSmallChangePayment(){
		return ("," + getSupportedPayModes() + ",").indexOf("," + PAY_MODE_SMALL_CHANGE + ",") > -1;
	}
	
	/**
	 * 返回系统配置的视频服务读取授权码
	 * @return 视频服务读取授权码
	 */
	public static final String getVideoServiceReadToken(){
		return getProperty(VIDEO_SERVICE_READ_TOKEN);
	}
	
	/**
	 * 返回系统配置的视频服务上传授权码
	 * @return 视频服务上传授权码
	 */
	public static final String getVideoServiceWriteToken(){
		return getProperty(VIDEO_SERVICE_WRITE_TOKEN);	
	}
	
	/**
	 * 返回系统配置的视频服务签名密钥
	 * @return 视频服务签名密钥
	 */
	public static final String getVideoServiceSecretKey(){
		return getProperty(VIDEO_SERVICE_SECRETKEY);
	}
	
	/**
	 * 返回系统配置的视频服务的分类:课程 
	 * @return 课程分类
	 */
	public static final String getVideoServiceCategoryCourse(){
		return getProperty(VIDEO_SERVICE_CATEGORY_COURSE);
	}
}
