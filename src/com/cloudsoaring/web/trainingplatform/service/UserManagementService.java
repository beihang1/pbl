package com.cloudsoaring.web.trainingplatform.service;

import java.io.File;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudsoaring.common.excel.BusinessHandler;
import com.cloudsoaring.common.excel.ExcelImportResult;
import com.cloudsoaring.common.excel.ExcelImportUtil;
import com.cloudsoaring.common.excel.RowValidatedResult;
import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.MD5Util;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.exception.BusinessException;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.CompressConfig;
import com.cloudsoaring.web.bus.entity.FileEntity;
import com.cloudsoaring.web.bus.entity.UserEntity;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.utils.HttpClientUtil;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseDbConstants;
import com.cloudsoaring.web.course.entity.MeasurementEntity;
import com.cloudsoaring.web.course.entity.QuestionOptionEntity;
import com.cloudsoaring.web.course.entity.TeacherApplyEntity;
import com.cloudsoaring.web.course.entity.UserCourseEntity;
import com.cloudsoaring.web.course.entity.UserMeasuermentAnswerEntity;
import com.cloudsoaring.web.course.entity.UserMeasurementResultEntity;
import com.cloudsoaring.web.course.service.CourseService;
import com.cloudsoaring.web.course.view.HomeworkView;
import com.cloudsoaring.web.course.view.PlanView;
import com.cloudsoaring.web.institution.entity.MeasurementInstitutionEntity;
import com.cloudsoaring.web.institution.entity.QuestionOptionInstitutionEntity;
import com.cloudsoaring.web.institution.service.InstitutionService;
import com.cloudsoaring.web.points.entity.UserPoint;
import com.cloudsoaring.web.points.service.PointService;
import com.cloudsoaring.web.teacherevalution.entity.QuestionOptionTeacherEntity;
import com.cloudsoaring.web.trainingplatform.constants.TpDbConstants;
import com.cloudsoaring.web.trainingplatform.constants.TpDefaultValues;
import com.cloudsoaring.web.trainingplatform.constants.TpMsgNames;
import com.cloudsoaring.web.trainingplatform.constants.TpStateConstants;
import com.cloudsoaring.web.trainingplatform.entity.TpUserEntity;
import com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity;


/**
 * 用户管理
 * 
 * @author
 *
 */
@Service
@SuppressWarnings("unchecked")
public class UserManagementService extends FileService implements
		BusinessHandler<TpUserEntity>, TpStateConstants, TpDbConstants ,TpDefaultValues{
	@Autowired
	private CourseService courseService;
	@Autowired
    private PointService pointService;
	@Autowired
    private InstitutionService institutionService;
	/**
	 * 检索用户
	 * 
	 * @param user
	 * @return 返回结果
	 */
	public ResultBean search4Page(TpUserEntity user) {
		if (StringUtil.isNotEmpty(user.getUserName())) {
			user.setUserName(StringUtil.trim(user.getUserName()));
		}
		if (StringUtil.isNotEmpty(user.getPersonName())) {
			user.setPersonName(StringUtil.trim(user.getPersonName()));
		}
		if (StringUtil.isNotEmpty(user.getEmail())) {
			user.setEmail(StringUtil.trim(user.getEmail()));
		}
		if (StringUtil.isNotEmpty(user.getRoleId())) {
			if (StringUtil.equals(user.getRoleId(),TpStateConstants.TEACHER_ROLE)|| StringUtil.equals(user.getRoleId(),TpStateConstants.ADMINISTRATOR_ROLE)) {
				// 教师和管理员的用户角色A
				user.setUserKbn(TpStateConstants.USER_KBN_A);
			} else {
				// 普通用户的用户角色C
				user.setRoleId(null);
				user.setUserKbn(TpStateConstants.USER_KBN_C);
			}
		}
		ResultBean result = commonDao.searchList4Page(user,
				TpDbConstants.USER_SEARCH_CONDITION);
		if (result.getData() != null) {
			List<TpUserEntity> list = (List<TpUserEntity>) result.getData();
			for (TpUserEntity userEntity : list) {
				UserRoleEntity roleEntity = new UserRoleEntity();
				roleEntity.setUserId(userEntity.getUserId());
				List<UserRoleEntity> entitylist = commonDao.searchList(
						roleEntity, TpDbConstants.SEARCH_ROLE_ID_BY_USER_ID);
				if (entitylist.size() > 0) {
					if (entitylist.get(0) != null) {
						if (StringUtil.equals(entitylist.get(0).getRoleId(),
								TpStateConstants.TEACHER_ROLE)) {
							userEntity.setRoleTeacherId(entitylist.get(0)
									.getRoleId());
						}
						if (StringUtil.equals(entitylist.get(0).getRoleId(),
								TpStateConstants.ADMINISTRATOR_ROLE)) {
							userEntity.setRoleAdministratorId(entitylist.get(0)
									.getRoleId());
						}
					}
					if (entitylist.size() > 1 && entitylist.get(1) != null) {
						if (StringUtil.equals(entitylist.get(1).getRoleId(),
								TpStateConstants.TEACHER_ROLE)) {
							userEntity.setRoleTeacherId(entitylist.get(1)
									.getRoleId());
						}
						if (StringUtil.equals(entitylist.get(1).getRoleId(),
								TpStateConstants.ADMINISTRATOR_ROLE)) {
							userEntity.setRoleAdministratorId(entitylist.get(1)
									.getRoleId());
						}
					}
				}
			}
		}
		return result;
	}

	/**
	 * 根据USER_ID检索用户具体信息
	 * 
	 * @param userId
	 * @return 返回结果
	 */
	public TpUserEntity searchById(String userId) {
		TpUserEntity condition = new TpUserEntity();
		condition.setUserId(userId);
		//根据查询条件查询tb_user数据表信息
		return (TpUserEntity) commonDao.searchOneData(condition,
				TpDbConstants.USER_SEARCH_CONDITION);
	}
	
	public TpUserEntity seachUserInfo(String userId){
		TpUserEntity condition = new TpUserEntity();
		condition.setUserId(userId);
		return (TpUserEntity) commonDao.searchOneData(condition,
				TpDbConstants.SQL_SEARCH_USER_ALL_INFO);
	}
	
	public ResultBean searchUserAll(TpUserEntity entity){
		return commonDao.searchList4Page(entity,TpDbConstants.SQL_SEARCH_USER_ALL_INFO);
	}
	
	public ResultBean searchManagerUserAll(TpUserEntity entity){
		return commonDao.searchList4Page(entity,TpDbConstants.SQL_USER_ALL_INFO);
	}
	/**
	 * 查找Email是否已存在
	 */
	public Boolean emailExist(TpUserEntity user) {
		int cnt = commonDao.searchCount(user, TpDbConstants.USER_EMAIL_EXIST);
		return cnt > 0;
	}

	/**
	 * 查找用户名是否已存在
	 * 
	 * @param user
	 * @return 返回结果
	 */
	public Boolean userNameExist(TpUserEntity user) {
		int cnt = commonDao.searchCount(user, TpDbConstants.USER_NAME_EXIST);
		return cnt > 0;
	}

	/**
	 * 查找用户是否已存在
	 * 
	 * @param user
	 * @return 返回结果
	 */
	public Boolean checkUser(TpUserEntity user) {
		int cnt = commonDao.searchCount(user, TpDbConstants.USER_EXIST);
		return cnt > 0;
	}
	
	public TpUserEntity checkUserByName(TpUserEntity user) {
		TpUserEntity entity = commonDao.searchOneData(user, TpDbConstants.SEARCH_COUNT_BY_USER_NAME);
		return entity;
	}
	
	
	public List<UserRoleEntity> getRoleByUserId(String userId) {
		UserRoleEntity userRole = new UserRoleEntity();
		userRole.setUserId(userId);
		return commonDao.searchList(userRole, TpDbConstants.SEARCH_USER_ROLE);
	}
	
	/**
	 * 根据userId检索数据是否存在
	 * 
	 * @param role
	 * @return 返回结果
	 */
	private int searchCount(UserRoleEntity role) {
		return commonDao.searchCount(role,
				TpDbConstants.SEARCH_COUNT_BY_USER_ID);
	}

	/**
	 * 保存用户修改/新增信息
	 * 
	 * @param user
	 */
	public void save(TpUserEntity user) {
		// 检查用户名是否已存在
		if (userNameExist(user)) {
			throw new BusinessException(getMessage(MSG_E_USERNAME_EXIST,
					user.getUserName()));
		}
		// 检查邮箱是否已存在
		if (StringUtil.isNotEmpty(user.getEmail()) && emailExist(user)) {
			throw new BusinessException(getMessage(MSG_E_EMAIL_EXIST,
					user.getEmail()));
		}
		if (StringUtil.isNotEmpty(user.getPassword())) {
			try {
				user.setPassword(MD5Util.getPassword4MD5(user.getPassword()));
			} catch (Exception ex) {

			}
		}
		String userName = StringUtil.trim(user.getUserName());
		user.setUserName(userName);
		if (StringUtil.isNotEmpty(user.getUserId())) {
			// 更新
			commonDao.updateData(user, TpDbConstants.USER_UPDATE);
		} else {
			user.setUserId(IDGenerator.genUID());
			// 插入
			user.setUserKbn(TpStateConstants.USER_KBN_C);
			commonDao.insertData(user, TpDbConstants.USER_INSERT);
		}
	}

	/**
	 * 针对个人中心的修改
	 * 
	 * @param user
	 * @param imagParams
	 * @throws Exception
	 */
	public TpUserEntity updateForSelf(TpUserEntity user, CompressConfig config)
			throws Exception {
		FileEntity image = processRequestFile("imageP", FILE_TYPE_IMAGE);
		compress(image, true, config);
		TpUserEntity query = new TpUserEntity();
		query.setUserId(user.getUserId());
		TpUserEntity dbUserInfo = searchOneData(query,
				TpDbConstants.USER_MANAGEMENT_SEARCH_USER_ALL);
		dbUserInfo.setPersonName(user.getPersonName());
		if (image != null) {
			dbUserInfo.setImage(image.getFileId());
		}
		dbUserInfo.setEmail(user.getEmail());
		dbUserInfo.setPhone(user.getPhone());
		dbUserInfo.setWeixin(user.getWeixin());
		dbUserInfo.setWeibo(user.getWeibo());
		dbUserInfo.setRemark(user.getRemark());
		dbUserInfo.setRegion(user.getRegion());
		dbUserInfo.setPosition(user.getPosition());
		update(dbUserInfo, TpDbConstants.USER_MANAGEMENT_UPDATE_USER_ALL);
		return dbUserInfo;
	}

	/**
	 * 删除用户
	 * 
	 * @param uIds
	 */
	public void deleteUser(String uIds) {
		String[] userIds = StringUtil.split(uIds, ",");
		if (userIds != null) {
			for (String userId : userIds) {
				if (StringUtil.isNotEmpty(userId)) {
					TpUserEntity user = new TpUserEntity();
					user.setUserId(userId);
					// 更新用户表的用户数据标记为已删除
					commonDao.updateData(user,
							TpDbConstants.USER_UPDATE_DEL_FLAG);
					UserRoleEntity role = new UserRoleEntity();
					role.setUserId(userId);
					// 删除用户角色表中该用户的所有角色信息
					commonDao.deleteData(role,
							TpDbConstants.DELETE_USER_ROLE_BY_USER_ID);
				}
			}
		}
	}

	/**
	 * 认证为老师
	 * 
	 * @param userId
	 */
	public void teacherAccount(String userId) {
		UserRoleEntity role = new UserRoleEntity();
		role.setUserId(userId);
		// 设定老师的角色ID
		role.setRoleId(TpStateConstants.TEACHER_ROLE);
		//先删除
		commonDao.deleteData(role,
				TpDbConstants.DELETE_BY_USER_ID_ADD_ROLE_ID);
		
		// 后插入用户角色为老师的用户角色信息
		commonDao.insertData(role, TpDbConstants.INSERT_USER_BY_ROLE_ID);
		
		beManagerAccount(userId);
	}

	/**
	 * 取消老师认证
	 * 
	 * @param userId
	 */
	public void unteacherAccount(String userId) {
		UserRoleEntity role = new UserRoleEntity();
		role.setRoleId(TpStateConstants.TEACHER_ROLE);
		role.setUserId(userId);
		// 删除用户老师角色的用户角色数据
		commonDao.deleteData(role,
				TpDbConstants.DELETE_BY_USER_ID_ADD_ROLE_ID);
		notManagerAccount(userId);
		
	}

	/**
	 * 认证为管理员
	 * 
	 * @param userId
	 */
	public void administratorAccount(String userId) {
		UserRoleEntity role = new UserRoleEntity();
		// 设定管理员角色ID
		role.setRoleId(TpStateConstants.ADMINISTRATOR_ROLE);
		role.setUserId(userId);
		// 删除用户角色表数据
		commonDao.deleteData(role,
				TpDbConstants.DELETE_BY_USER_ID_ADD_ROLE_ID);
		// 插入用户管理员角色的用户角色信息
		commonDao.insertData(role, TpDbConstants.INSERT_USER_BY_ROLE_ID);
		beManagerAccount(userId);
	}

	/**
	 * 认证为后台用户
	 * 
	 * @param userId
	 */
	public void beManagerAccount(String userId){
		TpUserEntity user = new TpUserEntity();
		user.setUserId(userId);
		user.setUserKbn(TpStateConstants.USER_KBN_A);
		commonDao.updateData(user, TpDbConstants.UPDATE_USER_KBN);
	}
	
	/**
	 * 取消认证为后台用户
	 * 
	 * @param userId
	 */
	public void notManagerAccount(String userId){
		TpUserEntity user = new TpUserEntity();
		user.setUserId(userId);
		user.setUserKbn(TpStateConstants.USER_KBN_C);
		commonDao.updateData(user, TpDbConstants.UPDATE_USER_KBN);
	}
	
	/**
	 * 取消认证为管理员
	 * 
	 * @param userId
	 */
	public void unAdministratorAccount(String userId) {
		UserRoleEntity role = new UserRoleEntity();
		role.setRoleId(TpStateConstants.ADMINISTRATOR_ROLE);
		role.setUserId(userId);
		// 删除用户角色表数据
		commonDao.deleteData(role,
				TpDbConstants.DELETE_BY_USER_ID_ADD_ROLE_ID);
		notManagerAccount(userId);

	}

	/**
	 * 导入
	 * 
	 * @param input
	 * @return 返回结果
	 * @throws Exception
	 */
	public ExcelImportResult<TpUserEntity> doImport(InputStream input)
			throws Exception {
		return ExcelImportUtil.importExcel(
				com.cloudsoaring.web.bus.entity.UserEntity.class,
				TpUserEntity.class, this, input);
	}

	/**
	 * 用户验证
	 */
	@Override
	public Boolean businessValidate(
			RowValidatedResult<TpUserEntity> validatedResult) {
		TpUserEntity user = validatedResult.getData();
		// 检查用户名是否已存在
		if (userNameExist(user)) {
			validatedResult.setResult(false).setMessage(
					getMessage(MSG_E_USERNAME_EXIST, user.getUserName()));
		} else if (StringUtil.isNotEmpty(user.getEmail()) && emailExist(user)) {
			validatedResult.setResult(false).setMessage(
					getMessage(MSG_E_EMAIL_EXIST, user.getEmail()));
		}
		return validatedResult.isResult();
	}

	/**
	 * 用户信息更新
	 */
	@Override
	public void doBusiness(RowValidatedResult<TpUserEntity> rowValidResult) {
		TpUserEntity user = rowValidResult.getData();
		user.setUserId(IDGenerator.genUID());
		if (StringUtil.isNotEmpty(user.getPassword())) {
			try {
				user.setPassword(MD5Util.getPassword4MD5(user.getPassword()));
			} catch (Exception ex) {

			}
		}
		user.setUserKbn(TpStateConstants.USER_KBN_C);
		commonDao.insertData(user, TpDbConstants.USER_INSERT);
	}

	public UserEntity storageSSOUserEntity(TpUserEntity user, String imageUrl,
			String thumbnailUrl, int connectionTimeout, int requestTimeout)
			throws Exception {
		TpUserEntity oldData = this.searchById(user.userId);
		String image = null;
		if (StringUtil.isNotEmpty(imageUrl)
				|| StringUtil.isNotEmpty(thumbnailUrl)) {
			File f = HttpClientUtil.requestTmpFile(
					StringUtil.getOrElse(imageUrl, thumbnailUrl),
					connectionTimeout, requestTimeout);
			if (f != null) {
				FileEntity file = this.processFile(f, FILE_TYPE_IMAGE);
				image = file.getFileId();
				compress(file, false);
			}
		}
		user.setPassword("1");
		if (oldData != null) {
			// 有历史数据
			if (StringUtil.isNotEmpty(oldData.getImage())) {
				this.deleteFileById(oldData.getImage(), FILE_TYPE_IMAGE);
			}
			oldData.setImage(image);
			if(StringUtil.isNotEmpty(user.getUserName())){
				oldData.setUserName(user.getUserName());
			}
			if(StringUtil.isNotEmpty(user.getPersonName())){
				oldData.setPersonName(user.getPersonName());
			}
			if(StringUtil.isNotEmpty(user.getMobile())){
				oldData.setMobile(user.getMobile());
			}
			if(StringUtil.isNotEmpty(user.getEmail())){
				oldData.setEmail(user.getEmail());
			}
			if(StringUtil.isNotEmpty(user.getWeibo())){
				oldData.setWeibo(user.getWeibo());
			}
			if(StringUtil.isNotEmpty(user.getWeixin())){
				oldData.setWeixin(user.getWeixin());
			}
			if(user.getCreateDate() != null){
				oldData.setCreateDate(user.getCreateDate());
			}
			//user.setPersonName(oldData.getPersonName());
			commonDao.updateData(oldData, TpDbConstants.USER_UPDATE);
			return oldData;
		} else {
			// 插入
			user.setImage(image);
			commonDao.insertData(user, TpDbConstants.USER_INSERT);
			return user;
		}
	}

	/**
	 * 得倒教师列表
	 * 
	 * @return 返回结果
	 */
	public List<TpUserEntity> getTeacherList() {
		TpUserEntity query = new TpUserEntity();
		// 教师和管理员的用户角色A
		query.setUserKbn(TpStateConstants.USER_KBN_A);
		query.setRoleId(TpStateConstants.TEACHER_ROLE);
		List<TpUserEntity> result = search(query,
				TpDbConstants.USER_SEARCH_CONDITION);
		return result;
	}

	/**
	 * 我的投票    
	 *  @param pageSize 每页显示多少
	 * 	@param pageNumber 第几页
	 */
	public ResultBean userVoteList(UserCourseEntity uCourse) {
		assertNotGuest();
		uCourse.setUserId(WebContext.getSessionUserId());
		return commonDao.searchList4Page(uCourse,
				TpDbConstants.SEARCH_USER_VOTE);
	}

	/**
	 * 
	 * @param measEntity
	 *            pageSize 每页多少条数据 pageNumber第几页
	 * @return
	 */

	public ResultBean userMeasurementList(MeasurementEntity measEntity) {
		assertNotGuest();
		measEntity.setUserId(WebContext.getSessionUserId());
		
		//检索我的测试
		//课程测试模块： 课程关联的测试信息类型为测试
		//章节测试模块: 非考试非测试章节下测试信息类型为测试
		return commonDao.searchList4Page(measEntity,
				TpDbConstants.SEARCH_USER_MEASUREMENT);

	}
	public ResultBean userExamList(MeasurementEntity measEntity) {
        assertNotGuest();
        measEntity.setUserId(WebContext.getSessionUserId());
        //检索我的测试
        //课程测试模块： 课程关联的测试信息类型为测试
        //章节测试模块: 非考试非测试章节下测试信息类型为测试
        return commonDao.searchList4Page(measEntity, TpDbConstants.SEARCH_USER_EXAM);
    }
	/**
	 * 查询我的课程章节作业
	 * 
	 * @return
	 */
	public ResultBean searchHomeworkList(HomeworkView homeworkView) {

		homeworkView.setCreateUser(WebContext.getSessionUserId());
		return commonDao.searchList4Page(homeworkView,
				TpDbConstants.SEARCH_USER_HOME_WORK_ALL);

	}

	/**
	 * 问卷调查列表检索
	 * 
	 * @param condition
	 *            问卷实体类
	 * @return 返回结果
	 * @author 
	 */
	public ResultBean selectQuestionList(MeasurementEntity condition) {
		//判断是否登录
		assertNotGuest();
		condition.setUserId(WebContext.getSessionUserId());
		return commonDao.searchList4Page(condition,SQL_SELECT_MEASURE_INFO_LIST);
	}
	/**
	 * 问卷参与人员列表检索
	 * @param condition
	 * @return
	 */
	public ResultBean selectQuestionUserList(String  measurementId) {
        //判断是否登录
        assertNotGuest();
        UserMeasurementResultEntity condition=new UserMeasurementResultEntity();
        condition.setMeasurementId(measurementId);
        return commonDao.searchList4Page(condition,CourseDbConstants.SQL_MEASUERMENT_USER_SEARCH);
    }
	/**
	 * 删除问卷调查列表数据
	 * @author heyaqin
	 * @return 返回结果
	 */
	public void deleteQuestionList(String id) {
		String[] ids = StringUtil.split(id, ",");
		if (ids != null) {
			for (String measureId : ids) {
				if (StringUtil.isNotEmpty(measureId)) {
					MeasurementEntity entity = new MeasurementEntity();
					entity.setId(measureId);
					MeasurementEntity measurementEntity = searchOneData(entity);
					if(measurementEntity!= null){
						courseService.delMeasure(measurementEntity.getLinkId(),measurementEntity.getLinkType(),MEASUERMENT_TYPE_QUESTIONNAIRE);
					}
				}
			}
		}
	}
	
	/**
	 * 问卷统计列表检索
	 * 
	 * @author HEYAQIN
	 * @param id
	 *            问卷ID
	 * @return 返回结果
	 */
	@SuppressWarnings("unused")
	public ArrayList<Map<String, String>> selectOptionsList(String id) {
		QuestionOptionEntity questionOptionEntity = new QuestionOptionEntity();
		questionOptionEntity.setMeasureId(id);
		List<QuestionOptionEntity> optionList = commonDao.searchList(questionOptionEntity,SQL_SELECT_OPTIONS_INFO_LIST);
		if (optionList.size() > 0) {
			Map<String, String> option = new HashMap<String, String>();
			// 总共有多少问题
			BigDecimal questionNum = optionList.get(0).getTotalQuestion();
			int ques = questionNum.intValue();
			
			// 选项的最大值
			BigDecimal optionNum = optionList.get(0).getTotalOption();
			int opt = optionNum.intValue();
			ArrayList<Map<String, String>> optionMapList = new ArrayList<Map<String, String>>();
			Map<String, String> map = new HashMap<String, String>();
			// 遍历所有的问题
			for (int i = 0; i < ques; i++) {
				// 遍历optionList
				for (int j = 0; j < optionList.size(); j++) {
					if (optionList.get(j).getQuestionOrder() != null) {
						if (i == (optionList.get(j).getQuestionOrder())
								.intValue()) {
							if (optionList.get(j).getOptionOrder() != null) {
								// 选项排序为1的时候则是一个新问题，第一列作为问题序列号
								if (optionList.get(j).getOptionOrder()
										.intValue() == 0) {
									map.put("optionMapListTitle", "问题" + (i+1));
								}
								// 得到 某一选项选择人数与填写问卷总人数的比率
								String rateString = optionList.get(j)
										.getSelectedNum()
										+ "/"
										+ optionList.get(j).getTotalNum();
//								if (optionList.get(j).getSelectedNum()
//										.intValue() == 0) {
//									rateString = "";
//								}
								// 保存某个选项的选择人数与总人数的比数值
								map.put("optionMapList"
										+ (optionList.get(j)
												.getOptionOrder()
												.intValue()), rateString);
							}else{
								throw new BusinessException(
										getMessage(TpMsgNames.MSG_E_QUESTION_OPTION_NOT_ORDER));
							}
						}
					}else{
						throw new BusinessException(
								getMessage(TpMsgNames.MSG_E_QUESTION_NOT_ORDER));
					}
				}
				optionMapList.add(map);
				map = new HashMap<String, String>();
			}
			return optionMapList;
		}
		return null;
	}

	/**
	 * 问卷统计检索选项最大值
	 * 
	 * @author HEYAQIN
	 * @param measureId
	 *            问卷ID
	 * @return 返回结果
	 */
	public int searchTotalOptions(String measureId) {
		QuestionOptionEntity questionOptionEntity = new QuestionOptionEntity();
		questionOptionEntity.setMeasureId(measureId);
		int optionNum = commonDao.searchCount(questionOptionEntity,
				SQL_SELECT_MAX_OPTIONS_INFO_QUESTION);
		return optionNum;
	}

	/**
	 * 计划管理
	 * 
	 * @param planView
	 * @return
	 */
	public ResultBean userManagePlan(PlanView planView) {
		// 判断用户是否登录
		assertNotGuest();
		planView.setManageUser(WebContext.getSessionUserId());
		ResultBean result = commonDao.searchList4Page(planView,
				SEARCH_PLAN_BY_MANAGEUSER);
		return result;

	}

	/**
	 * 检索该教师审批状态
	 * 
	 * @author LILIANG
	 * @param TeacherApplyEntity
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public TeacherApplyEntity searchTeacherApply(
			TeacherApplyEntity teacherApplyEntity) {
		teacherApplyEntity.setUserId(WebContext.getSessionUserId());
		TeacherApplyEntity teacherApply = commonDao.searchOneData(
				teacherApplyEntity, SEARCH_TEACHER_APPLY_STATUS);
		if (teacherApply != null) {
			List tagNameList = commonDao.searchList(teacherApply.getId(),
					SEARCH_TAG_NAME_BY_TAG_LINK);
			teacherApply.setTagNames(StringUtil.join(tagNameList, " , "));
		}
		return teacherApply;
	}

	/**
	 * 检索当前用户信息包含积分
	 * 
	 * @return
	 */
	public ResultBean searchUserInformation() {
		ResultBean resultBean=new ResultBean();
		// 参与者信息
		UserEntity userEntity = (UserEntity) WebContext.getSessionUser();

		// 检索参与者的总积分
		UserPoint userPoint = pointService.fetchPoint(WebContext
				.getSessionUserId());
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("userEntity", userEntity);
		map.put("userPoint", userPoint);
		map.put("isDelDiscuss", false);
		map.put("isTeacher", isTeacherOne());
		map.put("hsSessionId", WebContext.getSession().getId());
		if(CourseService.isAdministrator()){
		    map.put("isAdmin", true);
        }else{
            map.put("isAdmin", false);
        }
		resultBean.setData(map);
		resultBean.setStatus(true);
		return resultBean;
	}
	
	/**
	 * 判断当前用户是否是教师
	 * @return  true 是教师  false不是教师
	 */
	public boolean isTeacherOne() {
        return isTeacher(WebContext.getSessionUserId());
    }
	/**
	 * 判断用户是否是教师
	 * @return  true 是教师  false不是教师
	 */
	public boolean isTeacher(String userId) {
		UserRoleEntity ur = new UserRoleEntity();
		ur.setUserId(userId);
		ur.setRoleId(ROLE_TEACHER_ROLE);
		int count = commonDao.searchCount(ur, CourseDbConstants.SQL_IS_TEACHHER);
        return count > 0;
    }
	/**
	 * 检索该教师审批状态
	 * @author LILIANG
	 * @param TeacherApplyEntity
	 * @return 判断用户名是否存在 true存在/false不存在
	 */
	public boolean searchUserIsTeacher(){
		UserRoleEntity role = new UserRoleEntity();
		role.setUserId(WebContext.getSessionUserId());
		List<UserRoleEntity> userRole = commonDao.searchList(role, SEARCH_USER_ROLE);
		boolean flag = false;
		for(int i=0;i<userRole.size();i++){
			if(ROLE_TEACHER_ROLE.equals(userRole.get(i).getRoleId())){
				flag = true;
				return flag;
			}
		}			
		return flag;
	}

    public ResultBean selectAnswerUserList(String measurementId, String userId) {
        UserMeasuermentAnswerEntity en=new UserMeasuermentAnswerEntity();
        en.setUserId(userId);
        en.setMeasuermentId(measurementId);
        return commonDao.searchList4Page(en, SQL_SELECT_ANSWER_USER);
    }

	public ResultBean selectQuestionResultList(MeasurementInstitutionEntity condition) {
		//判断是否登录
		assertNotGuest();
		//condition.setUserId(WebContext.getSessionUserId());
		return commonDao.searchList4Page(condition,SQL_SELECT_MEASURE_RESULT_INFO_LIST);
	}

	public ArrayList<Map<String, String>> selectInstitutionOptionsList(String id) {

		QuestionOptionInstitutionEntity questionOptionEntity = new QuestionOptionInstitutionEntity();
		questionOptionEntity.setMeasureId(id);
		List<QuestionOptionInstitutionEntity> optionList = commonDao.searchList(questionOptionEntity,SQL_SELECT_INSTITUTION_OPTIONS_INFO_LIST);
		if (optionList.size() > 0) {
			Map<String, String> option = new HashMap<String, String>();
			// 总共有多少问题
			BigDecimal questionNum = optionList.get(0).getTotalQuestion();
			int ques = questionNum.intValue();
			
			// 选项的最大值
			BigDecimal optionNum = optionList.get(0).getTotalOption();
			int opt = optionNum.intValue();
			ArrayList<Map<String, String>> optionMapList = new ArrayList<Map<String, String>>();
			Map<String, String> map = new HashMap<String, String>();
			// 遍历所有的问题
			for (int i = 1; i < ques+1; i++) {
				// 遍历optionList
				for (int j = 0; j < optionList.size(); j++) {
					if (optionList.get(j).getQuestionOrder() != null) {
						if (i == Integer.valueOf(optionList.get(j).getQuestionOrder().toString())) {
							if (optionList.get(j).getOptionOrder() != null) {
								// 选项排序为1的时候则是一个新问题，第一列作为问题序列号
								if (Integer.valueOf(optionList.get(j).getOptionOrder().toString())== 0) {
									map.put("optionMapListTitle", "问题" + (i));
								}
								// 得到 某一选项选择人数与填写问卷总人数的比率
								String rateString = optionList.get(j)
										.getSelectedNum()
										+ "/"
										+ optionList.get(j).getTotalNum();
//								if (optionList.get(j).getSelectedNum()
//										.intValue() == 0) {
//									rateString = "";
//								}
								// 保存某个选项的选择人数与总人数的比数值
								map.put("optionMapList"
										+ (Integer.valueOf(optionList.get(j).getOptionOrder().toString())), rateString);
							}else{
								throw new BusinessException(
										getMessage(TpMsgNames.MSG_E_QUESTION_OPTION_NOT_ORDER));
							}
						}
					}else{
						throw new BusinessException(
								getMessage(TpMsgNames.MSG_E_QUESTION_NOT_ORDER));
					}
				}
				optionMapList.add(map);
				map = new HashMap<String, String>();
			}
			return optionMapList;
		}
		return null;
	}

	public void deleteEvalutionQuestionList(String id) {
		String[] ids = StringUtil.split(id, ",");
		if (ids != null) {
			for (String measureId : ids) {
				if (StringUtil.isNotEmpty(measureId)) {
					MeasurementInstitutionEntity entity = new MeasurementInstitutionEntity();
					entity.setId(measureId);
					MeasurementInstitutionEntity measurementEntity = searchOneData(entity);
					if(measurementEntity!= null){
						institutionService.delEvalutionMeasure(measurementEntity.getLinkId(),measurementEntity.getLinkType(),MEASUERMENT_TYPE_QUESTIONNAIRE);
					}
				}
			}
		}
		
	}

	public ResultBean selectEvalutionQuestionUserList(String measurementId) {
		//判断是否登录
        assertNotGuest();
        UserMeasurementResultEntity condition=new UserMeasurementResultEntity();
        condition.setMeasurementId(measurementId);
        return commonDao.searchList4Page(condition,CourseDbConstants.SQL_INSTITUTION_MEASUERMENT_USER_SEARCH);
	}

	public ResultBean selectEvalutionAnswerUserList(String measurementId, String userId) {
		    UserMeasuermentAnswerEntity en=new UserMeasuermentAnswerEntity();
	        en.setUserId(userId);
	        en.setMeasuermentId(measurementId);
	        return commonDao.searchList4Page(en, SQL_SELECT_ANSWER_USER);
	}

	public int searchInstitutionTotalOptions(String id) {
		QuestionOptionInstitutionEntity questionOptionEntity = new QuestionOptionInstitutionEntity();
		questionOptionEntity.setMeasureId(id);
		int optionNum = commonDao.searchCount(questionOptionEntity,
				SQL_SELECT_INSTITUTION_MAX_OPTIONS_INFO_QUESTION);
		return optionNum;
	}

	public ArrayList<Map<String, String>> selectTeacherOptionsList(String id) {
		QuestionOptionTeacherEntity questionOptionEntity = new QuestionOptionTeacherEntity();
		questionOptionEntity.setMeasureId(id);
		List<QuestionOptionTeacherEntity> optionList = commonDao.searchList(questionOptionEntity,SQL_SELECT_TEACHER_OPTIONS_INFO_LIST);
		if (optionList.size() > 0) {
			Map<String, String> option = new HashMap<String, String>();
			// 总共有多少问题
			BigDecimal questionNum = optionList.get(0).getTotalQuestion();
			int ques = questionNum.intValue();
			
			// 选项的最大值
			BigDecimal optionNum = optionList.get(0).getTotalOption();
			int opt = optionNum.intValue();
			ArrayList<Map<String, String>> optionMapList = new ArrayList<Map<String, String>>();
			Map<String, String> map = new HashMap<String, String>();
			// 遍历所有的问题
			for (int i = 1; i < ques+1; i++) {
				// 遍历optionList
				for (int j = 0; j < optionList.size(); j++) {
					if (optionList.get(j).getQuestionOrder() != null) {
						if (i == Integer.valueOf(optionList.get(j).getQuestionOrder().toString())) {
							if (optionList.get(j).getOptionOrder() != null) {
								// 选项排序为1的时候则是一个新问题，第一列作为问题序列号
								if (Integer.valueOf(optionList.get(j).getOptionOrder().toString())== 0) {
									map.put("optionMapListTitle", "问题" + (i));
								}
								// 得到 某一选项选择人数与填写问卷总人数的比率
								String rateString = optionList.get(j)
										.getSelectedNum()
										+ "/"
										+ optionList.get(j).getTotalNum();
//								if (optionList.get(j).getSelectedNum()
//										.intValue() == 0) {
//									rateString = "";
//								}
								// 保存某个选项的选择人数与总人数的比数值
								map.put("optionMapList"
										+ (Integer.valueOf(optionList.get(j).getOptionOrder().toString())), rateString);
							}else{
								throw new BusinessException(
										getMessage(TpMsgNames.MSG_E_QUESTION_OPTION_NOT_ORDER));
							}
						}
					}else{
						throw new BusinessException(
								getMessage(TpMsgNames.MSG_E_QUESTION_NOT_ORDER));
					}
				}
				optionMapList.add(map);
				map = new HashMap<String, String>();
			}
			return optionMapList;
		}
		return null;
	}

	public ResultBean selectTeacherEvalutionQuestionUserList(String measurementId) {
		   assertNotGuest();
	        UserMeasurementResultEntity condition=new UserMeasurementResultEntity();
	        condition.setMeasurementId(measurementId);
	        return commonDao.searchList4Page(condition,CourseDbConstants.SQL_TEACHER_MEASUERMENT_USER_SEARCH);
	}

	public TpUserEntity searchById(TpUserEntity userEntity) {
		userEntity.setSortName("create_date");
		userEntity.setSortOrder("desc");
		return commonDao.searchOneData(userEntity,USER_BY_USERID);
	}
}
