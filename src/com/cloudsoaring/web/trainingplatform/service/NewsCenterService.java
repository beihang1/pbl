/**
* Filename : NoticeManageService.java
* Author : TongLei
* Creation time : 下午1:44:13 - 2015年11月11日
* Description :
*/

package com.cloudsoaring.web.trainingplatform.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;



import com.cloudsoaring.exception.BusinessException;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.FileEntity;
import com.cloudsoaring.web.bus.entity.TagEntity;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.view.TagView;
import com.cloudsoaring.web.myinfo.entity.StudentClassInfoEntity;
import com.cloudsoaring.web.trainingplatform.constants.TpConstants;
import com.cloudsoaring.web.trainingplatform.entity.NewsCenterEntity;
import com.cloudsoaring.web.trainingplatform.entity.TpUserEntity;


/**
 * 公告管理相关业务
 * @author TongLei
 *
 */
@Service
public class NewsCenterService extends FileService implements TpConstants{
	
	/**
	 * 检索公告分类信息
	 * @param entity
	 * @return 返回结果
	 */
	public List searchNoticeTag(TagView view){
		return commonDao.searchList(view, SEARCH_NEWS_CENTER_CATEGORY);
	}
	/**
	 * 检索公告信息
	 * @param entity
	 * @return 返回结果
	 */
	public ResultBean searchNotice(NewsCenterEntity entity){
		return commonDao.searchList4Page(entity, SEARCH_NEWS_CENTER);
	}
	
	public NewsCenterEntity selectNotice(NewsCenterEntity entity){
		return commonDao.searchOneData(entity, EDIT_NEWS_CENTER_SELECT);
	}
	/**
	 * 新增公告
	 * @param entity
	 * @throws Exception
	 */
	public void insertNotice(NewsCenterEntity entity) throws Exception{
		//图片处理
		//FileEntity pictureFile = processRequestFile("picture",FILE_TYPE_IMAGE);
		//compressToImage(pictureFile, false);
		//entity.setPictureId(pictureFile.getFileId());
		entity.setStatus(INFOMATION_STATUS_NOT);
		entity.setRecomment(new BigDecimal(0));
		entity.setSortOrder("1");
		this.commonDao.insertData(entity,ADD_NEWS_CENTER);
	}
	
	/**
	 * 新增公告
	 * @param view
	 * 
	 */
	public void insertNoticeType(TagView view){
			this.commonDao.insertData(view,ADD_NOTICE_TYPE);
	}
	/**
	 * 发布公告
	 * @param entity
	 */
	public ResultBean isStatus(NewsCenterEntity entity){
		if(isNoticeExist(entity)){
			throw new BusinessException(getMessage(MSG_NOTICE_IS_NULL));
		}
		NewsCenterEntity infoEntity =new NewsCenterEntity();
		infoEntity = this.commonDao.searchOneData(entity, SEARCH_NEWS_CENTER);
		infoEntity.setUpdateUser(WebContext.getSessionUserId());
		if(INFOMATION_STATUS_NOT.equals(infoEntity.getStatus())){
			infoEntity.setStatus(INFOMATION_STATUS_ALREADY);
		}else{
			infoEntity.setStatus(INFOMATION_STATUS_NOT);
			this.commonDao.updateData(infoEntity, STATUS_NEWS_CENTER);
			return ResultBean.success(STATUS_NOT_SUCCESS);
		}
		this.commonDao.updateData(infoEntity, STATUS_NEWS_CENTER);
		return ResultBean.success(STATUS_NOTICE_SUCCESS);
	}
	/**
	 * 删除公告
	 * @param entity
	 * @throws Exception
	 */
	public void deleteInfo(NewsCenterEntity entity)throws Exception{
		if(isNoticeExist(entity)){
			throw new BusinessException(getMessage(MSG_NOTICE_IS_NULL));
		}
		NewsCenterEntity infoEntity =new NewsCenterEntity();
		infoEntity = this.commonDao.searchOneData(entity, SEARCH_NEWS_CENTER);
		String fileId = infoEntity.getPictureId();
		this.deleteFileById(fileId,FILE_TYPE_IMAGE);
		this.commonDao.deleteData(entity, DELETE_NEWS_CENTER);
	}
	public Boolean isNoticeExist(NewsCenterEntity entity){
		int cnt = commonDao.searchCount(entity, NEWS_CENTER_EXIST);
		return cnt == 0;
	}
	
	public void deleteNoticeType(TagView view){
		view = this.commonDao.searchOneData(view, SEARCH_NOTICE_TYPE);
		this.commonDao.deleteData(view, DELETE_NOTICE_TYPE);
	}
	/**
	 * 编辑公告
	 * @param entity
	 */
	public void editNotice(NewsCenterEntity entity) throws Exception{
		if(isNoticeExist(entity)){
			throw new BusinessException(getMessage(MSG_NOTICE_IS_NULL));
		}
		FileEntity pictureFile = processRequestFile("picture",FILE_TYPE_IMAGE);
		compressToImage(pictureFile, false);
		//图片处理
//			if(!entity.getOldPictureId().equals(pictureFile.getFileId())){
//				String fileId = entity.getOldPictureId();
//				this.deleteFileById(fileId, FILE_TYPE_IMAGE);
//			}
			entity.setPictureId(pictureFile.getFileId());
			entity.setUpdateUser(WebContext.getSessionUserId());
			entity.setSortOrder("1");
		this.commonDao.updateData(entity, EDIT_NEWS_CENTER_MESSAGE);
	}

	public void editNoticeType(TagView view){
		this.commonDao.updateData(view,EDIT_NOTICE_TYPE);
	}
	
	@SuppressWarnings("unchecked")
	public ResultBean searchInfoByCategoryId(NewsCenterEntity info){
		ResultBean result = new ResultBean();
		List<NewsCenterEntity> entityList = new ArrayList<NewsCenterEntity>();
		entityList = (List<NewsCenterEntity>) commonDao.searchList4Page(info, SELECT_NEWS_CENTER).getData();
		if(null!=entityList && entityList.size()>0) {
			for(int i=0;i<entityList.size();i++) {
				TagEntity tag = new TagEntity();	
				tag.setTagId(entityList.get(i).getCategoryId());
				TagEntity tags = commonDao.searchOneData(tag, SEARCH_TAG_BY_TAG_ID);
				entityList.get(i).setCategoryName(tags.getTagName());
				//时间格式化字符串
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				entityList.get(i).setCreateTime(sdf.format(entityList.get(i).getCreateDate()));
			}
		}
		result.setData(entityList);
		List<NewsCenterEntity> list = commonDao.searchList(info, SELECT_NEWS_CENTER);
		 int iPageCount=list.size()/info.getPageSize();
         if (list.size()%info.getPageSize()>0) {
            iPageCount++;
         }
		result.setTotal(iPageCount);
		return result;
	}
	
	public ResultBean searchCategory(TagView view){
		ResultBean result = new ResultBean();
		List<TagView> list = commonDao.searchList(view, SEARCH_NEWS_CENTER_CATEGORY);
		result.setStatus(true);
		result.setData(list);
		return result;
	}
	
	public NewsCenterEntity searchInfoById(NewsCenterEntity info){
		return commonDao.searchOneData(info, SEARCH_NEWS_CENTER_BY_ID);
	}
	public ResultBean searchTeacherInfoByCategoryId(NewsCenterEntity info) {
		ResultBean result = new ResultBean();
		List<NewsCenterEntity> entityList = new ArrayList<NewsCenterEntity>();
		TpUserEntity entity1=new TpUserEntity();
		entity1.setUserId(WebContext.getSessionUserId());
		entity1 = commonDao.searchOneData(entity1, USER_BY_USERID);
		if(null!=entity1 && StringUtils.isNotBlank(entity1.getSchoolName())) {
			info.setSchoolName(entity1.getSchoolName());
		}
		entityList = (List<NewsCenterEntity>) commonDao.searchList4Page(info, SELECT_NOT_NEWS_CENTER).getData();
		if(null!=entityList && entityList.size()>0) {
			for(int i=0;i<entityList.size();i++) {
				TagEntity tag = new TagEntity();	
				tag.setTagId(entityList.get(i).getCategoryId());
				TagEntity tags = commonDao.searchOneData(tag, SEARCH_TAG_BY_TAG_ID);
				entityList.get(i).setCategoryName(tags.getTagName());
				//时间格式化字符串
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				entityList.get(i).setCreateTime(sdf.format(entityList.get(i).getCreateDate()));
			}
		}
		result.setData(entityList);
		List<NewsCenterEntity> list = commonDao.searchList(info, SELECT_NOT_NEWS_CENTER);
		 int iPageCount=list.size()/info.getPageSize();
         if (list.size()%info.getPageSize()>0) {
            iPageCount++;
         }
		result.setTotal(iPageCount);
		return result;
	}
	public ResultBean searchStudentInfoByCategoryId(NewsCenterEntity info) {
		ResultBean result = new ResultBean();
		List<NewsCenterEntity> entityList = new ArrayList<NewsCenterEntity>();
		StudentClassInfoEntity entity1=new StudentClassInfoEntity();
		entity1.setStudentId(WebContext.getSessionUserId());
		entity1 = commonDao.searchOneData(entity1, STUDENT_ONE_LASS_SEARCH);
		if(null!=entity1 && StringUtils.isNotBlank(entity1.getSchoolName())) {
			info.setSchoolName(entity1.getSchoolName());
		}
		entityList = (List<NewsCenterEntity>) commonDao.searchList4Page(info, SELECT_NOT_NEWS_CENTER).getData();
		if(null!=entityList && entityList.size()>0) {
			for(int i=0;i<entityList.size();i++) {
				TagEntity tag = new TagEntity();	
				tag.setTagId(entityList.get(i).getCategoryId());
				TagEntity tags = commonDao.searchOneData(tag, SEARCH_TAG_BY_TAG_ID);
				entityList.get(i).setCategoryName(tags.getTagName());
				//时间格式化字符串
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				entityList.get(i).setCreateTime(sdf.format(entityList.get(i).getCreateDate()));
			}
		}
		result.setData(entityList);
		List<NewsCenterEntity> list = commonDao.searchList(info, SELECT_NOT_NEWS_CENTER);
		 int iPageCount=list.size()/info.getPageSize();
         if (list.size()%info.getPageSize()>0) {
            iPageCount++;
         }
		result.setTotal(iPageCount);
		return result;
	}
	public List<NewsCenterEntity> searchInfoNewsId(NewsCenterEntity info) {
		List<NewsCenterEntity> entityList = new ArrayList<NewsCenterEntity>();
		entityList = commonDao.searchList(info, SELECT_NEWS_CENTER);
		if(null!=entityList && entityList.size()>0) {
			for(int i=0;i<entityList.size();i++) {
				TagEntity tag = new TagEntity();	
				tag.setTagId(entityList.get(i).getCategoryId());
				TagEntity tags = commonDao.searchOneData(tag, SEARCH_TAG_BY_TAG_ID);
				entityList.get(i).setCategoryName(tags.getTagName());
			}
		}
		return entityList;
	}
	public List<NewsCenterEntity> searchTeacherNewsId(NewsCenterEntity info) {
		List<NewsCenterEntity> entityList = new ArrayList<NewsCenterEntity>();
		TpUserEntity entity1=new TpUserEntity();
		entity1.setUserId(WebContext.getSessionUserId());
		entity1 = commonDao.searchOneData(entity1, USER_BY_USERID);
		if(null!=entity1 && StringUtils.isNotBlank(entity1.getSchoolName())) {
			info.setSchoolName(entity1.getSchoolName());
		}
		entityList = commonDao.searchList(info, SELECT_NOT_NEWS_CENTER);
		if(null!=entityList && entityList.size()>0) {
			for(int i=0;i<entityList.size();i++) {
				TagEntity tag = new TagEntity();	
				tag.setTagId(entityList.get(i).getCategoryId());
				TagEntity tags = commonDao.searchOneData(tag, SEARCH_TAG_BY_TAG_ID);
				entityList.get(i).setCategoryName(tags.getTagName());
			}
		}
		return entityList;
	}
	public List<NewsCenterEntity> searchStudentInfoNewsId(NewsCenterEntity info) {
		List<NewsCenterEntity> entityList = new ArrayList<NewsCenterEntity>();
		StudentClassInfoEntity entity1=new StudentClassInfoEntity();
		entity1.setStudentId(WebContext.getSessionUserId());
		entity1 = commonDao.searchOneData(entity1, STUDENT_ONE_LASS_SEARCH);
		if(null!=entity1 && StringUtils.isNotBlank(entity1.getSchoolName())) {
			info.setSchoolName(entity1.getSchoolName());
		}
		entityList =commonDao.searchList(info, SELECT_NOT_NEWS_CENTER);
		if(null!=entityList && entityList.size()>0) {
			for(int i=0;i<entityList.size();i++) {
				TagEntity tag = new TagEntity();	
				tag.setTagId(entityList.get(i).getCategoryId());
				TagEntity tags = commonDao.searchOneData(tag, SEARCH_TAG_BY_TAG_ID);
				entityList.get(i).setCategoryName(tags.getTagName());
			}
		}
		return entityList;
	}
	public NewsCenterEntity searchById(NewsCenterEntity newsCenterEntity) {
		NewsCenterEntity info = new NewsCenterEntity();
		info=	commonDao.searchOneData(newsCenterEntity, SELECT_NEWS_CENTER);
		return info;
	}
}
