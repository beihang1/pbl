/**
* Filename : NoticeManageService.java
* Author : TongLei
* Creation time : 下午1:44:13 - 2015年11月11日
* Description :
*/

package com.cloudsoaring.web.trainingplatform.service;

import java.util.List;

import org.springframework.stereotype.Service;



import com.cloudsoaring.exception.BusinessException;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.FileEntity;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.view.TagView;
import com.cloudsoaring.web.trainingplatform.constants.TpConstants;

import com.cloudsoaring.web.trainingplatform.entity.InfomationEntity;


/**
 * 公告管理相关业务
 * @author TongLei
 *
 */
@Service
public class NoticeManageService extends FileService implements TpConstants{
	
	/**
	 * 检索公告分类信息
	 * @param entity
	 * @return 返回结果
	 */
	public List searchNoticeTag(TagView view){
		return commonDao.searchList(view, SEARCH_NOTICE_CATEGORY);
	}
	/**
	 * 检索公告信息
	 * @param entity
	 * @return 返回结果
	 */
	public ResultBean searchNotice(InfomationEntity entity){
		return commonDao.searchList4Page(entity, SEARCH_NOTICE);
	}
	
	public InfomationEntity selectNotice(InfomationEntity entity){
		return commonDao.searchOneData(entity, EDIT_NOTICE_SELECT);
	}
	/**
	 * 新增公告
	 * @param entity
	 * @throws Exception
	 */
	public void insertNotice(InfomationEntity entity) throws Exception{
		//图片处理
		FileEntity pictureFile = processRequestFile("picture",FILE_TYPE_IMAGE);
		compressToImage(pictureFile, false);
		entity.setPictureId(pictureFile.getFileId());
		entity.setStatus(INFOMATION_STATUS_NOT);;
		this.commonDao.insertData(entity,ADD_NOTICE);
	}
	
	/**
	 * 新增公告
	 * @param view
	 * 
	 */
	public void insertNoticeType(TagView view){
			this.commonDao.insertData(view,ADD_NOTICE_TYPE);
	}
	/**
	 * 发布公告
	 * @param entity
	 */
	public ResultBean isStatus(InfomationEntity entity){
		if(isNoticeExist(entity)){
			throw new BusinessException(getMessage(MSG_NOTICE_IS_NULL));
		}
		InfomationEntity infoEntity =new InfomationEntity();
		infoEntity = this.commonDao.searchOneData(entity, SEARCH_NOTICE);
		infoEntity.setUpdateUser(WebContext.getSessionUserId());
		if(INFOMATION_STATUS_NOT.equals(infoEntity.getStatus())){
			infoEntity.setStatus(INFOMATION_STATUS_ALREADY);
		}else{
			infoEntity.setStatus(INFOMATION_STATUS_NOT);
			this.commonDao.updateData(infoEntity, STATUS_NOTICE);
			return ResultBean.success(STATUS_NOT_SUCCESS);
		}
		this.commonDao.updateData(infoEntity, STATUS_NOTICE);
		return ResultBean.success(STATUS_NOTICE_SUCCESS);
	}
	/**
	 * 删除公告
	 * @param entity
	 * @throws Exception
	 */
	public void deleteInfo(InfomationEntity entity)throws Exception{
		if(isNoticeExist(entity)){
			throw new BusinessException(getMessage(MSG_NOTICE_IS_NULL));
		}
		InfomationEntity infoEntity =new InfomationEntity();
		infoEntity = this.commonDao.searchOneData(entity, SEARCH_NOTICE);
		String fileId = infoEntity.getPictureId();
		this.deleteFileById(fileId,FILE_TYPE_IMAGE);
		this.commonDao.deleteData(entity, DELETE_NOTICE);
	}
	public Boolean isNoticeExist(InfomationEntity entity){
		int cnt = commonDao.searchCount(entity, NOTICE_EXIST);
		return cnt == 0;
	}
	
	public void deleteNoticeType(TagView view){
		view = this.commonDao.searchOneData(view, SEARCH_NOTICE_TYPE);
		this.commonDao.deleteData(view, DELETE_NOTICE_TYPE);
	}
	/**
	 * 编辑公告
	 * @param entity
	 */
	public void editNotice(InfomationEntity entity) throws Exception{
		if(isNoticeExist(entity)){
			throw new BusinessException(getMessage(MSG_NOTICE_IS_NULL));
		}
		FileEntity pictureFile = processRequestFile("picture",FILE_TYPE_IMAGE);
		compressToImage(pictureFile, false);
		//图片处理
			if(!entity.getOldPictureId().equals(pictureFile.getFileId())){
				String fileId = entity.getOldPictureId();
				this.deleteFileById(fileId, FILE_TYPE_IMAGE);
			}
			entity.setPictureId(pictureFile.getFileId());
			entity.setUpdateUser(WebContext.getSessionUserId());
		this.commonDao.updateData(entity, EDIT_NOTICE_MESSAGE);
	}


/**
 * 编辑公告
 * @param view
 */
	public void editNoticeType(TagView view){
		this.commonDao.updateData(view,EDIT_NOTICE_TYPE);
		}
}
