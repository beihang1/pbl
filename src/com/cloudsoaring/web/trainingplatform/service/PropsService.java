package com.cloudsoaring.web.trainingplatform.service;


import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.BigDecimalUtil;
import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.FileEntity;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.constant.Constants;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.points.constant.PointConstants;
import com.cloudsoaring.web.points.entity.UserPoint;
import com.cloudsoaring.web.points.service.PointService;
import com.cloudsoaring.web.trainingplatform.constants.TpDbConstants;
import com.cloudsoaring.web.trainingplatform.constants.TpStateConstants;
import com.cloudsoaring.web.trainingplatform.entity.PropsEntity;
import com.cloudsoaring.web.trainingplatform.entity.PropsHisEntity;

@Service
public class PropsService extends  FileService implements TpDbConstants,TpStateConstants{
    @Autowired
    private PointService pointService;
    public ResultBean searchPropsList(PropsEntity entity) {
        return commonDao.searchList4Page(entity, SQL_SEARCH_PROPS);
    }

    public ResultBean saveProps(PropsHisEntity entity) {
        // 判断用户是否登录
        assertNotGuest();
        if(entity==null){
            return ResultBean.error().setMessages("道具赠送内容为空！");
        }
        assertNotEmpty(entity.getPropsId(), "道具ID为空");
        assertNotEmpty(entity.getGetUserId(), "赠送教师ID为空");
        assertNotEmpty(entity.getGaveNum().toString(), "道具赠送数量为空");
        assertNotEmpty(entity.getPropsFavour().toString(), "道具好感度为空");
        assertNotEmpty(entity.getPropsPrice().toString(), "道具价格不能为空");
        if(entity.getPropsFavour().compareTo(BigDecimal.ONE)<0){
            return ResultBean.error().setMessages("赠送数量不能小于1");
        }
        //扣除道具积分
        BigDecimal point=entity.getGaveNum().multiply(entity.getPropsPrice());
        UserPoint userPoint = pointService.consumePoint(PointConstants.SYSTEM_CODE_HS, WebContext.getSessionUserId(), "D1001", point , null,null);
        if (userPoint.isStatus()) {
	        //添加赠送历史
	        entity.setId(IDGenerator.genUID());
	        entity.setTimeGave(new Date());
	        entity.setGaveUserId(WebContext.getSessionUserId());
	        entity.setFlagShowMsg("0");
	        commonDao.insertData(entity, SQL_INSERT_PROPS_HIS);
	        //更新教师社交表中的好感度
            return ResultBean.success().setMessages("赠送成功！");
        } else {
        	return ResultBean.error().setMessages(userPoint.getMessages());
        }
    }

    public ResultBean searchGetPropsHis(PropsHisEntity entity) {
        // 判断用户是否登录
        assertNotEmpty(entity.getGetUserId(), "导师ID不能为空");
        entity.setSortName("timeGave");
        entity.setSortOrder("DESC");
        return commonDao.searchList4Page(entity, SQL_SEARCH_PROPS_HIS_GET);
    }
    public ResultBean searchGivePropsHis(PropsHisEntity entity) {
        // 判断用户是否登录
       assertNotEmpty(entity.getGetUserId(), "导师ID不能为空");
       entity.setSortName("propsFavourTotal");
       entity.setSortOrder("DESC");
       return commonDao.searchList4Page(entity, SQL_SEARCH_PROPS_HIS_DIVE);
    }

    public PropsEntity searchPropsById(PropsEntity entity) {
        PropsEntity props=commonDao.searchOneData(entity, SQL_SEARCH_PROPS_BY_ID);
        if(props.getPrice()!=null){
            props.setPrice(BigDecimalUtil.ignoreZeroScale(props.getPrice()));
        }
        if(props.getNumFavour()!=null){
            props.setNumFavour(BigDecimalUtil.ignoreZeroScale(props.getNumFavour()));
        }
        return props;
    }

    public void delProps(String id) {
        String[] ids = StringUtil.split(id, ",");
        if (ids != null) {
            for (String propsId : ids) {
                if (StringUtil.isNotEmpty(propsId)) {
                    commonDao.updateData(propsId,SQL_DELETE_PROPS);
                }
            }
         }
    }
    public ResultBean searchPropsHisList(PropsHisEntity entity) {
        return commonDao.searchList4Page(entity, SQL_SEARCH_PROPS_HIS_LIST);
    }

    public void addProps(PropsEntity entity) throws Exception {
        // 图片处理
        FileEntity pictureFile = processRequestFile("image", Constants.FILE_TYPE_IMAGE);
        // 缩略图
        compress(pictureFile, false);
        if(pictureFile != null && StringUtil.isNotEmpty(pictureFile.getFileId())){
            entity.setPropsIcon(pictureFile.getFileId());
        }
        entity.setId(IDGenerator.genUID());
        commonDao.insertData(entity, SQL_INSERT_PROPS);
        
    }

    public void editProps(PropsEntity entity) throws Exception {
        // 图片处理
        FileEntity pictureFile = processRequestFile("image", Constants.FILE_TYPE_IMAGE);
        // 缩略图
        compress(pictureFile, false);
        if(pictureFile != null && StringUtil.isNotEmpty(pictureFile.getFileId())){
            entity.setPropsIcon(pictureFile.getFileId());
        }
        commonDao.updateData(entity, SQL_UPDATE_PROPS);
    }

    @SuppressWarnings("static-access")
    public ResultBean updateFlagShowMsg(PropsHisEntity entity) {
        ResultBean result=new ResultBean();
        // 判断用户是否登录
        assertNotGuest();
        assertNotEmpty(entity.getId(), "赠送历史ID不能为空");
        assertExist(entity,SQL_PROPS_NOT_EXIST,"赠送历史信息不存在！");
        PropsHisEntity propsHis=commonDao.searchOneData(entity, SQL_SELECT_PROPS_HIS_BY_ID);
        if(propsHis != null){
            if(!propsHis.getGetUserId().equals(WebContext.getSessionUserId())){
                return result.error().setMessages("您不是被赠送人，无法屏蔽信息！");
            }
        }
        entity.setFlagShowMsg(FLAG_SHOW_MSG_NOT);
        commonDao.updateData(entity, SQL_UPDATE_FLAG_SHOW_MSG);
        return result.success().setMessages("屏蔽成功！");
    }

    public ResultBean immediatePayDetail(String propsId,String num) {
        assertNotGuest();
        ResultBean result = new ResultBean();
        assertNotEmpty(propsId, "道具ID为空");
        assertNotEmpty(num, "道具赠送数量不能为空");
        //道具
        PropsEntity props=commonDao.searchOneData(propsId, SQL_SEARCH_PROPS_BY_ID);
        props.setNum(num);
       // 检索参与者的总积分
        UserPoint userPoint = pointService.fetchPoint(WebContext.getSessionUserId());
        BigDecimal  price= props.getPrice().multiply(new BigDecimal(num));
        // 计算支付后积分余额
        BigDecimal remainPoints = userPoint.getPoints().subtract(price);
        Map<Object, Object> map = new HashMap<Object, Object>();
        map.put("props", props);
        map.put("userPoint", userPoint);
        map.put("remainPoints", remainPoints);
        result.setData(map);
        result.setStatus(true);
        return result;
    }
}
