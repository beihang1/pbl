package com.cloudsoaring.web.trainingplatform.service;

import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.common.service.BaseService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.trainingplatform.constants.TpDbConstants;
import com.cloudsoaring.web.trainingplatform.constants.TpStateConstants;
import com.cloudsoaring.web.trainingplatform.entity.TeacherLevelEntity;

@Service
public class TeacherLevelManagerService extends BaseService implements TpDbConstants,TpStateConstants{

    public ResultBean teacherLevel(TeacherLevelEntity entity) {
        return commonDao.searchList4Page(entity, SQL_SEARCH_TEAHER_LEVEL_LIST);
    }

    public void addLevel(TeacherLevelEntity entity) {
       entity.setLevelId(IDGenerator.genUID());
       entity.setCreateUser(WebContext.getSessionUserId());
       commonDao.insertData(entity, SQL_INSERT_TEACHER_LEVEL);
    }
    /**
     * 老师等级删除
     * @param levelId 等级ID
     */
    public void teacherLevelDelete(String levelId) {
        String[] levelIds = StringUtil.split(levelId, ",");
        if (levelIds != null) {
            for (String id : levelIds) {
                if (StringUtil.isNotEmpty(id)) {
                    TeacherLevelEntity entity=new TeacherLevelEntity();
                    entity.setLevelId(id);
                    commonDao.deleteData(entity, SQL_TEACHER_LEVEL_DELETE);
                }
            }
         }
        }
    /**
     * 编辑老师等级管理
     * @param entity 老师等级对象
     */
    public void updateLevel(TeacherLevelEntity entity) {
        entity.setUpdateUser(WebContext.getSessionUserId());
        commonDao.updateData(entity, SQL_UPDATE_TEACHER_LEVEL);
    }
    /**
     * 查询老师等级信息
     * @param levelId 等级ID
     * @return
     */
    public TeacherLevelEntity seachTeacherLevelById(String levelId) {
        TeacherLevelEntity entity= new TeacherLevelEntity();
        entity.setLevelId(levelId);
        return commonDao.searchOneData(entity, SQL_SEACHER_TEACHER_LEVEL_BY_ID);
    }
    
}
