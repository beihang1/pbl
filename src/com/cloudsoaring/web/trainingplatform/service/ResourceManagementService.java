package com.cloudsoaring.web.trainingplatform.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.FileEntity;
import com.cloudsoaring.web.bus.entity.TagEntity;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.constant.Constants;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.constant.CourseDbConstants;
import com.cloudsoaring.web.course.constant.CourseStateConstants;
import com.cloudsoaring.web.course.entity.ChapterEntity;
import com.cloudsoaring.web.course.entity.CourseMessageEntity;
import com.cloudsoaring.web.course.entity.MeasurementEntity;
import com.cloudsoaring.web.course.entity.TagLinkEntity;
import com.cloudsoaring.web.course.entity.UserCourseEntity;
import com.cloudsoaring.web.course.view.CourseManagerView;
import com.cloudsoaring.web.course.view.CourseView;
import com.cloudsoaring.web.course.view.TagView;
import com.cloudsoaring.web.pointmanager.constant.PointStateConstants;
import com.cloudsoaring.web.points.constant.PointConstants;
import com.cloudsoaring.web.points.entity.PointTypeEntity;
import com.cloudsoaring.web.trainingplatform.constants.TpDbConstants;
import com.cloudsoaring.web.trainingplatform.constants.TpStateConstants;
import com.cloudsoaring.web.trainingplatform.view.ResourceView;

@Service
public class ResourceManagementService extends FileService
		implements TpDbConstants, TpStateConstants, CourseConstants, CourseStateConstants {

	/**
	 * 资源管理列表
	 * 
	 * @param entity
	 * @return
	 */
	public ResultBean resourceList(ResourceView entity) {

		return commonDao.searchList4Page(entity, SQL_SEARCH_RESOURSE_LIST);
	}

	/**
	 * 
	 * 删除资源
	 * 
	 * @param id
	 */
	public void deleteResource(String id) {
		String[] ids = StringUtil.split(id, ",");
		if (ids != null) {
			for (String resourceId : ids) {
				if (StringUtil.isNotEmpty(resourceId)) {
					ResourceView entity = new ResourceView();
					entity.setId(resourceId);
					entity.setStateShare(STATE_SHARE_CANCEL);
					commonDao.updateData(entity, SQL_DELETE_RESOURSE_STATE);
				}
			}
		}

	}

	/**
	 * 更新资源信息
	 * 
	 * @param entity
	 * @return
	 * @throws Exception
	 */
	public ResultBean updateResource(ResourceView entity) throws Exception {
		ResultBean result = new ResultBean();
		FileEntity resultData = processRequestFile("file1", CourseStateConstants.FILE_TYPE_NETDISK);
		// 图片处理
		FileEntity pictureFile = processRequestFile("picture", Constants.FILE_TYPE_IMAGE);
		// 缩略图
		compress(pictureFile, false);
		if (pictureFile != null) {
			entity.setPictureId(pictureFile.getFileId());
		}
		ResourceView resource = commonDao.searchOneData(entity, SQL_INSERT_RESOURCE_BY_ID);
		if(resource != null) {
			resource.setFileId(resultData.getFileId());
			resource.setBriefInTroduction(entity.getBriefInTroduction());
			resource.setPictureId(entity.getPictureId());
			resource.setFileName(entity.getFileName());
			resource.setUserId(WebContext.getSessionUserId());
			commonDao.updateData(resource, SQL_UPDATE_RESOURCE_BY_ID);
			TagLinkEntity tag = new TagLinkEntity();
			tag.setLinkId(resource.getId());
			tag.setLinkType("SHARE");
			commonDao.deleteData(tag, DELETE_TAG_LINK_ALL_BY_LINKID);
			String firstTag = entity.getFirstTag();
			String secondTag = entity.getSecondTag();
			String[] firstTagArrOld = firstTag.split(",");
			String[] secondTagArrOld = secondTag.split(",");
			String[] firstTagArr = removeArrayEmptyTextBackNewArray(firstTagArrOld);
			String[] secondTagArr = removeArrayEmptyTextBackNewArray(secondTagArrOld);
			List<String> isSaveList = new ArrayList<String>();
			for (int i = 0; i < firstTagArr.length; i++) {
				// 分类
				TagLinkEntity first = new TagLinkEntity();
				first.setTagId(firstTagArr[i]);
				first.setLinkType("SHARE");
				first.setLinkId(resource.getId());
				System.out.println(firstTagArr[i] + "," + resource.getId());
				if (!isSaveList.contains(firstTagArr[i] + resource.getId())) {
					commonDao.insertData(first, CourseDbConstants.TAG_INSERT);
					isSaveList.add(firstTagArr[i] + resource.getId());
				}
				if (StringUtil.isNotEmpty(entity.getSecondTag())) {
					TagLinkEntity second = new TagLinkEntity();
					second.setTagId(secondTagArr[i]);
					second.setLinkId(resource.getId());
					second.setLinkType("SHARE");
					second.setParentTagId(firstTagArr[i]);
					System.out.println(secondTagArr[i] + "," + resource.getId() + "," + firstTagArr[i]);
					commonDao.insertData(second, CourseDbConstants.TAG_INSERT);
				}
			}
			return result.setMessages(getMessage("上传成功"));
		}else {
			if (resultData != null && StringUtil.isNotEmpty(resultData.getFileId()) ) {
				entity.setFileId(resultData.getFileId());
				entity.setDirectoryId("10000");
				entity.setFlagShare("1");
				entity.setStateShare("1");
				entity.setId(IDGenerator.genUID());
				entity.setTimeShare(new Date());
				entity.setUserId(WebContext.getSessionUserId());
				commonDao.insertData(entity, SQL_INSERT_RESOURCE);
				String firstTag = entity.getFirstTag();
				String secondTag = entity.getSecondTag();
				String[] firstTagArrOld = firstTag.split(",");
				String[] secondTagArrOld = secondTag.split(",");
				String[] firstTagArr = removeArrayEmptyTextBackNewArray(firstTagArrOld);
				String[] secondTagArr = removeArrayEmptyTextBackNewArray(secondTagArrOld);
				List<String> isSaveList = new ArrayList<String>();
				for (int i = 0; i < firstTagArr.length; i++) {
					// 分类
					TagLinkEntity first = new TagLinkEntity();
					first.setTagId(firstTagArr[i]);
					first.setLinkType("SHARE");
					first.setLinkId(entity.getId());
					System.out.println(firstTagArr[i] + "," + entity.getId());
					if (!isSaveList.contains(firstTagArr[i] + entity.getId())) {
						commonDao.insertData(first, CourseDbConstants.TAG_INSERT);
						isSaveList.add(firstTagArr[i] + entity.getId());
					}
					if (StringUtil.isNotEmpty(entity.getSecondTag())) {
						TagLinkEntity second = new TagLinkEntity();
						second.setTagId(secondTagArr[i]);
						second.setLinkId(entity.getId());
						second.setLinkType("SHARE");
						second.setParentTagId(firstTagArr[i]);
						System.out.println(secondTagArr[i] + "," + entity.getId() + "," + firstTagArr[i]);
						commonDao.insertData(second, CourseDbConstants.TAG_INSERT);
					}
				}
				return result.setMessages(getMessage("上传成功"));
			} else {
				return result.setStatus(false).setMessages("上传失败");
			}
		}
	}

	/**
	 * 根据resourceId获取Resource信息
	 * 
	 * @param resourceId
	 * @return ResultBean
	 */
	public ResultBean getResourceDetailById(String resourceId) {
		ResultBean result = new ResultBean();
		assertNotEmpty(resourceId, MSG_E_EMPTY_RESOURCE_ID);
		ResourceView view = new ResourceView();
		view.setId(resourceId);
		assertExist(view, SQL_IS_RESOURCE_EXIST, MSG_E_EMPTY_RESOURCE);
		view.setUserId(WebContext.getSessionUserId());
		view = commonDao.searchOneData(view, SEARCH_RESOURCE_DETAIL_BY_ID);
		view.setUserName(commonDao.searchOneData(view.getUserId(), SEARCH_USERNAME_BY_ID).toString());
		Date shareTime = view.getTimeShare();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		view.setCreatetime(sdf.format(shareTime));
		result.setStatus(true);
		result.setData(view);
		return result;
	}

	public ResourceView searchCourse4EditById(String id, String type) {
		ResourceView query = new ResourceView();
		query.setId(id);
		ResourceView result = commonDao.searchOneData(query, SEARCH_RESOURCE_DETAIL_BY_ID_TWO);
		// 标签信息
		String firstTagId = "";
		String firstTagName = "";
		String secondTagId = "";
		String secondTagName = "";
		TagView tag = new TagView();
		tag.setLinkId(id);
		tag.setLinkType(LINK_TYPE_SHARE);
		List<TagView> tagList = commonDao.searchList(tag, CourseDbConstants.SQL_SEARCH_TAG_BY_COURSE_ID);
		if (tagList != null && tagList.size() > 0) {
			for (int i = 0; i < tagList.size(); i++) {
				if (tagList.get(i).getParentTagId() == null) {
					firstTagId += tagList.get(i).getTagId() + ",";
					firstTagName += tagList.get(i).getTagName() + ",";
				} else {
					secondTagId += tagList.get(i).getTagId() + ",";
					secondTagName += tagList.get(i).getTagName() + ",";
				}
			}
		}
		result.setFirstTag(firstTagId);
		result.setFirstTagName(firstTagName);
		result.setSecondTag(secondTagId);
		result.setSecondTagName(secondTagName);
		return result;
	}

	/**
	 * 去除数组空元素
	 * 
	 * @param array
	 * @return String[]
	 */
	private String[] removeArrayEmptyTextBackNewArray(String[] array) {
		List<String> strList = Arrays.asList(array);
		List<String> strListNew = new ArrayList<String>();
		for (int i = 0; i < strList.size(); i++) {
			if (strList.get(i) != null && !strList.get(i).equals("")) {
				strListNew.add(strList.get(i));
			}
		}
		String[] strNewArray = strListNew.toArray(new String[strListNew.size()]);
		return strNewArray;
	}
}
