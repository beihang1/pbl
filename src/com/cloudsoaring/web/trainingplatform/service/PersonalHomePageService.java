/**
* Filename : PersonalHomePageService.java
* Author : TongLei
* Creation time : 下午3:48:16 - 2015年11月5日
* Description :
*/

package com.cloudsoaring.web.trainingplatform.service;

import org.springframework.stereotype.Service;

import com.cloudsoaring.web.bus.entity.UserEntity;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.entity.UserCourseEntity;
import com.cloudsoaring.web.trainingplatform.constants.TpDbConstants;
import com.cloudsoaring.web.trainingplatform.constants.TpStateConstants;
/**
 * 个人中心
 * @author TongLei
 *
 */
@Service
public class PersonalHomePageService extends FileService implements TpDbConstants,TpStateConstants{
	
	/**
	 * 个人中心首页检索用户信息
	 * @param user
	 * @return 返回结果
	 */
	public UserEntity searchPersonalUser(UserEntity user){
		user = commonDao.searchOneData(user, PERSONAL_HOME_PAGE_SEARCH);
		return user;
	}
	/**
	 * 参与的课程
	 * @param userCourse
	 * @return 返回结果
	 */
	public ResultBean searchAttendCourse(UserCourseEntity userCourse){
		userCourse.setJoinCourse(ALREADY_JOIN);
		return commonDao.searchList4Page(userCourse, ATTEND_COURSE_SEARCH);
	}
	/**
	 * 检索收藏的课程
	 * @param userCourse
	 * @return 返回结果
	 */
	public ResultBean searchCollectCourse(UserCourseEntity userCourse){
		userCourse.setFavorite(ALREADY_SAVE);
		return commonDao.searchList4Page(userCourse, COLLECT_COURSE_SEARCH);
	}
}
