package com.cloudsoaring.web.trainingplatform.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.cloudsoaring.web.common.service.BaseService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.view.TagView;
import com.cloudsoaring.web.trainingplatform.constants.TpDbConstants;

import com.cloudsoaring.web.trainingplatform.entity.InfomationEntity;

/**
 * 平台信息，信息分类
 * @author LIUYANSHUANG
 * @date 2015-11-07
 * @version V1.0
 *
 */
@Service
public class IndexInfoService extends BaseService implements TpDbConstants{

	/***
	 * 检索平台信息
	 * @param info
	 * @return 返回结果
	 */
	@SuppressWarnings("unchecked")
	public ResultBean getHomeInfoList(InfomationEntity info){
		ResultBean result = new ResultBean();
		List<InfomationEntity> list = commonDao.searchList(info, SEARCH_INFOMATION);
		result.setStatus(true);
		result.setData(list);
		return result;
	}
	
	/**
	 * 检索分类信息
	 * @param infoCategory
	 * @return 返回结果
	 */
	@SuppressWarnings("unchecked")
	public ResultBean searchCategory(TagView view){
		ResultBean result = new ResultBean();
		List<TagView> list = commonDao.searchList(view, SEARCH_NOTICE_CATEGORY);
		result.setStatus(true);
		result.setData(list);
		return result;
	}
	
	/***
	 * 根据分类ID检索平台信息
	 */
	public ResultBean searchInfoByCategoryId(InfomationEntity info){
		return commonDao.searchList4Page(info, SEARCH_INFOMATION);
	}
	
	/***
	 * 根据ID检索平台信息
	 * @param info
	 * @return 返回结果
	 */
	public InfomationEntity searchInfoById(InfomationEntity info){
		return commonDao.searchOneData(info, SEARCH_INFOMATION_BY_ID);
	}
}
