package com.cloudsoaring.web.trainingplatform;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cloudsoaring.web.AbstractWebUserEventHandler;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.UserEntity;
import com.cloudsoaring.web.course.constant.CourseDbConstants;
import com.cloudsoaring.web.course.constant.CourseStateConstants;
import com.cloudsoaring.web.pointmanager.constant.PointStateConstants;
import com.cloudsoaring.web.pointmanager.service.PointManagerService;
import com.cloudsoaring.web.pointmanager.view.PointTypeView;
import com.cloudsoaring.web.points.entity.PointTypeEntity;
import com.cloudsoaring.web.points.entity.UserPoint;
import com.cloudsoaring.web.points.service.PointService;
import com.cloudsoaring.web.trainingplatform.constants.TpDefaultValues;
import com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity;
import com.cloudsoaring.web.trainingplatform.service.UserManagementService;


@Component
public class TpWebUserEventHandler implements AbstractWebUserEventHandler{

	@Autowired
	private PointService pointService;
	@Autowired
	private UserManagementService userManagementService;
	@Autowired
	private PointManagerService pointManagerService;
	
	@Override
	public void login(UserEntity user, HttpServletRequest request,
			HttpServletResponse response) {
		UserPoint point = pointService.fetchPoint(user.getUserId());
		WebContext.session("POINTS", point.getPoints());
		WebContext.session("TOTAL_POINTS", point.getTotalPoints());
		
		WebContext.session("IS_TEACHER", userManagementService.isTeacher(user.getUserId()));
		
		//获取积分来源
		PointTypeEntity typeQuery = new PointTypeEntity();
		typeQuery.setSystemCode(PointStateConstants.POINT_SYSTEM_HS);
		List<PointTypeEntity> pointTypeList = pointManagerService.search(typeQuery);
		Map<String, PointTypeEntity> pointTypeMap = new HashMap<String, PointTypeEntity>();
		for(PointTypeEntity one : pointTypeList) {
			WebContext.session(one.getTypeCode(), one);
		}
		
		
	}

}
