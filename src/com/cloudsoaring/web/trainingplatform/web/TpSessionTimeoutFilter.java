/**
 * 
 */
package com.cloudsoaring.web.trainingplatform.web;

import java.net.URLEncoder;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.http.HttpMethod;

import com.cloudsoaring.common.utils.DateUtil;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.SessionTimeoutFilter;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.UserEntity;
import com.cloudsoaring.web.common.utils.ConfigUtil;
import com.cloudsoaring.web.common.utils.HttpClientUtil;
import com.cloudsoaring.web.common.utils.JSONUtil;
import com.cloudsoaring.web.common.utils.SpringUtil;
import com.cloudsoaring.web.trainingplatform.constants.TpConstants;
import com.cloudsoaring.web.trainingplatform.constants.TpStateConstants;
import com.cloudsoaring.web.trainingplatform.entity.TpUserEntity;
import com.cloudsoaring.web.trainingplatform.service.UserManagementService;

/**
 *Copyright (c),2015-2016, Cloudsoaring
 *<br/>This program is protected by copyright laws; 
 *<br/>Program Name: <b>CSWEB<b>
 *<br/>
 *<br/> 教学平台的身份校验过滤器，如果配置外部系统授权码地址，则将登录地址重定向至外部授权地址 
 *<br/>2016年1月7日 上午10:04:40
 *@author adlay
 *@since JDK 1.6
 *@version 1.0
 */
public class TpSessionTimeoutFilter extends SessionTimeoutFilter implements TpConstants {

	public String loginUrl(HttpServletRequest httpreq, boolean withRedirect){
		//配置了PC端单点登录地址
		String ssoUrl = ConfigUtil.getProperty(TpConstants.CONFIG_GA_WEB_ACCESS_CODE_URL);
		//判断是否配置了登录地址
		String loginUrl = ConfigUtil.getProperty(CONFIG_LOGIN_OTHER_URL);
    	if(StringUtil.isNotEmpty(ssoUrl)){
    		return "redirect_sso.action?pcweb=true";
    	}else if(StringUtil.isNotEmpty(loginUrl)){
    		loginUrl = loginUrl.replaceAll("\\{redirect\\}", URLEncoder.encode(WebContext.getLastRequestUrl(httpreq)));
    		return loginUrl;
    	}else{
    		return super.loginUrl(httpreq, withRedirect);
    	}
    }
	
	public boolean tryLoginOtherWay(HttpServletRequest httpreq, HttpServletResponse httpres, FilterChain chain){
		validUserInfo(httpreq);
		return true;
	}
	
	public static boolean validUserInfo(HttpServletRequest httpreq){
		//判断 是否配置了获取用户信息的URL
		String infoUrl = ConfigUtil.getProperty(CONFIG_LGOIN_OTHER_USER_INFO);
		if(StringUtil.isNotEmpty(infoUrl)){
			String accessToken = httpreq.getParameter("access_token");
			if(StringUtil.isEmpty(accessToken)){
				String qa = httpreq.getQueryString();
				Map<String, String> params = StringUtil.getQueryMapByQueryString(qa);
				accessToken = params.get("access_token");
			}
			if(StringUtil.isNotEmpty(accessToken)){
				accessToken = accessToken.split("\\?")[0];
				String sessionToken = (String)WebContext.session("access_token");
				//该用户已登录
				if((StringUtil.isNotEmpty(sessionToken) && sessionToken.equals(accessToken))){
					return true;
				}
				int connectionTimeout = ConfigUtil.getInteger(
						CONFIG_GA_CONNECT_TIMEOUT, 6000);
				int requestTimeout = ConfigUtil.getInteger(CONFIG_GA_REQUEST_TIMEOUT,
						6000);
				JSONObject result;
				try {
					infoUrl = infoUrl.replaceAll("\\{access_token\\}", accessToken);
					result = HttpClientUtil.requestJson(HttpMethod.POST, infoUrl, "UTF-8", connectionTimeout, requestTimeout);
					boolean failed = "1".equals(JSONUtil.getNotEmptyValue(result, "code"));
					boolean userInfoOk = false;
					if (!failed){
						JSONObject user = JSONUtil.getJsonObject(result, "msg");
						if(user != null){
							TpUserEntity entity = new TpUserEntity();
							entity.userId = JSONUtil.getNotEmptyValue(user, "userid");
							entity.setUserKbn(TpStateConstants.USER_KBN_C);
							// 成功
							if (StringUtil
									.isNotEmpty(entity.userId)) {
								entity.userName = JSONUtil.getNotEmptyValue(user, "mobile");
								entity.personName = JSONUtil.getNotEmptyValue(user, "name");
								entity.email = JSONUtil.getNotEmptyValue(user, "email");
								entity.mobile = JSONUtil.getNotEmptyValue(user, "mobile");
								entity.setWeibo(JSONUtil.getNotEmptyValue(user, "weibo"));
								entity.setWeixin(JSONUtil.getNotEmptyValue(user, "weixin"));
								//注册日期
								entity.setCreateDate(DateUtil.parse(JSONUtil.getNotEmptyValue(user, "regtime")));
								// 手机端头像?
								String thumbnailUrl = JSONUtil.getNotEmptyValue(user, "pictureMp");
								String imageUrl = JSONUtil.getNotEmptyValue(user, "picture");
								// 更新或插入用户信息
								UserManagementService userManagementService = (UserManagementService)SpringUtil.getBean("userManagementService");
								UserEntity loginUser = userManagementService.storageSSOUserEntity(
												entity,
												imageUrl,
												thumbnailUrl,
												connectionTimeout,
												requestTimeout);
								// 用户登录成功
								WebContext.loginUser(loginUser, false);
								WebContext.session("access_token", accessToken);
								userInfoOk = true;
							}
						}
					}
					if(!userInfoOk){
						WebContext.logout();
						return false;
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}
    	return true;
    }
}