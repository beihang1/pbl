/**
 * 
 */
package com.cloudsoaring.web.trainingplatform.entity;


/**
 *Copyright (c),2015-2016, Cloudsoaring
 *<br/>This program is protected by copyright laws; 
 *<br/>Program Name: <b>CSWEB<b>
 *<br/>
 *<br/>(用一句话描述该类的功能，替换括号及括号内的文字)
 *<br/>2015年11月12日 上午9:25:23
 *@author adlay
 *@since JDK 1.6
 *@version 1.0
 */
public class TpUserEntity extends com.cloudsoaring.web.bus.entity.UserEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String roleId;
	private String roleName;
	private String roleTeacherId;
	private String roleAdministratorId;
	//租户id
	private String tenantId;
	//外部接口调用返回的头像
	private String headurl;
    //所属省份
	private String proName;
	//所属市区
	private String cityName;
	//所属城镇
	private String townName;
	//所属学校主键
	private String schoolId;
	//所属学校名称
	private String schoolName;
	//所属学校类型
	private String schoolType;
	//所属课程主键
	private String subjectId;
	//所属课程名称
	private String subjectName;
	//教师职责
	private String duty;
	//是否是管理员
	private String isFlag;
	//是否学校管理员
	private String schoolManager;
	public String getRoleTeacherId() {
		return roleTeacherId;
	}
	public void setRoleTeacherId(String roleTeacherId) {
		this.roleTeacherId = roleTeacherId;
	}
	public String getRoleAdministratorId() {
		return roleAdministratorId;
	}
	public void setRoleAdministratorId(String roleAdministratorId) {
		this.roleAdministratorId = roleAdministratorId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getHeadurl() {
		return headurl;
	}
	public void setHeadurl(String headurl) {
		this.headurl = headurl;
	}
	public String getProName() {
		return proName;
	}
	public void setProName(String proName) {
		this.proName = proName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getTownName() {
		return townName;
	}
	public void setTownName(String townName) {
		this.townName = townName;
	}
	public String getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public String getSchoolType() {
		return schoolType;
	}
	public void setSchoolType(String schoolType) {
		this.schoolType = schoolType;
	}
	public String getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(String subjectId) {
		this.subjectId = subjectId;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public String getDuty() {
		return duty;
	}
	public void setDuty(String duty) {
		this.duty = duty;
	}
	public String getIsFlag() {
		return isFlag;
	}
	public void setIsFlag(String isFlag) {
		this.isFlag = isFlag;
	}
	public String getSchoolManager() {
		return schoolManager;
	}
	public void setSchoolManager(String schoolManager) {
		this.schoolManager = schoolManager;
	}
	
	
}
