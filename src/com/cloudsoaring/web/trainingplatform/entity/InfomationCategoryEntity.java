package com.cloudsoaring.web.trainingplatform.entity;


import java.math.BigDecimal;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_infomation_category
 * @date 2015-11-05
 * @version V1.0
 *
 */
public class InfomationCategoryEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**描述*/
	private String title;
	/**图片ID*/
	private String pictureId;
	/**首页推荐(0:不推荐,1:推荐)*/
	private BigDecimal homePush;
	
	//--非数据库字段--
	private String oldPictureId;
	private String fileName;
	
	
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getOldPictureId() {
		return oldPictureId;
	}
	public void setOldPictureId(String oldPictureId) {
		this.oldPictureId = oldPictureId;
	}
	public String getTitle(){
		return this.title;
	}
	public void setTitle(String title){
		this.title = title;
	}
	public String getPictureId(){
		return this.pictureId;
	}
	public void setPictureId(String pictureId){
		this.pictureId = pictureId;
	}
	public BigDecimal getHomePush(){
		return this.homePush;
	}
	public void setHomePush(BigDecimal homePush){
		this.homePush = homePush;
	}
}