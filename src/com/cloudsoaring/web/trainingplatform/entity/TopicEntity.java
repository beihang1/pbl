

package com.cloudsoaring.web.trainingplatform.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_topic
 * @date 2016-08-09
 * @version V1.0
 *
 */
public class TopicEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**导师ID*/
	private String teacherId;
	/**话题内容*/
	private String topicContent;
	
	/**导师ID*/
	public String getTeacherId(){
		return this.teacherId;
	}
	/**导师ID*/
	public void setTeacherId(String teacherId){
		this.teacherId = teacherId;
	}
	/**话题内容*/
	public String getTopicContent(){
		return this.topicContent;
	}
	/**话题内容*/
	public void setTopicContent(String topicContent){
		this.topicContent = topicContent;
	}
}
