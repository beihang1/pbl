

package com.cloudsoaring.web.trainingplatform.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_props_his
 * @date 2016-07-08
 * @version V1.0
 *
 */
public class PropsHisEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**赠送人ID*/
	private String gaveUserId;
	private String gaveUserName;
	/**道具ID*/
	private String propsId;
	/**道具名称*/
	private String propsName;
	/**道具图标*/
	private String propsIcon;
	/**道具图标名称*/
    private String propsIconName;
	/**道具价格（积分）*/
	private java.math.BigDecimal propsPrice;
	/**道具好感度*/
	private java.math.BigDecimal propsFavour;
	/**获赠人ID（导师）*/
	private String getUserId;
	private String getUserName;
	/***/
	private java.util.Date timeGave;
	/**赠送数量*/
	private java.math.BigDecimal gaveNum;
	/**是否显示用户赠送道具时填写的信息（0:显示，1:不显示）*/
	private String flagShowMsg;
	/**好感度之和*/
	private java.math.BigDecimal propsFavourTotal;
	/**给被赠送人的留言*/
	private String leaveMessage;
	
	public String getLeaveMessage() {
        return leaveMessage;
    }
    public void setLeaveMessage(String leaveMessage) {
        this.leaveMessage = leaveMessage;
    }
    public java.math.BigDecimal getPropsFavourTotal() {
        return propsFavourTotal;
    }
    public void setPropsFavourTotal(java.math.BigDecimal propsFavourTotal) {
        this.propsFavourTotal = propsFavourTotal;
    }
    public java.math.BigDecimal getGaveNum() {
        return gaveNum;
    }
    public void setGaveNum(java.math.BigDecimal gaveNum) {
        this.gaveNum = gaveNum;
    }
    public String getFlagShowMsg() {
        return flagShowMsg;
    }
    public void setFlagShowMsg(String flagShowMsg) {
        this.flagShowMsg = flagShowMsg;
    }
    public String getGaveUserName() {
        return gaveUserName;
    }
    public void setGaveUserName(String gaveUserName) {
        this.gaveUserName = gaveUserName;
    }
    public String getGetUserName() {
        return getUserName;
    }
    public void setGetUserName(String getUserName) {
        this.getUserName = getUserName;
    }
    public String getPropsIconName() {
        return propsIconName;
    }
    public void setPropsIconName(String propsIconName) {
        this.propsIconName = propsIconName;
    }
    /**赠送人ID*/
	public String getGaveUserId(){
		return this.gaveUserId;
	}
	/**赠送人ID*/
	public void setGaveUserId(String gaveUserId){
		this.gaveUserId = gaveUserId;
	}
	/**道具ID*/
	public String getPropsId(){
		return this.propsId;
	}
	/**道具ID*/
	public void setPropsId(String propsId){
		this.propsId = propsId;
	}
	/**道具名称*/
	public String getPropsName(){
		return this.propsName;
	}
	/**道具名称*/
	public void setPropsName(String propsName){
		this.propsName = propsName;
	}
	/**道具图标*/
	public String getPropsIcon(){
		return this.propsIcon;
	}
	/**道具图标*/
	public void setPropsIcon(String propsIcon){
		this.propsIcon = propsIcon;
	}
	/**道具价格（积分）*/
	public java.math.BigDecimal getPropsPrice(){
		return this.propsPrice;
	}
	/**道具价格（积分）*/
	public void setPropsPrice(java.math.BigDecimal propsPrice){
		this.propsPrice = propsPrice;
	}
	/**道具好感度*/
	public java.math.BigDecimal getPropsFavour(){
		return this.propsFavour;
	}
	/**道具好感度*/
	public void setPropsFavour(java.math.BigDecimal propsFavour){
		this.propsFavour = propsFavour;
	}
	/**获赠人ID（导师）*/
	public String getGetUserId(){
		return this.getUserId;
	}
	/**获赠人ID（导师）*/
	public void setGetUserId(String getUserId){
		this.getUserId = getUserId;
	}
	/***/
	public java.util.Date getTimeGave(){
		return this.timeGave;
	}
	/***/
	public void setTimeGave(java.util.Date timeGave){
		this.timeGave = timeGave;
	}
}
