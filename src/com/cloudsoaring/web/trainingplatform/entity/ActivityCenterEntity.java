package com.cloudsoaring.web.trainingplatform.entity;


import java.math.BigDecimal;
import java.util.Date;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_infomation
 * @date 2015-11-05
 * @version V1.0
 *
 */
public class ActivityCenterEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**标题*/
	private String title;
	/**内容*/
	private String content;
	/**图片*/
	private String pictureId;
	/**分类ID*/
	private String categoryId;
	/**首页推荐*/
	private BigDecimal recomment;
	/**发布状态（1：已发布，0：未发布）*/
	private BigDecimal status;
	//---非数据库字段----
	private String fileName;
	
	private String oldPictureId;
	private Date fromDateBegin;
	private Date fromDateEnd;
	private String isSign;
	private String personName;
	private int count;
	private String activeStatus;
	//发布范围
    private String fanwei;
    //学校名称
    private String schoolName;
    private String startTime;
    private String endTime;
	
	public Date getFromDateBegin() {
		return fromDateBegin;
	}
	public void setFromDateBegin(Date fromDateBegin) {
		this.fromDateBegin = fromDateBegin;
	}
	public Date getFromDateEnd() {
		return fromDateEnd;
	}
	public void setFromDateEnd(Date fromDateEnd) {
		this.fromDateEnd = fromDateEnd;
	}
	public String getOldPictureId() {
		return oldPictureId;
	}
	public void setOldPictureId(String oldPictureId) {
		this.oldPictureId = oldPictureId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public BigDecimal getStatus() {
		return status;
	}
	public void setStatus(BigDecimal status) {
		this.status = status;
	}
	public String getTitle(){
		return this.title;
	}
	public void setTitle(String title){
		this.title = title;
	}
	public String getContent(){
		return this.content;
	}
	public void setContent(String content){
		this.content = content;
	}
	public String getPictureId(){
		return this.pictureId;
	}
	public void setPictureId(String pictureId){
		this.pictureId = pictureId;
	}
	public String getCategoryId(){
		return this.categoryId;
	}
	public void setCategoryId(String categoryId){
		this.categoryId = categoryId;
	}
	public BigDecimal getRecomment(){
		return this.recomment;
	}
	public void setRecomment(BigDecimal recomment){
		this.recomment = recomment;
	}
	public String getIsSign() {
		return isSign;
	}
	public void setIsSign(String isSign) {
		this.isSign = isSign;
	}
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getActiveStatus() {
		return activeStatus;
	}
	public void setActiveStatus(String activeStatus) {
		this.activeStatus = activeStatus;
	}
	public String getFanwei() {
		return fanwei;
	}
	public void setFanwei(String fanwei) {
		this.fanwei = fanwei;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
}