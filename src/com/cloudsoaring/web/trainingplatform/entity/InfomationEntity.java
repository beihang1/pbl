package com.cloudsoaring.web.trainingplatform.entity;


import java.math.BigDecimal;
import java.util.Date;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_infomation
 * @date 2015-11-05
 * @version V1.0
 *
 */
public class InfomationEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**标题*/
	private String title;
	/**内容*/
	private String content;
	/**图片*/
	private String pictureId;
	/**分类ID*/
	private String categoryId;
	/**首页推荐*/
	private BigDecimal recomment;
	/**发布状态（1：已发布，0：未发布）*/
	private BigDecimal status;
	//---非数据库字段----
	private String fileName;
	
	private String oldPictureId;
	private Date fromDateBegin;
	private Date fromDateEnd;
	
	
	
	public Date getFromDateBegin() {
		return fromDateBegin;
	}
	public void setFromDateBegin(Date fromDateBegin) {
		this.fromDateBegin = fromDateBegin;
	}
	public Date getFromDateEnd() {
		return fromDateEnd;
	}
	public void setFromDateEnd(Date fromDateEnd) {
		this.fromDateEnd = fromDateEnd;
	}
	public String getOldPictureId() {
		return oldPictureId;
	}
	public void setOldPictureId(String oldPictureId) {
		this.oldPictureId = oldPictureId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public BigDecimal getStatus() {
		return status;
	}
	public void setStatus(BigDecimal status) {
		this.status = status;
	}
	public String getTitle(){
		return this.title;
	}
	public void setTitle(String title){
		this.title = title;
	}
	public String getContent(){
		return this.content;
	}
	public void setContent(String content){
		this.content = content;
	}
	public String getPictureId(){
		return this.pictureId;
	}
	public void setPictureId(String pictureId){
		this.pictureId = pictureId;
	}
	public String getCategoryId(){
		return this.categoryId;
	}
	public void setCategoryId(String categoryId){
		this.categoryId = categoryId;
	}
	public BigDecimal getRecomment(){
		return this.recomment;
	}
	public void setRecomment(BigDecimal recomment){
		this.recomment = recomment;
	}
}