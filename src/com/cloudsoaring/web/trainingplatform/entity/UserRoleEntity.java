package com.cloudsoaring.web.trainingplatform.entity;

import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: TB_USER_ROLE-用户角色关联表
 * @date 2015-05-20
 * @version V1.0
 *
 */
public class UserRoleEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**用户ID */
	private String userId;
	/**角色ID */
	private String roleId;
	/**角色名 */
	private String roleName;
	
	public String getUserId(){
		return this.userId;
	}
	public void setUserId(String userId){
		this.userId = userId;
	}
	public String getRoleId(){
		return this.roleId;
	}
	public void setRoleId(String roleId){
		this.roleId = roleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
}