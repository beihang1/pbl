package com.cloudsoaring.web.trainingplatform.entity;


import java.util.Date;
import com.cloudsoaring.web.common.entity.BaseEntity;

public class ActivityCenterUserEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private String activityCenterId;
	private String userId;
	private Date createDate;
	
	public String getActivityCenterId() {
		return activityCenterId;
	}
	public void setActivityCenterId(String activityCenterId) {
		this.activityCenterId = activityCenterId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}