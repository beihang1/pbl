

package com.cloudsoaring.web.trainingplatform.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_user_active
 * @date 2016-07-01
 * @version V1.0
 *
 */
public class UserActiveEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**活动ID*/
	private String activeId;
	/**用户ID*/
	private String userId;
	/**用户名*/
    private String userName;
    /**真是姓名*/
    private String personName;
	/**报名时间*/
	private java.util.Date timeEntry;
	private java.sql.Date timeEntryUser;
	/**签到时间*/
	private java.util.Date timeSign;
	private java.sql.Date timeSignUser;
	/**备注*/
	private String remark;
	/**活动状态（未发布：1，报名中：2，进行中：3，已结束：4，已取消：5）*/
    private String status;
    /**签到或报名标识：0签到，1报名*/
    private String flag;
    
	public java.sql.Date getTimeEntryUser() {
        return timeEntryUser;
    }
    public void setTimeEntryUser(java.sql.Date timeEntryUser) {
        this.timeEntryUser = timeEntryUser;
    }
    public java.sql.Date getTimeSignUser() {
        return timeSignUser;
    }
    public void setTimeSignUser(java.sql.Date timeSignUser) {
        this.timeSignUser = timeSignUser;
    }
    public String getFlag() {
        return flag;
    }
    public void setFlag(String flag) {
        this.flag = flag;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getPersonName() {
        return personName;
    }
    public void setPersonName(String personName) {
        this.personName = personName;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    /**活动ID*/
	public String getActiveId(){
		return this.activeId;
	}
	/**活动ID*/
	public void setActiveId(String activeId){
		this.activeId = activeId;
	}
	/**用户ID*/
	public String getUserId(){
		return this.userId;
	}
	/**用户ID*/
	public void setUserId(String userId){
		this.userId = userId;
	}
	/**报名时间*/
	public java.util.Date getTimeEntry(){
		return this.timeEntry;
	}
	/**报名时间*/
	public void setTimeEntry(java.util.Date timeEntry){
		this.timeEntry = timeEntry;
	}
	/**签到时间*/
	public java.util.Date getTimeSign(){
		return this.timeSign;
	}
	/**签到时间*/
	public void setTimeSign(java.util.Date timeSign){
		this.timeSign = timeSign;
	}
	/**备注*/
	public String getRemark(){
		return this.remark;
	}
	/**备注*/
	public void setRemark(String remark){
		this.remark = remark;
	}
}
