

package com.cloudsoaring.web.trainingplatform.entity;


import com.cloudsoaring.web.common.entity.BaseEntity;

/**   
 * @Title: Entity
 * @Description: tb_props
 * @date 2016-07-08
 * @version V1.0
 *
 */
public class PropsEntity extends BaseEntity{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**道具名称*/
	private String propsName;
	/**道具图标*/
	private String propsIcon;
	/**道具图标名称*/
    private String propsIconName;
	/**价格（积分）*/
	private java.math.BigDecimal price;
	/**好感度*/
	private java.math.BigDecimal numFavour;
	/**赠送道具数量*/
    private String num;
	
	public String getNum() {
        return num;
    }
    public void setNum(String num) {
        this.num = num;
    }
    public String getPropsIconName() {
        return propsIconName;
    }
    public void setPropsIconName(String propsIconName) {
        this.propsIconName = propsIconName;
    }
    /**道具名称*/
	public String getPropsName(){
		return this.propsName;
	}
	/**道具名称*/
	public void setPropsName(String propsName){
		this.propsName = propsName;
	}
	/**道具图标*/
	public String getPropsIcon(){
		return this.propsIcon;
	}
	/**道具图标*/
	public void setPropsIcon(String propsIcon){
		this.propsIcon = propsIcon;
	}
	/**价格（积分）*/
	public java.math.BigDecimal getPrice(){
		return this.price;
	}
	/**价格（积分）*/
	public void setPrice(java.math.BigDecimal price){
		this.price = price;
	}
	/**好感度*/
	public java.math.BigDecimal getNumFavour(){
		return this.numFavour;
	}
	/**好感度*/
	public void setNumFavour(java.math.BigDecimal numFavour){
		this.numFavour = numFavour;
	}
}
