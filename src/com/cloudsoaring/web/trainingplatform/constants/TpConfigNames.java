package com.cloudsoaring.web.trainingplatform.constants;

public interface TpConfigNames {
	//APP 版本配置
	public static final String CONFIG_APP_CONFIG_APK ="APP_CONFIG_APK";
	public static final String CONFIG_APP_CONFIG_DESC ="APP_CONFIG_DESC";
	public static final String CONFIG_APP_CONFIG_VERSION ="APP_CONFIG_VERSION";
	public final static String CONFIG_SHARE_ON_OFF = "SHARE_ON_OFF";
	public final static String CONFIG_INFORMATION_ON_OFF = "INFORMATION_ON_OFF";
	
	public final static String CONFIG_GA_ACCESS_CODE_URL = "GA_ACCESS_CODE_URL";
	public final static String CONFIG_GA_WEB_ACCESS_CODE_URL = "GA_WEB_ACCESS_CODE_URL";
	public final static String CONFIG_GA_ACCESS_TOKEN_URL = "GA_ACCESS_TOKEN_URL";
	public final static String CONFIG_GA_USER_INFO_URL = "GA_USER_INFO_URL";
	//获取授权回调URL
	public final static String CONFIG_GA_SSO_COMPLETE_URL = "GA_SSO_COMPLETE_URL";
	//手机端默认首页地址
	public final static String CONFIG_GA_MOBILE_HOME = "GA_MOBILE_HOME";
	//HTTP连接超时
	public final static String CONFIG_GA_CONNECT_TIMEOUT = "GA_CONNECT_TIMEOUT";
	//HTTP请求超时
	public final static String CONFIG_GA_REQUEST_TIMEOUT = "GA_REQUEST_TIMEOUT";
	//用户WebService地址
	public final static String CONFIG_GA_WEB_SERVICE_USER = "GA_WEB_SERVICE_USER";
	//用户webservice用户名
	public final static String CONFIG_GA_WEB_SERVICE_USERNAME = "GA_WEB_SERVICE_USERNAME";
	//用户webservice密码
	public final static String CONFIG_GA_WEB_SERVICE_PASSWORD = "GA_WEB_SERVICE_PASSWORD";
	
	//价格显示模式
	public final static String CONFIG_PRICE_DISPLAY_MODE = "PRICE_DISPLAY_MODE";
	//单位零钱换算的积分
	public final static String CONFIG_POINTS_FOR_SMALL_CHANGE = "POINTS_FOR_SMALL_CHANGE";
	//支持的支付方式
	public final static String CONFIG_SUPPORTED_PAY_MODES = "SUPPORTED_PAY_MODES";
	 /**积分折扣（元/分*/
    public static final String CONFIG_POINT_DISCOUNT="POINT_DISCOUNT";
    
    /**日期格式：yyyy/MM/dd*/
    public static final String CONFIG_DATE_FORMAT_DEFAULT_KEY="DATE_FORMAT_DEFAULT";
    /**日期格式：yyyy/MM/dd HH:mm:ss*/
    public static final String CONFIG_DATE_TIME_FORMAT_DEFAULT_KEY="DATE_TIME_FORMAT_DEFAULT";
    /**日期格式：yyyy/MM/dd HH:mm*/
    public static final String CONFIG_DATE_MINUTE_FORMAT_DEFAULT_KEY="DATE_MINUTE_FORMAT_DEFAULT";
    /**日期格式：yyyy/MM/dd*/
    public static final String CONFIG_DATE_FORMAT_DEFAULT_VALUE="date.format.default";
    /**日期格式：yyyy/MM/dd HH:mm:ss*/
    public static final String CONFIG_DATE_TIME_FORMAT_DEFAULT_VALUE="datetime.format.default";
    /**日期格式：yyyy/MM/dd HH:mm*/
    public static final String CONFIG_DATE_MINUTE_FORMAT_DEFAULT_VALUE="dateminute.format.default";
    /**获取第三方登录URL**/
    public static final String CONFIG_LOGIN_OTHER_URL = "LOGIN_OTHER_URL";
    /**获取第三方用户信息URL**/
    public static final String CONFIG_LGOIN_OTHER_USER_INFO = "LGOIN_OTHER_USER_INFO";
    /**注销第三方登录**/
    public static final String CONFIG_LOGIN_OTHER_LOGOUT_URL = "LOGIN_OTHER_LOGOUT_URL";
    
}
