package com.cloudsoaring.web.trainingplatform.constants;

public interface TpConstants extends TpConfigNames, TpDbConstants, TpLogConstants, TpDefaultValues, TpMsgNames, TpStateConstants{
    
	//是否加入课程
	public static String NOT_ATTEND_COURSE = "0";
	public static String ATTEND_COURSE ="1";
	
	public static String SESSION_KEY_SSO_STATE_TOKEN = "SSO_STATE_TOKEN";
	
	public static String SESSION_KEY_SSO_REAL_CALLBACK = "SSO_REAL_CALLBACK";
	public static String SESSION_KEY_SSO_CALLBACK_POSITION = "SSO_CALLBACK_POSITION";
	
	//价格显示模式：积分
	public static String PRICE_DISPLAY_MODE_POINTS = "P";
	//价格显示模式:零钱
	public static String PRICE_DISPLAY_MODE_SMALL_CHANGE = "S";
	//支付方式:积分
	public static String PAY_MODE_POINTS = "P";
	//支付方式:零钱
	public static String PAY_MODE_SMALL_CHANGE = "S";
	
	//视频服务上传授权码
	public static String VIDEO_SERVICE_WRITE_TOKEN = "VIDEO_SERVICE_WRITE_TOKEN";
	//视频服务读取授权码
	public static String VIDEO_SERVICE_READ_TOKEN = "VIDEO_SERVICE_READ_TOKEN";
	//视频服务签名密钥
	public static String VIDEO_SERVICE_SECRETKEY = "VIDEO_SERVICE_SECRETKEY";
	//视频分类：课程
	public static String VIDEO_SERVICE_CATEGORY_COURSE = "VIDEO_SERVICE_CATEGORY_COURSE";
	/**活动标识：笔记*/
    public static final String FLAG_NOTE="1";
    /**活动标识：评论flagDiscuss*/
    public static final String FLAG_DISCUSS="2";
    /**活动标识：问答flagQa*/
    public static final String FLAG_QA="3";
    /**活动标识：投票flagVote*/
    public static final String FLAG_VOTE="4";
    /**活动标识：1开启 0关闭*/
    public static final String FLAG="1";
    public static final String FLAG_NOT="0";
}
