package com.cloudsoaring.web.trainingplatform.constants;

public interface TpDbConstants {



	/**用户登录-根据用户名检索*/
	public static final String USER_SEARCH = "User.searchUser";
	/**登录成功，更新数据*/
	public static final String TP_LOGIN_SUCCESS = "User.loginSuccess";
	/**检索平台信息*/
	public static final String SEARCH_INFOMATION = "Infomation.select";
	
	public static final String SELECT_ACTIVITY_CENTER = "ActivityCenter.select";
	
	public static final String SELECT_NEWS_CENTER = "NewsCenter.select";
	/**检索分类信息*/
	public static final String SEARCH_INFOMATION_CATEGORY = "InfomationCategory.select";
	/**插入用户课程*/
	public static final String INSERT_USER_COURSE = "UserCourse.insert";
	/**更新用户课程*/
	public static final String UPDATE_USER_COURSE = "UserCourse.update";
	/**检索公告分类*/
	public static final String SEARCH_NOTICE_CATEGORY = "TagView.searchTagByNotice";
	
	public static final String SEARCH_ACTIVITY_CENTER_CATEGORY = "TagView.searchTagByActivityCenter";
	
	public static final String SEARCH_NEWS_CENTER_CATEGORY = "TagView.searchTagByNewsCenter";
	
	/**检索公告信息*/
	public static final String SEARCH_NOTICE = "Infomation.selectNotice";
	
	public static final String SEARCH_ACTIVITY_CENTER = "ActivityCenter.selectNotice";
	
	public static final String SEARCH_NEWS_CENTER = "NewsCenter.selectNotice";
	
	public static final String ADD_ACTIVITY_CENTER = "ActivityCenter.insert";
	
	public static final String ADD_ACTIVITY_CENTER_USER = "ActivityCenterUser.insert";
	
	public static final String SELECT_ACTIVITY_CENTER_USER = "ActivityCenterUser.selectUser";
	
	public static final String ADD_NEWS_CENTER = "NewsCenter.insert";
	/**检索公告信息*/
	public static final String SEARCH_NOTICE_TYPE = "TagView.selectLinkTag";
	
	/**新增公告*/
	public static final String ADD_NOTICE = "Infomation.insert";
	/**新增公告*/
	public static final String ADD_NOTICE_TYPE = "TagView.insertTag";
	/**编辑公告*/
	public static final String EDIT_NOTICE_TYPE ="TagView.updateTag";
	
	/**新增公告分类*/
	public static final String ADD_NOTICE_CATEGORY = "InfomationCategory.insert";
	/**发布公告修改*/
	public static final String STATUS_NOTICE ="Infomation.statusNotice";
	
	public static final String STATUS_ACTIVITY_CENTER ="ActivityCenter.statusNotice";
	
	public static final String STATUS_NEWS_CENTER ="NewsCenter.statusNotice";
	/**首页推荐修改*/
	public static final String UPDATE_HOME_PUSH= "InfomationCategory.updateHomePush";
	/**发布分享修改*/
	public static final String UPDATE_SHARE_STATUS ="Chapter.updateShareStatus";
	/**删除公告*/
	public static final String DELETE_NOTICE = "Infomation.delete";
	
	public static final String DELETE_ACTIVITY_CENTER = "ActivityCenter.delete";
	
	public static final String DELETE_NEWS_CENTER = "NewsCenter.delete";
	/**删除公告*/
	public static final String DELETE_NOTICE_TYPE = "TagView.deleteByCourse";
	/**删除分类*/
	public static final String DELETE_CATEGORY = "InfomationCategory.delete";
	/**编辑公告*/
	public static final String EDIT_NOTICE_MESSAGE ="Infomation.update";
	
	public static final String EDIT_ACTIVITY_CENTER_MESSAGE ="ActivityCenter.update";
	
	public static final String EDIT_NEWS_CENTER_MESSAGE ="NewsCenter.update";
	/**编辑公告分类*/
	public static final String EDIT_NOTICE_CATEGORY ="InfomationCategory.update";
	/**编辑公告检索*/
	public static final String EDIT_NOTICE_SELECT ="Infomation.selectForEdit";
	
	public static final String EDIT_ACTIVITY_CENTER_SELECT ="ActivityCenter.selectForEdit";
	
	public static final String EDIT_NEWS_CENTER_SELECT ="NewsCenter.selectForEdit";
	/**公告不存在*/
	public static final String NOTICE_EXIST = "Infomation.checkNoticeExist";
	
	public static final String ACTIVITY_CENTER_EXIST = "ActivityCenter.checkNoticeExist";
	
	public static final String NEWS_CENTER_EXIST = "NewsCenter.checkNoticeExist";
	
	/**检索班级信息*/
	public static final String GRADE_SEARCH = "Grade.selectGrade";
	/**新增班级*/
	public static final String GRADE_ADD = "Grade.insert";
	/**根据ID查询班级信息*/
	public static final String GRADE_SEARCH_BY_ID = "Grade.selectGradeById";
	/**根据ID更新班级信息*/
	public static final String GRADE_UPDATE_BY_ID = "Grade.updateGradeById";
	/**根据ID删除班级信息*/
	public static final String GRADE_DELETE_BY_ID = "Grade.deleteGradeById";
	/**添加班级学生*/
	public static final String GRADE_ADD_STUDENTS = "UserGrade.insert";
	/**查询班级学生*/
	public static final String GRADE_SEARCH_STUDENTS = "UserGrade.searchUser";
	/**删除班级学生*/
	public static final String GRADE_DELETE_STUDENTS = "UserGrade.deleteUser";
	/**检查班级学生唯一性*/
	public static final String GRADE_UNIQUE_STUDENTS = "UserGrade.uniqueUser";
	/**公告分类不存在*/
	public static final String NOTICE_CATEGORY_EXIST = "InfomationCategory.checkNoticeExist";
	/**分享不存在*/
	public static final String SHARE_EXIST = "Chapter.checkShareExist";
	/**删除分享*/
	public static final String DELETE_SHARE = "Chapter.delete";
	/**检索分类下有没有公告*/
	public static final String CHECK_CATEGORY_EXIST="Infomation.checkCategoryExist";

	/**个人中心首页检索*/
	public static final String PERSONAL_HOME_PAGE_SEARCH = "User.searchUser";
	/**参与的课程检索*/
	public static final String ATTEND_COURSE_SEARCH = "UserCourse.selectAttendCourse";
	/**收藏的课程检索*/
	public static final String COLLECT_COURSE_SEARCH = "UserCourse.selectCollectCourse";
	/**考试列表检索*/
	public static final String EXAM_LIST_SEARCH = "Measurement.select";
	/**分享列表检索*/
	public static final String CASE_LIST_SEARCH = "Chapter.selectShare";
	/**手机端检索课程列表*/
	public static final String SELECT_COURSE_LIST = "Course.selectCourse";
	public static final String SELECT_COURSE_LIST_ALL = "Course.selectCourseAll";
	/**PC端检索课程列表*/
	public static final String SELECT_COURSE_LIST_PC = "Course.selectCourseListPc";
	public static final String SELECT_COURSE_LIST_PC_ALL = "Course.selectCourseListPcAll";

	/**根据平台信息ID检索平台信息*/

	public static final String SEARCH_INFOMATION_BY_ID = "Infomation.selectByPk";
	
	public static final String SEARCH_ACTIVITY_CENTER_BY_ID = "ActivityCenter.selectByPk";
	
	public static final String SEARCH_NEWS_CENTER_BY_ID = "NewsCenter.selectByPk";
	/**检索课程*/
	public static final String SEARCH_COURSE = "Course.select";
	/**检索课程*/
	public static final String SEARCH_COURSE_LIST = "Course.selectCourseList";
	/**检索课程标签*/
	public static final String SEARCH_COURSE_TAG = "Course.searchTags";
	/**根据ID检索课程*/
	public static final String SEARCH_COURSE_BY_PK = "Course.selectByPk";
	/**检索用户课程*/
	public static final String SEARCH_USER_COURSE = "UserCourse.select";
	/**检索指定课程的点击数*/
	public static final String SEARCH_CLICK_COUNT = "Course.searchCount";
	/**检索课程章节*/
	public static final String SEARCH_CHAPTER = "Chapter.select";
	/**检索分享信息*/
	public static final String SEARCH_SHARE_DETAIL= "Chapter.selectShareDetail";
	/**根据ID检索章节信息*/
	public static final String SEARCH_CHAPTER_BY_PK = "Chapter.selectByPk";
	/**检索讨论*/
	public static final String SEARCH_DISCUSS = "Discuss.select";
	/***检索讨论数据*/
	public static final String SEARCH_DISCUSS_DATA = "DiscussData.select";
	/***删除讨论数据*/
	public static final String DELETE_DISCUSS_DATA = "DiscussData.delete";
	/***删除讨论数据*/
	public static final String DELETE_ALL_DISCUSS_DATA = "DiscussData.deleteDiscussData";
	/***找出分享下的所有讨论ID*/
	public static final String SELECT_DISCUSS = "Discuss.selectDiscuss";
	/***删除分享下的所有讨论*/
	public static final String DELETE_ALL_DISCUSS = "Discuss.deleteDiscuss";
	/***检索讨论数据*/
	public static final String SEARCH_DISCUSS_DATA_FOR_LIST = "DiscussData.selectDiscussData";
	/***检索讨论类型combobox*/
	public static final String SEARCH_LINK_TYPE_LIST = "Discuss.selectLinkTypeList";
	/**检索章节信息*/
	public static final String SEARCH_USER_CHAPTER = "UserChatper.select";
	/**更新用户章节信息*/
	public static final String UPDATE_USER_CHAPTER = "UserChapter.update";
	/**更新章节信息*/
	public static final String UPDATE_CHAPTER = "Chapter.update";
	/**新增用户章节信息*/
	public static final String INSERT_USER_CHAPTER = "UserChatper.insert";
	/**更新课程*/
	public static final String UPDATE_COURSE = "Course.update";
	/**检索用户名在数据库中是否存在*/
	public static final String SEARCH_USER_COUNT_BY_USER_ID = "User.checkeUserId";
	/**根据课程ID删除用户课程数据*/
	public static final String DELETE_USER_COURSE_BY_COURSEID = "UserCourse.deleteUserCourseByCourseId";
	/**根据章节ID删除用户章节数据*/
	public static final String DELETE_USER_CHAPTER_BY_CHAPTERID = "UserChatper.deleteUserChapter";
	
	//根据链接ID检索讨论数据
	public static final String DISCUSS_DATA_BY_LINKID = "DiscussData.selectDataByLinkId";
	//检索讨论标题信息
	public static final String DISCUSS_SEARCH = "Discuss.select";
	//插入讨论标题信息
	public static final String DISCUSS_INSERT = "Discuss.insert";
	//插入讨论明细信息
	public static final String DISCUSS_DATA_INSERT = "DiscussData.insert";
	/**我的投票*/
	public static final String SEARCH_USER_VOTE = "UserCourse.selectUserVote";
	/**我的测试*/
	public static final String SEARCH_USER_MEASUREMENT = "Measurement.selectUserMeasurement";
	/**我的考试*/
    public static final String SEARCH_USER_EXAM= "Measurement.selectUserExam";
	//检索章节的评分总分
	public static final String SELECT_SCORE_SUM_BY_CHAPTER = "UserChatper.selectSumByChapter";
	//检索章节的评分人数
	public static final String SELECT_USER_CHAPTER_SCORE_COUNT = "UserChatper.selectCountByChapter";
	//更新章节评分
	public static final String UPDATE_CHAPTER_SCORE = "Chapter.updateScore";
	//更新用户章节评分
	public static final String UPDATE_USER_CHAPTER_SCORE = "UserChatper.updateScore";
	//检索章节投票人数
	public static final String SELECT_USER_CHAPTER_VOTE_COUNT = "UserChatper.selectCountVoteByChapter";
	//更新章节投票信息
	public static final String UPDATE_CHAPTER_VOTE = "Chapter.updateVote";
	//更新用户章节投票
	public static final String UPDATE_USER_CHAPTER_VOTE = "UserChatper.updateVote";

	//检索课程的评分总分
	public static final String SELECT_SCORE_SUM_BY_COURSE = "UserCourse.selectSumByChapter";
	//检索课程的评分人数
	public static final String SELECT_USER_COURSE_SCORE_COUNT = "UserCourse.selectCountByChapter";
	//更新课程评分
	public static final String UPDATE_COURSE_SCORE = "Course.updateScore";
	//更新用户课程评分
	public static final String UPDATE_USER_COURSE_SCORE = "UserCourse.updateScore";
	//检索课程投票人数
	public static final String SELECT_USER_COURSE_VOTE_COUNT = "UserCourse.selectCountVoteByChapter";
	//更新课程投票信息
	public static final String UPDATE_COURSE_VOTE = "Course.updateVote";
	//更新用户课程投票
	public static final String UPDATE_USER_COURSE_VOTE = "UserCourse.updateVote";
	
	
	//检索问题选项
	public static final String MEASUERMENT_SEARCH_QUESTION_OPTIONS_LIST = "QuestionOption.select";
	//检索问题
	public static final String MEASUERMENT_SEARCH_QUESTION_LIST = "Question.select";
	public static final String MEASUERMENT_SEARCH_QUESTION_LIST_BY_MEASUREMENT = "Measurement.selectQuestionResult";
	public static final String MEASUERMENT_SEARCH = "Measurement.select";
	
	public static final String USER_MEASUERMENT_ANSWER_INSERT = "UserMeasuermentAnswer.insert";
	public static final String USER_MEASUERMENT_RESULT_INSERT = "UserMeasurementResult.insert";
	public static final String MEASUERMENT_SEARCH_EXTENDS = "Measurement.selectExtends";
	public static final String MEASUERMENT_ANSWER_DELETE = "UserMeasuermentAnswer.delete";
	//计算用户测试的总分
	public static final String USER_MEASUERMENT_RESULT_SEARCH_SCORE = "UserMeasurementResult.selectResultScore";
	//根据主键删除用户回答信息
	public static final String USER_MEASUERMENT_RESULT_DELETE = "UserMeasurementResult.delete";
	
	//根据USER_ID检索测试ID
	public static final String SEARCH_MEASUREMENT_ID_BY_USER_ID = "UserMeasurementResult.select";
	public static final String MEASUERMENT_USER_SEARCH = "UserMeasurementResult.selectMeasurementUser";
	//根据measurementId检索测试数据
	public static final String SEARCH_MEASUREMENT_DATA_BY_ID = "Measurement.selectByIdAndMeasureType";
	//根据measurementId检索考试数据
	public static final String SEARCH_MEASUREMENT_DATA_BY_ID_AND_MEASURE_TYPE = "Measurement.selectByIdAndMeasurementType";
	//根据USER_ID检索讨论数据ID
	public static final String SEARCH_DISCUSS_DATA_BY_USER_ID = "DiscussData.select";
	//根据discussId检索讨论数据
	public static final String SEARCH_DISCUSS_BY_ID = "Discuss.selectById";
	//根据discussId检索用户讨论内容
	public static final String SEARCH_DISCUSS_USER_DATA = "DiscussData.selectByDiscussId";
	
	
	//根据USER_ID检索课程ID
	public static final String SEARCH_COURSE_ID_BY_USER_ID = "UserCourse.select";
	//检索我的分享
	public static final String SEARCH_CHAPTER_DATA_BY_ID = "Chapter.selectMyChapter";
	//新增我的分享
	public static final String INSERT_CHAPTER_DATA = "Chapter.insertChapterData";
	/**分享管理检索*/
	public static final String SEARCH_SHARE_LIST ="Chapter.searchShareList";
	
	//根据USER_ID检索用户数据信息
	public static final String USER_SEARCH_DADTA = "User.searchUser";
	//根据用户id检索姓名
	public static final String USER_SEARCH_NAME_BY_USERID = "UserManagement.searchUserNameByUserId";
	//根据评价id检索
	public static final String USER_SEARCH_NAME_BY_EVALUTION = "TbInstitutionEvalution.selectById";
	//检索用户数据信息
	public static final String USER_SEARCH_CONDITION = "UserManagement.searchUser";
	
	//检索用户数据信息
	public static final String SQL_USER_ALL_INFO = "UserManagement.searchManagerUserAll";
	//检索用户数据信息
	public static final String SQL_SEARCH_USER_ALL_INFO = "UserManagement.searchUserAll";
	//根据USER_ID检索用户名是否已存在且没有被删除
	public final static String USER_NAME_EXIST = "UserManagement.checkUserNameExist";
	
	public final static String USER_EXIST = "UserManagement.checkUserExist";
	//检索Email是否已存在
	public final static String USER_EMAIL_EXIST = "UserManagement.checkEmailExist";
	//更新用户信息
	public final static String USER_UPDATE = "UserManagement.updateUser";
	//插入新用户信息
	public final static String USER_INSERT = "UserManagement.insertUser";
	//标记用户已删除
	public final static String USER_UPDATE_DEL_FLAG = "UserManagement.updateDelFlagById";
	//删除用户角色
	public final static String DELETE_USER_ROLE_BY_USER_ID = "UserManagement.deleteUserRole";
	//更新用户认证为教师/管理员
	public final static String UPDATE_USER_BY_ROLE_ID = "UserManagement.updateRoleDataUserById";
	//插入用户认证为教师/管理员
	public final static String INSERT_USER_BY_ROLE_ID = "UserManagement.insertRoleDataUserById";
	//取消用户的老师/管理员认证
	public final static String DELETE_BY_USER_ID_ADD_ROLE_ID = "UserManagement.deleteByUserIdAddRoleId";
	//检索用户所有信息
	public static final String USER_MANAGEMENT_SEARCH_USER_ALL = "UserManagement.searchUserAll";
	//更新用户所有信息
	public static final String USER_MANAGEMENT_UPDATE_USER_ALL = "UserManagement.updateUserAll";
	
	//检索用户角色表是否存在该用户
	public final static String SEARCH_COUNT_BY_USER_ID = "UserManagement.searchCountByUserId";
	
	public final static String SEARCH_COUNT_BY_USER_NAME = "UserManagement.searchUserByUsername";
	
	//检索用户角色表是否存在该用户
	public final static String SEARCH_ROLE_ID_BY_USER_ID = "UserManagement.searchRoleIdByUserId";
	//更新用户USER_KBN字段
	public final static String UPDATE_USER_KBN = "UserManagement.updateUserKbn";
	
	//更新问题信息删除问题选项
	public static final String QUESTION_DELET_OPTION_BY_QUESTION = "Question.deletOptionByQuestion";
	//根据问题信息删除用户回答的问题
	public static final String QUESTION_DELET_USER_ANSWER_BY_QUESTION = "Question.deletUserAnswerByQuestion";
	//获取课程下的最大章节数
	public static final String CHAPTER_SEARCH_MAX_ORDER = "Chapter.searchMaxOrder";
	
	/**检索相关课程*/
	public static final String SEARCH_RELEVANT = "Course.searchRelevantCourse";
	/**检索课程分类*/
	public static final String SEARCH_COURSE_CATEGORY = "Course.searchCourseCategory";

	/**课程标签管理一览*/
	public static final String COURSE_TAG_SEARCH = "TpTag.select";
	/**查找标签名*/
	public static final String SEARCH_TAG = "TpTag.searchTag";
	/**查找分类下的课程*/
	public static final String SEARCH_COUNT_CATAGORY = "CourseCatagory.searchCategory";
	/**更新删除数据状态*/
	public static final String UPDATE_DELETE_TAG = "TpTag.updateToDelete";
	

	//删除测试相关问题
	public static final String MEASUREMENT_DELETE_QUESTION = "Measurement.deleteQuestion";
	//删除测试相关问题中的选项
	public static final String MEASUREMENT_DELETE_OPTION = "Measurement.deleteOption";
	//删除测试相关用户回答
	public static final String MEASUREMENT_DELETE_USER_REESULT = "Measurement.deleteUserReesult";
	//删除测试相关用户答题
	public static final String MEASUREMENT_DELETE_USER_ANSWER = "Measurement.deleteUserAnswer";
	//删除某个用户的答题
	public static final String MEASUREMENT_DELETE_ANSWER_OF_USER = "Measurement.deleteAnswerOfUser";
	//插入考试结果历史表
	public static final String USER_MEASUREMENT_RESULT_INSERTHIS ="UserMeasurementResult.insertHis";
	//删除考试结果历史表
	public static final String USER_MEASUREMENT_RESULT_DELETEHIS ="UserMeasurementResult.deleteHis";
	//更新考试结果历史表
	public static final String USER_MEASUREMENT_RESULT_UPDATEHIS="UserMeasurementResult.updateHis";
	//插入考试答案历史表
	public static final String USER_MEASUREMENT_ANSWER_INSERTHIS = "UserMeasuermentAnswer.insertHis";
	public static final String USER_MEASUREMENT_ANSWER_DELETEHIS = "UserMeasuermentAnswer.deleteHis";
	public static final String SQL_SELECT_ANSWER_USER = "UserMeasuermentAnswer.selectAnswer";
	/**查询答案*/
	public static final String SQL_EVALUTION_SELECT_ANSWER_USER = "UserMeasuermentAnswer.selectEvalutionAnswer";
	/**老师首页-问卷调查检索*/
    public static final String SQL_SELECT_MEASURE_INFO_LIST="Measurement.selectMeasureInfoList";
    /**老师首页-问卷详情查看检索*/
    public static final String SQL_SELECT_OPTIONS_INFO_LIST="QuestionOption.selectOptionByMeasureId";
    /**老师首页-问卷详情-检索选项的最大值*/
    public static final String SQL_SELECT_MAX_OPTIONS_INFO_QUESTION="QuestionOption.selectMaxOptionsByMeasureId";

    public static final String SEARCH_USER_HOME_WORK_ALL="Homework.searchUserHomeworkAll";
    
    /**查询教师等级列表*/
    public static final String  SQL_SEARCH_TEAHER_LEVEL_LIST="TeacherLevel.select";
    /**保存老师等级信息*/
    public static final String SQL_INSERT_TEACHER_LEVEL="TeacherLevel.insert";
    /**教师等级删除*/
    public static final String SQL_TEACHER_LEVEL_DELETE="TeacherLevel.delete";
    /**根据ID检索教师等级信息*/
    public static final String SQL_SEACHER_TEACHER_LEVEL_BY_ID="TeacherLevel.selectByPk";
    /**更新教师等级信息*/
    public static final String SQL_UPDATE_TEACHER_LEVEL="TeacherLevel.updateLevel";

    /** 根据管理者检索计划 */
	public static final String SEARCH_PLAN_BY_MANAGEUSER = "Plan.selectPlanByManageUser";
	/** 根据planId查询用户信息 */
	public static final String SEARCH_PLANUSERID_BY_PLANID = "PlanUser.selectUserByPlanId";
	/**检索讲师提交信息*/
	public static final String SEARCH_TEACHERAPPLY="TeacherApply.select";
	/**检索讲师最近提交信息的状态*/
	public static final String SEARCH_TEACHER_APPLY_STATUS="TeacherApply.selectTeacherApplyStatus";
	/** 根据taglink检索tagName */
	public static final String SEARCH_TAG_NAME_BY_TAG_LINK = "TagLink.selectTagName";
	/**检索UserRole信息*/
	public static final String SEARCH_USER_ROLE="UserRole.select";
	/**检索资源信息*/
	public static final String SQL_SEARCH_RESOURSE_LIST="Resource.selectResource";
	/**资源更新*/
	public static final String SQL_UPDATE_RESOURSE_STATE="Resource.updateResource";
	/**资源删除*/
	public static final String SQL_DELETE_RESOURSE_STATE="Resource.deleteResource";
	/**资源上传*/
	public static final String SQL_INSERT_RESOURCE="Resource.insertResource";
	/**根据ID查询*/
	public static final String SQL_INSERT_RESOURCE_BY_ID="Resource.select4Move";
	/**资源更新*/
	public static final String SQL_UPDATE_RESOURCE_BY_ID="Resource.updateResourceById";
	
	/**活动列表*/
	public static final String SQL_SEARCH_ACTIVE_LIST="Active.selectActiveList";
	/**活动列表*/
    public static final String SQL_SEARCH_ACTIVE_LIST_GUEST="Active.selectListGuest";
	/**活动删除*/
	public static final String SQL_DELETE_ACTIVE="Active.delete";
	/**活动详情*/
	public static final String SQL_SEARCH_ACTIVE_DETAIL="Active.selectByPk";
	/**活动详情*/
    public static final String SQL_SEARCH_ACTIVE_DETAIL_FLAG="Active.selectById";
	/**活动新增*/
	public static final String SQL_INSERT_ACTIVE="Active.insert";
	public static final String SQL_UPDATE_ACTIVE="Active.update";
	/**更新报名人数*/
	public static final String  SQL_UPDATE_NUM_ENTRY="Active.updateNumEntry";
	/**更新签到人数*/
	public static final String SQL_UPDATE_NUM_SIGN="Active.updateNumSign";
	/**活动报名和签到*/
	public static final String SQL_INSERT_USER_ENTRY="UserActive.insert";
	public static final String SQL_SEARCH_USER_BY_ID="UserActive.searchById";
	/**活动报名列表*/
	public static final String SQL_SEARCH_ENTRY_LIST="UserActive.selectEntryList";
	public static final String SQL_USER_ACTIVE_LIST="UserActive.selectList";
	/**活动签到列表*/
	public static final String SQL_SEARCH_SIGN_LIST="UserActive.selectSignList";
	public static final String  SQL_UPDATE_USER_SIGN="UserActive.updateUserSign";
	
	public static final String  SQL_UPDATE_USER_ENTRY="UserActive.updateUserEntry";
	/**我的活动-报名*/
	public static final String SQL_SEARCH_MY_ACTIVE_ENTRY="Active.searchMyActiveEntry";
	/**我的活动-签到*/
	public static final String SQL_SEARCH_MY_ACTIVE_SIGN="Active.searchMyActiveSign";
	public static final String  SQL_IS_USER_ACTIVE_EXIST="UserActive.isActiveExist";
	/**检索活动明细的讨论信息*/
	public static final String SQL_SEARCH_ACTIVE_DISCUSS = "DiscussData.searchTutorRemarkList";
	/**保存活动明细的讨论信息*/
	public static final String SQL_SAVE_ACTIVE_DISCUSS = "Discuss.saveRemarkDiscuss";
	/**保存活动明细的讨论数据信息*/
	public static final String SQL_SAVE_DISCUSS_DATA_ACTIVE = "DiscussData.insert";
	/**根据ID检索DiscussEntity*/
	public static final String SQL_SEARCH_ACTIVE_DISCUSS_BY_PK = "Discuss.select";
	/**道具列表Props*/
	public static final String SQL_SEARCH_PROPS="Props.select";
	public static final String SQL_SEARCH_PROPS_BY_ID="Props.selectByPk";
	/**道具新增*/
	public static final String SQL_INSERT_PROPS="Props.insert";
	/**道具编辑*/
    public static final String SQL_UPDATE_PROPS="Props.update";
	/**赠送道具提交*/
    public static final String SQL_INSERT_PROPS_HIS="PropsHis.insert";
    /**导师屏蔽赠送道具信息*/
    public static final String SQL_UPDATE_FLAG_SHOW_MSG="PropsHis.updateFlagShowMsg";
    /**导师获得的道具历史*/
    public static final String SQL_SEARCH_PROPS_HIS_GET="PropsHis.searchGetPropsHis";
    /**用户赠送导师道具排行*/
    public static final String SQL_SEARCH_PROPS_HIS_DIVE="PropsHis.searchGivePropsHis";
    /**道具删除*/
    public static final String SQL_DELETE_PROPS="Props.delete";
    /**道具赠送历史*/
    public static final String  SQL_SEARCH_PROPS_HIS_LIST="PropsHis.searchPropsHisList";
    public static final String  SQL_PROPS_NOT_EXIST="PropsHis.isPropsHisExist";
    /**道具删除*/
    public static final String SQL_SELECT_PROPS_HIS_BY_ID="PropsHis.selectByPk";
    /**插入话题*/
    public static final String SQL_INSERT_TOPIC="Topic.insert";
    public static final String SQL_SEARCH_TOPIC_BY_USER="Topic.searchByUserId";
    public static final String SQL_SEARCH_TOPIC_USER="Topic.searchByUser";
    public static final String SQL_DELETE_TOPIC="Topic.deleteByUserId";
    public static final String SQL_SEARCH_LIST_BY_USER="Topic.searchListByUserId";
    
    public static final String ALL_USER_OPER_SEARCH = "UserOper.searchAllUserOper";
    public static final String USER_OPER_SEARCH = "UserOper.searchUserOper";
    
    
    public static final String TEACHER_INSERT = "Teacher.insert";
    public static final String TEACHER_UPDATE = "Teacher.updateTeacher";
    public static final String TEACHER_SELECT_BY_PK = "Teacher.selectByPk";
    
    public static final String TEACHER_SELECT_BY_USER_ID = "Teacher.selectByUserId";
    
    public static final String TEACHER_FAMILY_MEMBER_INSERT = "FamilyMember.insert";
    public static final String TEACHER_FAMILY_MEMBER_UPDATE = "FamilyMember.update";
    public static final String TEACHER_FAMILY_MEMBER_SELECT_BY_PK = "FamilyMember.selectByPk";
    public static final String TEACHER_FAMILY_MEMBER_DELETE = "FamilyMember.delete";
    
    public static final String TEACHER_CERTIFICATE_INSERT = "Certificate.insert";
    public static final String TEACHER_CERTIFICATE_UPDATE = "Certificate.update";
    public static final String TEACHER_CERTIFICATE_SELECT_BY_PK = "Certificate.selectByPk";
    public static final String TEACHER_CERTIFICATE_DELETE = "Certificate.delete";
    /**检索班级信息*/
	public static final String INSTITUTION_SEARCH = "Institution.selectInstitution";
	/**检索班级信息*/
	public static final String INSTITUTION_EXPORT_SEARCH = "Institution.selectExportInstitution";
	/**检索班级信息*/
	public static final String INSTITUTION_ORGAN_SEARCH = "InstitutionOrgan.select";
	/**检索班级信息*/
	public static final String INSTITUTION_MANAGER_SEARCH = "InstitutionManager.select";
	/**检索班级信息*/
	public static final String INSTITUTION_CITY_SEARCH = "TCityMapper.select";
	/**检索班级信息*/
	public static final String INSTITUTION_DISTRIC_SEARCH = "TDistricMapper.select";
	/**检索班级信息*/
	public static final String INSTITUTION_PROVICE_SEARCH = "TProvinceMapper.select";
    /**检索班级信息*/
	public static final String INSTITUTION_SCHOOL_SEARCH = "Institution.selectSchoolInstitution";
	 /**检索教育局下学校信息*/
	public static final String INSTITUTION_ORGAN_SCHOOL_SEARCH = "Institution.selectManagerSchoolInstitution";
	 /**检索校长和老师信息*/
	public static final String INSTITUTION_TEACHER_SCHOOL_SEARCH = "Institution.selectManagerTeacherSchoolInstitution";
	 /**检索本校信息*/
	public static final String INSTITUTION_PERSON_TEACHER_SCHOOL_SEARCH = "Institution.seacherSchoolLocationList";
	 /**检索教育局学校信息*/
	public static final String INSTITUTION_EDUTION_TEACHER_SCHOOL_SEARCH = "Institution.searchEdutionSchoolList";
	 /**检索学校教育局学校信息*/
	public static final String INSTITUTION_EDUTION_SCHOOL_SEARCH = "Institution.seacherPersonEdution";
	/**检索学校教育局学校信息*/
	public static final String INSTITUTION_EDUTION_SEARCH = "Institution.searchEdution";
    /**检索班级信息*/
	public static final String INSTITUTION_SCHOOL_ALL_SEARCH = "Institution.selectSchoolInstitution1";
   /**检索班级信息*/
	public static final String INSTITUTION_EVALUTION_SCHOOL_SEARCH = "Institution.selectSchoolEvalutionInstitution";
	/**新增班级*/
	public static final String INSTITUTION_ADD = "Institution.insert";
	/**新增班级*/
	public static final String INSTITUTION_ORGAN_ADD = "InstitutionOrgan.insert";
	/**新增班级*/
	public static final String INSTITUTION_MANAGER_ADD = "InstitutionManager.insert";
	/**新增班级*/
	public static final String INSTITUTION_DISTRIC_ADD = "TDistricMapper.insert";
	/**新增班级*/
	public static final String INSTITUTION_CITY_ADD = "TCityMapper.insert";
	/**新增班级*/
	public static final String INSTITUTION_PROVICE_ADD = "TProvinceMapper.insert";
	/**根据ID查询班级信息*/
	public static final String INSTITUTION_SEARCH_BY_ID = "Institution.selectInstitutionById";
	/**根据ID查询班级信息*/
	public static final String INSTITUTION_ORGAN_SEARCH_BY_ID = "InstitutionOrgan.selectInstitutionOrganById";
	/**根据ID查询班级信息*/
	public static final String INSTITUTION_MANAGER_SEARCH_BY_ID = "InstitutionManager.selectInstitutionManagerById";
	/**根据ID查询班级信息*/
	public static final String INSTITUTION_CITY_SEARCH_BY_ID = "TCityMapper.selectById";
	/**根据ID查询班级信息*/
	public static final String INSTITUTION_DISTRIC_SEARCH_BY_ID = "TDistricMapper.selectById";
	/**根据ID查询班级信息*/
	public static final String INSTITUTION_PROVINCE_SEARCH_BY_ID = "TProvinceMapper.selectById";
	/**保存评价*/
	public static final String INSTITUTION_INSERT = "TbInstitutionEvalution.insert";
	/**更新评价*/
	public static final String INSTITUTION_UPDATE = "TbInstitutionEvalution.update";
	/**根据ID更新班级信息*/
	public static final String INSTITUTION_UPDATE_BY_ID = "Institution.updateInstitutionById";
	/**根据ID更新班级信息*/
	public static final String INSTITUTION_ORGAN_UPDATE_BY_ID = "InstitutionOrgan.updateInstitutionOrganById";
	/**根据ID更新班级信息*/
	public static final String INSTITUTION_MANAGER_UPDATE_BY_ID = "InstitutionManager.updateInstitutionManagerById";
	/**根据ID更新班级信息*/
	public static final String INSTITUTION_CITY_UPDATE_BY_ID = "TCityMapper.updateById";
	/**根据ID更新班级信息*/
	public static final String INSTITUTION_DISTRIC_UPDATE_BY_ID = "TDistricMapper.updateById";
	/**根据ID更新班级信息*/
	public static final String INSTITUTION_PROVINCE_UPDATE_BY_ID = "TProvinceMapper.updateById";
	/**根据ID删除班级信息*/
	public static final String INSTITUTION_DELETE_BY_ID = "Institution.deleteInstitutionById";
	/**根据ID删除评价信息*/
	public static final String INSTITUTION_EVALUTION_DELETE_BY_ID = "TbInstitutionEvalution.delete";
	/**批量查询*/
	public static final String INSTITUTION_SEARCH_BY_IDS = "Institution.selectInstitutionByIds";
    
    
    public static final String STUDENT_INSERT = "Student.insert";
    public static final String STUDENT_UPDATE = "Student.updateStudent";
    public static final String STUDENT_SELECT_BY_PK = "Student.selectByPk";
    
    public static final String STUDENT_SELECT_BY_USER_ID = "Student.selectByUserId";
    
    public static final String STUDENT_FAMILY_MEMBER_INSERT = "StudentFamilyMember.insert";
    public static final String STUDENT_FAMILY_MEMBER_UPDATE = "StudentFamilyMember.update";
    public static final String STUDENT_FAMILY_MEMBER_SELECT_BY_PK = "StudentFamilyMember.selectByPk";
    public static final String STUDENT_FAMILY_MEMBER_DELETE = "StudentFamilyMember.delete";
    
    public static final String STUDENT_CERTIFICATE_INSERT = "StudentCertificate.insert";
    public static final String STUDENT_CERTIFICATE_UPDATE = "StudentCertificate.update";
    public static final String STUDENT_CERTIFICATE_SELECT_BY_PK = "StudentCertificate.selectByPk";
    public static final String STUDENT_CERTIFICATE_DELETE = "StudentCertificate.delete";
    /**学校-问卷调查检索*/
    public static final String SQL_SELECT_MEASURE_RESULT_INFO_LIST="MeasurementInstitution.selectMeasureInfoList";
    /**学校-问卷详情查看检索*/
    public static final String SQL_SELECT_INSTITUTION_OPTIONS_INFO_LIST="QuestionOptionInstitution.selectOptionByMeasureId";
    /**老师首页-问卷详情-检索选项的最大值*/
    public static final String SQL_SELECT_INSTITUTION_MAX_OPTIONS_INFO_QUESTION="QuestionOptionInstitution.selectMaxOptionsByMeasureId";
    
    public static final String TEACHER_HONOR_CERTIFICATE_INSERT = "TeacherHonorCertificate.insert";
    public static final String TEACHER_HONOR_CERTIFICATE_UPDATE = "TeacherHonorCertificate.update";
    public static final String TEACHER_HONOR_CERTIFICATE_SELECT_BY_PK = "TeacherHonorCertificate.selectByPk";
    public static final String TEACHER_HONOR_CERTIFICATE_DELETE = "TeacherHonorCertificate.delete";
    
    public static final String STUDENT_QUALITY_REPORT_SELECT = "StudentQualityReport.select";
    public static final String STUDENT_QUALITY_REPORT_INSERT = "StudentQualityReport.insert";
    public static final String STUDENT_QUALITY_REPORT_SELECT_BY_PK = "StudentQualityReport.selectByPk";
    /**学校-问卷详情查看检索*/
    public static final String SQL_SELECT_TEACHER_OPTIONS_INFO_LIST="QuestionOptionTeacher.selectOptionByMeasureId";
	/**根据用户ID查询班级信息*/
	public static final String USER_GRADE_SEARCH_BY_ID = "Grade.selectPersonGrade";
	 /**检索教师信息*/
	public static final String TEACHER_CLASS_SEARCH = "TeacherClassInfo.select";
	/**新增教师班级信息*/
	public static final String TEACHER_CLASS_ADD = "TeacherClassInfo.insert";
	/**根据ID查询教师信息*/
	public static final String TEACHER_CLASS_SEARCH_BY_ID = "TeacherClassInfo.selectTeacherClassInfoById";
	/**根据ID更新教师班级信息*/
	public static final String TEACHER_CLASS_UPDATE_BY_ID = "TeacherClassInfo.updateTeacherClassInfoById";
	/**根据ID删除教师班级信息*/
	public static final String TEACHER_CLASS_DELETE_BY_ID = "TeacherClassInfo.deleteTeacherClassInfoById";
	 /**检索学生信息*/
	public static final String STUDENT_CLASS_SEARCH = "StudentClassInfo.select";
	 /**检索学生信息*/
	public static final String STUDENT_ONE_LASS_SEARCH = "StudentClassInfo.selectByInfo";
	/**新增学生班级信息*/
	public static final String STUDENT_CLASS_ADD = "StudentClassInfo.insert";
	/**根据ID查询学生信息*/
	public static final String STUDENT_CLASS_SEARCH_BY_ID = "StudentClassInfo.selectStudentClassInfoById";
	/**根据ID更新学生班级信息*/
	public static final String STUDENT_CLASS_UPDATE_BY_ID = "StudentClassInfo.updateStudentClassInfoById";
	/**根据ID删除学生班级信息*/
	public static final String STUDENT_CLASS_DELETE_BY_ID = "StudentClassInfo.deleteStudentClassInfoById";
	/**查询学校下的年级信息*/
	public static final String STUDENT_GRADE_BY_ID = "StudentClassInfo.searchStudentGradeList";
	/**查询学校下的年级下班级信息*/
	public static final String STUDENT_CLASS_BY_ID = "StudentClassInfo.searchStudentClassList";
	/**活动报名数*/
	public static final String SELECT_ACTIVITY_COUNT = "ActivityCenterUser.selectCount";
	/** 根据ID条件查询标签名 */
	public static final String SEARCH_TAG_BY_TAG_ID = "TagMap.searchTagNameByTagId";
	//根据用户id检索姓名
	public static final String USER_BY_USERID = "UserManagement.searchUserByid";
	/**非管理员*/
	public static final String SELECT_NOT_NEWS_CENTER = "NewsCenter.selectAllNotManager";
	/**非管理员*/
	public static final String SELECT_NOT_ACTIVITY_CENTER = "ActivityCenter.selectNotManager";
	 /**检索教师创建查询得学生信息*/
	public static final String STUDENT_TEACHER_CLASS_SEARCH = "StudentClassInfo.selectByGroupSchoolId";
	
	/**作业提交状态*/
	public static final String HOMEWORK_ANSWER_STATUS_ONE = "1";
}
