package com.cloudsoaring.web.trainingplatform.constants;

import java.math.BigDecimal;

import com.cloudsoaring.web.common.dao.ibatis.plugin.MybatisConstant;


@MybatisConstant
public interface TpStateConstants {


	//测验类型
	public static final String MEASUERMENT_TYPE = "MEASUERMENT_TYPE";
	//测验类型 - 测试
	public static final String MEASUERMENT_TYPE_MEASUERMENT = "1";
	//测验类型 - 考试
	public static final String MEASUERMENT_TYPE_EXAM = "2";
	//测验类型 - 问卷
	public static final String MEASUERMENT_TYPE_QUESTIONNAIRE = "3";

	//所属章节/课程 - 章节
	public static final String LINK_TYPE_CHAPTER = "CHAPTER";
	//所属章节/课程 - 分享
	public static final String LINK_TYPE_SHARE = "SHARE";
	//所属章节/课程 - 课程
	public static final String LINK_TYPE_COURSE = "COURSE";
	//所属章节/课程 - 课程
    public static final String LINK_TYPE_EVALUTION = "EVALUTION";
	
	//讨论类型
	public static final String DISCUSS_TYPE = "DISCUSS_TYPE";
	//讨论类型 - 章节
	public static final String DISCUSS_TYPE_CHAPTER = "CHAPTER";
	//讨论类型 - 课程
	public static final String DISCUSS_TYPE_COURSE = "COURSE";
	//讨论类型 - 分享
	public static final String DISCUSS_TYPE_SHARE = "SHARE";
	//讨论类型 - 活动
	public static final String DISCUSS_TYPE_ACTIVE = "ACTIVE";
	//投票选项 - 好评
	public static final String VOTE_OPTION_GOOD = "4";
	//投票选项 - 中评
	public static final String VOTE_OPTION_NORMAL = "2";
	//投票选项 - 差评
	public static final String VOTE_OPTION_NOT_GOOD = "1";

	//课程类型 - 普通课程
	public static final String COURSE_TYPE_COMMON = "C";
	//课程类型 - 案例课程
	public static final String COURSE_TYPE_CASE = "A";
	//课程状态
	public static final String COURSE_STATUS="COURSE_STATUS";
	//课程状态 - 未发布
	public static final String COURSE_STATUS_NOT_PUBLIC = "0";
	//课程状态 - 发布
	public static final String COURSE_STATUS_PUBLIC = "1";
	//课程标签分类Id
	public static final String TAG_TYPE_COURSE = "0";
	//课程标签 - 首页推荐
	public static final String TAG_COURSE_HOME = "99";
	/**推荐状态*/
	public static final String PUSH_STATUS="PUSH_STATUS";
	/**平台信息-首页轮播-首页推荐*/
	public static final BigDecimal BANNER_PUSH = new BigDecimal("1");
	/**平台信息-首页轮播-首页不推荐*/
	public static final BigDecimal BANNER_NO_PUSH = new BigDecimal("0");
	/**公告分类为全部*/
	public static final String ALL_NOTICE_CATEGORY= "1000";
	/**普通课程*/
	public static final String COURSE_TYPE_C = "C";
	/**案例课程*/
	public static final String COURSE_TYPE_A = "A";
	/**用户是否加入课程-是*/
	public static final BigDecimal ALREADY_JOIN = new BigDecimal("1");
	/**用户是否加入课程-否*/
	public static final BigDecimal NOT_JOIN = new BigDecimal("0");
	/**是否收藏课程-是*/
	public static final BigDecimal ALREADY_SAVE = new BigDecimal("1");
	/**是否收藏课程-否*/
	public static final String NOT_SAVE = "0";
	/**用户是否点击章节-是*/
	public static final String ALREADY_CLICK = "1";
	/**用户是否点击章节-否*/
	public static final String NOT_CLICK = "0";
	/**课程 - 免费*/
	public static final BigDecimal COURSE_FREE_FLAG_FREE = new BigDecimal("1"); 
	/**课程 - 收费*/
	public static final BigDecimal COURSE_FREE_FLAG_CASH = new BigDecimal("0"); 
	/**老师角色*/
	public static final String TEACHER_ROLE = "04";
	/**管理员角色*/
	public static final String ADMINISTRATOR_ROLE = "01";
	/**管理员或者教师角色的用户类型*/
	public static final String USER_KBN_A = "A";
	/**普通用户角色的用户类型*/
	public static final String USER_KBN_C = "C";
	/**平台信息-已发布*/
	public static final BigDecimal INFOMATION_STATUS_ALREADY = new BigDecimal("1");
	/**平台信息-未发布*/
	public static final BigDecimal INFOMATION_STATUS_NOT = new BigDecimal("0");
	
	/**特殊课程--分享 的ID*/
	public static final String COURSE_ID_FOR_CHAPTER = "0";
	/**分享-已发布*/
	public static final String COURSE_STATUS_ALREADY = "1";
	/**分享-未发布*/
	public static final String COURSE_STATUS_NOT = "0";
	/**章节-已发布*/
	public static final String CHAPTER_STATUS_ALREADY = "1";
	/**章节-未发布*/
	public static final String CHAPTER_STATUS_NOT = "0";
	/**章节-未发布*/
	public static final String NOTICE_LINK_TYPE = "NOTICE";
	/**课程状态*/
	public static final String COURSE_STATUS_PROGRESS = "course_status_progress";
	/**课程状态-连载中*/
	public static final String COURSE_STATUS_CONTINU = "1";
	/**课程状态-已完成*/
	public static final String COURSE_STATUS_FINISH = "2";
	/**课程状态-已结束*/
	public static final String COURSE_STATUS_OVER = "3";
	/**大章节*/
	public static final BigDecimal CHAPTER_FLAG_BIG = new BigDecimal("1");
	/**小章节*/
	public static final BigDecimal CHAPTER_FLAG_SMALL = new BigDecimal("0");
	
	
	//标签类相关
	/**标签类 - 课程相关推荐*/
	public static final String TAG_TYPE_COURE_PUSH = "1";
	/**课程首页*/
	public static final String TAG_COURE_PUSH_HOME = "11";
	/**课程首页轮播*/
	public static final String TAG_COURE_PUSH_HOME_BANNER = "12";
	/**标签关联表 - 外链接ID 类型 */
	public static final String TAG_LINK_TYPY = "1";
	
	/**标签-未删除*/
	public static final String TAG_NOT_DELETE = "0";
	/**标签-已删除*/
	public static final String TAG_DELETE = "1";
	/**用户角色*/
	public static final String TP_USER_ROLE = "TP_USER_ROLE";
	
	/**考试状态 - 开始*/
	public static final String EXAM_STATUS_START = "1";
	/**考试状态 - 结束*/
	public static final String EXAM_STATUS_END = "2";
	
	/**问题类型  - 单选*/
	public static final String QUESTION_TYPE_RADIO = "1";
	/**问题类型  - 多选*/
	public static final String QUESTION_TYPE_MULT = "2";
	/**问题类型  - 判断*/
	public static final String QUESTION_TYPE_JUDGMENT = "3";
	/**标签类型  - 公告*/
	public static final String NOTICE_TAG_TYPE = "8";
	
	public static final String ACTIVITY_CENTER_TAG_TYPE = "6";
	
	public static final String NEWS_CENTER_TAG_TYPE = "7";
	/**资源状态：状态（用于资源管理：1【审批通过】，2【撤销】）*/
    public static final String STATE_SHARE = "STATE_SHARE";
    public static final String STATE_SHARE_CANCEL = "2";
    /**活动状态*/
    public static final String ACTIVE_STATUS = "ACTIVE_STATUS";
    /**活动状态-未发布*/
    public static final String ACTIVE_STATUS_NOT_PUBLIC = "1";
    /**活动状态-报名中*/
    public static final String ACTIVE_STATUS_START = "3";
    /**活动状态-进行中/即将开始*/
    public static final String ACTIVE_STATUS_FOR = "2";
    /**活动状态-已结束*/
    public static final String ACTIVE_STATUS_END = "4";
    /**活动状态-已取消*/
    //public static final String ACTIVE_STATUS_CANCEL = "5";
    /**活动是否直播*/
    public static final String ACTIVE_FLAG_PLAY_LIVE="ACTIVE_FLAG_PLAY_LIVE";
    /**活动是否直播-是*/
    public static final String ACTIVE_FLAG_PLAY_LIVE_IS="1";
    public static final String ACTIVE_FLAG_PLAY_LIVE_IS_CH="是";
    /**活动是否直播-否*/
    public static final String ACTIVE_FLAG_PLAY_LIVE_NOT="2";
    public static final String ACTIVE_FLAG_PLAY_LIVE_NOT_CH="否";
    /**报名标识：0否，1是*/
    public static final String FLAG_ENTRY_NOT="0";
    public static final String FLAG_ENTRY_IS="1";
    /**签到标识：0否，1是*/
    public static final String FLAG_SIGN_NOT="0";
    public static final String FLAG_SIGN_IS="1";
    /**是否显示用户赠送道具时填写的信息（0:显示，1:不显示）*/
    public static final String FLAG_SHOW_MSG_IS="0";
    public static final String FLAG_SHOW_MSG_NOT="1";
}