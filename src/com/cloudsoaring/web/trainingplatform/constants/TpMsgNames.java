package com.cloudsoaring.web.trainingplatform.constants;

public interface TpMsgNames {
	/**用户名不存在*/
	public static final String NAME_IS_NOT_EXIST = "msg.e.login.noUserName";
	/**密码错误*/
	public static final String ERROR_PASSWORD = "msg.e.login.passwordError";
	/**系统错误*/
	public static final String SYS_ERROR = "msg.e.sysError";

	public static final String NULL_USER_ID = "msg.e.userIdNull";
	/**认证为老师/取消老师认证*/
	public static final String MSG_TEACHER_UNTEACHER = "teacher.unteacher";
	/**认证为管理员/取消管理员认证*/
	public static final String MSG_ADMINISTRATOR_UNADMINISTRATOR = "administrator.unadministrator";
	/**认证为管理员/取消管理员认证*/
	public static final String MSG_MANAGER_UNMANAGER = "manager.unmanager";
	/**用户尚未收藏或加入课程*/
	public static final String MSG_DISS_COURSE = "msg.e.course.diss";
	
	/**新增分享成功!*/
	public static final String MSG_ADD_SHARE_SUCCESS = "msg.add.share.success";
	/**标题不可为空!*/
	public static final String TITLE_CANNOT_ENPTY = "msg.title.cannot.empty";
	/**内容不可为空!*/
	public static final String CONTENT_CANNOT_ENPTY = "msg.content.cannot.empty";
	/**问题之间没有进行排序，请排序后再进行统计！*/
	public static final String MSG_E_QUESTION_NOT_ORDER = "msg.e.questionNotOrder";
	/**问题的各个选项之间没有排序，请排序后再进行统计！*/
	public static final String MSG_E_QUESTION_OPTION_NOT_ORDER = "msg.e.questionOptionNotOrder";
	/**章节信息不为空**/
	public static final String MSG_E_CHAPTER_ID_NULL="msg.e.changerIdNull";
	/**课程信息不为空**/
	public static final String MSG_E_COURSE_ID_NULL="msg.e.courseIdNull";
	/**问题列表为空**/
	public static final String MSG_E_QUESTION_INFO_NULL="msg.e.questionInfoNull";
	/**问题中不包含测试ID**/
	public static final String MSG_E_QUESTION_INFO_NULLMEASUERMENT_ID="msg.e.questionInfoNullmeasuermentId";
	/**确定删除选中的问卷？**/
	public static final String CENTER_SURE_DELETE_QUESTION_LIST="msg.centerSureDelQuestionList";

	/**公告管理*/
	public static final String NOTICE_LIST = "notice.list";
	/**添加公告*/
	public static final String NOTICE_ADD ="notice.add";
	/**编辑公告*/
	public static final String NOTICE_EDIT ="notice.edit";
	/**公告分类管理*/
	public static final String NOTICE_CATEGORY_LIST = "notice.category.list";
	/**添加分类*/
	public static final String NOTICE_CATEGORY_ADD = "notice.category.add";
	/**编辑分类*/
	public static final String NOTICE_CATEGORY_EDIT = "notice.category.edit";
	/**首页推荐,取消首页推荐*/
	public static final String MSG_HOMEPUSH_UNHOMEPUSH = "homepush.unhomepush";
	/**首页推荐*/
	public static final String HOMEPUSH_CATEGORY = "homepush.category";
	/**取消首页推荐*/
	public static final String UNHOMEPUSH_CATEGORY = "unhomepush.category";
	/**首页推荐成功*/
	public static final String HOMEPUSH_CATEGORY_SUCCESS  = "homepush.category.success";
	/**取消首页推荐成功*/
	public static final String UNHOMEPUSH_CATEGORY_SUCCESS  = "unhomepush.category.success";
	/**新增公告成功!*/
	public static final String MSG_ADD_NOTICE_SUCCESS = "msg.add.notice.success";
	public static final String MSG_ADD_GRADE_SUCCESS = "msg.add.grade.success";
	/**新增公告分类成功!*/
	public static final String MSG_ADD_NOTICE_CATEGORY_SUCCESS = "msg.add.notice.category.success";
	/**发布,取消发布*/
	public static final String MSG_RELEASE_UNRELEASE ="release.unrelease";
	/**发布公告*/
	public static final String RELEASE_NOTICE="release.notice";
	/**取消发布公告*/
	public static final String UNRELEASE_NOTICE="unrelease.notice";
	/**发布分享*/
	public static final String RELEASE_SHARE="release.share";
	/**取消发布分享*/
	public static final String UNRELEASE_SHARE="unrelease.share";
	/**发布成功*/
	public static final String STATUS_NOTICE_SUCCESS ="status.notice.success";
	/**取消发布成功*/
	public static final String STATUS_NOT_SUCCESS ="status.not.success";
	/**数据已发布*/
	public static final String STATUS_NOTICE_OVER ="status.notice.over";
	/**删除公告成功*/
	public static final String DELETE_NOTICE_SUCCESS="delete.notice.success";
	public static final String DELETE_GRADE_SUCCESS="delete.grade.success";
	/**删除分类成功*/
	public static final String DELETE_CATEGORY_SUCCESS="delete.category.success";
	/**公告已删除*/
	public static final String DELETE_NOTICE_OVER = "delete.notice.over";
	/**分类已删除*/
	public static final String DELETE_CATEGORY_OVER = "delete.category.over";
	/**公告修改成功*/
	public static final String EDIT_NOTICE_SUCCESS = "edit.notice.success";
	public static final String EDIT_GRADE_SUCCESS = "edit.grade.success";
	/**公告不存在*/
	public static final String MSG_NOTICE_IS_NULL ="msg.notice.is.null";
	/**公告分类不存在*/
	public static final String MSG_NOTICE_CATEGORY_IS_NULL ="msg.notice.category.is.null";
	/**该分类下尚有公告存在,不允许删除*/
	public static final String MSG_HAVE_NOTICE_NOT_DELETE ="msg.have.notice.not.delete";
	/**分享不存在*/
	public static final String MSG_SHARE_IS_NULL ="msg.share.is.null";
	/**公告内容不能为空*/
	public static final String CONTENT_CANNOT_EMPTY="content.cannot.empty";

	/**用户名为空*/
	public static final String MSG_E_LOGIN_NULL_USER_NAME = "msg.e.login.nullusername";
	/**密码为空*/
	public static final String MSG_E_LOGIN_NULL_PASSWORD = "msg.e.login.nullpassword";
	/**ID为空*/
	public static final String MSG_E_NULL_ID = "msg.e.nullid";
	/**游客状态不允许加入课程*/
	public static final String MSG_E_GUEST_JOIN_COURSE = "msg.e.guestjoincourse";
	/**数据不存在*/
	public static final String MSG_E_NULL_DATA = "msg.e.nulldata";
	/**课程未发布*/
	public static final String MSG_E_NOT_PUBLIC = "msg.e.notpublic";
	/**课程不存在*/
	public static final String MSG_E_COURSE_NOT_EXIST = "msg.e.coursenotexist";
	/**章节不存在*/
	public static final String MSG_E_CHAPTER_NOT_EXIST = "msg.e.chapternotexist";
	/**分享不存在*/
	public static final String MSG_E_SHARE_NOT_EXIST = "msg.e.sharenotexist";
	/**用户章节不存在*/
	public static final String MSG_E_USER_CHAPTER_NOT_EXIST = "msg.e.userchapternotexist";
	/***用户不存在*/
	public static final String USER_NOT_EXIST = "msg.e.usernotexist";

	/**欢迎登录后台管理*/
	public static final String TITLE_WELCOME_MANAGER = "title.welcome.manager";
	/**欢迎登录理*/
	public static final String TITLE_WELCOME = "title.welcome";
	/**请登录*/
	public static final String TXT_PLEASE_LOGIN = "txt.please.login";
	/**请上传*/
	public static final String TXT_PLEASE_UPLOAD = "txt.please.upload";
	/**下载模板*/
	public static final String TXT_DOWNLOAD_TEMP = "txt.download.temp";
	/**上传*/
	public static final String TXT_UPLOAD = "txt.upload";
	/**请选择要上传的文件*/
	public static final String TXT_SELECT_FILE = "txt.select.file";

	/** 导入结果消息*/
	public static final String MSG_USER_IMPORT = "msg.user.import";
	public static final String MSG_COURSE_IMPORT = "msg.course.import";
	/**标题用户导入*/
	public static final String TITLE_IMPORT_USER = "title.import.user";
	/**用户没有登录,请先登录!*/
	public static final String USER_NOT_LOGING = "user.not.loging";
	/**删除失败!用户不可以删除自己!*/
	public static final String CAN_NOT_DELETE_YOURSELF = "user.cannot.delete.yourself";
	/**认证为老师成功!*/
	public static final String SUCCESS_IDENTIFY_TO_TEACHER = "msg.success.to.teacher";
	/**取消认证为老师成功!*/
	public static final String SUCCESS_IDENTIFY_TO_UNTEACHER = "msg.success.to.unteacher";
	/**认证为管理员成功!*/
	public static final String SUCCESS_IDENTIFY_TO_ADMINISTRATOR = "msg.success.to.administrator";
	/**取消认证为管理员成功!*/
	public static final String SUCCESS_IDENTIFY_TO_UNADMINISTRATOR = "msg.success.to.unadministrator";
	
	/**认证为后台用户成功!*/
	public static final String SUCCESS_IDENTIFY_TO_MANAGER = "msg.success.to.manager";
	/**取消认证为后台用户成功!*/
	public static final String SUCCESS_IDENTIFY_TO_UNMANAGER = "msg.success.to.unmanager";
	
	/**分享标题*/
	public static final String MSG_TITLE = "title";/**单位*/
	public static final String MSG_WORKPLACE = "workplace";
	
	public static final String MSG_SEX = "sex";
	/**职位*/
	public static final String MSG_POSITION = "position";
	/**地址*/
	public static final String MSG_REGION = "region";
	/**微信*/
	public static final String MSG_WEIXIN = "weixin";
	/**微博*/
	public static final String MSG_WEIBO = "weibo";
	/**描述*/
	public static final String MSG_DESCRIPTION = "description";
	/**用户导入*/
	public static final String MSG_IMPORT = "user.import";
	
	/**讨论分类*/
	public static final String MSG_LINK_TYPE = "discuss.type";
	/**标题/内容*/
	public static final String MSG_TITLE_CONTENT = "discuss.title.content";
	/**标题*/
	public static final String MSG_DISCUSS_TITLE = "discuss.title";
	/**用户名(姓名)*/
	public static final String MSG_USER_AND_PERSON_NAME = "discuss.user.person.name";
	/**评论时间*/
	public static final String MSG_CREATE_DATE = "discuss.create.date";
	/**内容*/
	public static final String MSG_CONTENT = "discuss.content";
	
	/**确定删除该条评论?*/
	public static final String MSG_DELETE_DISCUSS_DATA = "delete.discuss.data";
	/**请选择删除数据*/
	public static final String MSG_SELECT_DELETE_DATA = "select.delete.data";
	/**确定要认证{0}为老师*/
	public static final String MSG_CONFIRM_TO_TEACHER = "confirm.to.teacher";
	/**确定要取消认证{0}为老师*/
	public static final String MSG_CONFIRM_TO_UNTEACHER = "confirm.to.unteacher";
	
	
	/**确定要认证{0}为后台用户*/
	public static final String MSG_CONFIRM_TO_MANAGER = "confirm.to.manager";
	/**确定要取消{0}的后台用户认证*/
	public static final String MSG_CONFIRM_TO_UNMANAGER = "confirm.to.unmanager";
	
	/**确定要认证{0}为管理员*/
	public static final String MSG_CONFIRM_TO_ADMINISTRATOR = "confirm.to.administrator";
	/**确定要取消认证{0}为管理员*/
	public static final String MSG_CONFIRM_TO_UNADMINISTRATOR = "confirm.to.unadministrator";
	/**确定删除?*/
	public static final String SURE_TO_DELETE="sure.to.delete";
	/**删除成功*/
	public static final String DELETE_SUCCESS = "delete.success";
	/**编号*/
	public static final String MSG_NO = "number";
	/**状态*/
	public static final String MSG_STATUS ="status";
	/**是否首推*/
	public static final String MSG_IS_HOMEPUSH = "msg.is.homepush";
	/**公告标题*/
	public static final String NOTICE_TITLE = "notice.title";
	/**所属分类*/
	public static final String CATEGORY_DEPART = "categoru.depart";
	/**排序*/
	public static final String SORT_ORDER = "sort.order";
	/**分类描述*/
	public static final String CATEGORY_DESCRIPTION = "category.description";
	/**内容编辑*/
	public static final String CONTENT_EDIT = "content.edit";
	/**图片*/
	public static final String MSG_PICTURE = "msg.picture";
	/**添加视频*/
	public static final String MSG_ADD_VIDEO = "video.add";
	/**发送(视频)*/
	public static final String MSG_SEND_VIDEO = "video.send";
	/**名称:*/
	public static final String MSG_DISCUSS_NAME = "discuss.name";
	/**简介:*/
	public static final String MSG_DISCUSS_ABSTRACT = "discuss.abstract";
	/**简介:*/
	public static final String MSG_UPLOAD_SHARE = "upload.share";
	/**全部*/
	public static final String MSG_ALL_CATEGORY = "msg.all.category";
	/**返回课程*/
	public static final String BACK_COURSE = "course.back";


	//个人中心
	/**个人中心*/
	public static final String PERSONAL_CENTER="center.PersonalCenter";
	/**个人信息*/
	public static final String PERSONAL_INFORMATION="center.PersonalInformation";
	/**参与的课程*/
	public static final String PARTICIPATING_IN_THE_COURSE="center.ParticipatingInTheCourse";
	/**收藏的课程*/
	public static final String COLLECTION_IN_THE_COURSE="center.CollectionInTheCourse";
	/**发表过的讨论*/
	public static final String PUBLISHED_DISCUSSION="center.PublishedDiscussion";
	/**参与过的测试*/
	public static final String PARTICIPATED_IN_THE_TEST="center.ParticipatedInTheTest";
	/**我的分享*/
	public static final String MY_SHARE="center.MyShare";
	/**开课时间*/
	public static final String START_TIME="center.StartTime";
	/**报名人数*/
	public static final String REGISTRATION_NUMBER="center.RegistrationNumber";
	/**名称*/
	public static final String CENTER_NAME="center.name";
	/**简介*/
	public static final String CENTER_MOMO="center.momo";
	/**头像*/
	public static final String CENTER_IMAGE="center.image";
	/**保存成功*/
	public static final String CENTER_SAVE_SUCCESS="center.save_success";
	/**标题*/
	public static final String CENTER_TITLE="center.title";
	/**课程标题*/
	public static final String CENTER_COURSE_TITLE="center.coursetitle";
	/**封面*/
	public static final String CENTER_COVER_IMAGE="center.coverImage";
	/**封面简介*/
	public static final String CENTER_COVER="center.cover";
	/**分类*/
	public static final String CENTER_CLASSIFICATION="center.classification";
	/**课程期限*/
	public static final String CENTER_COURSE_PERIOD="center.coursePeriod";
	/**学习时长*/
	public static final String CENTER_LEARNING_TIME="center.learningTime";
	/**课程状态*/
	public static final String CENTER_COURSE_STATE="center.courseState";
	/**精品课程*/
	public static final String CENTER_GOOD_COURSE = "center.goodcourse";
	/**首页轮播*/
	public static final String CENTER_HOME_BANNER = "center.homeBanner";
	public static final String RECOMMEND_GOOD_COURSE="center.recommendGoodCourse";
	public static final String CANCEL_GOOD_COURSE="center.cancelGoodCourse";
	public static final String RECOMMEND_HOME_BANNER="center.recommendHomeBanner";
	public static final String CANCEL_HOME_BANNER="center.cancelHomeBanner";
	public static final String RECOMMEND="center.recommend";
	public static final String NOT_RECOMMEND="center.notRecommend";
	/**课程内容*/
	public static final String CENTER_COURSE_CONTENT="center.courseContent";
	/**题目*/
	public static final String CENTER_TOPIC="center.topic";
	/**添加*/
	public static final String CENTER_ADD_TO="center.addTo";
	/**删除*/
	public static final String CENTER_DELETE="center.delete";
	/**问题*/
	public static final String CENTER_THE_PROBLEM="center.theProblem";
	/**请输入题目*/
	public static final String CENTER_PLEASE_ENTER_A_QUESTION="center.pleaseEnterAQuestion";
	/**正确选项*/
	public static final String CENTER_CORRECT_OPTION="center.correctOption";
	/**A选项*/
	public static final String CENTER_A_OPTION="center.aOption";
	/**B选项*/
	public static final String CENTER_B_OPTION="center.bOption";
	/**C选项*/
	public static final String CENTER_C_OPTION="center.cOption";
	/**D选项*/
	public static final String CENTER_D_OPTION="center.dOption";
	/**最后的问题不能删除*/
	public static final String CENTER_LAST_QUESTION_NOT_DELETED="center.lastQuestionNotDeleted";
	/**日期*/
	public static final String CENTER_DATE="center.date";
	/**状态*/
	public static final String CENTER_STATE="center.state";
	/**编号*/
	public static final String CENTER_NO="center.no";
	/**编辑*/
	public static final String CENTER_EDIT="center.edit";
	/**发布*/
	public static final String CENTER_RELEASE="center.release";
	/**发布*/
	public static final String CENTER_NOT_RELEASE="center.notRelease";
	/**添加章节*/
	public static final String CENTER_ADD_CHAPTER="center.addChapter";
	/**上移*/
	public static final String CENTER_MOVE="center.move";
	/**下移*/
	public static final String CENTER_DOWN="center.down";
	/**导出考试数据*/
	public static final String EXPORT_EXAM_DATA="export.exam.data";
	/**章节*/
	public static final String CENTER_CHAPTER="center.chapter";
	/**是否确认删除当前课程？*/
	public static final String CENTER_SURE_DELETE_COURSE="center.sureDeleteCourse";
	/**课程管理*/
	public static final String CENTER_COURSE_MANAGEMENT="center.courseManagement";
	/**添加课程*/
	public static final String CENTER_ADD_COURSES="center.addCourses";
	/**编辑课程*/
	public static final String CENTER_EDITING_COURSE="center.editingCourse";
	/**大章节*/
	public static final String CENTER_LARGE_SECTIONS="center.LargeSections";
	/**章节内容*/
	public static final String CENTER_CHAPTERS="center.Chapters";
	/**编辑章节*/
	public static final String CENTER_EDIT_CHAPTER="center.EditChapter";
	/**是*/
	public static final String CENTER_YES="center.yes";
	/**否*/
	public static final String CENTER_NOT="center.not";
	public static final String CENTER_CONFIRM_PUSH_HOME="center.confirmPushHome";
	public static final String CENTER_CONFIRM_CANCEL_PUSH_HOME="center.confirmCancelPushHome";
	public static final String CENTER_CONFIRM_PUSH_HOME_BANNER="center.confirmPushHomeBanner";
	public static final String CENTER_CONFIRM_CANCEL_PUSH_HOME_BANNER="center.confirmCancelPushHomeBanner";
	public static final String CENTER_CONFIRM_PUBLIC="center.confirmPublic";
	public static final String CENTER_CONFIRM_NOT_PUBLIC="center.confirmNotPublic";

	
	/**用户未登录*/
	public static final String USER_NOT_LOGIN="user.notLogin";
	/**信息不能为空*/
	public static final String INFORMATION_CANNOT_EMPTY="information.cannot.empty";
	/**标签名已存在*/
	public static final String TAG_NAME_EXITS="tag.name.exits";
	/**删除失败!该分类下存在课程,不可删除!*/
	public static final String COURSE_EXITS="course.exits";
	/**确认删除标签  */
	public static final String DELETE_TAG="delete.tag";
	/**新增标签  */
	public static final String ADD_TAG="add.tag";
	/**新增  */
	public static final String TAG_DELETE="tag.delete";
	/**编辑  */
	public static final String TAG_EDIT="tag.edit";
	/**标签  */
	public static final String MSG_TAG="msg.tag";
	/**标签名*/
	public static final String TAG_NAME="tag.name";
	/**标签值*/
	public static final String TAG_VALUE="tag.value";
	/**保存*/
	public static final String TAG_SAVE="tag.save";
	/**关闭*/
	public static final String TAG_CLOSE="tag.close";
	/**关闭*/
	public static final String COURSES_TAG="course.tag";


	
	/**平台信息*/
	public static final String INDEX_INFO_NAME = "indexinfo.name";
	/**课程*/
	public static final String COURSE_NAME = "course.name";
	/**分享*/
	public static final String SHARE_NAME = "share.name";
	/**个人中心*/
	public static final String PERSON_CENTER = "person.center";
	/**全部*/
	public static final String INDEX_INFO_ALL = "indexinfo.all";
	/**教学平台-平台信息*/
	public static final String INDEX_INFO_TITLE = "indexinfo.title";
	/**教学平台-平台信息详情*/
	public static final String INDEX_DETAIL_TITLE = "infodetail.title";
	/**开课时间*/
	public static final String COURSE_START = "course.start";
	/**简介*/
	public static final String COURSE_SHORT_CONTENT = "course.shortcontent";
	/**报名人数*/
	public static final String COURSE_USER_NUM = "course.usernum";
	/**人*/
	public static final String COURSE_PERSON = "course.person";
	/**教学平台-课程*/
	public static final String COURSE_TITLE = "course.title";
	/**教学平台-课程详情*/
	public static final String COURSE_DETAIL_TITLE = "course.detailtitle";
	/**课程描述*/
	public static final String COURSE_DESC = "course.desc";
	/**查看全部*/
	public static final String COURSE_LOOK_ALL = "course.lookall";
	/**至*/
	public static final String COURSE_TO = "course.to";
	/**课程进度*/
	public static final String COURSE_STATUS = "course.status";
	/**加入课程*/
	public static final String COURSE_JOIN = "course.join";
	/**已加入课程*/
	public static final String COURSE_JOIN_ALREADY = "course.joinalready";
	/**收藏课程*/
	public static final String COURSE_SAVE = "course.save";
	/**取消收藏*/
	public static final String COURSE_DIS_SAVE = "course.dissave";
	/**授课教师*/
	public static final String COURSE_BY_TEACHER = "course.teacher";
	/**课程简介*/
	public static final String COURSE_MOMO = "course.momo";
	/**课程章节*/
	public static final String COURSE_CHAPTER = "course.chapter";
	/**讨论区*/
	public static final String COURSE_DISCUSS = "course.discuss";
	/**测试*/
	public static final String COURSE_MEASUREMENT = "course.measurement";
	/**投票*/
	public static final String COURSE_VOTE = "course.vote";
	/**请输入评论内容*/
	public static final String COURSE_PUTIN_DISCUSS = "course.putin";
	/**发送*/
	public static final String COURSE_SEND = "course.send";
	/**加载更多*/
	public static final String COURSE_GET_MORE = "course.getmore";
	/**相关课程*/
	public static final String COURSE_RELEVANT = "course.relevant";
	/**对我很有帮助，都是干货*/
	public static final String COURSE_VOTE_GOOD = "course.votegood";
	/**还行，比较一般*/
	public static final String COURSE_VOTE_NORMAL = "course.votenormal";
	/**几乎没什么帮助*/
	public static final String COURSE_VOTE_BAD = "course.votebad";
	/**我要投票*/
	public static final String COURSE_MY_VOTE = "course.myvote";
	/**加入成功！*/
	public static final String COURSE_JOIN_SUCCESS = "course.joinsuccess";
	/**尚未发布章节*/
	public static final String COURSE_NOT_CHAPTER = "course.notchapter";
	/**游客*/
	public static final String COURSE_GUEST = "course.guest";
	/**没有讨论记录*/
	public static final String COURSE_NO_DISCUSS = "course.nodiscuss";
	/**输入评论内容超过限定字符*/
	public static final String COURSE_INPUT_MOST = "course.inputmost";
	/**显示答案*/
	public static final String COURSE_SHOW_ANSWER = "course.showanswer";
	/**重新测试*/
	public static final String COURSE_RE_MEASUREMENT = "course.remeasurement";
	/**提交测试*/
	public static final String COURSE_SUBMIT_MEASUREMENT = "course.submitmeasurement";
	/**收藏成功！*/
	public static final String COURSE_SAVE_SUCCESS = "course.savesuccess";
	/**取消成功！*/
	public static final String COURSE_DIS_SAVE_SUCCESS = "course.dissavesuccess";
	/**教学平台-章节详情*/
	public static final String COURSE_CHAPTER_DETAIL = "course.chapterdetail";
	/**请选择投票选项*/
	public static final String COURSE_SELECT_VOTE = "course.selectvote";
	/**尚未选择答案，请答题后提交！*/
	public static final String COURSE_SUBMIT_MESUREMENT_ERROR = "course.submitmesurementerror";
	/**黑体*/
	public static final String COURSE_FONT_BLACK_COLOR = "course.blackfontcolor";
	public static final String CENT_EXAM_NAME="center.examName";
	public static final String CENT_EXAM_REMARK="center.examRemark";
	public static final String CENT_EXAM_START_END="center.examStartEnd";
	public static final String CENT_ADD_QUESTION="center.addQuestion";
	public static final String CENT_ADD_QUESTION_CHAPTER="center.addQuestionChapter";
	public static final String CENT_EXAM_EXAM_ADD="center.examAdd";
	public static final String CENT_EXAM_EXAM_EDIT="center.examEdit";
	/**是否考试*/
	public static final String CENT_IS_MEASURE = "center.isMeasure";
	//对
	public static final String CENT_OPTION_RIGHT = "center.option.right";
	//错
	public static final String CENT_OPTION_ERROR = "center.option.error";
	/**用户角色*/
	public static final String TP_USER_ROLE = "user.role";

	/**暂无数据*/
	public static final String NO_DATA = "info.nodata";
	/**考试结束时间应该大于等于考试开始时间*/
	public static final String END_DATA_AFTER_START_DATE = "endDate.after.startDate";

	/**考试开始时间*/
	public static final String MEASURE_START_DATE = "measureStartDate";
	/**考试结束时间*/
	public static final String MEASURE_END_DATE = "measureEndDate";
	/**考试时长*/
	public static final String MEASURE_DURATION = "measureDuration";



	//分享详情
	/**分享详情*/
	public static final String SHARE_DETAIL_TITLE = "share.detail.title";
	public static final String DISCUSSION_AREA = "discussion.area";
	public static final String MSG_SEND = "msg.send";
	public static final String LOADING_MORE = "loading.more";
	public static final String SHARE_DESCRIBE = "share.describe";
	public static final String LOOK_ALL_SHARE = "look.all.share";
	public static final String UPLOAD_TIME = "upload.time";
	public static final String UPLOAD_PERSON = "upload.person";
	public static final String PLEASE_WRITE_DESCRIBE = "please.write.describe";
	
	
	/**首页*/
	public static final String FIRST_PAGE = "page.firstpage";
	/**下一页*/
	public static final String NEXT_PAGE = "page.nextpage";
	/**上一页*/
	public static final String PRE_PAGE = "page.prepage";
	/**尾页*/
	public static final String LAST_PAGE = "page.lastpage";
	/**手机客户端*/
	public static final String MOBILE_CLIENT = "mobile.client";
	
	/**没有测试*/
	public static final String NO_MEASUREMENT = "course.nomeasurement";
	/**单选题*/
	public static final String SIGLE_SELECT = "course.singleselect";
	/**多选题*/
	public static final String MORE_SELECT = "course.moreselect";
	/**判断题*/
	public static final String JUDGMENT = "course.judgment";
	/**提交成功*/
	public static final String SUBMIT_OK = "course.submitok";
	/**倒计时*/
	public static final String COUNT_DOWN = "course.countdown";
	/**时*/
	public static final String HOUR = "course.hour";
	/**分*/
	public static final String MINS = "course.mins";
	/**秒*/
	public static final String SECONDS = "course.seconds";
	/**距考试结束还有10分钟*/
	public static final String LAST_TEN_MINS = "course.lasttenmins";
	/**距考试结束还有1分钟，请尽快提交*/
	public static final String LAST_ONE_MINS = "course.lastonemins";
	/**考试结束*/
	public static final String EXAM_OVER = "course.examover";
	/**没有考试*/
	public static final String NO_EXAM = "course.noexam";
	/**本次考试共*/
	public static final String TOTAL_QUESTION = "course.totalquestion";
	/**题*/
	public static final String TI = "course.ti";
	/**答对*/
	public static final String ANSWER_RIGHT = "course.answerright";
	/**考试*/
	public static final String EXAM = "course.exam";
	/**开始考试*/
	public static final String START_EXAM = "course.startexam";
	/**提交*/
	public static final String SUBMIT = "course.submit";
	/**尚未登录，请登录后开始考试*/
	public static final String NO_LOGIN_TO_LOGIN = "course.nologintologin";
	/**加入考试*/
	public static final String JOIN_EXAM = "course.joinexam";
	/**已加入考试*/
	public static final String ALREADY_JOIN_EXAM = "course.alreadyjoinexam";
	/**收藏考试*/
	public static final String SAVE_EXAM = "course.saveexam";
	/**考试描述*/
	public static final String EXAM_DESC = "course.examdesc";
	/**考试简介*/
	public static final String EXAM_CONTENT = "course.examcontent";
	/**考试章节*/
	public static final String EXAM_CHAPTER = "course.examchapter";
	/**考试时间*/
	public static final String EXAM_TIME = "exam.time";
	/**考试进度*/
	public static final String EXAM_STATUS = "exam.status";
	/**未开始*/
	public static final String NO_START = "exam.nostart";
	/**进行中*/
	public static final String EXAMING = "exam.examing";
	/**已结束*/
	public static final String EXAM_END = "exam.examend";
	/**考试总时长*/
	public static final String EXAM_TOTAL_TIME = "exam.totaltime";
	/**剩余考试时长*/
	public static final String EXAM_HAS_TIME = "exam.hastime";
	/**已答/总题数*/
	public static final String EXAM_SELECT_NUM = "exam.selectnum";
	/**考*/
	public static final String EXAM_KAO = "exam.kao";
	/**您使用的浏览器版本较低，建议您升级到*/
	public static final String LOW_BROWSER_VERSION = "browser.lowversion";
	/**Internet Explorer 9 */
	public static final String IE_9 = "browser.ie9";
	/**及以上版本*/
	public static final String FLOOR_BROWSER_VERSION = "browser.floorbrowserversion";
	/**关闭*/
	public static final String BROWSER_TIP_CLOSE = "browser.closetip";
	/**消息ID为空*/
	public static final String MSG_E_EMPTY_MESSAGE_ID = "msg.e.empty.message.id";
	/**机构修改*/
	public static final String EDIT_INSTITUTION_SUCCESS = "edit.institution.success";
	/**机构删除*/
	public static final String DELETE_INSTITUTION_SUCCESS = "delete.institution.success";
}
