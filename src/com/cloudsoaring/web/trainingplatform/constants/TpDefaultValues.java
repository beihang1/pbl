package com.cloudsoaring.web.trainingplatform.constants;

public interface TpDefaultValues {

	/** 数字1 */
	public static final Long DATA_ONE = 1L;
	
	/**平台信息，图片轮播，排序字段*/
	public static final String SORT_NAME="SORT_ORDER";
	/**排序方式asc*/
	public static final String SORT_ASC = "asc";
	public static final String DESC = "DESC";
	/**课程列表检索排序*/
	public static final String COURSE_SORT_NAME = "START_DATE";
	/**平台信息检索字段*/
	public static final String INFO_SORT_NAME = "CREATE_DATE";
	
	/**个人中心-我的消息-每页显示多少条记录*/
	public static final Integer DEFAULT_MY_MESSAGES_PAGE_SIZE = 20;
	/**老师角色*/
	public static final String ROLE_TEACHER_ROLE = "04";
}
