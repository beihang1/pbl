package com.cloudsoaring.web.trainingplatform.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.TagEntity;
import com.cloudsoaring.web.bus.entity.UserEntity;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.entity.ManagerUserEntity;
import com.cloudsoaring.web.course.service.CourseService;
import com.cloudsoaring.web.course.service.ManagerUserService;
import com.cloudsoaring.web.course.service.TagService;
import com.cloudsoaring.web.course.view.TagView;
import com.cloudsoaring.web.trainingplatform.constants.TpConstants;
import com.cloudsoaring.web.trainingplatform.entity.ActivityCenterEntity;
import com.cloudsoaring.web.trainingplatform.entity.ActivityCenterUserEntity;
import com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity;
import com.cloudsoaring.web.trainingplatform.service.ActivityCenterService;

@Controller
@RequestMapping("/pub/activityCenter")
public class ActivityCenterManageController extends BaseController implements TpConstants{
	@Autowired
	private ActivityCenterService activityCenterService;
	@Autowired
	private CourseService courseService;
	@Autowired
	private ManagerUserService managerUserService;
	@Autowired
	private TagService tagService;
	/**
	 * 跳转到公告管理页面
	 * @return 返回结果
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping("/noticelist.action")
	public ModelAndView noticeList(TagView view){
		List categorys = activityCenterService.searchNoticeTag(view);
		ModelAndView mv = new ModelAndView("activityCenter/manager/activityCenter_list");
		mv.addObject("categorys", categorys);
		mv = getRoleAndNum(mv);
		return mv;
	}
	
	/**
	 * 跳转到公告添加页面
	 * @return 返回结果
	 */
	
	@SuppressWarnings("rawtypes")
	@RequestMapping("/noticeadd.action")
	public ModelAndView noticeAdd(TagView view){
		List categorys = activityCenterService.searchNoticeTag(view);
		ModelAndView mv = new ModelAndView("activityCenter/manager/activityCenter_add");
		mv.addObject("categorys", categorys);
		mv = getRoleAndNum(mv);
		return mv;
	}
	/**
	 * 跳转到公告编辑页面
	 * @param id
	 * @return 返回结果
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping("/noticeedit.action")
	public ModelAndView noticeEdit(String id){
		List categorys = activityCenterService.searchNoticeTag(new TagView());
		ActivityCenterEntity entity = new ActivityCenterEntity();
		entity.setId(id);
		ActivityCenterEntity condition = activityCenterService.selectNotice(entity);
		ModelAndView mv = new ModelAndView("activityCenter/manager/activityCenter_edit");
		mv.addObject("categorys", categorys);
		mv.addObject("notice", condition);
		mv = getRoleAndNum(mv);
		return mv;
	}
	/**
	 * 检索公告分类信息
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchcategory.action")
	@ResponseBody
	public List searchCategory(InfomationCategoryEntity entity){
		return activityCenterService.search(entity);
	}*/
	
	/**
	 * 检索公告信息
	 * 根据公告分类
	 * @param entity
	 * @return 返回结果
	 */
	@RequestMapping("/searchnotice.action")
	@ResponseBody
	public ResultBean searchNotice(ActivityCenterEntity entity){
		return activityCenterService.searchNotice(entity);
	}
	/**
	 * 新增公告信息
	 * @param entity
	 * @return 返回结果
	 */
	@RequestMapping("/insertnotice.action")
	@ResponseBody
	public ResultBean insertNotice(ActivityCenterEntity entity) throws Exception {
		entity.setId(IDGenerator.genUID());
		if(StringUtil.isEmpty(entity.getContent())){
			return ResultBean.error(CONTENT_CANNOT_EMPTY);
		}
		activityCenterService.insertNotice(entity);
		
		TagView view = new TagView();
		view.setTagId(entity.getCategoryId());
		view.setLinkId(entity.getId());
		view.setLinkType(NOTICE_LINK_TYPE);
		
		activityCenterService.insertNoticeType(view);
		return ResultBean.success(MSG_ADD_NOTICE_SUCCESS);
	}
	/**
	 * 编辑保存
	 * @param entity
	 * @return 返回结果
	 */
	@RequestMapping("/updatenotice.action")
	@ResponseBody
	public ResultBean updateNotice(ActivityCenterEntity entity) throws Exception{
		if(StringUtil.isEmpty(entity.getContent())){
			return ResultBean.error(CONTENT_CANNOT_EMPTY);
		}
		activityCenterService.editNotice(entity);
		
		TagView view = new TagView();
		view.setTagId(entity.getCategoryId());
		view.setLinkId(entity.getId());
		view.setLinkType(NOTICE_LINK_TYPE);
		
		activityCenterService.editNoticeType(view);
		return ResultBean.success(EDIT_NOTICE_SUCCESS);
	}
	/**
	 * 发布公告
	 * @param entity
	 * @return 返回结果
	 */
	@RequestMapping("/statusinfo.action")
	@ResponseBody
	public ResultBean isStatus(ActivityCenterEntity entity){
		return 	activityCenterService.isStatus(entity);
	}
	/**
	 * 删除公告
	 * @param entity
	 * @return 返回结果
	 */
	@RequestMapping("/deleteinfo.action")
	@ResponseBody
	public ResultBean deleteInfo(ActivityCenterEntity entity)throws Exception{
		activityCenterService.deleteInfo(entity);
		return ResultBean.success(DELETE_NOTICE_SUCCESS);
	}
	
	@RequestMapping("/toIndex.action")
	public ModelAndView toIndex(String categoryId){
		ModelAndView mv = new ModelAndView();
		mv.setViewName("activityCenter/client/activityCenter_index");
		mv.addObject("categoryId", categoryId);
		TagEntity tags = tagService.searchOneTag(categoryId);
		mv.addObject("categoryName", tags.getTagName());
		int num = courseService.getStuStatus();
		mv.addObject("num", num);
		UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}
	
	@RequestMapping("/searchInfoByCatId.action")
	@ResponseBody
	public ResultBean searchInfoByCategory(ActivityCenterEntity info){
		ResultBean result = new ResultBean();
		info.setSortName(INFO_SORT_NAME);
		info.setSortOrder(DESC);
		info.setStatus(INFOMATION_STATUS_ALREADY);
		UserEntity user = WebContext.getSessionUser();
    	//判断是否管理员
    	ManagerUserEntity entity = managerUserService.searchByUserId(user.getUserId());
    	if(null!=entity) {
    		 result = activityCenterService.searchInfoByCategoryId(info);
    	}else {
    		if("A".equals(user.getUserKbn())) {
    			 result = activityCenterService.searchTeacherInfoByCategoryId(info);
    		}else if("C".equals(user.getUserKbn())) {
    			 result = activityCenterService.searchStudentInfoByCategoryId(info);
    		}
    	}
		return result;
	}
	
	@RequestMapping("/searchCategory.action")
	@ResponseBody
	public ResultBean searchCategory(){
		TagView view = new TagView();
		view.setSortName(SORT_NAME);
		view.setSortOrder(SORT_ASC);
		ResultBean result = activityCenterService.searchCategory(view);
		return result;
	}
	
	@RequestMapping("/toInfoDetail.action")
	public ModelAndView toInfoDetail(String infoId){
		ModelAndView mv = new ModelAndView();
		mv.setViewName("activityCenter/client/activityCenter_infoDetail");
		mv.addObject("infoId", infoId);
		int num = courseService.getStuStatus();
		mv.addObject("num", num);
		UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				mv.addObject("role", "s");
			}
			ActivityCenterUserEntity entity = new ActivityCenterUserEntity();
			entity.setActivityCenterId(infoId);
			entity.setUserId(WebContext.getSessionUserId());
			List<ActivityCenterUserEntity> usersList = activityCenterService.getUsersList(entity);
			ActivityCenterEntity info = new ActivityCenterEntity();
			info.setId(infoId);
			ActivityCenterEntity enity = activityCenterService.searchInfoById(info);
			String showButton = "";//0：不需要报名；1：需要报名；2：已经报名
			if(enity.getIsSign().equals("0")) {
				mv.addObject("showButton", "0");
			}else if(enity.getIsSign().equals("1")) {
				mv.addObject("showButton", "1");
			}
			if(usersList.size() > 0) {
				mv.addObject("showButton", "2");
			}
		}
		return mv;
	}
	
	@RequestMapping("/searchInfoDetail.action")
	@ResponseBody
	public ResultBean searchInfoById(String infoId){
		ResultBean result = new ResultBean();
		if(infoId == null || infoId.equals("")){
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_NULL_ID));
			return result;
		}
		ActivityCenterEntity info = new ActivityCenterEntity();
		info.setId(infoId);
		info = activityCenterService.searchInfoById(info);
		if(info == null){
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_NULL_DATA));
			return result;
		}
		result.setStatus(true);
		result.setData(info);
		return result;
	}
	
	@RequestMapping("/saveUser.action")
	@ResponseBody
	public ResultBean saveUser(String activityCenterId){
		ResultBean result = new ResultBean();
		result = activityCenterService.saveUser(activityCenterId);
		return result;
	}
	
	@RequestMapping("/searchUserInit.action")
    @ResponseBody
    public ModelAndView searchUserInit(String id){
        ModelAndView result = new ModelAndView("activityCenter/manager/activityCenter_user_list");
        result.addObject("id", id);
        result = getRoleAndNum(result);
        return result;
    }
	
	@RequestMapping("/searchUser.action")
    @ResponseBody
    public ResultBean searchUser(String id)  {
        return activityCenterService.searchActivityCenterUserList(id);
    }
	
	private ModelAndView getRoleAndNum(ModelAndView result) {
		int num = courseService.getStuStatus();
    	result.addObject("num", num);
    	UserEntity user = WebContext.getSessionUser();
    	//判断是否管理员
    	ManagerUserEntity entity = managerUserService.searchByUserId(user.getUserId());
    	if(!user.getUserName().equals("guest")) {
    		List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
    		//老师和教育局角色
    		if(roleList.size() > 0 && "A".equals(user.getUserKbn()) && null==entity && !"2542668".equals(user.getUserId())) {
    			for(UserRoleEntity role : roleList) {
    				if(role.getRoleId().equals("01")) {
    					result.addObject("role", "t");
    				}
    			}
    		//教育局角色	
    		}else if(roleList.size() > 0  && (null!=entity || "2542668".equals(user.getUserId()))){
    			result.addObject("role", "m");
    		}else if("B".equals(user.getUserKbn())){
    			result.addObject("role", "e");
    	    //家长和教研员角色
    		}else if("D".equals(user.getUserKbn()) || "E".equals(user.getUserKbn())){
    			result.addObject("role", "q");
    		//学生角色
    		}else {
    			result.addObject("role", "s");
    		}
    	}

    	if(null!=entity || "2542668".equals(user.getUserId())) {
    		result.addObject("manager", "1");
    	}else {
    		result.addObject("manager", "0");
    	}
    	return result;
    }
}
