package com.cloudsoaring.web.trainingplatform.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.UserEntity;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.service.CourseService;
import com.cloudsoaring.web.course.view.TagView;
import com.cloudsoaring.web.trainingplatform.constants.TpConstants;

import com.cloudsoaring.web.trainingplatform.entity.InfomationEntity;
import com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity;
import com.cloudsoaring.web.trainingplatform.service.IndexInfoService;


/**
 * 平台信息
 * @author LIUYANSHUANG
 *
 * @version V1.0
 *
 */
@Controller
@RequestMapping("/pub/indexInfo")
public class IndexInfoController extends BaseController implements TpConstants{

	/**注入IndexInfoService*/
	@Autowired
	private IndexInfoService indexInfoService;
	@Autowired
	private CourseService courseService;
	
	///PC前端页面控制
	/**
	 * 跳转到平台信息首页
	 * @return 返回结果
	 */
	@RequestMapping("/toIndex.action")
	public ModelAndView toIndex(String categoryId){
		ModelAndView mv = new ModelAndView();
		mv.setViewName("notice/client/index");
		mv.addObject("categoryId", categoryId);
		int num = courseService.getStuStatus();
		mv.addObject("num", num);
		UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}
	
	/***
	 * 跳转到平台信息详情页
	 * @param infoId
	 * @return 返回结果
	 */
	@RequestMapping("/toInfoDetail.action")
	public ModelAndView toInfoDetail(String infoId){
		ModelAndView mv = new ModelAndView();
		mv.setViewName("notice/client/infoDetail");
		mv.addObject("infoId", infoId);
		int num = courseService.getStuStatus();
		mv.addObject("num", num);
		UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}
	
	///PC前端页面控制END
	
	/**
	 * 平台首页，检索图片轮播
	 * @return 返回结果
	 */
	@RequestMapping("/getHomeInfoList.action")
	@ResponseBody
	public ResultBean getHomeInfoList(){
		InfomationEntity info = new InfomationEntity();
		info.setSortName(SORT_NAME);
		info.setSortOrder(SORT_ASC);
		info.setRecomment(BANNER_PUSH);
		info.setStatus(INFOMATION_STATUS_ALREADY);
		ResultBean result = indexInfoService.getHomeInfoList(info);
		return result;
	}
	
	/**
	 * 平台信息-分类推荐
	 * @return 返回结果
	 */
	@RequestMapping("/searchCategory.action")
	@ResponseBody
	public ResultBean searchCategory(){
		TagView view = new TagView();
		view.setSortName(SORT_NAME);
		view.setSortOrder(SORT_ASC);
		ResultBean result = indexInfoService.searchCategory(view);
		return result;
	}
	
	/**
	 * 根据分类ID，检索平台信息
	 * @return 返回结果
	 */
	@RequestMapping("/searchInfoByCatId.action")
	@ResponseBody
	public ResultBean searchInfoByCategory(InfomationEntity info){
		info.setSortName(INFO_SORT_NAME);
		info.setSortOrder(DESC);
		info.setStatus(INFOMATION_STATUS_ALREADY);
		ResultBean result = indexInfoService.searchInfoByCategoryId(info);
		return result;
	}
	
	/***
	 * 根据平台信息ID检索平台详情
	 * @param infoId
	 * @return 返回结果
	 */
	@RequestMapping("/searchInfoDetail.action")
	@ResponseBody
	public ResultBean searchInfoById(String infoId){
		ResultBean result = new ResultBean();
		if(infoId == null || infoId.equals("")){
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_NULL_ID));
			return result;
		}
		InfomationEntity info = new InfomationEntity();
		info.setId(infoId);
		info = indexInfoService.searchInfoById(info);
		if(info == null){
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_NULL_DATA));
			return result;
		}
		result.setStatus(true);
		result.setData(info);
		return result;
	}
}
