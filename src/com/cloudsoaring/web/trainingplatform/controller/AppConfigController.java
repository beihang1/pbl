package com.cloudsoaring.web.trainingplatform.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.bus.service.SystemService;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.utils.ConfigUtil;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.trainingplatform.constants.TpConstants;
import com.cloudsoaring.web.trainingplatform.utils.TpConfigUtil;

/***
 * 系统配置项相关
 * @author liuyanshuang
 *
 */
@Controller
public class AppConfigController extends BaseController implements TpConstants{

	@Autowired
	private SystemService systemService;
	
	/**
	 * 获取系统配置项
	 * @return
	 */
	@RequestMapping("/pub/config/getSysConfig.action")
	@ResponseBody
	public ResultBean getSysConfig(){
		Map<String,Object> map = new HashMap<String,Object>();
	    //零钱兑换积分数量
	    map.put(CONFIG_POINTS_FOR_SMALL_CHANGE,TpConfigUtil.getPointsForSmallChange());
	    //价格显示方式[{text:"积分",value:"P"},{text:"零钱",value:"S"}]
		map.put(CONFIG_PRICE_DISPLAY_MODE, TpConfigUtil.getPriceDisplayMode());
		//系统支持的支付模式
		map.put(CONFIG_SUPPORTED_PAY_MODES, ConfigUtil.getProperty(CONFIG_SUPPORTED_PAY_MODES));
		//获取日期格式 date.format.default  yyyy/MM/dd  datetime.format.default yyyy/MM/dd HH:mm:ss  dateminute.format.default yyyy/MM/dd HH:mm
	    map.put(CONFIG_DATE_FORMAT_DEFAULT_KEY, ConfigUtil.getProperty(CONFIG_DATE_FORMAT_DEFAULT_VALUE));
	    map.put(CONFIG_DATE_TIME_FORMAT_DEFAULT_KEY, ConfigUtil.getProperty(CONFIG_DATE_TIME_FORMAT_DEFAULT_VALUE));
	    map.put(CONFIG_DATE_MINUTE_FORMAT_DEFAULT_KEY, ConfigUtil.getProperty(CONFIG_DATE_MINUTE_FORMAT_DEFAULT_VALUE));
	    map.put(CONFIG_LOGIN_OTHER_URL, ConfigUtil.getProperty(CONFIG_LOGIN_OTHER_URL));
	    map.put("SSO_LOGIN_ENABLE", StringUtil.isNotEmpty(ConfigUtil.getProperty(CONFIG_LOGIN_OTHER_URL)));
	    map.put("ACCESS_STATISTICS_PAGE_PARAM", ConfigUtil.getProperty("ACCESS_STATISTICS_PAGE_PARAM"));
	    //移动端是否显示返回主平台的按钮
	    map.put("MOBILE_SHOW_INDEX_BACK_BUTTON", ConfigUtil.getProperty("MOBILE_SHOW_INDEX_BACK_BUTTON"));
	    //移动端首页是否显示导师模块
	    map.put("MOBILE_SHOW_INDEX_TEACHER", ConfigUtil.getProperty("MOBILE_SHOW_INDEX_TEACHER"));
	    ResultBean result = ResultBean.success();
	    result.setData(map);
		return result;
	}
	
}
