package com.cloudsoaring.web.trainingplatform.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudsoaring.common.utils.SHA1Encrypt;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.trainingplatform.utils.TpConfigUtil;

@Controller
public class VideoController extends BaseController {

	@RequestMapping("/common/video/sign.action")
	@ResponseBody
	public ResultBean sign(){
		String params = (String) getParameter("params");
		ResultBean result = ResultBean.success();
		if(StringUtil.isNotEmpty(params)){
			result.setData(SHA1Encrypt.buildSHA1RequestParams(params, TpConfigUtil.getVideoServiceSecretKey()));
		}else{
			result.setData(SHA1Encrypt.buildSHA1AscRequestParams(WebContext.getParameters(), TpConfigUtil.getVideoServiceSecretKey()));
		}
		return result;
	}
	
	/**
	 * 获取获取视频所需的签名、秘钥等信息
	 * @return
	 */
	@RequestMapping("/common/video/config.action")
	@ResponseBody
	public ResultBean getPolivConfig(){
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("readToken", TpConfigUtil.getVideoServiceReadToken());
		map.put("writeToken", TpConfigUtil.getVideoServiceWriteToken());
		//map.put("secretKey", TpConfigUtil.getVideoServiceSecretKey());
		map.put("categoryCourse", TpConfigUtil.getVideoServiceCategoryCourse());
		ResultBean result = ResultBean.success();
	    result.setData(map);
		return result;
	}
}
