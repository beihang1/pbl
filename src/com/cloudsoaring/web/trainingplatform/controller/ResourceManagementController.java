package com.cloudsoaring.web.trainingplatform.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.UserEntity;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.entity.ManagerUserEntity;
import com.cloudsoaring.web.course.service.CourseService;
import com.cloudsoaring.web.course.service.ManagerUserService;
import com.cloudsoaring.web.course.view.TagView;
import com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity;
import com.cloudsoaring.web.trainingplatform.service.ResourceManagementService;
import com.cloudsoaring.web.trainingplatform.view.ResourceView;

@Controller
@RequestMapping("/manager/resource")
public class ResourceManagementController extends BaseController {
   
    @Autowired
    private ResourceManagementService resourceManagementService;
    @Autowired
    private CourseService courseService;
    @Autowired
	private ManagerUserService managerUserService;
    /**
     * 资源管理列表画面初始化
     * @param condition
     * @return
     */
    @RequestMapping("/listInit.action")
    public ModelAndView teacherLevelInit(ResourceView condition){
        ModelAndView result = new ModelAndView("trainingplatform/manager/resource_list");
        condition.setPageSize(10);
        TagView tagQuery = new TagView();
        List<TagView> tags = courseService.searchTagList(tagQuery);
        List<TagView> tagsSecond = courseService.searchTagSecondList(tagQuery);
        result.addObject("catagoryTags", tags);
        result.addObject("tagsSecond", tagsSecond);
        result.addObject("condition", condition);
        return result;
    }
    
    /**
     * 资源管理列表画面初始化
     * @param condition
     * @return
     */
    @RequestMapping("/resourcelistInit.action")
    public ModelAndView resourcelistInit(ResourceView condition){
        ModelAndView result = new ModelAndView("trainingplatform/manager/resource_list2");
        condition.setPageSize(10);
        TagView tagQuery = new TagView();
        List<TagView> tags = courseService.searchTagList(tagQuery);
        List<TagView> tagsSecond = courseService.searchTagSecondList(tagQuery);
        result.addObject("catagoryTags", tags);
        result.addObject("tagsSecond", tagsSecond);
        result.addObject("condition", condition);
        result = getRoleAndNum(result);
        return result;
    }
    /**
     * 资源管理列表
     * @param entity
     * @return
     */
    @RequestMapping("/resourceList.action")
    @ResponseBody
    public ResultBean resourceList(ResourceView entity) {
        return resourceManagementService.resourceList(entity);
    }
    /**
     * 删除资源
     * @param id 等级ID
     */
    @RequestMapping("/deleteResource.action")
    @ResponseBody
    public ResultBean deleteResource(String id) {
         ResultBean result = new ResultBean();
         resourceManagementService.deleteResource(id);
         result.setMessages(getMessage(MSG_S_DELETE));
         return result;
    }
   /**
    * 资源上传
    * @param entity
    * @return
 * @throws Exception 
    */
    @RequestMapping("/resourceUpload.action")
    @ResponseBody
    public ResultBean resourceUpload(ResourceView entity) throws Exception {
        return resourceManagementService.updateResource(entity);
    }
    //
    @RequestMapping("/uploadInit.action")
    public ModelAndView uploadInit(ResourceView condition){
        ModelAndView result = new ModelAndView("trainingplatform/manager/resource_upload");
        TagView tagQuery = new TagView();
        List<TagView> tags = courseService.searchTagList(tagQuery);
        List<TagView> tagsSecond = courseService.searchTagSecondList(tagQuery);
        result.addObject("catagoryTags", tags);
        result.addObject("tagsSecond", tagsSecond);
        result.addObject("resource", new ResourceView());
        return result;
    }
    @RequestMapping("/resourceuploadInit.action")
    public ModelAndView resourceuploadInit(ResourceView condition){
        ModelAndView result = new ModelAndView("trainingplatform/manager/resource_upload2");
        TagView tagQuery = new TagView();
        List<TagView> tags = courseService.searchTagList(tagQuery);
        List<TagView> tagsSecond = courseService.searchTagSecondList(tagQuery);
        result.addObject("catagoryTags", tags);
        result.addObject("tagsSecond", tagsSecond);
        result.addObject("resource", new ResourceView());
        result = getRoleAndNum(result);
        return result;
    }
    
    private ModelAndView getRoleAndNum(ModelAndView result) {
    	int num = courseService.getStuStatus();
    	result.addObject("num", num);
    	UserEntity user = WebContext.getSessionUser();
    	//判断是否管理员
    	ManagerUserEntity entity = managerUserService.searchByUserId(user.getUserId());
    	if(!user.getUserName().equals("guest")) {
    		List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
    		//老师和教育局角色
    		if(roleList.size() > 0 && "A".equals(user.getUserKbn()) && null==entity && !"2542668".equals(user.getUserId())) {
    			for(UserRoleEntity role : roleList) {
    				if(role.getRoleId().equals("01")) {
    					result.addObject("role", "t");
    				}
    			}
    		//教育局角色	
    		}else if(roleList.size() > 0  && (null!=entity || "2542668".equals(user.getUserId()))){
    			result.addObject("role", "m");
    		}else if("B".equals(user.getUserKbn())){
    			result.addObject("role", "e");
    	    //家长和教研员角色
    		}else if("D".equals(user.getUserKbn()) || "E".equals(user.getUserKbn())){
    			result.addObject("role", "q");
    		//学生角色
    		}else {
    			result.addObject("role", "s");
    		}
    	}

    	if(null!=entity || "2542668".equals(user.getUserId())) {
    		result.addObject("manager", "1");
    	}else {
    		result.addObject("manager", "0");
    	}
    	return result;
    }
}
