package com.cloudsoaring.web.trainingplatform.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.trainingplatform.entity.PropsEntity;
import com.cloudsoaring.web.trainingplatform.entity.PropsHisEntity;
import com.cloudsoaring.web.trainingplatform.service.PropsService;

@Controller
public class PropsController extends BaseController{
    @Autowired
    private PropsService propsService;
    /**
     * APP接口：道具列表
     * @param entity
     * @return
     */
    @RequestMapping("/pub/props/searchPropsList.action")
    @ResponseBody
    public ResultBean searchPropsList(PropsEntity entity) {
        return propsService.searchPropsList(entity);
    }
    /**
     * APP接口：赠送道具提交
     * @param entity
     * @return
     */
    @RequestMapping("/client/props/saveProps.action")
    @ResponseBody
    public ResultBean saveProps(PropsHisEntity entity) {
        return propsService.saveProps(entity);
    }
    /**
     * APP接口：导师获得的道具历史
     * @param entity
     * @return
     */
    @RequestMapping("/pub/props/searchGetPropsHis.action")
    @ResponseBody
    public ResultBean searchGetPropsHis(PropsHisEntity entity) {
        return propsService.searchGetPropsHis(entity);
    }
    
    /**
     * APP接口：用户赠送导师道具排行
     * @param entity
     * @return
     */
    @RequestMapping("/pub/props/searchGivePropsHis.action")
    @ResponseBody
    public ResultBean searchGivePropsHis(PropsHisEntity entity) {
        return propsService.searchGivePropsHis(entity);
    }
    /**
     * APP接口：导师屏蔽赠送道具信息
     * @param entity
     * @return
     */
    @RequestMapping("/client/props/updateFlagShowMsg.action")
    @ResponseBody
    public ResultBean updateFlagShowMsg(PropsHisEntity entity) {
        return propsService.updateFlagShowMsg(entity);
    }
    /**
     * 道具列表画面初始化
     * @param condition
     * @return
     */
    @RequestMapping("/manager/props/listInit.action")
    public ModelAndView listInit(PropsEntity condition){
        ModelAndView result = new ModelAndView("trainingplatform/manager/props/props_list");
        condition.setPageSize(10);
        result.addObject("condition", condition);
        return result;
    }
    /**
     *道具列表
     * @param entity
     * @return
     */
    @RequestMapping("/manager/props/searchPropsList.action")
    @ResponseBody
    public ResultBean searchList(PropsEntity entity) {
        return propsService.searchPropsList(entity);
    }
    /**
     * 道具查看
     * @param entity
     * @return
     */
    @RequestMapping("/manager/props/viewPropsInit.action")
    public ModelAndView viewPropsInit(PropsEntity entity){
        ModelAndView result = new ModelAndView("trainingplatform/manager/props/props_view");
        PropsEntity props=propsService.searchPropsById(entity);
        result.addObject("props", props);
        return result;
    }
    /**
     * 道具删除
     * @param id
     * @return
     */
    @RequestMapping("/manager/props/delProps.action")
    @ResponseBody
    public ResultBean delProps(String id) {
        ResultBean result = new ResultBean();
        propsService.delProps(id);
        result.setMessages(getMessage(MSG_S_DELETE));
        return result;
   }
    /**
     *道具赠送历史查看
     * @param condition
     * @return
     */
    @RequestMapping("/manager/props/viewPropsHisInit.action")
    public ModelAndView viewPropsHisInit(PropsHisEntity condition,String id,String propsName){
        ModelAndView result = new ModelAndView("trainingplatform/manager/props/props_his_list");
        condition.setPageSize(10);
        result.addObject("condition", condition);
        result.addObject("id", id);
        result.addObject("propsName", propsName);
        return result;
    }
   
    /**
     * 道具赠送历史查看
     * @param entity
     * @return
     */
    @RequestMapping("/manager/props/viewPropsHisList.action")
    @ResponseBody
    public ResultBean viewPropsHisList(PropsHisEntity entity) {
        return propsService.searchPropsHisList(entity);
    }
    /**
     * 道具新增
     * @param entity
     * @return
     */
    @RequestMapping("/manager/props/addPropsInit.action")
    public ModelAndView addPropsInit(PropsEntity entity){
        ModelAndView result = new ModelAndView("trainingplatform/manager/props/props_add");
        result.addObject("props",new PropsEntity());
        return result;
    }
    /**
     * 道具编辑画面初始化
     * @param entity
     * @return
     */
    @RequestMapping("/manager/props/editProps.action")
    public ModelAndView editPropsInit(PropsEntity entity){
        ModelAndView result = new ModelAndView("trainingplatform/manager/props/props_add");
        PropsEntity props=propsService.searchPropsById(entity);
        result.addObject("props",props);
        return result;
    }
    /**
     * 道具新增和编辑的提交
     * @param entity
     * @return
     * @throws Exception
     */
    @RequestMapping("/manager/props/addProps.action")
    @ResponseBody
    public ResultBean addProps(PropsEntity entity) throws Exception {
        ResultBean result = ResultBean.success();
        if(StringUtil.isEmpty(entity.getId())) {
            propsService.addProps(entity);
        }else{
            propsService.editProps(entity);
        }
        return result;
   }
    @RequestMapping("/client/props/immediatePayDetail.action")
    @ResponseBody
    public ResultBean immediatePayDetail(String propsId,String num) {
        return propsService.immediatePayDetail(propsId,num);
    }
}
