/**
* Filename : PersonalHomePageController.java
* Author : TongLei
* Creation time : 下午3:46:23 - 2015年11月5日
* Description :
*/

package com.cloudsoaring.web.trainingplatform.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.UserEntity;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.common.view.ResultBean.ErrorCode;
import com.cloudsoaring.web.course.entity.UserCourseEntity;
import com.cloudsoaring.web.trainingplatform.constants.TpMsgNames;
import com.cloudsoaring.web.trainingplatform.service.PersonalHomePageService;

/**
 * 个人中心首页
 * @author TongLei
 *
 */
@Controller
@RequestMapping("/pub/personal/homePage")
public class PersonalHomePageController extends BaseController{
	@Autowired
	private PersonalHomePageService personalHomePageService;
	
	/**
	 * 个人中心首页检索用户信息
	 * @param user
	 * @return 返回结果
	 */
	@RequestMapping("/searchPersonal.action")
	@ResponseBody
	public ResultBean searchPersonal(UserEntity user){
		ResultBean result = new ResultBean();
		if(WebContext.isGuest()){
			result.setStatus(false);
			result.setMessages("用户未登录");
			result.setErrorCode(ErrorCode.NOT_LOGIN);
			return result;
		}
		user.setUserId(WebContext.getSessionUserId());
		user = personalHomePageService.searchPersonalUser(user);
		result.setData(user);
		return result;
	}
	
	
	/**
	 * 检索参与的课程返回页面
	 * @param userCourse
	 * @return 返回结果
	 */
	@RequestMapping("/searchAttendCourse.action")
	@ResponseBody
	public ResultBean searchAttendCourse(UserCourseEntity userCourse){
		ResultBean result = new ResultBean();
		if(WebContext.isGuest()){
			result.setStatus(false);
			result.setMessages(TpMsgNames.USER_NOT_LOGIN);
			//判断为未登录时使用ErrorCode
			result.setErrorCode(ErrorCode.NOT_LOGIN);
			return result;
		}
		userCourse.setUserId(WebContext.getSessionUserId());
		return personalHomePageService.searchAttendCourse(userCourse);
	}
	
	/**
	 * 检索收藏的课程返回页面
	 * @param userCourse
	 * @return 返回结果
	 */
	@RequestMapping("/searchCollectCourse.action")
	@ResponseBody
	public ResultBean searchCollectCourse(UserCourseEntity userCourse){
		ResultBean result = new ResultBean();
		if(WebContext.isGuest()){
			result.setStatus(false);
			result.setMessages(TpMsgNames.USER_NOT_LOGIN);
			result.setErrorCode(ErrorCode.NOT_LOGIN);
			return result;
		}
		userCourse.setUserId(WebContext.getSessionUserId());
		return personalHomePageService.searchCollectCourse(userCourse);
	}
}
