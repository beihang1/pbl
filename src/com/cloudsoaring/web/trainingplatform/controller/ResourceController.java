package com.cloudsoaring.web.trainingplatform.controller;


import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.exception.BusinessException;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.TagEntity;
import com.cloudsoaring.web.bus.entity.UserEntity;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.entity.ManagerUserEntity;
import com.cloudsoaring.web.course.service.ChapterService;
import com.cloudsoaring.web.course.service.CourseService;
import com.cloudsoaring.web.course.service.ManagerUserService;
import com.cloudsoaring.web.course.view.ChapterView;
import com.cloudsoaring.web.course.view.CourseView;
import com.cloudsoaring.web.course.view.TagView;
import com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity;
import com.cloudsoaring.web.trainingplatform.service.ResourceManagementService;
import com.cloudsoaring.web.trainingplatform.view.ResourceView;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
@Controller
public class ResourceController extends BaseController {
    @Autowired
    private ResourceManagementService resourceManagementService;
    @Autowired
	private ChapterService chapterService;
    @Autowired
    private CourseService courseService;
    @Autowired
	private ManagerUserService managerUserService;
    /**
     * PC-资源列表初始化页面
     * @param condition
     * @return
     */
    @RequestMapping("/pub/resourse/toResourse.action")
    public ModelAndView teacherLevelInit(ResourceView condition){
    	int num = courseService.getStuStatus();
        ModelAndView result = new ModelAndView("trainingplatform/client/resource_list");
        result.addObject("num", num);
        UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						result.addObject("role", "t");
					}
				}
			}else {
				result.addObject("role", "s");
			}
		}
        return result;
    }
    
    /**
     * 
     * @param resourceId
     * @return
     */
    @RequestMapping("/pub/resourse/resourceDetailInit.action")
	public ModelAndView resourceDetailInit(String resourceId,String fileId,String directionId,String classifyId) {
		ModelAndView result = new ModelAndView("trainingplatform/client/resource_detail");
		ResultBean data = resourceManagementService.getResourceDetailById(resourceId);
		if (!data.isStatus()) {
			throw new BusinessException(data.getMessages());
		}
		result.addObject("courseView", data.getData());
		ResourceView view = (ResourceView) data.getData();
		List<TagEntity> directTag = view.getDirectTag();
		List<TagEntity> categoryTag = view.getCategoryTag();
		result.addObject("directTag", directTag);
		result.addObject("categoryTag", categoryTag);
		String serverName = WebContext.getRequest().getServerName();
		int serverPort = WebContext.getRequest().getServerPort();
		result.addObject("serverUrl", serverName+":"+serverPort);
		int num = courseService.getStuStatus();
		result.addObject("num", num);
		UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						result.addObject("role", "t");
					}
				}
			}else {
				result.addObject("role", "s");
			}
		}
		return result;
	}
    
    /**
     * @param resourceId,fileId
     * @return
     */
    @RequestMapping("/pub/resourse/getResourceDetail.action")
    @ResponseBody
	public String getResourceDetail(String resourceId,String fileId)throws ParseException {
		JSONObject jo = new JSONObject();
		ResultBean data = resourceManagementService.getResourceDetailById(resourceId);
		if (!data.isStatus()) {
			throw new BusinessException(data.getMessages());
		}
		ResourceView view = (ResourceView) data.getData();
		List<TagEntity> directTag = view.getDirectTag();
		List<TagEntity> categoryTag = view.getCategoryTag();
		jo.put("fileName", view.getFileName());
		jo.put("userName", view.getUserName());
		jo.put("createTime", view.getCreatetime());
		jo.put("briefInTroduction", view.getBriefInTroduction());
		String tag = "";
		if(directTag.size() > 0) {
			for(TagEntity firstTag : directTag) {
				String directiveTagName = "";
				if(firstTag.getTagName()!=null && !"".equals(firstTag.getTagName())){
					directiveTagName = firstTag.getTagName() + "/";
				}
				for(TagEntity secondTag : categoryTag) {
					if(secondTag.getParentId().equals(firstTag.getTagId())){
						String categoryTagName = "";
						if(secondTag.getTagName()!=null && !"".equals(secondTag.getTagName())){
							categoryTagName = secondTag.getTagName() + "/";
							tag += directiveTagName+categoryTagName+";";
						}
					}
				}
			}
		}
		jo.put("tag", tag);
		return jo.toString();
	}
    
    /**
     * 编辑页面初始化
     * @param id 课程ID
     * @return
     */
    @RequestMapping("/pub/resourse/editInit.action")
    public ModelAndView editInit(String id) {
        ModelAndView result = new ModelAndView("trainingplatform/manager/resource_upload");
        String type="edit";
        String firstTagOne = null;
        String secondTagOne = null;
        String firstTagTwo = null;
        String secondTagTwo = null;
        String firstTagThree = null;
        String secondTagThree = null;
        String firstTagFour = null;
        String secondTagFour = null;
        String firstTagFive = null;
        String secondTagFive = null;
        ResourceView view = resourceManagementService.searchCourse4EditById(id,type);
        TagView tagQuery = new TagView();
        List<TagView> tags = courseService.searchTagList(tagQuery);
        List<TagView> tagsSecond = courseService.searchTagSecondList(tagQuery);
        String firstTag = view.getFirstTag();
        String secondTag = view.getSecondTag();
        String[] firstTagArr = firstTag.split(",");
        String[] secondTagArr = secondTag.split(",");
        if(firstTagArr.length == 1) {
        	firstTagOne = firstTagArr[0];
        	//secondTagOne = secondTagArr[0];
        }else if(firstTagArr.length == 2) {
        	firstTagOne = firstTagArr[0];
        	//secondTagOne = secondTagArr[0];
        	firstTagTwo = firstTagArr[1];
        	//secondTagTwo = secondTagArr[1];
        }else if(firstTagArr.length == 3) {
        	firstTagOne = firstTagArr[0];
        	//secondTagOne = secondTagArr[0];
        	firstTagTwo = firstTagArr[1];
        	//secondTagTwo = secondTagArr[1];
        	firstTagThree = firstTagArr[2];
        	//secondTagThree = secondTagArr[2];
        }else if(firstTagArr.length == 4) {
        	firstTagOne = firstTagArr[0];
        	//secondTagOne = secondTagArr[0];
        	firstTagTwo = firstTagArr[1];
        	//secondTagTwo = secondTagArr[1];
        	firstTagThree = firstTagArr[2];
        	//secondTagThree = secondTagArr[2];
        	firstTagFour = firstTagArr[3];
        	//secondTagFour = secondTagArr[3];
        }else if(firstTagArr.length == 5) {
        	firstTagOne = firstTagArr[0];
        	//secondTagOne = secondTagArr[0];
        	firstTagTwo = firstTagArr[1];
        	//secondTagTwo = secondTagArr[1];
        	firstTagThree = firstTagArr[2];
        	//secondTagThree = secondTagArr[2];
        	firstTagFour = firstTagArr[3];
        	//secondTagFour = secondTagArr[3];
        	firstTagFive = firstTagArr[4];
        	//secondTagFive = secondTagArr[4];
        }
        result.addObject("catagoryTags", tags);
        result.addObject("tagsSecond", tagsSecond);
        result.addObject("resource", view);
        result.addObject("pageType",type);
        result.addObject("firstTagOne",firstTagOne);
        List<TagView> secondTagOneList = courseService.searchSecondTagList(firstTagOne);
        
        result.addObject("secondTagOneList",secondTagOneList);
        result.addObject("secondTagOne",getSecondTag(secondTagOneList, secondTagArr));
        result.addObject("firstTagTwo",firstTagTwo);
        List<TagView> secondTagTwoList = courseService.searchSecondTagList(firstTagTwo);
        result.addObject("secondTagTwoList",secondTagTwoList);
        result.addObject("secondTagTwo",getSecondTag(secondTagTwoList, secondTagArr));
        result.addObject("firstTagThree",firstTagThree);
        List<TagView> secondTagThreeList = courseService.searchSecondTagList(firstTagThree);
        result.addObject("secondTagThreeList",secondTagThreeList);
        result.addObject("secondTagThree",getSecondTag(secondTagThreeList, secondTagArr));
        result.addObject("firstTagFour",firstTagFour);
        List<TagView> secondTagFourList = courseService.searchSecondTagList(firstTagFour);
        result.addObject("secondTagFourList",secondTagFourList);
        result.addObject("secondTagFour",getSecondTag(secondTagFourList, secondTagArr));
        result.addObject("firstTagFive",firstTagFive);
        List<TagView> secondTagFiveList = courseService.searchSecondTagList(firstTagFive);
        result.addObject("secondTagFiveList",secondTagFiveList);
        result.addObject("secondTagFive",getSecondTag(secondTagFiveList, secondTagArr));
        return result;
    }
    private String getSecondTag(List<TagView> secondTagOneList, String[] secondTagArr) {
    	if(secondTagOneList == null || secondTagOneList.size() == 0) {
    		return null;
    	}
    	String secondTag = "";
    	for(TagView tag : secondTagOneList) {
        	for(String s : secondTagArr) {
        		if(tag.getTagId().equals(s)) {
        			secondTag = s;
        		}
        	}
        }
    	return secondTag;
    }   
    /**
     * 编辑页面初始化
     * @param id 课程ID
     * @return
     */
    @RequestMapping("/pub/resourse/resourceeditInit.action")
    public ModelAndView resourceeditInit(String id) {
        ModelAndView result = new ModelAndView("trainingplatform/manager/resource_upload2");
        String type="edit";
        String firstTagOne = null;
        String secondTagOne = null;
        String firstTagTwo = null;
        String secondTagTwo = null;
        String firstTagThree = null;
        String secondTagThree = null;
        String firstTagFour = null;
        String secondTagFour = null;
        String firstTagFive = null;
        String secondTagFive = null;
        ResourceView view = resourceManagementService.searchCourse4EditById(id,type);
        TagView tagQuery = new TagView();
        List<TagView> tags = courseService.searchTagList(tagQuery);
        List<TagView> tagsSecond = courseService.searchTagSecondList(tagQuery);
        String firstTag = view.getFirstTag();
        String secondTag = view.getSecondTag();
        String[] firstTagArr = firstTag.split(",");
        String[] secondTagArr = secondTag.split(",");
        if(firstTagArr.length == 1) {
        	firstTagOne = firstTagArr[0];
        	//secondTagOne = secondTagArr[0];
        }else if(firstTagArr.length == 2) {
        	firstTagOne = firstTagArr[0];
        	//secondTagOne = secondTagArr[0];
        	firstTagTwo = firstTagArr[1];
        	//secondTagTwo = secondTagArr[1];
        }else if(firstTagArr.length == 3) {
        	firstTagOne = firstTagArr[0];
        	//secondTagOne = secondTagArr[0];
        	firstTagTwo = firstTagArr[1];
        	//secondTagTwo = secondTagArr[1];
        	firstTagThree = firstTagArr[2];
        	//secondTagThree = secondTagArr[2];
        }else if(firstTagArr.length == 4) {
        	firstTagOne = firstTagArr[0];
        	//secondTagOne = secondTagArr[0];
        	firstTagTwo = firstTagArr[1];
        	//secondTagTwo = secondTagArr[1];
        	firstTagThree = firstTagArr[2];
        	//secondTagThree = secondTagArr[2];
        	firstTagFour = firstTagArr[3];
        	//secondTagFour = secondTagArr[3];
        }else if(firstTagArr.length == 5) {
        	firstTagOne = firstTagArr[0];
        	//secondTagOne = secondTagArr[0];
        	firstTagTwo = firstTagArr[1];
        	//secondTagTwo = secondTagArr[1];
        	firstTagThree = firstTagArr[2];
        	//secondTagThree = secondTagArr[2];
        	firstTagFour = firstTagArr[3];
        	//secondTagFour = secondTagArr[3];
        	firstTagFive = firstTagArr[4];
        	//secondTagFive = secondTagArr[4];
        }
        result.addObject("catagoryTags", tags);
        result.addObject("tagsSecond", tagsSecond);
        result.addObject("resource", view);
        result.addObject("pageType",type);
        result.addObject("firstTagOne",firstTagOne);
        List<TagView> secondTagOneList = courseService.searchSecondTagList(firstTagOne);
        
        result.addObject("secondTagOneList",secondTagOneList);
        result.addObject("secondTagOne",getSecondTag(secondTagOneList, secondTagArr));
        result.addObject("firstTagTwo",firstTagTwo);
        List<TagView> secondTagTwoList = courseService.searchSecondTagList(firstTagTwo);
        result.addObject("secondTagTwoList",secondTagTwoList);
        result.addObject("secondTagTwo",getSecondTag(secondTagTwoList, secondTagArr));
        result.addObject("firstTagThree",firstTagThree);
        List<TagView> secondTagThreeList = courseService.searchSecondTagList(firstTagThree);
        result.addObject("secondTagThreeList",secondTagThreeList);
        result.addObject("secondTagThree",getSecondTag(secondTagThreeList, secondTagArr));
        result.addObject("firstTagFour",firstTagFour);
        List<TagView> secondTagFourList = courseService.searchSecondTagList(firstTagFour);
        result.addObject("secondTagFourList",secondTagFourList);
        result.addObject("secondTagFour",getSecondTag(secondTagFourList, secondTagArr));
        result.addObject("firstTagFive",firstTagFive);
        List<TagView> secondTagFiveList = courseService.searchSecondTagList(firstTagFive);
        result.addObject("secondTagFiveList",secondTagFiveList);
        result.addObject("secondTagFive",getSecondTag(secondTagFiveList, secondTagArr));
        result = getRoleAndNum(result);
        return result;
    }
    
    private ModelAndView getRoleAndNum(ModelAndView result) {
    	int num = courseService.getStuStatus();
    	result.addObject("num", num);
    	UserEntity user = WebContext.getSessionUser();
    	//判断是否管理员
    	ManagerUserEntity entity = managerUserService.searchByUserId(user.getUserId());
    	if(!user.getUserName().equals("guest")) {
    		List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
    		//老师和教育局角色
    		if(roleList.size() > 0 && "A".equals(user.getUserKbn()) && null==entity && !"2542668".equals(user.getUserId())) {
    			for(UserRoleEntity role : roleList) {
    				if(role.getRoleId().equals("01")) {
    					result.addObject("role", "t");
    				}
    			}
    		//教育局角色	
    		}else if(roleList.size() > 0  && (null!=entity || "2542668".equals(user.getUserId()))){
    			result.addObject("role", "m");
    		}else if("B".equals(user.getUserKbn())){
    			result.addObject("role", "e");
    	    //家长和教研员角色
    		}else if("D".equals(user.getUserKbn()) || "E".equals(user.getUserKbn())){
    			result.addObject("role", "q");
    		//学生角色
    		}else {
    			result.addObject("role", "s");
    		}
    	}

    	if(null!=entity || "2542668".equals(user.getUserId())) {
    		result.addObject("manager", "1");
    	}else {
    		result.addObject("manager", "0");
    	}
    	return result;
    }
    
}
