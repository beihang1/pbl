/**
* Filename : NoticeManageController.java
* Author : TongLei
* Creation time : 下午1:42:35 - 2015年11月11日
* Description :
*/

package com.cloudsoaring.web.trainingplatform.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.entity.BaseEntity;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseStateConstants;
import com.cloudsoaring.web.course.view.TagView;
import com.cloudsoaring.web.trainingplatform.constants.TpConstants;
import com.cloudsoaring.web.trainingplatform.entity.InfomationCategoryEntity;
import com.cloudsoaring.web.trainingplatform.entity.InfomationEntity;
import com.cloudsoaring.web.trainingplatform.service.NoticeManageService;





/**
 * 后台公告管理
 * @author TongLei
 *
 */
@Controller
@RequestMapping("/notice/manage")
public class NoticeManageController extends BaseController implements TpConstants{
	@Autowired
	private NoticeManageService noticeManageService;
	
	/**
	 * 跳转到公告管理页面
	 * @return 返回结果
	 */
	@RequestMapping("/noticelist.action")
	public ModelAndView noticeList(TagView view){
		List categorys = noticeManageService.searchNoticeTag(view);
		/*TagView allCategory = new TagView();*/
		/*allCategory.setTagId("all");
		allCategory.setTagName("全部");*/
		/*categorys.add(0, allCategory);*/
		ModelAndView mv = new ModelAndView("notice/manager/info_list");
		mv.addObject("categorys", categorys);
		return mv;
	}
	
	/**
	 * 跳转到公告添加页面
	 * @return 返回结果
	 */
	
	@RequestMapping("/noticeadd.action")
	public ModelAndView noticeAdd(TagView view){
		List categorys = noticeManageService.searchNoticeTag(view);
		ModelAndView mv = new ModelAndView("notice/manager/info_add");
		mv.addObject("categorys", categorys);
		return mv;
	}
	/**
	 * 跳转到公告编辑页面
	 * @param id
	 * @return 返回结果
	 */
	@RequestMapping("/noticeedit.action")
	public ModelAndView noticeEdit(String id){
		List categorys = noticeManageService.searchNoticeTag(new TagView());
		InfomationEntity entity = new InfomationEntity();
		entity.setId(id);
		InfomationEntity condition = noticeManageService.selectNotice(entity);
		ModelAndView mv = new ModelAndView("notice/manager/info_edit");
		mv.addObject("categorys", categorys);
		mv.addObject("notice", condition);
		return mv;
	}
	/**
	 * 检索公告分类信息
	 * @param entity
	 * @return 返回结果
	 */
	@RequestMapping("/searchcategory.action")
	@ResponseBody
	public List searchCategory(InfomationCategoryEntity entity){
		return noticeManageService.search(entity);
	}
	
	/**
	 * 检索公告信息
	 * 根据公告分类
	 * @param entity
	 * @return 返回结果
	 */
	@RequestMapping("/searchnotice.action")
	@ResponseBody
	public ResultBean searchNotice(InfomationEntity entity){
		/*if("all".equals(entity.getId())){
			entity.setCategoryId(null);
			entity.setId(null);
		}
		entity.setCategoryId(entity.getId());
		entity.setId(null);*/
		return noticeManageService.searchNotice(entity);
	}
	/**
	 * 新增公告信息
	 * @param entity
	 * @return 返回结果
	 */
	@RequestMapping("/insertnotice.action")
	@ResponseBody
	public ResultBean insertNotice(InfomationEntity entity) throws Exception {
		entity.setId(IDGenerator.genUID());
		if(StringUtil.isEmpty(entity.getContent())){
			return ResultBean.error(CONTENT_CANNOT_EMPTY);
		}
		noticeManageService.insertNotice(entity);
		
		TagView view = new TagView();
		view.setTagId(entity.getCategoryId());
		view.setLinkId(entity.getId());
		view.setLinkType(NOTICE_LINK_TYPE);
		
		noticeManageService.insertNoticeType(view);
		return ResultBean.success(MSG_ADD_NOTICE_SUCCESS);
	}
	/**
	 * 编辑保存
	 * @param entity
	 * @return 返回结果
	 */
	@RequestMapping("/updatenotice.action")
	@ResponseBody
	public ResultBean updateNotice(InfomationEntity entity) throws Exception{
		if(StringUtil.isEmpty(entity.getContent())){
			return ResultBean.error(CONTENT_CANNOT_EMPTY);
		}
		noticeManageService.editNotice(entity);
		
		TagView view = new TagView();
		view.setTagId(entity.getCategoryId());
		view.setLinkId(entity.getId());
		view.setLinkType(NOTICE_LINK_TYPE);
		
		noticeManageService.editNoticeType(view);
		return ResultBean.success(EDIT_NOTICE_SUCCESS);
	}
	/**
	 * 发布公告
	 * @param entity
	 * @return 返回结果
	 */
	@RequestMapping("/statusinfo.action")
	@ResponseBody
	public ResultBean isStatus(InfomationEntity entity){
		return 	noticeManageService.isStatus(entity);
	}
	/**
	 * 删除公告
	 * @param entity
	 * @return 返回结果
	 */
	@RequestMapping("/deleteinfo.action")
	@ResponseBody
	public ResultBean deleteInfo(InfomationEntity entity)throws Exception{
		noticeManageService.deleteInfo(entity);
		return ResultBean.success(DELETE_NOTICE_SUCCESS);
	}
	
	/**
	 * 公告标签
	 * @return
	 */
	@RequestMapping("/tagInit.action")
	public  ModelAndView init(){
		ModelAndView mv = new ModelAndView("/taxonomy/manager/taxonomy_list");
		BaseEntity condition=new BaseEntity();
		condition.setPageSize(5);
		mv.addObject("condition",condition);
		mv.addObject("tagTypelevel","");
		mv.addObject("businessType",CourseStateConstants.TAG_BUSSINESS_TYPE_NOTICE);
		return mv;
	}

}
