package com.cloudsoaring.web.trainingplatform.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.trainingplatform.entity.TeacherLevelEntity;
import com.cloudsoaring.web.trainingplatform.service.TeacherLevelManagerService;

@Controller
@RequestMapping("/manager/teacherLevel")
public class TeacherLevelManagerController extends BaseController{
    @Autowired
    private TeacherLevelManagerService teacherLevelManagerService;
    /**
     * 老师等级列表页面初始化
     * @return 返回老师等级管理页面
     */
    @RequestMapping("/teacherLevelInit.action")
    public ModelAndView teacherLevelInit(TeacherLevelEntity condition){
        ModelAndView result = new ModelAndView("trainingplatform/manager/teacher_level_list");
        condition.setPageSize(10);
        result.addObject("condition", condition);
        result.addObject("pageType","list");
        return result;
    }
    /**
     * 
     *教师等级列表查询
     * @param entity 老师等级对象
     * @return 教师等级列表数据
     */
    @RequestMapping("/teacherLevelList.action")
    @ResponseBody
    public ResultBean teacherLevel(TeacherLevelEntity entity) {
        
        return teacherLevelManagerService.teacherLevel(entity);
    }
  /**  
   * 老师等级新增页面初始化
   * @return 老师等级新增页面
   */
    @RequestMapping("/addLevelInit.action")
    public ModelAndView addLevelInit(){
        ModelAndView result = new ModelAndView("trainingplatform/manager/teacher_level_add");
        result.addObject("pageType","add");
        result.addObject("level",new TeacherLevelEntity());
        return result;
    }
    
    @RequestMapping("/editLevelInit.action")
    public ModelAndView editLevelInit(String levelId){
        ModelAndView result = new ModelAndView("trainingplatform/manager/teacher_level_add");
        result.addObject("pageType","edit");
        TeacherLevelEntity level = teacherLevelManagerService.seachTeacherLevelById(levelId);
        result.addObject("level",level);
        return result;
    }
   /**
    * 保存教师等级信息
    * @param entity 教师等级对象
    */
    @RequestMapping("/saveLevel.action")
    @ResponseBody
    public void saveLevel(TeacherLevelEntity entity) {
            
        if(StringUtil.isEmpty(entity.getLevelId())){
            //添加
            teacherLevelManagerService.addLevel(entity);
        }else {
            //编辑
            teacherLevelManagerService.updateLevel(entity);
        }
    }
    /**
     * 删除老师等级
     * @param levelId 等级ID
     */
    @RequestMapping("/teacherLevelDelete.action")
    @ResponseBody
    public ResultBean teacherLevelDelete(String levelId) {
         ResultBean result = new ResultBean();
         teacherLevelManagerService.teacherLevelDelete(levelId);
         result.setMessages(getMessage(MSG_S_DELETE));
         return result;
    }
}
