package com.cloudsoaring.web.trainingplatform.controller;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseMsgNames;
import com.cloudsoaring.web.trainingplatform.constants.TpMsgNames;
import com.cloudsoaring.web.trainingplatform.entity.TpUserEntity;
import com.cloudsoaring.web.trainingplatform.service.UserManagementService;




/**
 * 用户管理
 * @author hanjun
 *
 */
@Controller
@RequestMapping("/tpuser")
public class UserManagementController extends BaseController {

	@Autowired
	private UserManagementService userManagementService;
	
	/**
	 * 用户管理页面 初始化
	 * @param condition
	 * @return 返回结果
	 */
	@RequestMapping("/init.action")
	public ModelAndView searchInit(TpUserEntity condition){
		ModelAndView mv = new ModelAndView("trainingplatform/manager/user_list");
		mv.addObject("condition", condition);
		return mv;
	}
	
	/**
	 * 用户管理页面 检索列表
	 * @param condition
	 * @return 返回结果
	 */
	@RequestMapping("/search.action")
	@ResponseBody
	public ResultBean search(TpUserEntity condition){
		return userManagementService.search4Page(condition);
	}
	
	/**
	 * 用户新增
	 * @return 返回结果
	 */
	@RequestMapping("/add.action")
	public ModelAndView addMaster(){
		ModelAndView mv = new ModelAndView("trainingplatform/manager/user_add");
		TpUserEntity user = new TpUserEntity();
		mv.addObject("user", user);
		return mv;
	}
	
	/**
	 * 校验用户名
	 * @param user
	 * @return 返回结果
	 */
	@RequestMapping("/checkUserName.action")
	@ResponseBody
	public ResultBean checkUserName(TpUserEntity user){
		ResultBean result = new ResultBean();
		result.setStatus(true);
		if(StringUtil.isEmpty(user.userName)){
			return result;
		}
		//判断用户名是否存在
		boolean userNameExist = userManagementService.userNameExist(user);
		result.setStatus(!userNameExist);
		if(userNameExist){
			result.setMessages(getMessage(MSG_E_USERNAME_EXIST, user.userName));
			return result;
		}
		return result;
	}
	
	@RequestMapping("/checkPassword.action")
	@ResponseBody
	public ResultBean checkPassword(TpUserEntity user){
		ResultBean result = new ResultBean();
		result.setStatus(true);
		if(StringUtil.isEmpty(user.password)){
			return result;
		}
		String password= user.password;
		String reg = "(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9]).{8,16}";
		if (!password.matches(reg)) {
			result.setStatus(false);
			result.setMessages("密码长度8~16位,必须包含数字,字母,特殊符号！");
			return result;
		}
		return result;
	}
	
	/**
	 * 校验邮箱
	 * @param user
	 * @return 返回结果
	 */
	@RequestMapping("/checkEmail.action")
	@ResponseBody
	public ResultBean checkEmail(TpUserEntity user){
		ResultBean result = new ResultBean();
		result.setStatus(true);
		if(StringUtil.isEmpty(user.email)){
			return result;
		}
		//判断用户名是否存在
		boolean emailExist = userManagementService.emailExist(user);
		result.setStatus(!emailExist);
		if(emailExist){
			result.setMessages(getMessage(MSG_E_EMAIL_EXIST, user.email));
			return result;
		}
		return result;
	}
	
	/**
	 * 用户编辑
	 * @param userId
	 * @return 返回结果
	 */
	@RequestMapping("/edit.action")
	public ModelAndView edit(@RequestParam("userId")String userId){
		ModelAndView mv = new ModelAndView("trainingplatform/manager/user_edit");
		TpUserEntity user = userManagementService.searchById(userId);
		mv.addObject("user", user);
		return mv;
	}
	
	/**
	 * 保存用户信息
	 * @param user
	 * @return 返回结果
	 */
	@RequestMapping("/save.action")
	@ResponseBody
	public ResultBean save(TpUserEntity user){
		ResultBean result = new ResultBean();
		userManagementService.save(user);
		result.setData(user);
		result.setMessages(getMessage(MSG_S_SAVE));
		return result;
	}
	
	/**
	 * 删除用户
	 * @param userId
	 * @return 返回结果
	 */
	@RequestMapping("/delete.action")
	@ResponseBody
	public ResultBean delete(String userId){
		ResultBean result = new ResultBean();
		if(StringUtil.isEmpty(WebContext.getSessionUserId())){
			return ResultBean.error(CourseMsgNames.NULL_USER_ID);
		}
		if(StringUtil.equals(WebContext.getSessionUserId(), userId)){
			return ResultBean.error(CourseMsgNames.CAN_NOT_DELETE_YOURSELF);
		}
		userManagementService.deleteUser(userId);
		result.setMessages(getMessage(MSG_S_DELETE));
		return result;
	}
	/**
	 * 认证为老师
	 * @param userId
	 * @return 返回结果
	
	@RequestMapping("/teacher.action")
	@ResponseBody
	public ResultBean teacher(String userId){
		userManagementService.teacherAccount(userId);
		return ResultBean.success(TpMsgNames.SUCCESS_IDENTIFY_TO_TEACHER);
	}
	 
	/**
	 * 取消老师认证
	 * @param userId
	 * @return 返回结果
	
	@RequestMapping("/unTeacher.action")
	@ResponseBody
	public ResultBean unteacher(String userId){
		userManagementService.unteacherAccount(userId);
		return ResultBean.success(TpMsgNames.SUCCESS_IDENTIFY_TO_UNTEACHER);
	}
	
	/**
	 * 认证为管理员
	 * @param userId
	 * @return 返回结果
	*/
	@RequestMapping("/administrator.action")
	@ResponseBody
	public ResultBean administrator(String userId){
		userManagementService.administratorAccount(userId);
		return ResultBean.success(TpMsgNames.SUCCESS_IDENTIFY_TO_ADMINISTRATOR);
	}
	
	/**
	 * 取消管理员认证
	 * @param userId
	 * @return 返回结果
	
	 */
	@RequestMapping("/unAdministrator.action")
	@ResponseBody
	public ResultBean unAdministrator(String userId){
		userManagementService.unAdministratorAccount(userId);
		return ResultBean.success(TpMsgNames.SUCCESS_IDENTIFY_TO_UNADMINISTRATOR);
	}
	
	/**
	 * 认证为后台用户
	 * @param userId
	 * @return 返回结果
	*/
	@RequestMapping("/manager.action")
	@ResponseBody
	public ResultBean manager(String userId){
		userManagementService.beManagerAccount(userId);
		return ResultBean.success(TpMsgNames.SUCCESS_IDENTIFY_TO_MANAGER);
	}
	
	/**
	 * 取消后台用户认证
	 * @param userId
	 * @return 返回结果
	
	 */
	@RequestMapping("/unManager.action")
	@ResponseBody
	public ResultBean unManager(String userId){
		userManagementService.notManagerAccount(userId);
		return ResultBean.success(TpMsgNames.SUCCESS_IDENTIFY_TO_UNMANAGER);
	}
	
	/**
	 * 导入页面初始化
	 * @return 返回结果
	 */
	@RequestMapping("/importInit.action")
	public String importInit(){
		return "trainingplatform/manager/user_import";
	}
	
	/**
	 * 导入
	 * @param users
	 * @return 返回结果
	 * @throws Exception
	 */
	@ResponseBody
	@RequestMapping("/doImport.action")
	public ResultBean doImport(@RequestParam("users") CommonsMultipartFile users) throws Exception{
		InputStream input = null;;
		try{
			input = users.getInputStream();
			return ResultBean.success().setMessages(getMessage(MSG_S_OPERATION)).setData(userManagementService.doImport(input));
		}finally{
			IOUtils.closeQuietly(input);
		}
	}
}
