package com.cloudsoaring.web.trainingplatform.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.UserEntity;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.entity.ManagerUserEntity;
import com.cloudsoaring.web.course.service.CourseService;
import com.cloudsoaring.web.course.service.ManagerUserService;
import com.cloudsoaring.web.course.view.TagView;
import com.cloudsoaring.web.trainingplatform.constants.TpConstants;
import com.cloudsoaring.web.trainingplatform.entity.NewsCenterEntity;
import com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity;
import com.cloudsoaring.web.trainingplatform.service.NewsCenterService;

@Controller
@RequestMapping("/pub/newsCenter")
public class NewsCenterManageController extends BaseController implements TpConstants{
	@Autowired
	private NewsCenterService newsCenterService;
	@Autowired
	private CourseService courseService;
	@Autowired
	private ManagerUserService managerUserService;
	
	/**
	 * 跳转到公告管理页面
	 * @return 返回结果
	 */
	@RequestMapping("/noticelist.action")
	public ModelAndView noticeList(TagView view){
		List categorys = newsCenterService.searchNoticeTag(view);
		ModelAndView mv = new ModelAndView("newsCenter/manager/newsCenter_list");
		mv.addObject("categorys", categorys);
		mv = getRoleAndNum(mv);
		return mv;
	}
	
	/**
	 * 跳转到公告添加页面
	 * @return 返回结果
	 */
	
	@RequestMapping("/noticeadd.action")
	public ModelAndView noticeAdd(TagView view){
		List categorys = newsCenterService.searchNoticeTag(view);
		ModelAndView mv = new ModelAndView("newsCenter/manager/newsCenter_add");
		mv.addObject("categorys", categorys);
		mv = getRoleAndNum(mv);
		return mv;
	}
	/**
	 * 跳转到公告编辑页面
	 * @param id
	 * @return 返回结果
	 */
	@RequestMapping("/noticeedit.action")
	public ModelAndView noticeEdit(String id){
		List categorys = newsCenterService.searchNoticeTag(new TagView());
		NewsCenterEntity entity = new NewsCenterEntity();
		entity.setId(id);
		NewsCenterEntity condition = newsCenterService.selectNotice(entity);
		ModelAndView mv = new ModelAndView("newsCenter/manager/newsCenter_edit");
		mv.addObject("categorys", categorys);
		mv.addObject("notice", condition);
		mv = getRoleAndNum(mv);
		return mv;
	}
	/**
	 * 检索公告分类信息
	 * @param entity
	 * @return 返回结果
	 *//*
	@RequestMapping("/searchcategory.action")
	@ResponseBody
	public List searchCategory(InfomationCategoryEntity entity){
		return newsCenterService.search(entity);
	}*/
	
	/**
	 * 检索公告信息
	 * 根据公告分类
	 * @param entity
	 * @return 返回结果
	 */
	@RequestMapping("/searchnotice.action")
	@ResponseBody
	public ResultBean searchNotice(NewsCenterEntity entity){
		return newsCenterService.searchNotice(entity);
	}
	/**
	 * 新增公告信息
	 * @param entity
	 * @return 返回结果
	 */
	@RequestMapping("/insertnotice.action")
	@ResponseBody
	public ResultBean insertNotice(NewsCenterEntity entity) throws Exception {
		entity.setId(IDGenerator.genUID());
		if(StringUtil.isEmpty(entity.getContent())){
			return ResultBean.error(CONTENT_CANNOT_EMPTY);
		}
		newsCenterService.insertNotice(entity);
		
		TagView view = new TagView();
		view.setTagId(entity.getCategoryId());
		view.setLinkId(entity.getId());
		view.setLinkType(NOTICE_LINK_TYPE);
		
		newsCenterService.insertNoticeType(view);
		return ResultBean.success(MSG_ADD_NOTICE_SUCCESS);
	}
	/**
	 * 编辑保存
	 * @param entity
	 * @return 返回结果
	 */
	@RequestMapping("/updatenotice.action")
	@ResponseBody
	public ResultBean updateNotice(NewsCenterEntity entity) throws Exception{
		if(StringUtil.isEmpty(entity.getContent())){
			return ResultBean.error(CONTENT_CANNOT_EMPTY);
		}
		newsCenterService.editNotice(entity);
		
		TagView view = new TagView();
		view.setTagId(entity.getCategoryId());
		view.setLinkId(entity.getId());
		view.setLinkType(NOTICE_LINK_TYPE);
		
		newsCenterService.editNoticeType(view);
		return ResultBean.success(EDIT_NOTICE_SUCCESS);
	}
	/**
	 * 发布公告
	 * @param entity
	 * @return 返回结果
	 */
	@RequestMapping("/statusinfo.action")
	@ResponseBody
	public ResultBean isStatus(NewsCenterEntity entity){
		return 	newsCenterService.isStatus(entity);
	}
	/**
	 * 删除公告
	 * @param entity
	 * @return 返回结果
	 */
	@RequestMapping("/deleteinfo.action")
	@ResponseBody
	public ResultBean deleteInfo(NewsCenterEntity entity)throws Exception{
		newsCenterService.deleteInfo(entity);
		return ResultBean.success(DELETE_NOTICE_SUCCESS);
	}
	
	@RequestMapping("/toIndex.action")
	public ModelAndView toIndex(String categoryId){
		ModelAndView mv = new ModelAndView();
		mv.setViewName("newsCenter/client/newsCenter_index");
		mv.addObject("categoryId", categoryId);
		int num = courseService.getStuStatus();
		mv.addObject("num", num);
		UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}
	
	@RequestMapping("/searchInfoByCatId.action")
	@ResponseBody
	public ResultBean searchInfoByCategory(NewsCenterEntity info){
		ResultBean result = new ResultBean();
		info.setSortName(INFO_SORT_NAME);
		info.setSortOrder(DESC);
		info.setStatus(INFOMATION_STATUS_ALREADY);
		UserEntity user = WebContext.getSessionUser();
    	//判断是否管理员
    	ManagerUserEntity entity = managerUserService.searchByUserId(user.getUserId());
    	if(null!=entity || "2542668".equals(user.getUserId())) {
    		 result = newsCenterService.searchInfoByCategoryId(info);
    	}else {
    		if("A".equals(user.getUserKbn())) {
    			 result = newsCenterService.searchTeacherInfoByCategoryId(info);
    		}else if("C".equals(user.getUserKbn())) {
    			 result = newsCenterService.searchStudentInfoByCategoryId(info);
    		}
    	}
		
		return result;
	}
	
	@RequestMapping("/searchCategory.action")
	@ResponseBody
	public ResultBean searchCategory(){
		TagView view = new TagView();
		view.setSortName(SORT_NAME);
		view.setSortOrder(SORT_ASC);
		ResultBean result = newsCenterService.searchCategory(view);
		return result;
	}
	
	@RequestMapping("/toInfoDetail.action")
	public ModelAndView toInfoDetail(String infoId){
		ModelAndView mv = new ModelAndView();
		mv.setViewName("newsCenter/client/newsCenter_infoDetail");
		mv.addObject("infoId", infoId);
		int num = courseService.getStuStatus();
		mv.addObject("num", num);
		UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}
	
	@RequestMapping("/searchInfoDetail.action")
	@ResponseBody
	public ResultBean searchInfoById(String infoId){
		ResultBean result = new ResultBean();
		if(infoId == null || infoId.equals("")){
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_NULL_ID));
			return result;
		}
		NewsCenterEntity info = new NewsCenterEntity();
		info.setId(infoId);
		info = newsCenterService.searchInfoById(info);
		if(info == null){
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_NULL_DATA));
			return result;
		}
		result.setStatus(true);
		result.setData(info);
		return result;
	}
	
	private ModelAndView getRoleAndNum(ModelAndView result) {
		int num = courseService.getStuStatus();
    	result.addObject("num", num);
    	UserEntity user = WebContext.getSessionUser();
    	//判断是否管理员
    	ManagerUserEntity entity = managerUserService.searchByUserId(user.getUserId());
    	if(!user.getUserName().equals("guest")) {
    		List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
    		//老师和教育局角色
    		if(roleList.size() > 0 && "A".equals(user.getUserKbn()) && null==entity && !"2542668".equals(user.getUserId())) {
    			for(UserRoleEntity role : roleList) {
    				if(role.getRoleId().equals("01")) {
    					result.addObject("role", "t");
    				}
    			}
    		//教育局角色	
    		}else if(roleList.size() > 0  && (null!=entity || "2542668".equals(user.getUserId()))){
    			result.addObject("role", "m");
    		}else if("B".equals(user.getUserKbn())){
    			result.addObject("role", "e");
    	    //家长和教研员角色
    		}else if("D".equals(user.getUserKbn()) || "E".equals(user.getUserKbn())){
    			result.addObject("role", "q");
    		//学生角色
    		}else {
    			result.addObject("role", "s");
    		}
    	}

    	if(null!=entity || "2542668".equals(user.getUserId())) {
    		result.addObject("manager", "1");
    	}else {
    		result.addObject("manager", "0");
    	}
    	return result;
    }
}
