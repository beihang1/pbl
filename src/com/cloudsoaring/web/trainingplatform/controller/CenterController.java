package com.cloudsoaring.web.trainingplatform.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.exception.BusinessException;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.Message;
import com.cloudsoaring.web.bus.entity.TagEntity;
import com.cloudsoaring.web.bus.entity.UserEntity;
import com.cloudsoaring.web.bus.service.MessageService;
import com.cloudsoaring.web.common.conotroller.BaseController;
import com.cloudsoaring.web.common.constant.MsgNames;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.common.view.ResultBean.ErrorCode;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.MeasurementEntity;
import com.cloudsoaring.web.course.entity.QuestionOptionEntity;
import com.cloudsoaring.web.course.entity.TeacherApplyEntity;
import com.cloudsoaring.web.course.entity.UserCourseEntity;
import com.cloudsoaring.web.course.entity.UserMeasurementResultEntity;
import com.cloudsoaring.web.course.service.CourseService;
import com.cloudsoaring.web.course.service.MeasuermentService;
import com.cloudsoaring.web.course.service.TagService;
import com.cloudsoaring.web.course.view.HomeworkView;
import com.cloudsoaring.web.course.view.PlanView;
import com.cloudsoaring.web.trainingplatform.constants.TpConstants;
import com.cloudsoaring.web.trainingplatform.entity.TpUserEntity;
import com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity;
import com.cloudsoaring.web.trainingplatform.service.UserManagementService;
import com.cloudsoaring.web.trainingplatform.web.TpSessionTimeoutFilter;


/**
 * 个人中心
 * @author hanjun
 *
 */

@Controller
@RequestMapping("/client/center")
public class CenterController extends BaseController implements TpConstants{
	
	@Autowired
	private UserManagementService userManagementService;
	/**消息处理Service*/
	@Autowired
	private MessageService messageService;
	@Autowired
	private TagService tagService;
	@Autowired
    private CourseService courseService;
	 @Autowired
	private  MeasuermentService measuermentService;
	/**
	 * 用户信息
	 * @return 返回结果
	 */
	@RequestMapping("/userInfo.action")
	public ModelAndView userInfo(){
		int num = courseService.getStuStatus();
		ModelAndView mv = new ModelAndView("trainingplatform/client/center/center_user");
		UserEntity user = userManagementService.seachUserInfo(WebContext.getSessionUserId());
		mv.addObject("user", user);
		mv.addObject("num", num);
		UserEntity userRole = WebContext.getSessionUser();
		if(!userRole.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(userRole.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}
	
	
	/**
	 * 保存用户信息
	 * @param user
	 * @return 返回结果
	 * @throws Exception 
	 */
	@RequestMapping("/usersave.action")
	@ResponseBody
	public ResultBean userSave(TpUserEntity user) throws Exception{
		ResultBean result = new ResultBean();
		UserEntity newUserInfo = userManagementService.updateForSelf(user, getCompressConfigByName("imageP"));
		session(SESSION_KEY_USER, newUserInfo);
		result.setData(user);
		result.setMessages(getMessage(MsgNames.MSG_S_SAVE));
		WebContext.refresh();
		return result;
	}
	
	/**
	 * check用户邮箱
	 * @param user
	 * @return 返回结果
	 */
	@RequestMapping("/checkUserEmail.action")
	@ResponseBody
	public ResultBean checkUserEmail(TpUserEntity user){
		ResultBean result = new ResultBean();
		result.setStatus(true);
		if(StringUtil.isEmpty(user.userId)){
			return result;
		}
		if(StringUtil.isEmpty(user.email)){
			return result;
		}
		//判断用户名是否存在
		boolean userNameExist = userManagementService.emailExist(user);
		if(userNameExist){
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_EMAIL_EXIST, ""));
		}
		return result;
	}
	
	/**
	 * 个人中心已收藏课程初始化
	 * @param tagTypeId
	 * @return
	 */
	@RequestMapping("/userCourseFavoriteList.action")
	public  ModelAndView userCourseListFavorite(){
		ModelAndView mv = new ModelAndView("/trainingplatform/client/center/userCourse_FavoriteList");
		int num = courseService.getStuStatus();
		mv.addObject("num", num);
		UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}
	
	/**
	 * 个人中心已学课程初始化
	 * @param tagTypeId
	 * @return
	 */
	@RequestMapping("/userCourseStudyList.action")
	public  ModelAndView userCourseStudyList(){
		int num = courseService.getStuStatus();
		ModelAndView mv = new ModelAndView("/trainingplatform/client/center/userCourse_StudyList");
		mv.addObject("num", num);
		UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}
	
	@RequestMapping("/compulsoryCoursesList.action")
	public  ModelAndView compulsoryCoursesList(){
		int num = courseService.getStuStatus();
		ModelAndView mv = new ModelAndView("/trainingplatform/client/center/compulsoryCoursesList");
		mv.addObject("num", num);
		UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}
	
	/**必修课*/
	@RequestMapping("/electiveCoursesList.action")
	public  ModelAndView electiveCoursesList(){
		int num = courseService.getStuStatus();
		ModelAndView mv = new ModelAndView("/trainingplatform/client/center/electiveCoursesList");
		mv.addObject("num", num);
		UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}
	
	/**

     * 我的计划（已参与）初始化
     * @return
     */
	@RequestMapping("/userPlanJoinListInit.action")
	public ModelAndView userPlanJoinListInit(String planType){
		ModelAndView mv = new ModelAndView();
			mv.setViewName("/trainingplatform/client/center/user_plan_join_list");
			planType="STATE_PLAN_TYPE_JOIN";
			mv.addObject("planType",planType);
			return mv;  
	}

	
	/**
     * 我的计划（已完成）初始化
     * @return
     */
	@RequestMapping("/userPlanFinishListInit.action")
	public ModelAndView userPlanFinishListInit(String planType){
		ModelAndView mv = new ModelAndView();
			mv.setViewName("/trainingplatform/client/center/user_plan_finish_list");
			planType="STATE_PLAN_TYPE_FINISH";
			mv.addObject("planType",planType);
			return mv;  
	}
	
	/**

     * 我的案例（已参与）初始化
     * @return
     */
	@RequestMapping("/userCaseJoinListInit.action")
	public ModelAndView userCaseJoinListInit(String planType){
		ModelAndView mv = new ModelAndView();
			mv.setViewName("/trainingplatform/client/center/user_case_join_list");
			planType="STATE_PLAN_TYPE_JOIN";
			mv.addObject("planType",planType);
			return mv;  
	}

	
	/**
     * 我的案例（已完成）初始化
     * @return
     */
	@RequestMapping("/userCaseFinishListInit.action")
	public ModelAndView userCaseFinishListInit(String planType){
		ModelAndView mv = new ModelAndView();
			mv.setViewName("/trainingplatform/client/center/user_case_finish_list");
			planType="STATE_PLAN_TYPE_FINISH";
			mv.addObject("planType",planType);
			return mv;  
	}
	/**
	 * 我的投票初始化
	 */
	@RequestMapping("/userVoteList.action")
	@ResponseBody
	public ResultBean userVoteList(UserCourseEntity uCourse) {
		ResultBean result = userManagementService.userVoteList(uCourse);
		return result;
	}
	/**
	 * 我的投票
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/userVote.action")
	public ModelAndView userVote(UserCourseEntity uCourse) {
		ResultBean result = userManagementService.userVoteList(uCourse);
		List<UserCourseEntity> UCEList = (List<UserCourseEntity>) result.getData();
		ModelAndView mv = new ModelAndView();
		mv.addObject(UCEList);
		mv.setViewName("/trainingplatform/client/center/userCourse_VoteList");
		int num = courseService.getStuStatus();
		mv.addObject("num", num);
		UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}
	/**
	 * 我的测试初始化
	 */
	@RequestMapping("/userMeasurementList.action")
	@ResponseBody
	public ResultBean userMeasurementList(MeasurementEntity measEntity) {
		ResultBean result = userManagementService.userMeasurementList(measEntity);
		return result;
	}
	/**
	 * 我的测试
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/userMeasurement.action")
	public ModelAndView userMeasurement(MeasurementEntity measEntity) {
		ResultBean result = userManagementService.userMeasurementList(measEntity);
		List<MeasurementEntity> meList = (List<MeasurementEntity>) result.getData();
		ModelAndView mv = new ModelAndView();
		mv.addObject(meList);
		mv.setViewName("/trainingplatform/client/center/userCourse_MeasurementList");
		int num = courseService.getStuStatus();
		mv.addObject("num", num);
		UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}
	/**
	 * 我的考试
	 * @param measEntity
	 * @return
	 */
	@RequestMapping("/userExam.action")
    @ResponseBody
    public ResultBean userExam(MeasurementEntity measEntity) {
        ResultBean result = userManagementService.userExamList(measEntity);
        return result;
    }
	/**
	 * 我的考试
	 * @param measEntity
	 * @return
	 */
	@SuppressWarnings("unchecked")
    @RequestMapping("/userExamInit.action")
    public ModelAndView userExamInit(MeasurementEntity measEntity) {
        ResultBean result = userManagementService.userExamList(measEntity);
        List<MeasurementEntity> meList = (List<MeasurementEntity>) result.getData();
        ModelAndView mv = new ModelAndView();
        mv.addObject(meList);
        mv.setViewName("/trainingplatform/client/center/userCourse_examList");
        int num = courseService.getStuStatus();
        mv.addObject("num", num);
        UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				mv.addObject("role", "s");
			}
		}
        return mv;
    }
	
	/**
	 * 作业管理页面初始化
	 * @param
	 * @return
	 */
	@RequestMapping("/userHomeWorkInit.action")
	public  ModelAndView homeWorkInit(){
		ModelAndView mv = new ModelAndView("/trainingplatform/client/center/all_home_work");
		int num = courseService.getStuStatus();
		mv.addObject("num", num);
		UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}
	
	/**
	 * 作业管理
	 */
	@RequestMapping("searchHomeworkList.action")
	@ResponseBody
	public ResultBean searchHomeworkList(HomeworkView homeworkView) {
		return userManagementService.searchHomeworkList(homeworkView);
		
	}

	
	
	/**
	 * 我的笔记初始化
	 * @return
	 */
	@RequestMapping("/userNoteListInit.action")
	public  ModelAndView userNoteListInit(String type){
		ModelAndView mv = new ModelAndView("/trainingplatform/client/center/center_note_list");
		type="USER_COLLECT_NOTE";
		mv.addObject("type",type);
		int num = courseService.getStuStatus();
		mv.addObject("num", num);
		UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}

	/**
	 * 我的评论页面 初始化
	 * @param condition
	 * @return 返回结果
	 */
	@RequestMapping("/userDiscussListInit.action")
	public ModelAndView userDiscussListInit(String discussType){
		ModelAndView mv = new ModelAndView("/trainingplatform/client/center/center_discuss_list");
		int num = courseService.getStuStatus();
		mv.addObject("num", num);
		UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}
	
	
	/**
	 * 我的问答初始化
	 * @retrun 返回结果
	 */
	@RequestMapping("/userFaqListInit.action")
	public  ModelAndView userFaqListInit(String type){
		ModelAndView mv = new ModelAndView("/trainingplatform/client/center/center_faq_list");
		type="0";
		mv.addObject("type",type);
		int num = courseService.getStuStatus();
		mv.addObject("num", num);
		UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}
	
	/**
	 * 我的作业列表初始化
	 * @return
	 */
	@RequestMapping("/userHomeworkListInit.action")
	public  ModelAndView userHomeworkListInit(){
		ModelAndView mv = new ModelAndView("/trainingplatform/client/center/user_homeWork_list");
		int num = courseService.getStuStatus();
		mv.addObject("num", num);
		UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}
	
	
	/**
	 * 我的论坛初始化
	 * @param type  检索类型
	 * @return
	 */
	@RequestMapping("/userForumListInit.action")
	public  ModelAndView userForumListInit(String type){
		ModelAndView mv = new ModelAndView("/trainingplatform/client/center/user_forum_list");
		type="MY_FORUM_TYPE";
		mv.addObject("type",type);
		return mv;
	}
	
	/**
	 * 问卷调查列表
	 * @author heyaqin
	 * @return 返回结果
	 */
	@RequestMapping("/questionList.action")
	public ModelAndView questionList(MeasurementEntity condition){
		ModelAndView mv = new ModelAndView("trainingplatform/client/teacher/teacher_question_list");
		mv.addObject("condition", condition);
		int num = courseService.getStuStatus();
		mv.addObject("num", num);
		UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}
	/**
	 * 问卷参与人员 画面初始化
	 * @param condition
	 * @return
	 */
	@SuppressWarnings("unchecked")
    @RequestMapping("/viewUserInit.action")
    public ModelAndView viewUserInit(MeasurementEntity condition){
        ModelAndView mv = new ModelAndView("trainingplatform/client/teacher/teacher_question_user_list");
        mv.addObject("measurementId", condition.getId());
        List<MeasurementEntity> list = courseService.search(condition);
        if(list != null && list.size() != 0) {
            mv.addObject("condition", list.get(0));
        } else {
            throw new BusinessException("参数错误!");
        }
        int num = courseService.getStuStatus();
		mv.addObject("num", num);
		UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				mv.addObject("role", "s");
			}
		}
        return mv;
    }
	/**
     * 问卷参与人员 检索
     * @param condition
     * @return
     */
    @RequestMapping("/viewUserSearch.action")
    @ResponseBody
    public ResultBean viewUserSeacher(String  measurementId){
        return userManagementService.selectQuestionUserList(measurementId);
    }
    /**
     * 问卷参与人员回答详情
     * @param condition
     * @return
     */
    @RequestMapping("/viewDetailInit.action")
    public ModelAndView viewDetailInit(UserMeasurementResultEntity condition){
        ModelAndView mv = new ModelAndView("trainingplatform/client/teacher/teacher_question_user_detail");
        if(StringUtil.isEmpty(condition.getMeasurementId())){
            throw new BusinessException("参数错误!");
        }
        
        List<UserMeasurementResultEntity> list = measuermentService.searchUserMeasurementResultByEntiy(condition);
        if(list != null && list.size() != 0) {
            mv.addObject("condition", list.get(0));
        } else {
            mv.addObject("condition", new UserMeasurementResultEntity());
        }
        return mv;
    }
    /**
     * 问卷回答详情
     * @param measurementId
     * @param userId
     * @return
     */
    @RequestMapping("/viewUserAnswerSearch.action")
    @ResponseBody
    public ResultBean viewUserAnswerSearch(String  measurementId,String userId){
        return userManagementService.selectAnswerUserList(measurementId,userId);
    }
	/**
	 * 删除问卷调查列表
	 * @author heyaqin
	 * @return 返回结果
	 */
	@RequestMapping("/deleteQuestionList.action")
	@ResponseBody
	public ResultBean deleteQuestionList(String id){
		userManagementService.deleteQuestionList(id);
		return ResultBean.success();
	}
	
	

	/**
	 * 问卷调查列表检索
	 * @author 
	 * @return 返回结果
	 */
	@RequestMapping("/questionListSearch.action")
	@ResponseBody
	public ResultBean questionListSearch(MeasurementEntity condition){
		return userManagementService.selectQuestionList(condition);
	}
	
	/**
	 * 问卷统计列表初始化
	 * @author HEYAQIN
	 * @param id 问卷ID
	 * @return 返回结果
	 */
	@RequestMapping("/viewMeasurementListInit.action")
	public ModelAndView viewMeasurementListInit(String id) {
		ModelAndView mv = new ModelAndView("trainingplatform/client/teacher/teacher_question_statistics");
		int optionNum = userManagementService.searchTotalOptions(id)+1;
		QuestionOptionEntity questionOptionEntity = new QuestionOptionEntity();
		questionOptionEntity.setMeasureId(id);
		mv.addObject("questionOptionEntity",questionOptionEntity);
		mv.addObject("optionNum",optionNum);
		//问卷统计结果列表
		ArrayList<Map<String, String>> optionMapList = userManagementService.selectOptionsList(id);
		mv.addObject("optionMapList",optionMapList);
		int num = courseService.getStuStatus();
		mv.addObject("num", num);
		UserEntity user = WebContext.getSessionUser();
		if(!user.getUserName().equals("guest")) {
			List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
			if(roleList.size() > 0) {
				for(UserRoleEntity role : roleList) {
					if(role.getRoleId().equals("01")) {
						mv.addObject("role", "t");
					}
				}
			}else {
				mv.addObject("role", "s");
			}
		}
		return mv;
	}
	
	/**
	 * 问卷统计列表检索
	 * @author HEYAQIN
	 * @param id 问卷ID
	 * @return 返回结果
	 */
	@RequestMapping("/viewMeasurementList.action")
	@ResponseBody
	public ArrayList<Map<String, String>> viewMeasurementList(String id) {
		if (StringUtil.isEmpty(id)) {
			return null ;
		}else {
			ArrayList<Map<String, String>> optionMapList = userManagementService.selectOptionsList(id);
			return optionMapList;
		}
	}
	
	/**
	 * 计划管理初始化
	 * @param UserEntity
	 * @return
	 */	
	@RequestMapping("/userManagePlanInit.action")
	public ModelAndView userManagePlanInit() {
		ModelAndView mv = new ModelAndView("trainingplatform/client/center/userManage_paln");
		return mv;
	}

	/**
	 * 计划管理
	 * @param UserEntity
	 * @return
	 */
	@RequestMapping("/userManagePlan.action")
	@ResponseBody
	public ResultBean userManagePlan(PlanView planView) {
		planView.setCategory(CourseConstants.PLAN_CATEGORY_PLAN);
		ResultBean result = userManagementService.userManagePlan(planView);
		return result;				
	}
	
	/**
	 * 案例管理初始化
	 * @param UserEntity
	 * @return
	 */	
	@RequestMapping("/userManageCaseInit.action")
	public ModelAndView userManageCaseInit() {
		ModelAndView mv = new ModelAndView("trainingplatform/client/center/userManage_case");
		return mv;
	}

	/**
	 * 案例管理
	 * @param UserEntity
	 * @return
	 */
	@RequestMapping("/userManageCase.action")
	@ResponseBody
	public ResultBean userManageCase(PlanView planView) {
		planView.setCategory(CourseConstants.PLAN_CATEGORY_CASE);
		ResultBean result = userManagementService.userManagePlan(planView);
		return result;				
	}
	
	/**
	 * 计划管理用户
	 * @param UserEntity
	 * @return
	 */	
	@RequestMapping("/userManagePlanUserInit.action")
	public ModelAndView userManagePlanUserInit(@RequestParam("id") String id) {	
		ModelAndView mv = new ModelAndView("trainingplatform/client/center/userManage_palnUser");
		mv.addObject("id", id);
		return mv;
	}
	
	/**
	 * 案例管理用户
	 * @param UserEntity
	 * @return
	 */	
	@RequestMapping("/userManageCaseUserInit.action")
	public ModelAndView userManageCaseUserInit(@RequestParam("id") String id) {	
		ModelAndView mv = new ModelAndView("trainingplatform/client/center/userManage_caseUser");
		mv.addObject("id", id);
		return mv;
	}
	
	/**
	 * 教师审批
	 * @return
	 */
	@RequestMapping("/centerApplyTeacher.action")
	public  ModelAndView centerApplyTeacher(TeacherApplyEntity teacherApplyEntity){
		ModelAndView mv = new ModelAndView("trainingplatform/client/center/center_applyTeacher");
		List<TagEntity> tags=tagService.searchTagAll();
		TeacherApplyEntity teacherApply=userManagementService.searchTeacherApply(teacherApplyEntity);
		if(teacherApply==null){
			teacherApply=new TeacherApplyEntity();			
		}
		boolean flag = userManagementService.searchUserIsTeacher();
		mv.addObject("flag",flag);
		mv.addObject("teacherApply",teacherApply);
		mv.addObject("tags",tags);
		return mv;
	}
	
	/***
	 * 应聘讲师，是否有提交记录
	 * @return
	 */
	@RequestMapping("/centerApplyTeacherStatus.action")
	@ResponseBody
	public ResultBean centerApplyTeacherStatus(){
		TeacherApplyEntity teacherApply = new TeacherApplyEntity();
		teacherApply = userManagementService.searchTeacherApply(teacherApply);
		ResultBean result = ResultBean.success().setData(teacherApply);
		return result;
	}
	
	/**
	 * 跳转到用户个人中心-我的消息页面
	 * @return
	 */
	@RequestMapping("/myMessages.action")
	public ModelAndView toUserMessages(){
		ModelAndView mv = new ModelAndView("trainingplatform/client/center/user_messages");
		//检索当前登录用户的消息
		Message entity = new Message();
		//设置每页显示多少条
		entity.setPageSize(DEFAULT_MY_MESSAGES_PAGE_SIZE);
		entity.setSortName("CREATE_DATE");
		entity.setSortOrder("DESC");
		ResultBean result = messageService.pageAllMessages(entity);
		JSONObject json = JSONObject.fromObject(result);
		mv.addObject("messagesResult", json.toString());
		//检索当前用户有多少未读消息
		Integer unReadCount = messageService.searchUnreadCount();
		mv.addObject("unReadCount", unReadCount);
		return mv;
	}
	
	/**
	 * 分页检索当前登录用户的消息
	 * @param condition condition中主要设置参数 pageNumber:检索第几页，pageSize:每页显示多少条,status：已读未读状态
	 * @return
	 */
	@RequestMapping("/searchMyMessages.action")
	@ResponseBody
	public ResultBean searchUserMessages(Message condition){
		if(condition!=null){
			condition.setSortName("CREATE_DATE");
			condition.setSortOrder("DESC");
		}
		return messageService.pageAllMessages(condition);
	}
	
	/**
	 * 讲消息状态变更为已读
	 * @param messageIds 消息ID以逗号分隔字符串
	 * @return
	 */
	@RequestMapping("/flagMessageRead.action")
	@ResponseBody
	public ResultBean flagMessageRead(String messageIds){
		assertNotEmpty(messageIds, MSG_E_EMPTY_MESSAGE_ID);
		messageService.readMessages(messageIds.split(","));
		return ResultBean.success();
	}
	
	/***
	 * 删除消息
	 * @param messagesIds 消息ID以逗号分隔字符串
	 * @return
	 */
	@RequestMapping("/deleteMessages.action")
	@ResponseBody
	public ResultBean deleteMessages(String messageIds){
		assertNotEmpty(messageIds, MSG_E_EMPTY_MESSAGE_ID);
		messageService.deleteMessages(messageIds.split(","));
		return ResultBean.success();
	}
	/**
	 * 检索当前用户信息
	 * @return
	 */
	@RequestMapping("/searchUserInformation.action")
	@ResponseBody
	public ResultBean searchUserInformation(){
		//判断 是否配置了获取用户信息的URL
		boolean isValid = TpSessionTimeoutFilter.validUserInfo(getRequest());
		if(!isValid){
			throw new BusinessException(ErrorCode.NOT_LOGIN.toString(), getMessage(MSG_E_NOT_LOGIN));
		}else{
			return  userManagementService.searchUserInformation();
		}
	}
}
