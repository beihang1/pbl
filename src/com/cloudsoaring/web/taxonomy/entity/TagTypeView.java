package com.cloudsoaring.web.taxonomy.entity;

import com.cloudsoaring.web.common.entity.BaseEntity;
import com.cloudsoaring.web.common.entity.ITreeItem;


/**   
 * @Title: Entity
 * @Description: TB_TAG_TYPE-标签类
 * @date 2015-07-01
 * @version V1.0
 *
 */
public class TagTypeView extends BaseEntity implements Comparable<TagTypeView>, ITreeItem{
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	/**标签类ID*/
	private String tagTypeId;
	/**标签类名*/
	private String tagTypeName;
	/**标签类型,1单值，2区间*/
	private String tagClass;
	/**删除标记*/
	private String flagDel;
	/**备注*/
	private String remarks;
	/**锁定标记 */
	private String flagLock;
	/**父标签类ID*/
	private String parentId;
	/**业务类型*/
	private String businessType;
	
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	public String getBusinessType() {
		return businessType;
	}
	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}
	public String getTagTypeId(){
		return this.tagTypeId;
	}
	public void setTagTypeId(String tagTypeId){
		this.tagTypeId = tagTypeId;
	}
	public String getTagTypeName(){
		return this.tagTypeName;
	}
	public void setTagTypeName(String tagTypeName){
		this.tagTypeName = tagTypeName;
	}
	public String getTagClass(){
		return this.tagClass;
	}
	public void setTagClass(String tagClass){
		this.tagClass = tagClass;
	}
	public String getFlagDel(){
		return this.flagDel;
	}
	public void setFlagDel(String flagDel){
		this.flagDel = flagDel;
	}
	public String getRemarks(){
		return this.remarks;
	}
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	public String getFlagLock() {
		return flagLock;
	}
	public void setFlagLock(String flagLock) {
		this.flagLock = flagLock;
	}
	@Override
	public String getTreeId() {
		// TODO Auto-generated method stub
		return tagTypeId;
	}
	@Override
	public String getTreeParentId() {
		// TODO Auto-generated method stub
		return parentId;
	}
	@Override
	public String getTreeName() {
		// TODO Auto-generated method stub
		return tagTypeName;
	}
	@Override
	public int getTreeOrder() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public int compareTo(TagTypeView o) {
		// TODO Auto-generated method stub
		return 1;
	}
	/* (non-Javadoc)
	 * @see com.cloudsoaring.web.common.entity.ITreeItem#disabled()
	 */
	@Override
	public boolean disabled() {
		// TODO Auto-generated method stub
		return false;
	}
}