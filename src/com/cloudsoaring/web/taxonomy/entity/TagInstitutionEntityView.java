package com.cloudsoaring.web.taxonomy.entity;
import java.util.ArrayList;
import java.util.List;

import com.cloudsoaring.web.common.entity.BaseEntity;
import com.cloudsoaring.web.common.entity.ITreeItem;
/**
 * 学校标签实体类
 * @author Admin
 *
 */
@SuppressWarnings("serial")
public class TagInstitutionEntityView extends  BaseEntity implements Comparable<TagInstitutionEntityView>, ITreeItem{
	/**标签ID*/
	private String tagId;
	/**标签类ID*/
	private String tagTypeId;
	/**标签名*/
	private String tagName;
	/**标签下界值*/
	private String tagMinValue;
	/**标签上界值*/
	private String tagMaxValue;
	/**删除标记*/
	private String flagDel;
	/**权重*/
	private java.math.BigDecimal weight;
	/**父标签*/
	private String parentId;
	
	/**业务类型*/
	private String businessType;
	
	private String ico;
	/**父子类型*/
	private String type;
	/**标签*/
	private List<TagInstitutionEntityView> list=new ArrayList<TagInstitutionEntityView>();
	/**文件名*/
	private String fileName;
	/**标识*/
	private String flag;
	/**创建者*/
	private String createUser;
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List<TagInstitutionEntityView> getList() {
		return list;
	}

	public void setList(List<TagInstitutionEntityView> list) {
		this.list = list;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getIco() {
		return ico;
	}

	public void setIco(String ico) {
		this.ico = ico;
	}

	public String getTagId() {
		return tagId;
	}

	public void setTagId(String tagId) {
		this.tagId = tagId;
	}

	public String getTagTypeId() {
		return tagTypeId;
	}

	public void setTagTypeId(String tagTypeId) {
		this.tagTypeId = tagTypeId;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public String getTagMinValue() {
		return tagMinValue;
	}

	public void setTagMinValue(String tagMinValue) {
		this.tagMinValue = tagMinValue;
	}

	public String getTagMaxValue() {
		return tagMaxValue;
	}

	public void setTagMaxValue(String tagMaxValue) {
		this.tagMaxValue = tagMaxValue;
	}

	public String getFlagDel() {
		return flagDel;
	}

	public void setFlagDel(String flagDel) {
		this.flagDel = flagDel;
	}

	public java.math.BigDecimal getWeight() {
		return weight;
	}

	public void setWeight(java.math.BigDecimal weight) {
		this.weight = weight;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	@Override
	public String getTreeId() {
		// TODO Auto-generated method stub
		return tagId;
	}

	@Override
	public String getTreeParentId() {
		// TODO Auto-generated method stub
		return parentId;
	}

	@Override
	public String getTreeName() {
		// TODO Auto-generated method stub
		return tagName;
	}

	@Override
	public int getTreeOrder() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int compareTo(TagInstitutionEntityView o) {
		// TODO Auto-generated method stub
		return 1;
	}

	public String getBusinessType() {
		return businessType;
	}

	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}

	/* (non-Javadoc)
	 * @see com.cloudsoaring.web.common.entity.ITreeItem#disabled()
	 */
	@Override
	public boolean disabled() {
		// TODO Auto-generated method stub
		return false;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

}
