package com.cloudsoaring.web.taxonomy.entity;

import java.util.ArrayList;
import java.util.List;

import com.cloudsoaring.web.course.entity.QuestionEntity;
import com.cloudsoaring.web.course.entity.QuestionOptionEntity;
import com.cloudsoaring.web.institution.entity.QuestionOptionInstitutionEntity;
/**
 * 学校相关问题实体类
 * @author Admin
 *
 */
@SuppressWarnings("serial")
public class QuestionInstitutionView extends QuestionEntity {
	
	/**问卷标题*/
	private String titile;
	/**当前用户*/
	private String userId;
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getTitile() {
		return titile;
	}

	public void setTitile(String titile) {
		this.titile = titile;
	}

	/**题目list*/
	List<QuestionOptionInstitutionEntity> option = new ArrayList<QuestionOptionInstitutionEntity>();

	public List<QuestionOptionInstitutionEntity> getOption() {
		return option;
	}

	public void setOption(List<QuestionOptionInstitutionEntity> option) {
		this.option = option;
	}
	
}
