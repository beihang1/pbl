package com.cloudsoaring.web.taxonomy.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.bus.entity.FileEntity;
import com.cloudsoaring.web.bus.entity.TagEntity;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseStateConstants;
import com.cloudsoaring.web.taxonomy.constant.TaxonomyConstants;
import com.cloudsoaring.web.taxonomy.entity.TagEntityView;
import com.cloudsoaring.web.taxonomy.entity.TagTypeView;

/**
 * 课程分类管理
 * 
 * @author JGJ
 *
 */
@Service
public class TaxonomyService extends FileService implements TaxonomyConstants {

	/**
	 * 标签一览
	 * 
	 * @param BusinessType
	 *            业务类型
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ResultBean searchTagList(TagEntityView tag) {
		ResultBean result = new ResultBean();
		if (StringUtil.isEmpty(tag.getBusinessType())) {
			tag.setBusinessType(CourseStateConstants.TAG_BUSSINESS_TYPE_COURSE);
		}

		result = commonDao.searchList4Page(tag, SEARCH_TAG_LIST_BYTYPE);
		List<TagEntityView> list = (List<TagEntityView>) result.getData();
		if (list.size() == 0) {
			return result;
		}
		List<TagEntityView> listAll = new ArrayList<TagEntityView>();
		for (TagEntityView tagw : list) {
			listAll.add(tagw);
			for (TagEntityView taglist : tagw.getList()) {
				taglist.setType(CourseStateConstants.TAG_TYPE_RE);
				listAll.add(taglist);
			}

		}

		result.setData(listAll);

		return result;
	}

	/**
	 * 标签一览
	 * 
	 * @param BusinessType
	 *            业务类型
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ResultBean searchTagList2(TagEntityView tag) {
		ResultBean result = new ResultBean();
		if (StringUtil.isEmpty(tag.getBusinessType())) {
			tag.setBusinessType(CourseStateConstants.TAG_BUSSINESS_TYPE_PLAN);
		}

		result = commonDao.searchList4Page(tag, SEARCH_TAG_LIST_BYTYPE);
		List<TagEntityView> list = (List<TagEntityView>) result.getData();
		if (list.size() == 0) {
			return result;
		}
		List<TagEntityView> listAll = new ArrayList<TagEntityView>();
		for (TagEntityView tagw : list) {
			listAll.add(tagw);
			for (TagEntityView taglist : tagw.getList()) {
				taglist.setType(CourseStateConstants.TAG_TYPE_RE);
				listAll.add(taglist);
			}

		}

		result.setData(listAll);

		return result;
	}
	/**
	 * 保存新增标签
	 * 
	 * @param tagEntity
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saveTag.action")
	@ResponseBody
	public ResultBean saveTag(TagEntityView tagEntity) throws Exception {
		ResultBean result = new ResultBean();
		tagEntity.setTagId(IDGenerator.genUID());
		FileEntity resultData = processRequestFile("f1", "image");
		if (resultData != null) {
			tagEntity.setIco(resultData.getFileId());
		}

		if (StringUtil.isEmpty(tagEntity.getParentId())) {

			tagEntity.setBusinessType(StringUtil.getOrElse(
					tagEntity.getBusinessType(),
					CourseStateConstants.TAG_BUSSINESS_TYPE_COURSE));

			commonDao.insertData(tagEntity, INSERT_TAG_FA);
		} else {
			assertNotEmpty(tagEntity.getTagTypeId(), "标签类Id不能为空");
			commonDao.insertData(tagEntity, INSERT_TAG);
		}

		return result;

	}
	
	/**
	 * 保存新增标签
	 * 
	 * @param tagEntity
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/saveTag2.action")
	@ResponseBody
	public ResultBean saveTag2(TagEntityView tagEntity) throws Exception {
		ResultBean result = new ResultBean();
		tagEntity.setTagId(IDGenerator.genUID());
		FileEntity resultData = processRequestFile("f1", "image");
		if (resultData != null) {
			tagEntity.setIco(resultData.getFileId());
		}

		if (StringUtil.isEmpty(tagEntity.getParentId())) {

			tagEntity.setBusinessType(StringUtil.getOrElse(
					tagEntity.getBusinessType(),
					CourseStateConstants.TAG_BUSSINESS_TYPE_PLAN));

			commonDao.insertData(tagEntity, INSERT_TAG_FA);
		} else {
			assertNotEmpty(tagEntity.getTagTypeId(), "标签类Id不能为空");
			commonDao.insertData(tagEntity, INSERT_TAG);
		}

		return result;

	}

	/**
	 * 删除标签
	 * 
	 * @param tagIds
	 * @return
	 * @throws Exception
	 */
	public ResultBean delTag(String tagId, String ico) throws Exception {
		ResultBean result = new ResultBean();
		String[] tagIds = StringUtil.split(tagId, ",");
		if (tagIds != null&&tagIds.length>0) {
			int count = commonDao.searchCount(tagIds, SEARCH_TAG_LINK_COUNT);
			if (count > 0) {
				return result.setStatus(false).setMessages("不能删除正在使用的标签");
			}
			int countOne = commonDao.searchCount(tagIds, SEARCH_TAG_COUNT);
			if (countOne > 0) {
				return result.setStatus(false).setMessages("不能删除父标签");
			}

			commonDao.deleteData(tagIds, DELECTE_TAG);
			String[] icos = StringUtil.split(ico, ",");
			for (String ic : icos) {
				super.deleteFileById(ic, "image");
			}
		}
		return result;

	}

	/**
	 * 编辑标签
	 * 
	 * @param TagEntity
	 * @return
	 * @throws Exception
	 */
	public ResultBean updateTag(TagEntity tagEntity) throws Exception {
		ResultBean result = new ResultBean();
		FileEntity resultData = processRequestFile("f1", "image");
		if (resultData != null && StringUtil.isNotEmpty(resultData.getFileId())) {
			super.deleteFileById(tagEntity.getIco(), "image");
			tagEntity.setIco(resultData.getFileId());
		}
		commonDao.updateData(tagEntity, UPDATE_TAG);
		return result;

	}

	/**
	 * 检索子标签类
	 * 
	 * @param businessType
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<TagTypeView> searchTagTypeList(String businessType) {

		return commonDao.searchList(StringUtil.getOrElse(businessType,
				CourseStateConstants.TAG_BUSSINESS_TYPE_COURSE),
				SQL_SEARCH_TAG_TYPE_LIST);

	}

	/**
	 * 检索子标签类
	 * 
	 * @param businessType
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<TagTypeView> searchTagTypeList2(String businessType) {

		return commonDao.searchList(StringUtil.getOrElse(businessType,
				CourseStateConstants.TAG_BUSSINESS_TYPE_PLAN),
				SQL_SEARCH_TAG_TYPE_LIST);

	}
}
