package com.cloudsoaring.web.taxonomy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.TagEntity;
import com.cloudsoaring.web.bus.entity.UserEntity;
import com.cloudsoaring.web.common.entity.BaseEntity;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.entity.ManagerUserEntity;
import com.cloudsoaring.web.course.service.CourseService;
import com.cloudsoaring.web.course.service.ManagerUserService;
import com.cloudsoaring.web.taxonomy.entity.TagEntityView;
import com.cloudsoaring.web.taxonomy.entity.TagTypeView;
import com.cloudsoaring.web.taxonomy.service.TaxonomyService;
import com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity;

/**
 * 课程分类/标签
 * @author JGJ
 *
 */
@Controller
@RequestMapping("/manager/taxonomy")
public class TaxonomyController {

	@Autowired
	private TaxonomyService taxonomyService;
	@Autowired
    private CourseService courseService;
	@Autowired
	private ManagerUserService managerUserService;
	
	/**
	 * 页面初始化 
	 * @return
	 */
	@RequestMapping("/init.action")
	public  ModelAndView init(){
		ModelAndView mv = new ModelAndView("/taxonomy/manager/taxonomy_list");
		BaseEntity condition=new BaseEntity();
		//设置页面展示条数限制
		condition.setPageSize(5);
		mv.addObject("condition",condition);
		mv = getRoleAndNum(mv);
		//返回结果
		return mv;
	}
	
	/**
	 * 页面初始化 
	 * @return
	 */
	@RequestMapping("/courseinit.action")
	public  ModelAndView courseinit(){
		ModelAndView mv = new ModelAndView("/taxonomy/manager/taxonomy_course_list");
		BaseEntity condition=new BaseEntity();
		//设置页面展示条数限制
		condition.setPageSize(5);
		mv.addObject("condition",condition);
		//返回结果
		return mv;
	}
	
	


	/**
	 * 标签一览
	 * */
	@RequestMapping("/searchTagList.action")
	@ResponseBody
	public ResultBean searchTagList(TagEntityView tagEntity){	
		//返回结果
		return taxonomyService.searchTagList(tagEntity);
		
	}
	
	/**
	 * 标签一览
	 * */
	@RequestMapping("/searchTagList2.action")
	@ResponseBody
	public ResultBean searchTagList2(TagEntityView tagEntity){	
		//返回结果
		return taxonomyService.searchTagList2(tagEntity);
		
	}
	
	/**
	 * 保存新增标签
	 * @param parentId 父标签ID,tagTypeId:标签类ID,tagName:标签名称
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/saveTag.action")
	@ResponseBody
	public ResultBean saveTag(TagEntityView tagEntity) throws Exception{
		//返回结果
		return taxonomyService.saveTag(tagEntity);
		
	}
	
	/**
	 * 保存新增标签
	 * @param parentId 父标签ID,tagTypeId:标签类ID,tagName:标签名称
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/saveTag2.action")
	@ResponseBody
	public ResultBean saveTag2(TagEntityView tagEntity) throws Exception{	
		//返回结果
		return taxonomyService.saveTag2(tagEntity);
		
	}
	
	/**
	 * 
	 * 删除标签
	 * @param tagIds
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/delTag.action")
	@ResponseBody
	public ResultBean delTag(String tagId,String ico) throws Exception{
		//返回结果
		return taxonomyService.delTag(tagId,ico);	
		
	}
	
	/**
	 * 编辑标签
	 * @param TagEntity
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/updateTag.action")
	@ResponseBody
	public ResultBean updateTag(TagEntity tagEntity) throws Exception{
		//返回结果
		return taxonomyService.updateTag(tagEntity);	
		
	}
	/**
	 * 检索子标签类
	 * @param businessType 业务类型
	 * @return
	 */
	@RequestMapping("/searchTagTypeList.action")
	@ResponseBody
	public List<TagTypeView> searchTagTypeList(String businessType) {
		//返回结果
		return taxonomyService.searchTagTypeList(businessType);	
		
	}
	
	/**
	 * 检索子标签类
	 * @param businessType 业务类型
	 * @return
	 */
	@RequestMapping("/searchTagTypeList2.action")
	@ResponseBody
	public List<TagTypeView> searchTagTypeList2(String businessType) {
		//返回结果
		return taxonomyService.searchTagTypeList2(businessType);	
		
	}
	/**
	 * 角色信息收集
	 * @param result
	 * @return
	 */
	private ModelAndView getRoleAndNum(ModelAndView result) {
		int num = courseService.getStuStatus();
    	result.addObject("num", num);
    	UserEntity user = WebContext.getSessionUser();
    	//判断是否管理员
    	ManagerUserEntity entity = managerUserService.searchByUserId(user.getUserId());
    	if(!user.getUserName().equals("guest")) {
    		List<UserRoleEntity> roleList = courseService.getRoleByUserId(user.getUserId());
    		//老师和教育局角色
    		if(roleList.size() > 0 && "A".equals(user.getUserKbn()) && null==entity && !"2542668".equals(user.getUserId())) {
    			for(UserRoleEntity role : roleList) {
    				if(role.getRoleId().equals("01")) {
    					result.addObject("role", "t");
    				}
    			}
    		//教育局角色	
    		}else if(roleList.size() > 0  && (null!=entity || "2542668".equals(user.getUserId()))){
    			result.addObject("role", "m");
    		}else if("B".equals(user.getUserKbn())){
    			result.addObject("role", "e");
    	    //家长和教研员角色
    		}else if("D".equals(user.getUserKbn()) || "E".equals(user.getUserKbn())){
    			result.addObject("role", "q");
    		//学生角色
    		}else {
    			result.addObject("role", "s");
    		}
    	}
        //管理员角色
    	if(null!=entity || "2542668".equals(user.getUserId())) {
    		//是管理员
    		result.addObject("manager", "1");
    	}else {
    		//不是管理员
    		result.addObject("manager", "0");
    	}
    	return result;
	}

}
