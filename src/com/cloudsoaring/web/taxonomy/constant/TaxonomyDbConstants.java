package com.cloudsoaring.web.taxonomy.constant;

public interface TaxonomyDbConstants {
	/** 标签类列表 */
	public static final String SEARCH_TAG_TYPE_LIST = "TagView.searchTagTypeAllList";
	
	/** 标签类列表 */
	public static final String  SEARCH_TAG_TYPE_COUNT="TagView.searchTagTypeCount";
	
	/** 查询一个标签类*/
	public static final String SEARCH_ONE_TAG_TYPE="TagView.select";
	
	/** 标签列表 */
	public static final String SEARCH_TAG_LIST_BYTYPE="TagMap.searchTagListByType";
	/** 标签列表 */
	public static final String SEARCH_TAG_LIST_BYTYPE_INSTITUTION="TagInstitutionMap.searchTagListByType";
	/** 学校标签列表 */
	public static final String SCHOOL_TAG_LIST_BYTYPE_INSTITUTION="TagInstitutionMap.searchInstitutionTagListByType";
	/** 子标签列表 */
	public static final String SEARCH_TAG_SUB="TagMap.searchTagSub";
	/**
	 * 子标签个数
	 */
	public static final String SEARCH_TAG_COUNT="TagMap.searchTagCount";
	/** 新增一级标签 */
	public static final String INSERT_TAG_FA="TagMap.insertFa";
	/** 新增机构一级标签 */
	public static final String INSERT_TAG_FA_INSTITUTION="TagInstitutionMap.insertFa";
	
	/** 新增二级标签 */
	public static final String INSERT_TAG="TagMap.insert";
	
	/** 新增机构二级标签 */
	public static final String INSERT_TAG_INSTITUTION="TagInstitutionMap.insert";

	
	/** 新增二级标签 */
	public static final String INSERT_TAG_SUB="TagMap.insertSub";
	
	/** 编辑标签*/
	public static final String UPDATE_TAG="TagMap.updateTag";
	
	/** 编辑机构标签*/
	public static final String UPDATE_TAG_INSTITUTION="TagInstitutionMap.updateTag";
	
	/** 检索子标签类*/
	public static final String SQL_SEARCH_TAG_TYPE_LIST="TagView.searchTypeList";
	
	/** 检索机构子标签类*/
	public static final String SQL_SEARCH_TAG_TYPE_LIST_INSTITUTION="TagTypeInstitution.searchTypeList";
	
	/** 标签关联数量*/
	public static final String SEARCH_TAG_LINK_COUNT="TagLink.searchTagLinkCount";
	
	/** 标签机构关联数量*/
	public static final String SEARCH_TAG_LINK_COUNT_INSTITUTION="TagLinkInstitution.searchTagLinkCount";
	
	/** 删除标签*/
	public static final String DELECTE_TAG="TagMap.deleteTags";
	
	/** 删除机构标签*/
	public static final String DELECTE_TAG_INSTITUTION="TagInstitutionMap.deleteTags";
	
	/** 根据标签名查询标签数量*/
	public static final String SEARCH_COUNT_TAG="TagMap.searchCountTag";
	/**标签一级机构列表*/
    public static final String SQL_SEARCH_FIRST_TAG_TYPE_INSTITUTION="TagTypeInstitution.searchFirstTagType";
    /**标签二级机构列表*/
    public static final String SQL_SEARCH_SECOND_TAG_TYPE_INSTITUTION="TagTypeInstitution.searchSecondTagType";
    /**查询结构评价权限*/
    public static final String SELECT_INSTITUTION_LIST_MANAGER = "TbInstitutionEvalution.selectManagerList";
    /**查询结构待办评价权限*/
    public static final String SELECT_INSTITUTION_DB_LIST_MANAGER = "MeasurementInstitution.selectManagerDBList";
    /**查询结构已办评价权限*/
    public static final String SELECT_INSTITUTION_YB_LIST_MANAGER = "TbInstitutionEvalution.selectManagerYbList";
    /**查询结构评价权限*/
    public static final String SELECT_INSTITUTION_QUETION_LIST_MANAGER = "MeasurementInstitution.selectManagerList";
    /**查询结构权限评价权限*/
    public static final String SELECT_INSTITUTION_SCHOOL_QUETION_LIST_MANAGER = "MeasurementInstitution.selectSchoolManagerList";
    /**查询结构评价权限*/
    public static final String SELECT_QUETION_LIST_MANAGER = "UserMeasuermentAnswer.select";
    /**查询问卷结果权限*/
    public static final String SELECT_QUETION_RESULT_LIST_MANAGER = "UserMeasurementResult.select";
    /**标签一级机构列表*/
    public static final String SCHOOL_SEARCH_FIRST_TAG_TYPE_INSTITUTION="TagTypeInstitution.searchSchoolFirstTagType";
	
}
