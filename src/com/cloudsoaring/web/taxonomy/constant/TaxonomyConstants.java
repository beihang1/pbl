package com.cloudsoaring.web.taxonomy.constant;
/**
 * 
 * @author Admin
 *
 */
public interface TaxonomyConstants extends TaxonomyConfigNames, TaxonomyDbConstants, TaxonomyLogConstants, TaxonomyDefaultValues, TaxonomyMsgNames, TaxonomyStateConstants{
  
}
