/**
 * ExamToWebserviceServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.eggtwo.euq.service;

public class ExamToWebserviceServiceLocator extends org.apache.axis.client.Service implements com.eggtwo.euq.service.ExamToWebserviceService {

    public ExamToWebserviceServiceLocator() {
    }


    public ExamToWebserviceServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ExamToWebserviceServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ExamToWebservicePort
    private java.lang.String ExamToWebservicePort_address = "http://remote.hseduyun.com:50971/questionnaire_beihang/toExam/ToWebservice";

    public java.lang.String getExamToWebservicePortAddress() {
        return ExamToWebservicePort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ExamToWebservicePortWSDDServiceName = "ExamToWebservicePort";

    public java.lang.String getExamToWebservicePortWSDDServiceName() {
        return ExamToWebservicePortWSDDServiceName;
    }

    public void setExamToWebservicePortWSDDServiceName(java.lang.String name) {
        ExamToWebservicePortWSDDServiceName = name;
    }

    public com.eggtwo.euq.service.ExamToWebservice getExamToWebservicePort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ExamToWebservicePort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getExamToWebservicePort(endpoint);
    }

    public com.eggtwo.euq.service.ExamToWebservice getExamToWebservicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.eggtwo.euq.service.ExamToWebservicePortBindingStub _stub = new com.eggtwo.euq.service.ExamToWebservicePortBindingStub(portAddress, this);
            _stub.setPortName(getExamToWebservicePortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setExamToWebservicePortEndpointAddress(java.lang.String address) {
        ExamToWebservicePort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.eggtwo.euq.service.ExamToWebservice.class.isAssignableFrom(serviceEndpointInterface)) {
                com.eggtwo.euq.service.ExamToWebservicePortBindingStub _stub = new com.eggtwo.euq.service.ExamToWebservicePortBindingStub(new java.net.URL(ExamToWebservicePort_address), this);
                _stub.setPortName(getExamToWebservicePortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ExamToWebservicePort".equals(inputPortName)) {
            return getExamToWebservicePort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://service.euq.eggtwo.com/", "ExamToWebserviceService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://service.euq.eggtwo.com/", "ExamToWebservicePort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ExamToWebservicePort".equals(portName)) {
            setExamToWebservicePortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
