package com.eggtwo.euq.service;

public class ExamToWebserviceProxy implements com.eggtwo.euq.service.ExamToWebservice {
  private String _endpoint = null;
  private com.eggtwo.euq.service.ExamToWebservice examToWebservice = null;
  
  public ExamToWebserviceProxy() {
    _initExamToWebserviceProxy();
  }
  
  public ExamToWebserviceProxy(String endpoint) {
    _endpoint = endpoint;
    _initExamToWebserviceProxy();
  }
  
  private void _initExamToWebserviceProxy() {
    try {
      examToWebservice = (new com.eggtwo.euq.service.ExamToWebserviceServiceLocator()).getExamToWebservicePort();
      if (examToWebservice != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)examToWebservice)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)examToWebservice)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (examToWebservice != null)
      ((javax.xml.rpc.Stub)examToWebservice)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.eggtwo.euq.service.ExamToWebservice getExamToWebservice() {
    if (examToWebservice == null)
      _initExamToWebserviceProxy();
    return examToWebservice;
  }
  
  public java.lang.String examList(java.lang.String arg0, java.lang.Integer arg1, java.lang.Integer arg2, java.lang.String arg3) throws java.rmi.RemoteException{
    if (examToWebservice == null)
      _initExamToWebserviceProxy();
    return examToWebservice.examList(arg0, arg1, arg2, arg3);
  }
  
  
}