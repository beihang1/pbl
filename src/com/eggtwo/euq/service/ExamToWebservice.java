/**
 * ExamToWebservice.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.eggtwo.euq.service;

public interface ExamToWebservice extends java.rmi.Remote {
    public java.lang.String examList(java.lang.String arg0, java.lang.Integer arg1, java.lang.Integer arg2, java.lang.String arg3) throws java.rmi.RemoteException;
}
