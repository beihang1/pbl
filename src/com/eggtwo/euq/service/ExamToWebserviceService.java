/**
 * ExamToWebserviceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.eggtwo.euq.service;

public interface ExamToWebserviceService extends javax.xml.rpc.Service {
    public java.lang.String getExamToWebservicePortAddress();

    public com.eggtwo.euq.service.ExamToWebservice getExamToWebservicePort() throws javax.xml.rpc.ServiceException;

    public com.eggtwo.euq.service.ExamToWebservice getExamToWebservicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
