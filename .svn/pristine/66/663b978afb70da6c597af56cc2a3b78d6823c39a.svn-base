/**
* Filename : NoticeManageService.java
* Author : TongLei
* Creation time : 下午1:44:13 - 2015年11月11日
* Description :
*/

package com.cloudsoaring.web.trainingplatform.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.cloudsoaring.common.utils.IDGenerator;
import com.cloudsoaring.exception.BusinessException;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.bus.entity.FileEntity;
import com.cloudsoaring.web.bus.entity.TagEntity;
import com.cloudsoaring.web.bus.entity.UserEntity;
import com.cloudsoaring.web.bus.service.FileService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.view.TagView;
import com.cloudsoaring.web.institution.entity.InstitutionEntity;
import com.cloudsoaring.web.myinfo.entity.StudentClassInfoEntity;
import com.cloudsoaring.web.trainingplatform.constants.TpConstants;
import com.cloudsoaring.web.trainingplatform.entity.ActivityCenterEntity;
import com.cloudsoaring.web.trainingplatform.entity.ActivityCenterUserEntity;
import com.cloudsoaring.web.trainingplatform.entity.TpUserEntity;


/**
 * 公告管理相关业务
 * @author TongLei
 *
 */
@Service
public class ActivityCenterService extends FileService implements TpConstants{
	
	/**
	 * 检索公告分类信息
	 * @param entity
	 * @return 返回结果
	 */
	@SuppressWarnings("rawtypes")
	public List searchNoticeTag(TagView view){
		return commonDao.searchList(view, SEARCH_ACTIVITY_CENTER_CATEGORY);
	}
	/**
	 * 检索公告信息
	 * @param entity
	 * @return 返回结果
	 */
	public ResultBean searchNotice(ActivityCenterEntity entity){
		return commonDao.searchList4Page(entity, SEARCH_ACTIVITY_CENTER);
	}
	
	public ActivityCenterEntity selectNotice(ActivityCenterEntity entity){
		return commonDao.searchOneData(entity, EDIT_ACTIVITY_CENTER_SELECT);
	}
	/**
	 * 新增公告
	 * @param entity
	 * @throws Exception
	 */
	public void insertNotice(ActivityCenterEntity entity) throws Exception{
		//图片处理
		FileEntity pictureFile = processRequestFile("picture",FILE_TYPE_IMAGE);
		compressToImage(pictureFile, false);
		entity.setPictureId(pictureFile.getFileId());
		entity.setStatus(INFOMATION_STATUS_NOT);
		entity.setRecomment(new BigDecimal(0));
		entity.setSortOrder("1");
		this.commonDao.insertData(entity,ADD_ACTIVITY_CENTER);
	}
	
	/**
	 * 新增公告
	 * @param view
	 * 
	 */
	public void insertNoticeType(TagView view){
			this.commonDao.insertData(view,ADD_NOTICE_TYPE);
	}
	/**
	 * 发布公告
	 * @param entity
	 */
	public ResultBean isStatus(ActivityCenterEntity entity){
		if(isNoticeExist(entity)){
			throw new BusinessException(getMessage(MSG_NOTICE_IS_NULL));
		}
		ActivityCenterEntity infoEntity =new ActivityCenterEntity();
		infoEntity = this.commonDao.searchOneData(entity, SEARCH_ACTIVITY_CENTER);
		infoEntity.setUpdateUser(WebContext.getSessionUserId());
		if(INFOMATION_STATUS_NOT.equals(infoEntity.getStatus())){
			infoEntity.setStatus(INFOMATION_STATUS_ALREADY);
		}else{
			infoEntity.setStatus(INFOMATION_STATUS_NOT);
			this.commonDao.updateData(infoEntity, STATUS_ACTIVITY_CENTER);
			return ResultBean.success(STATUS_NOT_SUCCESS);
		}
		this.commonDao.updateData(infoEntity, STATUS_ACTIVITY_CENTER);
		return ResultBean.success(STATUS_NOTICE_SUCCESS);
	}
	/**
	 * 删除公告
	 * @param entity
	 * @throws Exception
	 */
	public void deleteInfo(ActivityCenterEntity entity)throws Exception{
		if(isNoticeExist(entity)){
			throw new BusinessException(getMessage(MSG_NOTICE_IS_NULL));
		}
		ActivityCenterEntity infoEntity =new ActivityCenterEntity();
		infoEntity = this.commonDao.searchOneData(entity, SEARCH_ACTIVITY_CENTER);
		String fileId = infoEntity.getPictureId();
		this.deleteFileById(fileId,FILE_TYPE_IMAGE);
		this.commonDao.deleteData(entity, DELETE_ACTIVITY_CENTER);
	}
	public Boolean isNoticeExist(ActivityCenterEntity entity){
		int cnt = commonDao.searchCount(entity, ACTIVITY_CENTER_EXIST);
		return cnt == 0;
	}
	
	public void deleteNoticeType(TagView view){
		view = this.commonDao.searchOneData(view, SEARCH_NOTICE_TYPE);
		this.commonDao.deleteData(view, DELETE_NOTICE_TYPE);
	}
	/**
	 * 编辑公告
	 * @param entity
	 */
	public void editNotice(ActivityCenterEntity entity) throws Exception{
		if(isNoticeExist(entity)){
			throw new BusinessException(getMessage(MSG_NOTICE_IS_NULL));
		}
		FileEntity pictureFile = processRequestFile("picture",FILE_TYPE_IMAGE);
		compressToImage(pictureFile, false);
		//图片处理
			/*if(!entity.getOldPictureId().equals(pictureFile.getFileId())){
				String fileId = entity.getOldPictureId();
				this.deleteFileById(fileId, FILE_TYPE_IMAGE);
			}*/
			entity.setPictureId(pictureFile.getFileId());
			entity.setUpdateUser(WebContext.getSessionUserId());
			entity.setSortOrder("1");
		this.commonDao.updateData(entity, EDIT_ACTIVITY_CENTER_MESSAGE);
	}

	public void editNoticeType(TagView view){
		this.commonDao.updateData(view,EDIT_NOTICE_TYPE);
	}
	
	public ResultBean searchInfoByCategoryId(ActivityCenterEntity info){
		ResultBean result = new ResultBean();
		@SuppressWarnings("unchecked")
		List<ActivityCenterEntity> entityList = (List<ActivityCenterEntity>) commonDao.searchList4Page(info, SELECT_ACTIVITY_CENTER).getData();
		if(null!=entityList && entityList.size()>0) {
			for(int i=0;i<entityList.size();i++) {
				String activityCenterId=entityList.get(i).getId();
				ActivityCenterUserEntity userEntity = new ActivityCenterUserEntity();
				userEntity.setActivityCenterId(activityCenterId);
				int count = commonDao.searchCount(userEntity, SELECT_ACTIVITY_COUNT);
				entityList.get(i).setCount(count);
				String uid = WebContext.getSessionUserId();
				Long endTime = entityList.get(i).getFromDateEnd().getTime();
				Long nowTime = new Date().getTime();
				if(endTime-nowTime>0) {
					ActivityCenterUserEntity userEntity1 = new ActivityCenterUserEntity();
					userEntity1.setActivityCenterId(activityCenterId);
					userEntity1.setUserId(uid);
					ActivityCenterUserEntity entity = commonDao.searchOneData(userEntity1, SELECT_ACTIVITY_CENTER_USER);
					if(null!=entity) {
						entityList.get(i).setActiveStatus("1");//已报名
					}else {
						entityList.get(i).setActiveStatus("0");//未报名
					}
				}else {
					entityList.get(i).setActiveStatus("2");//已结束
				}
				
			}
		}
		List<ActivityCenterEntity> list =commonDao.searchList(info, SELECT_ACTIVITY_CENTER);
		 int iPageCount=list.size()/info.getPageSize();
         if (list.size()%info.getPageSize()>0) {
            iPageCount++;
         }
         result.setPage(info.getPageNumber());
		result.setTotal(iPageCount);
		result.setData(entityList);
		return result;
	}
	
	public ResultBean searchCategory(TagView view){
		ResultBean result = new ResultBean();
		List<TagView> list = commonDao.searchList(view, SEARCH_ACTIVITY_CENTER_CATEGORY);
		result.setStatus(true);
		result.setData(list);
		return result;
	}
	
	public ActivityCenterEntity searchInfoById(ActivityCenterEntity info){
		return commonDao.searchOneData(info, SEARCH_ACTIVITY_CENTER_BY_ID);
	}
	
	public ResultBean saveUser(String activityCenterId) {
		ResultBean result = new ResultBean();
		ActivityCenterUserEntity entity = new ActivityCenterUserEntity();
		entity.setId(IDGenerator.genUID());
		entity.setActivityCenterId(activityCenterId);
		entity.setUserId(WebContext.getSessionUserId());
		entity.setCreateDate(new Date());
		commonDao.insertData(entity, ADD_ACTIVITY_CENTER_USER);
		result.setStatus(true);
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<ActivityCenterUserEntity> getUsersList(ActivityCenterUserEntity entity){
		return commonDao.searchList(entity, SELECT_ACTIVITY_CENTER_USER);
	}
	
	public ResultBean searchActivityCenterUserList(String id) {
		ActivityCenterUserEntity user = new ActivityCenterUserEntity();
		user.setActivityCenterId(id);
		return commonDao.searchList4Page(user, SELECT_ACTIVITY_CENTER_USER);
	}
	public ResultBean searchTeacherInfoByCategoryId(ActivityCenterEntity info) {
		ResultBean result = new ResultBean();
		TpUserEntity entity1=new TpUserEntity();
		entity1.setUserId(WebContext.getSessionUserId());
		entity1 = commonDao.searchOneData(entity1, USER_BY_USERID);
		if(null!=entity1 && StringUtils.isNotBlank(entity1.getSchoolName())) {
			info.setSchoolName(entity1.getSchoolName());
		}
		@SuppressWarnings("unchecked")
		List<ActivityCenterEntity> entityList = (List<ActivityCenterEntity>) commonDao.searchList4Page(info, SELECT_NOT_ACTIVITY_CENTER).getData();
		if(null!=entityList && entityList.size()>0) {
			for(int i=0;i<entityList.size();i++) {
				String activityCenterId=entityList.get(i).getId();
				ActivityCenterUserEntity userEntity = new ActivityCenterUserEntity();
				userEntity.setActivityCenterId(activityCenterId);
				int count = commonDao.searchCount(userEntity, SELECT_ACTIVITY_COUNT);
				entityList.get(i).setCount(count);
				String uid = WebContext.getSessionUserId();
				Long endTime = entityList.get(i).getFromDateEnd().getTime();
				Long nowTime = new Date().getTime();
				if(endTime-nowTime>0) {
					ActivityCenterUserEntity userEntity1 = new ActivityCenterUserEntity();
					userEntity1.setActivityCenterId(activityCenterId);
					userEntity1.setUserId(uid);
					ActivityCenterUserEntity entity = commonDao.searchOneData(userEntity1, SELECT_ACTIVITY_CENTER_USER);
					if(null!=entity) {
						entityList.get(i).setActiveStatus("1");//已报名
					}else {
						entityList.get(i).setActiveStatus("0");//未报名
					}
				}else {
					entityList.get(i).setActiveStatus("2");//已结束
				}
				
			}
		}
		List<ActivityCenterEntity> list =commonDao.searchList(info, SELECT_NOT_ACTIVITY_CENTER);
		 int iPageCount=list.size()/info.getPageSize();
         if (list.size()%info.getPageSize()>0) {
            iPageCount++;
         }
         result.setPage(info.getPageNumber());
		result.setTotal(iPageCount);
		result.setData(entityList);
		return result;
	}
	public ResultBean searchStudentInfoByCategoryId(ActivityCenterEntity info) {
		ResultBean result = new ResultBean();
		StudentClassInfoEntity entity1=new StudentClassInfoEntity();
		entity1.setStudentId(WebContext.getSessionUserId());
		entity1 = commonDao.searchOneData(entity1, STUDENT_ONE_LASS_SEARCH);
		if(null!=entity1 && StringUtils.isNotBlank(entity1.getSchoolName())) {
			info.setSchoolName(entity1.getSchoolName());
		}
		@SuppressWarnings("unchecked")
		List<ActivityCenterEntity> entityList = (List<ActivityCenterEntity>) commonDao.searchList4Page(info, SELECT_NOT_ACTIVITY_CENTER).getData();
		if(null!=entityList && entityList.size()>0) {
			for(int i=0;i<entityList.size();i++) {
				String activityCenterId=entityList.get(i).getId();
				ActivityCenterUserEntity userEntity = new ActivityCenterUserEntity();
				userEntity.setActivityCenterId(activityCenterId);
				int count = commonDao.searchCount(userEntity, SELECT_ACTIVITY_COUNT);
				entityList.get(i).setCount(count);
				String uid = WebContext.getSessionUserId();
				Long endTime = entityList.get(i).getFromDateEnd().getTime();
				Long nowTime = new Date().getTime();
				if(endTime-nowTime>0) {
					ActivityCenterUserEntity userEntity1 = new ActivityCenterUserEntity();
					userEntity1.setActivityCenterId(activityCenterId);
					userEntity1.setUserId(uid);
					ActivityCenterUserEntity entity = commonDao.searchOneData(userEntity1, SELECT_ACTIVITY_CENTER_USER);
					if(null!=entity) {
						entityList.get(i).setActiveStatus("1");//已报名
					}else {
						entityList.get(i).setActiveStatus("0");//未报名
					}
				}else {
					entityList.get(i).setActiveStatus("2");//已结束
				}
				
			}
		}
		List<ActivityCenterEntity> list =commonDao.searchList(info, SELECT_NOT_ACTIVITY_CENTER);
		 int iPageCount=list.size()/info.getPageSize();
         if (list.size()%info.getPageSize()>0) {
            iPageCount++;
         }
         result.setPage(info.getPageNumber());
		result.setTotal(iPageCount);
		result.setData(entityList);
		return result;
	}
	public ResultBean searchMobileInfoByCategoryId(ActivityCenterEntity info) {
		ResultBean result = new ResultBean();
		@SuppressWarnings("unchecked")
		List<ActivityCenterEntity> entityList =commonDao.searchList(info, SELECT_ACTIVITY_CENTER);
//		(List<ActivityCenterEntity>) commonDao.searchList4Page(info, SELECT_ACTIVITY_CENTER).getData();
		if(null!=entityList && entityList.size()>0) {
			for(int i=0;i<entityList.size();i++) {
				String activityCenterId=entityList.get(i).getId();
				ActivityCenterUserEntity userEntity = new ActivityCenterUserEntity();
				userEntity.setActivityCenterId(activityCenterId);
				int count = commonDao.searchCount(userEntity, SELECT_ACTIVITY_COUNT);
				entityList.get(i).setCount(count);
				String uid = WebContext.getSessionUserId();
				Long endTime = entityList.get(i).getFromDateEnd().getTime();
				Long nowTime = new Date().getTime();
				if(endTime-nowTime>0) {
					ActivityCenterUserEntity userEntity1 = new ActivityCenterUserEntity();
					userEntity1.setActivityCenterId(activityCenterId);
					userEntity1.setUserId(uid);
					ActivityCenterUserEntity entity = commonDao.searchOneData(userEntity1, SELECT_ACTIVITY_CENTER_USER);
					if(null!=entity) {
						entityList.get(i).setActiveStatus("1");//已报名
					}else {
						entityList.get(i).setActiveStatus("0");//未报名
					}
				}else {
					entityList.get(i).setActiveStatus("2");//已结束
				}
				
			}
		}

		result.setData(entityList);
		return result;
	}
	public ResultBean searchMobileTeacherInfoByCategoryId(ActivityCenterEntity info) {
		ResultBean result = new ResultBean();
		TpUserEntity entity1=new TpUserEntity();
		entity1.setUserId(WebContext.getSessionUserId());
		entity1 = commonDao.searchOneData(entity1, USER_BY_USERID);
		if(null!=entity1 && StringUtils.isNotBlank(entity1.getSchoolName())) {
			info.setSchoolName(entity1.getSchoolName());
		}
		@SuppressWarnings("unchecked")
		List<ActivityCenterEntity> entityList = commonDao.searchList(info, SELECT_NOT_ACTIVITY_CENTER);
		//(List<ActivityCenterEntity>) commonDao.searchList4Page(info, SELECT_NOT_ACTIVITY_CENTER).getData();
		if(null!=entityList && entityList.size()>0) {
			for(int i=0;i<entityList.size();i++) {
				String activityCenterId=entityList.get(i).getId();
				ActivityCenterUserEntity userEntity = new ActivityCenterUserEntity();
				userEntity.setActivityCenterId(activityCenterId);
				int count = commonDao.searchCount(userEntity, SELECT_ACTIVITY_COUNT);
				entityList.get(i).setCount(count);
				String uid = WebContext.getSessionUserId();
				Long endTime = entityList.get(i).getFromDateEnd().getTime();
				Long nowTime = new Date().getTime();
				if(endTime-nowTime>0) {
					ActivityCenterUserEntity userEntity1 = new ActivityCenterUserEntity();
					userEntity1.setActivityCenterId(activityCenterId);
					userEntity1.setUserId(uid);
					ActivityCenterUserEntity entity = commonDao.searchOneData(userEntity1, SELECT_ACTIVITY_CENTER_USER);
					if(null!=entity) {
						entityList.get(i).setActiveStatus("1");//已报名
					}else {
						entityList.get(i).setActiveStatus("0");//未报名
					}
				}else {
					entityList.get(i).setActiveStatus("2");//已结束
				}
				
			}
		}
		result.setData(entityList);
		return result;
	}
	public ResultBean searchMobileStudentInfoByCategoryId(ActivityCenterEntity info) {
		ResultBean result = new ResultBean();
		StudentClassInfoEntity entity1=new StudentClassInfoEntity();
		entity1.setStudentId(WebContext.getSessionUserId());
		entity1 = commonDao.searchOneData(entity1, STUDENT_ONE_LASS_SEARCH);
		if(null!=entity1 && StringUtils.isNotBlank(entity1.getSchoolName())) {
			info.setSchoolName(entity1.getSchoolName());
		}
		@SuppressWarnings("unchecked")
		List<ActivityCenterEntity> entityList = commonDao.searchList(info, SELECT_NOT_ACTIVITY_CENTER);
		//(List<ActivityCenterEntity>) commonDao.searchList4Page(info, SELECT_NOT_ACTIVITY_CENTER).getData();
		if(null!=entityList && entityList.size()>0) {
			for(int i=0;i<entityList.size();i++) {
				String activityCenterId=entityList.get(i).getId();
				ActivityCenterUserEntity userEntity = new ActivityCenterUserEntity();
				userEntity.setActivityCenterId(activityCenterId);
				int count = commonDao.searchCount(userEntity, SELECT_ACTIVITY_COUNT);
				entityList.get(i).setCount(count);
				String uid = WebContext.getSessionUserId();
				Long endTime = entityList.get(i).getFromDateEnd().getTime();
				Long nowTime = new Date().getTime();
				if(endTime-nowTime>0) {
					ActivityCenterUserEntity userEntity1 = new ActivityCenterUserEntity();
					userEntity1.setActivityCenterId(activityCenterId);
					userEntity1.setUserId(uid);
					ActivityCenterUserEntity entity = commonDao.searchOneData(userEntity1, SELECT_ACTIVITY_CENTER_USER);
					if(null!=entity) {
						entityList.get(i).setActiveStatus("1");//已报名
					}else {
						entityList.get(i).setActiveStatus("0");//未报名
					}
				}else {
					entityList.get(i).setActiveStatus("2");//已结束
				}
				
			}
		}
		result.setData(entityList);
		return result;
	}
}
