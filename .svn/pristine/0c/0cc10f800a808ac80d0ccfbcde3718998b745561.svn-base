<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="NoteData">
	<resultMap type="com.cloudsoaring.web.course.entity.NoteDataEntity" id="searchResultMap">
		<result property="id" column="ID"/>
		<result property="noteId" column="NOTE_ID"/>
		<result property="userId" column="USER_ID"/>
		<result property="content" column="CONTENT"/>
		<result property="fileId" column="FILE_ID"/>
		<result property="type" column="TYPE"/>
		<result property="screenshotTime" column="SCREENSHOT_TIME"/>
		<result property="screenshotId" column="SCREENSHOT_ID"/>
		<result property="createDate" column="CREATE_DATE"/>
		<result property="createUser" column="CREATE_USER"/>
		<result property="updateDate" column="UPDATE_DATE"/>
		<result property="updateUser" column="UPDATE_USER"/>
		<result property="likeNum" column="LIKE_NUM"/>
		<result property="collectionNum" column="COLLECTION_NUM"/>
		<result property="title" column="TITLE"/>
		<result property="userLike" column="USER_LIKE"/>
		<result property="userCollection" column="USER_COLLECTION"/>
		<result property="personName" column="PERSON_NAME"/>
		<result property="userImageId" column="USER_IMAGE_ID"/>
		<result property="canDelete" column="CAN_DELETE"/>
	</resultMap> 
	<!-- select -->
	<select id="select" resultMap="searchResultMap">
		SELECT
			ID,
			NOTE_ID,
			USER_ID,
			CONTENT,
			FILE_ID,
			TYPE,
			SCREENSHOT_TIME,
			SCREENSHOT_ID,
			CREATE_DATE,
			CREATE_USER,
			UPDATE_DATE,
			UPDATE_USER,
			LIKE_NUM,
			COLLECTION_NUM
		FROM
			tb_note_data
		<where>
			<if test="id != null and id != ''">
				AND ID = #{id}
			</if>
			<if test="noteId != null and noteId != ''">
				AND NOTE_ID = #{noteId}
			</if>
			<if test="userId != null and userId != ''">
				AND USER_ID = #{userId}
			</if>
			<if test="content != null and content != ''">
				AND CONTENT = #{content}
			</if>
			<if test="screenshotTime != null and screenshotTime != ''">
				AND SCREENSHOT_TIME = #{screenshotTime}
			</if>
			<if test="screenshotId != null and screenshotId != ''">
				AND SCREENSHOT_ID = #{screenshotId}
			</if>
			<if test="createDate != null and createDate != ''">
				AND CREATE_DATE = #{createDate}
			</if>
			<if test="createUser != null and createUser != ''">
				AND CREATE_USER = #{createUser}
			</if>
			<if test="updateDate != null and updateDate != ''">
				AND UPDATE_DATE = #{updateDate}
			</if>
			<if test="updateUser != null and updateUser != ''">
				AND UPDATE_USER = #{updateUser}
			</if>
			<if test="likeNum != null and likeNum != ''">
				AND LIKE_NUM = #{likeNum}
			</if>
			<if test="collectionNum != null and collectionNum != ''">
				AND COLLECTION_NUM = #{collectionNum}
			</if>
		</where>
		<if test="sortName != null and sortName != ''">
			ORDER BY ${sortName} ${sortOrder}
		</if>
	</select>
	
	<!-- 根据章节课程检索笔记列表 -->
	<select id="selectNoteList" resultMap="searchResultMap">
		SELECT
			nd.ID,
			nd.NOTE_ID,
			nd.USER_ID,
			nd.CONTENT,
			nd.FILE_ID,
			nd.TYPE,
			nd.SCREENSHOT_TIME,
			nd.SCREENSHOT_ID,
			nd.CREATE_DATE,
			nd.CREATE_USER,
			nd.UPDATE_DATE,
			nd.UPDATE_USER,
			nd.LIKE_NUM,
			nd.COLLECTION_NUM,
			n.TITLE,
			u.IMAGE as USER_IMAGE_ID,
			u.PERSON_NAME as PERSON_NAME,
			(select count(*) from tb_operate_record where LINK=nd.ID and OPERATE_TYPE=#{OPERATE_LIKE} and OPERATE_USER=#{loginId}) as USER_LIKE,
			(select count(*) from tb_operate_record where LINK=nd.ID and OPERATE_TYPE=#{OPERATE_COLLECT} and OPERATE_USER=#{loginId}) as USER_COLLECTION
			<if test="linkType==LINK_TYPE_COURSE">
				,(
					#{loginId} = nd.USER_ID or 
					#{loginId} = (
						select course.TEACHER_ID from tb_course course where course.ID = #{linkId}
					)
				)as CAN_DELETE
			</if>
			<if test="linkType==LINK_TYPE_CHAPTER">
				,(
					#{loginId} = nd.USER_ID or 
					#{loginId} = (
						select course.TEACHER_ID from tb_course course where course.ID = (select chapter.COURSE_ID from tb_chapter chapter where chapter.ID=#{linkId})
					)
				)as CAN_DELETE
			</if>
		FROM
			tb_note_data nd 
		left join tb_note n on n.id=nd.note_id 
		left join tb_user u on u.USER_ID=nd.USER_ID
		where   n.LINK_ID=#{linkId} and n.LINK_TYPE=#{linkType}
		 
		<if test="sortName != null and sortName != ''">
			ORDER BY ${sortName} ${sortOrder}
		</if>
	</select>
	
	<!-- 检索课程下的笔记 -->
	<select id="selecCoursetNoteList" resultMap="searchResultMap">
		SELECT
			nd.ID,
			nd.NOTE_ID,
			nd.USER_ID,
			nd.CONTENT,
			nd.FILE_ID,
			nd.TYPE,
			nd.SCREENSHOT_TIME,
			nd.SCREENSHOT_ID,
			nd.CREATE_DATE,
			nd.CREATE_USER,
			nd.UPDATE_DATE,
			nd.UPDATE_USER,
			nd.LIKE_NUM,
			nd.COLLECTION_NUM,
			n.TITLE,
			u.IMAGE as USER_IMAGE_ID,
			u.PERSON_NAME as PERSON_NAME,
			(select count(*) from tb_operate_record where LINK=nd.ID and OPERATE_TYPE=#{OPERATE_LIKE} and OPERATE_USER=#{loginId}) as USER_LIKE,
			(select count(*) from tb_operate_record where LINK=nd.ID and OPERATE_TYPE=#{OPERATE_COLLECT} and OPERATE_USER=#{loginId}) as USER_COLLECTION
			<if test="linkType==LINK_TYPE_COURSE">
				,(
					#{loginId} = nd.USER_ID or 
					#{loginId} = (
						select course.TEACHER_ID from tb_course course where course.ID = #{linkId}
					)
				)as CAN_DELETE
			</if>
			<if test="linkType==LINK_TYPE_CHAPTER">
				,(
					#{loginId} = nd.USER_ID or 
					#{loginId} = (
						select course.TEACHER_ID from tb_course course where course.ID = (select chapter.COURSE_ID from tb_chapter chapter where chapter.ID=#{linkId})
					)
				)as CAN_DELETE
			</if>
		FROM
			tb_note_data nd 
		left join tb_note n on n.id=nd.note_id 
		left join tb_user u on u.USER_ID=nd.USER_ID
		where   n.LINK_ID in (select ID from tb_chapter where COURSE_ID=#{linkId}) and n.LINK_TYPE=#{LINK_TYPE_CHAPTER}
		 
		<if test="sortName != null and sortName != ''">
			ORDER BY ${sortName} ${sortOrder}
		</if>
	</select>
	
	
	<!-- 检索用户笔记列表 -->
	<select id="selectUserNoteList" resultMap="searchResultMap">
		 SELECT
			nd.ID,
			nd.NOTE_ID,
			nd.USER_ID,
			nd.CONTENT,
			nd.FILE_ID,
			nd.TYPE,
			nd.SCREENSHOT_TIME,
			nd.SCREENSHOT_ID,
			nd.CREATE_DATE,
			nd.CREATE_USER,
			nd.UPDATE_DATE,
			nd.UPDATE_USER,
			nd.LIKE_NUM,
			nd.COLLECTION_NUM,
			(select PERSON_NAME from tb_user where user_id=nd.USER_ID) AS PERSON_NAME,
			(select TITLE from tb_note where ID=nd.NOTE_ID) AS TITLE,
			(select IMAGE from tb_user where USER_ID=nd.USER_ID) as USER_IMAGE_ID
		FROM
			tb_note_data nd 
		where    
		 		 nd.USER_ID=#{userId}
		<if test="sortName != null and sortName != ''">
			ORDER BY ${sortName} ${sortOrder}
		</if>
	</select>
	
	
	<!-- 检索用户采集笔记列表 -->
	<select id="selectUserCollectList" resultMap="searchResultMap">
		SELECT
			tnd.ID,
			tnd.NOTE_ID,
			tnd.USER_ID,
			tnd.CONTENT,
			tnd.FILE_ID,
			tnd.TYPE,
			tnd.SCREENSHOT_TIME,
			tnd.SCREENSHOT_ID,
			tnd.CREATE_DATE,
			tnd.CREATE_USER,
			tnd.UPDATE_DATE,
			tnd.UPDATE_USER,
			tnd.LIKE_NUM,
			tnd.COLLECTION_NUM,
			(select PERSON_NAME from tb_user where USER_ID=tnd.USER_ID) as PERSON_NAME,
			(select IMAGE from tb_user where USER_ID=tnd.USER_ID) as USER_IMAGE_ID,
			(select TITLE from tb_note where ID=tnd.NOTE_ID) as TITLE
		FROM
			tb_note_data tnd
		where 
			tnd.ID in(
				select tor.LINK
				from tb_operate_record tor
				where
				tor.OPERATE_TYPE=#{OPERATE_TYPE}
				and
				tor.OPERATE_USER=#{userId}
			)

		<if test="sortName != null and sortName != ''">
			ORDER BY ${sortName} ${sortOrder}
		</if>
	</select>
	
	
	<!-- select -->
	<select id="selectByPk" resultMap="searchResultMap">
		SELECT
			ID,
			NOTE_ID,
			USER_ID,
			CONTENT,
			FILE_ID,
			TYPE,
			SCREENSHOT_TIME,
			SCREENSHOT_ID,
			CREATE_DATE,
			CREATE_USER,
			UPDATE_DATE,
			UPDATE_USER,
			LIKE_NUM,
			COLLECTION_NUM
		FROM
			tb_note_data
		WHERE
			ID = #{id}
	</select>
	<select id="selectByNoteId" resultMap="searchResultMap">
		SELECT
			ID,
			NOTE_ID,
			USER_ID,
			CONTENT,
			FILE_ID,
			TYPE,
			SCREENSHOT_TIME,
			SCREENSHOT_ID,
			CREATE_DATE,
			CREATE_USER,
			UPDATE_DATE,
			UPDATE_USER,
			LIKE_NUM,
			COLLECTION_NUM
		FROM
			tb_note_data
		WHERE
			ID = #{id}
	</select>
	<!-- insert -->
	<insert id="insert" parameterType="com.cloudsoaring.web.course.entity.NoteDataEntity">
		INSERT INTO tb_note_data(
			ID,
			NOTE_ID,
			USER_ID,
			CONTENT,
			FILE_ID,
			TYPE,
			SCREENSHOT_TIME,
			SCREENSHOT_ID,
			CREATE_DATE,
			CREATE_USER,
			UPDATE_DATE,
			UPDATE_USER,
			LIKE_NUM,
			COLLECTION_NUM
		)VALUES(
			#{id},
			#{noteId},
			#{userId},
			#{content},
			#{fileId},
			#{type},
			#{screenshotTime},
			#{screenshotId},
			${SYSDATE},
			#{createUser},
			${SYSDATE},
			#{updateUser},
			#{likeNum},
			#{collectionNum}
		)
	</insert>
	<!-- update -->
	<update id="update" parameterType="com.cloudsoaring.web.course.entity.NoteDataEntity">
		UPDATE
			tb_note_data
		SET
			ID = #{id},
			NOTE_ID = #{noteId},
			USER_ID = #{userId},
			CONTENT = #{content},
			FILE_ID = #{fileId},
			TYPE = #{type},
			SCREENSHOT_TIME = #{screenshotTime},
			SCREENSHOT_ID = #{screenshotId},
			UPDATE_DATE = ${SYSDATE},
			UPDATE_USER = #{updateUser},
			LIKE_NUM = #{likeNum},
			COLLECTION_NUM = #{collectionNum}
		WHERE
			ID = #{id}
	</update>
	<!-- 笔记点赞 -->
	<update id="noteClickLike" parameterType="com.cloudsoaring.web.course.entity.NoteDataEntity">
		UPDATE
			tb_note_data
		SET 
			UPDATE_DATE = ${SYSDATE},
			UPDATE_USER = #{updateUser},
			LIKE_NUM = #{likeNum}
		WHERE
			ID = #{id}
	</update>
	<!--  笔记采集-->
	<update id="noteCollect" parameterType="com.cloudsoaring.web.course.entity.NoteDataEntity">
		UPDATE
			tb_note_data
		SET 
			UPDATE_DATE = ${SYSDATE},
			UPDATE_USER = #{updateUser},
			COLLECTION_NUM = #{collectionNum}
		WHERE
			ID = #{id}
	</update>
	
	<!-- delete -->
	<delete id="delete" parameterType="com.cloudsoaring.web.course.entity.NoteDataEntity">
		DELETE FROM tb_note_data  
		WHERE
			ID = #{id}
	</delete>
	
	<!-- 检索是否教师 -->
	<select id="isTeacher" parameterType="com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity" resultType="java.lang.Integer">
		select count(*) from tb_user_role where USER_ID=#{userId} and ROLE_ID=#{roleId}
	</select>
	
	<!-- 检索笔记数据是否存在 -->
	<select id="isExist" parameterType="com.cloudsoaring.web.course.entity.NoteDataEntity" resultType="java.lang.Integer">
		select count(*) from tb_note_data where ID=#{id}
	</select>
	
	<!-- 判断是否可以删除笔记 -->
	<select id="canDeleteById" parameterType="com.cloudsoaring.web.course.entity.NoteDataEntity"  resultMap="searchResultMap">
	select
		nd.ID,
		(
				#{loginId} = nd.USER_ID or
				#{loginId} = (
					select course.TEACHER_ID from tb_course course where 
					<if test="linkType=='COURSE'.toString()">
						course.ID = #{linkId}
					</if>
					<if test="linkType=='CHAPTER'.toString()">
						course.ID = (select chapter.COURSE_ID from tb_chapter chapter where chapter.ID=#{linkId})
					</if>
				)
			) as CAN_DELETE
		FROM
			tb_note_data nd
			where nd.ID=#{id}
	</select>
</mapper>
