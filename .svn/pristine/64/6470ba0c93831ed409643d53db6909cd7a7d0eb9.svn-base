<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="UserManagement">
	<resultMap
		type="com.cloudsoaring.web.trainingplatform.entity.TpUserEntity"
		id="UserMap">
		<result property="userId" column="USER_ID" />
		<result property="userName" column="USER_NAME" />
		<result property="personName" column="PERSON_NAME" />
		<result property="email" column="EMAIL" />
		<result property="userKbn" column="USER_KBN" />
		<result property="lockFlag" column="LOCK_FLAG" />
		<result property="mobile" column="MOBILE" />
		<result property="phone" column="PHONE" />
		<result property="allowedIp" column="ALLOWED_IP" />
		<result property="lastLoginTime" column="LAST_LOGIN_TIME" />
		<result property="lastLoginIp" column="LAST_LOGIN_IP" />
		<result property="loginCount" column="LOGIN_COUNT" />
		<result property="lastChangePwdTime"
			column="LAST_CHANGE_PWD_TIME" />
		<result property="status" column="STATUS" />
		<result property="delFlag" column="DEL_FLAG" />
		<result property="displayTheme" column="DISPLAY_THEME" />
		<result property="image" column="IMAGE" />
		<result property="sex" column="SEX" />
		<result property="region" column="REGION" />
		<result property="weixin" column="WEIXIN" />
		<result property="weibo" column="WEIBO" />
		<result property="remark" column="REMARK" />
		<result property="workplace" column="WORKPLACE" />
		<result property="position" column="POSITION" />
		<result property="tenantId" column="tenantId" />
		<result property="headurl" column="head_url" />
		<result property="proName" column="pro_name" />
		<result property="cityName" column="city_name" />
		<result property="townName" column="town_name" />
		<result property="schoolId" column="school_id" />
		<result property="schoolName" column="school_name" />
		<result property="schoolType" column="school_type" />
		<result property="subjectId" column="subject_id" />
		<result property="subjectName" column="subject_name" />
		<result property="duty" column="duty" />
		<result property="schoolManager" column="school_manager" />
	</resultMap>
	<resultMap
		type="com.cloudsoaring.web.trainingplatform.entity.TpUserEntity"
		id="UserAllMap">
		<result property="userId" column="USER_ID" />
		<result property="userName" column="USER_NAME" />
		<result property="password" column="PASSWORD" />
		<result property="personName" column="PERSON_NAME" />
		<result property="email" column="EMAIL" />
		<result property="sex" column="SEX" />
		<result property="image" column="IMAGE" />
		<result property="region" column="REGION" />
		<result property="weixin" column="WEIXIN" />
		<result property="weibo" column="WEIBO" />
		<result property="remark" column="REMARK" />
		<result property="departmentId" column="DEPARTMENT_ID" />
		<result property="mobile" column="MOBILE" />
		<result property="phone" column="PHONE" />
		<result property="allowedIp" column="ALLOWED_IP" />
		<result property="lastLoginTime" column="LAST_LOGIN_TIME" />
		<result property="lastLoginIp" column="LAST_LOGIN_IP" />
		<result property="loginCount" column="LOGIN_COUNT" />
		<result property="lastChangePwdTime"
			column="LAST_CHANGE_PWD_TIME" />
		<result property="status" column="STATUS" />
		<result property="delFlag" column="DEL_FLAG" />
		<result property="userKbn" column="USER_KBN" />
		<result property="companyId" column="COMPANY_ID" />
		<result property="lockFlag" column="LOCK_FLAG" />
		<result property="createDate" column="CREATE_DATE" />
		<result property="createUser" column="CREATE_USER" />
		<result property="updateDate" column="UPDATE_DATE" />
		<result property="updateUser" column="UPDATE_USER" />
		<result property="workplace" column="WORKPLACE" />
		<result property="position" column="POSITION" />
		<result property="tenantId" column="tenantId" />
		<result property="headurl" column="head_url" />
		<result property="proName" column="pro_name" />
		<result property="cityName" column="city_name" />
		<result property="townName" column="town_name" />
		<result property="schoolId" column="school_id" />
		<result property="schoolName" column="school_name" />
		<result property="schoolType" column="school_type" />
		<result property="subjectId" column="subject_id" />
		<result property="subjectName" column="subject_name" />
		<result property="duty" column="duty" />
		<result property="isFlag" column="isFlag" />
		<result property="schoolManager" column="school_manager" />

	</resultMap>
	<select id="searchUser" resultMap="UserMap"
		parameterType="com.cloudsoaring.web.trainingplatform.entity.TpUserEntity">
		<![CDATA[
		SELECT
			DISTINCT
			USER_ID,
			USER_NAME,
			PERSON_NAME,
			EMAIL,
			USER_KBN,
			SEX,
			IMAGE,
			REGION,
			WEIXIN,
			WEIBO,
			LOCK_FLAG,
			MOBILE,
			PHONE,
			REMARK,
			ALLOWED_IP,
			LAST_LOGIN_TIME,
			LAST_LOGIN_IP,
			LOGIN_COUNT,
			DISPLAY_THEME,
			LAST_CHANGE_PWD_TIME,
			STATUS,
			DEL_FLAG,
			WORKPLACE,
			POSITION,
			tenantId,
			head_url,
			pro_name,
			city_name,
			town_name,
			school_id,
			school_name,
			school_type,
			subject_id,
			subject_name,
			duty,
			school_manager
		FROM TB_USER
		WHERE DEL_FLAG = '0' AND USER_ID <> 'admin'
		]]>
		<if test="roleId != null and roleId != ''">
			AND EXISTS
			(
			SELECT 1 from tb_user_role WHERE ROLE_ID = #{roleId} and TB_USER.USER_ID =
			tb_user_role.USER_ID
			)
		</if>
		<if test="userId != null and userId != ''">
			AND USER_ID = #{userId}
		</if>
		<if test="userName != null and userName != ''">
			AND upper(USER_NAME) like %{CONCATTU('%', #{userName},
			'%')}
		</if>
		<if test="sex != null and sex != ''">
			AND SEX = #{sex}
		</if>
		<if test="weixin != null and weixin != ''">
			AND upper(WEIXIN) like %{CONCATTU('%', #{weixin}, '%')}
		</if>
		<if test="weibo != null and weibo != ''">
			AND upper(WEIBO) like %{CONCATTU('%', #{weibo}, '%')}
		</if>
		<if test="personName != null and personName != ''">
			AND upper(PERSON_NAME) like %{CONCATTU('%', #{personName},
			'%')}
		</if>
		<if test="email != null and email != ''">
			AND upper(EMAIL) like %{CONCATTU('%', #{email}, '%')}
		</if>
		<if test="phone != null and phone != ''">
			AND upper(PHONE) like %{CONCATTU('%', #{phone}, '%')}
		</if>
		<if test="allowedIp != null and allowedIp != ''">
			AND ALLOWED_IP = #{allowedIp}
		</if>
		<if test="status != null and status != ''">
			AND STATUS = #{status}
		</if>
		<if test="userKbn != null and userKbn != ''">
			AND USER_KBN = #{userKbn}
		</if>
		<if test="schoolManager != null and schoolManager != ''">
			AND school_manager = #{schoolManager}
		</if>
		<if test="sortName != null and sortName != ''">
			ORDER BY ${sortName} ${sortOrder}
		</if>
	</select>
	<select id="searchUserAll" resultMap="UserAllMap"
		parameterType="com.cloudsoaring.web.trainingplatform.entity.TpUserEntity">
		SELECT
		USER_ID,
		USER_NAME,
		PASSWORD,
		PERSON_NAME,
		EMAIL,
		SEX,
		IMAGE,
		REGION,
		WEIXIN,
		WEIBO,
		REMARK,
		DEPARTMENT_ID,
		MOBILE,
		PHONE,
		ALLOWED_IP,
		LAST_LOGIN_TIME,
		LAST_LOGIN_IP,
		LOGIN_COUNT,
		LAST_CHANGE_PWD_TIME,
		STATUS,
		DEL_FLAG,
		USER_KBN,
		COMPANY_ID,
		LOCK_FLAG,
		CREATE_DATE,
		CREATE_USER,
		UPDATE_DATE,
		UPDATE_USER,
		WORKPLACE,
		POSITION,
		tenantId,
		head_url,
		pro_name,
		city_name,
		town_name,
		school_id,
		school_name,
		school_type,
		subject_id,
		subject_name,
		school_manager
		FROM TB_USER
		WHERE DEL_FLAG = '0'
		<if test="userId != null and userId != ''">
			AND USER_ID = #{userId}
		</if>
		<if test="password != null and password != ''">
			<if test="userName != null and userName != ''">
				AND (PASSWORD = #{password} AND USER_NAME = #{userName})
			</if>
			<if test="userName == null and userName !=''">
				<if test="userId != null and userId != ''">
					AND (PASSWORD = #{password} AND USER_ID = #{userId})
				</if>
			</if>
		</if>
		<if
			test="(password == null or password == '') and userName != null and userName != ''">
			AND upper(USER_NAME) like %{CONCATTU('%', #{userName}, '%')}
		</if>
		<if test="usrName != null and usrName != ''">
			AND USER_NAME = #{usrName}
		</if>
		<if test="sex != null and sex != ''">
			AND SEX = #{sex}
		</if>
		<if test="weixin != null and weixin != ''">
			AND upper(WEIXIN) like %{CONCATTU('%', #{weixin}, '%')}
		</if>
		<if test="weibo != null and weibo != ''">
			AND upper(WEIBO) like %{CONCATTU('%', #{weibo}, '%')}
		</if>
		<if test="personName != null and personName != ''">
			AND upper(PERSON_NAME) like %{CONCATTU('%', #{personName},
			'%')}
		</if>
		<if test="email != null and email != ''">
			AND upper(EMAIL) like %{CONCATTU('%', #{email}, '%')}
		</if>
		<if test="phone != null and phone != ''">
			AND upper(PHONE) like %{CONCATTU('%', #{phone}, '%')}
		</if>
		<if test="allowedIp != null and allowedIp != ''">
			AND ALLOWED_IP = #{allowedIp}
		</if>
		<if test="status != null and status != ''">
			AND STATUS = #{status}
		</if>
		<if test="userKbn != null and userKbn != ''">
			AND USER_KBN = #{userKbn}
		</if>
		<if test="tenantId != null and tenantId != ''">
			AND tenantId = #{tenantId}
		</if>
		<if test="headurl != null and headurl != ''">
			AND head_url = #{headurl}
		</if>
		<if test="proName != null and proName != ''">
			AND pro_name = #{proName}
		</if>
		<if test="cityName != null and cityName != ''">
			AND city_name = #{cityName}
		</if>
		<if test="townName != null and townName != ''">
			AND town_name = #{townName}
		</if>
		<if test="schoolId != null and schoolId != ''">
			AND school_id = #{schoolId}
		</if>
		<if test="schoolName != null and schoolName != ''">
			AND school_name = #{schoolName}
		</if>
		<if test="schoolType != null and schoolType != ''">
			AND school_type = #{schoolType}
		</if>
		<if test="subjectId != null and subjectId != ''">
			AND subject_id = #{subjectId}
		</if>
		<if test="subjectName != null and subjectName != ''">
			AND subject_name = #{subjectName}
		</if>
		<if test="schoolManager != null and schoolManager != ''">
			AND school_manager = #{schoolManager}
		</if>
		<if test="sortName != null and sortName != ''">
			ORDER BY ${sortName} ${sortOrder}
		</if>
	</select>
	<!-- 新增用户 -->
	<insert id="insertUser"
		parameterType="com.cloudsoaring.web.trainingplatform.entity.TpUserEntity">
		INSERT INTO TB_USER(
		USER_ID,
		USER_NAME,
		PASSWORD,
		PERSON_NAME,
		EMAIL,
		SEX,
		IMAGE,
		REGION,
		WEIXIN,
		WEIBO,
		REMARK,
		DEPARTMENT_ID,
		MOBILE,
		PHONE,
		ALLOWED_IP,
		LAST_LOGIN_TIME,
		LAST_LOGIN_IP,
		LOGIN_COUNT,
		LAST_CHANGE_PWD_TIME,
		STATUS,
		DEL_FLAG,
		USER_KBN,
		COMPANY_ID,
		LOCK_FLAG,
		CREATE_DATE,
		CREATE_USER,
		UPDATE_DATE,
		UPDATE_USER,
		WORKPLACE,
		POSITION,
		tenantId,
		head_url,
		pro_name,
		city_name,
		town_name,
		school_id,
		school_name,
		school_type,
		subject_id,
		subject_name,
		duty,
		school_manager
		)VALUES(
		#{userId},
		#{userName},
		#{password},
		#{personName},
		#{email},
		#{sex},
		#{image},
		#{region},
		#{weixin},
		#{weibo},
		#{remark},
		#{departmentId},
		#{mobile},
		#{phone},
		#{allowedIp},
		NULL,
		NULL,
		0,
		NULL,
		'1',
		'0',
		#{userKbn},
		#{companyId},
		'0',
		<if test="createDate != null">
			#{createDate},
		</if>
		<if test="createDate == null">
			${SYSDATE},
		</if>
		#{createUser},
		${SYSDATE},
		#{updateUser},
		#{workplace},
		#{position},
		#{tenantId},
		#{headurl},
		#{proName},
		#{cityName},
		#{townName},
		#{schoolId},
		#{schoolName},
		#{schoolType},
		#{subjectId},
		#{subjectName},
		#{duty},
		#{schoolManager}
		)
	</insert>
	<insert id="insertUserByOut"
		parameterType="com.cloudsoaring.web.bus.entity.UserEntity">
		INSERT INTO TB_USER(
		USER_ID,
		USER_NAME,
		PASSWORD,
		PERSON_NAME,
		SEX,
		STATUS,
		DEL_FLAG,
		USER_KBN,
		LOCK_FLAG,
		CREATE_DATE,
		CREATE_USER,
		tenantId,
		head_url,
		pro_name,
		city_name,
		town_name,
		school_id,
		school_name,
		school_type,
		subject_id,
		subject_name
		)VALUES(
		#{userId},
		#{userName},
		#{password},
		#{personName},
		#{sex},
		'1',
		'0',
		#{userKbn},
		'0',
		#{createDate},
		#{createUser},
		#{tenantId},
		#{headurl},
		#{proName},
		#{cityName},
		#{townName},
		#{schoolId},
		#{schoolName},
		#{schoolType},
		#{subjectId},
		#{subjectName}
		)
	</insert>
	<!-- 更新用户信息 -->
	<update id="updateUser"
		parameterType="com.cloudsoaring.web.trainingplatform.entity.TpUserEntity">
		UPDATE TB_USER
		SET
		UPDATE_DATE = ${SYSDATE}
		,UPDATE_USER = #{updateUser}
		<if test="userName != null and userName != ''">
			,USER_NAME = #{userName}
		</if>
		<if test="createDate != null">
			,CREATE_DATE = #{createDate}
		</if>
		<if test="personName != null and personName != ''">
			,PERSON_NAME = #{personName}
		</if>
		<if test="sex != null and sex != ''">
			,SEX = #{sex}
		</if>
		<if test="region != null and region != ''">
			,REGION = #{region}
		</if>
		<if test="userKbn != null and userKbn != ''">
			,user_kbn = #{userKbn}
		</if>
		<if test="image != null and image != ''">
			,IMAGE = #{image}
		</if>
		<if test="weixin != null and weixin != ''">
			,WEIXIN = #{weixin}
		</if>
		<if test="remark != null and remark != ''">
			,REMARK = #{remark}
		</if>
		<if test="weibo != null and weibo != ''">
			,WEIBO = #{weibo}
		</if>
		<if test="departmentId != null and departmentId != ''">
			,DEPARTMENT_ID = #{departmentId}
		</if>
		<if test="email != null and email != ''">
			,EMAIL = #{email}
		</if>
		<if test="companyId != null and companyId != ''">
			,COMPANY_ID = #{companyId}
		</if>
		<if test="allowedIp != null and allowedIp != ''">
			,ALLOWED_IP = #{allowedIp}
		</if>
		<if test="password != null and password != ''">
			,PASSWORD = #{password}
			,LAST_CHANGE_PWD_TIME = ${SYSDATE}
		</if>
		<if test="status != null and status != ''">
			,STATUS = #{status}
		</if>
		<if test="mobile != null and mobile != ''">
			,MOBILE = #{mobile}
		</if>
		<if test="phone != null and phone != ''">
			,PHONE = #{phone}
		</if>
		<if test="workplace != null and workplace != ''">
			,WORKPLACE = #{workplace}
		</if>
		<if test="position != null and position != ''">
			,POSITION = #{position}
		</if>
		<if test="tenantId != null and tenantId != ''">
			,tenantId = #{tenantId}
		</if>
		<if test="headurl != null and headurl != ''">
			,head_url = #{headurl}
		</if>
		<if test="proName != null and proName != ''">
			,pro_name = #{proName}
		</if>
		<if test="cityName != null and cityName != ''">
			,city_name = #{cityName}
		</if>
		<if test="townName != null and townName != ''">
			,town_name = #{townName}
		</if>
		<if test="schoolId != null and schoolId != ''">
			,school_id = #{schoolId}
		</if>
		<if test="schoolName != null and schoolName != ''">
			,school_name = #{schoolName}
		</if>
		<if test="schoolType != null and schoolType != ''">
			,school_type = #{schoolType}
		</if>
		<if test="subjectId != null and subjectId != ''">
			,subject_id = #{subjectId}
		</if>
		<if test="subjectName != null and subjectName != ''">
			,subject_name = #{subjectName}
		</if>
		<if test="duty != null and duty != ''">
			,duty = #{duty}
		</if>
		<if test="schoolManager != null and schoolManager != ''">
			,school_manager = #{schoolManager}
		</if>
		WHERE USER_ID = #{userId}
	</update>
	<update id="updateUserAll"
		parameterType="com.cloudsoaring.web.trainingplatform.entity.TpUserEntity">
		UPDATE TB_USER
		SET
		UPDATE_DATE = ${SYSDATE}
		,UPDATE_USER = #{updateUser}
		,USER_NAME = #{userName}
		,PERSON_NAME = #{personName}
		,SEX = #{sex}
		,REGION = #{region}
		,IMAGE = #{image}
		,WEIXIN = #{weixin}
		,REMARK = #{remark}
		,WEIBO = #{weibo}
		,DEPARTMENT_ID = #{departmentId}
		,EMAIL = #{email}
		,COMPANY_ID = #{companyId}
		,ALLOWED_IP = #{allowedIp}
		,PASSWORD = #{password}
		,LAST_CHANGE_PWD_TIME = ${SYSDATE}
		,STATUS = #{status}
		,MOBILE = #{mobile}
		,PHONE = #{phone}
		,WORKPLACE = #{workplace}
		,POSITION = #{position}
		,school_manager=#{schoolManager}
		WHERE USER_ID = #{userId}
	</update>

	<update id="loginSuccess"
		parameterType="com.cloudsoaring.web.trainingplatform.entity.TpUserEntity">
		UPDATE TB_USER
		SET
		LAST_LOGIN_TIME = ${SYSDATE},
		LAST_LOGIN_IP = #{lastLoginIp},
		LOGIN_COUNT = LOGIN_COUNT + 1
		WHERE USER_ID = #{userId}
	</update>
	<select id="checkUserNameExist"
		parameterType="com.cloudsoaring.web.trainingplatform.entity.TpUserEntity"
		resultType="java.lang.Integer">
		SELECT
		COUNT(USER_NAME)
		FROM TB_USER
		WHERE
		USER_NAME = #{userName}
		and DEL_FLAG = 0
		<if test="userId != null and userId != ''">
			<![CDATA[
				AND USER_ID <> #{userId} 
			]]>
		</if>
	</select>
	<select id="checkUserExist"
		parameterType="com.cloudsoaring.web.trainingplatform.entity.TpUserEntity"
		resultType="java.lang.Integer">
		SELECT
		COUNT(USER_NAME)
		FROM TB_USER
		WHERE
		USER_NAME = #{userName}
		and PASSWORD = #{password}
		and DEL_FLAG = 0
		<if test="userId != null and userId != ''">
			<![CDATA[
				AND USER_ID <> #{userId} 
			]]>
		</if>
	</select>
	<select id="checkEmailExist"
		parameterType="com.cloudsoaring.web.trainingplatform.entity.TpUserEntity"
		resultType="java.lang.Integer">
		SELECT
		COUNT(EMAIL)
		FROM TB_USER
		WHERE
		DEL_FLAG = 0
		and EMAIL = #{email}
		<if test="userId != null and userId != ''">
			<![CDATA[
				AND USER_ID <> #{userId} 
			]]>
		</if>
	</select>
	<update id="updateDelFlagById"
		parameterType="com.cloudsoaring.web.trainingplatform.entity.TpUserEntity">
		<![CDATA[
		UPDATE TB_USER
		SET DEL_FLAG = '1'
		,UPDATE_DATE = ${SYSDATE}
		,UPDATE_USER = #{updateUser}
		WHERE USER_ID = #{userId}
		]]>
	</update>

	<update id="updateUserKbn"
		parameterType="com.cloudsoaring.web.trainingplatform.entity.TpUserEntity">
		UPDATE TB_USER
		SET
		USER_KBN = #{userKbn}
		, UPDATE_DATE = ${SYSDATE}
		, UPDATE_USER = #{updateUser}
		WHERE
		USER_ID = #{userId}
	</update>
	<delete id="deleteUserRole" parameterType="java.util.Map">
		DELETE FROM TB_USER_ROLE
		<where>
			<if test="userId != null and userId != ''">
				AND USER_ID = #{userId}
			</if>
			<if test="roleId != null and roleId != ''">
				AND ROLE_ID = #{roleId}
			</if>
		</where>
	</delete>
	<resultMap
		type="com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity"
		id="userRoleMap">
		<result property="userId" column="USER_ID" />
		<result property="roleId" column="ROLE_ID" />
		<result property="roleName" column="ROLE_NAME" />
		<result property="createUser" column="CREATE_USER" />
		<result property="createDate" column="CREATE_DATE" />
		<result property="updateUser" column="UPDATE_USER" />
		<result property="updateDate" column="UPDATE_DATE" />
	</resultMap>
	<!-- 更新用户角色为老师的用户数据 -->
	<update id="updateRoleDataUserById"
		parameterType="com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity">
		<![CDATA[
		UPDATE 
			TB_USER_ROLE
		SET 
			 ROLE_ID = #{roleId}
			,UPDATE_DATE = ${SYSDATE}
			,UPDATE_USER = #{updateUser}
		WHERE 
			USER_ID = #{userId}
		AND 
			ROLE_ID = #{roleId}
		]]>
	</update>
	<!-- 删除用户角色为老师/管理员的用户数据 -->
	<delete id="deleteByUserIdAddRoleId"
		parameterType="com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity">
		<![CDATA[
		DELETE 
			FROM TB_USER_ROLE
		WHERE	
			USER_ID = #{userId} 
		AND 
			ROLE_ID = #{roleId}
		]]>
	</delete>
	<!--检索用户是否存在角色表中 -->
	<select id="searchCountByUserId"
		parameterType="com.cloudsoaring.web.trainingplatform.entity.UserRoleEntity"
		resultType="java.lang.Integer">
		SELECT
		COUNT(1)
		FROM
		TB_USER_ROLE
		WHERE
		USER_ID = #{userId}
		AND
		ROLE_ID = #{roleId}
	</select>
	<select id="searchRoleIdByUserId" resultMap="userRoleMap">
		SELECT
		USER_ID,
		ROLE_ID,
		CREATE_DATE,
		CREATE_USER,
		UPDATE_DATE,
		UPDATE_USER
		FROM
		TB_USER_ROLE
		WHERE
		USER_ID = #{userId}
	</select>
	<select id="searchUserByUsername"
		parameterType="com.cloudsoaring.web.trainingplatform.entity.TpUserEntity"
		resultMap="UserAllMap">
		SELECT
		USER_ID,
		CREATE_DATE,
		CREATE_USER,
		UPDATE_DATE,
		UPDATE_USER
		FROM
		TB_USER
		WHERE
		USER_NAME = #{userName}
		AND
		PASSWORD = #{password}
	</select>
	<!-- 插入用户角色为老师/管理员的数据 -->
	<insert id="insertRoleDataUserById"
		parameterType="java.util.Map">
		INSERT INTO TB_USER_ROLE(
		USER_ID,
		ROLE_ID,
		CREATE_DATE,
		CREATE_USER,
		UPDATE_DATE,
		UPDATE_USER
		)VALUES(
		#{userId},
		#{roleId},
		${SYSDATE},
		#{createUser},
		${SYSDATE},
		#{updateUser}
		)
	</insert>
	<select id="searchUserNameByUserId" resultMap="UserAllMap">
		SELECT
		t.USER_ID,
		t.USER_NAME,
		t.PERSON_NAME,
		t.SEX,
		t.tenantId,
		t.head_url,
		t.pro_name,
		t.city_name,
		t.town_name,
		t.school_id,
		t.school_name,
		t.school_type,
		t.subject_id,
		t.subject_name,
		t.duty,
		t1.role isFlag

		FROM
		TB_USER t left join tb_manager_user t1 on t.USER_ID=t1.user_id
		WHERE
		t.USER_ID = #{userId}
	</select>
	<select id="selectUserByUsername" resultMap="UserAllMap">
		SELECT
		USER_ID,
		USER_NAME,
		PERSON_NAME
		FROM
		TB_USER
		WHERE
		USER_NAME = #{userName}
	</select>
	<select id="searchPlanUserAll" resultMap="UserAllMap"
		parameterType="com.cloudsoaring.web.trainingplatform.entity.TpUserEntity">
		SELECT
		USER_ID,
		USER_NAME,
		PASSWORD,
		PERSON_NAME,
		EMAIL,
		SEX,
		IMAGE,
		REGION,
		WEIXIN,
		WEIBO,
		REMARK,
		DEPARTMENT_ID,
		MOBILE,
		PHONE,
		ALLOWED_IP,
		LAST_LOGIN_TIME,
		LAST_LOGIN_IP,
		LOGIN_COUNT,
		LAST_CHANGE_PWD_TIME,
		STATUS,
		DEL_FLAG,
		USER_KBN,
		COMPANY_ID,
		LOCK_FLAG,
		CREATE_DATE,
		CREATE_USER,
		UPDATE_DATE,
		UPDATE_USER,
		WORKPLACE,
		POSITION,
		tenantId,
		head_url,
		pro_name,
		city_name,
		town_name,
		school_id,
		school_name,
		school_type,
		subject_id,
		subject_name,
		school_manager
		FROM TB_USER
		WHERE DEL_FLAG = '0'
		<if test="userId != null and userId != ''">
			AND USER_ID = #{userId}
		</if>
		<if test="password != null and password != ''">
			AND PASSWORD = #{password}
		</if>
		<if test="usrName != null and usrName != ''">
			AND upper(USER_NAME) like %{CONCATTU('%', #{usrName},
			'%')}
		</if>
		<if test="sex != null and sex != ''">
			AND SEX = #{sex}
		</if>
		<if test="weixin != null and weixin != ''">
			AND upper(WEIXIN) like %{CONCATTU('%', #{weixin}, '%')}
		</if>
		<if test="weibo != null and weibo != ''">
			AND upper(WEIBO) like %{CONCATTU('%', #{weibo}, '%')}
		</if>
		<if test="personName != null and personName != ''">
			AND upper(PERSON_NAME) like %{CONCATTU('%', #{personName},
			'%')}
		</if>
		<if test="email != null and email != ''">
			AND upper(EMAIL) like %{CONCATTU('%', #{email}, '%')}
		</if>
		<if test="phone != null and phone != ''">
			AND upper(PHONE) like %{CONCATTU('%', #{phone}, '%')}
		</if>
		<if test="allowedIp != null and allowedIp != ''">
			AND ALLOWED_IP = #{allowedIp}
		</if>
		<if test="status != null and status != ''">
			AND STATUS = #{status}
		</if>
		<if test="userKbn != null and userKbn != ''">
			AND USER_KBN = #{userKbn}
		</if>
		<if test="position != null and position != ''">
			AND upper(POSITION) like %{CONCATTU('%', #{position},
			'%')}
		</if>
		<if test="tenantId != null and tenantId != ''">
			AND tenantId = #{tenantId}
		</if>
		<if test="headurl != null and headurl != ''">
			AND head_url = #{headurl}
		</if>
		<if test="proName != null and proName != ''">
			AND pro_name = #{proName}
		</if>
		<if test="cityName != null and cityName != ''">
			AND city_name = #{cityName}
		</if>
		<if test="townName != null and townName != ''">
			AND town_name = #{townName}
		</if>
		<if test="schoolId != null and schoolId != ''">
			AND school_id = #{schoolId}
		</if>
		<if test="schoolName != null and schoolName != ''">
			AND school_name = #{schoolName}
		</if>
		<if test="schoolType != null and schoolType != ''">
			AND school_type = #{schoolType}
		</if>
		<if test="subjectId != null and subjectId != ''">
			AND subject_id = #{subjectId}
		</if>
		<if test="subjectName != null and subjectName != ''">
			AND subject_name = #{subjectName}
		</if>
		<if test="schoolManager != null and schoolManager != ''">
			AND school_manager = #{schoolManager}
		</if>
		<if test="sortName != null and sortName != ''">
			ORDER BY ${sortName} ${sortOrder}
		</if>
	</select>
	<select id="searchManagerUserAll" resultMap="UserAllMap"
		parameterType="com.cloudsoaring.web.trainingplatform.entity.TpUserEntity">
		SELECT
		USER_ID,
		USER_NAME,
		PASSWORD,
		PERSON_NAME,
		EMAIL,
		SEX,
		IMAGE,
		REGION,
		WEIXIN,
		WEIBO,
		REMARK,
		DEPARTMENT_ID,
		MOBILE,
		PHONE,
		ALLOWED_IP,
		LAST_LOGIN_TIME,
		LAST_LOGIN_IP,
		LOGIN_COUNT,
		LAST_CHANGE_PWD_TIME,
		STATUS,
		DEL_FLAG,
		USER_KBN,
		COMPANY_ID,
		LOCK_FLAG,
		CREATE_DATE,
		CREATE_USER,
		UPDATE_DATE,
		UPDATE_USER,
		WORKPLACE,
		POSITION,
		tenantId,
		head_url,
		pro_name,
		city_name,
		town_name,
		school_id,
		school_name,
		school_type,
		subject_id,
		subject_name,
		school_manager
		FROM TB_USER
		WHERE DEL_FLAG = '0'
		and (USER_KBN!='C' and USER_KBN!='D')
		<if test="userId != null and userId != ''">
			AND USER_ID = #{userId}
		</if>
		<if test="password != null and password != ''">
			<if test="userName != null and userName != ''">
				AND (PASSWORD = #{password} AND USER_NAME = #{userName})
			</if>
			<if test="userName == null and userName !=''">
				<if test="userId != null and userId != ''">
					AND (PASSWORD = #{password} AND USER_ID = #{userId})
				</if>
			</if>
		</if>
		<if
			test="(password == null or password == '') and userName != null and userName != ''">
			AND upper(USER_NAME) like %{CONCATTU('%', #{userName}, '%')}
		</if>
		<if test="usrName != null and usrName != ''">
			AND (USER_NAME = #{usrName} or upper(PERSON_NAME) like
			%{CONCATTU('%', #{usrName}, '%')})
		</if>
		<if test="sex != null and sex != ''">
			AND SEX = #{sex}
		</if>
		<if test="weixin != null and weixin != ''">
			AND upper(WEIXIN) like %{CONCATTU('%', #{weixin}, '%')}
		</if>
		<if test="weibo != null and weibo != ''">
			AND upper(WEIBO) like %{CONCATTU('%', #{weibo}, '%')}
		</if>
		<if test="personName != null and personName != ''">
			AND upper(PERSON_NAME) like %{CONCATTU('%', #{personName},
			'%')}
		</if>
		<if test="email != null and email != ''">
			AND upper(EMAIL) like %{CONCATTU('%', #{email}, '%')}
		</if>
		<if test="phone != null and phone != ''">
			AND upper(PHONE) like %{CONCATTU('%', #{phone}, '%')}
		</if>
		<if test="allowedIp != null and allowedIp != ''">
			AND ALLOWED_IP = #{allowedIp}
		</if>
		<if test="status != null and status != ''">
			AND STATUS = #{status}
		</if>
		<if test="userKbn != null and userKbn != ''">
			AND USER_KBN = #{userKbn}
		</if>
		<if test="tenantId != null and tenantId != ''">
			AND tenantId = #{tenantId}
		</if>
		<if test="headurl != null and headurl != ''">
			AND head_url = #{headurl}
		</if>
		<if test="proName != null and proName != ''">
			AND pro_name = #{proName}
		</if>
		<if test="cityName != null and cityName != ''">
			AND city_name = #{cityName}
		</if>
		<if test="townName != null and townName != ''">
			AND town_name = #{townName}
		</if>
		<if test="schoolId != null and schoolId != ''">
			AND school_id = #{schoolId}
		</if>
		<if test="schoolName != null and schoolName != ''">
			AND school_name = #{schoolName}
		</if>
		<if test="schoolType != null and schoolType != ''">
			AND school_type = #{schoolType}
		</if>
		<if test="subjectId != null and subjectId != ''">
			AND subject_id = #{subjectId}
		</if>
		<if test="subjectName != null and subjectName != ''">
			AND subject_name = #{subjectName}
		</if>
		<if test="schoolManager != null and schoolManager != ''">
			AND school_manager = #{schoolManager}
		</if>
		<if test="sortName != null and sortName != ''">
			ORDER BY ${sortName} ${sortOrder}
		</if>
	</select>
	<select id="searchUserByid" resultMap="UserMap"
		parameterType="com.cloudsoaring.web.trainingplatform.entity.TpUserEntity">
		<![CDATA[
		SELECT
			DISTINCT
			USER_ID,
			USER_NAME,
			PERSON_NAME,
			EMAIL,
			USER_KBN,
			SEX,
			IMAGE,
			REGION,
			WEIXIN,
			WEIBO,
			LOCK_FLAG,
			MOBILE,
			PHONE,
			REMARK,
			ALLOWED_IP,
			LAST_LOGIN_TIME,
			LAST_LOGIN_IP,
			LOGIN_COUNT,
			DISPLAY_THEME,
			LAST_CHANGE_PWD_TIME,
			STATUS,
			DEL_FLAG,
			WORKPLACE,
			POSITION,
			tenantId,
			head_url,
			pro_name,
			city_name,
			town_name,
			school_id,
			school_name,
			school_type,
			subject_id,
			subject_name,
			duty,
			school_manager
		FROM TB_USER
		WHERE DEL_FLAG = '0' 
		]]>
		<if test="roleId != null and roleId != ''">
			AND EXISTS
			(
			SELECT 1 from tb_user_role WHERE ROLE_ID = #{roleId} and TB_USER.USER_ID =
			tb_user_role.USER_ID
			)
		</if>
		<if test="userId != null and userId != ''">
			AND USER_ID = #{userId}
		</if>
		<if test="userName != null and userName != ''">
			AND upper(USER_NAME) like %{CONCATTU('%', #{userName},
			'%')}
		</if>
		<if test="sex != null and sex != ''">
			AND SEX = #{sex}
		</if>
		<if test="weixin != null and weixin != ''">
			AND upper(WEIXIN) like %{CONCATTU('%', #{weixin}, '%')}
		</if>
		<if test="weibo != null and weibo != ''">
			AND upper(WEIBO) like %{CONCATTU('%', #{weibo}, '%')}
		</if>
		<if test="personName != null and personName != ''">
			AND upper(PERSON_NAME) like %{CONCATTU('%', #{personName},
			'%')}
		</if>
		<if test="email != null and email != ''">
			AND upper(EMAIL) like %{CONCATTU('%', #{email}, '%')}
		</if>
		<if test="phone != null and phone != ''">
			AND upper(PHONE) like %{CONCATTU('%', #{phone}, '%')}
		</if>
		<if test="allowedIp != null and allowedIp != ''">
			AND ALLOWED_IP = #{allowedIp}
		</if>
		<if test="status != null and status != ''">
			AND STATUS = #{status}
		</if>
		<if test="userKbn != null and userKbn != ''">
			AND USER_KBN = #{userKbn}
		</if>
		<if test="schoolManager != null and schoolManager != ''">
			AND school_manager = #{schoolManager}
		</if>
		<if test="sortName != null and sortName != ''">
			ORDER BY ${sortName} ${sortOrder}
		</if>
	</select>
</mapper>