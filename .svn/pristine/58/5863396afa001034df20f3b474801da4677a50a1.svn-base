package com.cloudsoaring.web.institution.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.cloudsoaring.common.utils.StringUtil;
import com.cloudsoaring.web.WebContext;
import com.cloudsoaring.web.common.service.BaseService;
import com.cloudsoaring.web.common.view.ResultBean;
import com.cloudsoaring.web.course.constant.CourseConstants;
import com.cloudsoaring.web.course.entity.MeasurementEntity;
import com.cloudsoaring.web.course.entity.QuestionEntity;
import com.cloudsoaring.web.course.entity.QuestionOptionEntity;
import com.cloudsoaring.web.course.entity.UserMeasuermentAnswerEntity;
import com.cloudsoaring.web.course.entity.UserMeasurementResultEntity;
import com.cloudsoaring.web.course.view.QuestionView;
import com.cloudsoaring.web.institution.entity.MeasurementInstitutionEntity;
import com.cloudsoaring.web.institution.entity.QuestionInstitutionEntity;
import com.cloudsoaring.web.institution.entity.QuestionOptionInstitutionEntity;
import com.cloudsoaring.web.taxonomy.entity.QuestionInstitutionView;

@Service
public class QuestionInstitutionService extends BaseService implements CourseConstants {

	/**
	 * 问卷题目选项
	 */
	@SuppressWarnings("unchecked")
	public ResultBean questionnaireDetial(String linkId, String linkType) {
		ResultBean result = new ResultBean();
		// 链接ID为空
		assertNotEmpty(linkId, MSG_E_NULL_ID);
		// 链接类型为空
		assertNotEmpty(linkType, MSG_E_EMPTY_LINK_TYPE);
		// 链接类型错误：不等于COURSE
		if (!linkType.equalsIgnoreCase(LINK_TYPE_EVALUTION)) {
			result.setStatus(false);
			result.setMessages(MSG_E_ERROR_LINK_TYPE);
			return result;
		}
		//声明评价对象
		MeasurementInstitutionEntity meas = new MeasurementInstitutionEntity();
		meas.setLinkId(linkId);
		meas.setLinkType(linkType);
		meas.setMeasureType(MEASUERMENT_TYPE_QUESTIO);
		meas = commonDao.searchOneData(meas, MEASUREMENT_EVALUTION_SELECT);
		// 问卷不存在
		if (meas == null) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_MEAS_NOT_EXIST));
			return result;
		}
		//声明问题对象
		List<QuestionInstitutionView> questionViews = new ArrayList<QuestionInstitutionView>();
		QuestionInstitutionView questionView = new QuestionInstitutionView();
		questionView.setMeasureId(meas.getId());
		questionView.setUserId(WebContext.getSessionUserId());
		questionViews = commonDao
				.searchList(questionView, QUESTION_EVALUTION_SELECT_VIEW);
		// 题目不存在
		if (questionViews == null || questionViews.size() == 0) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_QUESTION_NOT_EXIST));
			return result;
		}
		for (int i = 0; i < questionViews.size(); i++) {
			QuestionOptionInstitutionEntity questionOptionEntity = new QuestionOptionInstitutionEntity();
			questionOptionEntity.setQuestionId(questionViews.get(i).getId());
			List<QuestionOptionInstitutionEntity> option = commonDao.searchList(
					questionOptionEntity, QUESTION_EVALUTION_OPTION_SELECT);
			// 选项不存在
			if (option == null || option.size() == 0) {
				result.setStatus(false);
				result.setMessages(getMessage(MSG_E_OPTION_NOT_EXIST));
				return result;
			}
			questionViews.get(i).setOption(option);
		}

		return result.setData(questionViews);
	}

	/**
	 * 问卷提交
	 * @param questionIds,optionIds都已","分割的id字符串
	 */
	public ResultBean submitQuestionnaire(String questionIds, String optionIds ,String measureId) {
		ResultBean result = new ResultBean();
		// 题目或选项为空
		if (questionIds == null || optionIds == null || optionIds .equals("") 
				|| questionIds .equals("")) {
			result.setStatus(false);
			result.setMessages(MSG_E_QUESTION_OPTION_NULL);
			return result;
		}
		String[] question = questionIds.split(",");
		String[] option = optionIds.split(",");
		List<String> questionId = Arrays.asList(question);
		List<String> optionId = Arrays.asList(option);
		// 判断用户是否登录
		if (WebContext.isGuest()) {
			result.setStatus(false);
			result.setMessages(getMessage(NULL_USER_ID));
			return result;
		}
		//判断学校是否存在
		if(StringUtil.isEmpty(measureId)){
			QuestionInstitutionEntity query = new QuestionInstitutionEntity();
			query.setId(question[0]);
			QuestionInstitutionEntity queryOne = searchOneData(query);
			if(queryOne == null){
				result.setStatus(false);
				result.setMessages("系统异常，请联系管理员");
				return result;
			}
			measureId = queryOne.getMeasureId();
		}
		//循环遍历问题列表
		for(int i=0;i<questionId.size();i++){
			String[] opIds = optionId.get(i).split(";");
			//循环遍历问题选项
			for(int j=0;j<opIds.length;j++){
				QuestionOptionInstitutionEntity questionOptionEntity = new QuestionOptionInstitutionEntity();
				questionOptionEntity.setId(opIds[j]);
				//更新问题选项
				commonDao.updateData(questionOptionEntity, QUESTION_EVALUTION_OPTION_UPDATE_NUM);
			}
			//保存评价数据
			UserMeasuermentAnswerEntity umae = new UserMeasuermentAnswerEntity();
			umae.setUserId(WebContext.getSessionUserId());
			//选项id
			umae.setOptionId(optionId.get(i));
			//问题id
			umae.setQuestionId(questionId.get(i));
			delete(umae);
			commonDao.insertData(umae, MEASUERMENT_ANSWER_INSERT);
			//更新问题数据表
			QuestionInstitutionEntity questionEntity = new QuestionInstitutionEntity();
			questionEntity.setId(questionId.get(i));
			commonDao.updateData(questionEntity, QUESTION_EVALUTION_UPDATE_NUM);
		}
		//保存测试评价数据
		UserMeasurementResultEntity userMeasurementResultEntity = new UserMeasurementResultEntity();
        //评价id
		userMeasurementResultEntity.setUserId(WebContext.getSessionUserId());
		delete(userMeasurementResultEntity);
		commonDao.insertData(userMeasurementResultEntity, MEASUERMENT_RESULT_INSERT);
		return result.setStatus(true);
	}
	/**
	
	
	 @SuppressWarnings("unchecked")
	public ResultBean questionnaireDetial3(String linkId, String linkType) {
		ResultBean result = new ResultBean();
		// 链接ID为空
		assertNotEmpty(linkId, MSG_E_NULL_ID);
		// 链接类型为空
		assertNotEmpty(linkType, MSG_E_EMPTY_LINK_TYPE);
		// 链接类型错误：不等于COURSE
		if (!linkType.equalsIgnoreCase(LINK_TYPE_EVALUTION)) {
			result.setStatus(false);
			result.setMessages(MSG_E_ERROR_LINK_TYPE);
			return result;
		}
		//声明评价对象
		MeasurementInstitutionEntity meas = new MeasurementInstitutionEntity();
		meas.setLinkId(linkId);
		meas.setLinkType(linkType);
		meas.setMeasureType(MEASUERMENT_TYPE_QUESTIO);
		meas = commonDao.searchOneData(meas, MEASUREMENT_EVALUTION_SELECT);
		// 问卷不存在
		if (meas == null) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_MEAS_NOT_EXIST));
			return result;
		}
		//声明问题对象
		List<QuestionInstitutionView> questionViews = new ArrayList<QuestionInstitutionView>();
		QuestionInstitutionView questionView = new QuestionInstitutionView();
		questionView.setMeasureId(meas.getId());
questionView.setUserId(WebContext.getSessionUserId());
		questionViews = commonDao
				.searchList(questionView, QUESTION_EVALUTION_SELECT_VIEW);
		return result.setData(questionViews);
	} 
	 @SuppressWarnings("unchecked")
	public ResultBean questionnaireDetial5(String linkId, String linkType) {
		ResultBean result = new ResultBean();
		// 链接ID为空
		assertNotEmpty(linkId, MSG_E_NULL_ID);
		// 链接类型为空
		assertNotEmpty(linkType, MSG_E_EMPTY_LINK_TYPE);
		// 链接类型错误：不等于COURSE
		if (!linkType.equalsIgnoreCase(LINK_TYPE_EVALUTION)) {
			result.setStatus(false);
			result.setMessages(MSG_E_ERROR_LINK_TYPE);
			return result;
		}
		//声明评价对象
		MeasurementInstitutionEntity meas = new MeasurementInstitutionEntity();
		meas.setLinkId(linkId);
		meas.setLinkType(linkType);
		meas.setMeasureType(MEASUERMENT_TYPE_QUESTIO);
		meas = commonDao.searchOneData(meas, MEASUREMENT_EVALUTION_SELECT);
		// 问卷不存在
		if (meas == null) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_MEAS_NOT_EXIST));
			return result;
		}
		//声明问题对象
		List<QuestionInstitutionView> questionViews = new ArrayList<QuestionInstitutionView>();
		QuestionInstitutionView questionView = new QuestionInstitutionView();
		questionView.setMeasureId(meas.getId());
		questionView.setUserId(WebContext.getSessionUserId());
		questionViews = commonDao
				.searchList(questionView, QUESTION_EVALUTION_SELECT_VIEW);
		// 题目不存在
		if (questionViews == null || questionViews.size() == 0) {
			result.setStatus(false);
			result.setMessages(getMessage(MSG_E_QUESTION_NOT_EXIST));
			return result;
		}
		for (int i = 0; i < questionViews.size(); i++) {
			QuestionOptionInstitutionEntity questionOptionEntity = new QuestionOptionInstitutionEntity();
			questionOptionEntity.setQuestionId(questionViews.get(i).getId());
			List<QuestionOptionInstitutionEntity> option = commonDao.searchList(
					questionOptionEntity, QUESTION_EVALUTION_OPTION_SELECT);
			// 选项不存在
			if (option == null || option.size() == 0) {
				result.setStatus(false);
				result.setMessages(getMessage(MSG_E_OPTION_NOT_EXIST));
				return result;
			}
			questionViews.get(i).setOption(option);
		}

		return result.setData(questionViews);
	} 
	 */
}
